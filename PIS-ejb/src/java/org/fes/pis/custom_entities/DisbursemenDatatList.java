/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.custom_entities;

import java.math.BigDecimal;

/**
 *
 * @author anu
 */
public class DisbursemenDatatList {
    
    Integer user_id;
    String from_month;
    String to_month;
    BigDecimal total_pay;
    Integer emp_non_payable_days;
    Integer rowid;
    String user_name;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
    

    public Integer getEmp_non_payable_days() {
        return emp_non_payable_days;
    }

    public void setEmp_non_payable_days(Integer emp_non_payable_days) {
        this.emp_non_payable_days = emp_non_payable_days;
    }

    public String getFrom_month() {
        return from_month;
    }

    public void setFrom_month(String from_month) {
        this.from_month = from_month;
    }

    public Integer getRowid() {
        return rowid;
    }

    public void setRowid(Integer rowid) {
        this.rowid = rowid;
    }

    public String getTo_month() {
        return to_month;
    }

    public void setTo_month(String to_month) {
        this.to_month = to_month;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public BigDecimal getTotal_pay() {
        return total_pay;
    }

    public void setTotal_pay(BigDecimal total_pay) {
        this.total_pay = total_pay;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DisbursemenDatatList other = (DisbursemenDatatList) obj;
        if (this.rowid != other.rowid && (this.rowid == null || !this.rowid.equals(other.rowid))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + (this.rowid != null ? this.rowid.hashCode() : 0);
        return hash;
    }
    
    
}
