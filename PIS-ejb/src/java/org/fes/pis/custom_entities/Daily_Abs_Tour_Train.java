package org.fes.pis.custom_entities;

import java.util.Date;
import org.fes.pis.entities.FtasAbsentees;
import org.fes.pis.entities.FtasDaily;
import org.fes.pis.entities.FtasTourScheduleDtl;
import org.fes.pis.entities.FtasTrainingScheduleDtl;

public class Daily_Abs_Tour_Train {

    Integer srno;
    Date attddt;
    FtasDaily ftasDaily;
    FtasTourScheduleDtl ftasTourScheduleDtl;
    FtasTrainingScheduleDtl ftasTrainingScheduleDtl;
    FtasAbsentees ftasAbsentees;
    String description;
    String place;

    public FtasDaily getFtasDaily() {
        return ftasDaily;
    }

    public void setFtasDaily(FtasDaily ftasDaily) {
        this.ftasDaily = ftasDaily;
    }

    public Date getAttddt() {
        return attddt;
    }

    public void setAttddt(Date attddt) {
        this.attddt = attddt;
    }

    public FtasAbsentees getFtasAbsentees() {
        return ftasAbsentees;
    }

    public void setFtasAbsentees(FtasAbsentees ftasAbsentees) {
        this.ftasAbsentees = ftasAbsentees;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public FtasTourScheduleDtl getFtasTourScheduleDtl() {
        return ftasTourScheduleDtl;
    }

    public void setFtasTourScheduleDtl(FtasTourScheduleDtl ftasTourScheduleDtl) {
        this.ftasTourScheduleDtl = ftasTourScheduleDtl;
    }

    public FtasTrainingScheduleDtl getFtasTrainingScheduleDtl() {
        return ftasTrainingScheduleDtl;
    }

    public void setFtasTrainingScheduleDtl(FtasTrainingScheduleDtl ftasTrainingScheduleDtl) {
        this.ftasTrainingScheduleDtl = ftasTrainingScheduleDtl;
    }

    public Integer getSrno() {
        return srno;
    }

    public void setSrno(Integer srno) {
        this.srno = srno;
    }

    public Daily_Abs_Tour_Train(Integer srno, Date attddt) {
        this.srno = srno;
        this.attddt = attddt;
    }

    public Daily_Abs_Tour_Train(Integer srno) {
        this.srno = srno;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Daily_Abs_Tour_Train other = (Daily_Abs_Tour_Train) obj;
        if (this.srno != other.srno && (this.srno == null || !this.srno.equals(other.srno))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.srno != null ? this.srno.hashCode() : 0);
        return hash;
    }
}
