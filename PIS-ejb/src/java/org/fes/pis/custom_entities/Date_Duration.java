package org.fes.pis.custom_entities;

import java.io.Serializable;
import java.util.Date;


public class Date_Duration  implements Serializable{
    Date start_date;
    Date end_date;

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date_Duration() {
    }
    public Date_Duration(Date start_date,Date end_date) {
        this.start_date=start_date;
        this.end_date=end_date;
    }

}
