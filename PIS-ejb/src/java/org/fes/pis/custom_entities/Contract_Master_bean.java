package org.fes.pis.custom_entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


public class Contract_Master_bean implements Serializable{

    public Contract_Master_bean() {
    }

    BigDecimal rgc_srg_key;
    String rgc_desc;
    BigDecimal ragm_srg_key;
    String ragm_desc;
    Date start_date;
    Date end_date;
    Integer rowid;

    //For Tree Table in Contract Master -- cmsrgkey,cont_type,parent_cmsrgkey,ragmsrgkey,vDESC,start_date,end_date,grp_type,lev

    BigDecimal cm_srg_key;
    String contract_type;
    BigDecimal parent_cm_srg_key;
    String description;
    String group_type;
    Integer level;
    Integer contract_period_no;
    String contract_period_unit;
    String type;

    public BigDecimal getCm_srg_key() {
        return cm_srg_key;
    }

    public void setCm_srg_key(BigDecimal cm_srg_key) {
        this.cm_srg_key = cm_srg_key;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getContract_period_no() {
        return contract_period_no;
    }

    public void setContract_period_no(Integer contract_period_no) {
        this.contract_period_no = contract_period_no;
    }

    public String getContract_period_unit() {
        return contract_period_unit;
    }

    public void setContract_period_unit(String contract_period_unit) {
        this.contract_period_unit = contract_period_unit;
    }

    public String getContract_type() {
        return contract_type;
    }

    public void setContract_type(String contract_type) {
        this.contract_type = contract_type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGroup_type() {
        return group_type;
    }

    public void setGroup_type(String group_type) {
        this.group_type = group_type;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public BigDecimal getParent_cm_srg_key() {
        return parent_cm_srg_key;
    }

    public void setParent_cm_srg_key(BigDecimal parent_cm_srg_key) {
        this.parent_cm_srg_key = parent_cm_srg_key;
    }




    public Integer getRowid() {
        return rowid;
    }

    public void setRowid(Integer rowid) {
        this.rowid = rowid;
    }


    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public String getRagm_desc() {
        return ragm_desc;
    }

    public void setRagm_desc(String ragm_desc) {
        this.ragm_desc = ragm_desc;
    }

    public BigDecimal getRagm_srg_key() {
        return ragm_srg_key;
    }

    public void setRagm_srg_key(BigDecimal ragm_srg_key) {
        this.ragm_srg_key = ragm_srg_key;
    }

    public String getRgc_desc() {
        return rgc_desc;
    }

    public void setRgc_desc(String rgc_desc) {
        this.rgc_desc = rgc_desc;
    }

    public BigDecimal getRgc_srg_key() {
        return rgc_srg_key;
    }

    public void setRgc_srg_key(BigDecimal rgc_srg_key) {
        this.rgc_srg_key = rgc_srg_key;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }


}
