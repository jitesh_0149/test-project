package org.fes.pis.custom_entities;

import java.util.ArrayList;
import java.util.List;

public class AlternateGroupSet {

    int altSrno;
    List<String> lst_GroupDescs = new ArrayList<String>();

    public int getAltSrno() {
        return altSrno;
    }

    public void setAltSrno(int altSrno) {
        this.altSrno = altSrno;
    }

    public List<String> getLst_GroupDescs() {
        return lst_GroupDescs;
    }

    public void setLst_GroupDescs(List<String> lst_GroupDescs) {
        this.lst_GroupDescs = lst_GroupDescs;
    }

    public AlternateGroupSet() {
    }

    public AlternateGroupSet(int altSrno) {
        this.altSrno = altSrno;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AlternateGroupSet other = (AlternateGroupSet) obj;
        if (this.altSrno != other.altSrno) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + this.altSrno;
        return hash;
    }
}
