/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.custom_entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author anu
 */
public class ExperienceBean implements Serializable {

    public ExperienceBean() {
    }
    String company_name;
    String designation;
    String grosspay;
    String perks;
    Date exp_from_date;
    Date exp_to_date;
    String years;
    String remarks;
    String company_adrress_no;
    Integer rowid;
    BigDecimal exSrgKey;
    boolean isEdit;

    public BigDecimal getExSrgKey() {
        return exSrgKey;
    }

    public void setExSrgKey(BigDecimal exSrgKey) {
        this.exSrgKey = exSrgKey;
    }

    public String getCompany_adrress_no() {
        return company_adrress_no;
    }

    public void setCompany_adrress_no(String company_adrress_no) {
        this.company_adrress_no = company_adrress_no;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Date getExp_from_date() {
        return exp_from_date;
    }

    public void setExp_from_date(Date exp_from_date) {
        this.exp_from_date = exp_from_date;
    }

    public Date getExp_to_date() {
        return exp_to_date;
    }

    public void setExp_to_date(Date exp_to_date) {
        this.exp_to_date = exp_to_date;
    }

    public String getGrosspay() {
        return grosspay;
    }

    public void setGrosspay(String grosspay) {
        this.grosspay = grosspay;
    }

    public String getPerks() {
        return perks;
    }

    public void setPerks(String perks) {
        this.perks = perks;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getYears() {
        return years;
    }

    public void setYears(String years) {
        this.years = years;
    }

    public Integer getRowid() {
        return rowid;
    }

    public void setRowid(Integer rowid) {
        this.rowid = rowid;
    }

    public boolean isIsEdit() {
        return isEdit;
    }

    public void setIsEdit(boolean isEdit) {
        this.isEdit = isEdit;
    }
}
