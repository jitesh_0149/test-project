/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.custom_entities;

import org.fes.pis.entities.FtasTourScheduleDtl;

/**
 *
 * @author anu
 */
public class FtasTourScheduleDtlEntry {

    FtasTourScheduleDtl entFtasTourScheduleDtl;
    boolean lastRow;

    public FtasTourScheduleDtl getEntFtasTourScheduleDtl() {
        return entFtasTourScheduleDtl;
    }

    public void setEntFtasTourScheduleDtl(FtasTourScheduleDtl entFtasTourScheduleDtl) {
        this.entFtasTourScheduleDtl = entFtasTourScheduleDtl;
    }

    public boolean isLastRow() {
        return lastRow;
    }

    public void setLastRow(boolean lastRow) {
        this.lastRow = lastRow;
    }

    public FtasTourScheduleDtlEntry() {
    }

    public FtasTourScheduleDtlEntry(FtasTourScheduleDtl entFtasTourScheduleDtl) {
        this.entFtasTourScheduleDtl = entFtasTourScheduleDtl;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FtasTourScheduleDtlEntry other = (FtasTourScheduleDtlEntry) obj;
        if (this.entFtasTourScheduleDtl != other.entFtasTourScheduleDtl && (this.entFtasTourScheduleDtl == null || !this.entFtasTourScheduleDtl.equals(other.entFtasTourScheduleDtl))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (this.entFtasTourScheduleDtl != null ? this.entFtasTourScheduleDtl.hashCode() : 0);
        return hash;
    }
}
