/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.custom_entities;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author anu
 */
public class AttendanceLockDataList {

    Integer rowid;
    Integer user_id;
    String user_name;
    Date from_date;
    Date to_date;
    BigDecimal leaves;
    BigDecimal pending_days;
    Integer oum_unit_srno;
    String oum_name;
    String other_reason;

    public String getOther_reason() {
        return other_reason;
    }

    public void setOther_reason(String other_reason) {
        this.other_reason = other_reason;
    }

    public String getOum_name() {
        return oum_name;
    }

    public void setOum_name(String oum_name) {
        this.oum_name = oum_name;
    }

    public Integer getOum_unit_srno() {
        return oum_unit_srno;
    }

    public void setOum_unit_srno(Integer oum_unit_srno) {
        this.oum_unit_srno = oum_unit_srno;
    }

    public Date getFrom_date() {
        return from_date;
    }

    public void setFrom_date(Date from_date) {
        this.from_date = from_date;
    }

    public BigDecimal getLeaves() {
        return leaves;
    }

    public void setLeaves(BigDecimal leaves) {
        this.leaves = leaves;
    }

    public BigDecimal getPending_days() {
        return pending_days;
    }

    public void setPending_days(BigDecimal pending_days) {
        this.pending_days = pending_days;
    }

    public Integer getRowid() {
        return rowid;
    }

    public void setRowid(Integer rowid) {
        this.rowid = rowid;
    }

    public Date getTo_date() {
        return to_date;
    }

    public void setTo_date(Date to_date) {
        this.to_date = to_date;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AttendanceLockDataList other = (AttendanceLockDataList) obj;
        if (this.rowid != other.rowid && (this.rowid == null || !this.rowid.equals(other.rowid))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + (this.rowid != null ? this.rowid.hashCode() : 0);
        return hash;
    }
}
