/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.custom_entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author anu
 */
public class Daily_Missing_Attendance implements Serializable {

    public Integer id;
    public Date missing_attendance;
    public Integer partial_half_no;
    public String statement_operation;
    public BigDecimal dly_srg_key;
    public String placeHalf1;
    public String placeHalf2;

    public BigDecimal getDly_srg_key() {
        return dly_srg_key;
    }

    public void setDly_srg_key(BigDecimal dly_srg_key) {
        this.dly_srg_key = dly_srg_key;
    }

    public Date getMissing_attendance() {
        return missing_attendance;
    }

    public void setMissing_attendance(Date missing_attendance) {
        this.missing_attendance = missing_attendance;
    }

    public Integer getPartial_half_no() {
        return partial_half_no;
    }

    public void setPartial_half_no(Integer partial_half_no) {
        this.partial_half_no = partial_half_no;
    }

    public String getStatement_operation() {
        return statement_operation;
    }

    public void setStatement_operation(String statement_operation) {
        this.statement_operation = statement_operation;
    }

    public String getPlaceHalf1() {
        return placeHalf1;
    }

    public void setPlaceHalf1(String placeHalf1) {
        this.placeHalf1 = placeHalf1;
    }

    public String getPlaceHalf2() {
        return placeHalf2;
    }

    public void setPlaceHalf2(String placeHalf2) {
        this.placeHalf2 = placeHalf2;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Daily_Missing_Attendance(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Daily_Missing_Attendance other = (Daily_Missing_Attendance) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
}
