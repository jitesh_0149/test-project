package org.fes.pis.custom_entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Locale;

public class System_Properties implements Serializable {
    //<editor-fold defaultstate="collapsed" desc="version">

    public static final String version = "1.0";
    //Major release number
    //Minor release number
    //Maintenance release number (bugfixes only)
    //If used at all: build number (or source control revision number)
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="rupee_symbol">
    String rupee_symbol = "<span>&#x20B9;</span>";//<span class=\"WebRupee\">Rs.</span>";
    String rupee_symbol_margin = "<span style=\"margin-left:5px;margin-right:5px;\">&#x20B9;</span>";//"<span class=\"WebRupee-Margin\">Rs.</span>";
    String rupee_symbol_margin_right = "<span style=\"margin-right:5px;\">&#x20B9;</span>";//"<span class=\"WebRupee-Margin-Right\">Rs.</span>";
    String rupee_symbol_margin_left = "<span style=\"margin-left:5px;\">&#x20B9;</span>";//"<span class=\"WebRupee-Margin-Left\">Rs.</span>";

    public String getRupee_symbol() {
        return rupee_symbol;
    }

    public void setRupee_symbol(String rupee_symbol) {
        this.rupee_symbol = rupee_symbol;
    }

    public String getRupee_symbol_margin() {
        return rupee_symbol_margin;
    }

    public void setRupee_symbol_margin(String rupee_symbol_margin) {
        this.rupee_symbol_margin = rupee_symbol_margin;
    }

    public String getRupee_symbol_margin_left() {
        return rupee_symbol_margin_left;
    }

    public void setRupee_symbol_margin_left(String rupee_symbol_margin_left) {
        this.rupee_symbol_margin_left = rupee_symbol_margin_left;
    }

    public String getRupee_symbol_margin_right() {
        return rupee_symbol_margin_right;
    }

    public void setRupee_symbol_margin_right(String rupee_symbol_margin_right) {
        this.rupee_symbol_margin_right = rupee_symbol_margin_right;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="SystemName">
    String SystemName = "PIS";
    String SystemUrl = "PIS";

    public String getSystemUrl() {
        return SystemUrl;
    }

    public void setSystemUrl(String SystemUrl) {
        this.SystemUrl = SystemUrl;
    }

    public String getSystemName() {
        return SystemName;
    }

    public void setSystemName(String SystemName) {
        this.SystemName = SystemName;
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="india_standard_time">
    String india_standard_time = "GMT+5:30";

    public String getIndia_standard_time() {
        return india_standard_time;
    }

    public void setIndia_standard_time(String india_standard_time) {
        this.india_standard_time = india_standard_time;
    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="countryName">
    String countryName = Locale.getDefault().getDisplayCountry().toUpperCase();

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="systemStartDate">
    static final Date attdSystemStartDate = new Date(2014 - 1900, 10, 6);

    public Date getAttdSystemStartDate() {

        return attdSystemStartDate;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Default Constructor">
    public System_Properties() {
    }
    //</editor-fold>
}
