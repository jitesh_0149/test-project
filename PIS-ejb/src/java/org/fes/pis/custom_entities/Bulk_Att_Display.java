/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fes.pis.custom_entities;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author anu
 */

public class Bulk_Att_Display implements Serializable{

    String user_id=new String();
    String user_name=new String();
    String att_date=new String();
    String att_time=new String();
    String from_half=new String();
    String from_half_time=new String();
    String to_half=new String();
    String to_half_time=new String();
    String oum_unit_srno=new String();
    BigDecimal dly_srg_key;

    public String getAtt_date() {
        return att_date;
    }

    public void setAtt_date(String att_date) {
        this.att_date = att_date;
    }

    public String getAtt_time() {
        return att_time;
    }

    public void setAtt_time(String att_time) {
        this.att_time = att_time;
    }

    public String getFrom_half() {
        return from_half;
    }

    public void setFrom_half(String from_half) {
        this.from_half = from_half;
    }

    public String getFrom_half_time() {
        return from_half_time;
    }

    public void setFrom_half_time(String from_half_time) {
        this.from_half_time = from_half_time;
    }

    public String getOum_unit_srno() {
        return oum_unit_srno;
    }

    public void setOum_unit_srno(String oum_unit_srno) {
        this.oum_unit_srno = oum_unit_srno;
    }

    public String getTo_half() {
        return to_half;
    }

    public void setTo_half(String to_half) {
        this.to_half = to_half;
    }

    public String getTo_half_time() {
        return to_half_time;
    }

    public void setTo_half_time(String to_half_time) {
        this.to_half_time = to_half_time;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public BigDecimal getDly_srg_key() {
        return dly_srg_key;
    }

    public void setDly_srg_key(BigDecimal dly_srg_key) {
        this.dly_srg_key = dly_srg_key;
    }


    /** Creates a new instance of Bulk_Att_Display */
    public Bulk_Att_Display() {
    }

}
