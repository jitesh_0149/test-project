/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fes.pis.custom_entities;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author anu
 */

public class LeaveApplicationClub  implements Serializable{

    /** Creates a new instance of LeaveApplicationClub */
    public LeaveApplicationClub() {
    }


    String leave_period;
    String leavecd;
    String leave_desc;
    String parent_leavecd;
    String from_half;
    String to_half;
    Float total_days;
    Float pipeline_balance;
    Float cancel_pipeline_balance;
    Float leave_laps;
    Float leave_balance;
    Float minimum_taken;
    Float maximum_taken;
    Date from_date;
    Date to_date;
    Integer rowid;
    boolean isLast;
    String leavecd_flags;

    public Date getFrom_date() {
        return from_date;
    }

    public void setFrom_date(Date from_date) {
        this.from_date = from_date;
    }

    public String getLeave_desc() {
        return leave_desc;
    }

    public void setLeave_desc(String leave_desc) {
        this.leave_desc = leave_desc;
    }

    public String getLeavecd() {
        return leavecd;
    }

    public void setLeavecd(String leavecd) {
        this.leavecd = leavecd;
    }

    public String getFrom_half() {
        return from_half;
    }

    public void setFrom_half(String from_half) {
        this.from_half = from_half;
    }

    public String getLeave_period() {
        return leave_period;
    }

    public void setLeave_period(String leave_period) {
        this.leave_period = leave_period;
    }

    public String getParent_leavecd() {
        return parent_leavecd;
    }

    public void setParent_leavecd(String parent_leavecd) {
        this.parent_leavecd = parent_leavecd;
    }

    public Date getTo_date() {
        return to_date;
    }

    public void setTo_date(Date to_date) {
        this.to_date = to_date;
    }

    public String getTo_half() {
        return to_half;
    }

    public void setTo_half(String to_half) {
        this.to_half = to_half;
    }

    public Integer getRowid() {
        return rowid;
    }

    public void setRowid(Integer rowid) {
        this.rowid = rowid;
    }

    public boolean isIsLast() {
        return isLast;
    }

    public void setIsLast(boolean isLast) {
        this.isLast = isLast;
    }

    public Float getCancel_pipeline_balance() {
        return cancel_pipeline_balance;
    }

    public void setCancel_pipeline_balance(Float cancel_pipeline_balance) {
        this.cancel_pipeline_balance = cancel_pipeline_balance;
    }

    public Float getLeave_balance() {
        return leave_balance;
    }

    public void setLeave_balance(Float leave_balance) {
        this.leave_balance = leave_balance;
    }

    public Float getLeave_laps() {
        return leave_laps;
    }

    public void setLeave_laps(Float leave_laps) {
        this.leave_laps = leave_laps;
    }

    public Float getMaximum_taken() {
        return maximum_taken;
    }

    public void setMaximum_taken(Float maximum_taken) {
        this.maximum_taken = maximum_taken;
    }

    public Float getMinimum_taken() {
        return minimum_taken;
    }

    public void setMinimum_taken(Float minimum_taken) {
        this.minimum_taken = minimum_taken;
    }

    public Float getPipeline_balance() {
        return pipeline_balance;
    }

    public void setPipeline_balance(Float pipeline_balance) {
        this.pipeline_balance = pipeline_balance;
    }

    public Float getTotal_days() {
        return total_days;
    }

    public void setTotal_days(Float total_days) {
        this.total_days = total_days;
    }

    public String getLeavecd_flags() {
        return leavecd_flags;
    }

    public void setLeavecd_flags(String leavecd_flags) {
        this.leavecd_flags = leavecd_flags;
    }
    

}
