package org.fes.pis.custom_entities;

/**
 *
 * @author anu
 */
public class PIS_GlobalData extends GlobalData {

    boolean systemUserFHRD;
    boolean systemUserFTAS;
    final String userTypeForDaily = "'EMPLOYEE','CONSULTANT','ASSIGNMENT'";
    final String userTypeForContract = "'EMPLOYEE'";

    public boolean isSystemUserFHRD() {
        return systemUserFHRD;
    }

    public void setSystemUserFHRD(boolean systemUserFHRD) {
        this.systemUserFHRD = systemUserFHRD;
    }

    public boolean isSystemUserFTAS() {
        return systemUserFTAS;
    }

    public void setSystemUserFTAS(boolean systemUserFTAS) {
        this.systemUserFTAS = systemUserFTAS;
    }

    public String getUserTypeForContract() {
        return userTypeForContract;
    }

    public String getUserTypeForDaily() {
        return userTypeForDaily;
    }
}
