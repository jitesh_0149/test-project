/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fes.pis.custom_entities;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author anu
 */
public class ApprovalApplicationList implements Serializable{
    public int  userid;
    public Date fromdate;
    public Date todate;
    public float days;
    public String application_type;
    public String reason;
    public String user_name;
    public String rdb_approval="3";
    public String id;
    public String sub_srno;
    public String oum_unit_srno;
    public  String srno;
    public String leavecd;
    public String from_half;
    public String to_half;
    public String leave_laps;
    public String cancel_srno;
    public String cancel_subsrno;

    public String getSrno() {
        return srno;
    }

    public void setSrno(String srno) {
        this.srno = srno;
    }

    public String getOum_unit_srno() {
        return oum_unit_srno;
    }

    public void setOum_unit_srno(String oum_unit_srno) {
        this.oum_unit_srno = oum_unit_srno;
    }

    public String getSub_srno() {
        return sub_srno;
    }

    public void setSub_srno(String sub_srno) {
        this.sub_srno = sub_srno;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRdb_approval() {
        return rdb_approval;
    }

    public void setRdb_approval(String rdb_approval) {
        this.rdb_approval = rdb_approval;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getApplication_type() {
        return application_type;
    }

    public void setApplication_type(String application_type) {
        this.application_type = application_type;
    }

    public float getDays() {
        return days;
    }

    public void setDays(float days) {
        this.days = days;
    }



    public Date getFromdate() {
        return fromdate;
    }

    public void setFromdate(Date fromdate) {
        this.fromdate = fromdate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getTodate() {
        return todate;
    }

    public void setTodate(Date todate) {
        this.todate = todate;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getFrom_half() {
        return from_half;
    }

    public void setFrom_half(String from_half) {
        this.from_half = from_half;
    }

    public String getLeave_laps() {
        return leave_laps;
    }

    public void setLeave_laps(String leave_laps) {
        this.leave_laps = leave_laps;
    }

    public String getLeavecd() {
        return leavecd;
    }

    public void setLeavecd(String leavecd) {
        this.leavecd = leavecd;
    }

    public String getTo_half() {
        return to_half;
    }

    public void setTo_half(String to_half) {
        this.to_half = to_half;
    }

    public String getCancel_srno() {
        return cancel_srno;
    }

    public void setCancel_srno(String cancel_srno) {
        this.cancel_srno = cancel_srno;
    }

    public String getCancel_subsrno() {
        return cancel_subsrno;
    }

    public void setCancel_subsrno(String cancel_subsrno) {
        this.cancel_subsrno = cancel_subsrno;
    }


            //USER_ID,min(FROM_DATE) from_date,max(TO_DATE) to_date,sum(days) days,APPLI_TYPE,REASON1
    /** Creates a new instance of ApprovalApplicationList */
    public ApprovalApplicationList() {
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApprovalApplicationList other = (ApprovalApplicationList) obj;
        if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
    
    

}
