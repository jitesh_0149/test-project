package org.fes.pis.custom_entities;

import java.io.Serializable;
import java.util.Date;
import org.fes.lib.utilities.DateTimeUtility;

public class Finyear implements Serializable {

    Integer finyear;
    Date start_date;
    String start_date_string;
    String end_date_string;
    Date end_date;
    Integer start_year;
    Integer end_year;

    public Integer getFinyear() {
        return finyear;
    }

    public void setFinyear(Integer finyear) {
        this.finyear = finyear;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Integer getEnd_year() {
        return end_year;
    }

    public void setEnd_year(Integer end_year) {
        this.end_year = end_year;
    }

    public Integer getStart_year() {
        return start_year;
    }

    public void setStart_year(Integer start_year) {
        this.start_year = start_year;
    }

    public String getEnd_date_string() {
        return end_date_string;
    }

    public void setEnd_date_string(String end_date_string) {
        this.end_date_string = end_date_string;
    }

    public String getStart_date_string() {
        return start_date_string;
    }

    public void setStart_date_string(String start_date_string) {
        this.start_date_string = start_date_string;
    }

    public Finyear() {
    }

    public Finyear(Integer finyear, Date start_date, Date end_date, Integer start_year, Integer end_year) {
        
        this.finyear = finyear;
        this.start_date = start_date;
        this.end_date = end_date;
        this.start_date_string = DateTimeUtility.ChangeDateFormat(start_date, null);
        this.end_date_string = DateTimeUtility.ChangeDateFormat(end_date, null);
        this.start_year = start_year;
        this.end_year = end_year;

    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Finyear other = (Finyear) obj;
        if (this.finyear != other.finyear && (this.finyear == null || !this.finyear.equals(other.finyear))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + (this.finyear != null ? this.finyear.hashCode() : 0);
        return hash;
    }
}
