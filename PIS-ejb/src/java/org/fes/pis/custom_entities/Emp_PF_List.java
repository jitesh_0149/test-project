/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.custom_entities;

import java.math.BigDecimal;

/**
 *
 * @author jitesh
 */
public class Emp_PF_List {
    Integer rowid;
    Integer year;
    Integer month;
    Integer userID;
    Integer empno;
    String userName;
    BigDecimal pfActno;
    BigDecimal pfSalary;
    BigDecimal pfEmpPf;
    BigDecimal pfEmpFpf;
    BigDecimal pfEmprPf;
    BigDecimal pfEmprFpf;
    BigDecimal pfEmpVpf;
    String ntgcfPFNo;

    public Integer getEmpno() {
        return empno;
    }

    public void setEmpno(Integer empno) {
        this.empno = empno;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public BigDecimal getPfActno() {
        return pfActno;
    }

    public void setPfActno(BigDecimal pfActno) {
        this.pfActno = pfActno;
    }

    public BigDecimal getPfEmpFpf() {
        return pfEmpFpf;
    }

    public void setPfEmpFpf(BigDecimal pfEmpFpf) {
        this.pfEmpFpf = pfEmpFpf;
    }

    public BigDecimal getPfEmpPf() {
        return pfEmpPf;
    }

    public void setPfEmpPf(BigDecimal pfEmpPf) {
        this.pfEmpPf = pfEmpPf;
    }

    public BigDecimal getPfEmpVpf() {
        return pfEmpVpf;
    }

    public void setPfEmpVpf(BigDecimal pfEmpVpf) {
        this.pfEmpVpf = pfEmpVpf;
    }

    public BigDecimal getPfEmprFpf() {
        return pfEmprFpf;
    }

    public void setPfEmprFpf(BigDecimal pfEmprFpf) {
        this.pfEmprFpf = pfEmprFpf;
    }

    public BigDecimal getPfEmprPf() {
        return pfEmprPf;
    }

    public void setPfEmprPf(BigDecimal pfEmprPf) {
        this.pfEmprPf = pfEmprPf;
    }

    public BigDecimal getPfSalary() {
        return pfSalary;
    }

    public void setPfSalary(BigDecimal pfSalary) {
        this.pfSalary = pfSalary;
    }

    public Integer getRowid() {
        return rowid;
    }

    public void setRowid(Integer rowid) {
        this.rowid = rowid;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Emp_PF_List other = (Emp_PF_List) obj;
        if (this.rowid != other.rowid && (this.rowid == null || !this.rowid.equals(other.rowid))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + (this.rowid != null ? this.rowid.hashCode() : 0);
        return hash;
    }

    public String getNtgcfPFNo() {
        return ntgcfPFNo;
    }

    public void setNtgcfPFNo(String ntgcfPFNo) {
        this.ntgcfPFNo = ntgcfPFNo;
    }
    
    
    
}
