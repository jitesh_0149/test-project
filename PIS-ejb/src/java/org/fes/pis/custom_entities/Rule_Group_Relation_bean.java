package org.fes.pis.custom_entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


public class Rule_Group_Relation_bean implements Serializable{


    BigDecimal ragm_srg_key;
    BigDecimal parent_ragm;
    BigDecimal rmgm_srg_key;
    String rmgm_srno;
    String group_desc;
    String cat_desc;
    Date start_date;
    Date end_date;
    Integer level;


    public Rule_Group_Relation_bean() {
    }

    public String getCat_desc() {
        return cat_desc;
    }

    public String getRmgm_srno() {
        return rmgm_srno;
    }

    public void setRmgm_srno(String rmgm_srno) {
        this.rmgm_srno = rmgm_srno;
    }

    public void setCat_desc(String cat_desc) {
        this.cat_desc = cat_desc;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public String getGroup_desc() {
        return group_desc;
    }

    public void setGroup_desc(String group_desc) {
        this.group_desc = group_desc;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public BigDecimal getParent_ragm() {
        return parent_ragm;
    }

    public void setParent_ragm(BigDecimal parent_ragm) {
        this.parent_ragm = parent_ragm;
    }

    public BigDecimal getRagm_srg_key() {
        return ragm_srg_key;
    }

    public void setRagm_srg_key(BigDecimal ragm_srg_key) {
        this.ragm_srg_key = ragm_srg_key;
    }

    public BigDecimal getRmgm_srg_key() {
        return rmgm_srg_key;
    }

    public void setRmgm_srg_key(BigDecimal rmgm_srg_key) {
        this.rmgm_srg_key = rmgm_srg_key;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }
    


}
