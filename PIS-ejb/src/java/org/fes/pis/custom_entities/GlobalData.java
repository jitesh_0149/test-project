package org.fes.pis.custom_entities;

import java.io.Serializable;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.SystOrgUnitMst;

public class GlobalData implements Serializable {

    FhrdEmpmst ent_login_user;
    Integer user_id;
    SystOrgUnitMst ent_working_ou;
    SystOrgUnitMst ent_posting_ou;
    Integer working_ou;
    SystOrgUnitMst organisation;
    String organisationShortName;
    String organisationName;
    Finyear selectedFinyear;

    public FhrdEmpmst getEnt_login_user() {
        return ent_login_user;
    }

    public void setEnt_login_user(FhrdEmpmst ent_login_user) {
        this.ent_login_user = ent_login_user;
    }

    public SystOrgUnitMst getEnt_posting_ou() {
        return ent_posting_ou;
    }

    public void setEnt_posting_ou(SystOrgUnitMst ent_posting_ou) {
        this.ent_posting_ou = ent_posting_ou;
    }

    public SystOrgUnitMst getEnt_working_ou() {
        return ent_working_ou;
    }

    public void setEnt_working_ou(SystOrgUnitMst ent_working_ou) {
        this.ent_working_ou = ent_working_ou;
    }

    public SystOrgUnitMst getOrganisation() {
        return organisation;
    }

    public void setOrganisation(SystOrgUnitMst organisation) {
        this.organisation = organisation;
    }

    public String getOrganisationName() {
        return organisationName;
    }

    public void setOrganisationName(String organisationName) {
        this.organisationName = organisationName;
    }

    public String getOrganisationShortName() {
        return organisationShortName;
    }

    public void setOrganisationShortName(String organisationShortName) {
        this.organisationShortName = organisationShortName;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getWorking_ou() {
        return working_ou;
    }

    public void setWorking_ou(Integer working_ou) {
        this.working_ou = working_ou;
    }

    public Finyear getSelectedFinyear() {
        return selectedFinyear;
    }

    public void setSelectedFinyear(Finyear selectedFinyear) {
        this.selectedFinyear = selectedFinyear;
    }
}
