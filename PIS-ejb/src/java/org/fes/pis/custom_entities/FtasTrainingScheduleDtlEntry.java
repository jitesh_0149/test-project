/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.custom_entities;

import java.io.Serializable;
import org.fes.pis.entities.FtasTrainingScheduleDtl;

/**
 *
 * @author anu
 */
public class FtasTrainingScheduleDtlEntry implements Serializable {

    FtasTrainingScheduleDtl entFtasTrainingScheduleDtl;
    boolean lastRow;

    public FtasTrainingScheduleDtl getEntFtasTrainingScheduleDtl() {
        return entFtasTrainingScheduleDtl;
    }

    public void setEntFtasTrainingScheduleDtl(FtasTrainingScheduleDtl entFtasTrainingScheduleDtl) {
        this.entFtasTrainingScheduleDtl = entFtasTrainingScheduleDtl;
    }

    public boolean isLastRow() {
        return lastRow;
    }

    public void setLastRow(boolean lastRow) {
        this.lastRow = lastRow;
    }

    public FtasTrainingScheduleDtlEntry() {
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FtasTrainingScheduleDtlEntry other = (FtasTrainingScheduleDtlEntry) obj;
        if (this.entFtasTrainingScheduleDtl != other.entFtasTrainingScheduleDtl && (this.entFtasTrainingScheduleDtl == null || !this.entFtasTrainingScheduleDtl.equals(other.entFtasTrainingScheduleDtl))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.entFtasTrainingScheduleDtl != null ? this.entFtasTrainingScheduleDtl.hashCode() : 0);
        return hash;
    }

    
}
