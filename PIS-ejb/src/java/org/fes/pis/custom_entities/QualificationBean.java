/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.custom_entities;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author anu
 */
public class QualificationBean implements Serializable{

    public QualificationBean() {
    }
    
    
    String degree;
    String subject;
    String university;
    String college;
    String percentage;
    String grade;
    String remarks;
    String pass_month;
    String pass_year;
    Integer rowid; 
    String dgrm_srg_key;
    String dgrd_srg_key;
    String um_srg_key;
    BigDecimal qnSrgKey;
    boolean isEdit;

    public BigDecimal getQnSrgKey() {
        return qnSrgKey;
    }

    public void setQnSrgKey(BigDecimal qnSrgKey) {
        this.qnSrgKey = qnSrgKey;
    }
    
    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getPass_month() {
        return pass_month;
    }

    public void setPass_month(String pass_month) {
        this.pass_month = pass_month;
    }

    public String getPass_year() {
        return pass_year;
    }

    public void setPass_year(String pass_year) {
        this.pass_year = pass_year;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public Integer getRowid() {
        return rowid;
    }

    public void setRowid(Integer rowid) {
        this.rowid = rowid;
    }

    public String getDgrd_srg_key() {
        return dgrd_srg_key;
    }

    public void setDgrd_srg_key(String dgrd_srg_key) {
        this.dgrd_srg_key = dgrd_srg_key;
    }

    public String getDgrm_srg_key() {
        return dgrm_srg_key;
    }

    public void setDgrm_srg_key(String dgrm_srg_key) {
        this.dgrm_srg_key = dgrm_srg_key;
    }

    public String getUm_srg_key() {
        return um_srg_key;
    }

    public void setUm_srg_key(String um_srg_key) {
        this.um_srg_key = um_srg_key;
    }

    public boolean isIsEdit() {
        return isEdit;
    }

    public void setIsEdit(boolean isEdit) {
        this.isEdit = isEdit;
    }
    
}
