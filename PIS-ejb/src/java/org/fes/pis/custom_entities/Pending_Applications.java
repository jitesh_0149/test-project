/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fes.pis.custom_entities;

import java.io.Serializable;

/**
 *
 * @author anu
 */

public class Pending_Applications implements Serializable{

    /** Creates a new instance of Pending_Applications */
    public Pending_Applications() {
    }
    public String user_id;
    public String user_name;
    public String leavecd;
    public String from_date;
    public String to_date;
    public String aprv_by;

    public String getAprv_by() {
        return aprv_by;
    }

    public void setAprv_by(String aprv_by) {
        this.aprv_by = aprv_by;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getLeavecd() {
        return leavecd;
    }

    public void setLeavecd(String leavecd) {
        this.leavecd = leavecd;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

}
