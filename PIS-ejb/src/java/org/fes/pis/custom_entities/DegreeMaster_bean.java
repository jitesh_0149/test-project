/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fes.pis.custom_entities;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author anu
 */
public class DegreeMaster_bean implements Serializable{

    public DegreeMaster_bean(){}

    BigDecimal  dgrm_srg_key;
    BigDecimal  dgrd_srg_key;
    String degreecd;
    Integer degree_srno;
    String stream;
    String subject_name;
    Integer lev;
    Integer degree_level;
    Integer rowid;
    boolean isLast;
    
    public Integer getDegree_level() {
        return degree_level;
    }

    public void setDegree_level(Integer degree_level) {
        this.degree_level = degree_level;
    }

    public Integer getDegree_srno() {
        return degree_srno;
    }

    public void setDegree_srno(Integer degree_srno) {
        this.degree_srno = degree_srno;
    }

    public String getDegreecd() {
        return degreecd;
    }

    public void setDegreecd(String degreecd) {
        this.degreecd = degreecd;
    }

    public BigDecimal getDgrd_srg_key() {
        return dgrd_srg_key;
    }

    public void setDgrd_srg_key(BigDecimal dgrd_srg_key) {
        this.dgrd_srg_key = dgrd_srg_key;
    }

    public BigDecimal getDgrm_srg_key() {
        return dgrm_srg_key;
    }

    public void setDgrm_srg_key(BigDecimal dgrm_srg_key) {
        this.dgrm_srg_key = dgrm_srg_key;
    }

    public Integer getLev() {
        return lev;
    }

    public void setLev(Integer lev) {
        this.lev = lev;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public Integer getRowid() {
        return rowid;
    }

    public void setRowid(Integer rowid) {
        this.rowid = rowid;
    }

    public boolean isIsLast() {
        return isLast;
    }

    public void setIsLast(boolean isLast) {
        this.isLast = isLast;
    }


    
}
