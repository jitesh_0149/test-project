/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.custom_entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 * @author anu
 */
public class PaytransMonthlyVariableBean implements Serializable{

    public PaytransMonthlyVariableBean() {}
    
    Integer userId;
    BigDecimal ptmvSrgKey;
    BigDecimal eedSrg_key;
    String earndedncd;
    String earndednName;
    BigInteger variableAmt;
    Integer rowId;

    public String getEarndednName() {
        return earndednName;
    }

    public void setEarndednName(String earndednName) {
        this.earndednName = earndednName;
    }

    public String getEarndedncd() {
        return earndedncd;
    }

    public void setEarndedncd(String earndedncd) {
        this.earndedncd = earndedncd;
    }

    public BigDecimal getEedSrg_key() {
        return eedSrg_key;
    }

    public void setEedSrg_key(BigDecimal eedSrg_key) {
        this.eedSrg_key = eedSrg_key;
    }

    public BigDecimal getPtmvSrgKey() {
        return ptmvSrgKey;
    }

    public void setPtmvSrgKey(BigDecimal ptmvSrgKey) {
        this.ptmvSrgKey = ptmvSrgKey;
    }

    public Integer getRowId() {
        return rowId;
    }

    public void setRowId(Integer rowId) {
        this.rowId = rowId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public BigInteger getVariableAmt() {
        return variableAmt;
    }

    public void setVariableAmt(BigInteger variableAmt) {
        this.variableAmt = variableAmt;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PaytransMonthlyVariableBean other = (PaytransMonthlyVariableBean) obj;
        if (this.rowId != other.rowId && (this.rowId == null || !this.rowId.equals(other.rowId))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (this.rowId != null ? this.rowId.hashCode() : 0);
        return hash;
    }    
    
}
