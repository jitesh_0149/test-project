/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.custom_entities;

import java.io.Serializable;

/**
 *
 * @author anu
 */
public class TourTraining implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="Default methods">
    /**
     * Creates a new instance of TourTraining
     */
    public TourTraining() {
    }// </editor-fold>
    String place;
    String from_date;
    String to_date;
    String from_time;
    String to_time;
    int RowId;
    String purpose;
    String schedule_from_datetime;
    String schedule_to_datetime;
    String schedule_desc;
    boolean lastRow;

    public boolean isLastRow() {
        return lastRow;
    }

    public void setLastRow(boolean lastRow) {
        this.lastRow = lastRow;
    }

    public String getSchedule_desc() {
        return schedule_desc;
    }

    public void setSchedule_desc(String schedule_desc) {
        this.schedule_desc = schedule_desc;
    }

    public String getSchedule_from_datetime() {
        return schedule_from_datetime;
    }

    public void setSchedule_from_datetime(String schedule_from_datetime) {
        this.schedule_from_datetime = schedule_from_datetime;
    }

    public String getSchedule_to_datetime() {
        return schedule_to_datetime;
    }

    public void setSchedule_to_datetime(String schedule_to_datetime) {
        this.schedule_to_datetime = schedule_to_datetime;
    }

    public int getRowId() {
        return RowId;
    }

    public void setRowId(int RowId) {
        this.RowId = RowId;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getFrom_time() {
        return from_time;
    }

    public void setFrom_time(String from_time) {
        this.from_time = from_time;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getTo_time() {
        return to_time;
    }

    public void setTo_time(String to_time) {
        this.to_time = to_time;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }
}
