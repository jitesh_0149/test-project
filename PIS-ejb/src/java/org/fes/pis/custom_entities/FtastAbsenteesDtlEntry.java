package org.fes.pis.custom_entities;

import java.io.Serializable;
import java.math.BigDecimal;
import org.fes.pis.entities.FtasAbsenteesDtl;

public class FtastAbsenteesDtlEntry implements Serializable {

    FtasAbsenteesDtl ftasAbsenteesDtl;
    BigDecimal actualLeavesUsed;
    boolean lastRow;

    public BigDecimal getActualLeavesUsed() {
        return actualLeavesUsed;
    }

    public void setActualLeavesUsed(BigDecimal actualLeavesUsed) {
        this.actualLeavesUsed = actualLeavesUsed;
    }

    public FtasAbsenteesDtl getFtasAbsenteesDtl() {
        return ftasAbsenteesDtl;
    }

    public void setFtasAbsenteesDtl(FtasAbsenteesDtl ftasAbsenteesDtl) {
        this.ftasAbsenteesDtl = ftasAbsenteesDtl;
    }

    public boolean isLastRow() {
        return lastRow;
    }

    public void setLastRow(boolean lastRow) {
        this.lastRow = lastRow;
    }

    public FtastAbsenteesDtlEntry() {
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FtastAbsenteesDtlEntry other = (FtastAbsenteesDtlEntry) obj;
        if (this.ftasAbsenteesDtl != other.ftasAbsenteesDtl && (this.ftasAbsenteesDtl == null || !this.ftasAbsenteesDtl.equals(other.ftasAbsenteesDtl))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.ftasAbsenteesDtl != null ? this.ftasAbsenteesDtl.hashCode() : 0);
        return hash;
    }
}
