/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.view_entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_CONTRACT_GROUP_DATA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdContractGroupData.findAll", query = "SELECT f FROM FhrdContractGroupData f"),
    @NamedQuery(name = "FhrdContractGroupData.findBySrno", query = "SELECT f FROM FhrdContractGroupData f WHERE f.srno = :srno"),
    @NamedQuery(name = "FhrdContractGroupData.findByCmSrgKey", query = "SELECT f FROM FhrdContractGroupData f WHERE f.cmSrgKey = :cmSrgKey"),
    @NamedQuery(name = "FhrdContractGroupData.findByContractDesc", query = "SELECT f FROM FhrdContractGroupData f WHERE f.contractDesc = :contractDesc"),
    @NamedQuery(name = "FhrdContractGroupData.findByRagmSrgKey", query = "SELECT f FROM FhrdContractGroupData f WHERE f.ragmSrgKey = :ragmSrgKey"),
    @NamedQuery(name = "FhrdContractGroupData.findByApplGroupDesc", query = "SELECT f FROM FhrdContractGroupData f WHERE f.applGroupDesc = :applGroupDesc"),
    @NamedQuery(name = "FhrdContractGroupData.findByRmgmSrgKey", query = "SELECT f FROM FhrdContractGroupData f WHERE f.rmgmSrgKey = :rmgmSrgKey"),
    @NamedQuery(name = "FhrdContractGroupData.findByManagGroupDesc", query = "SELECT f FROM FhrdContractGroupData f WHERE f.managGroupDesc = :managGroupDesc"),
    @NamedQuery(name = "FhrdContractGroupData.findByCatDesc", query = "SELECT f FROM FhrdContractGroupData f WHERE f.catDesc = :catDesc"),
    @NamedQuery(name = "FhrdContractGroupData.findByStartDate", query = "SELECT f FROM FhrdContractGroupData f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdContractGroupData.findByEndDate", query = "SELECT f FROM FhrdContractGroupData f WHERE f.endDate = :endDate")})
public class FhrdContractGroupData implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    private static final long serialVersionUID = 1L;
    @Column(name = "SRNO")
    @Id
    private BigInteger srno;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "CM_SRG_KEY")
    private BigDecimal cmSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CONTRACT_DESC")
    private String contractDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RAGM_SRG_KEY")
    private BigDecimal ragmSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "APPL_GROUP_DESC")
    private String applGroupDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RMGM_SRG_KEY")
    private BigDecimal rmgmSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "MANAG_GROUP_DESC")
    private String managGroupDesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CAT_DESC")
    private String catDesc;

    public FhrdContractGroupData() {
    }

    public BigInteger getSrno() {
        return srno;
    }

    public void setSrno(BigInteger srno) {
        this.srno = srno;
    }

    public BigDecimal getCmSrgKey() {
        return cmSrgKey;
    }

    public void setCmSrgKey(BigDecimal cmSrgKey) {
        this.cmSrgKey = cmSrgKey;
    }

    public String getContractDesc() {
        return contractDesc;
    }

    public void setContractDesc(String contractDesc) {
        this.contractDesc = contractDesc;
    }

    public BigDecimal getRagmSrgKey() {
        return ragmSrgKey;
    }

    public void setRagmSrgKey(BigDecimal ragmSrgKey) {
        this.ragmSrgKey = ragmSrgKey;
    }

    public String getApplGroupDesc() {
        return applGroupDesc;
    }

    public void setApplGroupDesc(String applGroupDesc) {
        this.applGroupDesc = applGroupDesc;
    }

    public BigDecimal getRmgmSrgKey() {
        return rmgmSrgKey;
    }

    public void setRmgmSrgKey(BigDecimal rmgmSrgKey) {
        this.rmgmSrgKey = rmgmSrgKey;
    }

    public String getManagGroupDesc() {
        return managGroupDesc;
    }

    public void setManagGroupDesc(String managGroupDesc) {
        this.managGroupDesc = managGroupDesc;
    }

    public String getCatDesc() {
        return catDesc;
    }

    public void setCatDesc(String catDesc) {
        this.catDesc = catDesc;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

}
