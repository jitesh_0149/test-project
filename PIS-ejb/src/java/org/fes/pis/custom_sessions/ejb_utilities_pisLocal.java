/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.custom_sessions;

import java.math.BigDecimal;
import java.util.Date;
import javax.ejb.Local;

/**
 *
 * @author anu
 */
@Local
public interface ejb_utilities_pisLocal {

    public Date getLastLockedDate(Integer p_user_id, Integer p_oum_unit_srno);

    public String getPartialCancFromDate(BigDecimal p_abs_srg_key, Date p_canc_from_date);
}