/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.custom_sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author anu
 */
@Local
public interface ejb_utilitiesLocal {

    public String getSchemaName();

    public List<Integer> getOu_acord_table_level(String p_table_name, Integer p_oum_unit_srno);

//    public BigDecimal getUniqueSrNo(String p_table_name);
    public BigDecimal getUniqueSrNo(Integer p_finyear, String p_table_name);
}