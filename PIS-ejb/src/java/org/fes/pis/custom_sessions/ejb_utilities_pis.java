/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.custom_sessions;

import java.math.BigDecimal;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.lib.utilities.DateTimeUtility;

@Stateless
public class ejb_utilities_pis implements ejb_utilities_pisLocal {

    
    @PersistenceContext
    private EntityManager em;

    @Override
    public Date getLastLockedDate(Integer p_user_id, Integer p_oum_unit_srno) {
        return (Date) em.createNativeQuery("SELECT FTAS_PACK.GET_LAST_LOCKED_DATE(" + p_user_id + "," + p_oum_unit_srno + ") FROM DUAL").getSingleResult();
    }

    @Override
    public String getPartialCancFromDate(BigDecimal p_abs_srg_key, Date p_canc_from_date) {
        return (String) em.createNativeQuery("SELECT FTAS_PACK.PARTIAL_CANC_FROM_DATE(" + p_abs_srg_key + "," + (p_canc_from_date == null ? "NULL" : "'" + DateTimeUtility.ChangeDateFormat(p_canc_from_date, null) + "'") + ") FROM DUAL").getSingleResult();
    }
}
