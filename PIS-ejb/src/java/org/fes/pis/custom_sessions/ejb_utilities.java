/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.custom_sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class ejb_utilities implements ejb_utilitiesLocal {

    @PersistenceContext
    private EntityManager em;
    // <editor-fold defaultstate="collapsed" desc="getSchemaName">
    @Override
    public String getSchemaName() {
        return em.createNativeQuery("SELECT USER FROM DUAL").getSingleResult().toString();
    }
    // </editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="getUniqueSrNo">
    @Override
    public BigDecimal getUniqueSrNo(Integer p_transactionYear, String p_table_name) {
//        System.out.println("select ou_pack.get_sequence_id("+p_finyear+",'" + p_table_name + "') from dual");
        return ((BigDecimal) em.createNativeQuery("select fhrd_pack.get_sequence_id(" + p_transactionYear + ",'" + p_table_name + "') from dual").getSingleResult());
    }
    // </editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="getUniqueSrNo">

//    @Override
//    public BigDecimal getUniqueSrNo(String p_table_name) {
//        return ((BigDecimal) em.createNativeQuery("select fhrd_pack.get_sequence_id('" + p_table_name + "') from dual").getSingleResult());
//    }
    // </editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="getOu_acord_table_level">
    @Override
    public List<Integer> getOu_acord_table_level(String p_table_name, Integer p_oum_unit_srno) {
        return em.createNativeQuery("select OU_PACK.GET_oum_unit_srno('" + p_table_name + "'," + p_oum_unit_srno + ") \"ou_list\" from dual").getResultList();
    }// </editor-fold>
    //    // <editor-fold defaultstate="collapsed" desc="getSubOuList">
//
//    public List<Integer> getSubOuList(Integer p_oum_unit_srno) {
//        List<Vector> v_temp_vector_list = null;
//        v_temp_vector_list = (List<Vector>) em.createNativeQuery("select COLUMN_VALUE \"LST_ORG_UNIT\" from table(ou_pack.GET_SUB_OU(" + p_oum_unit_srno + ",'b','y'))").getResultList();
//        List<Integer> lst_tem_int = new ArrayList<Integer>();
//        int n = v_temp_vector_list.size();
//        for (int i = 0; i < n; i++) {
//            lst_tem_int.add(new Integer(v_temp_vector_list.get(i).get(0).toString()));
//        }
//        return lst_tem_int;
//    }// </editor-fold>
}
