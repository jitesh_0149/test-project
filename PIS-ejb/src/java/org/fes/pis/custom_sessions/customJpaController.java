package org.fes.pis.custom_sessions;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import javax.validation.ConstraintViolationException;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.Database_Output;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.exceptions.PreexistingEntityException;
import org.fes.pis.custom_sessions.exceptions.RollbackFailureException;
import org.fes.pis.entities.*;
import org.fes.pis.jsf.utilities.JSFMessages;
//import org.fes.pis.jsf.utilities.JSFMessages;

/**
 *
 * @author anu
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class customJpaController {

    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    // <editor-fold defaultstate="collapsed" desc="Default/EJB Declaration">
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;
    @Resource
    private EJBContext context;

    public EntityManager getEntityManager() {
        return em;
    }

    public customJpaController() {
    }

// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Contract Master">     
    public void createFhrdContractMst(FhrdContractMst fhrdContractMst) {
        UserTransaction utx = context.getUserTransaction();
        try {

            utx.begin();
            em = getEntityManager();
            try {
                getEntityManager().persist(fhrdContractMst);
            } catch (ConstraintViolationException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getConstraintViolations().toString(), e);
                throw e;
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                try {
                    throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                } catch (RollbackFailureException ex1) {
                    Logger.getLogger(customJpaController.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
            if (em.find(FhrdContractMst.class, fhrdContractMst.getCmSrgKey()) != null) {
                try {
                    throw new PreexistingEntityException("FhrdContractMst " + fhrdContractMst + " already exists.", ex);
                } catch (PreexistingEntityException ex1) {
                    Logger.getLogger(customJpaController.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
//            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="FhrdRuleApplGroupMst">    

    public void createFhrdRuleApplGroupMst(FhrdRuleApplGroupMst fhrdRuleApplGroupMst) throws PreexistingEntityException, RollbackFailureException, Exception {
        UserTransaction utx = context.getUserTransaction();
        try {

            utx.begin();
            em = getEntityManager();
//            em.persist(fhrdRuleApplGroupMst);
            try {
                getEntityManager().persist(fhrdRuleApplGroupMst);
            } catch (ConstraintViolationException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getConstraintViolations().toString(), e);
                throw e;
            }
            em.flush();
            em.clear();
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                try {
                    throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                } catch (RollbackFailureException ex1) {
                    Logger.getLogger(customJpaController.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
            if (em.find(FhrdRuleApplGroupMst.class, fhrdRuleApplGroupMst.getRagmSrgKey()) != null) {
                try {
                    throw new PreexistingEntityException("FhrdRuleApplGroupMst " + fhrdRuleApplGroupMst + " already exists.", ex);
                } catch (PreexistingEntityException ex1) {
                    Logger.getLogger(customJpaController.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="FtasAbsentees">

    public boolean createFtasAbsentees(FtasAbsentees entFtasAbsentees, List<FtasAbsenteesDtl> lstFtasAbsenteesDtls) {
        boolean isTransactionComplete = false;
        UserTransaction utx = context.getUserTransaction();
        try {
            utx.begin();
            em = getEntityManager();
            try {
                em.persist(entFtasAbsentees);
                em.flush();
                em.clear();
                FtasAbsenteesDtl entAbsenteesDtl;
                for (int i = 0; i < lstFtasAbsenteesDtls.size(); i++) {
                    entAbsenteesDtl = lstFtasAbsenteesDtls.get(i);
                    entAbsenteesDtl.setAbsSrgKey(entFtasAbsentees);
                    em.persist(entAbsenteesDtl);
                    em.flush();
                    em.clear();
                }
            } catch (ConstraintViolationException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getConstraintViolations().toString(), e);
                throw e;
            } catch (EJBException ex) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createFtasAbsentees", null, ex);
            }
            utx.commit();
            isTransactionComplete = true;
        } catch (Exception ex) {
            try {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createFtasAbsentees", null, ex);
                utx.setRollbackOnly();
            } catch (Exception re) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createFtasAbsentees", null, re);
            }

        } finally {
            //entity manager should be closed
        }
        return isTransactionComplete;
    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cancelLeaveApplication"> 

    public boolean cancelLeaveApplication(FtasCancelabs entCancelabs, FtasAbsentees entFtasAbsentees) {
        boolean isTransactionComplete = false;
        UserTransaction utx = context.getUserTransaction();
        try {
            utx.begin();
            em = getEntityManager();
            try {
                entCancelabs.setAbsSrgKey(entFtasAbsentees);
                em.persist(entCancelabs);
                em.flush();
                em.clear();
                entFtasAbsentees.setCabsSrgKey(entCancelabs);
                entFtasAbsentees.setCabsAprvcomm(null);
//                entFtasAbsentees.setCabsAprvcomm("N"); 
                entFtasAbsentees.setUpdateby(entCancelabs.getCreateby());
                entFtasAbsentees.setUpdatedt(new Date());
                em.merge(entFtasAbsentees);
                em.flush();
                em.clear();

            } catch (ConstraintViolationException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getConstraintViolations().toString(), e);
                throw e;
            } catch (EJBException ex) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createFtasAbsentees", null, ex);
            }
            utx.commit();
            isTransactionComplete = true;
        } catch (Exception ex) {
            try {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createFtasAbsentees", null, ex);
                utx.setRollbackOnly();
            } catch (Exception re) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createFtasAbsentees", null, re);
            }

        } finally {
            //entity manager should be closed
        }
        return isTransactionComplete;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="FtasAbsenteesTour">

    public void createFtasAbsenteesTour(FtasAbsentees ent_ftasAbsentees) {
        UserTransaction utx = context.getUserTransaction();
        try {
            utx.begin();
            em = getEntityManager();
            em.persist(ent_ftasAbsentees);
            em.flush();
            em.clear();
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                try {
                    throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                } catch (RollbackFailureException ex1) {
                    Logger.getLogger(customJpaController.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }

    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="FtasDailyMissingAttendance">

    public void createFtasDailyMissingAttendance(List<FtasDaily> lst_ftasDaily, Integer p_insert_count) {
        UserTransaction utx = context.getUserTransaction();
        try {

            utx.begin();
            em = getEntityManager();
            FtasDaily ent_ftasDaily;
            for (int i = 0; i < lst_ftasDaily.size(); i++) {
                ent_ftasDaily = lst_ftasDaily.get(i);
                if (i < p_insert_count) {
                    em.persist(ent_ftasDaily);
                    em.flush();
                } else {
                    em.merge(ent_ftasDaily);
                    em.flush();
                }
                em.clear();
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                try {
                    throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                } catch (RollbackFailureException ex1) {
                    Logger.getLogger(customJpaController.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }

    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="createFtasTrainingMst">     

    public boolean createFtasTrainingMst(FtasAbsentees ftasTrainingMst, List<FtasTrainingScheduleDtl> lstFtasTrainingScheduleDtls, List<FtasTrainingOrgUnitDtl> lstFtasOrgDtl) {
        boolean isTransactionComplete = false;
        UserTransaction utx = context.getUserTransaction();
        try {
            try {
                utx.begin();
                em.persist(ftasTrainingMst);
                em.flush();
                em.clear();
                FtasTrainingScheduleDtl entScheduleDtl;
                for (int i = 0; i < lstFtasTrainingScheduleDtls.size(); i++) {
                    entScheduleDtl = lstFtasTrainingScheduleDtls.get(i);
                    entScheduleDtl.setAbsSrgKey(ftasTrainingMst);
                    em.persist(entScheduleDtl);
                    em.flush();
                    em.clear();
                }
                FtasTrainingOrgUnitDtl entOrgUnitDtl;
                for (int i = 0; i < lstFtasOrgDtl.size(); i++) {
                    entOrgUnitDtl = lstFtasOrgDtl.get(i);
                    entOrgUnitDtl.setTrannSrgKey(new FtasTrainingMst());
                    em.persist(entOrgUnitDtl);
                    em.flush();
                    em.clear();
                }
            } catch (ConstraintViolationException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getConstraintViolations().toString(), e);
                throw e;
            } catch (EJBException ex) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createFtasTrainingMst", null, ex);
            }
            utx.commit();
            isTransactionComplete = true;
        } catch (Exception ex) {
            try {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createFtasTrainingMst", null, ex);
                utx.setRollbackOnly();
            } catch (Exception re) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createFtasTrainingMst", null, re);
            }

        } finally {
            //entity manager should be closed
        }
        return isTransactionComplete;

//        try {
//            UserTransaction utx = context.getUserTransaction();utx.begin();
//            em = getEntityManager();
//            em.persist(ftasTrainingMst);
////            em.flush();
////            em.clear();
////            em.persist(ftasAbsentees);
////            em.flush();
////            em.clear();
////            FtasTrainingScheduleDtl entFtasTrainingScheduleDtl=new FtasTrainingScheduleDtl();
////            for (int i = 0; i < lstFtasTrainingScheduleDtls.size(); i++) {
////                entFtasTrainingScheduleDtl = lstFtasTrainingScheduleDtls.get(i);
////                em.persist(entFtasTrainingScheduleDtl);
////                em.flush();
////                em.clear();
////            }
//            utx.commit();
//        } catch (Exception ex) {
//            try {
//                utx.rollback();
//            } catch (Exception re) {
//                try {
//                    throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
//                } catch (RollbackFailureException ex1) {
//                    Logger.getLogger(customJpaController.class.getName()).log(Level.SEVERE, null, ex1);
//                }
//            }
//            if (em.find(FtasTrainingMst.class, ftasTrainingMst.getTrannSrgKey()) != null) {
//                try {
//                    throw new PreexistingEntityException("FtasTrainingMst " + ftasTrainingMst + " already exists.", ex);
//                } catch (PreexistingEntityException ex1) {
//                    Logger.getLogger(customJpaController.class.getName()).log(Level.SEVERE, null, ex1);
//                }
//            }
//            try {
//                throw ex;
//            } catch (Exception ex1) {
//                Logger.getLogger(customJpaController.class.getName()).log(Level.SEVERE, null, ex1);
//            }
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }

    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="updateFtasAbsentees for Leave Application Approval">

    public void updateFtasAbsentees(List<FtasAbsentees> lst_FtasAbsenteeses) {
        UserTransaction utx = context.getUserTransaction();
        try {
            utx.begin();
            em = getEntityManager();
            FtasAbsentees ent_FtasAbsentees;
            for (int i = 0; i < lst_FtasAbsenteeses.size(); i++) {
//                System.out.println(i + " " + lst_FtasAbsenteeses.get(i));
                ent_FtasAbsentees = lst_FtasAbsenteeses.get(i);
                em.merge(ent_FtasAbsentees);
                em.flush();
                em.clear();
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                try {
                    throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                } catch (RollbackFailureException ex1) {
                    Logger.getLogger(customJpaController.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }

    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="createFhrdDegreeMst"> 

    public boolean createFhrdDegreeMst(FhrdDegreemst entFhrdDegreemst, List<FhrdDegreedtl> lstFhrdDegreedtls) {
        boolean isTransactionComplete = false;
        UserTransaction utx = context.getUserTransaction();
        try {
            utx.begin();
            em.persist(entFhrdDegreemst);
            int n = lstFhrdDegreedtls.size();
            for (int i = 0; i < n; i++) {
                em.persist(lstFhrdDegreedtls.get(i));
            }
            em.flush();
            em.clear();
            utx.commit();
            isTransactionComplete = true;
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                try {
                    throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                } catch (RollbackFailureException ex1) {
                    Logger.getLogger(customJpaController.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        } finally {
        }
        return isTransactionComplete;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ApproveRejectAppls"> 

    public boolean ApproveRejectAppls(FtasAbsentees[] selectedAppls, String p_approveComment, Integer p_approveBy) {
        boolean isTransactionComplete = false;
        UserTransaction utx = context.getUserTransaction();
        try {
            utx.begin();
            int n = selectedAppls.length;
            for (int i = 0; i < n; i++) {
                FtasAbsentees ent_Absentees = em.find(FtasAbsentees.class, selectedAppls[i].getAbsSrgKey());
                if (selectedAppls[i].getCabsSrgKey() == null) {
                    ent_Absentees.setUpdateby(new FhrdEmpmst(p_approveBy));
                    ent_Absentees.setArcomment(selectedAppls[i].getArcomment().toUpperCase());
                    ent_Absentees.setAprvby(new FhrdEmpmst(p_approveBy));
                    ent_Absentees.setAprvDate(new Date());
                    ent_Absentees.setAprvcomm(p_approveComment);
                    em.merge(ent_Absentees);
                } else {
                    FtasCancelabs ent_Cancelabs = em.find(FtasCancelabs.class, selectedAppls[i].getCabsSrgKey().getCabsSrgKey());
                    ent_Cancelabs.setUpdateby(new FhrdEmpmst(p_approveBy));
                    ent_Cancelabs.setArcomment(selectedAppls[i].getCabsSrgKey().getArcomment().toUpperCase());
                    ent_Cancelabs.setAprvby(new FhrdEmpmst(p_approveBy));
                    ent_Cancelabs.setAprvDate(new Date());
                    ent_Cancelabs.setAprvcomm(p_approveComment);
                    em.merge(ent_Cancelabs);
                }
            }
            utx.commit();
            isTransactionComplete = true;
        } catch (Exception ex) {
            try {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "ApproveRejectAppls", null, ex);
                utx.setRollbackOnly();
            } catch (Exception re) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "ApproveRejectAppls", null, re);
            }

        } finally {
            //entity manager should be closed
        }
        return isTransactionComplete;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ApproveRejectCancelAppls"> 

    public boolean ApproveRejectCancelAppls(FtasCancelabs[] selectedAppls, String p_approveComment, Integer p_approveBy) {
        boolean isTransactionComplete = false;
        UserTransaction utx = context.getUserTransaction();
        try {
            utx.begin();
            int n = selectedAppls.length;
            for (int i = 0; i < n; i++) {
                FtasCancelabs ent_Cancelabs = em.find(FtasCancelabs.class, selectedAppls[i].getCabsSrgKey());
                ent_Cancelabs.setUpdateby(new FhrdEmpmst(p_approveBy));
                ent_Cancelabs.setAprvby(new FhrdEmpmst(p_approveBy));
                ent_Cancelabs.setAprvDate(new Date());
                ent_Cancelabs.setAprvcomm(p_approveComment);
                em.merge(ent_Cancelabs);
            }
            utx.commit();
            isTransactionComplete = true;
        } catch (Exception ex) {
            try {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "ApproveRejectCancelAppls", null, ex);
                utx.setRollbackOnly();
            } catch (Exception re) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "ApproveRejectCancelAppls", null, re);
            }

        } finally {
            //entity manager should be closed
        }
        return isTransactionComplete;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="createNewEmployee">

    public boolean createNewEmployee(FhrdEmpmst p_empMst, FcadAddrDtl p_addrDtl, boolean p_createmode, Integer p_proccesBy, Integer p_oum_unit_srno) {
        boolean isTransactionComplete = false;
        UserTransaction utx = context.getUserTransaction();
        try {
            utx.begin();
            Integer v_system_user_id = new Integer(em.createNativeQuery("select OU_PACK.GET_SYSUSER_ID('S') from dual").getSingleResult().toString());
            FcadAddrMst ent_AddrMst = p_addrDtl.getFcadAddrMst();
            BigDecimal v_am_unique_srno = ent_AddrMst.getAmUniqueSrno();
            boolean createFcadAddrMst = false;
            if (ent_AddrMst.getAmUniqueSrno().equals(new BigDecimal("-1"))) {
                v_am_unique_srno = ((BigDecimal) em.createNativeQuery("select fhrd_pack.get_sequence_id(null,'FCAD_ADDR_MST') from dual").getSingleResult());
                createFcadAddrMst = true;
            }
            if (createFcadAddrMst) {
                List<FcadTeleFax> lst_TeleFaxs = (List<FcadTeleFax>) ent_AddrMst.getFcadTeleFaxCollection();
                ent_AddrMst.setFcadTeleFaxCollection(null);
                ent_AddrMst.setAmUniqueSrno(v_am_unique_srno);
                ent_AddrMst.setOumUnitSrno(new SystOrgUnitMst(p_oum_unit_srno));
                ent_AddrMst.setSystemUserid(v_system_user_id);
                em.persist(ent_AddrMst);
                ent_AddrMst = em.find(FcadAddrMst.class, ent_AddrMst.getAmUniqueSrno());
                for (FcadTeleFax t : lst_TeleFaxs) {
                    t.setAmUniqueSrno(ent_AddrMst);
                    em.persist(t);
                }
                p_addrDtl.getFcadAddrDtlPK().setAdAmUniqueSrno(new BigInteger(v_am_unique_srno.toString()));
                p_addrDtl.setSystemUserid(v_system_user_id);
                em.persist(p_addrDtl);

                Long v_agm_unique_srno = Long.valueOf(((BigDecimal) em.createNativeQuery("SELECT AGM_UNIQUE_SRNO FROM FCAD_ADDR_GRP_MST WHERE AGM_GRP_CD='EMP'").getSingleResult()).toString());
                FcadAddrGrpDtl ent_AddrGrpDtl = new FcadAddrGrpDtl();
                ent_AddrGrpDtl.setAgdUniqueSrno(new Long("-1"));
                ent_AddrGrpDtl.setRecordDeleteFlag("N");
                ent_AddrGrpDtl.setAgdAmUniqueSrno(em.find(FcadAddrMst.class, v_am_unique_srno));
                ent_AddrGrpDtl.setAgdAgmUniqueSrno(em.find(FcadAddrGrpMst.class, v_agm_unique_srno));
                ent_AddrGrpDtl.setOumUnitSrno(new SystOrgUnitMst(p_oum_unit_srno));
                ent_AddrGrpDtl.setCreateby(new FhrdEmpmst(p_proccesBy));
                ent_AddrGrpDtl.setCreatedt(new Date());
                ent_AddrGrpDtl.setSystemUserid(v_system_user_id);
                em.persist(ent_AddrGrpDtl);
                em.flush();
                em.clear();
            } else {
                List<FcadTeleFax> lst_gen_tele_fax = (List<FcadTeleFax>) ent_AddrMst.getFcadTeleFaxCollection();
                ent_AddrMst.setFcadTeleFaxCollection(null);
                System.out.println("eeeeeeeeee "+ent_AddrMst);

                //<editor-fold defaultstate="collapsed" desc="Address Detail Updation">
                FcadAddrDtl prev_addr_dtl = (FcadAddrDtl) em.createNativeQuery(
                        " SELECT * FROM FCAD_ADDR_DTL"
                        + " WHERE AD_AM_UNIQUE_SRNO = " + v_am_unique_srno
                        + " AND AD_END_DATE IS NULL OR AD_END_DATE >= SYSDATE", FcadAddrDtl.class).getSingleResult();

                //Check whether address changed?

                boolean updateAddress = false;

                if (!updateAddress
                        && ((prev_addr_dtl.getAdAddr1() != null && !prev_addr_dtl.getAdAddr1().equals(p_addrDtl.getAdAddr1()))
                        || (prev_addr_dtl.getAdAddr1() == null && !p_addrDtl.getAdAddr1().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (!updateAddress
                        && ((prev_addr_dtl.getAdAddr2() != null && !prev_addr_dtl.getAdAddr2().equals(p_addrDtl.getAdAddr2()))
                        || (prev_addr_dtl.getAdAddr2() == null && !p_addrDtl.getAdAddr2().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (!updateAddress
                        && ((prev_addr_dtl.getAdAddr3() != null && !prev_addr_dtl.getAdAddr3().equals(p_addrDtl.getAdAddr3()))
                        || (prev_addr_dtl.getAdAddr3() == null && !p_addrDtl.getAdAddr3().trim().isEmpty()))) {
                    updateAddress = true;
                }

                if (!updateAddress
                        && ((prev_addr_dtl.getAdCountry() != null && !prev_addr_dtl.getAdCountry().equals(p_addrDtl.getAdCountry()))
                        || (prev_addr_dtl.getAdCountry() == null && !p_addrDtl.getAdCountry().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (!updateAddress
                        && ((prev_addr_dtl.getAdState() != null && !prev_addr_dtl.getAdState().equals(p_addrDtl.getAdState()))
                        || (prev_addr_dtl.getAdState() == null && !p_addrDtl.getAdState().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (!updateAddress
                        && ((prev_addr_dtl.getAdCity() != null && !prev_addr_dtl.getAdCity().equals(p_addrDtl.getAdCity()))
                        || (prev_addr_dtl.getAdCity() == null && !p_addrDtl.getAdCity().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (!updateAddress
                        && ((prev_addr_dtl.getAdPincode() != null && !prev_addr_dtl.getAdPincode().equals(p_addrDtl.getAdPincode()))
                        || (prev_addr_dtl.getAdPincode() == null && !p_addrDtl.getAdPincode().trim().isEmpty()))) {
                    updateAddress = true;
                }

                if (!updateAddress
                        && ((prev_addr_dtl.getAdEmail() != null && !prev_addr_dtl.getAdEmail().equals(p_addrDtl.getAdEmail()))
                        || (prev_addr_dtl.getAdEmail() == null && !p_addrDtl.getAdEmail().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (!updateAddress
                        && ((prev_addr_dtl.getAdEmailAlt() != null && !prev_addr_dtl.getAdEmailAlt().equals(p_addrDtl.getAdEmailAlt()))
                        || (prev_addr_dtl.getAdEmailAlt() == null && !p_addrDtl.getAdEmailAlt().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (!updateAddress
                        && ((prev_addr_dtl.getAdWebsite() != null && !prev_addr_dtl.getAdWebsite().equals(p_addrDtl.getAdWebsite()))
                        || (prev_addr_dtl.getAdWebsite() == null && !p_addrDtl.getAdWebsite().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (!updateAddress
                        && ((prev_addr_dtl.getAdPresAddr1() != null && !prev_addr_dtl.getAdPresAddr1().equals(p_addrDtl.getAdPresAddr1()))
                        || (prev_addr_dtl.getAdPresAddr1() == null && !p_addrDtl.getAdPresAddr1().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (!updateAddress
                        && ((prev_addr_dtl.getAdPresAddr2() != null && !prev_addr_dtl.getAdPresAddr2().equals(p_addrDtl.getAdPresAddr2()))
                        || (prev_addr_dtl.getAdPresAddr2() == null && !p_addrDtl.getAdPresAddr2().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (!updateAddress
                        && ((prev_addr_dtl.getAdPresAddr3() != null && !prev_addr_dtl.getAdPresAddr3().equals(p_addrDtl.getAdPresAddr3()))
                        || (prev_addr_dtl.getAdPresAddr3() == null && !p_addrDtl.getAdPresAddr3().trim().isEmpty()))) {
                    updateAddress = true;
                }

                if (!updateAddress
                        && ((prev_addr_dtl.getAdPresCountry() != null && !prev_addr_dtl.getAdPresCountry().equals(p_addrDtl.getAdPresCountry()))
                        || (prev_addr_dtl.getAdPresCountry() == null && !p_addrDtl.getAdPresCountry().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (!updateAddress
                        && ((prev_addr_dtl.getAdPresState() != null && !prev_addr_dtl.getAdPresState().equals(p_addrDtl.getAdPresState()))
                        || (prev_addr_dtl.getAdPresState() == null && !p_addrDtl.getAdPresState().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (!updateAddress
                        && ((prev_addr_dtl.getAdPresCity() != null && !prev_addr_dtl.getAdPresCity().equals(p_addrDtl.getAdPresCity()))
                        || (prev_addr_dtl.getAdPresCity() == null && !p_addrDtl.getAdPresCity().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (!updateAddress
                        && ((prev_addr_dtl.getAdPresPincode() != null && !prev_addr_dtl.getAdPresPincode().equals(p_addrDtl.getAdPresPincode()))
                        || (prev_addr_dtl.getAdPresPincode() == null && !p_addrDtl.getAdPresPincode().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (!updateAddress
                        && ((prev_addr_dtl.getAdPermAddr1() != null && !prev_addr_dtl.getAdPermAddr1().equals(p_addrDtl.getAdPermAddr1()))
                        || (prev_addr_dtl.getAdPermAddr1() == null && !p_addrDtl.getAdPermAddr1().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (!updateAddress
                        && ((prev_addr_dtl.getAdPermAddr2() != null && !prev_addr_dtl.getAdPermAddr2().equals(p_addrDtl.getAdPermAddr2()))
                        || (prev_addr_dtl.getAdPermAddr2() == null && !p_addrDtl.getAdPermAddr2().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (!updateAddress
                        && ((prev_addr_dtl.getAdPermAddr3() != null && !prev_addr_dtl.getAdPermAddr3().equals(p_addrDtl.getAdPermAddr3()))
                        || (prev_addr_dtl.getAdPermAddr3() == null && !p_addrDtl.getAdPermAddr3().trim().isEmpty()))) {
                    updateAddress = true;
                }

                if (!updateAddress
                        && ((prev_addr_dtl.getAdPermCountry() != null && !prev_addr_dtl.getAdPermCountry().equals(p_addrDtl.getAdPermCountry()))
                        || (prev_addr_dtl.getAdPermCountry() == null && !p_addrDtl.getAdPermCountry().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (!updateAddress
                        && ((prev_addr_dtl.getAdPermState() != null && !prev_addr_dtl.getAdPermState().equals(p_addrDtl.getAdPermState()))
                        || (prev_addr_dtl.getAdPermState() == null && !p_addrDtl.getAdPermState().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (!updateAddress
                        && ((prev_addr_dtl.getAdPermCity() != null && !prev_addr_dtl.getAdPermCity().equals(p_addrDtl.getAdPermCity()))
                        || (prev_addr_dtl.getAdPermCity() == null && !p_addrDtl.getAdPermCity().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (!updateAddress
                        && ((prev_addr_dtl.getAdPermPincode() != null && !prev_addr_dtl.getAdPermPincode().equals(p_addrDtl.getAdPermPincode()))
                        || (prev_addr_dtl.getAdPermPincode() == null && !p_addrDtl.getAdPermPincode().trim().isEmpty()))) {
                    updateAddress = true;
                }
                if (updateAddress) {
                    prev_addr_dtl.setAdEndDate(p_addrDtl.getAdStartDate());
                    prev_addr_dtl.setUpdateby(new FhrdEmpmst(p_proccesBy));
                    em.merge(prev_addr_dtl);

                    p_addrDtl.setFcadAddrDtlPK(new FcadAddrDtlPK(new BigInteger(ent_AddrMst.getAmUniqueSrno().toString()), -1));
                    p_addrDtl.setFcadAddrMst(ent_AddrMst);
                    p_addrDtl.setUpdateby(new FhrdEmpmst(p_proccesBy));
                    em.persist(p_addrDtl);
                    em.flush();
                    em.clear();
                }
                //</editor-fold>
                //<editor-fold defaultstate="collapsed" desc="General Telephone/Fax Settings">
                List<FcadTeleFax> lst_gen_prev_tele_fax = em.createNativeQuery("SELECT * "
                        + " FROM FCAD_TELE_FAX"
                        + " WHERE AM_UNIQUE_SRNO=" + ent_AddrMst.getAmUniqueSrno()
                        + " AND PD_UNIQUE_SRNO IS NULL", FcadTeleFax.class).getResultList();
                System.out.println("lst_gen_prev_tele_fax " + lst_gen_prev_tele_fax.size());
                for (FcadTeleFax f : lst_gen_prev_tele_fax) {
                    if (!lst_gen_tele_fax.contains(f)) {
                        em.remove(em.find(FcadTeleFax.class, f.getTelFaxUniqueSrno()));
                    }
                }
                for (FcadTeleFax f : lst_gen_tele_fax) {
                    System.out.println("f.getTelFaxUniqueSrno() " + f.getTelFaxUniqueSrno());
                    if (f.getTelFaxUniqueSrno() < 0) {
                        em.persist(f);
                    }
                }
                //</editor-fold>
            }
            if (p_createmode) {
                p_empMst.setAddressno(new FcadAddrMst(v_am_unique_srno));
                em.persist(p_empMst);
                em.flush();
                em.clear();
                FhrdEmpmst v_empMst = em.find(FhrdEmpmst.class, p_empMst.getUserId());
                List<FhrdQualification> lstFhrdQualifications = (List<FhrdQualification>) p_empMst.getFhrdQualificationCollection();
                for (int i = 0; i < lstFhrdQualifications.size(); i++) {
                    if (lstFhrdQualifications.get(i).getFhrdQualificationPK().getQualSrno() < 0) {
                        lstFhrdQualifications.get(i).setFhrdEmpmst(v_empMst);
                        em.persist(lstFhrdQualifications.get(i));
                    }
                }
                List<FhrdExperience> lstFhrdExperiences = (List<FhrdExperience>) p_empMst.getFhrdExperienceCollection1();
                for (int i = 0; i < lstFhrdExperiences.size(); i++) {
                    if (lstFhrdExperiences.get(i).getFhrdExperiencePK().getExpSrno() < 0) {
                        lstFhrdExperiences.get(i).setFhrdEmpmst(v_empMst);
                        em.persist(lstFhrdExperiences.get(i));
                    }
                }
                List<FhrdDependent> lstFhrdDependents = (List<FhrdDependent>) p_empMst.getFhrdDependentCollection();
                for (int i = 0; i < lstFhrdDependents.size(); i++) {
                    if (lstFhrdDependents.get(i).getFhrdDependentPK().getDepSrno() < 0) {
                        lstFhrdDependents.get(i).setFhrdEmpmst(v_empMst);
                        em.persist(lstFhrdDependents.get(i));
                    }
                }

            } else {
                p_empMst.setAddressno(new FcadAddrMst(v_am_unique_srno));
                em.flush();
                em.clear();
                List<FhrdQualification> lstFhrdQualifications = (List<FhrdQualification>) p_empMst.getFhrdQualificationCollection();
                for (int i = 0; i < lstFhrdQualifications.size(); i++) {
                    if (lstFhrdQualifications.get(i).getFhrdQualificationPK().getQualSrno() < 0) {
                        em.persist(lstFhrdQualifications.get(i));
                    }
                }
                List<FhrdExperience> lstFhrdExperiences = (List<FhrdExperience>) p_empMst.getFhrdExperienceCollection1();
                for (int i = 0; i < lstFhrdExperiences.size(); i++) {
                    if (lstFhrdExperiences.get(i).getFhrdExperiencePK().getExpSrno() < 0) {
                        em.persist(lstFhrdExperiences.get(i));
                    }
                }
                List<FhrdDependent> lstFhrdDependents = (List<FhrdDependent>) p_empMst.getFhrdDependentCollection();
                for (int i = 0; i < lstFhrdDependents.size(); i++) {
                    if (lstFhrdDependents.get(i).getFhrdDependentPK().getDepSrno() < 0) {
                        em.persist(lstFhrdDependents.get(i));
                    }
                }
                em.merge(p_empMst);
                em.flush();
                em.clear();
            }
            utx.commit();
            isTransactionComplete = true;
        } catch (ConstraintViolationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getConstraintViolations().toString(), e);
        } catch (EJBException e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createNewEmployee", null, e);
        } catch (Exception ex) {
            try {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createNewEmployee", null, ex);
                utx.setRollbackOnly();
            } catch (Exception re) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createNewEmployee", null, re);
            }

        } finally {
            //entity manager should be closed
        }
        return isTransactionComplete;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="createContract">

    public boolean createContract(FhrdContractMst ent_ContractMst, FhrdContractGroupRelation ent_ContractGroupRelation) {
        boolean isTransactionComplete = false;
        UserTransaction utx = context.getUserTransaction();
        try {
            utx.begin();
            em.persist(ent_ContractMst);
            em.flush();
            em.clear();
            em.persist(ent_ContractGroupRelation);
            utx.commit();
            isTransactionComplete = true;
        } catch (ConstraintViolationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getConstraintViolations().toString(), e);
        } catch (EJBException e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createContract", null, e);
        } catch (Exception ex) {
            try {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createContract", null, ex);
                utx.setRollbackOnly();
            } catch (Exception re) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createContract", null, re);
            }

        } finally {
            //entity manager should be closed
        }
        return isTransactionComplete;
    }
    //</editor-fold>   
    //<editor-fold defaultstate="collapsed" desc="assignEmpContract"> 

    public boolean assignEmpContract(List<FhrdEmpContractMst> lstFhrdEmpContractMsts) {
        boolean isTransactionComplete = false;
        UserTransaction utx = context.getUserTransaction();
        try {
            Date v_max_date = null;
            Integer v_user_id = null;
            Integer v_processBy = null;
            utx.begin();
            int n = lstFhrdEmpContractMsts.size();
            for (int i = 0; i < n; i++) {
                FhrdEmpContractMst ent_ContractMst = lstFhrdEmpContractMsts.get(i);
                v_processBy = ent_ContractMst.getCreateby().getUserId();
                v_user_id = ent_ContractMst.getUserId().getUserId();
                v_max_date = DateTimeUtility.findBiggestDate(v_max_date, ent_ContractMst.getEndDate());
                em.persist(ent_ContractMst);
                em.flush();
                em.clear();
            }
            FhrdEmpmst ent_Empmst = em.find(FhrdEmpmst.class, v_user_id);
            ent_Empmst.setContractDueDate(v_max_date);
            ent_Empmst.setUpdateby(v_processBy);
            em.merge(ent_Empmst);
            em.flush();
            em.clear();
            utx.commit();
            isTransactionComplete = true;
        } catch (ConstraintViolationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getConstraintViolations().toString(), e);
        } catch (EJBException e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "assignEmpContract", null, e);
        } catch (Exception ex) {
            try {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "assignEmpContract", null, ex);
                utx.setRollbackOnly();
            } catch (Exception re) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "assignEmpContract", null, re);
            }

        } finally {
            //entity manager should be closed
        }
        return isTransactionComplete;
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="createEmpEvent"> 

    public boolean createEmpEvent(List<FhrdEmpEventDtl> lstFhrdEmpEventDtls) {
        boolean isTransactionComplete = false;
        UserTransaction utx = context.getUserTransaction();
        try {
            utx.begin();
            em = getEntityManager();
            try {
                FhrdEmpEventDtl entEmpEventDtl;
                for (int i = 0; i < lstFhrdEmpEventDtls.size(); i++) {
                    entEmpEventDtl = lstFhrdEmpEventDtls.get(i);
                    em.persist(entEmpEventDtl);
                    em.flush();
                    em.clear();
                }
            } catch (ConstraintViolationException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getConstraintViolations().toString(), e);
                throw e;
            } catch (EJBException ex) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createEmpEvent", null, ex);
            }
            utx.commit();
            isTransactionComplete = true;
        } catch (Exception ex) {
            try {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createFtasAbsentees", null, ex);
                utx.setRollbackOnly();
            } catch (Exception re) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createFtasAbsentees", null, re);
            }

        } finally {
            //entity manager should be closed
        }
        return isTransactionComplete;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="createFhrdLeaverule"> 

    public boolean createFhrdLeaverule(FhrdLeaverule entFhrdLeaverule, FhrdLeaverule entFhrdLeaverule_parent) {
        boolean isTransactionComplete = false;
        UserTransaction utx = context.getUserTransaction();
        try {
            utx.begin();
            try {
                em.persist(entFhrdLeaverule);
                if (entFhrdLeaverule_parent != null) {
                    em.merge(entFhrdLeaverule_parent);
                }
                em.flush();
                em.clear();
            } catch (ConstraintViolationException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getConstraintViolations().toString(), e);
                throw e;
            }

            utx.commit();
            isTransactionComplete = true;
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                try {
                    throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                } catch (RollbackFailureException ex1) {
                    Logger.getLogger(customJpaController.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        } finally {
        }
        return isTransactionComplete;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="createClubLeaveApplication"> 

    public Database_Output createClubLeaveApplication(FtasAbsentees entFtasAbsentees, List<FtasAbsenteesDtl> lsFtasAbsenteesDtls) {
        Database_Output database_Output = new Database_Output(false);
        UserTransaction utx = context.getUserTransaction();
        try {
            utx.begin();
            try {
                em.persist(entFtasAbsentees);
                em.flush();
                em.clear();
                int n = lsFtasAbsenteesDtls.size();
                for (int i = 0; i < n; i++) {
                    FtasAbsenteesDtl entAbsenteesDtl = lsFtasAbsenteesDtls.get(i);
                    em.persist(entAbsenteesDtl);
                    em.flush();
                    em.clear();
                }
            } catch (ConstraintViolationException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getConstraintViolations().toString(), e);
                throw e;
            }

            utx.commit();
            database_Output.setExecuted_successfully(true);
        } catch (Exception ex) {
            try {
                utx.rollback();
                database_Output.setFacesMessage(JSFMessages.getErrorMessage(ex));
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createClubLeaveApplication", null, ex);
            } catch (Exception ex1) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createClubLeaveApplication", null, ex1);
                database_Output.setFacesMessage(JSFMessages.getErrorMessage(ex1));

            }
        } finally {
        }
        return database_Output;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="FtasAbsenteesTour">

    public boolean createFtasAbsenteesTrainingOther(FtasAbsentees ent_ftasAbsentees, FtasTrainingMst entFtasTrainingMst, List<FtasTrainingScheduleDtl> lstScheduleDtls, List<FtasTrainingOrgUnitDtl> lstOrgUnitDtls) {
        UserTransaction utx = context.getUserTransaction();
        boolean isTransactionComplete = false;
        try {
            utx.begin();
            em = getEntityManager();
            em.persist(entFtasTrainingMst);
            em.flush();
            em.clear();
            int n = lstOrgUnitDtls.size();
            int m = lstScheduleDtls.size();
            for (int i = 0; i < m; i++) {
                FtasTrainingScheduleDtl entS = lstScheduleDtls.get(i);
                em.persist(entS);
                em.flush();
                em.clear();
            }
//            for (int i = 0; i < n; i++) {
//                FtasTrainingOrgUnitDtl entOrg = lstOrgUnitDtls.get(i);
//                em.persist(entOrg);
//                em.flush();
//                em.clear();
//            }
            //ent_ftasAbsentees.setTroudSrgKey(entFtasTrainingMst.getFtasTrainingOrgUnitDtlCollection().);
            em.persist(ent_ftasAbsentees);
            em.flush();
            em.clear();
            utx.commit();
            isTransactionComplete = true;
        } catch (EJBException ex) {
            System.out.println("ejb-err " + ex.toString());
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                try {
                    throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                } catch (RollbackFailureException ex1) {
                    Logger.getLogger(customJpaController.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return isTransactionComplete;
    }// </editor-fold>  
    //<editor-fold defaultstate="collapsed" desc="setSuddenContractEnd"> 

    public boolean setSuddenContractEnd(List<FhrdEmpContractMst> lstFhrdEmpContractMsts, Integer p_processBy) {
        boolean isTransactionComplete = false;
        UserTransaction utx = context.getUserTransaction();
        try {
            utx.begin();
            int n = lstFhrdEmpContractMsts.size();
            for (int i = 0; i < n; i++) {
                em.merge(lstFhrdEmpContractMsts.get(i));
                em.flush();
                em.clear();
                if (lstFhrdEmpContractMsts.get(i).getContractCategory().equals("M")) {
                    FhrdEmpmst ent_Empmst = em.find(FhrdEmpmst.class, lstFhrdEmpContractMsts.get(i).getUserId());
                    ent_Empmst.setContractDueDate(lstFhrdEmpContractMsts.get(i).getEndDate());
                    ent_Empmst.setUpdateby(p_processBy);
                    em.merge(ent_Empmst);
                    em.flush();
                    em.clear();
                }

            }
            utx.commit();
            isTransactionComplete = true;
        } catch (ConstraintViolationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getConstraintViolations().toString(), e);
        } catch (EJBException e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setSuddenContractEnd", null, e);
        } catch (Exception ex) {
            try {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setSuddenContractEnd", null, ex);
                utx.setRollbackOnly();
            } catch (Exception re) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setSuddenContractEnd", null, re);
            }

        } finally {
            //entity manager should be closed
        }
        return isTransactionComplete;
    }
    //</editor-fold>     
    //<editor-fold defaultstate="collapsed" desc="setMonthlyVariableValues"> 

    public boolean setMonthlyVariableValues(List<FhrdPaytransMonvar> lstFhrdPaytransMonvars_insert, List<FhrdPaytransMonvar> lstFhrdPaytransMonvars_update) {
        boolean isTransactionComplete = false;
        UserTransaction utx = context.getUserTransaction();
        try {
            utx.begin();
            int nInsert = lstFhrdPaytransMonvars_insert.size();
            int nUpdate = lstFhrdPaytransMonvars_update.size();
            for (int i = 0; i < nInsert; i++) {
                em.persist(lstFhrdPaytransMonvars_insert.get(i));
                em.flush();
                em.clear();
            }
            for (int i = 0; i < nUpdate; i++) {
                em.merge(lstFhrdPaytransMonvars_update.get(i));
                em.flush();
                em.clear();
            }
            utx.commit();
            isTransactionComplete = true;
        } catch (ConstraintViolationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getConstraintViolations().toString(), e);
        } catch (EJBException e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setMonthlyVariableValues", null, e);
        } catch (Exception ex) {
            try {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setMonthlyVariableValues", null, ex);
                utx.setRollbackOnly();
            } catch (Exception re) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setMonthlyVariableValues", null, re);
            }

        } finally {
            //entity manager should be closed
        }
        return isTransactionComplete;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="setEmpTransfer"> 

    public boolean setEmpTransfer(FhrdEmpEventDtl entFhrdEmpEventDtl, FhrdEmpmst entFhrdEmpmst) {
        boolean isTransactionComplete = false;
        UserTransaction utx = context.getUserTransaction();
        try {
            utx.begin();
            em.persist(entFhrdEmpEventDtl);
            em.flush();
            em.clear();
            em.merge(entFhrdEmpmst);
            em.flush();
            em.clear();
            utx.commit();
            isTransactionComplete = true;
        } catch (ConstraintViolationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getConstraintViolations().toString(), e);
        } catch (EJBException e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setMonthlyVariableValues", null, e);
        } catch (Exception ex) {
            try {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setMonthlyVariableValues", null, ex);
                utx.setRollbackOnly();
            } catch (Exception re) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setMonthlyVariableValues", null, re);
            }

        } finally {
            //entity manager should be closed
        }
        return isTransactionComplete;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="setDisbursedYes"> 

    public boolean setDisbursedYes(List<FhrdPaytransSummary> lstFhrdPaytransSummarys) {
        boolean isTransactionComplete = false;
        UserTransaction utx = context.getUserTransaction();
        try {
            utx.begin();
            int nInsert = lstFhrdPaytransSummarys.size();
            System.out.println("in jpa " + nInsert);
            for (int i = 0; i < nInsert; i++) {
                System.out.println("for user " + lstFhrdPaytransSummarys.get(i).getFhrdPaytransSummaryPK().getUserId());
                em.merge(lstFhrdPaytransSummarys.get(i));
                em.flush();
                em.clear();
            }
            utx.commit();
            isTransactionComplete = true;
        } catch (ConstraintViolationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getConstraintViolations().toString(), e);
        } catch (EJBException e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setDisbursedYes", null, e);
        } catch (Exception ex) {
            try {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setDisbursedYes", null, ex);
                utx.setRollbackOnly();
            } catch (Exception re) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setDisbursedYes", null, re);
            }

        } finally {
            //entity manager should be closed
        }
        return isTransactionComplete;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="setEmpAttendanceLock">

    public boolean setEmpAttendanceLock(List<FtasLockDtl> lstFtasLockDtls) {
        boolean isTransactionComplete = false;
        UserTransaction utx = context.getUserTransaction();
        try {
            utx.begin();
            int nInsert = lstFtasLockDtls.size();

            for (int i = 0; i < nInsert; i++) {
                em.persist(lstFtasLockDtls.get(i));
                em.flush();
                em.clear();
            }
            utx.commit();
            isTransactionComplete = true;
        } catch (ConstraintViolationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getConstraintViolations().toString(), e);
        } catch (EJBException e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setEmpAttendanceLock", null, e);
        } catch (Exception ex) {
            try {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setEmpAttendanceLock", null, ex);
                utx.setRollbackOnly();
            } catch (Exception re) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setEmpAttendanceLock", null, re);
            }

        } finally {
            //entity manager should be closed
        }
        return isTransactionComplete;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ResignRelieveTermination">

    public boolean ResignRelieveTermination(FhrdEmpeventRelieve entEmpeventRelieve, List<FhrdEmpContractMst> lsFhrdEmpContractMsts, Integer p_processBy) {
        boolean isTransactionComplete = false;
        UserTransaction utx = context.getUserTransaction();
        try {
            utx.begin();
            em.persist(entEmpeventRelieve);
            int nUpdate = lsFhrdEmpContractMsts.size();

            for (int i = 0; i < nUpdate; i++) {
                em.merge(lsFhrdEmpContractMsts.get(i));
                em.flush();
                em.clear();
                if (lsFhrdEmpContractMsts.get(i).getContractCategory().equals("M")) {
                    FhrdEmpmst ent_Empmst = em.find(FhrdEmpmst.class, lsFhrdEmpContractMsts.get(i).getUserId().getUserId());
                    ent_Empmst.setContractDueDate(lsFhrdEmpContractMsts.get(i).getEndDate());
                    ent_Empmst.setUpdateby(p_processBy);
                    em.merge(ent_Empmst);
                    em.flush();
                    em.clear();
                }

            }
            utx.commit();
            isTransactionComplete = true;
        } catch (ConstraintViolationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getConstraintViolations().toString(), e);
        } catch (EJBException e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setEmpAttendanceLock", null, e);
        } catch (Exception ex) {
            try {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setEmpAttendanceLock", null, ex);
                utx.setRollbackOnly();
            } catch (Exception re) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setEmpAttendanceLock", null, re);
            }

        } finally {
            //entity manager should be closed
        }
        return isTransactionComplete;

    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ApproveRejectEncAppls"> 
    public boolean ApproveRejectEncAppls(FhrdEmpEncash[] selectedAppls, String p_approveComment, Integer p_approveBy) {
        boolean isTransactionComplete = false;
        UserTransaction utx = context.getUserTransaction();
        try {
            utx.begin();
            int n = selectedAppls.length;
            for (int i = 0; i < n; i++) {

//                System.out.println("selectedAppls[i].getAprvcomm() " + selectedAppls[i].getAprvcomm());
                FhrdEmpEncash ent_EmpEncash = em.find(FhrdEmpEncash.class, selectedAppls[i].getEncSrgKey());
                if (selectedAppls[i].getCancEncSrgKey() == null) {
                    ent_EmpEncash.setUpdateby(new FhrdEmpmst(p_approveBy));
                    System.out.println("selectedAppls[i].getAprvcomm() " + p_approveComment);
                    ent_EmpEncash.setAprvcomm(p_approveComment);
                    ent_EmpEncash.setAprvby(new FhrdEmpmst(p_approveBy));
                    ent_EmpEncash.setAprvDate(new Date());
                    em.merge(ent_EmpEncash);
                } else {
//                    FhrdEmpEncash ent_Cancelabs = em.find(FhrdEmpCancEncash.class, selectedAppls[i].get.getCabsSrgKey());
//                    ent_Cancelabs.setUpdateby(new FhrdEmpmst(p_approveBy));
//                    ent_Cancelabs.setArcomment(selectedAppls[i].getCabsSrgKey().getArcomment().toUpperCase());
//                    ent_Cancelabs.setAprvby(new FhrdEmpmst(p_approveBy));
//                    ent_Cancelabs.setAprvDate(new Date());
//                    ent_Cancelabs.setAprvcomm(p_approveComment);
//                    em.merge(ent_Cancelabs);
                }
            }
            utx.commit();
            isTransactionComplete = true;
        } catch (Exception ex) {
            try {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "ApproveRejectAppls", null, ex);
                utx.setRollbackOnly();
            } catch (Exception re) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "ApproveRejectAppls", null, re);
            }

        } finally {
            //entity manager should be closed
        }
        return isTransactionComplete;
    }
    //</editor-fold>
}
