/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_TADA_CONV_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasTadaConvMst.findAll", query = "SELECT f FROM FtasTadaConvMst f"),
    @NamedQuery(name = "FtasTadaConvMst.findByCmSrgKey", query = "SELECT f FROM FtasTadaConvMst f WHERE f.cmSrgKey = :cmSrgKey"),
    @NamedQuery(name = "FtasTadaConvMst.findByCmDesc", query = "SELECT f FROM FtasTadaConvMst f WHERE f.cmDesc = :cmDesc"),
    @NamedQuery(name = "FtasTadaConvMst.findByCmRate", query = "SELECT f FROM FtasTadaConvMst f WHERE f.cmRate = :cmRate"),
    @NamedQuery(name = "FtasTadaConvMst.findByStartDate", query = "SELECT f FROM FtasTadaConvMst f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FtasTadaConvMst.findByEndDate", query = "SELECT f FROM FtasTadaConvMst f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FtasTadaConvMst.findByCreatedt", query = "SELECT f FROM FtasTadaConvMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasTadaConvMst.findByUpdatedt", query = "SELECT f FROM FtasTadaConvMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasTadaConvMst.findBySystemUserid", query = "SELECT f FROM FtasTadaConvMst f WHERE f.systemUserid = :systemUserid")})
public class FtasTadaConvMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CM_SRG_KEY")
    private Long cmSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "CM_DESC")
    private String cmDesc;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "CM_RATE")
    private BigDecimal cmRate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cmSrgKey")
    private Collection<FtasTadaFareDtl> ftasTadaFareDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cmSrgKey")
    private Collection<FtasTadaConvExpDtl> ftasTadaConvExpDtlCollection;

    public FtasTadaConvMst() {
    }

    public FtasTadaConvMst(Long cmSrgKey) {
        this.cmSrgKey = cmSrgKey;
    }

    public FtasTadaConvMst(Long cmSrgKey, String cmDesc, Date startDate, Date createdt) {
        this.cmSrgKey = cmSrgKey;
        this.cmDesc = cmDesc;
        this.startDate = startDate;
        this.createdt = createdt;
    }

    public Long getCmSrgKey() {
        return cmSrgKey;
    }

    public void setCmSrgKey(Long cmSrgKey) {
        this.cmSrgKey = cmSrgKey;
    }

    public String getCmDesc() {
        return cmDesc;
    }

    public void setCmDesc(String cmDesc) {
        this.cmDesc = cmDesc;
    }

    public BigDecimal getCmRate() {
        return cmRate;
    }

    public void setCmRate(BigDecimal cmRate) {
        this.cmRate = cmRate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @XmlTransient
    public Collection<FtasTadaFareDtl> getFtasTadaFareDtlCollection() {
        return ftasTadaFareDtlCollection;
    }

    public void setFtasTadaFareDtlCollection(Collection<FtasTadaFareDtl> ftasTadaFareDtlCollection) {
        this.ftasTadaFareDtlCollection = ftasTadaFareDtlCollection;
    }

    @XmlTransient
    public Collection<FtasTadaConvExpDtl> getFtasTadaConvExpDtlCollection() {
        return ftasTadaConvExpDtlCollection;
    }

    public void setFtasTadaConvExpDtlCollection(Collection<FtasTadaConvExpDtl> ftasTadaConvExpDtlCollection) {
        this.ftasTadaConvExpDtlCollection = ftasTadaConvExpDtlCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cmSrgKey != null ? cmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasTadaConvMst)) {
            return false;
        }
        FtasTadaConvMst other = (FtasTadaConvMst) object;
        if ((this.cmSrgKey == null && other.cmSrgKey != null) || (this.cmSrgKey != null && !this.cmSrgKey.equals(other.cmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasTadaConvMst[ cmSrgKey=" + cmSrgKey + " ]";
    }
    
}
