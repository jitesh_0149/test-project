/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_PAYTRANSEARNDEDN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdPaytransearndedn.findAll", query = "SELECT f FROM FhrdPaytransearndedn f"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByUserId", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.fhrdPaytransearndednPK.userId = :userId"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByEarndedncd", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.earndedncd = :earndedncd"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByOrgSalaryFromDate", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.fhrdPaytransearndednPK.orgSalaryFromDate = :orgSalaryFromDate"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByEmpSalaryFromDate", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.fhrdPaytransearndednPK.empSalaryFromDate = :empSalaryFromDate"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByEmpSalaryToDate", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.empSalaryToDate = :empSalaryToDate"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByOrgSalaryToDate", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.orgSalaryToDate = :orgSalaryToDate"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByEarndednFlag", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.earndednFlag = :earndednFlag"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByActualAmt", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.actualAmt = :actualAmt"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByAssumedAmt", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.assumedAmt = :assumedAmt"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByArrearsAmt", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.arrearsAmt = :arrearsAmt"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByRecoveryAmt", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.recoveryAmt = :recoveryAmt"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByAdjustedAmt", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.adjustedAmt = :adjustedAmt"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByPrintInPaySlip", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.printInPaySlip = :printInPaySlip"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByPrintIf0Flag", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.printIf0Flag = :printIf0Flag"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByCreatedt", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByUpdatedt", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdPaytransearndedn.findBySystemUserid", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByExportFlag", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByImportFlag", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByEmpNonPayableDays", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.empNonPayableDays = :empNonPayableDays"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByEmpPayableDays", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.empPayableDays = :empPayableDays"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByTranYear", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByPtedEdmSrgKey", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.fhrdPaytransearndednPK.ptedEdmSrgKey = :ptedEdmSrgKey"),
    @NamedQuery(name = "FhrdPaytransearndedn.findBySalaryTransactionType", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.salaryTransactionType = :salaryTransactionType"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByBasecodeFlag", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.basecodeFlag = :basecodeFlag"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByNetAmt", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.netAmt = :netAmt"),
    @NamedQuery(name = "FhrdPaytransearndedn.findBySalarySrgKey", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.salarySrgKey = :salarySrgKey"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByOrgPayableDays", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.orgPayableDays = :orgPayableDays"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByOrgWorkingDays", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.orgWorkingDays = :orgWorkingDays"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByActualAttendance", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.actualAttendance = :actualAttendance"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByConsiderAsAttendance", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.considerAsAttendance = :considerAsAttendance"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByNoOfLeaveEncash", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.noOfLeaveEncash = :noOfLeaveEncash"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByEmpWorkingDays", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.empWorkingDays = :empWorkingDays"),
    @NamedQuery(name = "FhrdPaytransearndedn.findByNoOfHolidays", query = "SELECT f FROM FhrdPaytransearndedn f WHERE f.noOfHolidays = :noOfHolidays")})
public class FhrdPaytransearndedn implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FhrdPaytransearndednPK fhrdPaytransearndednPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "EARNDEDNCD")
    private String earndedncd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_SALARY_TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date empSalaryToDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_SALARY_TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgSalaryToDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EARNDEDN_FLAG")
    private String earndednFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTUAL_AMT")
    private BigInteger actualAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ASSUMED_AMT")
    private BigInteger assumedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ARREARS_AMT")
    private BigInteger arrearsAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RECOVERY_AMT")
    private BigInteger recoveryAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADJUSTED_AMT")
    private BigInteger adjustedAmt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PRINT_IN_PAY_SLIP")
    private String printInPaySlip;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PRINT_IF_0_FLAG")
    private String printIf0Flag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_NON_PAYABLE_DAYS")
    private BigDecimal empNonPayableDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_PAYABLE_DAYS")
    private BigDecimal empPayableDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "SALARY_TRANSACTION_TYPE")
    private String salaryTransactionType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "BASECODE_FLAG")
    private String basecodeFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NET_AMT")
    private BigInteger netAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SALARY_SRG_KEY")
    private BigInteger salarySrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_PAYABLE_DAYS")
    private BigDecimal orgPayableDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_WORKING_DAYS")
    private BigDecimal orgWorkingDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTUAL_ATTENDANCE")
    private BigDecimal actualAttendance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONSIDER_AS_ATTENDANCE")
    private BigDecimal considerAsAttendance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NO_OF_LEAVE_ENCASH")
    private BigDecimal noOfLeaveEncash;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_WORKING_DAYS")
    private BigDecimal empWorkingDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NO_OF_HOLIDAYS")
    private BigDecimal noOfHolidays;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FhrdEmpmst fhrdEmpmst;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "PTED_EED_SRG_KEY", referencedColumnName = "EED_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdEmpearndedn ptedEedSrgKey;
    @JoinColumn(name = "PTED_EDM_SRG_KEY", referencedColumnName = "EDM_SRG_KEY", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FhrdEarndednmst fhrdEarndednmst;

    public FhrdPaytransearndedn() {
    }

    public FhrdPaytransearndedn(FhrdPaytransearndednPK fhrdPaytransearndednPK) {
        this.fhrdPaytransearndednPK = fhrdPaytransearndednPK;
    }

    public FhrdPaytransearndedn(FhrdPaytransearndednPK fhrdPaytransearndednPK, String earndedncd, Date empSalaryToDate, Date orgSalaryToDate, String earndednFlag, BigInteger actualAmt, BigInteger assumedAmt, BigInteger arrearsAmt, BigInteger recoveryAmt, BigInteger adjustedAmt, String printInPaySlip, String printIf0Flag, Date createdt, BigDecimal empNonPayableDays, BigDecimal empPayableDays, short tranYear, String salaryTransactionType, String basecodeFlag, BigInteger netAmt, BigInteger salarySrgKey, BigDecimal orgPayableDays, BigDecimal orgWorkingDays, BigDecimal actualAttendance, BigDecimal considerAsAttendance, BigDecimal noOfLeaveEncash, BigDecimal empWorkingDays, BigDecimal noOfHolidays) {
        this.fhrdPaytransearndednPK = fhrdPaytransearndednPK;
        this.earndedncd = earndedncd;
        this.empSalaryToDate = empSalaryToDate;
        this.orgSalaryToDate = orgSalaryToDate;
        this.earndednFlag = earndednFlag;
        this.actualAmt = actualAmt;
        this.assumedAmt = assumedAmt;
        this.arrearsAmt = arrearsAmt;
        this.recoveryAmt = recoveryAmt;
        this.adjustedAmt = adjustedAmt;
        this.printInPaySlip = printInPaySlip;
        this.printIf0Flag = printIf0Flag;
        this.createdt = createdt;
        this.empNonPayableDays = empNonPayableDays;
        this.empPayableDays = empPayableDays;
        this.tranYear = tranYear;
        this.salaryTransactionType = salaryTransactionType;
        this.basecodeFlag = basecodeFlag;
        this.netAmt = netAmt;
        this.salarySrgKey = salarySrgKey;
        this.orgPayableDays = orgPayableDays;
        this.orgWorkingDays = orgWorkingDays;
        this.actualAttendance = actualAttendance;
        this.considerAsAttendance = considerAsAttendance;
        this.noOfLeaveEncash = noOfLeaveEncash;
        this.empWorkingDays = empWorkingDays;
        this.noOfHolidays = noOfHolidays;
    }

    public FhrdPaytransearndedn(int userId, Date orgSalaryFromDate, Date empSalaryFromDate, BigDecimal ptedEdmSrgKey) {
        this.fhrdPaytransearndednPK = new FhrdPaytransearndednPK(userId, orgSalaryFromDate, empSalaryFromDate, ptedEdmSrgKey);
    }

    public FhrdPaytransearndednPK getFhrdPaytransearndednPK() {
        return fhrdPaytransearndednPK;
    }

    public void setFhrdPaytransearndednPK(FhrdPaytransearndednPK fhrdPaytransearndednPK) {
        this.fhrdPaytransearndednPK = fhrdPaytransearndednPK;
    }

    public String getEarndedncd() {
        return earndedncd;
    }

    public void setEarndedncd(String earndedncd) {
        this.earndedncd = earndedncd;
    }

    public Date getEmpSalaryToDate() {
        return empSalaryToDate;
    }

    public void setEmpSalaryToDate(Date empSalaryToDate) {
        this.empSalaryToDate = empSalaryToDate;
    }

    public Date getOrgSalaryToDate() {
        return orgSalaryToDate;
    }

    public void setOrgSalaryToDate(Date orgSalaryToDate) {
        this.orgSalaryToDate = orgSalaryToDate;
    }

    public String getEarndednFlag() {
        return earndednFlag;
    }

    public void setEarndednFlag(String earndednFlag) {
        this.earndednFlag = earndednFlag;
    }

    public BigInteger getActualAmt() {
        return actualAmt;
    }

    public void setActualAmt(BigInteger actualAmt) {
        this.actualAmt = actualAmt;
    }

    public BigInteger getAssumedAmt() {
        return assumedAmt;
    }

    public void setAssumedAmt(BigInteger assumedAmt) {
        this.assumedAmt = assumedAmt;
    }

    public BigInteger getArrearsAmt() {
        return arrearsAmt;
    }

    public void setArrearsAmt(BigInteger arrearsAmt) {
        this.arrearsAmt = arrearsAmt;
    }

    public BigInteger getRecoveryAmt() {
        return recoveryAmt;
    }

    public void setRecoveryAmt(BigInteger recoveryAmt) {
        this.recoveryAmt = recoveryAmt;
    }

    public BigInteger getAdjustedAmt() {
        return adjustedAmt;
    }

    public void setAdjustedAmt(BigInteger adjustedAmt) {
        this.adjustedAmt = adjustedAmt;
    }

    public String getPrintInPaySlip() {
        return printInPaySlip;
    }

    public void setPrintInPaySlip(String printInPaySlip) {
        this.printInPaySlip = printInPaySlip;
    }

    public String getPrintIf0Flag() {
        return printIf0Flag;
    }

    public void setPrintIf0Flag(String printIf0Flag) {
        this.printIf0Flag = printIf0Flag;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public BigDecimal getEmpNonPayableDays() {
        return empNonPayableDays;
    }

    public void setEmpNonPayableDays(BigDecimal empNonPayableDays) {
        this.empNonPayableDays = empNonPayableDays;
    }

    public BigDecimal getEmpPayableDays() {
        return empPayableDays;
    }

    public void setEmpPayableDays(BigDecimal empPayableDays) {
        this.empPayableDays = empPayableDays;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public String getSalaryTransactionType() {
        return salaryTransactionType;
    }

    public void setSalaryTransactionType(String salaryTransactionType) {
        this.salaryTransactionType = salaryTransactionType;
    }

    public String getBasecodeFlag() {
        return basecodeFlag;
    }

    public void setBasecodeFlag(String basecodeFlag) {
        this.basecodeFlag = basecodeFlag;
    }

    public BigInteger getNetAmt() {
        return netAmt;
    }

    public void setNetAmt(BigInteger netAmt) {
        this.netAmt = netAmt;
    }

    public BigInteger getSalarySrgKey() {
        return salarySrgKey;
    }

    public void setSalarySrgKey(BigInteger salarySrgKey) {
        this.salarySrgKey = salarySrgKey;
    }

    public BigDecimal getOrgPayableDays() {
        return orgPayableDays;
    }

    public void setOrgPayableDays(BigDecimal orgPayableDays) {
        this.orgPayableDays = orgPayableDays;
    }

    public BigDecimal getOrgWorkingDays() {
        return orgWorkingDays;
    }

    public void setOrgWorkingDays(BigDecimal orgWorkingDays) {
        this.orgWorkingDays = orgWorkingDays;
    }

    public BigDecimal getActualAttendance() {
        return actualAttendance;
    }

    public void setActualAttendance(BigDecimal actualAttendance) {
        this.actualAttendance = actualAttendance;
    }

    public BigDecimal getConsiderAsAttendance() {
        return considerAsAttendance;
    }

    public void setConsiderAsAttendance(BigDecimal considerAsAttendance) {
        this.considerAsAttendance = considerAsAttendance;
    }

    public BigDecimal getNoOfLeaveEncash() {
        return noOfLeaveEncash;
    }

    public void setNoOfLeaveEncash(BigDecimal noOfLeaveEncash) {
        this.noOfLeaveEncash = noOfLeaveEncash;
    }

    public BigDecimal getEmpWorkingDays() {
        return empWorkingDays;
    }

    public void setEmpWorkingDays(BigDecimal empWorkingDays) {
        this.empWorkingDays = empWorkingDays;
    }

    public BigDecimal getNoOfHolidays() {
        return noOfHolidays;
    }

    public void setNoOfHolidays(BigDecimal noOfHolidays) {
        this.noOfHolidays = noOfHolidays;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getFhrdEmpmst() {
        return fhrdEmpmst;
    }

    public void setFhrdEmpmst(FhrdEmpmst fhrdEmpmst) {
        this.fhrdEmpmst = fhrdEmpmst;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpearndedn getPtedEedSrgKey() {
        return ptedEedSrgKey;
    }

    public void setPtedEedSrgKey(FhrdEmpearndedn ptedEedSrgKey) {
        this.ptedEedSrgKey = ptedEedSrgKey;
    }

    public FhrdEarndednmst getFhrdEarndednmst() {
        return fhrdEarndednmst;
    }

    public void setFhrdEarndednmst(FhrdEarndednmst fhrdEarndednmst) {
        this.fhrdEarndednmst = fhrdEarndednmst;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fhrdPaytransearndednPK != null ? fhrdPaytransearndednPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdPaytransearndedn)) {
            return false;
        }
        FhrdPaytransearndedn other = (FhrdPaytransearndedn) object;
        if ((this.fhrdPaytransearndednPK == null && other.fhrdPaytransearndednPK != null) || (this.fhrdPaytransearndednPK != null && !this.fhrdPaytransearndednPK.equals(other.fhrdPaytransearndednPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdPaytransearndedn[ fhrdPaytransearndednPK=" + fhrdPaytransearndednPK + " ]";
    }
    
}
