/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_EMPEVENT_PROMOTION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdEmpeventPromotion.findAll", query = "SELECT f FROM FhrdEmpeventPromotion f"),
    @NamedQuery(name = "FhrdEmpeventPromotion.findByEmpepSrgKey", query = "SELECT f FROM FhrdEmpeventPromotion f WHERE f.empepSrgKey = :empepSrgKey"),
    @NamedQuery(name = "FhrdEmpeventPromotion.findByEventDate", query = "SELECT f FROM FhrdEmpeventPromotion f WHERE f.eventDate = :eventDate"),
    @NamedQuery(name = "FhrdEmpeventPromotion.findByRemarks", query = "SELECT f FROM FhrdEmpeventPromotion f WHERE f.remarks = :remarks"),
    @NamedQuery(name = "FhrdEmpeventPromotion.findByCreatedt", query = "SELECT f FROM FhrdEmpeventPromotion f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdEmpeventPromotion.findByUpdatedt", query = "SELECT f FROM FhrdEmpeventPromotion f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdEmpeventPromotion.findBySystemUserid", query = "SELECT f FROM FhrdEmpeventPromotion f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdEmpeventPromotion.findByOldDsgmSrgKey", query = "SELECT f FROM FhrdEmpeventPromotion f WHERE f.oldDsgmSrgKey = :oldDsgmSrgKey"),
    @NamedQuery(name = "FhrdEmpeventPromotion.findByNewDsgmSrgKey", query = "SELECT f FROM FhrdEmpeventPromotion f WHERE f.newDsgmSrgKey = :newDsgmSrgKey"),
    @NamedQuery(name = "FhrdEmpeventPromotion.findBySalaryIncrementFlag", query = "SELECT f FROM FhrdEmpeventPromotion f WHERE f.salaryIncrementFlag = :salaryIncrementFlag"),
    @NamedQuery(name = "FhrdEmpeventPromotion.findByRefOumUnitSrno", query = "SELECT f FROM FhrdEmpeventPromotion f WHERE f.refOumUnitSrno = :refOumUnitSrno"),
    @NamedQuery(name = "FhrdEmpeventPromotion.findByExportFlag", query = "SELECT f FROM FhrdEmpeventPromotion f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdEmpeventPromotion.findByImportFlag", query = "SELECT f FROM FhrdEmpeventPromotion f WHERE f.importFlag = :importFlag")})
public class FhrdEmpeventPromotion implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMPEP_SRG_KEY")
    private BigDecimal empepSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EVENT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventDate;
    @Size(max = 500)
    @Column(name = "REMARKS")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OLD_DSGM_SRG_KEY")
    private BigDecimal oldDsgmSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NEW_DSGM_SRG_KEY")
    private BigDecimal newDsgmSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "SALARY_INCREMENT_FLAG")
    private String salaryIncrementFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REF_OUM_UNIT_SRNO")
    private int refOumUnitSrno;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "EM_SRG_KEY", referencedColumnName = "EM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdEventMst emSrgKey;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst userId;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;

    public FhrdEmpeventPromotion() {
    }

    public FhrdEmpeventPromotion(BigDecimal empepSrgKey) {
        this.empepSrgKey = empepSrgKey;
    }

    public FhrdEmpeventPromotion(BigDecimal empepSrgKey, Date eventDate, Date createdt, BigDecimal oldDsgmSrgKey, BigDecimal newDsgmSrgKey, String salaryIncrementFlag, int refOumUnitSrno) {
        this.empepSrgKey = empepSrgKey;
        this.eventDate = eventDate;
        this.createdt = createdt;
        this.oldDsgmSrgKey = oldDsgmSrgKey;
        this.newDsgmSrgKey = newDsgmSrgKey;
        this.salaryIncrementFlag = salaryIncrementFlag;
        this.refOumUnitSrno = refOumUnitSrno;
    }

    public BigDecimal getEmpepSrgKey() {
        return empepSrgKey;
    }

    public void setEmpepSrgKey(BigDecimal empepSrgKey) {
        this.empepSrgKey = empepSrgKey;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public BigDecimal getOldDsgmSrgKey() {
        return oldDsgmSrgKey;
    }

    public void setOldDsgmSrgKey(BigDecimal oldDsgmSrgKey) {
        this.oldDsgmSrgKey = oldDsgmSrgKey;
    }

    public BigDecimal getNewDsgmSrgKey() {
        return newDsgmSrgKey;
    }

    public void setNewDsgmSrgKey(BigDecimal newDsgmSrgKey) {
        this.newDsgmSrgKey = newDsgmSrgKey;
    }

    public String getSalaryIncrementFlag() {
        return salaryIncrementFlag;
    }

    public void setSalaryIncrementFlag(String salaryIncrementFlag) {
        this.salaryIncrementFlag = salaryIncrementFlag;
    }

    public int getRefOumUnitSrno() {
        return refOumUnitSrno;
    }

    public void setRefOumUnitSrno(int refOumUnitSrno) {
        this.refOumUnitSrno = refOumUnitSrno;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEventMst getEmSrgKey() {
        return emSrgKey;
    }

    public void setEmSrgKey(FhrdEventMst emSrgKey) {
        this.emSrgKey = emSrgKey;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getUserId() {
        return userId;
    }

    public void setUserId(FhrdEmpmst userId) {
        this.userId = userId;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (empepSrgKey != null ? empepSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdEmpeventPromotion)) {
            return false;
        }
        FhrdEmpeventPromotion other = (FhrdEmpeventPromotion) object;
        if ((this.empepSrgKey == null && other.empepSrgKey != null) || (this.empepSrgKey != null && !this.empepSrgKey.equals(other.empepSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdEmpeventPromotion[ empepSrgKey=" + empepSrgKey + " ]";
    }
    
}
