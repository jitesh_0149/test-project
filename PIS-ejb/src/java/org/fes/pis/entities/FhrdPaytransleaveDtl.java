/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_PAYTRANSLEAVE_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdPaytransleaveDtl.findAll", query = "SELECT f FROM FhrdPaytransleaveDtl f"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByLeavecd", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.leavecd = :leavecd"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByOrgAttendanceFromDate", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.orgAttendanceFromDate = :orgAttendanceFromDate"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByEmpAttendanceFromDate", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.empAttendanceFromDate = :empAttendanceFromDate"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByEmpAttendanceToDate", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.empAttendanceToDate = :empAttendanceToDate"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByOrgAttendanceToDate", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.orgAttendanceToDate = :orgAttendanceToDate"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByOpeningBal", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.openingBal = :openingBal"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByNoOfLeaveEarn", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.noOfLeaveEarn = :noOfLeaveEarn"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByAddNewLeavebal", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.addNewLeavebal = :addNewLeavebal"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByLeavebalCarryforward", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.leavebalCarryforward = :leavebalCarryforward"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByLeaveCons", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.leaveCons = :leaveCons"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByLeaveBalLaps", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.leaveBalLaps = :leaveBalLaps"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByNoOfLeaveEncash", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.noOfLeaveEncash = :noOfLeaveEncash"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByLeaveEncashAmt", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.leaveEncashAmt = :leaveEncashAmt"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByCurrentBal", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.currentBal = :currentBal"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByEmpNonPayableDays", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.empNonPayableDays = :empNonPayableDays"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByParentLeavecd", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.parentLeavecd = :parentLeavecd"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByParentLeavecons", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.parentLeavecons = :parentLeavecons"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByConsumedFreq", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.consumedFreq = :consumedFreq"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByCreatedt", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByUpdatedt", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findBySystemUserid", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByExportFlag", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByImportFlag", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByRemarks", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.remarks = :remarks"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByLeaveEarnableDays", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.leaveEarnableDays = :leaveEarnableDays"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByPldSrgKey", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.pldSrgKey = :pldSrgKey"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByDeductDays", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.deductDays = :deductDays"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByTranYear", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByParentLmSrgKey", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.parentLmSrgKey = :parentLmSrgKey"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByDeductDaysRatio", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.deductDaysRatio = :deductDaysRatio"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByAttdSrgKey", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.attdSrgKey = :attdSrgKey"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByTotalAddNewLeavebal", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.totalAddNewLeavebal = :totalAddNewLeavebal"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByElrSrgKey", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.elrSrgKey = :elrSrgKey"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByVirtualLeave", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.virtualLeave = :virtualLeave"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByBalCheckedInAppliFlag", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.balCheckedInAppliFlag = :balCheckedInAppliFlag"),
    @NamedQuery(name = "FhrdPaytransleaveDtl.findByPrintIf0Cons", query = "SELECT f FROM FhrdPaytransleaveDtl f WHERE f.printIf0Cons = :printIf0Cons")})
public class FhrdPaytransleaveDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "LEAVECD")
    private String leavecd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_ATTENDANCE_FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgAttendanceFromDate;
    @Column(name = "EMP_ATTENDANCE_FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date empAttendanceFromDate;
    @Column(name = "EMP_ATTENDANCE_TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date empAttendanceToDate;
    @Column(name = "ORG_ATTENDANCE_TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgAttendanceToDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "OPENING_BAL")
    private BigDecimal openingBal;
    @Column(name = "NO_OF_LEAVE_EARN")
    private BigDecimal noOfLeaveEarn;
    @Column(name = "ADD_NEW_LEAVEBAL")
    private BigDecimal addNewLeavebal;
    @Column(name = "LEAVEBAL_CARRYFORWARD")
    private BigDecimal leavebalCarryforward;
    @Column(name = "LEAVE_CONS")
    private BigDecimal leaveCons;
    @Column(name = "LEAVE_BAL_LAPS")
    private BigDecimal leaveBalLaps;
    @Column(name = "NO_OF_LEAVE_ENCASH")
    private BigDecimal noOfLeaveEncash;
    @Column(name = "LEAVE_ENCASH_AMT")
    private BigDecimal leaveEncashAmt;
    @Column(name = "CURRENT_BAL")
    private BigDecimal currentBal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_NON_PAYABLE_DAYS")
    private BigDecimal empNonPayableDays;
    @Size(max = 1)
    @Column(name = "PARENT_LEAVECD")
    private String parentLeavecd;
    @Column(name = "PARENT_LEAVECONS")
    private BigDecimal parentLeavecons;
    @Column(name = "CONSUMED_FREQ")
    private Long consumedFreq;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Size(max = 1000)
    @Column(name = "REMARKS")
    private String remarks;
    @Column(name = "LEAVE_EARNABLE_DAYS")
    private BigDecimal leaveEarnableDays;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "PLD_SRG_KEY")
    private String pldSrgKey;
    @Column(name = "DEDUCT_DAYS")
    private BigDecimal deductDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Column(name = "PARENT_LM_SRG_KEY")
    private BigDecimal parentLmSrgKey;
    @Column(name = "DEDUCT_DAYS_RATIO")
    private BigDecimal deductDaysRatio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATTD_SRG_KEY")
    private BigInteger attdSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_ADD_NEW_LEAVEBAL")
    private BigDecimal totalAddNewLeavebal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ELR_SRG_KEY")
    private BigDecimal elrSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "VIRTUAL_LEAVE")
    private String virtualLeave;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "BAL_CHECKED_IN_APPLI_FLAG")
    private String balCheckedInAppliFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PRINT_IF_0_CONS")
    private String printIf0Cons;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne
    private FtasLeavemst lmSrgKey;
    @JoinColumn(name = "PLM_SRG_KEY", referencedColumnName = "PLM_SRG_KEY")
    @ManyToOne
    private FhrdPaytransleaveMst plmSrgKey;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst userId;
    @JoinColumn(name = "EMPLB_SRG_KEY", referencedColumnName = "EMPLB_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdEmpleavebal emplbSrgKey;

    public FhrdPaytransleaveDtl() {
    }

    public FhrdPaytransleaveDtl(String pldSrgKey) {
        this.pldSrgKey = pldSrgKey;
    }

    public FhrdPaytransleaveDtl(String pldSrgKey, String leavecd, Date orgAttendanceFromDate, BigDecimal empNonPayableDays, Date createdt, short tranYear, BigInteger attdSrgKey, BigDecimal totalAddNewLeavebal, BigDecimal elrSrgKey, String virtualLeave, String balCheckedInAppliFlag, String printIf0Cons) {
        this.pldSrgKey = pldSrgKey;
        this.leavecd = leavecd;
        this.orgAttendanceFromDate = orgAttendanceFromDate;
        this.empNonPayableDays = empNonPayableDays;
        this.createdt = createdt;
        this.tranYear = tranYear;
        this.attdSrgKey = attdSrgKey;
        this.totalAddNewLeavebal = totalAddNewLeavebal;
        this.elrSrgKey = elrSrgKey;
        this.virtualLeave = virtualLeave;
        this.balCheckedInAppliFlag = balCheckedInAppliFlag;
        this.printIf0Cons = printIf0Cons;
    }

    public String getLeavecd() {
        return leavecd;
    }

    public void setLeavecd(String leavecd) {
        this.leavecd = leavecd;
    }

    public Date getOrgAttendanceFromDate() {
        return orgAttendanceFromDate;
    }

    public void setOrgAttendanceFromDate(Date orgAttendanceFromDate) {
        this.orgAttendanceFromDate = orgAttendanceFromDate;
    }

    public Date getEmpAttendanceFromDate() {
        return empAttendanceFromDate;
    }

    public void setEmpAttendanceFromDate(Date empAttendanceFromDate) {
        this.empAttendanceFromDate = empAttendanceFromDate;
    }

    public Date getEmpAttendanceToDate() {
        return empAttendanceToDate;
    }

    public void setEmpAttendanceToDate(Date empAttendanceToDate) {
        this.empAttendanceToDate = empAttendanceToDate;
    }

    public Date getOrgAttendanceToDate() {
        return orgAttendanceToDate;
    }

    public void setOrgAttendanceToDate(Date orgAttendanceToDate) {
        this.orgAttendanceToDate = orgAttendanceToDate;
    }

    public BigDecimal getOpeningBal() {
        return openingBal;
    }

    public void setOpeningBal(BigDecimal openingBal) {
        this.openingBal = openingBal;
    }

    public BigDecimal getNoOfLeaveEarn() {
        return noOfLeaveEarn;
    }

    public void setNoOfLeaveEarn(BigDecimal noOfLeaveEarn) {
        this.noOfLeaveEarn = noOfLeaveEarn;
    }

    public BigDecimal getAddNewLeavebal() {
        return addNewLeavebal;
    }

    public void setAddNewLeavebal(BigDecimal addNewLeavebal) {
        this.addNewLeavebal = addNewLeavebal;
    }

    public BigDecimal getLeavebalCarryforward() {
        return leavebalCarryforward;
    }

    public void setLeavebalCarryforward(BigDecimal leavebalCarryforward) {
        this.leavebalCarryforward = leavebalCarryforward;
    }

    public BigDecimal getLeaveCons() {
        return leaveCons;
    }

    public void setLeaveCons(BigDecimal leaveCons) {
        this.leaveCons = leaveCons;
    }

    public BigDecimal getLeaveBalLaps() {
        return leaveBalLaps;
    }

    public void setLeaveBalLaps(BigDecimal leaveBalLaps) {
        this.leaveBalLaps = leaveBalLaps;
    }

    public BigDecimal getNoOfLeaveEncash() {
        return noOfLeaveEncash;
    }

    public void setNoOfLeaveEncash(BigDecimal noOfLeaveEncash) {
        this.noOfLeaveEncash = noOfLeaveEncash;
    }

    public BigDecimal getLeaveEncashAmt() {
        return leaveEncashAmt;
    }

    public void setLeaveEncashAmt(BigDecimal leaveEncashAmt) {
        this.leaveEncashAmt = leaveEncashAmt;
    }

    public BigDecimal getCurrentBal() {
        return currentBal;
    }

    public void setCurrentBal(BigDecimal currentBal) {
        this.currentBal = currentBal;
    }

    public BigDecimal getEmpNonPayableDays() {
        return empNonPayableDays;
    }

    public void setEmpNonPayableDays(BigDecimal empNonPayableDays) {
        this.empNonPayableDays = empNonPayableDays;
    }

    public String getParentLeavecd() {
        return parentLeavecd;
    }

    public void setParentLeavecd(String parentLeavecd) {
        this.parentLeavecd = parentLeavecd;
    }

    public BigDecimal getParentLeavecons() {
        return parentLeavecons;
    }

    public void setParentLeavecons(BigDecimal parentLeavecons) {
        this.parentLeavecons = parentLeavecons;
    }

    public Long getConsumedFreq() {
        return consumedFreq;
    }

    public void setConsumedFreq(Long consumedFreq) {
        this.consumedFreq = consumedFreq;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public BigDecimal getLeaveEarnableDays() {
        return leaveEarnableDays;
    }

    public void setLeaveEarnableDays(BigDecimal leaveEarnableDays) {
        this.leaveEarnableDays = leaveEarnableDays;
    }

    public String getPldSrgKey() {
        return pldSrgKey;
    }

    public void setPldSrgKey(String pldSrgKey) {
        this.pldSrgKey = pldSrgKey;
    }

    public BigDecimal getDeductDays() {
        return deductDays;
    }

    public void setDeductDays(BigDecimal deductDays) {
        this.deductDays = deductDays;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public BigDecimal getParentLmSrgKey() {
        return parentLmSrgKey;
    }

    public void setParentLmSrgKey(BigDecimal parentLmSrgKey) {
        this.parentLmSrgKey = parentLmSrgKey;
    }

    public BigDecimal getDeductDaysRatio() {
        return deductDaysRatio;
    }

    public void setDeductDaysRatio(BigDecimal deductDaysRatio) {
        this.deductDaysRatio = deductDaysRatio;
    }

    public BigInteger getAttdSrgKey() {
        return attdSrgKey;
    }

    public void setAttdSrgKey(BigInteger attdSrgKey) {
        this.attdSrgKey = attdSrgKey;
    }

    public BigDecimal getTotalAddNewLeavebal() {
        return totalAddNewLeavebal;
    }

    public void setTotalAddNewLeavebal(BigDecimal totalAddNewLeavebal) {
        this.totalAddNewLeavebal = totalAddNewLeavebal;
    }

    public BigDecimal getElrSrgKey() {
        return elrSrgKey;
    }

    public void setElrSrgKey(BigDecimal elrSrgKey) {
        this.elrSrgKey = elrSrgKey;
    }

    public String getVirtualLeave() {
        return virtualLeave;
    }

    public void setVirtualLeave(String virtualLeave) {
        this.virtualLeave = virtualLeave;
    }

    public String getBalCheckedInAppliFlag() {
        return balCheckedInAppliFlag;
    }

    public void setBalCheckedInAppliFlag(String balCheckedInAppliFlag) {
        this.balCheckedInAppliFlag = balCheckedInAppliFlag;
    }

    public String getPrintIf0Cons() {
        return printIf0Cons;
    }

    public void setPrintIf0Cons(String printIf0Cons) {
        this.printIf0Cons = printIf0Cons;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasLeavemst getLmSrgKey() {
        return lmSrgKey;
    }

    public void setLmSrgKey(FtasLeavemst lmSrgKey) {
        this.lmSrgKey = lmSrgKey;
    }

    public FhrdPaytransleaveMst getPlmSrgKey() {
        return plmSrgKey;
    }

    public void setPlmSrgKey(FhrdPaytransleaveMst plmSrgKey) {
        this.plmSrgKey = plmSrgKey;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getUserId() {
        return userId;
    }

    public void setUserId(FhrdEmpmst userId) {
        this.userId = userId;
    }

    public FhrdEmpleavebal getEmplbSrgKey() {
        return emplbSrgKey;
    }

    public void setEmplbSrgKey(FhrdEmpleavebal emplbSrgKey) {
        this.emplbSrgKey = emplbSrgKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pldSrgKey != null ? pldSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdPaytransleaveDtl)) {
            return false;
        }
        FhrdPaytransleaveDtl other = (FhrdPaytransleaveDtl) object;
        if ((this.pldSrgKey == null && other.pldSrgKey != null) || (this.pldSrgKey != null && !this.pldSrgKey.equals(other.pldSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdPaytransleaveDtl[ pldSrgKey=" + pldSrgKey + " ]";
    }
    
}
