/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_TADA_OTHER_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasTadaOtherDtl.findAll", query = "SELECT f FROM FtasTadaOtherDtl f"),
    @NamedQuery(name = "FtasTadaOtherDtl.findByTbmSrgKey", query = "SELECT f FROM FtasTadaOtherDtl f WHERE f.ftasTadaOtherDtlPK.tbmSrgKey = :tbmSrgKey"),
    @NamedQuery(name = "FtasTadaOtherDtl.findBySrno", query = "SELECT f FROM FtasTadaOtherDtl f WHERE f.ftasTadaOtherDtlPK.srno = :srno"),
    @NamedQuery(name = "FtasTadaOtherDtl.findByUserId", query = "SELECT f FROM FtasTadaOtherDtl f WHERE f.userId = :userId"),
    @NamedQuery(name = "FtasTadaOtherDtl.findByOthClaimAmt", query = "SELECT f FROM FtasTadaOtherDtl f WHERE f.othClaimAmt = :othClaimAmt"),
    @NamedQuery(name = "FtasTadaOtherDtl.findByOthClaimDesc", query = "SELECT f FROM FtasTadaOtherDtl f WHERE f.othClaimDesc = :othClaimDesc"),
    @NamedQuery(name = "FtasTadaOtherDtl.findByCreatedt", query = "SELECT f FROM FtasTadaOtherDtl f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasTadaOtherDtl.findByUpdatedt", query = "SELECT f FROM FtasTadaOtherDtl f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasTadaOtherDtl.findBySystemUserid", query = "SELECT f FROM FtasTadaOtherDtl f WHERE f.systemUserid = :systemUserid")})
public class FtasTadaOtherDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FtasTadaOtherDtlPK ftasTadaOtherDtlPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_ID")
    private int userId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "OTH_CLAIM_AMT")
    private BigDecimal othClaimAmt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "OTH_CLAIM_DESC")
    private String othClaimDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "TBM_SRG_KEY", referencedColumnName = "TBM_SRG_KEY", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FtasTadaBillMst ftasTadaBillMst;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FtasTadaOtherDtl() {
    }

    public FtasTadaOtherDtl(FtasTadaOtherDtlPK ftasTadaOtherDtlPK) {
        this.ftasTadaOtherDtlPK = ftasTadaOtherDtlPK;
    }

    public FtasTadaOtherDtl(FtasTadaOtherDtlPK ftasTadaOtherDtlPK, int userId, BigDecimal othClaimAmt, String othClaimDesc, Date createdt) {
        this.ftasTadaOtherDtlPK = ftasTadaOtherDtlPK;
        this.userId = userId;
        this.othClaimAmt = othClaimAmt;
        this.othClaimDesc = othClaimDesc;
        this.createdt = createdt;
    }

    public FtasTadaOtherDtl(BigDecimal tbmSrgKey, int srno) {
        this.ftasTadaOtherDtlPK = new FtasTadaOtherDtlPK(tbmSrgKey, srno);
    }

    public FtasTadaOtherDtlPK getFtasTadaOtherDtlPK() {
        return ftasTadaOtherDtlPK;
    }

    public void setFtasTadaOtherDtlPK(FtasTadaOtherDtlPK ftasTadaOtherDtlPK) {
        this.ftasTadaOtherDtlPK = ftasTadaOtherDtlPK;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public BigDecimal getOthClaimAmt() {
        return othClaimAmt;
    }

    public void setOthClaimAmt(BigDecimal othClaimAmt) {
        this.othClaimAmt = othClaimAmt;
    }

    public String getOthClaimDesc() {
        return othClaimDesc;
    }

    public void setOthClaimDesc(String othClaimDesc) {
        this.othClaimDesc = othClaimDesc;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasTadaBillMst getFtasTadaBillMst() {
        return ftasTadaBillMst;
    }

    public void setFtasTadaBillMst(FtasTadaBillMst ftasTadaBillMst) {
        this.ftasTadaBillMst = ftasTadaBillMst;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ftasTadaOtherDtlPK != null ? ftasTadaOtherDtlPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasTadaOtherDtl)) {
            return false;
        }
        FtasTadaOtherDtl other = (FtasTadaOtherDtl) object;
        if ((this.ftasTadaOtherDtlPK == null && other.ftasTadaOtherDtlPK != null) || (this.ftasTadaOtherDtlPK != null && !this.ftasTadaOtherDtlPK.equals(other.ftasTadaOtherDtlPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasTadaOtherDtl[ ftasTadaOtherDtlPK=" + ftasTadaOtherDtlPK + " ]";
    }
    
}
