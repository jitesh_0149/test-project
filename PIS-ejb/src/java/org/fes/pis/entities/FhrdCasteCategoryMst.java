/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_CASTE_CATEGORY_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdCasteCategoryMst.findAll", query = "SELECT f FROM FhrdCasteCategoryMst f"),
    @NamedQuery(name = "FhrdCasteCategoryMst.findByCcmSrgKey", query = "SELECT f FROM FhrdCasteCategoryMst f WHERE f.ccmSrgKey = :ccmSrgKey"),
    @NamedQuery(name = "FhrdCasteCategoryMst.findByShortname", query = "SELECT f FROM FhrdCasteCategoryMst f WHERE f.shortname = :shortname"),
    @NamedQuery(name = "FhrdCasteCategoryMst.findByDescription", query = "SELECT f FROM FhrdCasteCategoryMst f WHERE f.description = :description"),
    @NamedQuery(name = "FhrdCasteCategoryMst.findByCreateby", query = "SELECT f FROM FhrdCasteCategoryMst f WHERE f.createby = :createby"),
    @NamedQuery(name = "FhrdCasteCategoryMst.findByCreatedt", query = "SELECT f FROM FhrdCasteCategoryMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdCasteCategoryMst.findByUpdateby", query = "SELECT f FROM FhrdCasteCategoryMst f WHERE f.updateby = :updateby"),
    @NamedQuery(name = "FhrdCasteCategoryMst.findByUpdatedt", query = "SELECT f FROM FhrdCasteCategoryMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdCasteCategoryMst.findByExportFlag", query = "SELECT f FROM FhrdCasteCategoryMst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdCasteCategoryMst.findByImportFlag", query = "SELECT f FROM FhrdCasteCategoryMst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdCasteCategoryMst.findBySystemUserid", query = "SELECT f FROM FhrdCasteCategoryMst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdCasteCategoryMst.findByStartDate", query = "SELECT f FROM FhrdCasteCategoryMst f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdCasteCategoryMst.findByEndDate", query = "SELECT f FROM FhrdCasteCategoryMst f WHERE f.endDate = :endDate")})
public class FhrdCasteCategoryMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CCM_SRG_KEY")
    private Integer ccmSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "SHORTNAME")
    private String shortname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEBY")
    private int createby;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEBY")
    private Integer updateby;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @OneToMany(mappedBy = "ccmSrgKey")
    private Collection<FhrdEmpmst> fhrdEmpmstCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;

    public FhrdCasteCategoryMst() {
    }

    public FhrdCasteCategoryMst(Integer ccmSrgKey) {
        this.ccmSrgKey = ccmSrgKey;
    }

    public FhrdCasteCategoryMst(Integer ccmSrgKey, String shortname, String description, int createby, Date createdt, Date startDate) {
        this.ccmSrgKey = ccmSrgKey;
        this.shortname = shortname;
        this.description = description;
        this.createby = createby;
        this.createdt = createdt;
        this.startDate = startDate;
    }

    public Integer getCcmSrgKey() {
        return ccmSrgKey;
    }

    public void setCcmSrgKey(Integer ccmSrgKey) {
        this.ccmSrgKey = ccmSrgKey;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCreateby() {
        return createby;
    }

    public void setCreateby(int createby) {
        this.createby = createby;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Integer getUpdateby() {
        return updateby;
    }

    public void setUpdateby(Integer updateby) {
        this.updateby = updateby;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @XmlTransient
    public Collection<FhrdEmpmst> getFhrdEmpmstCollection() {
        return fhrdEmpmstCollection;
    }

    public void setFhrdEmpmstCollection(Collection<FhrdEmpmst> fhrdEmpmstCollection) {
        this.fhrdEmpmstCollection = fhrdEmpmstCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ccmSrgKey != null ? ccmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdCasteCategoryMst)) {
            return false;
        }
        FhrdCasteCategoryMst other = (FhrdCasteCategoryMst) object;
        if ((this.ccmSrgKey == null && other.ccmSrgKey != null) || (this.ccmSrgKey != null && !this.ccmSrgKey.equals(other.ccmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdCasteCategoryMst[ ccmSrgKey=" + ccmSrgKey + " ]";
    }
    
}
