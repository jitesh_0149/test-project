/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_LEAVERULE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdLeaverule.findAll", query = "SELECT f FROM FhrdLeaverule f"),
    @NamedQuery(name = "FhrdLeaverule.findBySrno", query = "SELECT f FROM FhrdLeaverule f WHERE f.srno = :srno"),
    @NamedQuery(name = "FhrdLeaverule.findByLeavecd", query = "SELECT f FROM FhrdLeaverule f WHERE f.leavecd = :leavecd"),
    @NamedQuery(name = "FhrdLeaverule.findByStartDate", query = "SELECT f FROM FhrdLeaverule f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdLeaverule.findByEndDate", query = "SELECT f FROM FhrdLeaverule f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FhrdLeaverule.findByWefDate", query = "SELECT f FROM FhrdLeaverule f WHERE f.wefDate = :wefDate"),
    @NamedQuery(name = "FhrdLeaverule.findByBalanceEarnType", query = "SELECT f FROM FhrdLeaverule f WHERE f.balanceEarnType = :balanceEarnType"),
    @NamedQuery(name = "FhrdLeaverule.findByNoOfLeave", query = "SELECT f FROM FhrdLeaverule f WHERE f.noOfLeave = :noOfLeave"),
    @NamedQuery(name = "FhrdLeaverule.findByProportionateBalance", query = "SELECT f FROM FhrdLeaverule f WHERE f.proportionateBalance = :proportionateBalance"),
    @NamedQuery(name = "FhrdLeaverule.findByParentSrno", query = "SELECT f FROM FhrdLeaverule f WHERE f.parentSrno = :parentSrno"),
    @NamedQuery(name = "FhrdLeaverule.findByCarryForward", query = "SELECT f FROM FhrdLeaverule f WHERE f.carryForward = :carryForward"),
    @NamedQuery(name = "FhrdLeaverule.findByCarryForwardMax", query = "SELECT f FROM FhrdLeaverule f WHERE f.carryForwardMax = :carryForwardMax"),
    @NamedQuery(name = "FhrdLeaverule.findByCarryForwardRemaining", query = "SELECT f FROM FhrdLeaverule f WHERE f.carryForwardRemaining = :carryForwardRemaining"),
    @NamedQuery(name = "FhrdLeaverule.findByDaysPerLeaveBalComparison", query = "SELECT f FROM FhrdLeaverule f WHERE f.daysPerLeaveBalComparison = :daysPerLeaveBalComparison"),
    @NamedQuery(name = "FhrdLeaverule.findByBalCheckedInAppliFlag", query = "SELECT f FROM FhrdLeaverule f WHERE f.balCheckedInAppliFlag = :balCheckedInAppliFlag"),
    @NamedQuery(name = "FhrdLeaverule.findByParentCarryForward", query = "SELECT f FROM FhrdLeaverule f WHERE f.parentCarryForward = :parentCarryForward"),
    @NamedQuery(name = "FhrdLeaverule.findByParentCarryForwardMax", query = "SELECT f FROM FhrdLeaverule f WHERE f.parentCarryForwardMax = :parentCarryForwardMax"),
    @NamedQuery(name = "FhrdLeaverule.findByParentCarryforwardRemaining", query = "SELECT f FROM FhrdLeaverule f WHERE f.parentCarryforwardRemaining = :parentCarryforwardRemaining"),
    @NamedQuery(name = "FhrdLeaverule.findByParentLeaveRatio", query = "SELECT f FROM FhrdLeaverule f WHERE f.parentLeaveRatio = :parentLeaveRatio"),
    @NamedQuery(name = "FhrdLeaverule.findByDeductDays", query = "SELECT f FROM FhrdLeaverule f WHERE f.deductDays = :deductDays"),
    @NamedQuery(name = "FhrdLeaverule.findByDeductLeaves", query = "SELECT f FROM FhrdLeaverule f WHERE f.deductLeaves = :deductLeaves"),
    @NamedQuery(name = "FhrdLeaverule.findByEarnLeavePeriod", query = "SELECT f FROM FhrdLeaverule f WHERE f.earnLeavePeriod = :earnLeavePeriod"),
    @NamedQuery(name = "FhrdLeaverule.findByEarnLeavePeriodUnit", query = "SELECT f FROM FhrdLeaverule f WHERE f.earnLeavePeriodUnit = :earnLeavePeriodUnit"),
    @NamedQuery(name = "FhrdLeaverule.findByEarnLeavePerPeriod", query = "SELECT f FROM FhrdLeaverule f WHERE f.earnLeavePerPeriod = :earnLeavePerPeriod"),
    @NamedQuery(name = "FhrdLeaverule.findByAccumulation", query = "SELECT f FROM FhrdLeaverule f WHERE f.accumulation = :accumulation"),
    @NamedQuery(name = "FhrdLeaverule.findByGenderFlag", query = "SELECT f FROM FhrdLeaverule f WHERE f.genderFlag = :genderFlag"),
    @NamedQuery(name = "FhrdLeaverule.findByMaxSpecialApprove", query = "SELECT f FROM FhrdLeaverule f WHERE f.maxSpecialApprove = :maxSpecialApprove"),
    @NamedQuery(name = "FhrdLeaverule.findByFreqPeriod", query = "SELECT f FROM FhrdLeaverule f WHERE f.freqPeriod = :freqPeriod"),
    @NamedQuery(name = "FhrdLeaverule.findByFreqPeriodUnit", query = "SELECT f FROM FhrdLeaverule f WHERE f.freqPeriodUnit = :freqPeriodUnit"),
    @NamedQuery(name = "FhrdLeaverule.findByFreqPerPeriod", query = "SELECT f FROM FhrdLeaverule f WHERE f.freqPerPeriod = :freqPerPeriod"),
    @NamedQuery(name = "FhrdLeaverule.findByEncashFormula", query = "SELECT f FROM FhrdLeaverule f WHERE f.encashFormula = :encashFormula"),
    @NamedQuery(name = "FhrdLeaverule.findByEncashment", query = "SELECT f FROM FhrdLeaverule f WHERE f.encashment = :encashment"),
    @NamedQuery(name = "FhrdLeaverule.findByClubWithOther", query = "SELECT f FROM FhrdLeaverule f WHERE f.clubWithOther = :clubWithOther"),
    @NamedQuery(name = "FhrdLeaverule.findByEarnConfigFlag", query = "SELECT f FROM FhrdLeaverule f WHERE f.earnConfigFlag = :earnConfigFlag"),
    @NamedQuery(name = "FhrdLeaverule.findByHolidayEmbedded", query = "SELECT f FROM FhrdLeaverule f WHERE f.holidayEmbedded = :holidayEmbedded"),
    @NamedQuery(name = "FhrdLeaverule.findByCommuted", query = "SELECT f FROM FhrdLeaverule f WHERE f.commuted = :commuted"),
    @NamedQuery(name = "FhrdLeaverule.findByMaxTaken", query = "SELECT f FROM FhrdLeaverule f WHERE f.maxTaken = :maxTaken"),
    @NamedQuery(name = "FhrdLeaverule.findByMinTaken", query = "SELECT f FROM FhrdLeaverule f WHERE f.minTaken = :minTaken"),
    @NamedQuery(name = "FhrdLeaverule.findByLeaveFormula", query = "SELECT f FROM FhrdLeaverule f WHERE f.leaveFormula = :leaveFormula"),
    @NamedQuery(name = "FhrdLeaverule.findByLeaveBalDedPerLeave", query = "SELECT f FROM FhrdLeaverule f WHERE f.leaveBalDedPerLeave = :leaveBalDedPerLeave"),
    @NamedQuery(name = "FhrdLeaverule.findByPartFulfillSubsysFlag", query = "SELECT f FROM FhrdLeaverule f WHERE f.partFulfillSubsysFlag = :partFulfillSubsysFlag"),
    @NamedQuery(name = "FhrdLeaverule.findByEncashMinLeave", query = "SELECT f FROM FhrdLeaverule f WHERE f.encashMinLeave = :encashMinLeave"),
    @NamedQuery(name = "FhrdLeaverule.findByEncashMaxLeave", query = "SELECT f FROM FhrdLeaverule f WHERE f.encashMaxLeave = :encashMaxLeave"),
    @NamedQuery(name = "FhrdLeaverule.findByEncashExtraMinLeave", query = "SELECT f FROM FhrdLeaverule f WHERE f.encashExtraMinLeave = :encashExtraMinLeave"),
    @NamedQuery(name = "FhrdLeaverule.findByEncashApprovalMinLeave", query = "SELECT f FROM FhrdLeaverule f WHERE f.encashApprovalMinLeave = :encashApprovalMinLeave"),
    @NamedQuery(name = "FhrdLeaverule.findByEncashFreq", query = "SELECT f FROM FhrdLeaverule f WHERE f.encashFreq = :encashFreq"),
    @NamedQuery(name = "FhrdLeaverule.findByEncashFreqPeriod", query = "SELECT f FROM FhrdLeaverule f WHERE f.encashFreqPeriod = :encashFreqPeriod"),
    @NamedQuery(name = "FhrdLeaverule.findByEncashFreqPeriodUnit", query = "SELECT f FROM FhrdLeaverule f WHERE f.encashFreqPeriodUnit = :encashFreqPeriodUnit"),
    @NamedQuery(name = "FhrdLeaverule.findByCommuteMinLeave", query = "SELECT f FROM FhrdLeaverule f WHERE f.commuteMinLeave = :commuteMinLeave"),
    @NamedQuery(name = "FhrdLeaverule.findByCommuteDeductDays", query = "SELECT f FROM FhrdLeaverule f WHERE f.commuteDeductDays = :commuteDeductDays"),
    @NamedQuery(name = "FhrdLeaverule.findByCommuteDeductLeaves", query = "SELECT f FROM FhrdLeaverule f WHERE f.commuteDeductLeaves = :commuteDeductLeaves"),
    @NamedQuery(name = "FhrdLeaverule.findByLeaveBalDedPerComutleave", query = "SELECT f FROM FhrdLeaverule f WHERE f.leaveBalDedPerComutleave = :leaveBalDedPerComutleave"),
    @NamedQuery(name = "FhrdLeaverule.findByRemarks", query = "SELECT f FROM FhrdLeaverule f WHERE f.remarks = :remarks"),
    @NamedQuery(name = "FhrdLeaverule.findByCommsrno", query = "SELECT f FROM FhrdLeaverule f WHERE f.commsrno = :commsrno"),
    @NamedQuery(name = "FhrdLeaverule.findByCommflag", query = "SELECT f FROM FhrdLeaverule f WHERE f.commflag = :commflag"),
    @NamedQuery(name = "FhrdLeaverule.findByCissuesrno", query = "SELECT f FROM FhrdLeaverule f WHERE f.cissuesrno = :cissuesrno"),
    @NamedQuery(name = "FhrdLeaverule.findByDefaultInheritanceFlag", query = "SELECT f FROM FhrdLeaverule f WHERE f.defaultInheritanceFlag = :defaultInheritanceFlag"),
    @NamedQuery(name = "FhrdLeaverule.findByCreatedt", query = "SELECT f FROM FhrdLeaverule f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdLeaverule.findByUpdatedt", query = "SELECT f FROM FhrdLeaverule f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdLeaverule.findBySystemUserid", query = "SELECT f FROM FhrdLeaverule f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdLeaverule.findByExportFlag", query = "SELECT f FROM FhrdLeaverule f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdLeaverule.findByImportFlag", query = "SELECT f FROM FhrdLeaverule f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdLeaverule.findByStartMonth", query = "SELECT f FROM FhrdLeaverule f WHERE f.startMonth = :startMonth"),
    @NamedQuery(name = "FhrdLeaverule.findByEndMonth", query = "SELECT f FROM FhrdLeaverule f WHERE f.endMonth = :endMonth"),
    @NamedQuery(name = "FhrdLeaverule.findByContrdueCarryForwrd", query = "SELECT f FROM FhrdLeaverule f WHERE f.contrdueCarryForwrd = :contrdueCarryForwrd"),
    @NamedQuery(name = "FhrdLeaverule.findByContrdueCarryForwrdMax", query = "SELECT f FROM FhrdLeaverule f WHERE f.contrdueCarryForwrdMax = :contrdueCarryForwrdMax"),
    @NamedQuery(name = "FhrdLeaverule.findByContrdueCarryforwrdRemaining", query = "SELECT f FROM FhrdLeaverule f WHERE f.contrdueCarryforwrdRemaining = :contrdueCarryforwrdRemaining"),
    @NamedQuery(name = "FhrdLeaverule.findByHalfdayApplicable", query = "SELECT f FROM FhrdLeaverule f WHERE f.halfdayApplicable = :halfdayApplicable"),
    @NamedQuery(name = "FhrdLeaverule.findByLrSrgKey", query = "SELECT f FROM FhrdLeaverule f WHERE f.lrSrgKey = :lrSrgKey"),
    @NamedQuery(name = "FhrdLeaverule.findByLmAltSrno", query = "SELECT f FROM FhrdLeaverule f WHERE f.lmAltSrno = :lmAltSrno"),
    @NamedQuery(name = "FhrdLeaverule.findByFixMonth", query = "SELECT f FROM FhrdLeaverule f WHERE f.fixMonth = :fixMonth"),
    @NamedQuery(name = "FhrdLeaverule.findByFreqConsFrom", query = "SELECT f FROM FhrdLeaverule f WHERE f.freqConsFrom = :freqConsFrom"),
    @NamedQuery(name = "FhrdLeaverule.findByFinaliseFlag", query = "SELECT f FROM FhrdLeaverule f WHERE f.finaliseFlag = :finaliseFlag"),
    @NamedQuery(name = "FhrdLeaverule.findByLeaveLapsInClubFlag", query = "SELECT f FROM FhrdLeaverule f WHERE f.leaveLapsInClubFlag = :leaveLapsInClubFlag"),
    @NamedQuery(name = "FhrdLeaverule.findByEligibilityApplicable", query = "SELECT f FROM FhrdLeaverule f WHERE f.eligibilityApplicable = :eligibilityApplicable"),
    @NamedQuery(name = "FhrdLeaverule.findByEligibilityPeriod", query = "SELECT f FROM FhrdLeaverule f WHERE f.eligibilityPeriod = :eligibilityPeriod"),
    @NamedQuery(name = "FhrdLeaverule.findByEligibilityUnit", query = "SELECT f FROM FhrdLeaverule f WHERE f.eligibilityUnit = :eligibilityUnit"),
    @NamedQuery(name = "FhrdLeaverule.findByEligibilityConsidered", query = "SELECT f FROM FhrdLeaverule f WHERE f.eligibilityConsidered = :eligibilityConsidered"),
    @NamedQuery(name = "FhrdLeaverule.findByGroupLeaveDesc", query = "SELECT f FROM FhrdLeaverule f WHERE f.groupLeaveDesc = :groupLeaveDesc"),
    @NamedQuery(name = "FhrdLeaverule.findByVirtualLeave", query = "SELECT f FROM FhrdLeaverule f WHERE f.virtualLeave = :virtualLeave"),
    @NamedQuery(name = "FhrdLeaverule.findBySystemUseridFlag", query = "SELECT f FROM FhrdLeaverule f WHERE f.systemUseridFlag = :systemUseridFlag"),
    @NamedQuery(name = "FhrdLeaverule.findByEarnIncludeFlag", query = "SELECT f FROM FhrdLeaverule f WHERE f.earnIncludeFlag = :earnIncludeFlag"),
    @NamedQuery(name = "FhrdLeaverule.findByMultipleRuleFlag", query = "SELECT f FROM FhrdLeaverule f WHERE f.multipleRuleFlag = :multipleRuleFlag"),
    @NamedQuery(name = "FhrdLeaverule.findByReqBalOnRelieve", query = "SELECT f FROM FhrdLeaverule f WHERE f.reqBalOnRelieve = :reqBalOnRelieve"),
    @NamedQuery(name = "FhrdLeaverule.findByExhaustOtherLeaveFlag", query = "SELECT f FROM FhrdLeaverule f WHERE f.exhaustOtherLeaveFlag = :exhaustOtherLeaveFlag"),
    @NamedQuery(name = "FhrdLeaverule.findByEncashAutoFlag", query = "SELECT f FROM FhrdLeaverule f WHERE f.encashAutoFlag = :encashAutoFlag"),
    @NamedQuery(name = "FhrdLeaverule.findByEncashAutoMonth", query = "SELECT f FROM FhrdLeaverule f WHERE f.encashAutoMonth = :encashAutoMonth"),
    @NamedQuery(name = "FhrdLeaverule.findByProportionateAtJoinOnly", query = "SELECT f FROM FhrdLeaverule f WHERE f.proportionateAtJoinOnly = :proportionateAtJoinOnly"),
    @NamedQuery(name = "FhrdLeaverule.findByContAllowedFlag", query = "SELECT f FROM FhrdLeaverule f WHERE f.contAllowedFlag = :contAllowedFlag"),
    @NamedQuery(name = "FhrdLeaverule.findByUserEntryFlag", query = "SELECT f FROM FhrdLeaverule f WHERE f.userEntryFlag = :userEntryFlag")})
public class FhrdLeaverule implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SRNO")
    private int srno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "LEAVECD")
    private String leavecd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WEF_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date wefDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "BALANCE_EARN_TYPE")
    private String balanceEarnType;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "NO_OF_LEAVE")
    private BigDecimal noOfLeave;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PROPORTIONATE_BALANCE")
    private String proportionateBalance;
    @Column(name = "PARENT_SRNO")
    private Integer parentSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "CARRY_FORWARD")
    private String carryForward;
    @Column(name = "CARRY_FORWARD_MAX")
    private BigDecimal carryForwardMax;
    @Size(max = 1)
    @Column(name = "CARRY_FORWARD_REMAINING")
    private String carryForwardRemaining;
    @Column(name = "DAYS_PER_LEAVE_BAL_COMPARISON")
    private BigDecimal daysPerLeaveBalComparison;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "BAL_CHECKED_IN_APPLI_FLAG")
    private String balCheckedInAppliFlag;
    @Size(max = 1)
    @Column(name = "PARENT_CARRY_FORWARD")
    private String parentCarryForward;
    @Column(name = "PARENT_CARRY_FORWARD_MAX")
    private BigDecimal parentCarryForwardMax;
    @Size(max = 1)
    @Column(name = "PARENT_CARRYFORWARD_REMAINING")
    private String parentCarryforwardRemaining;
    @Column(name = "PARENT_LEAVE_RATIO")
    private BigDecimal parentLeaveRatio;
    @Column(name = "DEDUCT_DAYS")
    private BigDecimal deductDays;
    @Column(name = "DEDUCT_LEAVES")
    private BigDecimal deductLeaves;
    @Column(name = "EARN_LEAVE_PERIOD")
    private BigDecimal earnLeavePeriod;
    @Size(max = 1)
    @Column(name = "EARN_LEAVE_PERIOD_UNIT")
    private String earnLeavePeriodUnit;
    @Column(name = "EARN_LEAVE_PER_PERIOD")
    private BigDecimal earnLeavePerPeriod;
    @Column(name = "ACCUMULATION")
    private BigDecimal accumulation;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "GENDER_FLAG")
    private String genderFlag;
    @Column(name = "MAX_SPECIAL_APPROVE")
    private BigDecimal maxSpecialApprove;
    @Column(name = "FREQ_PERIOD")
    private Integer freqPeriod;
    @Size(max = 1)
    @Column(name = "FREQ_PERIOD_UNIT")
    private String freqPeriodUnit;
    @Column(name = "FREQ_PER_PERIOD")
    private Integer freqPerPeriod;
    @Size(max = 50)
    @Column(name = "ENCASH_FORMULA")
    private String encashFormula;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "ENCASHMENT")
    private String encashment;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "CLUB_WITH_OTHER")
    private String clubWithOther;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EARN_CONFIG_FLAG")
    private String earnConfigFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "HOLIDAY_EMBEDDED")
    private String holidayEmbedded;
    @Size(max = 1)
    @Column(name = "COMMUTED")
    private String commuted;
    @Column(name = "MAX_TAKEN")
    private BigDecimal maxTaken;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MIN_TAKEN")
    private BigDecimal minTaken;
    @Size(max = 120)
    @Column(name = "LEAVE_FORMULA")
    private String leaveFormula;
    @Column(name = "LEAVE_BAL_DED_PER_LEAVE")
    private BigDecimal leaveBalDedPerLeave;
    @Size(max = 1)
    @Column(name = "PART_FULFILL_SUBSYS_FLAG")
    private String partFulfillSubsysFlag;
    @Column(name = "ENCASH_MIN_LEAVE")
    private BigDecimal encashMinLeave;
    @Column(name = "ENCASH_MAX_LEAVE")
    private BigDecimal encashMaxLeave;
    @Column(name = "ENCASH_EXTRA_MIN_LEAVE")
    private BigDecimal encashExtraMinLeave;
    @Column(name = "ENCASH_APPROVAL_MIN_LEAVE")
    private BigDecimal encashApprovalMinLeave;
    @Column(name = "ENCASH_FREQ")
    private BigDecimal encashFreq;
    @Column(name = "ENCASH_FREQ_PERIOD")
    private BigDecimal encashFreqPeriod;
    @Size(max = 1)
    @Column(name = "ENCASH_FREQ_PERIOD_UNIT")
    private String encashFreqPeriodUnit;
    @Column(name = "COMMUTE_MIN_LEAVE")
    private BigDecimal commuteMinLeave;
    @Column(name = "COMMUTE_DEDUCT_DAYS")
    private BigDecimal commuteDeductDays;
    @Column(name = "COMMUTE_DEDUCT_LEAVES")
    private BigDecimal commuteDeductLeaves;
    @Column(name = "LEAVE_BAL_DED_PER_COMUTLEAVE")
    private BigDecimal leaveBalDedPerComutleave;
    @Size(max = 255)
    @Column(name = "REMARKS")
    private String remarks;
    @Column(name = "COMMSRNO")
    private Integer commsrno;
    @Size(max = 1)
    @Column(name = "COMMFLAG")
    private String commflag;
    @Column(name = "CISSUESRNO")
    private Integer cissuesrno;
    @Size(max = 1)
    @Column(name = "DEFAULT_INHERITANCE_FLAG")
    private String defaultInheritanceFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Size(max = 3)
    @Column(name = "START_MONTH")
    private String startMonth;
    @Size(max = 3)
    @Column(name = "END_MONTH")
    private String endMonth;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "CONTRDUE_CARRY_FORWRD")
    private String contrdueCarryForwrd;
    @Column(name = "CONTRDUE_CARRY_FORWRD_MAX")
    private BigDecimal contrdueCarryForwrdMax;
    @Size(max = 1)
    @Column(name = "CONTRDUE_CARRYFORWRD_REMAINING")
    private String contrdueCarryforwrdRemaining;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "HALFDAY_APPLICABLE")
    private String halfdayApplicable;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "LR_SRG_KEY")
    private BigDecimal lrSrgKey;
    @Column(name = "LM_ALT_SRNO")
    private Integer lmAltSrno;
    @Size(max = 1)
    @Column(name = "FIX_MONTH")
    private String fixMonth;
    @Size(max = 1)
    @Column(name = "FREQ_CONS_FROM")
    private String freqConsFrom;
    @Size(max = 1)
    @Column(name = "FINALISE_FLAG")
    private String finaliseFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "LEAVE_LAPS_IN_CLUB_FLAG")
    private String leaveLapsInClubFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "ELIGIBILITY_APPLICABLE")
    private String eligibilityApplicable;
    @Column(name = "ELIGIBILITY_PERIOD")
    private Short eligibilityPeriod;
    @Size(max = 1)
    @Column(name = "ELIGIBILITY_UNIT")
    private String eligibilityUnit;
    @Size(max = 1)
    @Column(name = "ELIGIBILITY_CONSIDERED")
    private String eligibilityConsidered;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "GROUP_LEAVE_DESC")
    private String groupLeaveDesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "VIRTUAL_LEAVE")
    private String virtualLeave;
    @Size(max = 1)
    @Column(name = "SYSTEM_USERID_FLAG")
    private String systemUseridFlag;
    @Size(max = 1)
    @Column(name = "EARN_INCLUDE_FLAG")
    private String earnIncludeFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "MULTIPLE_RULE_FLAG")
    private String multipleRuleFlag;
    @Column(name = "REQ_BAL_ON_RELIEVE")
    private Integer reqBalOnRelieve;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EXHAUST_OTHER_LEAVE_FLAG")
    private String exhaustOtherLeaveFlag;
    @Size(max = 1)
    @Column(name = "ENCASH_AUTO_FLAG")
    private String encashAutoFlag;
    @Size(max = 3)
    @Column(name = "ENCASH_AUTO_MONTH")
    private String encashAutoMonth;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PROPORTIONATE_AT_JOIN_ONLY")
    private String proportionateAtJoinOnly;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "CONT_ALLOWED_FLAG")
    private String contAllowedFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "USER_ENTRY_FLAG")
    private String userEntryFlag;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lrSrgKey")
    private Collection<FhrdEmpleavebal> fhrdEmpleavebalCollection;
    @OneToMany(mappedBy = "parentLrSrgKey")
    private Collection<FhrdEmpleavebal> fhrdEmpleavebalCollection1;
    @OneToMany(mappedBy = "contrdueCarryLrSrgKey")
    private Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lrSrgKey")
    private Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection1;
    @OneToMany(mappedBy = "parentLrSrgKey")
    private Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lrSrgKey")
    private Collection<FhrdEmpEncash> fhrdEmpEncashCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne
    private FtasLeavemst lmSrgKey;
    @JoinColumn(name = "RMGM_SRG_KEY", referencedColumnName = "RMGM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdRuleManagGroupMst rmgmSrgKey;
    @OneToMany(mappedBy = "parentLrSrgKey")
    private Collection<FhrdLeaverule> fhrdLeaveruleCollection;
    @JoinColumn(name = "PARENT_LR_SRG_KEY", referencedColumnName = "LR_SRG_KEY")
    @ManyToOne
    private FhrdLeaverule parentLrSrgKey;
    @OneToMany(mappedBy = "contrdueCarryLrSrgKey")
    private Collection<FhrdLeaverule> fhrdLeaveruleCollection1;
    @JoinColumn(name = "CONTRDUE_CARRY_LR_SRG_KEY", referencedColumnName = "LR_SRG_KEY")
    @ManyToOne
    private FhrdLeaverule contrdueCarryLrSrgKey;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FhrdLeaverule() {
    }

    public FhrdLeaverule(BigDecimal lrSrgKey) {
        this.lrSrgKey = lrSrgKey;
    }

    public FhrdLeaverule(BigDecimal lrSrgKey, int srno, String leavecd, Date startDate, Date wefDate, String balanceEarnType, String proportionateBalance, String carryForward, String balCheckedInAppliFlag, String genderFlag, String encashment, String clubWithOther, String earnConfigFlag, String holidayEmbedded, BigDecimal minTaken, Date createdt, String contrdueCarryForwrd, String halfdayApplicable, String leaveLapsInClubFlag, String eligibilityApplicable, String groupLeaveDesc, String virtualLeave, String multipleRuleFlag, String exhaustOtherLeaveFlag, String proportionateAtJoinOnly, String contAllowedFlag, String userEntryFlag) {
        this.lrSrgKey = lrSrgKey;
        this.srno = srno;
        this.leavecd = leavecd;
        this.startDate = startDate;
        this.wefDate = wefDate;
        this.balanceEarnType = balanceEarnType;
        this.proportionateBalance = proportionateBalance;
        this.carryForward = carryForward;
        this.balCheckedInAppliFlag = balCheckedInAppliFlag;
        this.genderFlag = genderFlag;
        this.encashment = encashment;
        this.clubWithOther = clubWithOther;
        this.earnConfigFlag = earnConfigFlag;
        this.holidayEmbedded = holidayEmbedded;
        this.minTaken = minTaken;
        this.createdt = createdt;
        this.contrdueCarryForwrd = contrdueCarryForwrd;
        this.halfdayApplicable = halfdayApplicable;
        this.leaveLapsInClubFlag = leaveLapsInClubFlag;
        this.eligibilityApplicable = eligibilityApplicable;
        this.groupLeaveDesc = groupLeaveDesc;
        this.virtualLeave = virtualLeave;
        this.multipleRuleFlag = multipleRuleFlag;
        this.exhaustOtherLeaveFlag = exhaustOtherLeaveFlag;
        this.proportionateAtJoinOnly = proportionateAtJoinOnly;
        this.contAllowedFlag = contAllowedFlag;
        this.userEntryFlag = userEntryFlag;
    }

    public int getSrno() {
        return srno;
    }

    public void setSrno(int srno) {
        this.srno = srno;
    }

    public String getLeavecd() {
        return leavecd;
    }

    public void setLeavecd(String leavecd) {
        this.leavecd = leavecd;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getWefDate() {
        return wefDate;
    }

    public void setWefDate(Date wefDate) {
        this.wefDate = wefDate;
    }

    public String getBalanceEarnType() {
        return balanceEarnType;
    }

    public void setBalanceEarnType(String balanceEarnType) {
        this.balanceEarnType = balanceEarnType;
    }

    public BigDecimal getNoOfLeave() {
        return noOfLeave;
    }

    public void setNoOfLeave(BigDecimal noOfLeave) {
        this.noOfLeave = noOfLeave;
    }

    public String getProportionateBalance() {
        return proportionateBalance;
    }

    public void setProportionateBalance(String proportionateBalance) {
        this.proportionateBalance = proportionateBalance;
    }

    public Integer getParentSrno() {
        return parentSrno;
    }

    public void setParentSrno(Integer parentSrno) {
        this.parentSrno = parentSrno;
    }

    public String getCarryForward() {
        return carryForward;
    }

    public void setCarryForward(String carryForward) {
        this.carryForward = carryForward;
    }

    public BigDecimal getCarryForwardMax() {
        return carryForwardMax;
    }

    public void setCarryForwardMax(BigDecimal carryForwardMax) {
        this.carryForwardMax = carryForwardMax;
    }

    public String getCarryForwardRemaining() {
        return carryForwardRemaining;
    }

    public void setCarryForwardRemaining(String carryForwardRemaining) {
        this.carryForwardRemaining = carryForwardRemaining;
    }

    public BigDecimal getDaysPerLeaveBalComparison() {
        return daysPerLeaveBalComparison;
    }

    public void setDaysPerLeaveBalComparison(BigDecimal daysPerLeaveBalComparison) {
        this.daysPerLeaveBalComparison = daysPerLeaveBalComparison;
    }

    public String getBalCheckedInAppliFlag() {
        return balCheckedInAppliFlag;
    }

    public void setBalCheckedInAppliFlag(String balCheckedInAppliFlag) {
        this.balCheckedInAppliFlag = balCheckedInAppliFlag;
    }

    public String getParentCarryForward() {
        return parentCarryForward;
    }

    public void setParentCarryForward(String parentCarryForward) {
        this.parentCarryForward = parentCarryForward;
    }

    public BigDecimal getParentCarryForwardMax() {
        return parentCarryForwardMax;
    }

    public void setParentCarryForwardMax(BigDecimal parentCarryForwardMax) {
        this.parentCarryForwardMax = parentCarryForwardMax;
    }

    public String getParentCarryforwardRemaining() {
        return parentCarryforwardRemaining;
    }

    public void setParentCarryforwardRemaining(String parentCarryforwardRemaining) {
        this.parentCarryforwardRemaining = parentCarryforwardRemaining;
    }

    public BigDecimal getParentLeaveRatio() {
        return parentLeaveRatio;
    }

    public void setParentLeaveRatio(BigDecimal parentLeaveRatio) {
        this.parentLeaveRatio = parentLeaveRatio;
    }

    public BigDecimal getDeductDays() {
        return deductDays;
    }

    public void setDeductDays(BigDecimal deductDays) {
        this.deductDays = deductDays;
    }

    public BigDecimal getDeductLeaves() {
        return deductLeaves;
    }

    public void setDeductLeaves(BigDecimal deductLeaves) {
        this.deductLeaves = deductLeaves;
    }

    public BigDecimal getEarnLeavePeriod() {
        return earnLeavePeriod;
    }

    public void setEarnLeavePeriod(BigDecimal earnLeavePeriod) {
        this.earnLeavePeriod = earnLeavePeriod;
    }

    public String getEarnLeavePeriodUnit() {
        return earnLeavePeriodUnit;
    }

    public void setEarnLeavePeriodUnit(String earnLeavePeriodUnit) {
        this.earnLeavePeriodUnit = earnLeavePeriodUnit;
    }

    public BigDecimal getEarnLeavePerPeriod() {
        return earnLeavePerPeriod;
    }

    public void setEarnLeavePerPeriod(BigDecimal earnLeavePerPeriod) {
        this.earnLeavePerPeriod = earnLeavePerPeriod;
    }

    public BigDecimal getAccumulation() {
        return accumulation;
    }

    public void setAccumulation(BigDecimal accumulation) {
        this.accumulation = accumulation;
    }

    public String getGenderFlag() {
        return genderFlag;
    }

    public void setGenderFlag(String genderFlag) {
        this.genderFlag = genderFlag;
    }

    public BigDecimal getMaxSpecialApprove() {
        return maxSpecialApprove;
    }

    public void setMaxSpecialApprove(BigDecimal maxSpecialApprove) {
        this.maxSpecialApprove = maxSpecialApprove;
    }

    public Integer getFreqPeriod() {
        return freqPeriod;
    }

    public void setFreqPeriod(Integer freqPeriod) {
        this.freqPeriod = freqPeriod;
    }

    public String getFreqPeriodUnit() {
        return freqPeriodUnit;
    }

    public void setFreqPeriodUnit(String freqPeriodUnit) {
        this.freqPeriodUnit = freqPeriodUnit;
    }

    public Integer getFreqPerPeriod() {
        return freqPerPeriod;
    }

    public void setFreqPerPeriod(Integer freqPerPeriod) {
        this.freqPerPeriod = freqPerPeriod;
    }

    public String getEncashFormula() {
        return encashFormula;
    }

    public void setEncashFormula(String encashFormula) {
        this.encashFormula = encashFormula;
    }

    public String getEncashment() {
        return encashment;
    }

    public void setEncashment(String encashment) {
        this.encashment = encashment;
    }

    public String getClubWithOther() {
        return clubWithOther;
    }

    public void setClubWithOther(String clubWithOther) {
        this.clubWithOther = clubWithOther;
    }

    public String getEarnConfigFlag() {
        return earnConfigFlag;
    }

    public void setEarnConfigFlag(String earnConfigFlag) {
        this.earnConfigFlag = earnConfigFlag;
    }

    public String getHolidayEmbedded() {
        return holidayEmbedded;
    }

    public void setHolidayEmbedded(String holidayEmbedded) {
        this.holidayEmbedded = holidayEmbedded;
    }

    public String getCommuted() {
        return commuted;
    }

    public void setCommuted(String commuted) {
        this.commuted = commuted;
    }

    public BigDecimal getMaxTaken() {
        return maxTaken;
    }

    public void setMaxTaken(BigDecimal maxTaken) {
        this.maxTaken = maxTaken;
    }

    public BigDecimal getMinTaken() {
        return minTaken;
    }

    public void setMinTaken(BigDecimal minTaken) {
        this.minTaken = minTaken;
    }

    public String getLeaveFormula() {
        return leaveFormula;
    }

    public void setLeaveFormula(String leaveFormula) {
        this.leaveFormula = leaveFormula;
    }

    public BigDecimal getLeaveBalDedPerLeave() {
        return leaveBalDedPerLeave;
    }

    public void setLeaveBalDedPerLeave(BigDecimal leaveBalDedPerLeave) {
        this.leaveBalDedPerLeave = leaveBalDedPerLeave;
    }

    public String getPartFulfillSubsysFlag() {
        return partFulfillSubsysFlag;
    }

    public void setPartFulfillSubsysFlag(String partFulfillSubsysFlag) {
        this.partFulfillSubsysFlag = partFulfillSubsysFlag;
    }

    public BigDecimal getEncashMinLeave() {
        return encashMinLeave;
    }

    public void setEncashMinLeave(BigDecimal encashMinLeave) {
        this.encashMinLeave = encashMinLeave;
    }

    public BigDecimal getEncashMaxLeave() {
        return encashMaxLeave;
    }

    public void setEncashMaxLeave(BigDecimal encashMaxLeave) {
        this.encashMaxLeave = encashMaxLeave;
    }

    public BigDecimal getEncashExtraMinLeave() {
        return encashExtraMinLeave;
    }

    public void setEncashExtraMinLeave(BigDecimal encashExtraMinLeave) {
        this.encashExtraMinLeave = encashExtraMinLeave;
    }

    public BigDecimal getEncashApprovalMinLeave() {
        return encashApprovalMinLeave;
    }

    public void setEncashApprovalMinLeave(BigDecimal encashApprovalMinLeave) {
        this.encashApprovalMinLeave = encashApprovalMinLeave;
    }

    public BigDecimal getEncashFreq() {
        return encashFreq;
    }

    public void setEncashFreq(BigDecimal encashFreq) {
        this.encashFreq = encashFreq;
    }

    public BigDecimal getEncashFreqPeriod() {
        return encashFreqPeriod;
    }

    public void setEncashFreqPeriod(BigDecimal encashFreqPeriod) {
        this.encashFreqPeriod = encashFreqPeriod;
    }

    public String getEncashFreqPeriodUnit() {
        return encashFreqPeriodUnit;
    }

    public void setEncashFreqPeriodUnit(String encashFreqPeriodUnit) {
        this.encashFreqPeriodUnit = encashFreqPeriodUnit;
    }

    public BigDecimal getCommuteMinLeave() {
        return commuteMinLeave;
    }

    public void setCommuteMinLeave(BigDecimal commuteMinLeave) {
        this.commuteMinLeave = commuteMinLeave;
    }

    public BigDecimal getCommuteDeductDays() {
        return commuteDeductDays;
    }

    public void setCommuteDeductDays(BigDecimal commuteDeductDays) {
        this.commuteDeductDays = commuteDeductDays;
    }

    public BigDecimal getCommuteDeductLeaves() {
        return commuteDeductLeaves;
    }

    public void setCommuteDeductLeaves(BigDecimal commuteDeductLeaves) {
        this.commuteDeductLeaves = commuteDeductLeaves;
    }

    public BigDecimal getLeaveBalDedPerComutleave() {
        return leaveBalDedPerComutleave;
    }

    public void setLeaveBalDedPerComutleave(BigDecimal leaveBalDedPerComutleave) {
        this.leaveBalDedPerComutleave = leaveBalDedPerComutleave;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getCommsrno() {
        return commsrno;
    }

    public void setCommsrno(Integer commsrno) {
        this.commsrno = commsrno;
    }

    public String getCommflag() {
        return commflag;
    }

    public void setCommflag(String commflag) {
        this.commflag = commflag;
    }

    public Integer getCissuesrno() {
        return cissuesrno;
    }

    public void setCissuesrno(Integer cissuesrno) {
        this.cissuesrno = cissuesrno;
    }

    public String getDefaultInheritanceFlag() {
        return defaultInheritanceFlag;
    }

    public void setDefaultInheritanceFlag(String defaultInheritanceFlag) {
        this.defaultInheritanceFlag = defaultInheritanceFlag;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public String getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(String startMonth) {
        this.startMonth = startMonth;
    }

    public String getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(String endMonth) {
        this.endMonth = endMonth;
    }

    public String getContrdueCarryForwrd() {
        return contrdueCarryForwrd;
    }

    public void setContrdueCarryForwrd(String contrdueCarryForwrd) {
        this.contrdueCarryForwrd = contrdueCarryForwrd;
    }

    public BigDecimal getContrdueCarryForwrdMax() {
        return contrdueCarryForwrdMax;
    }

    public void setContrdueCarryForwrdMax(BigDecimal contrdueCarryForwrdMax) {
        this.contrdueCarryForwrdMax = contrdueCarryForwrdMax;
    }

    public String getContrdueCarryforwrdRemaining() {
        return contrdueCarryforwrdRemaining;
    }

    public void setContrdueCarryforwrdRemaining(String contrdueCarryforwrdRemaining) {
        this.contrdueCarryforwrdRemaining = contrdueCarryforwrdRemaining;
    }

    public String getHalfdayApplicable() {
        return halfdayApplicable;
    }

    public void setHalfdayApplicable(String halfdayApplicable) {
        this.halfdayApplicable = halfdayApplicable;
    }

    public BigDecimal getLrSrgKey() {
        return lrSrgKey;
    }

    public void setLrSrgKey(BigDecimal lrSrgKey) {
        this.lrSrgKey = lrSrgKey;
    }

    public Integer getLmAltSrno() {
        return lmAltSrno;
    }

    public void setLmAltSrno(Integer lmAltSrno) {
        this.lmAltSrno = lmAltSrno;
    }

    public String getFixMonth() {
        return fixMonth;
    }

    public void setFixMonth(String fixMonth) {
        this.fixMonth = fixMonth;
    }

    public String getFreqConsFrom() {
        return freqConsFrom;
    }

    public void setFreqConsFrom(String freqConsFrom) {
        this.freqConsFrom = freqConsFrom;
    }

    public String getFinaliseFlag() {
        return finaliseFlag;
    }

    public void setFinaliseFlag(String finaliseFlag) {
        this.finaliseFlag = finaliseFlag;
    }

    public String getLeaveLapsInClubFlag() {
        return leaveLapsInClubFlag;
    }

    public void setLeaveLapsInClubFlag(String leaveLapsInClubFlag) {
        this.leaveLapsInClubFlag = leaveLapsInClubFlag;
    }

    public String getEligibilityApplicable() {
        return eligibilityApplicable;
    }

    public void setEligibilityApplicable(String eligibilityApplicable) {
        this.eligibilityApplicable = eligibilityApplicable;
    }

    public Short getEligibilityPeriod() {
        return eligibilityPeriod;
    }

    public void setEligibilityPeriod(Short eligibilityPeriod) {
        this.eligibilityPeriod = eligibilityPeriod;
    }

    public String getEligibilityUnit() {
        return eligibilityUnit;
    }

    public void setEligibilityUnit(String eligibilityUnit) {
        this.eligibilityUnit = eligibilityUnit;
    }

    public String getEligibilityConsidered() {
        return eligibilityConsidered;
    }

    public void setEligibilityConsidered(String eligibilityConsidered) {
        this.eligibilityConsidered = eligibilityConsidered;
    }

    public String getGroupLeaveDesc() {
        return groupLeaveDesc;
    }

    public void setGroupLeaveDesc(String groupLeaveDesc) {
        this.groupLeaveDesc = groupLeaveDesc;
    }

    public String getVirtualLeave() {
        return virtualLeave;
    }

    public void setVirtualLeave(String virtualLeave) {
        this.virtualLeave = virtualLeave;
    }

    public String getSystemUseridFlag() {
        return systemUseridFlag;
    }

    public void setSystemUseridFlag(String systemUseridFlag) {
        this.systemUseridFlag = systemUseridFlag;
    }

    public String getEarnIncludeFlag() {
        return earnIncludeFlag;
    }

    public void setEarnIncludeFlag(String earnIncludeFlag) {
        this.earnIncludeFlag = earnIncludeFlag;
    }

    public String getMultipleRuleFlag() {
        return multipleRuleFlag;
    }

    public void setMultipleRuleFlag(String multipleRuleFlag) {
        this.multipleRuleFlag = multipleRuleFlag;
    }

    public Integer getReqBalOnRelieve() {
        return reqBalOnRelieve;
    }

    public void setReqBalOnRelieve(Integer reqBalOnRelieve) {
        this.reqBalOnRelieve = reqBalOnRelieve;
    }

    public String getExhaustOtherLeaveFlag() {
        return exhaustOtherLeaveFlag;
    }

    public void setExhaustOtherLeaveFlag(String exhaustOtherLeaveFlag) {
        this.exhaustOtherLeaveFlag = exhaustOtherLeaveFlag;
    }

    public String getEncashAutoFlag() {
        return encashAutoFlag;
    }

    public void setEncashAutoFlag(String encashAutoFlag) {
        this.encashAutoFlag = encashAutoFlag;
    }

    public String getEncashAutoMonth() {
        return encashAutoMonth;
    }

    public void setEncashAutoMonth(String encashAutoMonth) {
        this.encashAutoMonth = encashAutoMonth;
    }

    public String getProportionateAtJoinOnly() {
        return proportionateAtJoinOnly;
    }

    public void setProportionateAtJoinOnly(String proportionateAtJoinOnly) {
        this.proportionateAtJoinOnly = proportionateAtJoinOnly;
    }

    public String getContAllowedFlag() {
        return contAllowedFlag;
    }

    public void setContAllowedFlag(String contAllowedFlag) {
        this.contAllowedFlag = contAllowedFlag;
    }

    public String getUserEntryFlag() {
        return userEntryFlag;
    }

    public void setUserEntryFlag(String userEntryFlag) {
        this.userEntryFlag = userEntryFlag;
    }

    @XmlTransient
    public Collection<FhrdEmpleavebal> getFhrdEmpleavebalCollection() {
        return fhrdEmpleavebalCollection;
    }

    public void setFhrdEmpleavebalCollection(Collection<FhrdEmpleavebal> fhrdEmpleavebalCollection) {
        this.fhrdEmpleavebalCollection = fhrdEmpleavebalCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpleavebal> getFhrdEmpleavebalCollection1() {
        return fhrdEmpleavebalCollection1;
    }

    public void setFhrdEmpleavebalCollection1(Collection<FhrdEmpleavebal> fhrdEmpleavebalCollection1) {
        this.fhrdEmpleavebalCollection1 = fhrdEmpleavebalCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpleaverule> getFhrdEmpleaveruleCollection() {
        return fhrdEmpleaveruleCollection;
    }

    public void setFhrdEmpleaveruleCollection(Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection) {
        this.fhrdEmpleaveruleCollection = fhrdEmpleaveruleCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpleaverule> getFhrdEmpleaveruleCollection1() {
        return fhrdEmpleaveruleCollection1;
    }

    public void setFhrdEmpleaveruleCollection1(Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection1) {
        this.fhrdEmpleaveruleCollection1 = fhrdEmpleaveruleCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpleaverule> getFhrdEmpleaveruleCollection2() {
        return fhrdEmpleaveruleCollection2;
    }

    public void setFhrdEmpleaveruleCollection2(Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection2) {
        this.fhrdEmpleaveruleCollection2 = fhrdEmpleaveruleCollection2;
    }

    @XmlTransient
    public Collection<FhrdEmpEncash> getFhrdEmpEncashCollection() {
        return fhrdEmpEncashCollection;
    }

    public void setFhrdEmpEncashCollection(Collection<FhrdEmpEncash> fhrdEmpEncashCollection) {
        this.fhrdEmpEncashCollection = fhrdEmpEncashCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasLeavemst getLmSrgKey() {
        return lmSrgKey;
    }

    public void setLmSrgKey(FtasLeavemst lmSrgKey) {
        this.lmSrgKey = lmSrgKey;
    }

    public FhrdRuleManagGroupMst getRmgmSrgKey() {
        return rmgmSrgKey;
    }

    public void setRmgmSrgKey(FhrdRuleManagGroupMst rmgmSrgKey) {
        this.rmgmSrgKey = rmgmSrgKey;
    }

    @XmlTransient
    public Collection<FhrdLeaverule> getFhrdLeaveruleCollection() {
        return fhrdLeaveruleCollection;
    }

    public void setFhrdLeaveruleCollection(Collection<FhrdLeaverule> fhrdLeaveruleCollection) {
        this.fhrdLeaveruleCollection = fhrdLeaveruleCollection;
    }

    public FhrdLeaverule getParentLrSrgKey() {
        return parentLrSrgKey;
    }

    public void setParentLrSrgKey(FhrdLeaverule parentLrSrgKey) {
        this.parentLrSrgKey = parentLrSrgKey;
    }

    @XmlTransient
    public Collection<FhrdLeaverule> getFhrdLeaveruleCollection1() {
        return fhrdLeaveruleCollection1;
    }

    public void setFhrdLeaveruleCollection1(Collection<FhrdLeaverule> fhrdLeaveruleCollection1) {
        this.fhrdLeaveruleCollection1 = fhrdLeaveruleCollection1;
    }

    public FhrdLeaverule getContrdueCarryLrSrgKey() {
        return contrdueCarryLrSrgKey;
    }

    public void setContrdueCarryLrSrgKey(FhrdLeaverule contrdueCarryLrSrgKey) {
        this.contrdueCarryLrSrgKey = contrdueCarryLrSrgKey;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lrSrgKey != null ? lrSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdLeaverule)) {
            return false;
        }
        FhrdLeaverule other = (FhrdLeaverule) object;
        if ((this.lrSrgKey == null && other.lrSrgKey != null) || (this.lrSrgKey != null && !this.lrSrgKey.equals(other.lrSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdLeaverule[ lrSrgKey=" + lrSrgKey + " ]";
    }
    
}
