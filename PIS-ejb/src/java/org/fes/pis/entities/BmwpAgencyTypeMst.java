/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "BMWP_AGENCY_TYPE_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmwpAgencyTypeMst.findAll", query = "SELECT b FROM BmwpAgencyTypeMst b"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmUniqueSrno", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmUniqueSrno = :atmUniqueSrno"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmSrno", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmSrno = :atmSrno"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmShortDesc", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmShortDesc = :atmShortDesc"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmTypeDesc", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmTypeDesc = :atmTypeDesc"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByCreatedt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.createdt = :createdt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByUpdatedt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.updatedt = :updatedt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findBySystemUserid", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.systemUserid = :systemUserid"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByExportFlag", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.exportFlag = :exportFlag"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByImportFlag", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.importFlag = :importFlag"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmMouFundAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmMouFundAmt = :atmMouFundAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmReceivedFundAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmReceivedFundAmt = :atmReceivedFundAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmToReceivedFundAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmToReceivedFundAmt = :atmToReceivedFundAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmSurrenderFundAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmSurrenderFundAmt = :atmSurrenderFundAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmTotalFundAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmTotalFundAmt = :atmTotalFundAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmWorkplanAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmWorkplanAmt = :atmWorkplanAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmWpSurrenderAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmWpSurrenderAmt = :atmWpSurrenderAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmReAllotWpAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmReAllotWpAmt = :atmReAllotWpAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmOfferWpAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmOfferWpAmt = :atmOfferWpAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmReviseWpAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmReviseWpAmt = :atmReviseWpAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmCommitedAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmCommitedAmt = :atmCommitedAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmPipelinePayAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmPipelinePayAmt = :atmPipelinePayAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmAdvanceCommitAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmAdvanceCommitAmt = :atmAdvanceCommitAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmAdvancePayAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmAdvancePayAmt = :atmAdvancePayAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmPipeAdvadjCommitAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmPipeAdvadjCommitAmt = :atmPipeAdvadjCommitAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmPipeAdvadjPayAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmPipeAdvadjPayAmt = :atmPipeAdvadjPayAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmReceiptPayAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmReceiptPayAmt = :atmReceiptPayAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmReceiptCommitAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmReceiptCommitAmt = :atmReceiptCommitAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmExpenseAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmExpenseAmt = :atmExpenseAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmUsableBalanceAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmUsableBalanceAmt = :atmUsableBalanceAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmBalanceAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmBalanceAmt = :atmBalanceAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmPipeCommitedAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmPipeCommitedAmt = :atmPipeCommitedAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmStartDate", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmStartDate = :atmStartDate"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmEndDate", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmEndDate = :atmEndDate"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmEndDatePossible", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmEndDatePossible = :atmEndDatePossible"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmTransactionDate", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmTransactionDate = :atmTransactionDate"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmProCrExpAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmProCrExpAmt = :atmProCrExpAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmReceivedAdvanceAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmReceivedAdvanceAmt = :atmReceivedAdvanceAmt"),
    @NamedQuery(name = "BmwpAgencyTypeMst.findByAtmCarryFwdAdvAmt", query = "SELECT b FROM BmwpAgencyTypeMst b WHERE b.atmCarryFwdAdvAmt = :atmCarryFwdAdvAmt")})
public class BmwpAgencyTypeMst implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_UNIQUE_SRNO")
    private BigDecimal atmUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_SRNO")
    private int atmSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "ATM_SHORT_DESC")
    private String atmShortDesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "ATM_TYPE_DESC")
    private String atmTypeDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_MOU_FUND_AMT")
    private BigDecimal atmMouFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_RECEIVED_FUND_AMT")
    private BigDecimal atmReceivedFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_TO_RECEIVED_FUND_AMT")
    private BigDecimal atmToReceivedFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_SURRENDER_FUND_AMT")
    private BigDecimal atmSurrenderFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_TOTAL_FUND_AMT")
    private BigDecimal atmTotalFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_WORKPLAN_AMT")
    private BigDecimal atmWorkplanAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_WP_SURRENDER_AMT")
    private BigDecimal atmWpSurrenderAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_RE_ALLOT_WP_AMT")
    private BigDecimal atmReAllotWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_OFFER_WP_AMT")
    private BigDecimal atmOfferWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_REVISE_WP_AMT")
    private BigDecimal atmReviseWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_COMMITED_AMT")
    private BigDecimal atmCommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_PIPELINE_PAY_AMT")
    private BigDecimal atmPipelinePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_ADVANCE_COMMIT_AMT")
    private BigDecimal atmAdvanceCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_ADVANCE_PAY_AMT")
    private BigDecimal atmAdvancePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_PIPE_ADVADJ_COMMIT_AMT")
    private BigDecimal atmPipeAdvadjCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_PIPE_ADVADJ_PAY_AMT")
    private BigDecimal atmPipeAdvadjPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_RECEIPT_PAY_AMT")
    private BigDecimal atmReceiptPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_RECEIPT_COMMIT_AMT")
    private BigDecimal atmReceiptCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_EXPENSE_AMT")
    private BigDecimal atmExpenseAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_USABLE_BALANCE_AMT")
    private BigDecimal atmUsableBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_BALANCE_AMT")
    private BigDecimal atmBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_PIPE_COMMITED_AMT")
    private BigDecimal atmPipeCommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date atmStartDate;
    @Column(name = "ATM_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date atmEndDate;
    @Column(name = "ATM_END_DATE_POSSIBLE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date atmEndDatePossible;
    @Column(name = "ATM_TRANSACTION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date atmTransactionDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_PRO_CR_EXP_AMT")
    private BigDecimal atmProCrExpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_RECEIVED_ADVANCE_AMT")
    private BigDecimal atmReceivedAdvanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATM_CARRY_FWD_ADV_AMT")
    private BigDecimal atmCarryFwdAdvAmt;
    @OneToMany(mappedBy = "apablAtmUniqueSrno")
    private Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection;
    @OneToMany(mappedBy = "aplAtmUniqueSrno")
    private Collection<BmwpAgencyProjectLink> bmwpAgencyProjectLinkCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "famAtmUniqueSrno")
    private Collection<BmwpFundAgencyMst> bmwpFundAgencyMstCollection;
    @OneToMany(mappedBy = "apabdAtmUniqueSrno")
    private Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @OneToMany(mappedBy = "apamAtmUniqueSrno")
    private Collection<BmwpApActivityMst> bmwpApActivityMstCollection;
    @OneToMany(mappedBy = "apbmAtmUniqueSrno")
    private Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection;
    @OneToMany(mappedBy = "wpAtmUniqueSrno")
    private Collection<BmwpWorkplan> bmwpWorkplanCollection;

    public BmwpAgencyTypeMst() {
    }

    public BmwpAgencyTypeMst(BigDecimal atmUniqueSrno) {
        this.atmUniqueSrno = atmUniqueSrno;
    }

    public BmwpAgencyTypeMst(BigDecimal atmUniqueSrno, int atmSrno, String atmShortDesc, String atmTypeDesc, Date createdt, BigDecimal atmMouFundAmt, BigDecimal atmReceivedFundAmt, BigDecimal atmToReceivedFundAmt, BigDecimal atmSurrenderFundAmt, BigDecimal atmTotalFundAmt, BigDecimal atmWorkplanAmt, BigDecimal atmWpSurrenderAmt, BigDecimal atmReAllotWpAmt, BigDecimal atmOfferWpAmt, BigDecimal atmReviseWpAmt, BigDecimal atmCommitedAmt, BigDecimal atmPipelinePayAmt, BigDecimal atmAdvanceCommitAmt, BigDecimal atmAdvancePayAmt, BigDecimal atmPipeAdvadjCommitAmt, BigDecimal atmPipeAdvadjPayAmt, BigDecimal atmReceiptPayAmt, BigDecimal atmReceiptCommitAmt, BigDecimal atmExpenseAmt, BigDecimal atmUsableBalanceAmt, BigDecimal atmBalanceAmt, BigDecimal atmPipeCommitedAmt, Date atmStartDate, BigDecimal atmProCrExpAmt, BigDecimal atmReceivedAdvanceAmt, BigDecimal atmCarryFwdAdvAmt) {
        this.atmUniqueSrno = atmUniqueSrno;
        this.atmSrno = atmSrno;
        this.atmShortDesc = atmShortDesc;
        this.atmTypeDesc = atmTypeDesc;
        this.createdt = createdt;
        this.atmMouFundAmt = atmMouFundAmt;
        this.atmReceivedFundAmt = atmReceivedFundAmt;
        this.atmToReceivedFundAmt = atmToReceivedFundAmt;
        this.atmSurrenderFundAmt = atmSurrenderFundAmt;
        this.atmTotalFundAmt = atmTotalFundAmt;
        this.atmWorkplanAmt = atmWorkplanAmt;
        this.atmWpSurrenderAmt = atmWpSurrenderAmt;
        this.atmReAllotWpAmt = atmReAllotWpAmt;
        this.atmOfferWpAmt = atmOfferWpAmt;
        this.atmReviseWpAmt = atmReviseWpAmt;
        this.atmCommitedAmt = atmCommitedAmt;
        this.atmPipelinePayAmt = atmPipelinePayAmt;
        this.atmAdvanceCommitAmt = atmAdvanceCommitAmt;
        this.atmAdvancePayAmt = atmAdvancePayAmt;
        this.atmPipeAdvadjCommitAmt = atmPipeAdvadjCommitAmt;
        this.atmPipeAdvadjPayAmt = atmPipeAdvadjPayAmt;
        this.atmReceiptPayAmt = atmReceiptPayAmt;
        this.atmReceiptCommitAmt = atmReceiptCommitAmt;
        this.atmExpenseAmt = atmExpenseAmt;
        this.atmUsableBalanceAmt = atmUsableBalanceAmt;
        this.atmBalanceAmt = atmBalanceAmt;
        this.atmPipeCommitedAmt = atmPipeCommitedAmt;
        this.atmStartDate = atmStartDate;
        this.atmProCrExpAmt = atmProCrExpAmt;
        this.atmReceivedAdvanceAmt = atmReceivedAdvanceAmt;
        this.atmCarryFwdAdvAmt = atmCarryFwdAdvAmt;
    }

    public BigDecimal getAtmUniqueSrno() {
        return atmUniqueSrno;
    }

    public void setAtmUniqueSrno(BigDecimal atmUniqueSrno) {
        this.atmUniqueSrno = atmUniqueSrno;
    }

    public int getAtmSrno() {
        return atmSrno;
    }

    public void setAtmSrno(int atmSrno) {
        this.atmSrno = atmSrno;
    }

    public String getAtmShortDesc() {
        return atmShortDesc;
    }

    public void setAtmShortDesc(String atmShortDesc) {
        this.atmShortDesc = atmShortDesc;
    }

    public String getAtmTypeDesc() {
        return atmTypeDesc;
    }

    public void setAtmTypeDesc(String atmTypeDesc) {
        this.atmTypeDesc = atmTypeDesc;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public BigDecimal getAtmMouFundAmt() {
        return atmMouFundAmt;
    }

    public void setAtmMouFundAmt(BigDecimal atmMouFundAmt) {
        this.atmMouFundAmt = atmMouFundAmt;
    }

    public BigDecimal getAtmReceivedFundAmt() {
        return atmReceivedFundAmt;
    }

    public void setAtmReceivedFundAmt(BigDecimal atmReceivedFundAmt) {
        this.atmReceivedFundAmt = atmReceivedFundAmt;
    }

    public BigDecimal getAtmToReceivedFundAmt() {
        return atmToReceivedFundAmt;
    }

    public void setAtmToReceivedFundAmt(BigDecimal atmToReceivedFundAmt) {
        this.atmToReceivedFundAmt = atmToReceivedFundAmt;
    }

    public BigDecimal getAtmSurrenderFundAmt() {
        return atmSurrenderFundAmt;
    }

    public void setAtmSurrenderFundAmt(BigDecimal atmSurrenderFundAmt) {
        this.atmSurrenderFundAmt = atmSurrenderFundAmt;
    }

    public BigDecimal getAtmTotalFundAmt() {
        return atmTotalFundAmt;
    }

    public void setAtmTotalFundAmt(BigDecimal atmTotalFundAmt) {
        this.atmTotalFundAmt = atmTotalFundAmt;
    }

    public BigDecimal getAtmWorkplanAmt() {
        return atmWorkplanAmt;
    }

    public void setAtmWorkplanAmt(BigDecimal atmWorkplanAmt) {
        this.atmWorkplanAmt = atmWorkplanAmt;
    }

    public BigDecimal getAtmWpSurrenderAmt() {
        return atmWpSurrenderAmt;
    }

    public void setAtmWpSurrenderAmt(BigDecimal atmWpSurrenderAmt) {
        this.atmWpSurrenderAmt = atmWpSurrenderAmt;
    }

    public BigDecimal getAtmReAllotWpAmt() {
        return atmReAllotWpAmt;
    }

    public void setAtmReAllotWpAmt(BigDecimal atmReAllotWpAmt) {
        this.atmReAllotWpAmt = atmReAllotWpAmt;
    }

    public BigDecimal getAtmOfferWpAmt() {
        return atmOfferWpAmt;
    }

    public void setAtmOfferWpAmt(BigDecimal atmOfferWpAmt) {
        this.atmOfferWpAmt = atmOfferWpAmt;
    }

    public BigDecimal getAtmReviseWpAmt() {
        return atmReviseWpAmt;
    }

    public void setAtmReviseWpAmt(BigDecimal atmReviseWpAmt) {
        this.atmReviseWpAmt = atmReviseWpAmt;
    }

    public BigDecimal getAtmCommitedAmt() {
        return atmCommitedAmt;
    }

    public void setAtmCommitedAmt(BigDecimal atmCommitedAmt) {
        this.atmCommitedAmt = atmCommitedAmt;
    }

    public BigDecimal getAtmPipelinePayAmt() {
        return atmPipelinePayAmt;
    }

    public void setAtmPipelinePayAmt(BigDecimal atmPipelinePayAmt) {
        this.atmPipelinePayAmt = atmPipelinePayAmt;
    }

    public BigDecimal getAtmAdvanceCommitAmt() {
        return atmAdvanceCommitAmt;
    }

    public void setAtmAdvanceCommitAmt(BigDecimal atmAdvanceCommitAmt) {
        this.atmAdvanceCommitAmt = atmAdvanceCommitAmt;
    }

    public BigDecimal getAtmAdvancePayAmt() {
        return atmAdvancePayAmt;
    }

    public void setAtmAdvancePayAmt(BigDecimal atmAdvancePayAmt) {
        this.atmAdvancePayAmt = atmAdvancePayAmt;
    }

    public BigDecimal getAtmPipeAdvadjCommitAmt() {
        return atmPipeAdvadjCommitAmt;
    }

    public void setAtmPipeAdvadjCommitAmt(BigDecimal atmPipeAdvadjCommitAmt) {
        this.atmPipeAdvadjCommitAmt = atmPipeAdvadjCommitAmt;
    }

    public BigDecimal getAtmPipeAdvadjPayAmt() {
        return atmPipeAdvadjPayAmt;
    }

    public void setAtmPipeAdvadjPayAmt(BigDecimal atmPipeAdvadjPayAmt) {
        this.atmPipeAdvadjPayAmt = atmPipeAdvadjPayAmt;
    }

    public BigDecimal getAtmReceiptPayAmt() {
        return atmReceiptPayAmt;
    }

    public void setAtmReceiptPayAmt(BigDecimal atmReceiptPayAmt) {
        this.atmReceiptPayAmt = atmReceiptPayAmt;
    }

    public BigDecimal getAtmReceiptCommitAmt() {
        return atmReceiptCommitAmt;
    }

    public void setAtmReceiptCommitAmt(BigDecimal atmReceiptCommitAmt) {
        this.atmReceiptCommitAmt = atmReceiptCommitAmt;
    }

    public BigDecimal getAtmExpenseAmt() {
        return atmExpenseAmt;
    }

    public void setAtmExpenseAmt(BigDecimal atmExpenseAmt) {
        this.atmExpenseAmt = atmExpenseAmt;
    }

    public BigDecimal getAtmUsableBalanceAmt() {
        return atmUsableBalanceAmt;
    }

    public void setAtmUsableBalanceAmt(BigDecimal atmUsableBalanceAmt) {
        this.atmUsableBalanceAmt = atmUsableBalanceAmt;
    }

    public BigDecimal getAtmBalanceAmt() {
        return atmBalanceAmt;
    }

    public void setAtmBalanceAmt(BigDecimal atmBalanceAmt) {
        this.atmBalanceAmt = atmBalanceAmt;
    }

    public BigDecimal getAtmPipeCommitedAmt() {
        return atmPipeCommitedAmt;
    }

    public void setAtmPipeCommitedAmt(BigDecimal atmPipeCommitedAmt) {
        this.atmPipeCommitedAmt = atmPipeCommitedAmt;
    }

    public Date getAtmStartDate() {
        return atmStartDate;
    }

    public void setAtmStartDate(Date atmStartDate) {
        this.atmStartDate = atmStartDate;
    }

    public Date getAtmEndDate() {
        return atmEndDate;
    }

    public void setAtmEndDate(Date atmEndDate) {
        this.atmEndDate = atmEndDate;
    }

    public Date getAtmEndDatePossible() {
        return atmEndDatePossible;
    }

    public void setAtmEndDatePossible(Date atmEndDatePossible) {
        this.atmEndDatePossible = atmEndDatePossible;
    }

    public Date getAtmTransactionDate() {
        return atmTransactionDate;
    }

    public void setAtmTransactionDate(Date atmTransactionDate) {
        this.atmTransactionDate = atmTransactionDate;
    }

    public BigDecimal getAtmProCrExpAmt() {
        return atmProCrExpAmt;
    }

    public void setAtmProCrExpAmt(BigDecimal atmProCrExpAmt) {
        this.atmProCrExpAmt = atmProCrExpAmt;
    }

    public BigDecimal getAtmReceivedAdvanceAmt() {
        return atmReceivedAdvanceAmt;
    }

    public void setAtmReceivedAdvanceAmt(BigDecimal atmReceivedAdvanceAmt) {
        this.atmReceivedAdvanceAmt = atmReceivedAdvanceAmt;
    }

    public BigDecimal getAtmCarryFwdAdvAmt() {
        return atmCarryFwdAdvAmt;
    }

    public void setAtmCarryFwdAdvAmt(BigDecimal atmCarryFwdAdvAmt) {
        this.atmCarryFwdAdvAmt = atmCarryFwdAdvAmt;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetLink> getBmwpApActivityBudgetLinkCollection() {
        return bmwpApActivityBudgetLinkCollection;
    }

    public void setBmwpApActivityBudgetLinkCollection(Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection) {
        this.bmwpApActivityBudgetLinkCollection = bmwpApActivityBudgetLinkCollection;
    }

    @XmlTransient
    public Collection<BmwpAgencyProjectLink> getBmwpAgencyProjectLinkCollection() {
        return bmwpAgencyProjectLinkCollection;
    }

    public void setBmwpAgencyProjectLinkCollection(Collection<BmwpAgencyProjectLink> bmwpAgencyProjectLinkCollection) {
        this.bmwpAgencyProjectLinkCollection = bmwpAgencyProjectLinkCollection;
    }

    @XmlTransient
    public Collection<BmwpFundAgencyMst> getBmwpFundAgencyMstCollection() {
        return bmwpFundAgencyMstCollection;
    }

    public void setBmwpFundAgencyMstCollection(Collection<BmwpFundAgencyMst> bmwpFundAgencyMstCollection) {
        this.bmwpFundAgencyMstCollection = bmwpFundAgencyMstCollection;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetDtl> getBmwpApActivityBudgetDtlCollection() {
        return bmwpApActivityBudgetDtlCollection;
    }

    public void setBmwpApActivityBudgetDtlCollection(Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection) {
        this.bmwpApActivityBudgetDtlCollection = bmwpApActivityBudgetDtlCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @XmlTransient
    public Collection<BmwpApActivityMst> getBmwpApActivityMstCollection() {
        return bmwpApActivityMstCollection;
    }

    public void setBmwpApActivityMstCollection(Collection<BmwpApActivityMst> bmwpApActivityMstCollection) {
        this.bmwpApActivityMstCollection = bmwpApActivityMstCollection;
    }

    @XmlTransient
    public Collection<BmwpApBudgetMst> getBmwpApBudgetMstCollection() {
        return bmwpApBudgetMstCollection;
    }

    public void setBmwpApBudgetMstCollection(Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection) {
        this.bmwpApBudgetMstCollection = bmwpApBudgetMstCollection;
    }

    @XmlTransient
    public Collection<BmwpWorkplan> getBmwpWorkplanCollection() {
        return bmwpWorkplanCollection;
    }

    public void setBmwpWorkplanCollection(Collection<BmwpWorkplan> bmwpWorkplanCollection) {
        this.bmwpWorkplanCollection = bmwpWorkplanCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (atmUniqueSrno != null ? atmUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmwpAgencyTypeMst)) {
            return false;
        }
        BmwpAgencyTypeMst other = (BmwpAgencyTypeMst) object;
        if ((this.atmUniqueSrno == null && other.atmUniqueSrno != null) || (this.atmUniqueSrno != null && !this.atmUniqueSrno.equals(other.atmUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.BmwpAgencyTypeMst[ atmUniqueSrno=" + atmUniqueSrno + " ]";
    }
    
}
