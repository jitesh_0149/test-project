/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_RULE_GROUP_CATEGORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdRuleGroupCategory.findAll", query = "SELECT f FROM FhrdRuleGroupCategory f"),
    @NamedQuery(name = "FhrdRuleGroupCategory.findByRgcSrgKey", query = "SELECT f FROM FhrdRuleGroupCategory f WHERE f.rgcSrgKey = :rgcSrgKey"),
    @NamedQuery(name = "FhrdRuleGroupCategory.findByCatDesc", query = "SELECT f FROM FhrdRuleGroupCategory f WHERE f.catDesc = :catDesc"),
    @NamedQuery(name = "FhrdRuleGroupCategory.findByCreatedt", query = "SELECT f FROM FhrdRuleGroupCategory f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdRuleGroupCategory.findByUpdatedt", query = "SELECT f FROM FhrdRuleGroupCategory f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdRuleGroupCategory.findBySystemUserid", query = "SELECT f FROM FhrdRuleGroupCategory f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdRuleGroupCategory.findByStartDate", query = "SELECT f FROM FhrdRuleGroupCategory f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdRuleGroupCategory.findByEndDate", query = "SELECT f FROM FhrdRuleGroupCategory f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FhrdRuleGroupCategory.findBySrno", query = "SELECT f FROM FhrdRuleGroupCategory f WHERE f.srno = :srno")})
public class FhrdRuleGroupCategory implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "RGC_SRG_KEY")
    private BigDecimal rgcSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CAT_DESC")
    private String catDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SRNO")
    private int srno;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rgcSrgKey")
    private Collection<FhrdRuleManagGroupMst> fhrdRuleManagGroupMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rgcSrgKey")
    private Collection<FhrdEmpContractGroupMst> fhrdEmpContractGroupMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rgcSrgKey")
    private Collection<FhrdRuleGroupRelation> fhrdRuleGroupRelationCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FhrdRuleGroupCategory() {
    }

    public FhrdRuleGroupCategory(BigDecimal rgcSrgKey) {
        this.rgcSrgKey = rgcSrgKey;
    }

    public FhrdRuleGroupCategory(BigDecimal rgcSrgKey, String catDesc, Date createdt, Date startDate, int srno) {
        this.rgcSrgKey = rgcSrgKey;
        this.catDesc = catDesc;
        this.createdt = createdt;
        this.startDate = startDate;
        this.srno = srno;
    }

    public BigDecimal getRgcSrgKey() {
        return rgcSrgKey;
    }

    public void setRgcSrgKey(BigDecimal rgcSrgKey) {
        this.rgcSrgKey = rgcSrgKey;
    }

    public String getCatDesc() {
        return catDesc;
    }

    public void setCatDesc(String catDesc) {
        this.catDesc = catDesc;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getSrno() {
        return srno;
    }

    public void setSrno(int srno) {
        this.srno = srno;
    }

    @XmlTransient
    public Collection<FhrdRuleManagGroupMst> getFhrdRuleManagGroupMstCollection() {
        return fhrdRuleManagGroupMstCollection;
    }

    public void setFhrdRuleManagGroupMstCollection(Collection<FhrdRuleManagGroupMst> fhrdRuleManagGroupMstCollection) {
        this.fhrdRuleManagGroupMstCollection = fhrdRuleManagGroupMstCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpContractGroupMst> getFhrdEmpContractGroupMstCollection() {
        return fhrdEmpContractGroupMstCollection;
    }

    public void setFhrdEmpContractGroupMstCollection(Collection<FhrdEmpContractGroupMst> fhrdEmpContractGroupMstCollection) {
        this.fhrdEmpContractGroupMstCollection = fhrdEmpContractGroupMstCollection;
    }

    @XmlTransient
    public Collection<FhrdRuleGroupRelation> getFhrdRuleGroupRelationCollection() {
        return fhrdRuleGroupRelationCollection;
    }

    public void setFhrdRuleGroupRelationCollection(Collection<FhrdRuleGroupRelation> fhrdRuleGroupRelationCollection) {
        this.fhrdRuleGroupRelationCollection = fhrdRuleGroupRelationCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rgcSrgKey != null ? rgcSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdRuleGroupCategory)) {
            return false;
        }
        FhrdRuleGroupCategory other = (FhrdRuleGroupCategory) object;
        if ((this.rgcSrgKey == null && other.rgcSrgKey != null) || (this.rgcSrgKey != null && !this.rgcSrgKey.equals(other.rgcSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdRuleGroupCategory[ rgcSrgKey=" + rgcSrgKey + " ]";
    }
    
}
