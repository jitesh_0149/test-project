/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "SYST_MENUMST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SystMenumst.findAll", query = "SELECT s FROM SystMenumst s"),
    @NamedQuery(name = "SystMenumst.findByMenuid", query = "SELECT s FROM SystMenumst s WHERE s.systMenumstPK.menuid = :menuid"),
    @NamedQuery(name = "SystMenumst.findByMenudesc", query = "SELECT s FROM SystMenumst s WHERE s.menudesc = :menudesc"),
    @NamedQuery(name = "SystMenumst.findByUrl", query = "SELECT s FROM SystMenumst s WHERE s.url = :url"),
    @NamedQuery(name = "SystMenumst.findByTooltip", query = "SELECT s FROM SystMenumst s WHERE s.tooltip = :tooltip"),
    @NamedQuery(name = "SystMenumst.findByOumUnitSrno", query = "SELECT s FROM SystMenumst s WHERE s.systMenumstPK.oumUnitSrno = :oumUnitSrno"),
    @NamedQuery(name = "SystMenumst.findByCreatedt", query = "SELECT s FROM SystMenumst s WHERE s.createdt = :createdt"),
    @NamedQuery(name = "SystMenumst.findByUpdatedt", query = "SELECT s FROM SystMenumst s WHERE s.updatedt = :updatedt"),
    @NamedQuery(name = "SystMenumst.findByExportFlag", query = "SELECT s FROM SystMenumst s WHERE s.exportFlag = :exportFlag"),
    @NamedQuery(name = "SystMenumst.findByImportFlag", query = "SELECT s FROM SystMenumst s WHERE s.importFlag = :importFlag"),
    @NamedQuery(name = "SystMenumst.findBySystemUserid", query = "SELECT s FROM SystMenumst s WHERE s.systemUserid = :systemUserid"),
    @NamedQuery(name = "SystMenumst.findByWorkingFlag", query = "SELECT s FROM SystMenumst s WHERE s.workingFlag = :workingFlag"),
    @NamedQuery(name = "SystMenumst.findByReqAdminSystList", query = "SELECT s FROM SystMenumst s WHERE s.reqAdminSystList = :reqAdminSystList"),
    @NamedQuery(name = "SystMenumst.findByApplicableUserList", query = "SELECT s FROM SystMenumst s WHERE s.applicableUserList = :applicableUserList"),
    @NamedQuery(name = "SystMenumst.findByAccessSpanFlag", query = "SELECT s FROM SystMenumst s WHERE s.accessSpanFlag = :accessSpanFlag")})
public class SystMenumst implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SystMenumstPK systMenumstPK;
    @Size(max = 50)
    @Column(name = "MENUDESC")
    private String menudesc;
    @Size(max = 2000)
    @Column(name = "URL")
    private String url;
    @Size(max = 1000)
    @Column(name = "TOOLTIP")
    private String tooltip;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "WORKING_FLAG")
    private String workingFlag;
    @Size(max = 200)
    @Column(name = "REQ_ADMIN_SYST_LIST")
    private String reqAdminSystList;
    @Size(max = 4000)
    @Column(name = "APPLICABLE_USER_LIST")
    private String applicableUserList;
    @Size(max = 1)
    @Column(name = "ACCESS_SPAN_FLAG")
    private String accessSpanFlag;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private SystOrgUnitMst systOrgUnitMst;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "systMenumst")
    private Collection<SystMenumst> systMenumstCollection;
    @JoinColumns({
        @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO", insertable = false, updatable = false),
        @JoinColumn(name = "PARENTID", referencedColumnName = "MENUID")})
    @ManyToOne(optional = false)
    private SystMenumst systMenumst;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;

    public SystMenumst() {
    }

    public SystMenumst(SystMenumstPK systMenumstPK) {
        this.systMenumstPK = systMenumstPK;
    }

    public SystMenumst(SystMenumstPK systMenumstPK, Date createdt) {
        this.systMenumstPK = systMenumstPK;
        this.createdt = createdt;
    }

    public SystMenumst(String menuid, int oumUnitSrno) {
        this.systMenumstPK = new SystMenumstPK(menuid, oumUnitSrno);
    }

    public SystMenumstPK getSystMenumstPK() {
        return systMenumstPK;
    }

    public void setSystMenumstPK(SystMenumstPK systMenumstPK) {
        this.systMenumstPK = systMenumstPK;
    }

    public String getMenudesc() {
        return menudesc;
    }

    public void setMenudesc(String menudesc) {
        this.menudesc = menudesc;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTooltip() {
        return tooltip;
    }

    public void setTooltip(String tooltip) {
        this.tooltip = tooltip;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getWorkingFlag() {
        return workingFlag;
    }

    public void setWorkingFlag(String workingFlag) {
        this.workingFlag = workingFlag;
    }

    public String getReqAdminSystList() {
        return reqAdminSystList;
    }

    public void setReqAdminSystList(String reqAdminSystList) {
        this.reqAdminSystList = reqAdminSystList;
    }

    public String getApplicableUserList() {
        return applicableUserList;
    }

    public void setApplicableUserList(String applicableUserList) {
        this.applicableUserList = applicableUserList;
    }

    public String getAccessSpanFlag() {
        return accessSpanFlag;
    }

    public void setAccessSpanFlag(String accessSpanFlag) {
        this.accessSpanFlag = accessSpanFlag;
    }

    public SystOrgUnitMst getSystOrgUnitMst() {
        return systOrgUnitMst;
    }

    public void setSystOrgUnitMst(SystOrgUnitMst systOrgUnitMst) {
        this.systOrgUnitMst = systOrgUnitMst;
    }

    @XmlTransient
    public Collection<SystMenumst> getSystMenumstCollection() {
        return systMenumstCollection;
    }

    public void setSystMenumstCollection(Collection<SystMenumst> systMenumstCollection) {
        this.systMenumstCollection = systMenumstCollection;
    }

    public SystMenumst getSystMenumst() {
        return systMenumst;
    }

    public void setSystMenumst(SystMenumst systMenumst) {
        this.systMenumst = systMenumst;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (systMenumstPK != null ? systMenumstPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SystMenumst)) {
            return false;
        }
        SystMenumst other = (SystMenumst) object;
        if ((this.systMenumstPK == null && other.systMenumstPK != null) || (this.systMenumstPK != null && !this.systMenumstPK.equals(other.systMenumstPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.SystMenumst[ systMenumstPK=" + systMenumstPK + " ]";
    }
    
}
