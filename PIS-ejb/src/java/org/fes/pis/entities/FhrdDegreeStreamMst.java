/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_DEGREE_STREAM_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdDegreeStreamMst.findAll", query = "SELECT f FROM FhrdDegreeStreamMst f"),
    @NamedQuery(name = "FhrdDegreeStreamMst.findByDsmSrgKey", query = "SELECT f FROM FhrdDegreeStreamMst f WHERE f.dsmSrgKey = :dsmSrgKey"),
    @NamedQuery(name = "FhrdDegreeStreamMst.findByDsmDescription", query = "SELECT f FROM FhrdDegreeStreamMst f WHERE f.dsmDescription = :dsmDescription"),
    @NamedQuery(name = "FhrdDegreeStreamMst.findByCreatedt", query = "SELECT f FROM FhrdDegreeStreamMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdDegreeStreamMst.findByUpdatedt", query = "SELECT f FROM FhrdDegreeStreamMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdDegreeStreamMst.findByExportFlag", query = "SELECT f FROM FhrdDegreeStreamMst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdDegreeStreamMst.findByImportFlag", query = "SELECT f FROM FhrdDegreeStreamMst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdDegreeStreamMst.findBySystemUserid", query = "SELECT f FROM FhrdDegreeStreamMst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdDegreeStreamMst.findByStartDate", query = "SELECT f FROM FhrdDegreeStreamMst f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdDegreeStreamMst.findByEndDate", query = "SELECT f FROM FhrdDegreeStreamMst f WHERE f.endDate = :endDate")})
public class FhrdDegreeStreamMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DSM_SRG_KEY")
    private Integer dsmSrgKey;
    @Size(max = 100)
    @Column(name = "DSM_DESCRIPTION")
    private String dsmDescription;
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @OneToMany(mappedBy = "dsmSrgKey")
    private Collection<FhrdQualification> fhrdQualificationCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FhrdDegreeStreamMst() {
    }

    public FhrdDegreeStreamMst(Integer dsmSrgKey) {
        this.dsmSrgKey = dsmSrgKey;
    }

    public FhrdDegreeStreamMst(Integer dsmSrgKey, Date startDate) {
        this.dsmSrgKey = dsmSrgKey;
        this.startDate = startDate;
    }

    public Integer getDsmSrgKey() {
        return dsmSrgKey;
    }

    public void setDsmSrgKey(Integer dsmSrgKey) {
        this.dsmSrgKey = dsmSrgKey;
    }

    public String getDsmDescription() {
        return dsmDescription;
    }

    public void setDsmDescription(String dsmDescription) {
        this.dsmDescription = dsmDescription;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @XmlTransient
    public Collection<FhrdQualification> getFhrdQualificationCollection() {
        return fhrdQualificationCollection;
    }

    public void setFhrdQualificationCollection(Collection<FhrdQualification> fhrdQualificationCollection) {
        this.fhrdQualificationCollection = fhrdQualificationCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dsmSrgKey != null ? dsmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdDegreeStreamMst)) {
            return false;
        }
        FhrdDegreeStreamMst other = (FhrdDegreeStreamMst) object;
        if ((this.dsmSrgKey == null && other.dsmSrgKey != null) || (this.dsmSrgKey != null && !this.dsmSrgKey.equals(other.dsmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdDegreeStreamMst[ dsmSrgKey=" + dsmSrgKey + " ]";
    }
    
}
