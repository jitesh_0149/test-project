/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_PAYTRANSEARNDEDN_SUMMARY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdPaytransearndednSummary.findAll", query = "SELECT f FROM FhrdPaytransearndednSummary f"),
    @NamedQuery(name = "FhrdPaytransearndednSummary.findByOumUnitSrno", query = "SELECT f FROM FhrdPaytransearndednSummary f WHERE f.fhrdPaytransearndednSummaryPK.oumUnitSrno = :oumUnitSrno"),
    @NamedQuery(name = "FhrdPaytransearndednSummary.findByOrgSalaryFromDate", query = "SELECT f FROM FhrdPaytransearndednSummary f WHERE f.fhrdPaytransearndednSummaryPK.orgSalaryFromDate = :orgSalaryFromDate"),
    @NamedQuery(name = "FhrdPaytransearndednSummary.findByOrgSalaryToDate", query = "SELECT f FROM FhrdPaytransearndednSummary f WHERE f.fhrdPaytransearndednSummaryPK.orgSalaryToDate = :orgSalaryToDate"),
    @NamedQuery(name = "FhrdPaytransearndednSummary.findByUserId", query = "SELECT f FROM FhrdPaytransearndednSummary f WHERE f.fhrdPaytransearndednSummaryPK.userId = :userId"),
    @NamedQuery(name = "FhrdPaytransearndednSummary.findByEarndedncd", query = "SELECT f FROM FhrdPaytransearndednSummary f WHERE f.fhrdPaytransearndednSummaryPK.earndedncd = :earndedncd"),
    @NamedQuery(name = "FhrdPaytransearndednSummary.findByEarndednFlag", query = "SELECT f FROM FhrdPaytransearndednSummary f WHERE f.earndednFlag = :earndednFlag"),
    @NamedQuery(name = "FhrdPaytransearndednSummary.findByActualAmt", query = "SELECT f FROM FhrdPaytransearndednSummary f WHERE f.actualAmt = :actualAmt"),
    @NamedQuery(name = "FhrdPaytransearndednSummary.findByRoundActualAmt", query = "SELECT f FROM FhrdPaytransearndednSummary f WHERE f.roundActualAmt = :roundActualAmt"),
    @NamedQuery(name = "FhrdPaytransearndednSummary.findByRoundArrearsAmt", query = "SELECT f FROM FhrdPaytransearndednSummary f WHERE f.roundArrearsAmt = :roundArrearsAmt"),
    @NamedQuery(name = "FhrdPaytransearndednSummary.findByRoundRecoveryAmt", query = "SELECT f FROM FhrdPaytransearndednSummary f WHERE f.roundRecoveryAmt = :roundRecoveryAmt"),
    @NamedQuery(name = "FhrdPaytransearndednSummary.findByPrintInPaySlip", query = "SELECT f FROM FhrdPaytransearndednSummary f WHERE f.printInPaySlip = :printInPaySlip"),
    @NamedQuery(name = "FhrdPaytransearndednSummary.findByPrintIf0Flag", query = "SELECT f FROM FhrdPaytransearndednSummary f WHERE f.printIf0Flag = :printIf0Flag"),
    @NamedQuery(name = "FhrdPaytransearndednSummary.findByEmpNonPayableDays", query = "SELECT f FROM FhrdPaytransearndednSummary f WHERE f.empNonPayableDays = :empNonPayableDays"),
    @NamedQuery(name = "FhrdPaytransearndednSummary.findByEmpPayableDays", query = "SELECT f FROM FhrdPaytransearndednSummary f WHERE f.empPayableDays = :empPayableDays"),
    @NamedQuery(name = "FhrdPaytransearndednSummary.findBySalarySrgKey", query = "SELECT f FROM FhrdPaytransearndednSummary f WHERE f.salarySrgKey = :salarySrgKey")})
public class FhrdPaytransearndednSummary implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FhrdPaytransearndednSummaryPK fhrdPaytransearndednSummaryPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EARNDEDN_FLAG")
    private String earndednFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTUAL_AMT")
    private BigInteger actualAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ROUND_ACTUAL_AMT")
    private long roundActualAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ROUND_ARREARS_AMT")
    private BigInteger roundArrearsAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ROUND_RECOVERY_AMT")
    private BigInteger roundRecoveryAmt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PRINT_IN_PAY_SLIP")
    private String printInPaySlip;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PRINT_IF_0_FLAG")
    private String printIf0Flag;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_NON_PAYABLE_DAYS")
    private BigDecimal empNonPayableDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_PAYABLE_DAYS")
    private BigDecimal empPayableDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SALARY_SRG_KEY")
    private BigInteger salarySrgKey;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private SystOrgUnitMst systOrgUnitMst;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FhrdEmpmst fhrdEmpmst;
    @JoinColumn(name = "PTEDS_EDM_SRG_KEY", referencedColumnName = "EDM_SRG_KEY")
    @ManyToOne
    private FhrdEarndednmst ptedsEdmSrgKey;

    public FhrdPaytransearndednSummary() {
    }

    public FhrdPaytransearndednSummary(FhrdPaytransearndednSummaryPK fhrdPaytransearndednSummaryPK) {
        this.fhrdPaytransearndednSummaryPK = fhrdPaytransearndednSummaryPK;
    }

    public FhrdPaytransearndednSummary(FhrdPaytransearndednSummaryPK fhrdPaytransearndednSummaryPK, String earndednFlag, BigInteger actualAmt, long roundActualAmt, BigInteger roundArrearsAmt, BigInteger roundRecoveryAmt, String printInPaySlip, String printIf0Flag, BigDecimal empNonPayableDays, BigDecimal empPayableDays, BigInteger salarySrgKey) {
        this.fhrdPaytransearndednSummaryPK = fhrdPaytransearndednSummaryPK;
        this.earndednFlag = earndednFlag;
        this.actualAmt = actualAmt;
        this.roundActualAmt = roundActualAmt;
        this.roundArrearsAmt = roundArrearsAmt;
        this.roundRecoveryAmt = roundRecoveryAmt;
        this.printInPaySlip = printInPaySlip;
        this.printIf0Flag = printIf0Flag;
        this.empNonPayableDays = empNonPayableDays;
        this.empPayableDays = empPayableDays;
        this.salarySrgKey = salarySrgKey;
    }

    public FhrdPaytransearndednSummary(int oumUnitSrno, Date orgSalaryFromDate, Date orgSalaryToDate, int userId, String earndedncd) {
        this.fhrdPaytransearndednSummaryPK = new FhrdPaytransearndednSummaryPK(oumUnitSrno, orgSalaryFromDate, orgSalaryToDate, userId, earndedncd);
    }

    public FhrdPaytransearndednSummaryPK getFhrdPaytransearndednSummaryPK() {
        return fhrdPaytransearndednSummaryPK;
    }

    public void setFhrdPaytransearndednSummaryPK(FhrdPaytransearndednSummaryPK fhrdPaytransearndednSummaryPK) {
        this.fhrdPaytransearndednSummaryPK = fhrdPaytransearndednSummaryPK;
    }

    public String getEarndednFlag() {
        return earndednFlag;
    }

    public void setEarndednFlag(String earndednFlag) {
        this.earndednFlag = earndednFlag;
    }

    public BigInteger getActualAmt() {
        return actualAmt;
    }

    public void setActualAmt(BigInteger actualAmt) {
        this.actualAmt = actualAmt;
    }

    public long getRoundActualAmt() {
        return roundActualAmt;
    }

    public void setRoundActualAmt(long roundActualAmt) {
        this.roundActualAmt = roundActualAmt;
    }

    public BigInteger getRoundArrearsAmt() {
        return roundArrearsAmt;
    }

    public void setRoundArrearsAmt(BigInteger roundArrearsAmt) {
        this.roundArrearsAmt = roundArrearsAmt;
    }

    public BigInteger getRoundRecoveryAmt() {
        return roundRecoveryAmt;
    }

    public void setRoundRecoveryAmt(BigInteger roundRecoveryAmt) {
        this.roundRecoveryAmt = roundRecoveryAmt;
    }

    public String getPrintInPaySlip() {
        return printInPaySlip;
    }

    public void setPrintInPaySlip(String printInPaySlip) {
        this.printInPaySlip = printInPaySlip;
    }

    public String getPrintIf0Flag() {
        return printIf0Flag;
    }

    public void setPrintIf0Flag(String printIf0Flag) {
        this.printIf0Flag = printIf0Flag;
    }

    public BigDecimal getEmpNonPayableDays() {
        return empNonPayableDays;
    }

    public void setEmpNonPayableDays(BigDecimal empNonPayableDays) {
        this.empNonPayableDays = empNonPayableDays;
    }

    public BigDecimal getEmpPayableDays() {
        return empPayableDays;
    }

    public void setEmpPayableDays(BigDecimal empPayableDays) {
        this.empPayableDays = empPayableDays;
    }

    public BigInteger getSalarySrgKey() {
        return salarySrgKey;
    }

    public void setSalarySrgKey(BigInteger salarySrgKey) {
        this.salarySrgKey = salarySrgKey;
    }

    public SystOrgUnitMst getSystOrgUnitMst() {
        return systOrgUnitMst;
    }

    public void setSystOrgUnitMst(SystOrgUnitMst systOrgUnitMst) {
        this.systOrgUnitMst = systOrgUnitMst;
    }

    public FhrdEmpmst getFhrdEmpmst() {
        return fhrdEmpmst;
    }

    public void setFhrdEmpmst(FhrdEmpmst fhrdEmpmst) {
        this.fhrdEmpmst = fhrdEmpmst;
    }

    public FhrdEarndednmst getPtedsEdmSrgKey() {
        return ptedsEdmSrgKey;
    }

    public void setPtedsEdmSrgKey(FhrdEarndednmst ptedsEdmSrgKey) {
        this.ptedsEdmSrgKey = ptedsEdmSrgKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fhrdPaytransearndednSummaryPK != null ? fhrdPaytransearndednSummaryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdPaytransearndednSummary)) {
            return false;
        }
        FhrdPaytransearndednSummary other = (FhrdPaytransearndednSummary) object;
        if ((this.fhrdPaytransearndednSummaryPK == null && other.fhrdPaytransearndednSummaryPK != null) || (this.fhrdPaytransearndednSummaryPK != null && !this.fhrdPaytransearndednSummaryPK.equals(other.fhrdPaytransearndednSummaryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdPaytransearndednSummary[ fhrdPaytransearndednSummaryPK=" + fhrdPaytransearndednSummaryPK + " ]";
    }
    
}
