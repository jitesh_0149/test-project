/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_CONTRACT_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdContractMst.findAll", query = "SELECT f FROM FhrdContractMst f"),
    @NamedQuery(name = "FhrdContractMst.findByContractSrno", query = "SELECT f FROM FhrdContractMst f WHERE f.contractSrno = :contractSrno"),
    @NamedQuery(name = "FhrdContractMst.findByContractType", query = "SELECT f FROM FhrdContractMst f WHERE f.contractType = :contractType"),
    @NamedQuery(name = "FhrdContractMst.findByContractDesc", query = "SELECT f FROM FhrdContractMst f WHERE f.contractDesc = :contractDesc"),
    @NamedQuery(name = "FhrdContractMst.findByCreatedt", query = "SELECT f FROM FhrdContractMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdContractMst.findByUpdatedt", query = "SELECT f FROM FhrdContractMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdContractMst.findBySystemUserid", query = "SELECT f FROM FhrdContractMst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdContractMst.findByExportFlag", query = "SELECT f FROM FhrdContractMst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdContractMst.findByImportFlag", query = "SELECT f FROM FhrdContractMst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdContractMst.findByCmSrgKey", query = "SELECT f FROM FhrdContractMst f WHERE f.cmSrgKey = :cmSrgKey"),
    @NamedQuery(name = "FhrdContractMst.findByContractCategory", query = "SELECT f FROM FhrdContractMst f WHERE f.contractCategory = :contractCategory"),
    @NamedQuery(name = "FhrdContractMst.findByStartDate", query = "SELECT f FROM FhrdContractMst f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdContractMst.findByEndDate", query = "SELECT f FROM FhrdContractMst f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FhrdContractMst.findByCmAltSrno", query = "SELECT f FROM FhrdContractMst f WHERE f.cmAltSrno = :cmAltSrno"),
    @NamedQuery(name = "FhrdContractMst.findByCmAltSubsrno", query = "SELECT f FROM FhrdContractMst f WHERE f.cmAltSubsrno = :cmAltSubsrno")})
public class FhrdContractMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONTRACT_SRNO")
    private int contractSrno;
    @Size(max = 10)
    @Column(name = "CONTRACT_TYPE")
    private String contractType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CONTRACT_DESC")
    private String contractDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CM_SRG_KEY")
    private BigDecimal cmSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "CONTRACT_CATEGORY")
    private String contractCategory;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Column(name = "CM_ALT_SRNO")
    private Integer cmAltSrno;
    @Column(name = "CM_ALT_SUBSRNO")
    private Integer cmAltSubsrno;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cmSrgKey")
    private Collection<FhrdContractGroupRelation> fhrdContractGroupRelationCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cmSrgKey")
    private Collection<FhrdEmpContractGroupMst> fhrdEmpContractGroupMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cmSrgKey")
    private Collection<FhrdEmpContractMst> fhrdEmpContractMstCollection;

    public FhrdContractMst() {
    }

    public FhrdContractMst(BigDecimal cmSrgKey) {
        this.cmSrgKey = cmSrgKey;
    }

    public FhrdContractMst(BigDecimal cmSrgKey, int contractSrno, String contractDesc, Date createdt, String contractCategory, Date startDate) {
        this.cmSrgKey = cmSrgKey;
        this.contractSrno = contractSrno;
        this.contractDesc = contractDesc;
        this.createdt = createdt;
        this.contractCategory = contractCategory;
        this.startDate = startDate;
    }

    public int getContractSrno() {
        return contractSrno;
    }

    public void setContractSrno(int contractSrno) {
        this.contractSrno = contractSrno;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getContractDesc() {
        return contractDesc;
    }

    public void setContractDesc(String contractDesc) {
        this.contractDesc = contractDesc;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public BigDecimal getCmSrgKey() {
        return cmSrgKey;
    }

    public void setCmSrgKey(BigDecimal cmSrgKey) {
        this.cmSrgKey = cmSrgKey;
    }

    public String getContractCategory() {
        return contractCategory;
    }

    public void setContractCategory(String contractCategory) {
        this.contractCategory = contractCategory;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getCmAltSrno() {
        return cmAltSrno;
    }

    public void setCmAltSrno(Integer cmAltSrno) {
        this.cmAltSrno = cmAltSrno;
    }

    public Integer getCmAltSubsrno() {
        return cmAltSubsrno;
    }

    public void setCmAltSubsrno(Integer cmAltSubsrno) {
        this.cmAltSubsrno = cmAltSubsrno;
    }

    @XmlTransient
    public Collection<FhrdContractGroupRelation> getFhrdContractGroupRelationCollection() {
        return fhrdContractGroupRelationCollection;
    }

    public void setFhrdContractGroupRelationCollection(Collection<FhrdContractGroupRelation> fhrdContractGroupRelationCollection) {
        this.fhrdContractGroupRelationCollection = fhrdContractGroupRelationCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @XmlTransient
    public Collection<FhrdEmpContractGroupMst> getFhrdEmpContractGroupMstCollection() {
        return fhrdEmpContractGroupMstCollection;
    }

    public void setFhrdEmpContractGroupMstCollection(Collection<FhrdEmpContractGroupMst> fhrdEmpContractGroupMstCollection) {
        this.fhrdEmpContractGroupMstCollection = fhrdEmpContractGroupMstCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpContractMst> getFhrdEmpContractMstCollection() {
        return fhrdEmpContractMstCollection;
    }

    public void setFhrdEmpContractMstCollection(Collection<FhrdEmpContractMst> fhrdEmpContractMstCollection) {
        this.fhrdEmpContractMstCollection = fhrdEmpContractMstCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cmSrgKey != null ? cmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdContractMst)) {
            return false;
        }
        FhrdContractMst other = (FhrdContractMst) object;
        if ((this.cmSrgKey == null && other.cmSrgKey != null) || (this.cmSrgKey != null && !this.cmSrgKey.equals(other.cmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdContractMst[ cmSrgKey=" + cmSrgKey + " ]";
    }
    
}
