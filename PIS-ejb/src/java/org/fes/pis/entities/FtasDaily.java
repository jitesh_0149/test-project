/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_DAILY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasDaily.findAll", query = "SELECT f FROM FtasDaily f"),
    @NamedQuery(name = "FtasDaily.findByAttddt", query = "SELECT f FROM FtasDaily f WHERE f.attddt = :attddt"),
    @NamedQuery(name = "FtasDaily.findByAttdtime", query = "SELECT f FROM FtasDaily f WHERE f.attdtime = :attdtime"),
    @NamedQuery(name = "FtasDaily.findByHalf1", query = "SELECT f FROM FtasDaily f WHERE f.half1 = :half1"),
    @NamedQuery(name = "FtasDaily.findByHalf2", query = "SELECT f FROM FtasDaily f WHERE f.half2 = :half2"),
    @NamedQuery(name = "FtasDaily.findByBacklogflag", query = "SELECT f FROM FtasDaily f WHERE f.backlogflag = :backlogflag"),
    @NamedQuery(name = "FtasDaily.findByHalf1attdtime", query = "SELECT f FROM FtasDaily f WHERE f.half1attdtime = :half1attdtime"),
    @NamedQuery(name = "FtasDaily.findByHalf2attdtime", query = "SELECT f FROM FtasDaily f WHERE f.half2attdtime = :half2attdtime"),
    @NamedQuery(name = "FtasDaily.findByCreatedt", query = "SELECT f FROM FtasDaily f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasDaily.findByUpdatedt", query = "SELECT f FROM FtasDaily f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasDaily.findByExportFlag", query = "SELECT f FROM FtasDaily f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FtasDaily.findByImportFlag", query = "SELECT f FROM FtasDaily f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FtasDaily.findBySystemUserid", query = "SELECT f FROM FtasDaily f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FtasDaily.findByEmpno", query = "SELECT f FROM FtasDaily f WHERE f.empno = :empno"),
    @NamedQuery(name = "FtasDaily.findByBHalf1", query = "SELECT f FROM FtasDaily f WHERE f.bHalf1 = :bHalf1"),
    @NamedQuery(name = "FtasDaily.findByAHalf2", query = "SELECT f FROM FtasDaily f WHERE f.aHalf2 = :aHalf2"),
    @NamedQuery(name = "FtasDaily.findByDlySrgKey", query = "SELECT f FROM FtasDaily f WHERE f.dlySrgKey = :dlySrgKey"),
    @NamedQuery(name = "FtasDaily.findByHalf1DPerLeaveBalComp", query = "SELECT f FROM FtasDaily f WHERE f.half1DPerLeaveBalComp = :half1DPerLeaveBalComp"),
    @NamedQuery(name = "FtasDaily.findByHalf2DPerLeaveBalComp", query = "SELECT f FROM FtasDaily f WHERE f.half2DPerLeaveBalComp = :half2DPerLeaveBalComp"),
    @NamedQuery(name = "FtasDaily.findByParhalf1DPerLeaveBalComp", query = "SELECT f FROM FtasDaily f WHERE f.parhalf1DPerLeaveBalComp = :parhalf1DPerLeaveBalComp"),
    @NamedQuery(name = "FtasDaily.findByParhalf2DPerLeaveBalComp", query = "SELECT f FROM FtasDaily f WHERE f.parhalf2DPerLeaveBalComp = :parhalf2DPerLeaveBalComp"),
    @NamedQuery(name = "FtasDaily.findByTranYear", query = "SELECT f FROM FtasDaily f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FtasDaily.findByParHalf1", query = "SELECT f FROM FtasDaily f WHERE f.parHalf1 = :parHalf1"),
    @NamedQuery(name = "FtasDaily.findByParHalf2", query = "SELECT f FROM FtasDaily f WHERE f.parHalf2 = :parHalf2"),
    @NamedQuery(name = "FtasDaily.findByChildHalf1", query = "SELECT f FROM FtasDaily f WHERE f.childHalf1 = :childHalf1"),
    @NamedQuery(name = "FtasDaily.findByChildHalf2", query = "SELECT f FROM FtasDaily f WHERE f.childHalf2 = :childHalf2"),
    @NamedQuery(name = "FtasDaily.findByTtrHalf1", query = "SELECT f FROM FtasDaily f WHERE f.ttrHalf1 = :ttrHalf1"),
    @NamedQuery(name = "FtasDaily.findByTtrHalf2", query = "SELECT f FROM FtasDaily f WHERE f.ttrHalf2 = :ttrHalf2"),
    @NamedQuery(name = "FtasDaily.findByActualHalf1", query = "SELECT f FROM FtasDaily f WHERE f.actualHalf1 = :actualHalf1"),
    @NamedQuery(name = "FtasDaily.findByActualHalf2", query = "SELECT f FROM FtasDaily f WHERE f.actualHalf2 = :actualHalf2"),
    @NamedQuery(name = "FtasDaily.findByPendingHalf1", query = "SELECT f FROM FtasDaily f WHERE f.pendingHalf1 = :pendingHalf1"),
    @NamedQuery(name = "FtasDaily.findByPendingHalf2", query = "SELECT f FROM FtasDaily f WHERE f.pendingHalf2 = :pendingHalf2")})
public class FtasDaily implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "ATTDDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date attddt;
    @Column(name = "ATTDTIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date attdtime;
    @Size(max = 5)
    @Column(name = "HALF1")
    private String half1;
    @Size(max = 5)
    @Column(name = "HALF2")
    private String half2;
    @Size(max = 1)
    @Column(name = "BACKLOGFLAG")
    private String backlogflag;
    @Column(name = "HALF1ATTDTIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date half1attdtime;
    @Column(name = "HALF2ATTDTIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date half2attdtime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Column(name = "EMPNO")
    private Integer empno;
    @Size(max = 5)
    @Column(name = "B_HALF1")
    private String bHalf1;
    @Size(max = 5)
    @Column(name = "A_HALF2")
    private String aHalf2;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DLY_SRG_KEY")
    private BigDecimal dlySrgKey;
    @Column(name = "HALF1_D_PER_LEAVE_BAL_COMP")
    private BigDecimal half1DPerLeaveBalComp;
    @Column(name = "HALF2_D_PER_LEAVE_BAL_COMP")
    private BigDecimal half2DPerLeaveBalComp;
    @Column(name = "PARHALF1_D_PER_LEAVE_BAL_COMP")
    private BigDecimal parhalf1DPerLeaveBalComp;
    @Column(name = "PARHALF2_D_PER_LEAVE_BAL_COMP")
    private BigDecimal parhalf2DPerLeaveBalComp;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Size(max = 5)
    @Column(name = "PAR_HALF1")
    private String parHalf1;
    @Size(max = 5)
    @Column(name = "PAR_HALF2")
    private String parHalf2;
    @Size(max = 5)
    @Column(name = "CHILD_HALF1")
    private String childHalf1;
    @Size(max = 5)
    @Column(name = "CHILD_HALF2")
    private String childHalf2;
    @Size(max = 5)
    @Column(name = "TTR_HALF1")
    private String ttrHalf1;
    @Size(max = 5)
    @Column(name = "TTR_HALF2")
    private String ttrHalf2;
    @Size(max = 5)
    @Column(name = "ACTUAL_HALF1")
    private String actualHalf1;
    @Size(max = 5)
    @Column(name = "ACTUAL_HALF2")
    private String actualHalf2;
    @Size(max = 5)
    @Column(name = "PENDING_HALF1")
    private String pendingHalf1;
    @Size(max = 5)
    @Column(name = "PENDING_HALF2")
    private String pendingHalf2;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CHILD_HALF2_LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne
    private FtasLeavemst childHalf2LmSrgKey;
    @JoinColumn(name = "TTR_HALF1_LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne
    private FtasLeavemst ttrHalf1LmSrgKey;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst userId;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "HALF1_LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne
    private FtasLeavemst half1LmSrgKey;
    @JoinColumn(name = "PAR_HALF2_LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne
    private FtasLeavemst parHalf2LmSrgKey;
    @JoinColumn(name = "PAR_HALF1_LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne
    private FtasLeavemst parHalf1LmSrgKey;
    @JoinColumn(name = "HALF2_LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne
    private FtasLeavemst half2LmSrgKey;
    @JoinColumn(name = "CHILD_HALF1_LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne
    private FtasLeavemst childHalf1LmSrgKey;
    @JoinColumn(name = "PENDING_HALF2_LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne
    private FtasLeavemst pendingHalf2LmSrgKey;
    @JoinColumn(name = "ACTUAL_HALF2_LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne
    private FtasLeavemst actualHalf2LmSrgKey;
    @JoinColumn(name = "ACTUAL_HALF1_LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne
    private FtasLeavemst actualHalf1LmSrgKey;
    @JoinColumn(name = "PENDING_HALF1_LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne
    private FtasLeavemst pendingHalf1LmSrgKey;
    @JoinColumn(name = "TTR_HALF2_LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne
    private FtasLeavemst ttrHalf2LmSrgKey;

    public FtasDaily() {
    }

    public FtasDaily(BigDecimal dlySrgKey) {
        this.dlySrgKey = dlySrgKey;
    }

    public FtasDaily(BigDecimal dlySrgKey, Date createdt, short tranYear) {
        this.dlySrgKey = dlySrgKey;
        this.createdt = createdt;
        this.tranYear = tranYear;
    }

    public Date getAttddt() {
        return attddt;
    }

    public void setAttddt(Date attddt) {
        this.attddt = attddt;
    }

    public Date getAttdtime() {
        return attdtime;
    }

    public void setAttdtime(Date attdtime) {
        this.attdtime = attdtime;
    }

    public String getHalf1() {
        return half1;
    }

    public void setHalf1(String half1) {
        this.half1 = half1;
    }

    public String getHalf2() {
        return half2;
    }

    public void setHalf2(String half2) {
        this.half2 = half2;
    }

    public String getBacklogflag() {
        return backlogflag;
    }

    public void setBacklogflag(String backlogflag) {
        this.backlogflag = backlogflag;
    }

    public Date getHalf1attdtime() {
        return half1attdtime;
    }

    public void setHalf1attdtime(Date half1attdtime) {
        this.half1attdtime = half1attdtime;
    }

    public Date getHalf2attdtime() {
        return half2attdtime;
    }

    public void setHalf2attdtime(Date half2attdtime) {
        this.half2attdtime = half2attdtime;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public Integer getEmpno() {
        return empno;
    }

    public void setEmpno(Integer empno) {
        this.empno = empno;
    }

    public String getBHalf1() {
        return bHalf1;
    }

    public void setBHalf1(String bHalf1) {
        this.bHalf1 = bHalf1;
    }

    public String getAHalf2() {
        return aHalf2;
    }

    public void setAHalf2(String aHalf2) {
        this.aHalf2 = aHalf2;
    }

    public BigDecimal getDlySrgKey() {
        return dlySrgKey;
    }

    public void setDlySrgKey(BigDecimal dlySrgKey) {
        this.dlySrgKey = dlySrgKey;
    }

    public BigDecimal getHalf1DPerLeaveBalComp() {
        return half1DPerLeaveBalComp;
    }

    public void setHalf1DPerLeaveBalComp(BigDecimal half1DPerLeaveBalComp) {
        this.half1DPerLeaveBalComp = half1DPerLeaveBalComp;
    }

    public BigDecimal getHalf2DPerLeaveBalComp() {
        return half2DPerLeaveBalComp;
    }

    public void setHalf2DPerLeaveBalComp(BigDecimal half2DPerLeaveBalComp) {
        this.half2DPerLeaveBalComp = half2DPerLeaveBalComp;
    }

    public BigDecimal getParhalf1DPerLeaveBalComp() {
        return parhalf1DPerLeaveBalComp;
    }

    public void setParhalf1DPerLeaveBalComp(BigDecimal parhalf1DPerLeaveBalComp) {
        this.parhalf1DPerLeaveBalComp = parhalf1DPerLeaveBalComp;
    }

    public BigDecimal getParhalf2DPerLeaveBalComp() {
        return parhalf2DPerLeaveBalComp;
    }

    public void setParhalf2DPerLeaveBalComp(BigDecimal parhalf2DPerLeaveBalComp) {
        this.parhalf2DPerLeaveBalComp = parhalf2DPerLeaveBalComp;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public String getParHalf1() {
        return parHalf1;
    }

    public void setParHalf1(String parHalf1) {
        this.parHalf1 = parHalf1;
    }

    public String getParHalf2() {
        return parHalf2;
    }

    public void setParHalf2(String parHalf2) {
        this.parHalf2 = parHalf2;
    }

    public String getChildHalf1() {
        return childHalf1;
    }

    public void setChildHalf1(String childHalf1) {
        this.childHalf1 = childHalf1;
    }

    public String getChildHalf2() {
        return childHalf2;
    }

    public void setChildHalf2(String childHalf2) {
        this.childHalf2 = childHalf2;
    }

    public String getTtrHalf1() {
        return ttrHalf1;
    }

    public void setTtrHalf1(String ttrHalf1) {
        this.ttrHalf1 = ttrHalf1;
    }

    public String getTtrHalf2() {
        return ttrHalf2;
    }

    public void setTtrHalf2(String ttrHalf2) {
        this.ttrHalf2 = ttrHalf2;
    }

    public String getActualHalf1() {
        return actualHalf1;
    }

    public void setActualHalf1(String actualHalf1) {
        this.actualHalf1 = actualHalf1;
    }

    public String getActualHalf2() {
        return actualHalf2;
    }

    public void setActualHalf2(String actualHalf2) {
        this.actualHalf2 = actualHalf2;
    }

    public String getPendingHalf1() {
        return pendingHalf1;
    }

    public void setPendingHalf1(String pendingHalf1) {
        this.pendingHalf1 = pendingHalf1;
    }

    public String getPendingHalf2() {
        return pendingHalf2;
    }

    public void setPendingHalf2(String pendingHalf2) {
        this.pendingHalf2 = pendingHalf2;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasLeavemst getChildHalf2LmSrgKey() {
        return childHalf2LmSrgKey;
    }

    public void setChildHalf2LmSrgKey(FtasLeavemst childHalf2LmSrgKey) {
        this.childHalf2LmSrgKey = childHalf2LmSrgKey;
    }

    public FtasLeavemst getTtrHalf1LmSrgKey() {
        return ttrHalf1LmSrgKey;
    }

    public void setTtrHalf1LmSrgKey(FtasLeavemst ttrHalf1LmSrgKey) {
        this.ttrHalf1LmSrgKey = ttrHalf1LmSrgKey;
    }

    public FhrdEmpmst getUserId() {
        return userId;
    }

    public void setUserId(FhrdEmpmst userId) {
        this.userId = userId;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FtasLeavemst getHalf1LmSrgKey() {
        return half1LmSrgKey;
    }

    public void setHalf1LmSrgKey(FtasLeavemst half1LmSrgKey) {
        this.half1LmSrgKey = half1LmSrgKey;
    }

    public FtasLeavemst getParHalf2LmSrgKey() {
        return parHalf2LmSrgKey;
    }

    public void setParHalf2LmSrgKey(FtasLeavemst parHalf2LmSrgKey) {
        this.parHalf2LmSrgKey = parHalf2LmSrgKey;
    }

    public FtasLeavemst getParHalf1LmSrgKey() {
        return parHalf1LmSrgKey;
    }

    public void setParHalf1LmSrgKey(FtasLeavemst parHalf1LmSrgKey) {
        this.parHalf1LmSrgKey = parHalf1LmSrgKey;
    }

    public FtasLeavemst getHalf2LmSrgKey() {
        return half2LmSrgKey;
    }

    public void setHalf2LmSrgKey(FtasLeavemst half2LmSrgKey) {
        this.half2LmSrgKey = half2LmSrgKey;
    }

    public FtasLeavemst getChildHalf1LmSrgKey() {
        return childHalf1LmSrgKey;
    }

    public void setChildHalf1LmSrgKey(FtasLeavemst childHalf1LmSrgKey) {
        this.childHalf1LmSrgKey = childHalf1LmSrgKey;
    }

    public FtasLeavemst getPendingHalf2LmSrgKey() {
        return pendingHalf2LmSrgKey;
    }

    public void setPendingHalf2LmSrgKey(FtasLeavemst pendingHalf2LmSrgKey) {
        this.pendingHalf2LmSrgKey = pendingHalf2LmSrgKey;
    }

    public FtasLeavemst getActualHalf2LmSrgKey() {
        return actualHalf2LmSrgKey;
    }

    public void setActualHalf2LmSrgKey(FtasLeavemst actualHalf2LmSrgKey) {
        this.actualHalf2LmSrgKey = actualHalf2LmSrgKey;
    }

    public FtasLeavemst getActualHalf1LmSrgKey() {
        return actualHalf1LmSrgKey;
    }

    public void setActualHalf1LmSrgKey(FtasLeavemst actualHalf1LmSrgKey) {
        this.actualHalf1LmSrgKey = actualHalf1LmSrgKey;
    }

    public FtasLeavemst getPendingHalf1LmSrgKey() {
        return pendingHalf1LmSrgKey;
    }

    public void setPendingHalf1LmSrgKey(FtasLeavemst pendingHalf1LmSrgKey) {
        this.pendingHalf1LmSrgKey = pendingHalf1LmSrgKey;
    }

    public FtasLeavemst getTtrHalf2LmSrgKey() {
        return ttrHalf2LmSrgKey;
    }

    public void setTtrHalf2LmSrgKey(FtasLeavemst ttrHalf2LmSrgKey) {
        this.ttrHalf2LmSrgKey = ttrHalf2LmSrgKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dlySrgKey != null ? dlySrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasDaily)) {
            return false;
        }
        FtasDaily other = (FtasDaily) object;
        if ((this.dlySrgKey == null && other.dlySrgKey != null) || (this.dlySrgKey != null && !this.dlySrgKey.equals(other.dlySrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasDaily[ dlySrgKey=" + dlySrgKey + " ]";
    }
    
}
