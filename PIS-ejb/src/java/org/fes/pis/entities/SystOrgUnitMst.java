/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "SYST_ORG_UNIT_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SystOrgUnitMst.findAll", query = "SELECT s FROM SystOrgUnitMst s"),
    @NamedQuery(name = "SystOrgUnitMst.findByOumUnitSrno", query = "SELECT s FROM SystOrgUnitMst s WHERE s.oumUnitSrno = :oumUnitSrno"),
    @NamedQuery(name = "SystOrgUnitMst.findByOumName", query = "SELECT s FROM SystOrgUnitMst s WHERE s.oumName = :oumName"),
    @NamedQuery(name = "SystOrgUnitMst.findByOumShortName", query = "SELECT s FROM SystOrgUnitMst s WHERE s.oumShortName = :oumShortName"),
    @NamedQuery(name = "SystOrgUnitMst.findByOumLocSrno", query = "SELECT s FROM SystOrgUnitMst s WHERE s.oumLocSrno = :oumLocSrno"),
    @NamedQuery(name = "SystOrgUnitMst.findByOumLocation", query = "SELECT s FROM SystOrgUnitMst s WHERE s.oumLocation = :oumLocation"),
    @NamedQuery(name = "SystOrgUnitMst.findByOumStartDate", query = "SELECT s FROM SystOrgUnitMst s WHERE s.oumStartDate = :oumStartDate"),
    @NamedQuery(name = "SystOrgUnitMst.findByOumEndDate", query = "SELECT s FROM SystOrgUnitMst s WHERE s.oumEndDate = :oumEndDate"),
    @NamedQuery(name = "SystOrgUnitMst.findByOumOperStDate", query = "SELECT s FROM SystOrgUnitMst s WHERE s.oumOperStDate = :oumOperStDate"),
    @NamedQuery(name = "SystOrgUnitMst.findByOumOperEndDate", query = "SELECT s FROM SystOrgUnitMst s WHERE s.oumOperEndDate = :oumOperEndDate"),
    @NamedQuery(name = "SystOrgUnitMst.findByOumNativeOuNo", query = "SELECT s FROM SystOrgUnitMst s WHERE s.oumNativeOuNo = :oumNativeOuNo"),
    @NamedQuery(name = "SystOrgUnitMst.findByOumParentUnitSrnoFk", query = "SELECT s FROM SystOrgUnitMst s WHERE s.oumParentUnitSrnoFk = :oumParentUnitSrnoFk"),
    @NamedQuery(name = "SystOrgUnitMst.findByOumHead", query = "SELECT s FROM SystOrgUnitMst s WHERE s.oumHead = :oumHead"),
    @NamedQuery(name = "SystOrgUnitMst.findByOumHeadAddrnoFk", query = "SELECT s FROM SystOrgUnitMst s WHERE s.oumHeadAddrnoFk = :oumHeadAddrnoFk"),
    @NamedQuery(name = "SystOrgUnitMst.findByOumRemarks", query = "SELECT s FROM SystOrgUnitMst s WHERE s.oumRemarks = :oumRemarks"),
    @NamedQuery(name = "SystOrgUnitMst.findByCreateby", query = "SELECT s FROM SystOrgUnitMst s WHERE s.createby = :createby"),
    @NamedQuery(name = "SystOrgUnitMst.findByCreatedt", query = "SELECT s FROM SystOrgUnitMst s WHERE s.createdt = :createdt"),
    @NamedQuery(name = "SystOrgUnitMst.findByUpdateby", query = "SELECT s FROM SystOrgUnitMst s WHERE s.updateby = :updateby"),
    @NamedQuery(name = "SystOrgUnitMst.findByUpdatedt", query = "SELECT s FROM SystOrgUnitMst s WHERE s.updatedt = :updatedt"),
    @NamedQuery(name = "SystOrgUnitMst.findByExportFlag", query = "SELECT s FROM SystOrgUnitMst s WHERE s.exportFlag = :exportFlag"),
    @NamedQuery(name = "SystOrgUnitMst.findByImportFlag", query = "SELECT s FROM SystOrgUnitMst s WHERE s.importFlag = :importFlag"),
    @NamedQuery(name = "SystOrgUnitMst.findBySystemUserid", query = "SELECT s FROM SystOrgUnitMst s WHERE s.systemUserid = :systemUserid")})
public class SystOrgUnitMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "OUM_UNIT_SRNO")
    private Integer oumUnitSrno;
    @Size(max = 200)
    @Column(name = "OUM_NAME")
    private String oumName;
    @Size(max = 10)
    @Column(name = "OUM_SHORT_NAME")
    private String oumShortName;
    @Column(name = "OUM_LOC_SRNO")
    private Short oumLocSrno;
    @Size(max = 50)
    @Column(name = "OUM_LOCATION")
    private String oumLocation;
    @Column(name = "OUM_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date oumStartDate;
    @Column(name = "OUM_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date oumEndDate;
    @Column(name = "OUM_OPER_ST_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date oumOperStDate;
    @Column(name = "OUM_OPER_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date oumOperEndDate;
    @Lob
    @Column(name = "OUM_IMAGE")
    private byte[] oumImage;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OUM_NATIVE_OU_NO")
    private int oumNativeOuNo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OUM_PARENT_UNIT_SRNO_FK")
    private int oumParentUnitSrnoFk;
    @Column(name = "OUM_HEAD")
    private Integer oumHead;
    @Column(name = "OUM_HEAD_ADDRNO_FK")
    private Integer oumHeadAddrnoFk;
    @Size(max = 4000)
    @Column(name = "OUM_REMARKS")
    private String oumRemarks;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEBY")
    private int createby;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEBY")
    private Integer updateby;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasTadaOtherDtl> ftasTadaOtherDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<BmwpUnitMst> bmwpUnitMstCollection;
    @JoinColumns({
        @JoinColumn(name = "OUM_UTM_TYPE_SRNO_FK", referencedColumnName = "UTM_TYPE_SRNO"),
        @JoinColumn(name = "OUM_UTM_TYPE_CODE_FK", referencedColumnName = "UTM_TYPE_CODE")})
    @ManyToOne
    private SystOrgUnitTypeMst systOrgUnitTypeMst;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdContractGroupRelation> fhrdContractGroupRelationCollection;
    @OneToMany(mappedBy = "oumUnitSrno")
    private Collection<FcadTeleFax> fcadTeleFaxCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasLeavemst> ftasLeavemstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdContractMst> fhrdContractMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<BmwpAgencyProjectLink> bmwpAgencyProjectLinkCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<BmwpAgencyProjectCategory> bmwpAgencyProjectCategoryCollection;
    @OneToMany(mappedBy = "oumUnitSrno")
    private Collection<FhrdExperience> fhrdExperienceCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FcadAddrGrpDtl> fcadAddrGrpDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdRuleManagGroupMst> fhrdRuleManagGroupMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasTadaBillMst> ftasTadaBillMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "systOrgUnitMst")
    private Collection<FhrdPaytransearndednSummary> fhrdPaytransearndednSummaryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasLockMst> ftasLockMstCollection;
    @OneToMany(mappedBy = "oumUnitSrno")
    private Collection<FhrdEdRepMst> fhrdEdRepMstCollection;
    @OneToMany(mappedBy = "aprvAuthOum")
    private Collection<FtasAbsentees> ftasAbsenteesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasAbsentees> ftasAbsenteesCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasTadaConvMst> ftasTadaConvMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdUniversitymst> fhrdUniversitymstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "systOrgUnitMst")
    private Collection<SystMenumst> systMenumstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasTadaGradeMst> ftasTadaGradeMstCollection;
    @OneToMany(mappedBy = "oumUnitSrno")
    private Collection<FhrdQualification> fhrdQualificationCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasTadaCashDednDtl> ftasTadaCashDednDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdEmpeventRelieve> fhrdEmpeventRelieveCollection;
    @OneToMany(mappedBy = "oumUnitSrno")
    private Collection<FhrdGradedtl> fhrdGradedtlCollection;
    @OneToMany(mappedBy = "adTransOu")
    private Collection<FcadAddrDtl> fcadAddrDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FcadAddrDtl> fcadAddrDtlCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdEmpCategoryMst> fhrdEmpCategoryMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection;
    @OneToMany(mappedBy = "oumUnitSrno")
    private Collection<FhrdEdRepDtl> fhrdEdRepDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdPaytransearndedn> fhrdPaytransearndednCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdDesigmst> fhrdDesigmstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdEarndedn> fhrdEarndednCollection;
    @OneToMany(mappedBy = "oumUnitSrno")
    private Collection<FcadAddrJobStageTitle> fcadAddrJobStageTitleCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdRuleGroupRelation> fhrdRuleGroupRelationCollection;
    @OneToMany(mappedBy = "oumUnitSrno")
    private Collection<FhrdServicemst> fhrdServicemstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdDegreedtl> fhrdDegreedtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdRuleApplGroupMst> fhrdRuleApplGroupMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdPaytransleaveDtl> fhrdPaytransleaveDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<BmwpFundAgencyMst> bmwpFundAgencyMstCollection;
    @OneToMany(mappedBy = "oumUnitSrno")
    private Collection<FtasDaily> ftasDailyCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdEmpleavebal> fhrdEmpleavebalCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<BmwpPartyTypeMst> bmwpPartyTypeMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdReligionMst> fhrdReligionMstCollection;
    @OneToMany(mappedBy = "oumUnitSrno")
    private Collection<FcadPersDtl> fcadPersDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasTadaGradeCityLink> ftasTadaGradeCityLinkCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "refOumUnitSrno")
    private Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<BmwpActivityBudgetLink> bmwpActivityBudgetLinkCollection;
    @OneToMany(mappedBy = "oumUnitSrno")
    private Collection<FcadAddrJobMst> fcadAddrJobMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasTadaFareDtl> ftasTadaFareDtlCollection;
    @OneToMany(mappedBy = "oumUnitSrno")
    private Collection<FtasLockDtl> ftasLockDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdRuleGroupCategory> fhrdRuleGroupCategoryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdDegreeLevelMst> fhrdDegreeLevelMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasTrainingScheduleDtl> ftasTrainingScheduleDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdEarndednmst> fhrdEarndednmstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasTourScheduleDtl> ftasTourScheduleDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection;
    @OneToMany(mappedBy = "oumUnitSrno")
    private Collection<FhrdDegreeStreamMst> fhrdDegreeStreamMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdPaytransleaveMst> fhrdPaytransleaveMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createbyOu")
    private Collection<FcadAddrGrpMst> fcadAddrGrpMstCollection;
    @OneToMany(mappedBy = "agmVisibilityToOu")
    private Collection<FcadAddrGrpMst> fcadAddrGrpMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FcadAddrGrpMst> fcadAddrGrpMstCollection2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "postingOumUnitSrno")
    private Collection<FhrdEmpmst> fhrdEmpmstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<BmwpFundSrcMst> bmwpFundSrcMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasHolidayDtl> ftasHolidayDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<BmwpBudgetMst> bmwpBudgetMstCollection;
    @OneToMany(mappedBy = "oumUnitSrno")
    private Collection<FhrdPortfolioMst> fhrdPortfolioMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<BmwpAgencyTypeMst> bmwpAgencyTypeMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<BmwpAgenSecMst> bmwpAgenSecMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasTadaFacilityDtl> ftasTadaFacilityDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdEventMst> fhrdEventMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdDegreemst> fhrdDegreemstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasHolidayMst> ftasHolidayMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<BmwpApActivityMst> bmwpApActivityMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdCasteCategoryMst> fhrdCasteCategoryMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<BmwpActivityMst> bmwpActivityMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<BmwpPartyTypeDtl> bmwpPartyTypeDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdPaytrans> fhrdPaytransCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdEmpEncash> fhrdEmpEncashCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<BmwpWorkplan> bmwpWorkplanCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "systOrgUnitMst")
    private Collection<FhrdPaytransSummary> fhrdPaytransSummaryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasTrainingOrgUnitDtl> ftasTrainingOrgUnitDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "systOrgUnitMst")
    private Collection<FhrdQualificationLevel> fhrdQualificationLevelCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdEmpeventPromotion> fhrdEmpeventPromotionCollection;
    @OneToMany(mappedBy = "oumUnitSrno")
    private Collection<FhrdGrademst> fhrdGrademstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdEarndednConfig> fhrdEarndednConfigCollection;
    @OneToMany(mappedBy = "applOumUnitSrno")
    private Collection<FhrdEarndednConfig> fhrdEarndednConfigCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fromOumUnitSrno")
    private Collection<FhrdEmpeventTransfer> fhrdEmpeventTransferCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "toOumUnitSrno")
    private Collection<FhrdEmpeventTransfer> fhrdEmpeventTransferCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdEmpeventTransfer> fhrdEmpeventTransferCollection2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<BmwpActivityBudgetDtl> bmwpActivityBudgetDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasTrainingAnnDetail> ftasTrainingAnnDetailCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "systOrgUnitMst")
    private Collection<SystUserTypeMst> systUserTypeMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasTadaCashallowRuleMst> ftasTadaCashallowRuleMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FcadAddrMst> fcadAddrMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<BmwpProjectMst> bmwpProjectMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdEmpearndedn> fhrdEmpearndednCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdLeaverule> fhrdLeaveruleCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FtasTadaConvExpDtl> ftasTadaConvExpDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oumUnitSrno")
    private Collection<FhrdEmpCancEncash> fhrdEmpCancEncashCollection;
    @OneToMany(mappedBy = "oumUnitSrno")
    private Collection<FhrdDependent> fhrdDependentCollection;

    public SystOrgUnitMst() {
    }

    public SystOrgUnitMst(Integer oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public SystOrgUnitMst(Integer oumUnitSrno, int oumNativeOuNo, int oumParentUnitSrnoFk, int createby, Date createdt) {
        this.oumUnitSrno = oumUnitSrno;
        this.oumNativeOuNo = oumNativeOuNo;
        this.oumParentUnitSrnoFk = oumParentUnitSrnoFk;
        this.createby = createby;
        this.createdt = createdt;
    }

    public Integer getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(Integer oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public String getOumName() {
        return oumName;
    }

    public void setOumName(String oumName) {
        this.oumName = oumName;
    }

    public String getOumShortName() {
        return oumShortName;
    }

    public void setOumShortName(String oumShortName) {
        this.oumShortName = oumShortName;
    }

    public Short getOumLocSrno() {
        return oumLocSrno;
    }

    public void setOumLocSrno(Short oumLocSrno) {
        this.oumLocSrno = oumLocSrno;
    }

    public String getOumLocation() {
        return oumLocation;
    }

    public void setOumLocation(String oumLocation) {
        this.oumLocation = oumLocation;
    }

    public Date getOumStartDate() {
        return oumStartDate;
    }

    public void setOumStartDate(Date oumStartDate) {
        this.oumStartDate = oumStartDate;
    }

    public Date getOumEndDate() {
        return oumEndDate;
    }

    public void setOumEndDate(Date oumEndDate) {
        this.oumEndDate = oumEndDate;
    }

    public Date getOumOperStDate() {
        return oumOperStDate;
    }

    public void setOumOperStDate(Date oumOperStDate) {
        this.oumOperStDate = oumOperStDate;
    }

    public Date getOumOperEndDate() {
        return oumOperEndDate;
    }

    public void setOumOperEndDate(Date oumOperEndDate) {
        this.oumOperEndDate = oumOperEndDate;
    }

    public byte[] getOumImage() {
        return oumImage;
    }

    public void setOumImage(byte[] oumImage) {
        this.oumImage = oumImage;
    }

    public int getOumNativeOuNo() {
        return oumNativeOuNo;
    }

    public void setOumNativeOuNo(int oumNativeOuNo) {
        this.oumNativeOuNo = oumNativeOuNo;
    }

    public int getOumParentUnitSrnoFk() {
        return oumParentUnitSrnoFk;
    }

    public void setOumParentUnitSrnoFk(int oumParentUnitSrnoFk) {
        this.oumParentUnitSrnoFk = oumParentUnitSrnoFk;
    }

    public Integer getOumHead() {
        return oumHead;
    }

    public void setOumHead(Integer oumHead) {
        this.oumHead = oumHead;
    }

    public Integer getOumHeadAddrnoFk() {
        return oumHeadAddrnoFk;
    }

    public void setOumHeadAddrnoFk(Integer oumHeadAddrnoFk) {
        this.oumHeadAddrnoFk = oumHeadAddrnoFk;
    }

    public String getOumRemarks() {
        return oumRemarks;
    }

    public void setOumRemarks(String oumRemarks) {
        this.oumRemarks = oumRemarks;
    }

    public int getCreateby() {
        return createby;
    }

    public void setCreateby(int createby) {
        this.createby = createby;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Integer getUpdateby() {
        return updateby;
    }

    public void setUpdateby(Integer updateby) {
        this.updateby = updateby;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    @XmlTransient
    public Collection<FtasTadaOtherDtl> getFtasTadaOtherDtlCollection() {
        return ftasTadaOtherDtlCollection;
    }

    public void setFtasTadaOtherDtlCollection(Collection<FtasTadaOtherDtl> ftasTadaOtherDtlCollection) {
        this.ftasTadaOtherDtlCollection = ftasTadaOtherDtlCollection;
    }

    @XmlTransient
    public Collection<BmwpUnitMst> getBmwpUnitMstCollection() {
        return bmwpUnitMstCollection;
    }

    public void setBmwpUnitMstCollection(Collection<BmwpUnitMst> bmwpUnitMstCollection) {
        this.bmwpUnitMstCollection = bmwpUnitMstCollection;
    }

    public SystOrgUnitTypeMst getSystOrgUnitTypeMst() {
        return systOrgUnitTypeMst;
    }

    public void setSystOrgUnitTypeMst(SystOrgUnitTypeMst systOrgUnitTypeMst) {
        this.systOrgUnitTypeMst = systOrgUnitTypeMst;
    }

    @XmlTransient
    public Collection<FhrdContractGroupRelation> getFhrdContractGroupRelationCollection() {
        return fhrdContractGroupRelationCollection;
    }

    public void setFhrdContractGroupRelationCollection(Collection<FhrdContractGroupRelation> fhrdContractGroupRelationCollection) {
        this.fhrdContractGroupRelationCollection = fhrdContractGroupRelationCollection;
    }

    @XmlTransient
    public Collection<FcadTeleFax> getFcadTeleFaxCollection() {
        return fcadTeleFaxCollection;
    }

    public void setFcadTeleFaxCollection(Collection<FcadTeleFax> fcadTeleFaxCollection) {
        this.fcadTeleFaxCollection = fcadTeleFaxCollection;
    }

    @XmlTransient
    public Collection<FtasLeavemst> getFtasLeavemstCollection() {
        return ftasLeavemstCollection;
    }

    public void setFtasLeavemstCollection(Collection<FtasLeavemst> ftasLeavemstCollection) {
        this.ftasLeavemstCollection = ftasLeavemstCollection;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetLink> getBmwpApActivityBudgetLinkCollection() {
        return bmwpApActivityBudgetLinkCollection;
    }

    public void setBmwpApActivityBudgetLinkCollection(Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection) {
        this.bmwpApActivityBudgetLinkCollection = bmwpApActivityBudgetLinkCollection;
    }

    @XmlTransient
    public Collection<FhrdContractMst> getFhrdContractMstCollection() {
        return fhrdContractMstCollection;
    }

    public void setFhrdContractMstCollection(Collection<FhrdContractMst> fhrdContractMstCollection) {
        this.fhrdContractMstCollection = fhrdContractMstCollection;
    }

    @XmlTransient
    public Collection<BmwpAgencyProjectLink> getBmwpAgencyProjectLinkCollection() {
        return bmwpAgencyProjectLinkCollection;
    }

    public void setBmwpAgencyProjectLinkCollection(Collection<BmwpAgencyProjectLink> bmwpAgencyProjectLinkCollection) {
        this.bmwpAgencyProjectLinkCollection = bmwpAgencyProjectLinkCollection;
    }

    @XmlTransient
    public Collection<BmwpAgencyProjectCategory> getBmwpAgencyProjectCategoryCollection() {
        return bmwpAgencyProjectCategoryCollection;
    }

    public void setBmwpAgencyProjectCategoryCollection(Collection<BmwpAgencyProjectCategory> bmwpAgencyProjectCategoryCollection) {
        this.bmwpAgencyProjectCategoryCollection = bmwpAgencyProjectCategoryCollection;
    }

    @XmlTransient
    public Collection<FhrdExperience> getFhrdExperienceCollection() {
        return fhrdExperienceCollection;
    }

    public void setFhrdExperienceCollection(Collection<FhrdExperience> fhrdExperienceCollection) {
        this.fhrdExperienceCollection = fhrdExperienceCollection;
    }

    @XmlTransient
    public Collection<FcadAddrGrpDtl> getFcadAddrGrpDtlCollection() {
        return fcadAddrGrpDtlCollection;
    }

    public void setFcadAddrGrpDtlCollection(Collection<FcadAddrGrpDtl> fcadAddrGrpDtlCollection) {
        this.fcadAddrGrpDtlCollection = fcadAddrGrpDtlCollection;
    }

    @XmlTransient
    public Collection<FhrdRuleManagGroupMst> getFhrdRuleManagGroupMstCollection() {
        return fhrdRuleManagGroupMstCollection;
    }

    public void setFhrdRuleManagGroupMstCollection(Collection<FhrdRuleManagGroupMst> fhrdRuleManagGroupMstCollection) {
        this.fhrdRuleManagGroupMstCollection = fhrdRuleManagGroupMstCollection;
    }

    @XmlTransient
    public Collection<FtasTadaBillMst> getFtasTadaBillMstCollection() {
        return ftasTadaBillMstCollection;
    }

    public void setFtasTadaBillMstCollection(Collection<FtasTadaBillMst> ftasTadaBillMstCollection) {
        this.ftasTadaBillMstCollection = ftasTadaBillMstCollection;
    }

    @XmlTransient
    public Collection<FhrdPaytransearndednSummary> getFhrdPaytransearndednSummaryCollection() {
        return fhrdPaytransearndednSummaryCollection;
    }

    public void setFhrdPaytransearndednSummaryCollection(Collection<FhrdPaytransearndednSummary> fhrdPaytransearndednSummaryCollection) {
        this.fhrdPaytransearndednSummaryCollection = fhrdPaytransearndednSummaryCollection;
    }

    @XmlTransient
    public Collection<FtasLockMst> getFtasLockMstCollection() {
        return ftasLockMstCollection;
    }

    public void setFtasLockMstCollection(Collection<FtasLockMst> ftasLockMstCollection) {
        this.ftasLockMstCollection = ftasLockMstCollection;
    }

    @XmlTransient
    public Collection<FhrdEdRepMst> getFhrdEdRepMstCollection() {
        return fhrdEdRepMstCollection;
    }

    public void setFhrdEdRepMstCollection(Collection<FhrdEdRepMst> fhrdEdRepMstCollection) {
        this.fhrdEdRepMstCollection = fhrdEdRepMstCollection;
    }

    @XmlTransient
    public Collection<FtasAbsentees> getFtasAbsenteesCollection() {
        return ftasAbsenteesCollection;
    }

    public void setFtasAbsenteesCollection(Collection<FtasAbsentees> ftasAbsenteesCollection) {
        this.ftasAbsenteesCollection = ftasAbsenteesCollection;
    }

    @XmlTransient
    public Collection<FtasAbsentees> getFtasAbsenteesCollection1() {
        return ftasAbsenteesCollection1;
    }

    public void setFtasAbsenteesCollection1(Collection<FtasAbsentees> ftasAbsenteesCollection1) {
        this.ftasAbsenteesCollection1 = ftasAbsenteesCollection1;
    }

    @XmlTransient
    public Collection<FtasTadaConvMst> getFtasTadaConvMstCollection() {
        return ftasTadaConvMstCollection;
    }

    public void setFtasTadaConvMstCollection(Collection<FtasTadaConvMst> ftasTadaConvMstCollection) {
        this.ftasTadaConvMstCollection = ftasTadaConvMstCollection;
    }

    @XmlTransient
    public Collection<FhrdUniversitymst> getFhrdUniversitymstCollection() {
        return fhrdUniversitymstCollection;
    }

    public void setFhrdUniversitymstCollection(Collection<FhrdUniversitymst> fhrdUniversitymstCollection) {
        this.fhrdUniversitymstCollection = fhrdUniversitymstCollection;
    }

    @XmlTransient
    public Collection<SystMenumst> getSystMenumstCollection() {
        return systMenumstCollection;
    }

    public void setSystMenumstCollection(Collection<SystMenumst> systMenumstCollection) {
        this.systMenumstCollection = systMenumstCollection;
    }

    @XmlTransient
    public Collection<FtasTadaGradeMst> getFtasTadaGradeMstCollection() {
        return ftasTadaGradeMstCollection;
    }

    public void setFtasTadaGradeMstCollection(Collection<FtasTadaGradeMst> ftasTadaGradeMstCollection) {
        this.ftasTadaGradeMstCollection = ftasTadaGradeMstCollection;
    }

    @XmlTransient
    public Collection<FhrdQualification> getFhrdQualificationCollection() {
        return fhrdQualificationCollection;
    }

    public void setFhrdQualificationCollection(Collection<FhrdQualification> fhrdQualificationCollection) {
        this.fhrdQualificationCollection = fhrdQualificationCollection;
    }

    @XmlTransient
    public Collection<FtasTadaCashDednDtl> getFtasTadaCashDednDtlCollection() {
        return ftasTadaCashDednDtlCollection;
    }

    public void setFtasTadaCashDednDtlCollection(Collection<FtasTadaCashDednDtl> ftasTadaCashDednDtlCollection) {
        this.ftasTadaCashDednDtlCollection = ftasTadaCashDednDtlCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpeventRelieve> getFhrdEmpeventRelieveCollection() {
        return fhrdEmpeventRelieveCollection;
    }

    public void setFhrdEmpeventRelieveCollection(Collection<FhrdEmpeventRelieve> fhrdEmpeventRelieveCollection) {
        this.fhrdEmpeventRelieveCollection = fhrdEmpeventRelieveCollection;
    }

    @XmlTransient
    public Collection<FhrdGradedtl> getFhrdGradedtlCollection() {
        return fhrdGradedtlCollection;
    }

    public void setFhrdGradedtlCollection(Collection<FhrdGradedtl> fhrdGradedtlCollection) {
        this.fhrdGradedtlCollection = fhrdGradedtlCollection;
    }

    @XmlTransient
    public Collection<FcadAddrDtl> getFcadAddrDtlCollection() {
        return fcadAddrDtlCollection;
    }

    public void setFcadAddrDtlCollection(Collection<FcadAddrDtl> fcadAddrDtlCollection) {
        this.fcadAddrDtlCollection = fcadAddrDtlCollection;
    }

    @XmlTransient
    public Collection<FcadAddrDtl> getFcadAddrDtlCollection1() {
        return fcadAddrDtlCollection1;
    }

    public void setFcadAddrDtlCollection1(Collection<FcadAddrDtl> fcadAddrDtlCollection1) {
        this.fcadAddrDtlCollection1 = fcadAddrDtlCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpCategoryMst> getFhrdEmpCategoryMstCollection() {
        return fhrdEmpCategoryMstCollection;
    }

    public void setFhrdEmpCategoryMstCollection(Collection<FhrdEmpCategoryMst> fhrdEmpCategoryMstCollection) {
        this.fhrdEmpCategoryMstCollection = fhrdEmpCategoryMstCollection;
    }

    @XmlTransient
    public Collection<FtasAbsenteesDtl> getFtasAbsenteesDtlCollection() {
        return ftasAbsenteesDtlCollection;
    }

    public void setFtasAbsenteesDtlCollection(Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection) {
        this.ftasAbsenteesDtlCollection = ftasAbsenteesDtlCollection;
    }

    @XmlTransient
    public Collection<FhrdEdRepDtl> getFhrdEdRepDtlCollection() {
        return fhrdEdRepDtlCollection;
    }

    public void setFhrdEdRepDtlCollection(Collection<FhrdEdRepDtl> fhrdEdRepDtlCollection) {
        this.fhrdEdRepDtlCollection = fhrdEdRepDtlCollection;
    }

    @XmlTransient
    public Collection<FhrdPaytransearndedn> getFhrdPaytransearndednCollection() {
        return fhrdPaytransearndednCollection;
    }

    public void setFhrdPaytransearndednCollection(Collection<FhrdPaytransearndedn> fhrdPaytransearndednCollection) {
        this.fhrdPaytransearndednCollection = fhrdPaytransearndednCollection;
    }

    @XmlTransient
    public Collection<FhrdDesigmst> getFhrdDesigmstCollection() {
        return fhrdDesigmstCollection;
    }

    public void setFhrdDesigmstCollection(Collection<FhrdDesigmst> fhrdDesigmstCollection) {
        this.fhrdDesigmstCollection = fhrdDesigmstCollection;
    }

    @XmlTransient
    public Collection<FhrdEarndedn> getFhrdEarndednCollection() {
        return fhrdEarndednCollection;
    }

    public void setFhrdEarndednCollection(Collection<FhrdEarndedn> fhrdEarndednCollection) {
        this.fhrdEarndednCollection = fhrdEarndednCollection;
    }

    @XmlTransient
    public Collection<FcadAddrJobStageTitle> getFcadAddrJobStageTitleCollection() {
        return fcadAddrJobStageTitleCollection;
    }

    public void setFcadAddrJobStageTitleCollection(Collection<FcadAddrJobStageTitle> fcadAddrJobStageTitleCollection) {
        this.fcadAddrJobStageTitleCollection = fcadAddrJobStageTitleCollection;
    }

    @XmlTransient
    public Collection<FhrdRuleGroupRelation> getFhrdRuleGroupRelationCollection() {
        return fhrdRuleGroupRelationCollection;
    }

    public void setFhrdRuleGroupRelationCollection(Collection<FhrdRuleGroupRelation> fhrdRuleGroupRelationCollection) {
        this.fhrdRuleGroupRelationCollection = fhrdRuleGroupRelationCollection;
    }

    @XmlTransient
    public Collection<FhrdServicemst> getFhrdServicemstCollection() {
        return fhrdServicemstCollection;
    }

    public void setFhrdServicemstCollection(Collection<FhrdServicemst> fhrdServicemstCollection) {
        this.fhrdServicemstCollection = fhrdServicemstCollection;
    }

    @XmlTransient
    public Collection<FhrdDegreedtl> getFhrdDegreedtlCollection() {
        return fhrdDegreedtlCollection;
    }

    public void setFhrdDegreedtlCollection(Collection<FhrdDegreedtl> fhrdDegreedtlCollection) {
        this.fhrdDegreedtlCollection = fhrdDegreedtlCollection;
    }

    @XmlTransient
    public Collection<FhrdRuleApplGroupMst> getFhrdRuleApplGroupMstCollection() {
        return fhrdRuleApplGroupMstCollection;
    }

    public void setFhrdRuleApplGroupMstCollection(Collection<FhrdRuleApplGroupMst> fhrdRuleApplGroupMstCollection) {
        this.fhrdRuleApplGroupMstCollection = fhrdRuleApplGroupMstCollection;
    }

    @XmlTransient
    public Collection<FhrdPaytransleaveDtl> getFhrdPaytransleaveDtlCollection() {
        return fhrdPaytransleaveDtlCollection;
    }

    public void setFhrdPaytransleaveDtlCollection(Collection<FhrdPaytransleaveDtl> fhrdPaytransleaveDtlCollection) {
        this.fhrdPaytransleaveDtlCollection = fhrdPaytransleaveDtlCollection;
    }

    @XmlTransient
    public Collection<BmwpFundAgencyMst> getBmwpFundAgencyMstCollection() {
        return bmwpFundAgencyMstCollection;
    }

    public void setBmwpFundAgencyMstCollection(Collection<BmwpFundAgencyMst> bmwpFundAgencyMstCollection) {
        this.bmwpFundAgencyMstCollection = bmwpFundAgencyMstCollection;
    }

    @XmlTransient
    public Collection<FtasDaily> getFtasDailyCollection() {
        return ftasDailyCollection;
    }

    public void setFtasDailyCollection(Collection<FtasDaily> ftasDailyCollection) {
        this.ftasDailyCollection = ftasDailyCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpleavebal> getFhrdEmpleavebalCollection() {
        return fhrdEmpleavebalCollection;
    }

    public void setFhrdEmpleavebalCollection(Collection<FhrdEmpleavebal> fhrdEmpleavebalCollection) {
        this.fhrdEmpleavebalCollection = fhrdEmpleavebalCollection;
    }

    @XmlTransient
    public Collection<BmwpPartyTypeMst> getBmwpPartyTypeMstCollection() {
        return bmwpPartyTypeMstCollection;
    }

    public void setBmwpPartyTypeMstCollection(Collection<BmwpPartyTypeMst> bmwpPartyTypeMstCollection) {
        this.bmwpPartyTypeMstCollection = bmwpPartyTypeMstCollection;
    }

    @XmlTransient
    public Collection<FhrdReligionMst> getFhrdReligionMstCollection() {
        return fhrdReligionMstCollection;
    }

    public void setFhrdReligionMstCollection(Collection<FhrdReligionMst> fhrdReligionMstCollection) {
        this.fhrdReligionMstCollection = fhrdReligionMstCollection;
    }

    @XmlTransient
    public Collection<FcadPersDtl> getFcadPersDtlCollection() {
        return fcadPersDtlCollection;
    }

    public void setFcadPersDtlCollection(Collection<FcadPersDtl> fcadPersDtlCollection) {
        this.fcadPersDtlCollection = fcadPersDtlCollection;
    }

    @XmlTransient
    public Collection<FtasTadaGradeCityLink> getFtasTadaGradeCityLinkCollection() {
        return ftasTadaGradeCityLinkCollection;
    }

    public void setFtasTadaGradeCityLinkCollection(Collection<FtasTadaGradeCityLink> ftasTadaGradeCityLinkCollection) {
        this.ftasTadaGradeCityLinkCollection = ftasTadaGradeCityLinkCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpleaverule> getFhrdEmpleaveruleCollection() {
        return fhrdEmpleaveruleCollection;
    }

    public void setFhrdEmpleaveruleCollection(Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection) {
        this.fhrdEmpleaveruleCollection = fhrdEmpleaveruleCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpleaverule> getFhrdEmpleaveruleCollection1() {
        return fhrdEmpleaveruleCollection1;
    }

    public void setFhrdEmpleaveruleCollection1(Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection1) {
        this.fhrdEmpleaveruleCollection1 = fhrdEmpleaveruleCollection1;
    }

    @XmlTransient
    public Collection<BmwpActivityBudgetLink> getBmwpActivityBudgetLinkCollection() {
        return bmwpActivityBudgetLinkCollection;
    }

    public void setBmwpActivityBudgetLinkCollection(Collection<BmwpActivityBudgetLink> bmwpActivityBudgetLinkCollection) {
        this.bmwpActivityBudgetLinkCollection = bmwpActivityBudgetLinkCollection;
    }

    @XmlTransient
    public Collection<FcadAddrJobMst> getFcadAddrJobMstCollection() {
        return fcadAddrJobMstCollection;
    }

    public void setFcadAddrJobMstCollection(Collection<FcadAddrJobMst> fcadAddrJobMstCollection) {
        this.fcadAddrJobMstCollection = fcadAddrJobMstCollection;
    }

    @XmlTransient
    public Collection<FtasTadaFareDtl> getFtasTadaFareDtlCollection() {
        return ftasTadaFareDtlCollection;
    }

    public void setFtasTadaFareDtlCollection(Collection<FtasTadaFareDtl> ftasTadaFareDtlCollection) {
        this.ftasTadaFareDtlCollection = ftasTadaFareDtlCollection;
    }

    @XmlTransient
    public Collection<FtasLockDtl> getFtasLockDtlCollection() {
        return ftasLockDtlCollection;
    }

    public void setFtasLockDtlCollection(Collection<FtasLockDtl> ftasLockDtlCollection) {
        this.ftasLockDtlCollection = ftasLockDtlCollection;
    }

    @XmlTransient
    public Collection<FhrdRuleGroupCategory> getFhrdRuleGroupCategoryCollection() {
        return fhrdRuleGroupCategoryCollection;
    }

    public void setFhrdRuleGroupCategoryCollection(Collection<FhrdRuleGroupCategory> fhrdRuleGroupCategoryCollection) {
        this.fhrdRuleGroupCategoryCollection = fhrdRuleGroupCategoryCollection;
    }

    @XmlTransient
    public Collection<FhrdDegreeLevelMst> getFhrdDegreeLevelMstCollection() {
        return fhrdDegreeLevelMstCollection;
    }

    public void setFhrdDegreeLevelMstCollection(Collection<FhrdDegreeLevelMst> fhrdDegreeLevelMstCollection) {
        this.fhrdDegreeLevelMstCollection = fhrdDegreeLevelMstCollection;
    }

    @XmlTransient
    public Collection<FtasTrainingScheduleDtl> getFtasTrainingScheduleDtlCollection() {
        return ftasTrainingScheduleDtlCollection;
    }

    public void setFtasTrainingScheduleDtlCollection(Collection<FtasTrainingScheduleDtl> ftasTrainingScheduleDtlCollection) {
        this.ftasTrainingScheduleDtlCollection = ftasTrainingScheduleDtlCollection;
    }

    @XmlTransient
    public Collection<FhrdEarndednmst> getFhrdEarndednmstCollection() {
        return fhrdEarndednmstCollection;
    }

    public void setFhrdEarndednmstCollection(Collection<FhrdEarndednmst> fhrdEarndednmstCollection) {
        this.fhrdEarndednmstCollection = fhrdEarndednmstCollection;
    }

    @XmlTransient
    public Collection<FtasTourScheduleDtl> getFtasTourScheduleDtlCollection() {
        return ftasTourScheduleDtlCollection;
    }

    public void setFtasTourScheduleDtlCollection(Collection<FtasTourScheduleDtl> ftasTourScheduleDtlCollection) {
        this.ftasTourScheduleDtlCollection = ftasTourScheduleDtlCollection;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetDtl> getBmwpApActivityBudgetDtlCollection() {
        return bmwpApActivityBudgetDtlCollection;
    }

    public void setBmwpApActivityBudgetDtlCollection(Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection) {
        this.bmwpApActivityBudgetDtlCollection = bmwpApActivityBudgetDtlCollection;
    }

    @XmlTransient
    public Collection<FhrdDegreeStreamMst> getFhrdDegreeStreamMstCollection() {
        return fhrdDegreeStreamMstCollection;
    }

    public void setFhrdDegreeStreamMstCollection(Collection<FhrdDegreeStreamMst> fhrdDegreeStreamMstCollection) {
        this.fhrdDegreeStreamMstCollection = fhrdDegreeStreamMstCollection;
    }

    @XmlTransient
    public Collection<FhrdPaytransleaveMst> getFhrdPaytransleaveMstCollection() {
        return fhrdPaytransleaveMstCollection;
    }

    public void setFhrdPaytransleaveMstCollection(Collection<FhrdPaytransleaveMst> fhrdPaytransleaveMstCollection) {
        this.fhrdPaytransleaveMstCollection = fhrdPaytransleaveMstCollection;
    }

    @XmlTransient
    public Collection<FcadAddrGrpMst> getFcadAddrGrpMstCollection() {
        return fcadAddrGrpMstCollection;
    }

    public void setFcadAddrGrpMstCollection(Collection<FcadAddrGrpMst> fcadAddrGrpMstCollection) {
        this.fcadAddrGrpMstCollection = fcadAddrGrpMstCollection;
    }

    @XmlTransient
    public Collection<FcadAddrGrpMst> getFcadAddrGrpMstCollection1() {
        return fcadAddrGrpMstCollection1;
    }

    public void setFcadAddrGrpMstCollection1(Collection<FcadAddrGrpMst> fcadAddrGrpMstCollection1) {
        this.fcadAddrGrpMstCollection1 = fcadAddrGrpMstCollection1;
    }

    @XmlTransient
    public Collection<FcadAddrGrpMst> getFcadAddrGrpMstCollection2() {
        return fcadAddrGrpMstCollection2;
    }

    public void setFcadAddrGrpMstCollection2(Collection<FcadAddrGrpMst> fcadAddrGrpMstCollection2) {
        this.fcadAddrGrpMstCollection2 = fcadAddrGrpMstCollection2;
    }

    @XmlTransient
    public Collection<FhrdEmpmst> getFhrdEmpmstCollection() {
        return fhrdEmpmstCollection;
    }

    public void setFhrdEmpmstCollection(Collection<FhrdEmpmst> fhrdEmpmstCollection) {
        this.fhrdEmpmstCollection = fhrdEmpmstCollection;
    }

    @XmlTransient
    public Collection<BmwpFundSrcMst> getBmwpFundSrcMstCollection() {
        return bmwpFundSrcMstCollection;
    }

    public void setBmwpFundSrcMstCollection(Collection<BmwpFundSrcMst> bmwpFundSrcMstCollection) {
        this.bmwpFundSrcMstCollection = bmwpFundSrcMstCollection;
    }

    @XmlTransient
    public Collection<FtasHolidayDtl> getFtasHolidayDtlCollection() {
        return ftasHolidayDtlCollection;
    }

    public void setFtasHolidayDtlCollection(Collection<FtasHolidayDtl> ftasHolidayDtlCollection) {
        this.ftasHolidayDtlCollection = ftasHolidayDtlCollection;
    }

    @XmlTransient
    public Collection<BmwpBudgetMst> getBmwpBudgetMstCollection() {
        return bmwpBudgetMstCollection;
    }

    public void setBmwpBudgetMstCollection(Collection<BmwpBudgetMst> bmwpBudgetMstCollection) {
        this.bmwpBudgetMstCollection = bmwpBudgetMstCollection;
    }

    @XmlTransient
    public Collection<FhrdPortfolioMst> getFhrdPortfolioMstCollection() {
        return fhrdPortfolioMstCollection;
    }

    public void setFhrdPortfolioMstCollection(Collection<FhrdPortfolioMst> fhrdPortfolioMstCollection) {
        this.fhrdPortfolioMstCollection = fhrdPortfolioMstCollection;
    }

    @XmlTransient
    public Collection<BmwpAgencyTypeMst> getBmwpAgencyTypeMstCollection() {
        return bmwpAgencyTypeMstCollection;
    }

    public void setBmwpAgencyTypeMstCollection(Collection<BmwpAgencyTypeMst> bmwpAgencyTypeMstCollection) {
        this.bmwpAgencyTypeMstCollection = bmwpAgencyTypeMstCollection;
    }

    @XmlTransient
    public Collection<BmwpAgenSecMst> getBmwpAgenSecMstCollection() {
        return bmwpAgenSecMstCollection;
    }

    public void setBmwpAgenSecMstCollection(Collection<BmwpAgenSecMst> bmwpAgenSecMstCollection) {
        this.bmwpAgenSecMstCollection = bmwpAgenSecMstCollection;
    }

    @XmlTransient
    public Collection<FtasTadaFacilityDtl> getFtasTadaFacilityDtlCollection() {
        return ftasTadaFacilityDtlCollection;
    }

    public void setFtasTadaFacilityDtlCollection(Collection<FtasTadaFacilityDtl> ftasTadaFacilityDtlCollection) {
        this.ftasTadaFacilityDtlCollection = ftasTadaFacilityDtlCollection;
    }

    @XmlTransient
    public Collection<FhrdEventMst> getFhrdEventMstCollection() {
        return fhrdEventMstCollection;
    }

    public void setFhrdEventMstCollection(Collection<FhrdEventMst> fhrdEventMstCollection) {
        this.fhrdEventMstCollection = fhrdEventMstCollection;
    }

    @XmlTransient
    public Collection<FhrdDegreemst> getFhrdDegreemstCollection() {
        return fhrdDegreemstCollection;
    }

    public void setFhrdDegreemstCollection(Collection<FhrdDegreemst> fhrdDegreemstCollection) {
        this.fhrdDegreemstCollection = fhrdDegreemstCollection;
    }

    @XmlTransient
    public Collection<FtasHolidayMst> getFtasHolidayMstCollection() {
        return ftasHolidayMstCollection;
    }

    public void setFtasHolidayMstCollection(Collection<FtasHolidayMst> ftasHolidayMstCollection) {
        this.ftasHolidayMstCollection = ftasHolidayMstCollection;
    }

    @XmlTransient
    public Collection<BmwpApActivityMst> getBmwpApActivityMstCollection() {
        return bmwpApActivityMstCollection;
    }

    public void setBmwpApActivityMstCollection(Collection<BmwpApActivityMst> bmwpApActivityMstCollection) {
        this.bmwpApActivityMstCollection = bmwpApActivityMstCollection;
    }

    @XmlTransient
    public Collection<FhrdCasteCategoryMst> getFhrdCasteCategoryMstCollection() {
        return fhrdCasteCategoryMstCollection;
    }

    public void setFhrdCasteCategoryMstCollection(Collection<FhrdCasteCategoryMst> fhrdCasteCategoryMstCollection) {
        this.fhrdCasteCategoryMstCollection = fhrdCasteCategoryMstCollection;
    }

    @XmlTransient
    public Collection<BmwpActivityMst> getBmwpActivityMstCollection() {
        return bmwpActivityMstCollection;
    }

    public void setBmwpActivityMstCollection(Collection<BmwpActivityMst> bmwpActivityMstCollection) {
        this.bmwpActivityMstCollection = bmwpActivityMstCollection;
    }

    @XmlTransient
    public Collection<BmwpPartyTypeDtl> getBmwpPartyTypeDtlCollection() {
        return bmwpPartyTypeDtlCollection;
    }

    public void setBmwpPartyTypeDtlCollection(Collection<BmwpPartyTypeDtl> bmwpPartyTypeDtlCollection) {
        this.bmwpPartyTypeDtlCollection = bmwpPartyTypeDtlCollection;
    }

    @XmlTransient
    public Collection<FhrdPaytrans> getFhrdPaytransCollection() {
        return fhrdPaytransCollection;
    }

    public void setFhrdPaytransCollection(Collection<FhrdPaytrans> fhrdPaytransCollection) {
        this.fhrdPaytransCollection = fhrdPaytransCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpEncash> getFhrdEmpEncashCollection() {
        return fhrdEmpEncashCollection;
    }

    public void setFhrdEmpEncashCollection(Collection<FhrdEmpEncash> fhrdEmpEncashCollection) {
        this.fhrdEmpEncashCollection = fhrdEmpEncashCollection;
    }

    @XmlTransient
    public Collection<BmwpApBudgetMst> getBmwpApBudgetMstCollection() {
        return bmwpApBudgetMstCollection;
    }

    public void setBmwpApBudgetMstCollection(Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection) {
        this.bmwpApBudgetMstCollection = bmwpApBudgetMstCollection;
    }

    @XmlTransient
    public Collection<BmwpWorkplan> getBmwpWorkplanCollection() {
        return bmwpWorkplanCollection;
    }

    public void setBmwpWorkplanCollection(Collection<BmwpWorkplan> bmwpWorkplanCollection) {
        this.bmwpWorkplanCollection = bmwpWorkplanCollection;
    }

    @XmlTransient
    public Collection<FhrdPaytransSummary> getFhrdPaytransSummaryCollection() {
        return fhrdPaytransSummaryCollection;
    }

    public void setFhrdPaytransSummaryCollection(Collection<FhrdPaytransSummary> fhrdPaytransSummaryCollection) {
        this.fhrdPaytransSummaryCollection = fhrdPaytransSummaryCollection;
    }

    @XmlTransient
    public Collection<FtasTrainingOrgUnitDtl> getFtasTrainingOrgUnitDtlCollection() {
        return ftasTrainingOrgUnitDtlCollection;
    }

    public void setFtasTrainingOrgUnitDtlCollection(Collection<FtasTrainingOrgUnitDtl> ftasTrainingOrgUnitDtlCollection) {
        this.ftasTrainingOrgUnitDtlCollection = ftasTrainingOrgUnitDtlCollection;
    }

    @XmlTransient
    public Collection<FhrdQualificationLevel> getFhrdQualificationLevelCollection() {
        return fhrdQualificationLevelCollection;
    }

    public void setFhrdQualificationLevelCollection(Collection<FhrdQualificationLevel> fhrdQualificationLevelCollection) {
        this.fhrdQualificationLevelCollection = fhrdQualificationLevelCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpeventPromotion> getFhrdEmpeventPromotionCollection() {
        return fhrdEmpeventPromotionCollection;
    }

    public void setFhrdEmpeventPromotionCollection(Collection<FhrdEmpeventPromotion> fhrdEmpeventPromotionCollection) {
        this.fhrdEmpeventPromotionCollection = fhrdEmpeventPromotionCollection;
    }

    @XmlTransient
    public Collection<FhrdGrademst> getFhrdGrademstCollection() {
        return fhrdGrademstCollection;
    }

    public void setFhrdGrademstCollection(Collection<FhrdGrademst> fhrdGrademstCollection) {
        this.fhrdGrademstCollection = fhrdGrademstCollection;
    }

    @XmlTransient
    public Collection<FhrdEarndednConfig> getFhrdEarndednConfigCollection() {
        return fhrdEarndednConfigCollection;
    }

    public void setFhrdEarndednConfigCollection(Collection<FhrdEarndednConfig> fhrdEarndednConfigCollection) {
        this.fhrdEarndednConfigCollection = fhrdEarndednConfigCollection;
    }

    @XmlTransient
    public Collection<FhrdEarndednConfig> getFhrdEarndednConfigCollection1() {
        return fhrdEarndednConfigCollection1;
    }

    public void setFhrdEarndednConfigCollection1(Collection<FhrdEarndednConfig> fhrdEarndednConfigCollection1) {
        this.fhrdEarndednConfigCollection1 = fhrdEarndednConfigCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpeventTransfer> getFhrdEmpeventTransferCollection() {
        return fhrdEmpeventTransferCollection;
    }

    public void setFhrdEmpeventTransferCollection(Collection<FhrdEmpeventTransfer> fhrdEmpeventTransferCollection) {
        this.fhrdEmpeventTransferCollection = fhrdEmpeventTransferCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpeventTransfer> getFhrdEmpeventTransferCollection1() {
        return fhrdEmpeventTransferCollection1;
    }

    public void setFhrdEmpeventTransferCollection1(Collection<FhrdEmpeventTransfer> fhrdEmpeventTransferCollection1) {
        this.fhrdEmpeventTransferCollection1 = fhrdEmpeventTransferCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpeventTransfer> getFhrdEmpeventTransferCollection2() {
        return fhrdEmpeventTransferCollection2;
    }

    public void setFhrdEmpeventTransferCollection2(Collection<FhrdEmpeventTransfer> fhrdEmpeventTransferCollection2) {
        this.fhrdEmpeventTransferCollection2 = fhrdEmpeventTransferCollection2;
    }

    @XmlTransient
    public Collection<BmwpActivityBudgetDtl> getBmwpActivityBudgetDtlCollection() {
        return bmwpActivityBudgetDtlCollection;
    }

    public void setBmwpActivityBudgetDtlCollection(Collection<BmwpActivityBudgetDtl> bmwpActivityBudgetDtlCollection) {
        this.bmwpActivityBudgetDtlCollection = bmwpActivityBudgetDtlCollection;
    }

    @XmlTransient
    public Collection<FtasTrainingAnnDetail> getFtasTrainingAnnDetailCollection() {
        return ftasTrainingAnnDetailCollection;
    }

    public void setFtasTrainingAnnDetailCollection(Collection<FtasTrainingAnnDetail> ftasTrainingAnnDetailCollection) {
        this.ftasTrainingAnnDetailCollection = ftasTrainingAnnDetailCollection;
    }

    @XmlTransient
    public Collection<SystUserTypeMst> getSystUserTypeMstCollection() {
        return systUserTypeMstCollection;
    }

    public void setSystUserTypeMstCollection(Collection<SystUserTypeMst> systUserTypeMstCollection) {
        this.systUserTypeMstCollection = systUserTypeMstCollection;
    }

    @XmlTransient
    public Collection<FtasTadaCashallowRuleMst> getFtasTadaCashallowRuleMstCollection() {
        return ftasTadaCashallowRuleMstCollection;
    }

    public void setFtasTadaCashallowRuleMstCollection(Collection<FtasTadaCashallowRuleMst> ftasTadaCashallowRuleMstCollection) {
        this.ftasTadaCashallowRuleMstCollection = ftasTadaCashallowRuleMstCollection;
    }

    @XmlTransient
    public Collection<FcadAddrMst> getFcadAddrMstCollection() {
        return fcadAddrMstCollection;
    }

    public void setFcadAddrMstCollection(Collection<FcadAddrMst> fcadAddrMstCollection) {
        this.fcadAddrMstCollection = fcadAddrMstCollection;
    }

    @XmlTransient
    public Collection<BmwpProjectMst> getBmwpProjectMstCollection() {
        return bmwpProjectMstCollection;
    }

    public void setBmwpProjectMstCollection(Collection<BmwpProjectMst> bmwpProjectMstCollection) {
        this.bmwpProjectMstCollection = bmwpProjectMstCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpearndedn> getFhrdEmpearndednCollection() {
        return fhrdEmpearndednCollection;
    }

    public void setFhrdEmpearndednCollection(Collection<FhrdEmpearndedn> fhrdEmpearndednCollection) {
        this.fhrdEmpearndednCollection = fhrdEmpearndednCollection;
    }

    @XmlTransient
    public Collection<FhrdLeaverule> getFhrdLeaveruleCollection() {
        return fhrdLeaveruleCollection;
    }

    public void setFhrdLeaveruleCollection(Collection<FhrdLeaverule> fhrdLeaveruleCollection) {
        this.fhrdLeaveruleCollection = fhrdLeaveruleCollection;
    }

    @XmlTransient
    public Collection<FtasTadaConvExpDtl> getFtasTadaConvExpDtlCollection() {
        return ftasTadaConvExpDtlCollection;
    }

    public void setFtasTadaConvExpDtlCollection(Collection<FtasTadaConvExpDtl> ftasTadaConvExpDtlCollection) {
        this.ftasTadaConvExpDtlCollection = ftasTadaConvExpDtlCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpCancEncash> getFhrdEmpCancEncashCollection() {
        return fhrdEmpCancEncashCollection;
    }

    public void setFhrdEmpCancEncashCollection(Collection<FhrdEmpCancEncash> fhrdEmpCancEncashCollection) {
        this.fhrdEmpCancEncashCollection = fhrdEmpCancEncashCollection;
    }

    @XmlTransient
    public Collection<FhrdDependent> getFhrdDependentCollection() {
        return fhrdDependentCollection;
    }

    public void setFhrdDependentCollection(Collection<FhrdDependent> fhrdDependentCollection) {
        this.fhrdDependentCollection = fhrdDependentCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (oumUnitSrno != null ? oumUnitSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SystOrgUnitMst)) {
            return false;
        }
        SystOrgUnitMst other = (SystOrgUnitMst) object;
        if ((this.oumUnitSrno == null && other.oumUnitSrno != null) || (this.oumUnitSrno != null && !this.oumUnitSrno.equals(other.oumUnitSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.SystOrgUnitMst[ oumUnitSrno=" + oumUnitSrno + " ]";
    }
    
}
