/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "BMWP_PROJECT_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmwpProjectMst.findAll", query = "SELECT b FROM BmwpProjectMst b"),
    @NamedQuery(name = "BmwpProjectMst.findByPmUniqueSrno", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmUniqueSrno = :pmUniqueSrno"),
    @NamedQuery(name = "BmwpProjectMst.findByPmSrno", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmSrno = :pmSrno"),
    @NamedQuery(name = "BmwpProjectMst.findByPmProjShortName", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmProjShortName = :pmProjShortName"),
    @NamedQuery(name = "BmwpProjectMst.findByPmProjName", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmProjName = :pmProjName"),
    @NamedQuery(name = "BmwpProjectMst.findByPmFundType", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmFundType = :pmFundType"),
    @NamedQuery(name = "BmwpProjectMst.findByPmRemarks", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmRemarks = :pmRemarks"),
    @NamedQuery(name = "BmwpProjectMst.findByPmMouFundAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmMouFundAmt = :pmMouFundAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmReceivedFundAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmReceivedFundAmt = :pmReceivedFundAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmToReceivedFundAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmToReceivedFundAmt = :pmToReceivedFundAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmSurrenderFundAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmSurrenderFundAmt = :pmSurrenderFundAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmTotalFundAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmTotalFundAmt = :pmTotalFundAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmWorkplanAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmWorkplanAmt = :pmWorkplanAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmWpSurrenderAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmWpSurrenderAmt = :pmWpSurrenderAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmReAllotWpAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmReAllotWpAmt = :pmReAllotWpAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmOfferWpAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmOfferWpAmt = :pmOfferWpAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmReviseWpAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmReviseWpAmt = :pmReviseWpAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmCommitedAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmCommitedAmt = :pmCommitedAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmPipelinePayAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmPipelinePayAmt = :pmPipelinePayAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmAdvanceCommitAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmAdvanceCommitAmt = :pmAdvanceCommitAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmAdvancePayAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmAdvancePayAmt = :pmAdvancePayAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmPipeAdvadjCommitAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmPipeAdvadjCommitAmt = :pmPipeAdvadjCommitAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmPipeAdvadjPayAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmPipeAdvadjPayAmt = :pmPipeAdvadjPayAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmReceiptPayAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmReceiptPayAmt = :pmReceiptPayAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmReceiptCommitAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmReceiptCommitAmt = :pmReceiptCommitAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmExpenseAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmExpenseAmt = :pmExpenseAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmUsableBalanceAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmUsableBalanceAmt = :pmUsableBalanceAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmBalanceAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmBalanceAmt = :pmBalanceAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByCreatedt", query = "SELECT b FROM BmwpProjectMst b WHERE b.createdt = :createdt"),
    @NamedQuery(name = "BmwpProjectMst.findByUpdatedt", query = "SELECT b FROM BmwpProjectMst b WHERE b.updatedt = :updatedt"),
    @NamedQuery(name = "BmwpProjectMst.findBySystemUserid", query = "SELECT b FROM BmwpProjectMst b WHERE b.systemUserid = :systemUserid"),
    @NamedQuery(name = "BmwpProjectMst.findByExportFlag", query = "SELECT b FROM BmwpProjectMst b WHERE b.exportFlag = :exportFlag"),
    @NamedQuery(name = "BmwpProjectMst.findByImportFlag", query = "SELECT b FROM BmwpProjectMst b WHERE b.importFlag = :importFlag"),
    @NamedQuery(name = "BmwpProjectMst.findByPmPipeCommitedAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmPipeCommitedAmt = :pmPipeCommitedAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmRelationFlag", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmRelationFlag = :pmRelationFlag"),
    @NamedQuery(name = "BmwpProjectMst.findByPmStartDate", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmStartDate = :pmStartDate"),
    @NamedQuery(name = "BmwpProjectMst.findByPmEndDate", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmEndDate = :pmEndDate"),
    @NamedQuery(name = "BmwpProjectMst.findByPmEndDatePossible", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmEndDatePossible = :pmEndDatePossible"),
    @NamedQuery(name = "BmwpProjectMst.findByPmTransactionDate", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmTransactionDate = :pmTransactionDate"),
    @NamedQuery(name = "BmwpProjectMst.findByPmProCrExpAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmProCrExpAmt = :pmProCrExpAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmReceivedAdvanceAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmReceivedAdvanceAmt = :pmReceivedAdvanceAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmCarryFwdAdvAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmCarryFwdAdvAmt = :pmCarryFwdAdvAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmCommContriAmt", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmCommContriAmt = :pmCommContriAmt"),
    @NamedQuery(name = "BmwpProjectMst.findByPmFundMonitorFlag", query = "SELECT b FROM BmwpProjectMst b WHERE b.pmFundMonitorFlag = :pmFundMonitorFlag")})
public class BmwpProjectMst implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_UNIQUE_SRNO")
    private BigDecimal pmUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_SRNO")
    private int pmSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "PM_PROJ_SHORT_NAME")
    private String pmProjShortName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "PM_PROJ_NAME")
    private String pmProjName;
    @Size(max = 1)
    @Column(name = "PM_FUND_TYPE")
    private String pmFundType;
    @Size(max = 256)
    @Column(name = "PM_REMARKS")
    private String pmRemarks;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_MOU_FUND_AMT")
    private BigDecimal pmMouFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_RECEIVED_FUND_AMT")
    private BigDecimal pmReceivedFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_TO_RECEIVED_FUND_AMT")
    private BigDecimal pmToReceivedFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_SURRENDER_FUND_AMT")
    private BigDecimal pmSurrenderFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_TOTAL_FUND_AMT")
    private BigDecimal pmTotalFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_WORKPLAN_AMT")
    private BigDecimal pmWorkplanAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_WP_SURRENDER_AMT")
    private BigDecimal pmWpSurrenderAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_RE_ALLOT_WP_AMT")
    private BigDecimal pmReAllotWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_OFFER_WP_AMT")
    private BigDecimal pmOfferWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_REVISE_WP_AMT")
    private BigDecimal pmReviseWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_COMMITED_AMT")
    private BigDecimal pmCommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_PIPELINE_PAY_AMT")
    private BigDecimal pmPipelinePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_ADVANCE_COMMIT_AMT")
    private BigDecimal pmAdvanceCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_ADVANCE_PAY_AMT")
    private BigDecimal pmAdvancePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_PIPE_ADVADJ_COMMIT_AMT")
    private BigDecimal pmPipeAdvadjCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_PIPE_ADVADJ_PAY_AMT")
    private BigDecimal pmPipeAdvadjPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_RECEIPT_PAY_AMT")
    private BigDecimal pmReceiptPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_RECEIPT_COMMIT_AMT")
    private BigDecimal pmReceiptCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_EXPENSE_AMT")
    private BigDecimal pmExpenseAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_USABLE_BALANCE_AMT")
    private BigDecimal pmUsableBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_BALANCE_AMT")
    private BigDecimal pmBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_PIPE_COMMITED_AMT")
    private BigDecimal pmPipeCommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PM_RELATION_FLAG")
    private String pmRelationFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pmStartDate;
    @Column(name = "PM_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pmEndDate;
    @Column(name = "PM_END_DATE_POSSIBLE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pmEndDatePossible;
    @Column(name = "PM_TRANSACTION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pmTransactionDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_PRO_CR_EXP_AMT")
    private BigDecimal pmProCrExpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_RECEIVED_ADVANCE_AMT")
    private BigDecimal pmReceivedAdvanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_CARRY_FWD_ADV_AMT")
    private BigDecimal pmCarryFwdAdvAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_COMM_CONTRI_AMT")
    private BigDecimal pmCommContriAmt;
    @Size(max = 1)
    @Column(name = "PM_FUND_MONITOR_FLAG")
    private String pmFundMonitorFlag;
    @OneToMany(mappedBy = "apablPmUniqueSrno")
    private Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "aplPmUniqueSrno")
    private Collection<BmwpAgencyProjectLink> bmwpAgencyProjectLinkCollection;
    @OneToMany(mappedBy = "apabdPmUniqueSrno")
    private Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection;
    @OneToMany(mappedBy = "apamPmUniqueSrno")
    private Collection<BmwpApActivityMst> bmwpApActivityMstCollection;
    @OneToMany(mappedBy = "apbmPmUniqueSrno")
    private Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection;
    @OneToMany(mappedBy = "wpPmUniqueSrno")
    private Collection<BmwpWorkplan> bmwpWorkplanCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @OneToMany(mappedBy = "pmParentUniqueSrno")
    private Collection<BmwpProjectMst> bmwpProjectMstCollection;
    @JoinColumn(name = "PM_PARENT_UNIQUE_SRNO", referencedColumnName = "PM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpProjectMst pmParentUniqueSrno;
    @JoinColumn(name = "PM_FSM_UNIQUE_SRNO", referencedColumnName = "FSM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpFundSrcMst pmFsmUniqueSrno;

    public BmwpProjectMst() {
    }

    public BmwpProjectMst(BigDecimal pmUniqueSrno) {
        this.pmUniqueSrno = pmUniqueSrno;
    }

    public BmwpProjectMst(BigDecimal pmUniqueSrno, int pmSrno, String pmProjShortName, String pmProjName, BigDecimal pmMouFundAmt, BigDecimal pmReceivedFundAmt, BigDecimal pmToReceivedFundAmt, BigDecimal pmSurrenderFundAmt, BigDecimal pmTotalFundAmt, BigDecimal pmWorkplanAmt, BigDecimal pmWpSurrenderAmt, BigDecimal pmReAllotWpAmt, BigDecimal pmOfferWpAmt, BigDecimal pmReviseWpAmt, BigDecimal pmCommitedAmt, BigDecimal pmPipelinePayAmt, BigDecimal pmAdvanceCommitAmt, BigDecimal pmAdvancePayAmt, BigDecimal pmPipeAdvadjCommitAmt, BigDecimal pmPipeAdvadjPayAmt, BigDecimal pmReceiptPayAmt, BigDecimal pmReceiptCommitAmt, BigDecimal pmExpenseAmt, BigDecimal pmUsableBalanceAmt, BigDecimal pmBalanceAmt, Date createdt, BigDecimal pmPipeCommitedAmt, String pmRelationFlag, Date pmStartDate, BigDecimal pmProCrExpAmt, BigDecimal pmReceivedAdvanceAmt, BigDecimal pmCarryFwdAdvAmt, BigDecimal pmCommContriAmt) {
        this.pmUniqueSrno = pmUniqueSrno;
        this.pmSrno = pmSrno;
        this.pmProjShortName = pmProjShortName;
        this.pmProjName = pmProjName;
        this.pmMouFundAmt = pmMouFundAmt;
        this.pmReceivedFundAmt = pmReceivedFundAmt;
        this.pmToReceivedFundAmt = pmToReceivedFundAmt;
        this.pmSurrenderFundAmt = pmSurrenderFundAmt;
        this.pmTotalFundAmt = pmTotalFundAmt;
        this.pmWorkplanAmt = pmWorkplanAmt;
        this.pmWpSurrenderAmt = pmWpSurrenderAmt;
        this.pmReAllotWpAmt = pmReAllotWpAmt;
        this.pmOfferWpAmt = pmOfferWpAmt;
        this.pmReviseWpAmt = pmReviseWpAmt;
        this.pmCommitedAmt = pmCommitedAmt;
        this.pmPipelinePayAmt = pmPipelinePayAmt;
        this.pmAdvanceCommitAmt = pmAdvanceCommitAmt;
        this.pmAdvancePayAmt = pmAdvancePayAmt;
        this.pmPipeAdvadjCommitAmt = pmPipeAdvadjCommitAmt;
        this.pmPipeAdvadjPayAmt = pmPipeAdvadjPayAmt;
        this.pmReceiptPayAmt = pmReceiptPayAmt;
        this.pmReceiptCommitAmt = pmReceiptCommitAmt;
        this.pmExpenseAmt = pmExpenseAmt;
        this.pmUsableBalanceAmt = pmUsableBalanceAmt;
        this.pmBalanceAmt = pmBalanceAmt;
        this.createdt = createdt;
        this.pmPipeCommitedAmt = pmPipeCommitedAmt;
        this.pmRelationFlag = pmRelationFlag;
        this.pmStartDate = pmStartDate;
        this.pmProCrExpAmt = pmProCrExpAmt;
        this.pmReceivedAdvanceAmt = pmReceivedAdvanceAmt;
        this.pmCarryFwdAdvAmt = pmCarryFwdAdvAmt;
        this.pmCommContriAmt = pmCommContriAmt;
    }

    public BigDecimal getPmUniqueSrno() {
        return pmUniqueSrno;
    }

    public void setPmUniqueSrno(BigDecimal pmUniqueSrno) {
        this.pmUniqueSrno = pmUniqueSrno;
    }

    public int getPmSrno() {
        return pmSrno;
    }

    public void setPmSrno(int pmSrno) {
        this.pmSrno = pmSrno;
    }

    public String getPmProjShortName() {
        return pmProjShortName;
    }

    public void setPmProjShortName(String pmProjShortName) {
        this.pmProjShortName = pmProjShortName;
    }

    public String getPmProjName() {
        return pmProjName;
    }

    public void setPmProjName(String pmProjName) {
        this.pmProjName = pmProjName;
    }

    public String getPmFundType() {
        return pmFundType;
    }

    public void setPmFundType(String pmFundType) {
        this.pmFundType = pmFundType;
    }

    public String getPmRemarks() {
        return pmRemarks;
    }

    public void setPmRemarks(String pmRemarks) {
        this.pmRemarks = pmRemarks;
    }

    public BigDecimal getPmMouFundAmt() {
        return pmMouFundAmt;
    }

    public void setPmMouFundAmt(BigDecimal pmMouFundAmt) {
        this.pmMouFundAmt = pmMouFundAmt;
    }

    public BigDecimal getPmReceivedFundAmt() {
        return pmReceivedFundAmt;
    }

    public void setPmReceivedFundAmt(BigDecimal pmReceivedFundAmt) {
        this.pmReceivedFundAmt = pmReceivedFundAmt;
    }

    public BigDecimal getPmToReceivedFundAmt() {
        return pmToReceivedFundAmt;
    }

    public void setPmToReceivedFundAmt(BigDecimal pmToReceivedFundAmt) {
        this.pmToReceivedFundAmt = pmToReceivedFundAmt;
    }

    public BigDecimal getPmSurrenderFundAmt() {
        return pmSurrenderFundAmt;
    }

    public void setPmSurrenderFundAmt(BigDecimal pmSurrenderFundAmt) {
        this.pmSurrenderFundAmt = pmSurrenderFundAmt;
    }

    public BigDecimal getPmTotalFundAmt() {
        return pmTotalFundAmt;
    }

    public void setPmTotalFundAmt(BigDecimal pmTotalFundAmt) {
        this.pmTotalFundAmt = pmTotalFundAmt;
    }

    public BigDecimal getPmWorkplanAmt() {
        return pmWorkplanAmt;
    }

    public void setPmWorkplanAmt(BigDecimal pmWorkplanAmt) {
        this.pmWorkplanAmt = pmWorkplanAmt;
    }

    public BigDecimal getPmWpSurrenderAmt() {
        return pmWpSurrenderAmt;
    }

    public void setPmWpSurrenderAmt(BigDecimal pmWpSurrenderAmt) {
        this.pmWpSurrenderAmt = pmWpSurrenderAmt;
    }

    public BigDecimal getPmReAllotWpAmt() {
        return pmReAllotWpAmt;
    }

    public void setPmReAllotWpAmt(BigDecimal pmReAllotWpAmt) {
        this.pmReAllotWpAmt = pmReAllotWpAmt;
    }

    public BigDecimal getPmOfferWpAmt() {
        return pmOfferWpAmt;
    }

    public void setPmOfferWpAmt(BigDecimal pmOfferWpAmt) {
        this.pmOfferWpAmt = pmOfferWpAmt;
    }

    public BigDecimal getPmReviseWpAmt() {
        return pmReviseWpAmt;
    }

    public void setPmReviseWpAmt(BigDecimal pmReviseWpAmt) {
        this.pmReviseWpAmt = pmReviseWpAmt;
    }

    public BigDecimal getPmCommitedAmt() {
        return pmCommitedAmt;
    }

    public void setPmCommitedAmt(BigDecimal pmCommitedAmt) {
        this.pmCommitedAmt = pmCommitedAmt;
    }

    public BigDecimal getPmPipelinePayAmt() {
        return pmPipelinePayAmt;
    }

    public void setPmPipelinePayAmt(BigDecimal pmPipelinePayAmt) {
        this.pmPipelinePayAmt = pmPipelinePayAmt;
    }

    public BigDecimal getPmAdvanceCommitAmt() {
        return pmAdvanceCommitAmt;
    }

    public void setPmAdvanceCommitAmt(BigDecimal pmAdvanceCommitAmt) {
        this.pmAdvanceCommitAmt = pmAdvanceCommitAmt;
    }

    public BigDecimal getPmAdvancePayAmt() {
        return pmAdvancePayAmt;
    }

    public void setPmAdvancePayAmt(BigDecimal pmAdvancePayAmt) {
        this.pmAdvancePayAmt = pmAdvancePayAmt;
    }

    public BigDecimal getPmPipeAdvadjCommitAmt() {
        return pmPipeAdvadjCommitAmt;
    }

    public void setPmPipeAdvadjCommitAmt(BigDecimal pmPipeAdvadjCommitAmt) {
        this.pmPipeAdvadjCommitAmt = pmPipeAdvadjCommitAmt;
    }

    public BigDecimal getPmPipeAdvadjPayAmt() {
        return pmPipeAdvadjPayAmt;
    }

    public void setPmPipeAdvadjPayAmt(BigDecimal pmPipeAdvadjPayAmt) {
        this.pmPipeAdvadjPayAmt = pmPipeAdvadjPayAmt;
    }

    public BigDecimal getPmReceiptPayAmt() {
        return pmReceiptPayAmt;
    }

    public void setPmReceiptPayAmt(BigDecimal pmReceiptPayAmt) {
        this.pmReceiptPayAmt = pmReceiptPayAmt;
    }

    public BigDecimal getPmReceiptCommitAmt() {
        return pmReceiptCommitAmt;
    }

    public void setPmReceiptCommitAmt(BigDecimal pmReceiptCommitAmt) {
        this.pmReceiptCommitAmt = pmReceiptCommitAmt;
    }

    public BigDecimal getPmExpenseAmt() {
        return pmExpenseAmt;
    }

    public void setPmExpenseAmt(BigDecimal pmExpenseAmt) {
        this.pmExpenseAmt = pmExpenseAmt;
    }

    public BigDecimal getPmUsableBalanceAmt() {
        return pmUsableBalanceAmt;
    }

    public void setPmUsableBalanceAmt(BigDecimal pmUsableBalanceAmt) {
        this.pmUsableBalanceAmt = pmUsableBalanceAmt;
    }

    public BigDecimal getPmBalanceAmt() {
        return pmBalanceAmt;
    }

    public void setPmBalanceAmt(BigDecimal pmBalanceAmt) {
        this.pmBalanceAmt = pmBalanceAmt;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public BigDecimal getPmPipeCommitedAmt() {
        return pmPipeCommitedAmt;
    }

    public void setPmPipeCommitedAmt(BigDecimal pmPipeCommitedAmt) {
        this.pmPipeCommitedAmt = pmPipeCommitedAmt;
    }

    public String getPmRelationFlag() {
        return pmRelationFlag;
    }

    public void setPmRelationFlag(String pmRelationFlag) {
        this.pmRelationFlag = pmRelationFlag;
    }

    public Date getPmStartDate() {
        return pmStartDate;
    }

    public void setPmStartDate(Date pmStartDate) {
        this.pmStartDate = pmStartDate;
    }

    public Date getPmEndDate() {
        return pmEndDate;
    }

    public void setPmEndDate(Date pmEndDate) {
        this.pmEndDate = pmEndDate;
    }

    public Date getPmEndDatePossible() {
        return pmEndDatePossible;
    }

    public void setPmEndDatePossible(Date pmEndDatePossible) {
        this.pmEndDatePossible = pmEndDatePossible;
    }

    public Date getPmTransactionDate() {
        return pmTransactionDate;
    }

    public void setPmTransactionDate(Date pmTransactionDate) {
        this.pmTransactionDate = pmTransactionDate;
    }

    public BigDecimal getPmProCrExpAmt() {
        return pmProCrExpAmt;
    }

    public void setPmProCrExpAmt(BigDecimal pmProCrExpAmt) {
        this.pmProCrExpAmt = pmProCrExpAmt;
    }

    public BigDecimal getPmReceivedAdvanceAmt() {
        return pmReceivedAdvanceAmt;
    }

    public void setPmReceivedAdvanceAmt(BigDecimal pmReceivedAdvanceAmt) {
        this.pmReceivedAdvanceAmt = pmReceivedAdvanceAmt;
    }

    public BigDecimal getPmCarryFwdAdvAmt() {
        return pmCarryFwdAdvAmt;
    }

    public void setPmCarryFwdAdvAmt(BigDecimal pmCarryFwdAdvAmt) {
        this.pmCarryFwdAdvAmt = pmCarryFwdAdvAmt;
    }

    public BigDecimal getPmCommContriAmt() {
        return pmCommContriAmt;
    }

    public void setPmCommContriAmt(BigDecimal pmCommContriAmt) {
        this.pmCommContriAmt = pmCommContriAmt;
    }

    public String getPmFundMonitorFlag() {
        return pmFundMonitorFlag;
    }

    public void setPmFundMonitorFlag(String pmFundMonitorFlag) {
        this.pmFundMonitorFlag = pmFundMonitorFlag;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetLink> getBmwpApActivityBudgetLinkCollection() {
        return bmwpApActivityBudgetLinkCollection;
    }

    public void setBmwpApActivityBudgetLinkCollection(Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection) {
        this.bmwpApActivityBudgetLinkCollection = bmwpApActivityBudgetLinkCollection;
    }

    @XmlTransient
    public Collection<BmwpAgencyProjectLink> getBmwpAgencyProjectLinkCollection() {
        return bmwpAgencyProjectLinkCollection;
    }

    public void setBmwpAgencyProjectLinkCollection(Collection<BmwpAgencyProjectLink> bmwpAgencyProjectLinkCollection) {
        this.bmwpAgencyProjectLinkCollection = bmwpAgencyProjectLinkCollection;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetDtl> getBmwpApActivityBudgetDtlCollection() {
        return bmwpApActivityBudgetDtlCollection;
    }

    public void setBmwpApActivityBudgetDtlCollection(Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection) {
        this.bmwpApActivityBudgetDtlCollection = bmwpApActivityBudgetDtlCollection;
    }

    @XmlTransient
    public Collection<BmwpApActivityMst> getBmwpApActivityMstCollection() {
        return bmwpApActivityMstCollection;
    }

    public void setBmwpApActivityMstCollection(Collection<BmwpApActivityMst> bmwpApActivityMstCollection) {
        this.bmwpApActivityMstCollection = bmwpApActivityMstCollection;
    }

    @XmlTransient
    public Collection<BmwpApBudgetMst> getBmwpApBudgetMstCollection() {
        return bmwpApBudgetMstCollection;
    }

    public void setBmwpApBudgetMstCollection(Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection) {
        this.bmwpApBudgetMstCollection = bmwpApBudgetMstCollection;
    }

    @XmlTransient
    public Collection<BmwpWorkplan> getBmwpWorkplanCollection() {
        return bmwpWorkplanCollection;
    }

    public void setBmwpWorkplanCollection(Collection<BmwpWorkplan> bmwpWorkplanCollection) {
        this.bmwpWorkplanCollection = bmwpWorkplanCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @XmlTransient
    public Collection<BmwpProjectMst> getBmwpProjectMstCollection() {
        return bmwpProjectMstCollection;
    }

    public void setBmwpProjectMstCollection(Collection<BmwpProjectMst> bmwpProjectMstCollection) {
        this.bmwpProjectMstCollection = bmwpProjectMstCollection;
    }

    public BmwpProjectMst getPmParentUniqueSrno() {
        return pmParentUniqueSrno;
    }

    public void setPmParentUniqueSrno(BmwpProjectMst pmParentUniqueSrno) {
        this.pmParentUniqueSrno = pmParentUniqueSrno;
    }

    public BmwpFundSrcMst getPmFsmUniqueSrno() {
        return pmFsmUniqueSrno;
    }

    public void setPmFsmUniqueSrno(BmwpFundSrcMst pmFsmUniqueSrno) {
        this.pmFsmUniqueSrno = pmFsmUniqueSrno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pmUniqueSrno != null ? pmUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmwpProjectMst)) {
            return false;
        }
        BmwpProjectMst other = (BmwpProjectMst) object;
        if ((this.pmUniqueSrno == null && other.pmUniqueSrno != null) || (this.pmUniqueSrno != null && !this.pmUniqueSrno.equals(other.pmUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.BmwpProjectMst[ pmUniqueSrno=" + pmUniqueSrno + " ]";
    }
    
}
