/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "BMWP_PARTY_TYPE_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmwpPartyTypeMst.findAll", query = "SELECT b FROM BmwpPartyTypeMst b"),
    @NamedQuery(name = "BmwpPartyTypeMst.findByPtmUniqueSrno", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.ptmUniqueSrno = :ptmUniqueSrno"),
    @NamedQuery(name = "BmwpPartyTypeMst.findByPtmSrno", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.ptmSrno = :ptmSrno"),
    @NamedQuery(name = "BmwpPartyTypeMst.findByPtmShortDesc", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.ptmShortDesc = :ptmShortDesc"),
    @NamedQuery(name = "BmwpPartyTypeMst.findByPtmDesc", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.ptmDesc = :ptmDesc"),
    @NamedQuery(name = "BmwpPartyTypeMst.findByPtmRemarks", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.ptmRemarks = :ptmRemarks"),
    @NamedQuery(name = "BmwpPartyTypeMst.findByPtmOwner", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.ptmOwner = :ptmOwner"),
    @NamedQuery(name = "BmwpPartyTypeMst.findByPtmTableName", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.ptmTableName = :ptmTableName"),
    @NamedQuery(name = "BmwpPartyTypeMst.findByPtmColumnList", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.ptmColumnList = :ptmColumnList"),
    @NamedQuery(name = "BmwpPartyTypeMst.findByPtmWhereCondition", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.ptmWhereCondition = :ptmWhereCondition"),
    @NamedQuery(name = "BmwpPartyTypeMst.findByPtmOrderBy", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.ptmOrderBy = :ptmOrderBy"),
    @NamedQuery(name = "BmwpPartyTypeMst.findByCreatedt", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.createdt = :createdt"),
    @NamedQuery(name = "BmwpPartyTypeMst.findByUpdatedt", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.updatedt = :updatedt"),
    @NamedQuery(name = "BmwpPartyTypeMst.findBySystemUserid", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.systemUserid = :systemUserid"),
    @NamedQuery(name = "BmwpPartyTypeMst.findByExportFlag", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.exportFlag = :exportFlag"),
    @NamedQuery(name = "BmwpPartyTypeMst.findByImportFlag", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.importFlag = :importFlag"),
    @NamedQuery(name = "BmwpPartyTypeMst.findByPtmDeclareAs", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.ptmDeclareAs = :ptmDeclareAs"),
    @NamedQuery(name = "BmwpPartyTypeMst.findByPtmVisibleFlag", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.ptmVisibleFlag = :ptmVisibleFlag"),
    @NamedQuery(name = "BmwpPartyTypeMst.findByPtmFilterFlag", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.ptmFilterFlag = :ptmFilterFlag"),
    @NamedQuery(name = "BmwpPartyTypeMst.findByPtmFinPartyFlag", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.ptmFinPartyFlag = :ptmFinPartyFlag"),
    @NamedQuery(name = "BmwpPartyTypeMst.findByPtmWorkplanPartyFlag", query = "SELECT b FROM BmwpPartyTypeMst b WHERE b.ptmWorkplanPartyFlag = :ptmWorkplanPartyFlag")})
public class BmwpPartyTypeMst implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PTM_UNIQUE_SRNO")
    private BigDecimal ptmUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PTM_SRNO")
    private int ptmSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "PTM_SHORT_DESC")
    private String ptmShortDesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "PTM_DESC")
    private String ptmDesc;
    @Size(max = 1000)
    @Column(name = "PTM_REMARKS")
    private String ptmRemarks;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "PTM_OWNER")
    private String ptmOwner;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "PTM_TABLE_NAME")
    private String ptmTableName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "PTM_COLUMN_LIST")
    private String ptmColumnList;
    @Size(max = 1000)
    @Column(name = "PTM_WHERE_CONDITION")
    private String ptmWhereCondition;
    @Size(max = 200)
    @Column(name = "PTM_ORDER_BY")
    private String ptmOrderBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PTM_DECLARE_AS")
    private String ptmDeclareAs;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PTM_VISIBLE_FLAG")
    private String ptmVisibleFlag;
    @Size(max = 1)
    @Column(name = "PTM_FILTER_FLAG")
    private String ptmFilterFlag;
    @Size(max = 1)
    @Column(name = "PTM_FIN_PARTY_FLAG")
    private String ptmFinPartyFlag;
    @Size(max = 1)
    @Column(name = "PTM_WORKPLAN_PARTY_FLAG")
    private String ptmWorkplanPartyFlag;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @OneToMany(mappedBy = "ptmRefUniqueSrno")
    private Collection<BmwpPartyTypeMst> bmwpPartyTypeMstCollection;
    @JoinColumn(name = "PTM_REF_UNIQUE_SRNO", referencedColumnName = "PTM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpPartyTypeMst ptmRefUniqueSrno;
    @OneToMany(mappedBy = "bmPtmUniqueSrno")
    private Collection<BmwpBudgetMst> bmwpBudgetMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ptdPtmUniqueSrno")
    private Collection<BmwpPartyTypeDtl> bmwpPartyTypeDtlCollection;
    @OneToMany(mappedBy = "apbmPtmUniqueSrno")
    private Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection;

    public BmwpPartyTypeMst() {
    }

    public BmwpPartyTypeMst(BigDecimal ptmUniqueSrno) {
        this.ptmUniqueSrno = ptmUniqueSrno;
    }

    public BmwpPartyTypeMst(BigDecimal ptmUniqueSrno, int ptmSrno, String ptmShortDesc, String ptmDesc, String ptmOwner, String ptmTableName, String ptmColumnList, Date createdt, String ptmDeclareAs, String ptmVisibleFlag) {
        this.ptmUniqueSrno = ptmUniqueSrno;
        this.ptmSrno = ptmSrno;
        this.ptmShortDesc = ptmShortDesc;
        this.ptmDesc = ptmDesc;
        this.ptmOwner = ptmOwner;
        this.ptmTableName = ptmTableName;
        this.ptmColumnList = ptmColumnList;
        this.createdt = createdt;
        this.ptmDeclareAs = ptmDeclareAs;
        this.ptmVisibleFlag = ptmVisibleFlag;
    }

    public BigDecimal getPtmUniqueSrno() {
        return ptmUniqueSrno;
    }

    public void setPtmUniqueSrno(BigDecimal ptmUniqueSrno) {
        this.ptmUniqueSrno = ptmUniqueSrno;
    }

    public int getPtmSrno() {
        return ptmSrno;
    }

    public void setPtmSrno(int ptmSrno) {
        this.ptmSrno = ptmSrno;
    }

    public String getPtmShortDesc() {
        return ptmShortDesc;
    }

    public void setPtmShortDesc(String ptmShortDesc) {
        this.ptmShortDesc = ptmShortDesc;
    }

    public String getPtmDesc() {
        return ptmDesc;
    }

    public void setPtmDesc(String ptmDesc) {
        this.ptmDesc = ptmDesc;
    }

    public String getPtmRemarks() {
        return ptmRemarks;
    }

    public void setPtmRemarks(String ptmRemarks) {
        this.ptmRemarks = ptmRemarks;
    }

    public String getPtmOwner() {
        return ptmOwner;
    }

    public void setPtmOwner(String ptmOwner) {
        this.ptmOwner = ptmOwner;
    }

    public String getPtmTableName() {
        return ptmTableName;
    }

    public void setPtmTableName(String ptmTableName) {
        this.ptmTableName = ptmTableName;
    }

    public String getPtmColumnList() {
        return ptmColumnList;
    }

    public void setPtmColumnList(String ptmColumnList) {
        this.ptmColumnList = ptmColumnList;
    }

    public String getPtmWhereCondition() {
        return ptmWhereCondition;
    }

    public void setPtmWhereCondition(String ptmWhereCondition) {
        this.ptmWhereCondition = ptmWhereCondition;
    }

    public String getPtmOrderBy() {
        return ptmOrderBy;
    }

    public void setPtmOrderBy(String ptmOrderBy) {
        this.ptmOrderBy = ptmOrderBy;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public String getPtmDeclareAs() {
        return ptmDeclareAs;
    }

    public void setPtmDeclareAs(String ptmDeclareAs) {
        this.ptmDeclareAs = ptmDeclareAs;
    }

    public String getPtmVisibleFlag() {
        return ptmVisibleFlag;
    }

    public void setPtmVisibleFlag(String ptmVisibleFlag) {
        this.ptmVisibleFlag = ptmVisibleFlag;
    }

    public String getPtmFilterFlag() {
        return ptmFilterFlag;
    }

    public void setPtmFilterFlag(String ptmFilterFlag) {
        this.ptmFilterFlag = ptmFilterFlag;
    }

    public String getPtmFinPartyFlag() {
        return ptmFinPartyFlag;
    }

    public void setPtmFinPartyFlag(String ptmFinPartyFlag) {
        this.ptmFinPartyFlag = ptmFinPartyFlag;
    }

    public String getPtmWorkplanPartyFlag() {
        return ptmWorkplanPartyFlag;
    }

    public void setPtmWorkplanPartyFlag(String ptmWorkplanPartyFlag) {
        this.ptmWorkplanPartyFlag = ptmWorkplanPartyFlag;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @XmlTransient
    public Collection<BmwpPartyTypeMst> getBmwpPartyTypeMstCollection() {
        return bmwpPartyTypeMstCollection;
    }

    public void setBmwpPartyTypeMstCollection(Collection<BmwpPartyTypeMst> bmwpPartyTypeMstCollection) {
        this.bmwpPartyTypeMstCollection = bmwpPartyTypeMstCollection;
    }

    public BmwpPartyTypeMst getPtmRefUniqueSrno() {
        return ptmRefUniqueSrno;
    }

    public void setPtmRefUniqueSrno(BmwpPartyTypeMst ptmRefUniqueSrno) {
        this.ptmRefUniqueSrno = ptmRefUniqueSrno;
    }

    @XmlTransient
    public Collection<BmwpBudgetMst> getBmwpBudgetMstCollection() {
        return bmwpBudgetMstCollection;
    }

    public void setBmwpBudgetMstCollection(Collection<BmwpBudgetMst> bmwpBudgetMstCollection) {
        this.bmwpBudgetMstCollection = bmwpBudgetMstCollection;
    }

    @XmlTransient
    public Collection<BmwpPartyTypeDtl> getBmwpPartyTypeDtlCollection() {
        return bmwpPartyTypeDtlCollection;
    }

    public void setBmwpPartyTypeDtlCollection(Collection<BmwpPartyTypeDtl> bmwpPartyTypeDtlCollection) {
        this.bmwpPartyTypeDtlCollection = bmwpPartyTypeDtlCollection;
    }

    @XmlTransient
    public Collection<BmwpApBudgetMst> getBmwpApBudgetMstCollection() {
        return bmwpApBudgetMstCollection;
    }

    public void setBmwpApBudgetMstCollection(Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection) {
        this.bmwpApBudgetMstCollection = bmwpApBudgetMstCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ptmUniqueSrno != null ? ptmUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmwpPartyTypeMst)) {
            return false;
        }
        BmwpPartyTypeMst other = (BmwpPartyTypeMst) object;
        if ((this.ptmUniqueSrno == null && other.ptmUniqueSrno != null) || (this.ptmUniqueSrno != null && !this.ptmUniqueSrno.equals(other.ptmUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.BmwpPartyTypeMst[ ptmUniqueSrno=" + ptmUniqueSrno + " ]";
    }
    
}
