/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_ABSENTEES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasAbsentees.findAll", query = "SELECT f FROM FtasAbsentees f"),
    @NamedQuery(name = "FtasAbsentees.findByEmpno", query = "SELECT f FROM FtasAbsentees f WHERE f.empno = :empno"),
    @NamedQuery(name = "FtasAbsentees.findBySrno", query = "SELECT f FROM FtasAbsentees f WHERE f.srno = :srno"),
    @NamedQuery(name = "FtasAbsentees.findByFromDate", query = "SELECT f FROM FtasAbsentees f WHERE f.fromDate = :fromDate"),
    @NamedQuery(name = "FtasAbsentees.findByToDate", query = "SELECT f FROM FtasAbsentees f WHERE f.toDate = :toDate"),
    @NamedQuery(name = "FtasAbsentees.findByFromhalf", query = "SELECT f FROM FtasAbsentees f WHERE f.fromhalf = :fromhalf"),
    @NamedQuery(name = "FtasAbsentees.findByTohalf", query = "SELECT f FROM FtasAbsentees f WHERE f.tohalf = :tohalf"),
    @NamedQuery(name = "FtasAbsentees.findByDays", query = "SELECT f FROM FtasAbsentees f WHERE f.days = :days"),
    @NamedQuery(name = "FtasAbsentees.findByApplicationFlag", query = "SELECT f FROM FtasAbsentees f WHERE f.applicationFlag = :applicationFlag"),
    @NamedQuery(name = "FtasAbsentees.findByCancelSrno", query = "SELECT f FROM FtasAbsentees f WHERE f.cancelSrno = :cancelSrno"),
    @NamedQuery(name = "FtasAbsentees.findByInstitute", query = "SELECT f FROM FtasAbsentees f WHERE f.institute = :institute"),
    @NamedQuery(name = "FtasAbsentees.findByPlace1", query = "SELECT f FROM FtasAbsentees f WHERE f.place1 = :place1"),
    @NamedQuery(name = "FtasAbsentees.findByReason1", query = "SELECT f FROM FtasAbsentees f WHERE f.reason1 = :reason1"),
    @NamedQuery(name = "FtasAbsentees.findByFees", query = "SELECT f FROM FtasAbsentees f WHERE f.fees = :fees"),
    @NamedQuery(name = "FtasAbsentees.findByActnpurp", query = "SELECT f FROM FtasAbsentees f WHERE f.actnpurp = :actnpurp"),
    @NamedQuery(name = "FtasAbsentees.findByRecmby", query = "SELECT f FROM FtasAbsentees f WHERE f.recmby = :recmby"),
    @NamedQuery(name = "FtasAbsentees.findByRecmdt", query = "SELECT f FROM FtasAbsentees f WHERE f.recmdt = :recmdt"),
    @NamedQuery(name = "FtasAbsentees.findByRecmactn", query = "SELECT f FROM FtasAbsentees f WHERE f.recmactn = :recmactn"),
    @NamedQuery(name = "FtasAbsentees.findByRecmpurp", query = "SELECT f FROM FtasAbsentees f WHERE f.recmpurp = :recmpurp"),
    @NamedQuery(name = "FtasAbsentees.findByRecmcomm", query = "SELECT f FROM FtasAbsentees f WHERE f.recmcomm = :recmcomm"),
    @NamedQuery(name = "FtasAbsentees.findByAprvDate", query = "SELECT f FROM FtasAbsentees f WHERE f.aprvDate = :aprvDate"),
    @NamedQuery(name = "FtasAbsentees.findByAprvcomm", query = "SELECT f FROM FtasAbsentees f WHERE f.aprvcomm = :aprvcomm"),
    @NamedQuery(name = "FtasAbsentees.findByUpdatedt", query = "SELECT f FROM FtasAbsentees f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasAbsentees.findByCreatedt", query = "SELECT f FROM FtasAbsentees f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasAbsentees.findByExportFlag", query = "SELECT f FROM FtasAbsentees f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FtasAbsentees.findByImportFlag", query = "SELECT f FROM FtasAbsentees f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FtasAbsentees.findBySystemUserid", query = "SELECT f FROM FtasAbsentees f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FtasAbsentees.findByAppliType", query = "SELECT f FROM FtasAbsentees f WHERE f.appliType = :appliType"),
    @NamedQuery(name = "FtasAbsentees.findByLeaveBalLaps", query = "SELECT f FROM FtasAbsentees f WHERE f.leaveBalLaps = :leaveBalLaps"),
    @NamedQuery(name = "FtasAbsentees.findByAbsSrgKey", query = "SELECT f FROM FtasAbsentees f WHERE f.absSrgKey = :absSrgKey"),
    @NamedQuery(name = "FtasAbsentees.findByTranYear", query = "SELECT f FROM FtasAbsentees f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FtasAbsentees.findByCabsAprvcomm", query = "SELECT f FROM FtasAbsentees f WHERE f.cabsAprvcomm = :cabsAprvcomm"),
    @NamedQuery(name = "FtasAbsentees.findByAppliDate", query = "SELECT f FROM FtasAbsentees f WHERE f.appliDate = :appliDate"),
    @NamedQuery(name = "FtasAbsentees.findByArcomment", query = "SELECT f FROM FtasAbsentees f WHERE f.arcomment = :arcomment"),
    @NamedQuery(name = "FtasAbsentees.findByCloneOfAbsSrgKey", query = "SELECT f FROM FtasAbsentees f WHERE f.cloneOfAbsSrgKey = :cloneOfAbsSrgKey")})
public class FtasAbsentees implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "EMPNO")
    private Integer empno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SRNO")
    private int srno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date toDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "FROMHALF")
    private String fromhalf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "TOHALF")
    private String tohalf;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "DAYS")
    private BigDecimal days;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "APPLICATION_FLAG")
    private String applicationFlag;
    @Column(name = "CANCEL_SRNO")
    private Integer cancelSrno;
    @Size(max = 70)
    @Column(name = "INSTITUTE")
    private String institute;
    @Size(max = 4000)
    @Column(name = "PLACE1")
    private String place1;
    @Size(max = 4000)
    @Column(name = "REASON1")
    private String reason1;
    @Column(name = "FEES")
    private BigDecimal fees;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "ACTNPURP")
    private String actnpurp;
    @Column(name = "RECMBY")
    private Integer recmby;
    @Column(name = "RECMDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date recmdt;
    @Column(name = "RECMACTN")
    private Integer recmactn;
    @Size(max = 1)
    @Column(name = "RECMPURP")
    private String recmpurp;
    @Size(max = 1)
    @Column(name = "RECMCOMM")
    private String recmcomm;
    @Column(name = "APRV_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date aprvDate;
    @Size(max = 1)
    @Column(name = "APRVCOMM")
    private String aprvcomm;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "APPLI_TYPE")
    private String appliType;
    @Column(name = "LEAVE_BAL_LAPS")
    private BigDecimal leaveBalLaps;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ABS_SRG_KEY")
    private BigDecimal absSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Size(max = 1)
    @Column(name = "CABS_APRVCOMM")
    private String cabsAprvcomm;
    @Column(name = "APPLI_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date appliDate;
    @Size(max = 500)
    @Column(name = "ARCOMMENT")
    private String arcomment;
    @Column(name = "CLONE_OF_ABS_SRG_KEY")
    private BigDecimal cloneOfAbsSrgKey;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "absSrgKey")
    private Collection<FtasTadaBillMst> ftasTadaBillMstCollection;
    @JoinColumn(name = "APRV_AUTH_OUM", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst aprvAuthOum;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "TROUD_SRG_KEY", referencedColumnName = "TROUD_SRG_KEY")
    @ManyToOne
    private FtasTrainingOrgUnitDtl troudSrgKey;
    @JoinColumn(name = "CABS_SRG_KEY", referencedColumnName = "CABS_SRG_KEY")
    @ManyToOne
    private FtasCancelabs cabsSrgKey;
    @JoinColumn(name = "ACTNBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst actnby;
    @JoinColumn(name = "APRVBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst aprvby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst userId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "absSrgKey")
    private Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "absSrgKey")
    private Collection<FtasTrainingScheduleDtl> ftasTrainingScheduleDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "absSrgKey")
    private Collection<FtasTourScheduleDtl> ftasTourScheduleDtlCollection;
    @OneToMany(mappedBy = "absSrgKey")
    private Collection<FtasCancelabs> ftasCancelabsCollection;

    public FtasAbsentees() {
    }

    public FtasAbsentees(BigDecimal absSrgKey) {
        this.absSrgKey = absSrgKey;
    }

    public FtasAbsentees(BigDecimal absSrgKey, int srno, Date fromDate, Date toDate, String fromhalf, String tohalf, BigDecimal days, String applicationFlag, String actnpurp, Date createdt, String appliType, short tranYear) {
        this.absSrgKey = absSrgKey;
        this.srno = srno;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.fromhalf = fromhalf;
        this.tohalf = tohalf;
        this.days = days;
        this.applicationFlag = applicationFlag;
        this.actnpurp = actnpurp;
        this.createdt = createdt;
        this.appliType = appliType;
        this.tranYear = tranYear;
    }

    public Integer getEmpno() {
        return empno;
    }

    public void setEmpno(Integer empno) {
        this.empno = empno;
    }

    public int getSrno() {
        return srno;
    }

    public void setSrno(int srno) {
        this.srno = srno;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getFromhalf() {
        return fromhalf;
    }

    public void setFromhalf(String fromhalf) {
        this.fromhalf = fromhalf;
    }

    public String getTohalf() {
        return tohalf;
    }

    public void setTohalf(String tohalf) {
        this.tohalf = tohalf;
    }

    public BigDecimal getDays() {
        return days;
    }

    public void setDays(BigDecimal days) {
        this.days = days;
    }

    public String getApplicationFlag() {
        return applicationFlag;
    }

    public void setApplicationFlag(String applicationFlag) {
        this.applicationFlag = applicationFlag;
    }

    public Integer getCancelSrno() {
        return cancelSrno;
    }

    public void setCancelSrno(Integer cancelSrno) {
        this.cancelSrno = cancelSrno;
    }

    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public String getPlace1() {
        return place1;
    }

    public void setPlace1(String place1) {
        this.place1 = place1;
    }

    public String getReason1() {
        return reason1;
    }

    public void setReason1(String reason1) {
        this.reason1 = reason1;
    }

    public BigDecimal getFees() {
        return fees;
    }

    public void setFees(BigDecimal fees) {
        this.fees = fees;
    }

    public String getActnpurp() {
        return actnpurp;
    }

    public void setActnpurp(String actnpurp) {
        this.actnpurp = actnpurp;
    }

    public Integer getRecmby() {
        return recmby;
    }

    public void setRecmby(Integer recmby) {
        this.recmby = recmby;
    }

    public Date getRecmdt() {
        return recmdt;
    }

    public void setRecmdt(Date recmdt) {
        this.recmdt = recmdt;
    }

    public Integer getRecmactn() {
        return recmactn;
    }

    public void setRecmactn(Integer recmactn) {
        this.recmactn = recmactn;
    }

    public String getRecmpurp() {
        return recmpurp;
    }

    public void setRecmpurp(String recmpurp) {
        this.recmpurp = recmpurp;
    }

    public String getRecmcomm() {
        return recmcomm;
    }

    public void setRecmcomm(String recmcomm) {
        this.recmcomm = recmcomm;
    }

    public Date getAprvDate() {
        return aprvDate;
    }

    public void setAprvDate(Date aprvDate) {
        this.aprvDate = aprvDate;
    }

    public String getAprvcomm() {
        return aprvcomm;
    }

    public void setAprvcomm(String aprvcomm) {
        this.aprvcomm = aprvcomm;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getAppliType() {
        return appliType;
    }

    public void setAppliType(String appliType) {
        this.appliType = appliType;
    }

    public BigDecimal getLeaveBalLaps() {
        return leaveBalLaps;
    }

    public void setLeaveBalLaps(BigDecimal leaveBalLaps) {
        this.leaveBalLaps = leaveBalLaps;
    }

    public BigDecimal getAbsSrgKey() {
        return absSrgKey;
    }

    public void setAbsSrgKey(BigDecimal absSrgKey) {
        this.absSrgKey = absSrgKey;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public String getCabsAprvcomm() {
        return cabsAprvcomm;
    }

    public void setCabsAprvcomm(String cabsAprvcomm) {
        this.cabsAprvcomm = cabsAprvcomm;
    }

    public Date getAppliDate() {
        return appliDate;
    }

    public void setAppliDate(Date appliDate) {
        this.appliDate = appliDate;
    }

    public String getArcomment() {
        return arcomment;
    }

    public void setArcomment(String arcomment) {
        this.arcomment = arcomment;
    }

    public BigDecimal getCloneOfAbsSrgKey() {
        return cloneOfAbsSrgKey;
    }

    public void setCloneOfAbsSrgKey(BigDecimal cloneOfAbsSrgKey) {
        this.cloneOfAbsSrgKey = cloneOfAbsSrgKey;
    }

    @XmlTransient
    public Collection<FtasTadaBillMst> getFtasTadaBillMstCollection() {
        return ftasTadaBillMstCollection;
    }

    public void setFtasTadaBillMstCollection(Collection<FtasTadaBillMst> ftasTadaBillMstCollection) {
        this.ftasTadaBillMstCollection = ftasTadaBillMstCollection;
    }

    public SystOrgUnitMst getAprvAuthOum() {
        return aprvAuthOum;
    }

    public void setAprvAuthOum(SystOrgUnitMst aprvAuthOum) {
        this.aprvAuthOum = aprvAuthOum;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasTrainingOrgUnitDtl getTroudSrgKey() {
        return troudSrgKey;
    }

    public void setTroudSrgKey(FtasTrainingOrgUnitDtl troudSrgKey) {
        this.troudSrgKey = troudSrgKey;
    }

    public FtasCancelabs getCabsSrgKey() {
        return cabsSrgKey;
    }

    public void setCabsSrgKey(FtasCancelabs cabsSrgKey) {
        this.cabsSrgKey = cabsSrgKey;
    }

    public FhrdEmpmst getActnby() {
        return actnby;
    }

    public void setActnby(FhrdEmpmst actnby) {
        this.actnby = actnby;
    }

    public FhrdEmpmst getAprvby() {
        return aprvby;
    }

    public void setAprvby(FhrdEmpmst aprvby) {
        this.aprvby = aprvby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUserId() {
        return userId;
    }

    public void setUserId(FhrdEmpmst userId) {
        this.userId = userId;
    }

    @XmlTransient
    public Collection<FtasAbsenteesDtl> getFtasAbsenteesDtlCollection() {
        return ftasAbsenteesDtlCollection;
    }

    public void setFtasAbsenteesDtlCollection(Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection) {
        this.ftasAbsenteesDtlCollection = ftasAbsenteesDtlCollection;
    }

    @XmlTransient
    public Collection<FtasTrainingScheduleDtl> getFtasTrainingScheduleDtlCollection() {
        return ftasTrainingScheduleDtlCollection;
    }

    public void setFtasTrainingScheduleDtlCollection(Collection<FtasTrainingScheduleDtl> ftasTrainingScheduleDtlCollection) {
        this.ftasTrainingScheduleDtlCollection = ftasTrainingScheduleDtlCollection;
    }

    @XmlTransient
    public Collection<FtasTourScheduleDtl> getFtasTourScheduleDtlCollection() {
        return ftasTourScheduleDtlCollection;
    }

    public void setFtasTourScheduleDtlCollection(Collection<FtasTourScheduleDtl> ftasTourScheduleDtlCollection) {
        this.ftasTourScheduleDtlCollection = ftasTourScheduleDtlCollection;
    }

    @XmlTransient
    public Collection<FtasCancelabs> getFtasCancelabsCollection() {
        return ftasCancelabsCollection;
    }

    public void setFtasCancelabsCollection(Collection<FtasCancelabs> ftasCancelabsCollection) {
        this.ftasCancelabsCollection = ftasCancelabsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (absSrgKey != null ? absSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasAbsentees)) {
            return false;
        }
        FtasAbsentees other = (FtasAbsentees) object;
        if ((this.absSrgKey == null && other.absSrgKey != null) || (this.absSrgKey != null && !this.absSrgKey.equals(other.absSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasAbsentees[ absSrgKey=" + absSrgKey + " ]";
    }
    
}
