/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_EVENT_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdEventMst.findAll", query = "SELECT f FROM FhrdEventMst f"),
    @NamedQuery(name = "FhrdEventMst.findByEventName", query = "SELECT f FROM FhrdEventMst f WHERE f.eventName = :eventName"),
    @NamedQuery(name = "FhrdEventMst.findByEventDesc", query = "SELECT f FROM FhrdEventMst f WHERE f.eventDesc = :eventDesc"),
    @NamedQuery(name = "FhrdEventMst.findByEventType", query = "SELECT f FROM FhrdEventMst f WHERE f.eventType = :eventType"),
    @NamedQuery(name = "FhrdEventMst.findByEventPeriodRequiredFlag", query = "SELECT f FROM FhrdEventMst f WHERE f.eventPeriodRequiredFlag = :eventPeriodRequiredFlag"),
    @NamedQuery(name = "FhrdEventMst.findByParentEventSrno", query = "SELECT f FROM FhrdEventMst f WHERE f.parentEventSrno = :parentEventSrno"),
    @NamedQuery(name = "FhrdEventMst.findByEventMessage", query = "SELECT f FROM FhrdEventMst f WHERE f.eventMessage = :eventMessage"),
    @NamedQuery(name = "FhrdEventMst.findByAlertStartPeriodNo", query = "SELECT f FROM FhrdEventMst f WHERE f.alertStartPeriodNo = :alertStartPeriodNo"),
    @NamedQuery(name = "FhrdEventMst.findByAlertStartPeriodUnit", query = "SELECT f FROM FhrdEventMst f WHERE f.alertStartPeriodUnit = :alertStartPeriodUnit"),
    @NamedQuery(name = "FhrdEventMst.findByEventTwoAlertsGapNo", query = "SELECT f FROM FhrdEventMst f WHERE f.eventTwoAlertsGapNo = :eventTwoAlertsGapNo"),
    @NamedQuery(name = "FhrdEventMst.findByEventTwoAlertsGapUnit", query = "SELECT f FROM FhrdEventMst f WHERE f.eventTwoAlertsGapUnit = :eventTwoAlertsGapUnit"),
    @NamedQuery(name = "FhrdEventMst.findByAlertReceivingPersonEmail", query = "SELECT f FROM FhrdEventMst f WHERE f.alertReceivingPersonEmail = :alertReceivingPersonEmail"),
    @NamedQuery(name = "FhrdEventMst.findByCreatedt", query = "SELECT f FROM FhrdEventMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdEventMst.findByUpdatedt", query = "SELECT f FROM FhrdEventMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdEventMst.findBySystemUserid", query = "SELECT f FROM FhrdEventMst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdEventMst.findByExportFlag", query = "SELECT f FROM FhrdEventMst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdEventMst.findByImportFlag", query = "SELECT f FROM FhrdEventMst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdEventMst.findByEventNameColumnUse", query = "SELECT f FROM FhrdEventMst f WHERE f.eventNameColumnUse = :eventNameColumnUse"),
    @NamedQuery(name = "FhrdEventMst.findByEventNameColumnUseDesc", query = "SELECT f FROM FhrdEventMst f WHERE f.eventNameColumnUseDesc = :eventNameColumnUseDesc"),
    @NamedQuery(name = "FhrdEventMst.findByEventOrder", query = "SELECT f FROM FhrdEventMst f WHERE f.eventOrder = :eventOrder"),
    @NamedQuery(name = "FhrdEventMst.findByEmSrgKey", query = "SELECT f FROM FhrdEventMst f WHERE f.emSrgKey = :emSrgKey"),
    @NamedQuery(name = "FhrdEventMst.findByEventEffectTime", query = "SELECT f FROM FhrdEventMst f WHERE f.eventEffectTime = :eventEffectTime"),
    @NamedQuery(name = "FhrdEventMst.findByAlertReceivingPerson2Email", query = "SELECT f FROM FhrdEventMst f WHERE f.alertReceivingPerson2Email = :alertReceivingPerson2Email"),
    @NamedQuery(name = "FhrdEventMst.findByEventSrno", query = "SELECT f FROM FhrdEventMst f WHERE f.eventSrno = :eventSrno"),
    @NamedQuery(name = "FhrdEventMst.findBySystEventName", query = "SELECT f FROM FhrdEventMst f WHERE f.systEventName = :systEventName")})
public class FhrdEventMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "EVENT_NAME")
    private String eventName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "EVENT_DESC")
    private String eventDesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EVENT_TYPE")
    private String eventType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EVENT_PERIOD_REQUIRED_FLAG")
    private String eventPeriodRequiredFlag;
    @Column(name = "PARENT_EVENT_SRNO")
    private Integer parentEventSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "EVENT_MESSAGE")
    private String eventMessage;
    @Column(name = "ALERT_START_PERIOD_NO")
    private Integer alertStartPeriodNo;
    @Size(max = 1)
    @Column(name = "ALERT_START_PERIOD_UNIT")
    private String alertStartPeriodUnit;
    @Column(name = "EVENT_TWO_ALERTS_GAP_NO")
    private Integer eventTwoAlertsGapNo;
    @Size(max = 1)
    @Column(name = "EVENT_TWO_ALERTS_GAP_UNIT")
    private String eventTwoAlertsGapUnit;
    @Size(max = 500)
    @Column(name = "ALERT_RECEIVING_PERSON_EMAIL")
    private String alertReceivingPersonEmail;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EVENT_NAME_COLUMN_USE")
    private String eventNameColumnUse;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "EVENT_NAME_COLUMN_USE_DESC")
    private String eventNameColumnUseDesc;
    @Column(name = "EVENT_ORDER")
    private Short eventOrder;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "EM_SRG_KEY")
    private BigDecimal emSrgKey;
    @Size(max = 1)
    @Column(name = "EVENT_EFFECT_TIME")
    private String eventEffectTime;
    @Size(max = 500)
    @Column(name = "ALERT_RECEIVING_PERSON2_EMAIL")
    private String alertReceivingPerson2Email;
    @Column(name = "EVENT_SRNO")
    private Integer eventSrno;
    @Size(max = 30)
    @Column(name = "SYST_EVENT_NAME")
    private String systEventName;
    @OneToMany(mappedBy = "emSrgKey")
    private Collection<FhrdEmpEventDtl> fhrdEmpEventDtlCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "ALERT_RECEIVING_PERSON2", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst alertReceivingPerson2;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "ALERT_RECEIVING_PERSON", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst alertReceivingPerson;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "emSrgKey")
    private Collection<FhrdEmpeventPromotion> fhrdEmpeventPromotionCollection;

    public FhrdEventMst() {
    }

    public FhrdEventMst(BigDecimal emSrgKey) {
        this.emSrgKey = emSrgKey;
    }

    public FhrdEventMst(BigDecimal emSrgKey, String eventName, String eventDesc, String eventType, String eventPeriodRequiredFlag, String eventMessage, Date createdt, String eventNameColumnUse, String eventNameColumnUseDesc) {
        this.emSrgKey = emSrgKey;
        this.eventName = eventName;
        this.eventDesc = eventDesc;
        this.eventType = eventType;
        this.eventPeriodRequiredFlag = eventPeriodRequiredFlag;
        this.eventMessage = eventMessage;
        this.createdt = createdt;
        this.eventNameColumnUse = eventNameColumnUse;
        this.eventNameColumnUseDesc = eventNameColumnUseDesc;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDesc() {
        return eventDesc;
    }

    public void setEventDesc(String eventDesc) {
        this.eventDesc = eventDesc;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventPeriodRequiredFlag() {
        return eventPeriodRequiredFlag;
    }

    public void setEventPeriodRequiredFlag(String eventPeriodRequiredFlag) {
        this.eventPeriodRequiredFlag = eventPeriodRequiredFlag;
    }

    public Integer getParentEventSrno() {
        return parentEventSrno;
    }

    public void setParentEventSrno(Integer parentEventSrno) {
        this.parentEventSrno = parentEventSrno;
    }

    public String getEventMessage() {
        return eventMessage;
    }

    public void setEventMessage(String eventMessage) {
        this.eventMessage = eventMessage;
    }

    public Integer getAlertStartPeriodNo() {
        return alertStartPeriodNo;
    }

    public void setAlertStartPeriodNo(Integer alertStartPeriodNo) {
        this.alertStartPeriodNo = alertStartPeriodNo;
    }

    public String getAlertStartPeriodUnit() {
        return alertStartPeriodUnit;
    }

    public void setAlertStartPeriodUnit(String alertStartPeriodUnit) {
        this.alertStartPeriodUnit = alertStartPeriodUnit;
    }

    public Integer getEventTwoAlertsGapNo() {
        return eventTwoAlertsGapNo;
    }

    public void setEventTwoAlertsGapNo(Integer eventTwoAlertsGapNo) {
        this.eventTwoAlertsGapNo = eventTwoAlertsGapNo;
    }

    public String getEventTwoAlertsGapUnit() {
        return eventTwoAlertsGapUnit;
    }

    public void setEventTwoAlertsGapUnit(String eventTwoAlertsGapUnit) {
        this.eventTwoAlertsGapUnit = eventTwoAlertsGapUnit;
    }

    public String getAlertReceivingPersonEmail() {
        return alertReceivingPersonEmail;
    }

    public void setAlertReceivingPersonEmail(String alertReceivingPersonEmail) {
        this.alertReceivingPersonEmail = alertReceivingPersonEmail;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public String getEventNameColumnUse() {
        return eventNameColumnUse;
    }

    public void setEventNameColumnUse(String eventNameColumnUse) {
        this.eventNameColumnUse = eventNameColumnUse;
    }

    public String getEventNameColumnUseDesc() {
        return eventNameColumnUseDesc;
    }

    public void setEventNameColumnUseDesc(String eventNameColumnUseDesc) {
        this.eventNameColumnUseDesc = eventNameColumnUseDesc;
    }

    public Short getEventOrder() {
        return eventOrder;
    }

    public void setEventOrder(Short eventOrder) {
        this.eventOrder = eventOrder;
    }

    public BigDecimal getEmSrgKey() {
        return emSrgKey;
    }

    public void setEmSrgKey(BigDecimal emSrgKey) {
        this.emSrgKey = emSrgKey;
    }

    public String getEventEffectTime() {
        return eventEffectTime;
    }

    public void setEventEffectTime(String eventEffectTime) {
        this.eventEffectTime = eventEffectTime;
    }

    public String getAlertReceivingPerson2Email() {
        return alertReceivingPerson2Email;
    }

    public void setAlertReceivingPerson2Email(String alertReceivingPerson2Email) {
        this.alertReceivingPerson2Email = alertReceivingPerson2Email;
    }

    public Integer getEventSrno() {
        return eventSrno;
    }

    public void setEventSrno(Integer eventSrno) {
        this.eventSrno = eventSrno;
    }

    public String getSystEventName() {
        return systEventName;
    }

    public void setSystEventName(String systEventName) {
        this.systEventName = systEventName;
    }

    @XmlTransient
    public Collection<FhrdEmpEventDtl> getFhrdEmpEventDtlCollection() {
        return fhrdEmpEventDtlCollection;
    }

    public void setFhrdEmpEventDtlCollection(Collection<FhrdEmpEventDtl> fhrdEmpEventDtlCollection) {
        this.fhrdEmpEventDtlCollection = fhrdEmpEventDtlCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getAlertReceivingPerson2() {
        return alertReceivingPerson2;
    }

    public void setAlertReceivingPerson2(FhrdEmpmst alertReceivingPerson2) {
        this.alertReceivingPerson2 = alertReceivingPerson2;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getAlertReceivingPerson() {
        return alertReceivingPerson;
    }

    public void setAlertReceivingPerson(FhrdEmpmst alertReceivingPerson) {
        this.alertReceivingPerson = alertReceivingPerson;
    }

    @XmlTransient
    public Collection<FhrdEmpeventPromotion> getFhrdEmpeventPromotionCollection() {
        return fhrdEmpeventPromotionCollection;
    }

    public void setFhrdEmpeventPromotionCollection(Collection<FhrdEmpeventPromotion> fhrdEmpeventPromotionCollection) {
        this.fhrdEmpeventPromotionCollection = fhrdEmpeventPromotionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (emSrgKey != null ? emSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdEventMst)) {
            return false;
        }
        FhrdEventMst other = (FhrdEventMst) object;
        if ((this.emSrgKey == null && other.emSrgKey != null) || (this.emSrgKey != null && !this.emSrgKey.equals(other.emSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdEventMst[ emSrgKey=" + emSrgKey + " ]";
    }
    
}
