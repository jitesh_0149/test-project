/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_ABSENTEES_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasAbsenteesDtl.findAll", query = "SELECT f FROM FtasAbsenteesDtl f"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByAbsdSrgKey", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.absdSrgKey = :absdSrgKey"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByLeavecd", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.leavecd = :leavecd"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByFromDate", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.fromDate = :fromDate"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByToDate", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.toDate = :toDate"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByFromhalf", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.fromhalf = :fromhalf"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByTohalf", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.tohalf = :tohalf"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByDays", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.days = :days"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByEncash", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.encash = :encash"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByLeaveBalLaps", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.leaveBalLaps = :leaveBalLaps"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByCreatedt", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByUpdatedt", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasAbsenteesDtl.findBySystemUserid", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByExportFlag", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByImportFlag", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByAprvcomm", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.aprvcomm = :aprvcomm"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByRecmcomm", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.recmcomm = :recmcomm"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByTtrElrSrgKey", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.ttrElrSrgKey = :ttrElrSrgKey"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByParentElrSrgKey", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.parentElrSrgKey = :parentElrSrgKey"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByTranYear", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByCarryForwardFromOldBal", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.carryForwardFromOldBal = :carryForwardFromOldBal"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByAppliType", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.appliType = :appliType"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByLastLeavecd", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.lastLeavecd = :lastLeavecd"),
    @NamedQuery(name = "FtasAbsenteesDtl.findByTotalDays", query = "SELECT f FROM FtasAbsenteesDtl f WHERE f.totalDays = :totalDays")})
public class FtasAbsenteesDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ABSD_SRG_KEY")
    private BigDecimal absdSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "LEAVECD")
    private String leavecd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date toDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "FROMHALF")
    private String fromhalf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "TOHALF")
    private String tohalf;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DAYS")
    private BigDecimal days;
    @Column(name = "ENCASH")
    private Short encash;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LEAVE_BAL_LAPS")
    private BigDecimal leaveBalLaps;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Size(max = 1)
    @Column(name = "APRVCOMM")
    private String aprvcomm;
    @Size(max = 1)
    @Column(name = "RECMCOMM")
    private String recmcomm;
    @Column(name = "TTR_ELR_SRG_KEY")
    private BigDecimal ttrElrSrgKey;
    @Column(name = "PARENT_ELR_SRG_KEY")
    private BigDecimal parentElrSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CARRY_FORWARD_FROM_OLD_BAL")
    private BigDecimal carryForwardFromOldBal;
    @Size(max = 20)
    @Column(name = "APPLI_TYPE")
    private String appliType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "LAST_LEAVECD")
    private String lastLeavecd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_DAYS")
    private BigDecimal totalDays;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne(optional = false)
    private FtasLeavemst lmSrgKey;
    @JoinColumn(name = "TTR_LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne
    private FtasLeavemst ttrLmSrgKey;
    @JoinColumn(name = "PARENT_LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne
    private FtasLeavemst parentLmSrgKey;
    @JoinColumn(name = "CHILD_LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne
    private FtasLeavemst childLmSrgKey;
    @JoinColumn(name = "EMPLB_SRG_KEY", referencedColumnName = "EMPLB_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdEmpleavebal emplbSrgKey;
    @JoinColumn(name = "CHILD_EMPLB_SRG_KEY", referencedColumnName = "EMPLB_SRG_KEY")
    @ManyToOne
    private FhrdEmpleavebal childEmplbSrgKey;
    @JoinColumn(name = "ACTUAL_EMPLB_SRG_KEY", referencedColumnName = "EMPLB_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdEmpleavebal actualEmplbSrgKey;
    @JoinColumn(name = "PARENT_EMPLB_SRG_KEY", referencedColumnName = "EMPLB_SRG_KEY")
    @ManyToOne
    private FhrdEmpleavebal parentEmplbSrgKey;
    @JoinColumn(name = "TTR_EMPLB_SRG_KEY", referencedColumnName = "EMPLB_SRG_KEY")
    @ManyToOne
    private FhrdEmpleavebal ttrEmplbSrgKey;
    @JoinColumn(name = "ELR_SRG_KEY", referencedColumnName = "ELR_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdEmpleaverule elrSrgKey;
    @JoinColumn(name = "CHILD_ELR_SRG_KEY", referencedColumnName = "ELR_SRG_KEY")
    @ManyToOne
    private FhrdEmpleaverule childElrSrgKey;
    @JoinColumn(name = "ACTUAL_ELR_SRG_KEY", referencedColumnName = "ELR_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdEmpleaverule actualElrSrgKey;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst userId;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst createby;
    @JoinColumn(name = "ABS_SRG_KEY", referencedColumnName = "ABS_SRG_KEY")
    @ManyToOne(optional = false)
    private FtasAbsentees absSrgKey;
    @JoinColumn(name = "ACTUAL_LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne(optional = false)
    private FtasLeavemst actualLmSrgKey;

    public FtasAbsenteesDtl() {
    }

    public FtasAbsenteesDtl(BigDecimal absdSrgKey) {
        this.absdSrgKey = absdSrgKey;
    }

    public FtasAbsenteesDtl(BigDecimal absdSrgKey, String leavecd, Date fromDate, Date toDate, String fromhalf, String tohalf, BigDecimal days, BigDecimal leaveBalLaps, Date createdt, short tranYear, BigDecimal carryForwardFromOldBal, String lastLeavecd, BigDecimal totalDays) {
        this.absdSrgKey = absdSrgKey;
        this.leavecd = leavecd;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.fromhalf = fromhalf;
        this.tohalf = tohalf;
        this.days = days;
        this.leaveBalLaps = leaveBalLaps;
        this.createdt = createdt;
        this.tranYear = tranYear;
        this.carryForwardFromOldBal = carryForwardFromOldBal;
        this.lastLeavecd = lastLeavecd;
        this.totalDays = totalDays;
    }

    public BigDecimal getAbsdSrgKey() {
        return absdSrgKey;
    }

    public void setAbsdSrgKey(BigDecimal absdSrgKey) {
        this.absdSrgKey = absdSrgKey;
    }

    public String getLeavecd() {
        return leavecd;
    }

    public void setLeavecd(String leavecd) {
        this.leavecd = leavecd;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getFromhalf() {
        return fromhalf;
    }

    public void setFromhalf(String fromhalf) {
        this.fromhalf = fromhalf;
    }

    public String getTohalf() {
        return tohalf;
    }

    public void setTohalf(String tohalf) {
        this.tohalf = tohalf;
    }

    public BigDecimal getDays() {
        return days;
    }

    public void setDays(BigDecimal days) {
        this.days = days;
    }

    public Short getEncash() {
        return encash;
    }

    public void setEncash(Short encash) {
        this.encash = encash;
    }

    public BigDecimal getLeaveBalLaps() {
        return leaveBalLaps;
    }

    public void setLeaveBalLaps(BigDecimal leaveBalLaps) {
        this.leaveBalLaps = leaveBalLaps;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public String getAprvcomm() {
        return aprvcomm;
    }

    public void setAprvcomm(String aprvcomm) {
        this.aprvcomm = aprvcomm;
    }

    public String getRecmcomm() {
        return recmcomm;
    }

    public void setRecmcomm(String recmcomm) {
        this.recmcomm = recmcomm;
    }

    public BigDecimal getTtrElrSrgKey() {
        return ttrElrSrgKey;
    }

    public void setTtrElrSrgKey(BigDecimal ttrElrSrgKey) {
        this.ttrElrSrgKey = ttrElrSrgKey;
    }

    public BigDecimal getParentElrSrgKey() {
        return parentElrSrgKey;
    }

    public void setParentElrSrgKey(BigDecimal parentElrSrgKey) {
        this.parentElrSrgKey = parentElrSrgKey;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public BigDecimal getCarryForwardFromOldBal() {
        return carryForwardFromOldBal;
    }

    public void setCarryForwardFromOldBal(BigDecimal carryForwardFromOldBal) {
        this.carryForwardFromOldBal = carryForwardFromOldBal;
    }

    public String getAppliType() {
        return appliType;
    }

    public void setAppliType(String appliType) {
        this.appliType = appliType;
    }

    public String getLastLeavecd() {
        return lastLeavecd;
    }

    public void setLastLeavecd(String lastLeavecd) {
        this.lastLeavecd = lastLeavecd;
    }

    public BigDecimal getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(BigDecimal totalDays) {
        this.totalDays = totalDays;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasLeavemst getLmSrgKey() {
        return lmSrgKey;
    }

    public void setLmSrgKey(FtasLeavemst lmSrgKey) {
        this.lmSrgKey = lmSrgKey;
    }

    public FtasLeavemst getTtrLmSrgKey() {
        return ttrLmSrgKey;
    }

    public void setTtrLmSrgKey(FtasLeavemst ttrLmSrgKey) {
        this.ttrLmSrgKey = ttrLmSrgKey;
    }

    public FtasLeavemst getParentLmSrgKey() {
        return parentLmSrgKey;
    }

    public void setParentLmSrgKey(FtasLeavemst parentLmSrgKey) {
        this.parentLmSrgKey = parentLmSrgKey;
    }

    public FtasLeavemst getChildLmSrgKey() {
        return childLmSrgKey;
    }

    public void setChildLmSrgKey(FtasLeavemst childLmSrgKey) {
        this.childLmSrgKey = childLmSrgKey;
    }

    public FhrdEmpleavebal getEmplbSrgKey() {
        return emplbSrgKey;
    }

    public void setEmplbSrgKey(FhrdEmpleavebal emplbSrgKey) {
        this.emplbSrgKey = emplbSrgKey;
    }

    public FhrdEmpleavebal getChildEmplbSrgKey() {
        return childEmplbSrgKey;
    }

    public void setChildEmplbSrgKey(FhrdEmpleavebal childEmplbSrgKey) {
        this.childEmplbSrgKey = childEmplbSrgKey;
    }

    public FhrdEmpleavebal getActualEmplbSrgKey() {
        return actualEmplbSrgKey;
    }

    public void setActualEmplbSrgKey(FhrdEmpleavebal actualEmplbSrgKey) {
        this.actualEmplbSrgKey = actualEmplbSrgKey;
    }

    public FhrdEmpleavebal getParentEmplbSrgKey() {
        return parentEmplbSrgKey;
    }

    public void setParentEmplbSrgKey(FhrdEmpleavebal parentEmplbSrgKey) {
        this.parentEmplbSrgKey = parentEmplbSrgKey;
    }

    public FhrdEmpleavebal getTtrEmplbSrgKey() {
        return ttrEmplbSrgKey;
    }

    public void setTtrEmplbSrgKey(FhrdEmpleavebal ttrEmplbSrgKey) {
        this.ttrEmplbSrgKey = ttrEmplbSrgKey;
    }

    public FhrdEmpleaverule getElrSrgKey() {
        return elrSrgKey;
    }

    public void setElrSrgKey(FhrdEmpleaverule elrSrgKey) {
        this.elrSrgKey = elrSrgKey;
    }

    public FhrdEmpleaverule getChildElrSrgKey() {
        return childElrSrgKey;
    }

    public void setChildElrSrgKey(FhrdEmpleaverule childElrSrgKey) {
        this.childElrSrgKey = childElrSrgKey;
    }

    public FhrdEmpleaverule getActualElrSrgKey() {
        return actualElrSrgKey;
    }

    public void setActualElrSrgKey(FhrdEmpleaverule actualElrSrgKey) {
        this.actualElrSrgKey = actualElrSrgKey;
    }

    public FhrdEmpmst getUserId() {
        return userId;
    }

    public void setUserId(FhrdEmpmst userId) {
        this.userId = userId;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FtasAbsentees getAbsSrgKey() {
        return absSrgKey;
    }

    public void setAbsSrgKey(FtasAbsentees absSrgKey) {
        this.absSrgKey = absSrgKey;
    }

    public FtasLeavemst getActualLmSrgKey() {
        return actualLmSrgKey;
    }

    public void setActualLmSrgKey(FtasLeavemst actualLmSrgKey) {
        this.actualLmSrgKey = actualLmSrgKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (absdSrgKey != null ? absdSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasAbsenteesDtl)) {
            return false;
        }
        FtasAbsenteesDtl other = (FtasAbsenteesDtl) object;
        if ((this.absdSrgKey == null && other.absdSrgKey != null) || (this.absdSrgKey != null && !this.absdSrgKey.equals(other.absdSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasAbsenteesDtl[ absdSrgKey=" + absdSrgKey + " ]";
    }
    
}
