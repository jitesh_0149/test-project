/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_EXPERIENCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdExperience.findAll", query = "SELECT f FROM FhrdExperience f"),
    @NamedQuery(name = "FhrdExperience.findByUserId", query = "SELECT f FROM FhrdExperience f WHERE f.fhrdExperiencePK.userId = :userId"),
    @NamedQuery(name = "FhrdExperience.findByExpSrno", query = "SELECT f FROM FhrdExperience f WHERE f.fhrdExperiencePK.expSrno = :expSrno"),
    @NamedQuery(name = "FhrdExperience.findByCompanyname", query = "SELECT f FROM FhrdExperience f WHERE f.companyname = :companyname"),
    @NamedQuery(name = "FhrdExperience.findByDesignation", query = "SELECT f FROM FhrdExperience f WHERE f.designation = :designation"),
    @NamedQuery(name = "FhrdExperience.findByGrosspay", query = "SELECT f FROM FhrdExperience f WHERE f.grosspay = :grosspay"),
    @NamedQuery(name = "FhrdExperience.findByPerks", query = "SELECT f FROM FhrdExperience f WHERE f.perks = :perks"),
    @NamedQuery(name = "FhrdExperience.findByFromdate", query = "SELECT f FROM FhrdExperience f WHERE f.fromdate = :fromdate"),
    @NamedQuery(name = "FhrdExperience.findByTodate", query = "SELECT f FROM FhrdExperience f WHERE f.todate = :todate"),
    @NamedQuery(name = "FhrdExperience.findByYears", query = "SELECT f FROM FhrdExperience f WHERE f.years = :years"),
    @NamedQuery(name = "FhrdExperience.findByRemarks", query = "SELECT f FROM FhrdExperience f WHERE f.remarks = :remarks"),
    @NamedQuery(name = "FhrdExperience.findByCompanyaddrno", query = "SELECT f FROM FhrdExperience f WHERE f.companyaddrno = :companyaddrno"),
    @NamedQuery(name = "FhrdExperience.findByCreatedt", query = "SELECT f FROM FhrdExperience f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdExperience.findByUpdatedt", query = "SELECT f FROM FhrdExperience f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdExperience.findByExportFlag", query = "SELECT f FROM FhrdExperience f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdExperience.findByImportFlag", query = "SELECT f FROM FhrdExperience f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdExperience.findBySystemUserid", query = "SELECT f FROM FhrdExperience f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdExperience.findByEmpno", query = "SELECT f FROM FhrdExperience f WHERE f.empno = :empno")})
public class FhrdExperience implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FhrdExperiencePK fhrdExperiencePK;
    @Size(max = 100)
    @Column(name = "COMPANYNAME")
    private String companyname;
    @Size(max = 50)
    @Column(name = "DESIGNATION")
    private String designation;
    @Column(name = "GROSSPAY")
    private Long grosspay;
    @Size(max = 500)
    @Column(name = "PERKS")
    private String perks;
    @Column(name = "FROMDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromdate;
    @Column(name = "TODATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date todate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "YEARS")
    private BigDecimal years;
    @Size(max = 250)
    @Column(name = "REMARKS")
    private String remarks;
    @Column(name = "COMPANYADDRNO")
    private Long companyaddrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Column(name = "EMPNO")
    private Integer empno;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FhrdEmpmst fhrdEmpmst;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;

    public FhrdExperience() {
    }

    public FhrdExperience(FhrdExperiencePK fhrdExperiencePK) {
        this.fhrdExperiencePK = fhrdExperiencePK;
    }

    public FhrdExperience(FhrdExperiencePK fhrdExperiencePK, Date createdt) {
        this.fhrdExperiencePK = fhrdExperiencePK;
        this.createdt = createdt;
    }

    public FhrdExperience(int userId, int expSrno) {
        this.fhrdExperiencePK = new FhrdExperiencePK(userId, expSrno);
    }

    public FhrdExperiencePK getFhrdExperiencePK() {
        return fhrdExperiencePK;
    }

    public void setFhrdExperiencePK(FhrdExperiencePK fhrdExperiencePK) {
        this.fhrdExperiencePK = fhrdExperiencePK;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Long getGrosspay() {
        return grosspay;
    }

    public void setGrosspay(Long grosspay) {
        this.grosspay = grosspay;
    }

    public String getPerks() {
        return perks;
    }

    public void setPerks(String perks) {
        this.perks = perks;
    }

    public Date getFromdate() {
        return fromdate;
    }

    public void setFromdate(Date fromdate) {
        this.fromdate = fromdate;
    }

    public Date getTodate() {
        return todate;
    }

    public void setTodate(Date todate) {
        this.todate = todate;
    }

    public BigDecimal getYears() {
        return years;
    }

    public void setYears(BigDecimal years) {
        this.years = years;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Long getCompanyaddrno() {
        return companyaddrno;
    }

    public void setCompanyaddrno(Long companyaddrno) {
        this.companyaddrno = companyaddrno;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public Integer getEmpno() {
        return empno;
    }

    public void setEmpno(Integer empno) {
        this.empno = empno;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getFhrdEmpmst() {
        return fhrdEmpmst;
    }

    public void setFhrdEmpmst(FhrdEmpmst fhrdEmpmst) {
        this.fhrdEmpmst = fhrdEmpmst;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fhrdExperiencePK != null ? fhrdExperiencePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdExperience)) {
            return false;
        }
        FhrdExperience other = (FhrdExperience) object;
        if ((this.fhrdExperiencePK == null && other.fhrdExperiencePK != null) || (this.fhrdExperiencePK != null && !this.fhrdExperiencePK.equals(other.fhrdExperiencePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdExperience[ fhrdExperiencePK=" + fhrdExperiencePK + " ]";
    }
    
}
