/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_EMPMST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdEmpmst.findAll", query = "SELECT f FROM FhrdEmpmst f"),
    @NamedQuery(name = "FhrdEmpmst.findByEmpno", query = "SELECT f FROM FhrdEmpmst f WHERE f.empno = :empno"),
    @NamedQuery(name = "FhrdEmpmst.findByUserId", query = "SELECT f FROM FhrdEmpmst f WHERE f.userId = :userId"),
    @NamedQuery(name = "FhrdEmpmst.findByUserName", query = "SELECT f FROM FhrdEmpmst f WHERE f.userName = :userName"),
    @NamedQuery(name = "FhrdEmpmst.findByPasswd", query = "SELECT f FROM FhrdEmpmst f WHERE f.passwd = :passwd"),
    @NamedQuery(name = "FhrdEmpmst.findByEmpinit", query = "SELECT f FROM FhrdEmpmst f WHERE f.empinit = :empinit"),
    @NamedQuery(name = "FhrdEmpmst.findByBloodgroup", query = "SELECT f FROM FhrdEmpmst f WHERE f.bloodgroup = :bloodgroup"),
    @NamedQuery(name = "FhrdEmpmst.findByGender", query = "SELECT f FROM FhrdEmpmst f WHERE f.gender = :gender"),
    @NamedQuery(name = "FhrdEmpmst.findByBirthdt", query = "SELECT f FROM FhrdEmpmst f WHERE f.birthdt = :birthdt"),
    @NamedQuery(name = "FhrdEmpmst.findByMarital", query = "SELECT f FROM FhrdEmpmst f WHERE f.marital = :marital"),
    @NamedQuery(name = "FhrdEmpmst.findByHometown", query = "SELECT f FROM FhrdEmpmst f WHERE f.hometown = :hometown"),
    @NamedQuery(name = "FhrdEmpmst.findByIfscCode", query = "SELECT f FROM FhrdEmpmst f WHERE f.ifscCode = :ifscCode"),
    @NamedQuery(name = "FhrdEmpmst.findByNtgcfpfno", query = "SELECT f FROM FhrdEmpmst f WHERE f.ntgcfpfno = :ntgcfpfno"),
    @NamedQuery(name = "FhrdEmpmst.findByBankAccountno", query = "SELECT f FROM FhrdEmpmst f WHERE f.bankAccountno = :bankAccountno"),
    @NamedQuery(name = "FhrdEmpmst.findByPanno", query = "SELECT f FROM FhrdEmpmst f WHERE f.panno = :panno"),
    @NamedQuery(name = "FhrdEmpmst.findByPayflag", query = "SELECT f FROM FhrdEmpmst f WHERE f.payflag = :payflag"),
    @NamedQuery(name = "FhrdEmpmst.findByServicetype", query = "SELECT f FROM FhrdEmpmst f WHERE f.servicetype = :servicetype"),
    @NamedQuery(name = "FhrdEmpmst.findByGradesrno", query = "SELECT f FROM FhrdEmpmst f WHERE f.gradesrno = :gradesrno"),
    @NamedQuery(name = "FhrdEmpmst.findByQtrsrno", query = "SELECT f FROM FhrdEmpmst f WHERE f.qtrsrno = :qtrsrno"),
    @NamedQuery(name = "FhrdEmpmst.findByCreateby", query = "SELECT f FROM FhrdEmpmst f WHERE f.createby = :createby"),
    @NamedQuery(name = "FhrdEmpmst.findByCreatedt", query = "SELECT f FROM FhrdEmpmst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdEmpmst.findByUpdateby", query = "SELECT f FROM FhrdEmpmst f WHERE f.updateby = :updateby"),
    @NamedQuery(name = "FhrdEmpmst.findByUpdatedt", query = "SELECT f FROM FhrdEmpmst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdEmpmst.findBySystemUserid", query = "SELECT f FROM FhrdEmpmst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdEmpmst.findByExportFlag", query = "SELECT f FROM FhrdEmpmst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdEmpmst.findByImportFlag", query = "SELECT f FROM FhrdEmpmst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdEmpmst.findByOrgJoinDate", query = "SELECT f FROM FhrdEmpmst f WHERE f.orgJoinDate = :orgJoinDate"),
    @NamedQuery(name = "FhrdEmpmst.findByContractDueDate", query = "SELECT f FROM FhrdEmpmst f WHERE f.contractDueDate = :contractDueDate"),
    @NamedQuery(name = "FhrdEmpmst.findByOrgResignDate", query = "SELECT f FROM FhrdEmpmst f WHERE f.orgResignDate = :orgResignDate"),
    @NamedQuery(name = "FhrdEmpmst.findByOrgRelieveDate", query = "SELECT f FROM FhrdEmpmst f WHERE f.orgRelieveDate = :orgRelieveDate"),
    @NamedQuery(name = "FhrdEmpmst.findByEmpcmSrgKey", query = "SELECT f FROM FhrdEmpmst f WHERE f.empcmSrgKey = :empcmSrgKey"),
    @NamedQuery(name = "FhrdEmpmst.findByBankName", query = "SELECT f FROM FhrdEmpmst f WHERE f.bankName = :bankName"),
    @NamedQuery(name = "FhrdEmpmst.findByBankLocation", query = "SELECT f FROM FhrdEmpmst f WHERE f.bankLocation = :bankLocation"),
    @NamedQuery(name = "FhrdEmpmst.findByEmailId", query = "SELECT f FROM FhrdEmpmst f WHERE f.emailId = :emailId"),
    @NamedQuery(name = "FhrdEmpmst.findByUserLastName", query = "SELECT f FROM FhrdEmpmst f WHERE f.userLastName = :userLastName"),
    @NamedQuery(name = "FhrdEmpmst.findByUserFirstName", query = "SELECT f FROM FhrdEmpmst f WHERE f.userFirstName = :userFirstName"),
    @NamedQuery(name = "FhrdEmpmst.findByVerification", query = "SELECT f FROM FhrdEmpmst f WHERE f.verification = :verification"),
    @NamedQuery(name = "FhrdEmpmst.findByVerificationRemark", query = "SELECT f FROM FhrdEmpmst f WHERE f.verificationRemark = :verificationRemark"),
    @NamedQuery(name = "FhrdEmpmst.findByUserMiddleName", query = "SELECT f FROM FhrdEmpmst f WHERE f.userMiddleName = :userMiddleName"),
    @NamedQuery(name = "FhrdEmpmst.findByNationality", query = "SELECT f FROM FhrdEmpmst f WHERE f.nationality = :nationality"),
    @NamedQuery(name = "FhrdEmpmst.findByPassNo", query = "SELECT f FROM FhrdEmpmst f WHERE f.passNo = :passNo"),
    @NamedQuery(name = "FhrdEmpmst.findByPassAuthority", query = "SELECT f FROM FhrdEmpmst f WHERE f.passAuthority = :passAuthority"),
    @NamedQuery(name = "FhrdEmpmst.findByPassIssueDt", query = "SELECT f FROM FhrdEmpmst f WHERE f.passIssueDt = :passIssueDt"),
    @NamedQuery(name = "FhrdEmpmst.findByPassExpiryDt", query = "SELECT f FROM FhrdEmpmst f WHERE f.passExpiryDt = :passExpiryDt"),
    @NamedQuery(name = "FhrdEmpmst.findByFatherName", query = "SELECT f FROM FhrdEmpmst f WHERE f.fatherName = :fatherName"),
    @NamedQuery(name = "FhrdEmpmst.findByMotherName", query = "SELECT f FROM FhrdEmpmst f WHERE f.motherName = :motherName"),
    @NamedQuery(name = "FhrdEmpmst.findByRgSrgKey", query = "SELECT f FROM FhrdEmpmst f WHERE f.rgSrgKey = :rgSrgKey"),
    @NamedQuery(name = "FhrdEmpmst.findByExpYearConsAtJoining", query = "SELECT f FROM FhrdEmpmst f WHERE f.expYearConsAtJoining = :expYearConsAtJoining"),
    @NamedQuery(name = "FhrdEmpmst.findByExpMonthConsAtJoining", query = "SELECT f FROM FhrdEmpmst f WHERE f.expMonthConsAtJoining = :expMonthConsAtJoining"),
    @NamedQuery(name = "FhrdEmpmst.findByUpdationRemark", query = "SELECT f FROM FhrdEmpmst f WHERE f.updationRemark = :updationRemark"),
    @NamedQuery(name = "FhrdEmpmst.findByPhysicallyChallenged", query = "SELECT f FROM FhrdEmpmst f WHERE f.physicallyChallenged = :physicallyChallenged"),
    @NamedQuery(name = "FhrdEmpmst.findByTerminationDate", query = "SELECT f FROM FhrdEmpmst f WHERE f.terminationDate = :terminationDate"),
    @NamedQuery(name = "FhrdEmpmst.findByUanPfNo", query = "SELECT f FROM FhrdEmpmst f WHERE f.uanPfNo = :uanPfNo")})
public class FhrdEmpmst implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMPNO")
    private int empno;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_ID")
    private Integer userId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "USER_NAME")
    private String userName;
    @Size(max = 40)
    @Column(name = "PASSWD")
    private String passwd;
    @Size(max = 5)
    @Column(name = "EMPINIT")
    private String empinit;
    @Size(max = 6)
    @Column(name = "BLOODGROUP")
    private String bloodgroup;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "GENDER")
    private String gender;
    @Column(name = "BIRTHDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date birthdt;
    @Size(max = 1)
    @Column(name = "MARITAL")
    private String marital;
    @Size(max = 50)
    @Column(name = "HOMETOWN")
    private String hometown;
    @Size(max = 30)
    @Column(name = "IFSC_CODE")
    private String ifscCode;
    @Size(max = 30)
    @Column(name = "NTGCFPFNO")
    private String ntgcfpfno;
    @Size(max = 30)
    @Column(name = "BANK_ACCOUNTNO")
    private String bankAccountno;
    @Size(max = 30)
    @Column(name = "PANNO")
    private String panno;
    @Size(max = 1)
    @Column(name = "PAYFLAG")
    private String payflag;
    @Size(max = 1)
    @Column(name = "SERVICETYPE")
    private String servicetype;
    @Column(name = "GRADESRNO")
    private Short gradesrno;
    @Column(name = "QTRSRNO")
    private Short qtrsrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEBY")
    private int createby;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEBY")
    private Integer updateby;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "ORG_JOIN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgJoinDate;
    @Column(name = "CONTRACT_DUE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date contractDueDate;
    @Column(name = "ORG_RESIGN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgResignDate;
    @Column(name = "ORG_RELIEVE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgRelieveDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "EMPCM_SRG_KEY")
    private BigDecimal empcmSrgKey;
    @Size(max = 70)
    @Column(name = "BANK_NAME")
    private String bankName;
    @Size(max = 30)
    @Column(name = "BANK_LOCATION")
    private String bankLocation;
    @Size(max = 400)
    @Column(name = "EMAIL_ID")
    private String emailId;
    @Size(max = 70)
    @Column(name = "USER_LAST_NAME")
    private String userLastName;
    @Size(max = 70)
    @Column(name = "USER_FIRST_NAME")
    private String userFirstName;
    @Size(max = 1)
    @Column(name = "VERIFICATION")
    private String verification;
    @Size(max = 1000)
    @Column(name = "VERIFICATION_REMARK")
    private String verificationRemark;
    @Lob
    @Column(name = "EMP_IMG")
    private byte[] empImg;
    @Size(max = 70)
    @Column(name = "USER_MIDDLE_NAME")
    private String userMiddleName;
    @Size(max = 100)
    @Column(name = "NATIONALITY")
    private String nationality;
    @Size(max = 20)
    @Column(name = "PASS_NO")
    private String passNo;
    @Size(max = 100)
    @Column(name = "PASS_AUTHORITY")
    private String passAuthority;
    @Column(name = "PASS_ISSUE_DT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date passIssueDt;
    @Column(name = "PASS_EXPIRY_DT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date passExpiryDt;
    @Size(max = 100)
    @Column(name = "FATHER_NAME")
    private String fatherName;
    @Size(max = 100)
    @Column(name = "MOTHER_NAME")
    private String motherName;
    @Column(name = "RG_SRG_KEY")
    private Integer rgSrgKey;
    @Column(name = "EXP_YEAR_CONS_AT_JOINING")
    private Short expYearConsAtJoining;
    @Column(name = "EXP_MONTH_CONS_AT_JOINING")
    private Short expMonthConsAtJoining;
    @Size(max = 500)
    @Column(name = "UPDATION_REMARK")
    private String updationRemark;
    @Size(max = 1)
    @Column(name = "PHYSICALLY_CHALLENGED")
    private String physicallyChallenged;
    @Column(name = "TERMINATION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date terminationDate;
    @Column(name = "UAN_PF_NO")
    private Long uanPfNo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasTadaOtherDtl> ftasTadaOtherDtlCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasTadaOtherDtl> ftasTadaOtherDtlCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<BmwpUnitMst> bmwpUnitMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<BmwpUnitMst> bmwpUnitMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdContractGroupRelation> fhrdContractGroupRelationCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdContractGroupRelation> fhrdContractGroupRelationCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FcadTeleFax> fcadTeleFaxCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FcadTeleFax> fcadTeleFaxCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasLeavemst> ftasLeavemstCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasLeavemst> ftasLeavemstCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdContractMst> fhrdContractMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdContractMst> fhrdContractMstCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<BmwpAgencyProjectLink> bmwpAgencyProjectLinkCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<BmwpAgencyProjectLink> bmwpAgencyProjectLinkCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<BmwpAgencyProjectCategory> bmwpAgencyProjectCategoryCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<BmwpAgencyProjectCategory> bmwpAgencyProjectCategoryCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdExperience> fhrdExperienceCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fhrdEmpmst")
    private Collection<FhrdExperience> fhrdExperienceCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdExperience> fhrdExperienceCollection2;
    @OneToMany(mappedBy = "updateby")
    private Collection<FcadAddrGrpDtl> fcadAddrGrpDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FcadAddrGrpDtl> fcadAddrGrpDtlCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdRuleManagGroupMst> fhrdRuleManagGroupMstCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdRuleManagGroupMst> fhrdRuleManagGroupMstCollection1;
    @OneToMany(mappedBy = "aprvby")
    private Collection<FtasTadaBillMst> ftasTadaBillMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "actnby")
    private Collection<FtasTadaBillMst> ftasTadaBillMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasTadaBillMst> ftasTadaBillMstCollection2;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasTadaBillMst> ftasTadaBillMstCollection3;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<FtasTadaBillMst> ftasTadaBillMstCollection4;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fhrdEmpmst")
    private Collection<FhrdPaytransearndednSummary> fhrdPaytransearndednSummaryCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasLockMst> ftasLockMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasLockMst> ftasLockMstCollection1;
    @OneToMany(mappedBy = "createby")
    private Collection<FhrdEdRepMst> fhrdEdRepMstCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdEdRepMst> fhrdEdRepMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "actnby")
    private Collection<FtasAbsentees> ftasAbsenteesCollection;
    @OneToMany(mappedBy = "aprvby")
    private Collection<FtasAbsentees> ftasAbsenteesCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasAbsentees> ftasAbsenteesCollection2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasAbsentees> ftasAbsenteesCollection3;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<FtasAbsentees> ftasAbsenteesCollection4;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasTadaConvMst> ftasTadaConvMstCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasTadaConvMst> ftasTadaConvMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdUniversitymst> fhrdUniversitymstCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdUniversitymst> fhrdUniversitymstCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasCategoryMst> ftasCategoryMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasCategoryMst> ftasCategoryMstCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<SystMenumst> systMenumstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<SystMenumst> systMenumstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasTadaGradeMst> ftasTadaGradeMstCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasTadaGradeMst> ftasTadaGradeMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdQualification> fhrdQualificationCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdQualification> fhrdQualificationCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fhrdEmpmst")
    private Collection<FhrdQualification> fhrdQualificationCollection2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasTadaCashDednDtl> ftasTadaCashDednDtlCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasTadaCashDednDtl> ftasTadaCashDednDtlCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdEmpeventRelieve> fhrdEmpeventRelieveCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<FhrdEmpeventRelieve> fhrdEmpeventRelieveCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdEmpeventRelieve> fhrdEmpeventRelieveCollection2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdGradedtl> fhrdGradedtlCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdGradedtl> fhrdGradedtlCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FcadAddrDtl> fcadAddrDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FcadAddrDtl> fcadAddrDtlCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdEmpCategoryMst> fhrdEmpCategoryMstCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdEmpCategoryMst> fhrdEmpCategoryMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection1;
    @OneToMany(mappedBy = "createby")
    private Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection2;
    @OneToMany(mappedBy = "createby")
    private Collection<FhrdEdRepDtl> fhrdEdRepDtlCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdEdRepDtl> fhrdEdRepDtlCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<SystOrgUnitTypeMst> systOrgUnitTypeMstCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<SystOrgUnitTypeMst> systOrgUnitTypeMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fhrdEmpmst")
    private Collection<FhrdPaytransearndedn> fhrdPaytransearndednCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdPaytransearndedn> fhrdPaytransearndednCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdPaytransearndedn> fhrdPaytransearndednCollection2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasTrainingMst> ftasTrainingMstCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasTrainingMst> ftasTrainingMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "announceBy")
    private Collection<FtasTrainingMst> ftasTrainingMstCollection2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdEarndedn> fhrdEarndednCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdEarndedn> fhrdEarndednCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FcadAddrJobStageTitle> fcadAddrJobStageTitleCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FcadAddrJobStageTitle> fcadAddrJobStageTitleCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdRuleGroupRelation> fhrdRuleGroupRelationCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdRuleGroupRelation> fhrdRuleGroupRelationCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdServicemst> fhrdServicemstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdServicemst> fhrdServicemstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdDegreedtl> fhrdDegreedtlCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdDegreedtl> fhrdDegreedtlCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdRuleApplGroupMst> fhrdRuleApplGroupMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdRuleApplGroupMst> fhrdRuleApplGroupMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdPaytransleaveDtl> fhrdPaytransleaveDtlCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdPaytransleaveDtl> fhrdPaytransleaveDtlCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<FhrdPaytransleaveDtl> fhrdPaytransleaveDtlCollection2;
    @OneToMany(mappedBy = "updateby")
    private Collection<BmwpFundAgencyMst> bmwpFundAgencyMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<BmwpFundAgencyMst> bmwpFundAgencyMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<FtasDaily> ftasDailyCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasDaily> ftasDailyCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasDaily> ftasDailyCollection2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdEmpleavebal> fhrdEmpleavebalCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdEmpleavebal> fhrdEmpleavebalCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<FhrdEmpleavebal> fhrdEmpleavebalCollection2;
    @OneToMany(mappedBy = "updateby")
    private Collection<BmwpPartyTypeMst> bmwpPartyTypeMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<BmwpPartyTypeMst> bmwpPartyTypeMstCollection1;
    @OneToMany(mappedBy = "createby")
    private Collection<FhrdReligionMst> fhrdReligionMstCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdReligionMst> fhrdReligionMstCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdPaytransMonvar> fhrdPaytransMonvarCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<FhrdPaytransMonvar> fhrdPaytransMonvarCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdPaytransMonvar> fhrdPaytransMonvarCollection2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FcadPersDtl> fcadPersDtlCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FcadPersDtl> fcadPersDtlCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasTadaGradeCityLink> ftasTadaGradeCityLinkCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasTadaGradeCityLink> ftasTadaGradeCityLinkCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection2;
    @OneToMany(mappedBy = "updateby")
    private Collection<BmwpActivityBudgetLink> bmwpActivityBudgetLinkCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<BmwpActivityBudgetLink> bmwpActivityBudgetLinkCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FcadAddrJobMst> fcadAddrJobMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FcadAddrJobMst> fcadAddrJobMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasTadaFareDtl> ftasTadaFareDtlCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasTadaFareDtl> ftasTadaFareDtlCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasLockDtl> ftasLockDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasLockDtl> ftasLockDtlCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fhrdEmpmst")
    private Collection<FtasBulkAttTemp> ftasBulkAttTempCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdRuleGroupCategory> fhrdRuleGroupCategoryCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdRuleGroupCategory> fhrdRuleGroupCategoryCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdDegreeLevelMst> fhrdDegreeLevelMstCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdDegreeLevelMst> fhrdDegreeLevelMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasTrainingScheduleDtl> ftasTrainingScheduleDtlCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasTrainingScheduleDtl> ftasTrainingScheduleDtlCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdEarndednmst> fhrdEarndednmstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdEarndednmst> fhrdEarndednmstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasTourScheduleDtl> ftasTourScheduleDtlCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasTourScheduleDtl> ftasTourScheduleDtlCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdEmpEventDtl> fhrdEmpEventDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdEmpEventDtl> fhrdEmpEventDtlCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<FhrdEmpEventDtl> fhrdEmpEventDtlCollection2;
    @OneToMany(mappedBy = "createby")
    private Collection<FhrdDegreeStreamMst> fhrdDegreeStreamMstCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdDegreeStreamMst> fhrdDegreeStreamMstCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdPaytransleaveMst> fhrdPaytransleaveMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdPaytransleaveMst> fhrdPaytransleaveMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<FhrdPaytransleaveMst> fhrdPaytransleaveMstCollection2;
    @OneToMany(mappedBy = "updateby")
    private Collection<FcadAddrGrpMst> fcadAddrGrpMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FcadAddrGrpMst> fcadAddrGrpMstCollection1;
    @JoinColumns({
        @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO"),
        @JoinColumn(name = "USER_TYPE_SRNO", referencedColumnName = "USER_TYPE_SRNO")})
    @ManyToOne(optional = false)
    private SystUserTypeMst systUserTypeMst;
    @JoinColumn(name = "POSTING_OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst postingOumUnitSrno;
    @JoinColumn(name = "PM_UNIQUE_SRNO", referencedColumnName = "PM_UNIQUE_SRNO")
    @ManyToOne
    private FhrdPortfolioMst pmUniqueSrno;
    @OneToMany(mappedBy = "reportingUserId")
    private Collection<FhrdEmpmst> fhrdEmpmstCollection;
    @JoinColumn(name = "REPORTING_USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst reportingUserId;
    @JoinColumn(name = "DSGM_SRG_KEY", referencedColumnName = "DSGM_SRG_KEY")
    @ManyToOne
    private FhrdDesigmst dsgmSrgKey;
    @JoinColumn(name = "CCM_SRG_KEY", referencedColumnName = "CCM_SRG_KEY")
    @ManyToOne
    private FhrdCasteCategoryMst ccmSrgKey;
    @JoinColumn(name = "ADDRESSNO", referencedColumnName = "AM_UNIQUE_SRNO")
    @ManyToOne
    private FcadAddrMst addressno;
    @OneToMany(mappedBy = "updateby")
    private Collection<BmwpFundSrcMst> bmwpFundSrcMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<BmwpFundSrcMst> bmwpFundSrcMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasHolidayDtl> ftasHolidayDtlCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasHolidayDtl> ftasHolidayDtlCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<BmwpBudgetMst> bmwpBudgetMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<BmwpBudgetMst> bmwpBudgetMstCollection1;
    @OneToMany(mappedBy = "createby")
    private Collection<FhrdPortfolioMst> fhrdPortfolioMstCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdPortfolioMst> fhrdPortfolioMstCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<BmwpAgencyTypeMst> bmwpAgencyTypeMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<BmwpAgencyTypeMst> bmwpAgencyTypeMstCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<BmwpAgenSecMst> bmwpAgenSecMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<BmwpAgenSecMst> bmwpAgenSecMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasTadaFacilityDtl> ftasTadaFacilityDtlCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasTadaFacilityDtl> ftasTadaFacilityDtlCollection1;
    @OneToMany(mappedBy = "alertReceivingPerson2")
    private Collection<FhrdEventMst> fhrdEventMstCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdEventMst> fhrdEventMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdEventMst> fhrdEventMstCollection2;
    @OneToMany(mappedBy = "alertReceivingPerson")
    private Collection<FhrdEventMst> fhrdEventMstCollection3;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdDegreemst> fhrdDegreemstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdDegreemst> fhrdDegreemstCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasHolidayMst> ftasHolidayMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasHolidayMst> ftasHolidayMstCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<BmwpApActivityMst> bmwpApActivityMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<BmwpApActivityMst> bmwpApActivityMstCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<BmwpActivityMst> bmwpActivityMstCollection;
    @OneToMany(mappedBy = "createby")
    private Collection<BmwpActivityMst> bmwpActivityMstCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<BmwpPartyTypeDtl> bmwpPartyTypeDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<BmwpPartyTypeDtl> bmwpPartyTypeDtlCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdPaytrans> fhrdPaytransCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<FhrdPaytrans> fhrdPaytransCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdPaytrans> fhrdPaytransCollection2;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdEmpEncash> fhrdEmpEncashCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<FhrdEmpEncash> fhrdEmpEncashCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdEmpEncash> fhrdEmpEncashCollection2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "actnby")
    private Collection<FhrdEmpEncash> fhrdEmpEncashCollection3;
    @OneToMany(mappedBy = "aprvby")
    private Collection<FhrdEmpEncash> fhrdEmpEncashCollection4;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<BmwpWorkplan> bmwpWorkplanCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<BmwpWorkplan> bmwpWorkplanCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fhrdEmpmst")
    private Collection<FhrdPaytransSummary> fhrdPaytransSummaryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasTrainingOrgUnitDtl> ftasTrainingOrgUnitDtlCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasTrainingOrgUnitDtl> ftasTrainingOrgUnitDtlCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdQualificationLevel> fhrdQualificationLevelCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdQualificationLevel> fhrdQualificationLevelCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdEmpeventPromotion> fhrdEmpeventPromotionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<FhrdEmpeventPromotion> fhrdEmpeventPromotionCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdEmpeventPromotion> fhrdEmpeventPromotionCollection2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdGrademst> fhrdGrademstCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdGrademst> fhrdGrademstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdEarndednConfig> fhrdEarndednConfigCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdEarndednConfig> fhrdEarndednConfigCollection1;
    @OneToMany(mappedBy = "currentReportingUserId")
    private Collection<FhrdEmpeventTransfer> fhrdEmpeventTransferCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<FhrdEmpeventTransfer> fhrdEmpeventTransferCollection1;
    @OneToMany(mappedBy = "newReportingUserId")
    private Collection<FhrdEmpeventTransfer> fhrdEmpeventTransferCollection2;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdEmpeventTransfer> fhrdEmpeventTransferCollection3;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdEmpeventTransfer> fhrdEmpeventTransferCollection4;
    @OneToMany(mappedBy = "updateby")
    private Collection<BmwpActivityBudgetDtl> bmwpActivityBudgetDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<BmwpActivityBudgetDtl> bmwpActivityBudgetDtlCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasTrainingAnnDetail> ftasTrainingAnnDetailCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasTrainingAnnDetail> ftasTrainingAnnDetailCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdEmpContractMst> fhrdEmpContractMstCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdEmpContractMst> fhrdEmpContractMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<FhrdEmpContractMst> fhrdEmpContractMstCollection2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasTadaCashallowRuleMst> ftasTadaCashallowRuleMstCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasTadaCashallowRuleMst> ftasTadaCashallowRuleMstCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FcadAddrMst> fcadAddrMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FcadAddrMst> fcadAddrMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<BmwpProjectMst> bmwpProjectMstCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<BmwpProjectMst> bmwpProjectMstCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdEmpearndedn> fhrdEmpearndednCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<FhrdEmpearndedn> fhrdEmpearndednCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdEmpearndedn> fhrdEmpearndednCollection2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdLeaverule> fhrdLeaveruleCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdLeaverule> fhrdLeaveruleCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasTadaConvExpDtl> ftasTadaConvExpDtlCollection;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasTadaConvExpDtl> ftasTadaConvExpDtlCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<FhrdEmpCancEncash> fhrdEmpCancEncashCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdEmpCancEncash> fhrdEmpCancEncashCollection1;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdEmpCancEncash> fhrdEmpCancEncashCollection2;
    @OneToMany(mappedBy = "actnby")
    private Collection<FhrdEmpCancEncash> fhrdEmpCancEncashCollection3;
    @OneToMany(mappedBy = "aprvby")
    private Collection<FhrdEmpCancEncash> fhrdEmpCancEncashCollection4;
    @OneToMany(mappedBy = "aprvby")
    private Collection<FtasCancelabs> ftasCancelabsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FtasCancelabs> ftasCancelabsCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "actnby")
    private Collection<FtasCancelabs> ftasCancelabsCollection2;
    @OneToMany(mappedBy = "updateby")
    private Collection<FtasCancelabs> ftasCancelabsCollection3;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<FtasCancelabs> ftasCancelabsCollection4;
    @OneToMany(mappedBy = "updateby")
    private Collection<FhrdDependent> fhrdDependentCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createby")
    private Collection<FhrdDependent> fhrdDependentCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fhrdEmpmst")
    private Collection<FhrdDependent> fhrdDependentCollection2;

    public FhrdEmpmst() {
    }

    public FhrdEmpmst(Integer userId) {
        this.userId = userId;
    }

    public FhrdEmpmst(Integer userId, int empno, String userName, String gender, int createby, Date createdt) {
        this.userId = userId;
        this.empno = empno;
        this.userName = userName;
        this.gender = gender;
        this.createby = createby;
        this.createdt = createdt;
    }

    public int getEmpno() {
        return empno;
    }

    public void setEmpno(int empno) {
        this.empno = empno;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getEmpinit() {
        return empinit;
    }

    public void setEmpinit(String empinit) {
        this.empinit = empinit;
    }

    public String getBloodgroup() {
        return bloodgroup;
    }

    public void setBloodgroup(String bloodgroup) {
        this.bloodgroup = bloodgroup;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthdt() {
        return birthdt;
    }

    public void setBirthdt(Date birthdt) {
        this.birthdt = birthdt;
    }

    public String getMarital() {
        return marital;
    }

    public void setMarital(String marital) {
        this.marital = marital;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getNtgcfpfno() {
        return ntgcfpfno;
    }

    public void setNtgcfpfno(String ntgcfpfno) {
        this.ntgcfpfno = ntgcfpfno;
    }

    public String getBankAccountno() {
        return bankAccountno;
    }

    public void setBankAccountno(String bankAccountno) {
        this.bankAccountno = bankAccountno;
    }

    public String getPanno() {
        return panno;
    }

    public void setPanno(String panno) {
        this.panno = panno;
    }

    public String getPayflag() {
        return payflag;
    }

    public void setPayflag(String payflag) {
        this.payflag = payflag;
    }

    public String getServicetype() {
        return servicetype;
    }

    public void setServicetype(String servicetype) {
        this.servicetype = servicetype;
    }

    public Short getGradesrno() {
        return gradesrno;
    }

    public void setGradesrno(Short gradesrno) {
        this.gradesrno = gradesrno;
    }

    public Short getQtrsrno() {
        return qtrsrno;
    }

    public void setQtrsrno(Short qtrsrno) {
        this.qtrsrno = qtrsrno;
    }

    public int getCreateby() {
        return createby;
    }

    public void setCreateby(int createby) {
        this.createby = createby;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Integer getUpdateby() {
        return updateby;
    }

    public void setUpdateby(Integer updateby) {
        this.updateby = updateby;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Date getOrgJoinDate() {
        return orgJoinDate;
    }

    public void setOrgJoinDate(Date orgJoinDate) {
        this.orgJoinDate = orgJoinDate;
    }

    public Date getContractDueDate() {
        return contractDueDate;
    }

    public void setContractDueDate(Date contractDueDate) {
        this.contractDueDate = contractDueDate;
    }

    public Date getOrgResignDate() {
        return orgResignDate;
    }

    public void setOrgResignDate(Date orgResignDate) {
        this.orgResignDate = orgResignDate;
    }

    public Date getOrgRelieveDate() {
        return orgRelieveDate;
    }

    public void setOrgRelieveDate(Date orgRelieveDate) {
        this.orgRelieveDate = orgRelieveDate;
    }

    public BigDecimal getEmpcmSrgKey() {
        return empcmSrgKey;
    }

    public void setEmpcmSrgKey(BigDecimal empcmSrgKey) {
        this.empcmSrgKey = empcmSrgKey;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankLocation() {
        return bankLocation;
    }

    public void setBankLocation(String bankLocation) {
        this.bankLocation = bankLocation;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getVerification() {
        return verification;
    }

    public void setVerification(String verification) {
        this.verification = verification;
    }

    public String getVerificationRemark() {
        return verificationRemark;
    }

    public void setVerificationRemark(String verificationRemark) {
        this.verificationRemark = verificationRemark;
    }

    public byte[] getEmpImg() {
        return empImg;
    }

    public void setEmpImg(byte[] empImg) {
        this.empImg = empImg;
    }

    public String getUserMiddleName() {
        return userMiddleName;
    }

    public void setUserMiddleName(String userMiddleName) {
        this.userMiddleName = userMiddleName;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getPassNo() {
        return passNo;
    }

    public void setPassNo(String passNo) {
        this.passNo = passNo;
    }

    public String getPassAuthority() {
        return passAuthority;
    }

    public void setPassAuthority(String passAuthority) {
        this.passAuthority = passAuthority;
    }

    public Date getPassIssueDt() {
        return passIssueDt;
    }

    public void setPassIssueDt(Date passIssueDt) {
        this.passIssueDt = passIssueDt;
    }

    public Date getPassExpiryDt() {
        return passExpiryDt;
    }

    public void setPassExpiryDt(Date passExpiryDt) {
        this.passExpiryDt = passExpiryDt;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public Integer getRgSrgKey() {
        return rgSrgKey;
    }

    public void setRgSrgKey(Integer rgSrgKey) {
        this.rgSrgKey = rgSrgKey;
    }

    public Short getExpYearConsAtJoining() {
        return expYearConsAtJoining;
    }

    public void setExpYearConsAtJoining(Short expYearConsAtJoining) {
        this.expYearConsAtJoining = expYearConsAtJoining;
    }

    public Short getExpMonthConsAtJoining() {
        return expMonthConsAtJoining;
    }

    public void setExpMonthConsAtJoining(Short expMonthConsAtJoining) {
        this.expMonthConsAtJoining = expMonthConsAtJoining;
    }

    public String getUpdationRemark() {
        return updationRemark;
    }

    public void setUpdationRemark(String updationRemark) {
        this.updationRemark = updationRemark;
    }

    public String getPhysicallyChallenged() {
        return physicallyChallenged;
    }

    public void setPhysicallyChallenged(String physicallyChallenged) {
        this.physicallyChallenged = physicallyChallenged;
    }

    public Date getTerminationDate() {
        return terminationDate;
    }

    public void setTerminationDate(Date terminationDate) {
        this.terminationDate = terminationDate;
    }

    public Long getUanPfNo() {
        return uanPfNo;
    }

    public void setUanPfNo(Long uanPfNo) {
        this.uanPfNo = uanPfNo;
    }

    @XmlTransient
    public Collection<FtasTadaOtherDtl> getFtasTadaOtherDtlCollection() {
        return ftasTadaOtherDtlCollection;
    }

    public void setFtasTadaOtherDtlCollection(Collection<FtasTadaOtherDtl> ftasTadaOtherDtlCollection) {
        this.ftasTadaOtherDtlCollection = ftasTadaOtherDtlCollection;
    }

    @XmlTransient
    public Collection<FtasTadaOtherDtl> getFtasTadaOtherDtlCollection1() {
        return ftasTadaOtherDtlCollection1;
    }

    public void setFtasTadaOtherDtlCollection1(Collection<FtasTadaOtherDtl> ftasTadaOtherDtlCollection1) {
        this.ftasTadaOtherDtlCollection1 = ftasTadaOtherDtlCollection1;
    }

    @XmlTransient
    public Collection<BmwpUnitMst> getBmwpUnitMstCollection() {
        return bmwpUnitMstCollection;
    }

    public void setBmwpUnitMstCollection(Collection<BmwpUnitMst> bmwpUnitMstCollection) {
        this.bmwpUnitMstCollection = bmwpUnitMstCollection;
    }

    @XmlTransient
    public Collection<BmwpUnitMst> getBmwpUnitMstCollection1() {
        return bmwpUnitMstCollection1;
    }

    public void setBmwpUnitMstCollection1(Collection<BmwpUnitMst> bmwpUnitMstCollection1) {
        this.bmwpUnitMstCollection1 = bmwpUnitMstCollection1;
    }

    @XmlTransient
    public Collection<FhrdContractGroupRelation> getFhrdContractGroupRelationCollection() {
        return fhrdContractGroupRelationCollection;
    }

    public void setFhrdContractGroupRelationCollection(Collection<FhrdContractGroupRelation> fhrdContractGroupRelationCollection) {
        this.fhrdContractGroupRelationCollection = fhrdContractGroupRelationCollection;
    }

    @XmlTransient
    public Collection<FhrdContractGroupRelation> getFhrdContractGroupRelationCollection1() {
        return fhrdContractGroupRelationCollection1;
    }

    public void setFhrdContractGroupRelationCollection1(Collection<FhrdContractGroupRelation> fhrdContractGroupRelationCollection1) {
        this.fhrdContractGroupRelationCollection1 = fhrdContractGroupRelationCollection1;
    }

    @XmlTransient
    public Collection<FcadTeleFax> getFcadTeleFaxCollection() {
        return fcadTeleFaxCollection;
    }

    public void setFcadTeleFaxCollection(Collection<FcadTeleFax> fcadTeleFaxCollection) {
        this.fcadTeleFaxCollection = fcadTeleFaxCollection;
    }

    @XmlTransient
    public Collection<FcadTeleFax> getFcadTeleFaxCollection1() {
        return fcadTeleFaxCollection1;
    }

    public void setFcadTeleFaxCollection1(Collection<FcadTeleFax> fcadTeleFaxCollection1) {
        this.fcadTeleFaxCollection1 = fcadTeleFaxCollection1;
    }

    @XmlTransient
    public Collection<FtasLeavemst> getFtasLeavemstCollection() {
        return ftasLeavemstCollection;
    }

    public void setFtasLeavemstCollection(Collection<FtasLeavemst> ftasLeavemstCollection) {
        this.ftasLeavemstCollection = ftasLeavemstCollection;
    }

    @XmlTransient
    public Collection<FtasLeavemst> getFtasLeavemstCollection1() {
        return ftasLeavemstCollection1;
    }

    public void setFtasLeavemstCollection1(Collection<FtasLeavemst> ftasLeavemstCollection1) {
        this.ftasLeavemstCollection1 = ftasLeavemstCollection1;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetLink> getBmwpApActivityBudgetLinkCollection() {
        return bmwpApActivityBudgetLinkCollection;
    }

    public void setBmwpApActivityBudgetLinkCollection(Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection) {
        this.bmwpApActivityBudgetLinkCollection = bmwpApActivityBudgetLinkCollection;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetLink> getBmwpApActivityBudgetLinkCollection1() {
        return bmwpApActivityBudgetLinkCollection1;
    }

    public void setBmwpApActivityBudgetLinkCollection1(Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection1) {
        this.bmwpApActivityBudgetLinkCollection1 = bmwpApActivityBudgetLinkCollection1;
    }

    @XmlTransient
    public Collection<FhrdContractMst> getFhrdContractMstCollection() {
        return fhrdContractMstCollection;
    }

    public void setFhrdContractMstCollection(Collection<FhrdContractMst> fhrdContractMstCollection) {
        this.fhrdContractMstCollection = fhrdContractMstCollection;
    }

    @XmlTransient
    public Collection<FhrdContractMst> getFhrdContractMstCollection1() {
        return fhrdContractMstCollection1;
    }

    public void setFhrdContractMstCollection1(Collection<FhrdContractMst> fhrdContractMstCollection1) {
        this.fhrdContractMstCollection1 = fhrdContractMstCollection1;
    }

    @XmlTransient
    public Collection<BmwpAgencyProjectLink> getBmwpAgencyProjectLinkCollection() {
        return bmwpAgencyProjectLinkCollection;
    }

    public void setBmwpAgencyProjectLinkCollection(Collection<BmwpAgencyProjectLink> bmwpAgencyProjectLinkCollection) {
        this.bmwpAgencyProjectLinkCollection = bmwpAgencyProjectLinkCollection;
    }

    @XmlTransient
    public Collection<BmwpAgencyProjectLink> getBmwpAgencyProjectLinkCollection1() {
        return bmwpAgencyProjectLinkCollection1;
    }

    public void setBmwpAgencyProjectLinkCollection1(Collection<BmwpAgencyProjectLink> bmwpAgencyProjectLinkCollection1) {
        this.bmwpAgencyProjectLinkCollection1 = bmwpAgencyProjectLinkCollection1;
    }

    @XmlTransient
    public Collection<BmwpAgencyProjectCategory> getBmwpAgencyProjectCategoryCollection() {
        return bmwpAgencyProjectCategoryCollection;
    }

    public void setBmwpAgencyProjectCategoryCollection(Collection<BmwpAgencyProjectCategory> bmwpAgencyProjectCategoryCollection) {
        this.bmwpAgencyProjectCategoryCollection = bmwpAgencyProjectCategoryCollection;
    }

    @XmlTransient
    public Collection<BmwpAgencyProjectCategory> getBmwpAgencyProjectCategoryCollection1() {
        return bmwpAgencyProjectCategoryCollection1;
    }

    public void setBmwpAgencyProjectCategoryCollection1(Collection<BmwpAgencyProjectCategory> bmwpAgencyProjectCategoryCollection1) {
        this.bmwpAgencyProjectCategoryCollection1 = bmwpAgencyProjectCategoryCollection1;
    }

    @XmlTransient
    public Collection<FhrdExperience> getFhrdExperienceCollection() {
        return fhrdExperienceCollection;
    }

    public void setFhrdExperienceCollection(Collection<FhrdExperience> fhrdExperienceCollection) {
        this.fhrdExperienceCollection = fhrdExperienceCollection;
    }

    @XmlTransient
    public Collection<FhrdExperience> getFhrdExperienceCollection1() {
        return fhrdExperienceCollection1;
    }

    public void setFhrdExperienceCollection1(Collection<FhrdExperience> fhrdExperienceCollection1) {
        this.fhrdExperienceCollection1 = fhrdExperienceCollection1;
    }

    @XmlTransient
    public Collection<FhrdExperience> getFhrdExperienceCollection2() {
        return fhrdExperienceCollection2;
    }

    public void setFhrdExperienceCollection2(Collection<FhrdExperience> fhrdExperienceCollection2) {
        this.fhrdExperienceCollection2 = fhrdExperienceCollection2;
    }

    @XmlTransient
    public Collection<FcadAddrGrpDtl> getFcadAddrGrpDtlCollection() {
        return fcadAddrGrpDtlCollection;
    }

    public void setFcadAddrGrpDtlCollection(Collection<FcadAddrGrpDtl> fcadAddrGrpDtlCollection) {
        this.fcadAddrGrpDtlCollection = fcadAddrGrpDtlCollection;
    }

    @XmlTransient
    public Collection<FcadAddrGrpDtl> getFcadAddrGrpDtlCollection1() {
        return fcadAddrGrpDtlCollection1;
    }

    public void setFcadAddrGrpDtlCollection1(Collection<FcadAddrGrpDtl> fcadAddrGrpDtlCollection1) {
        this.fcadAddrGrpDtlCollection1 = fcadAddrGrpDtlCollection1;
    }

    @XmlTransient
    public Collection<FhrdRuleManagGroupMst> getFhrdRuleManagGroupMstCollection() {
        return fhrdRuleManagGroupMstCollection;
    }

    public void setFhrdRuleManagGroupMstCollection(Collection<FhrdRuleManagGroupMst> fhrdRuleManagGroupMstCollection) {
        this.fhrdRuleManagGroupMstCollection = fhrdRuleManagGroupMstCollection;
    }

    @XmlTransient
    public Collection<FhrdRuleManagGroupMst> getFhrdRuleManagGroupMstCollection1() {
        return fhrdRuleManagGroupMstCollection1;
    }

    public void setFhrdRuleManagGroupMstCollection1(Collection<FhrdRuleManagGroupMst> fhrdRuleManagGroupMstCollection1) {
        this.fhrdRuleManagGroupMstCollection1 = fhrdRuleManagGroupMstCollection1;
    }

    @XmlTransient
    public Collection<FtasTadaBillMst> getFtasTadaBillMstCollection() {
        return ftasTadaBillMstCollection;
    }

    public void setFtasTadaBillMstCollection(Collection<FtasTadaBillMst> ftasTadaBillMstCollection) {
        this.ftasTadaBillMstCollection = ftasTadaBillMstCollection;
    }

    @XmlTransient
    public Collection<FtasTadaBillMst> getFtasTadaBillMstCollection1() {
        return ftasTadaBillMstCollection1;
    }

    public void setFtasTadaBillMstCollection1(Collection<FtasTadaBillMst> ftasTadaBillMstCollection1) {
        this.ftasTadaBillMstCollection1 = ftasTadaBillMstCollection1;
    }

    @XmlTransient
    public Collection<FtasTadaBillMst> getFtasTadaBillMstCollection2() {
        return ftasTadaBillMstCollection2;
    }

    public void setFtasTadaBillMstCollection2(Collection<FtasTadaBillMst> ftasTadaBillMstCollection2) {
        this.ftasTadaBillMstCollection2 = ftasTadaBillMstCollection2;
    }

    @XmlTransient
    public Collection<FtasTadaBillMst> getFtasTadaBillMstCollection3() {
        return ftasTadaBillMstCollection3;
    }

    public void setFtasTadaBillMstCollection3(Collection<FtasTadaBillMst> ftasTadaBillMstCollection3) {
        this.ftasTadaBillMstCollection3 = ftasTadaBillMstCollection3;
    }

    @XmlTransient
    public Collection<FtasTadaBillMst> getFtasTadaBillMstCollection4() {
        return ftasTadaBillMstCollection4;
    }

    public void setFtasTadaBillMstCollection4(Collection<FtasTadaBillMst> ftasTadaBillMstCollection4) {
        this.ftasTadaBillMstCollection4 = ftasTadaBillMstCollection4;
    }

    @XmlTransient
    public Collection<FhrdPaytransearndednSummary> getFhrdPaytransearndednSummaryCollection() {
        return fhrdPaytransearndednSummaryCollection;
    }

    public void setFhrdPaytransearndednSummaryCollection(Collection<FhrdPaytransearndednSummary> fhrdPaytransearndednSummaryCollection) {
        this.fhrdPaytransearndednSummaryCollection = fhrdPaytransearndednSummaryCollection;
    }

    @XmlTransient
    public Collection<FtasLockMst> getFtasLockMstCollection() {
        return ftasLockMstCollection;
    }

    public void setFtasLockMstCollection(Collection<FtasLockMst> ftasLockMstCollection) {
        this.ftasLockMstCollection = ftasLockMstCollection;
    }

    @XmlTransient
    public Collection<FtasLockMst> getFtasLockMstCollection1() {
        return ftasLockMstCollection1;
    }

    public void setFtasLockMstCollection1(Collection<FtasLockMst> ftasLockMstCollection1) {
        this.ftasLockMstCollection1 = ftasLockMstCollection1;
    }

    @XmlTransient
    public Collection<FhrdEdRepMst> getFhrdEdRepMstCollection() {
        return fhrdEdRepMstCollection;
    }

    public void setFhrdEdRepMstCollection(Collection<FhrdEdRepMst> fhrdEdRepMstCollection) {
        this.fhrdEdRepMstCollection = fhrdEdRepMstCollection;
    }

    @XmlTransient
    public Collection<FhrdEdRepMst> getFhrdEdRepMstCollection1() {
        return fhrdEdRepMstCollection1;
    }

    public void setFhrdEdRepMstCollection1(Collection<FhrdEdRepMst> fhrdEdRepMstCollection1) {
        this.fhrdEdRepMstCollection1 = fhrdEdRepMstCollection1;
    }

    @XmlTransient
    public Collection<FtasAbsentees> getFtasAbsenteesCollection() {
        return ftasAbsenteesCollection;
    }

    public void setFtasAbsenteesCollection(Collection<FtasAbsentees> ftasAbsenteesCollection) {
        this.ftasAbsenteesCollection = ftasAbsenteesCollection;
    }

    @XmlTransient
    public Collection<FtasAbsentees> getFtasAbsenteesCollection1() {
        return ftasAbsenteesCollection1;
    }

    public void setFtasAbsenteesCollection1(Collection<FtasAbsentees> ftasAbsenteesCollection1) {
        this.ftasAbsenteesCollection1 = ftasAbsenteesCollection1;
    }

    @XmlTransient
    public Collection<FtasAbsentees> getFtasAbsenteesCollection2() {
        return ftasAbsenteesCollection2;
    }

    public void setFtasAbsenteesCollection2(Collection<FtasAbsentees> ftasAbsenteesCollection2) {
        this.ftasAbsenteesCollection2 = ftasAbsenteesCollection2;
    }

    @XmlTransient
    public Collection<FtasAbsentees> getFtasAbsenteesCollection3() {
        return ftasAbsenteesCollection3;
    }

    public void setFtasAbsenteesCollection3(Collection<FtasAbsentees> ftasAbsenteesCollection3) {
        this.ftasAbsenteesCollection3 = ftasAbsenteesCollection3;
    }

    @XmlTransient
    public Collection<FtasAbsentees> getFtasAbsenteesCollection4() {
        return ftasAbsenteesCollection4;
    }

    public void setFtasAbsenteesCollection4(Collection<FtasAbsentees> ftasAbsenteesCollection4) {
        this.ftasAbsenteesCollection4 = ftasAbsenteesCollection4;
    }

    @XmlTransient
    public Collection<FtasTadaConvMst> getFtasTadaConvMstCollection() {
        return ftasTadaConvMstCollection;
    }

    public void setFtasTadaConvMstCollection(Collection<FtasTadaConvMst> ftasTadaConvMstCollection) {
        this.ftasTadaConvMstCollection = ftasTadaConvMstCollection;
    }

    @XmlTransient
    public Collection<FtasTadaConvMst> getFtasTadaConvMstCollection1() {
        return ftasTadaConvMstCollection1;
    }

    public void setFtasTadaConvMstCollection1(Collection<FtasTadaConvMst> ftasTadaConvMstCollection1) {
        this.ftasTadaConvMstCollection1 = ftasTadaConvMstCollection1;
    }

    @XmlTransient
    public Collection<FhrdUniversitymst> getFhrdUniversitymstCollection() {
        return fhrdUniversitymstCollection;
    }

    public void setFhrdUniversitymstCollection(Collection<FhrdUniversitymst> fhrdUniversitymstCollection) {
        this.fhrdUniversitymstCollection = fhrdUniversitymstCollection;
    }

    @XmlTransient
    public Collection<FhrdUniversitymst> getFhrdUniversitymstCollection1() {
        return fhrdUniversitymstCollection1;
    }

    public void setFhrdUniversitymstCollection1(Collection<FhrdUniversitymst> fhrdUniversitymstCollection1) {
        this.fhrdUniversitymstCollection1 = fhrdUniversitymstCollection1;
    }

    @XmlTransient
    public Collection<FtasCategoryMst> getFtasCategoryMstCollection() {
        return ftasCategoryMstCollection;
    }

    public void setFtasCategoryMstCollection(Collection<FtasCategoryMst> ftasCategoryMstCollection) {
        this.ftasCategoryMstCollection = ftasCategoryMstCollection;
    }

    @XmlTransient
    public Collection<FtasCategoryMst> getFtasCategoryMstCollection1() {
        return ftasCategoryMstCollection1;
    }

    public void setFtasCategoryMstCollection1(Collection<FtasCategoryMst> ftasCategoryMstCollection1) {
        this.ftasCategoryMstCollection1 = ftasCategoryMstCollection1;
    }

    @XmlTransient
    public Collection<SystMenumst> getSystMenumstCollection() {
        return systMenumstCollection;
    }

    public void setSystMenumstCollection(Collection<SystMenumst> systMenumstCollection) {
        this.systMenumstCollection = systMenumstCollection;
    }

    @XmlTransient
    public Collection<SystMenumst> getSystMenumstCollection1() {
        return systMenumstCollection1;
    }

    public void setSystMenumstCollection1(Collection<SystMenumst> systMenumstCollection1) {
        this.systMenumstCollection1 = systMenumstCollection1;
    }

    @XmlTransient
    public Collection<FtasTadaGradeMst> getFtasTadaGradeMstCollection() {
        return ftasTadaGradeMstCollection;
    }

    public void setFtasTadaGradeMstCollection(Collection<FtasTadaGradeMst> ftasTadaGradeMstCollection) {
        this.ftasTadaGradeMstCollection = ftasTadaGradeMstCollection;
    }

    @XmlTransient
    public Collection<FtasTadaGradeMst> getFtasTadaGradeMstCollection1() {
        return ftasTadaGradeMstCollection1;
    }

    public void setFtasTadaGradeMstCollection1(Collection<FtasTadaGradeMst> ftasTadaGradeMstCollection1) {
        this.ftasTadaGradeMstCollection1 = ftasTadaGradeMstCollection1;
    }

    @XmlTransient
    public Collection<FhrdQualification> getFhrdQualificationCollection() {
        return fhrdQualificationCollection;
    }

    public void setFhrdQualificationCollection(Collection<FhrdQualification> fhrdQualificationCollection) {
        this.fhrdQualificationCollection = fhrdQualificationCollection;
    }

    @XmlTransient
    public Collection<FhrdQualification> getFhrdQualificationCollection1() {
        return fhrdQualificationCollection1;
    }

    public void setFhrdQualificationCollection1(Collection<FhrdQualification> fhrdQualificationCollection1) {
        this.fhrdQualificationCollection1 = fhrdQualificationCollection1;
    }

    @XmlTransient
    public Collection<FhrdQualification> getFhrdQualificationCollection2() {
        return fhrdQualificationCollection2;
    }

    public void setFhrdQualificationCollection2(Collection<FhrdQualification> fhrdQualificationCollection2) {
        this.fhrdQualificationCollection2 = fhrdQualificationCollection2;
    }

    @XmlTransient
    public Collection<FtasTadaCashDednDtl> getFtasTadaCashDednDtlCollection() {
        return ftasTadaCashDednDtlCollection;
    }

    public void setFtasTadaCashDednDtlCollection(Collection<FtasTadaCashDednDtl> ftasTadaCashDednDtlCollection) {
        this.ftasTadaCashDednDtlCollection = ftasTadaCashDednDtlCollection;
    }

    @XmlTransient
    public Collection<FtasTadaCashDednDtl> getFtasTadaCashDednDtlCollection1() {
        return ftasTadaCashDednDtlCollection1;
    }

    public void setFtasTadaCashDednDtlCollection1(Collection<FtasTadaCashDednDtl> ftasTadaCashDednDtlCollection1) {
        this.ftasTadaCashDednDtlCollection1 = ftasTadaCashDednDtlCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpeventRelieve> getFhrdEmpeventRelieveCollection() {
        return fhrdEmpeventRelieveCollection;
    }

    public void setFhrdEmpeventRelieveCollection(Collection<FhrdEmpeventRelieve> fhrdEmpeventRelieveCollection) {
        this.fhrdEmpeventRelieveCollection = fhrdEmpeventRelieveCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpeventRelieve> getFhrdEmpeventRelieveCollection1() {
        return fhrdEmpeventRelieveCollection1;
    }

    public void setFhrdEmpeventRelieveCollection1(Collection<FhrdEmpeventRelieve> fhrdEmpeventRelieveCollection1) {
        this.fhrdEmpeventRelieveCollection1 = fhrdEmpeventRelieveCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpeventRelieve> getFhrdEmpeventRelieveCollection2() {
        return fhrdEmpeventRelieveCollection2;
    }

    public void setFhrdEmpeventRelieveCollection2(Collection<FhrdEmpeventRelieve> fhrdEmpeventRelieveCollection2) {
        this.fhrdEmpeventRelieveCollection2 = fhrdEmpeventRelieveCollection2;
    }

    @XmlTransient
    public Collection<FhrdGradedtl> getFhrdGradedtlCollection() {
        return fhrdGradedtlCollection;
    }

    public void setFhrdGradedtlCollection(Collection<FhrdGradedtl> fhrdGradedtlCollection) {
        this.fhrdGradedtlCollection = fhrdGradedtlCollection;
    }

    @XmlTransient
    public Collection<FhrdGradedtl> getFhrdGradedtlCollection1() {
        return fhrdGradedtlCollection1;
    }

    public void setFhrdGradedtlCollection1(Collection<FhrdGradedtl> fhrdGradedtlCollection1) {
        this.fhrdGradedtlCollection1 = fhrdGradedtlCollection1;
    }

    @XmlTransient
    public Collection<FcadAddrDtl> getFcadAddrDtlCollection() {
        return fcadAddrDtlCollection;
    }

    public void setFcadAddrDtlCollection(Collection<FcadAddrDtl> fcadAddrDtlCollection) {
        this.fcadAddrDtlCollection = fcadAddrDtlCollection;
    }

    @XmlTransient
    public Collection<FcadAddrDtl> getFcadAddrDtlCollection1() {
        return fcadAddrDtlCollection1;
    }

    public void setFcadAddrDtlCollection1(Collection<FcadAddrDtl> fcadAddrDtlCollection1) {
        this.fcadAddrDtlCollection1 = fcadAddrDtlCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpCategoryMst> getFhrdEmpCategoryMstCollection() {
        return fhrdEmpCategoryMstCollection;
    }

    public void setFhrdEmpCategoryMstCollection(Collection<FhrdEmpCategoryMst> fhrdEmpCategoryMstCollection) {
        this.fhrdEmpCategoryMstCollection = fhrdEmpCategoryMstCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpCategoryMst> getFhrdEmpCategoryMstCollection1() {
        return fhrdEmpCategoryMstCollection1;
    }

    public void setFhrdEmpCategoryMstCollection1(Collection<FhrdEmpCategoryMst> fhrdEmpCategoryMstCollection1) {
        this.fhrdEmpCategoryMstCollection1 = fhrdEmpCategoryMstCollection1;
    }

    @XmlTransient
    public Collection<FtasAbsenteesDtl> getFtasAbsenteesDtlCollection() {
        return ftasAbsenteesDtlCollection;
    }

    public void setFtasAbsenteesDtlCollection(Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection) {
        this.ftasAbsenteesDtlCollection = ftasAbsenteesDtlCollection;
    }

    @XmlTransient
    public Collection<FtasAbsenteesDtl> getFtasAbsenteesDtlCollection1() {
        return ftasAbsenteesDtlCollection1;
    }

    public void setFtasAbsenteesDtlCollection1(Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection1) {
        this.ftasAbsenteesDtlCollection1 = ftasAbsenteesDtlCollection1;
    }

    @XmlTransient
    public Collection<FtasAbsenteesDtl> getFtasAbsenteesDtlCollection2() {
        return ftasAbsenteesDtlCollection2;
    }

    public void setFtasAbsenteesDtlCollection2(Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection2) {
        this.ftasAbsenteesDtlCollection2 = ftasAbsenteesDtlCollection2;
    }

    @XmlTransient
    public Collection<FhrdEdRepDtl> getFhrdEdRepDtlCollection() {
        return fhrdEdRepDtlCollection;
    }

    public void setFhrdEdRepDtlCollection(Collection<FhrdEdRepDtl> fhrdEdRepDtlCollection) {
        this.fhrdEdRepDtlCollection = fhrdEdRepDtlCollection;
    }

    @XmlTransient
    public Collection<FhrdEdRepDtl> getFhrdEdRepDtlCollection1() {
        return fhrdEdRepDtlCollection1;
    }

    public void setFhrdEdRepDtlCollection1(Collection<FhrdEdRepDtl> fhrdEdRepDtlCollection1) {
        this.fhrdEdRepDtlCollection1 = fhrdEdRepDtlCollection1;
    }

    @XmlTransient
    public Collection<SystOrgUnitTypeMst> getSystOrgUnitTypeMstCollection() {
        return systOrgUnitTypeMstCollection;
    }

    public void setSystOrgUnitTypeMstCollection(Collection<SystOrgUnitTypeMst> systOrgUnitTypeMstCollection) {
        this.systOrgUnitTypeMstCollection = systOrgUnitTypeMstCollection;
    }

    @XmlTransient
    public Collection<SystOrgUnitTypeMst> getSystOrgUnitTypeMstCollection1() {
        return systOrgUnitTypeMstCollection1;
    }

    public void setSystOrgUnitTypeMstCollection1(Collection<SystOrgUnitTypeMst> systOrgUnitTypeMstCollection1) {
        this.systOrgUnitTypeMstCollection1 = systOrgUnitTypeMstCollection1;
    }

    @XmlTransient
    public Collection<FhrdPaytransearndedn> getFhrdPaytransearndednCollection() {
        return fhrdPaytransearndednCollection;
    }

    public void setFhrdPaytransearndednCollection(Collection<FhrdPaytransearndedn> fhrdPaytransearndednCollection) {
        this.fhrdPaytransearndednCollection = fhrdPaytransearndednCollection;
    }

    @XmlTransient
    public Collection<FhrdPaytransearndedn> getFhrdPaytransearndednCollection1() {
        return fhrdPaytransearndednCollection1;
    }

    public void setFhrdPaytransearndednCollection1(Collection<FhrdPaytransearndedn> fhrdPaytransearndednCollection1) {
        this.fhrdPaytransearndednCollection1 = fhrdPaytransearndednCollection1;
    }

    @XmlTransient
    public Collection<FhrdPaytransearndedn> getFhrdPaytransearndednCollection2() {
        return fhrdPaytransearndednCollection2;
    }

    public void setFhrdPaytransearndednCollection2(Collection<FhrdPaytransearndedn> fhrdPaytransearndednCollection2) {
        this.fhrdPaytransearndednCollection2 = fhrdPaytransearndednCollection2;
    }

    @XmlTransient
    public Collection<FtasTrainingMst> getFtasTrainingMstCollection() {
        return ftasTrainingMstCollection;
    }

    public void setFtasTrainingMstCollection(Collection<FtasTrainingMst> ftasTrainingMstCollection) {
        this.ftasTrainingMstCollection = ftasTrainingMstCollection;
    }

    @XmlTransient
    public Collection<FtasTrainingMst> getFtasTrainingMstCollection1() {
        return ftasTrainingMstCollection1;
    }

    public void setFtasTrainingMstCollection1(Collection<FtasTrainingMst> ftasTrainingMstCollection1) {
        this.ftasTrainingMstCollection1 = ftasTrainingMstCollection1;
    }

    @XmlTransient
    public Collection<FtasTrainingMst> getFtasTrainingMstCollection2() {
        return ftasTrainingMstCollection2;
    }

    public void setFtasTrainingMstCollection2(Collection<FtasTrainingMst> ftasTrainingMstCollection2) {
        this.ftasTrainingMstCollection2 = ftasTrainingMstCollection2;
    }

    @XmlTransient
    public Collection<FhrdEarndedn> getFhrdEarndednCollection() {
        return fhrdEarndednCollection;
    }

    public void setFhrdEarndednCollection(Collection<FhrdEarndedn> fhrdEarndednCollection) {
        this.fhrdEarndednCollection = fhrdEarndednCollection;
    }

    @XmlTransient
    public Collection<FhrdEarndedn> getFhrdEarndednCollection1() {
        return fhrdEarndednCollection1;
    }

    public void setFhrdEarndednCollection1(Collection<FhrdEarndedn> fhrdEarndednCollection1) {
        this.fhrdEarndednCollection1 = fhrdEarndednCollection1;
    }

    @XmlTransient
    public Collection<FcadAddrJobStageTitle> getFcadAddrJobStageTitleCollection() {
        return fcadAddrJobStageTitleCollection;
    }

    public void setFcadAddrJobStageTitleCollection(Collection<FcadAddrJobStageTitle> fcadAddrJobStageTitleCollection) {
        this.fcadAddrJobStageTitleCollection = fcadAddrJobStageTitleCollection;
    }

    @XmlTransient
    public Collection<FcadAddrJobStageTitle> getFcadAddrJobStageTitleCollection1() {
        return fcadAddrJobStageTitleCollection1;
    }

    public void setFcadAddrJobStageTitleCollection1(Collection<FcadAddrJobStageTitle> fcadAddrJobStageTitleCollection1) {
        this.fcadAddrJobStageTitleCollection1 = fcadAddrJobStageTitleCollection1;
    }

    @XmlTransient
    public Collection<FhrdRuleGroupRelation> getFhrdRuleGroupRelationCollection() {
        return fhrdRuleGroupRelationCollection;
    }

    public void setFhrdRuleGroupRelationCollection(Collection<FhrdRuleGroupRelation> fhrdRuleGroupRelationCollection) {
        this.fhrdRuleGroupRelationCollection = fhrdRuleGroupRelationCollection;
    }

    @XmlTransient
    public Collection<FhrdRuleGroupRelation> getFhrdRuleGroupRelationCollection1() {
        return fhrdRuleGroupRelationCollection1;
    }

    public void setFhrdRuleGroupRelationCollection1(Collection<FhrdRuleGroupRelation> fhrdRuleGroupRelationCollection1) {
        this.fhrdRuleGroupRelationCollection1 = fhrdRuleGroupRelationCollection1;
    }

    @XmlTransient
    public Collection<FhrdServicemst> getFhrdServicemstCollection() {
        return fhrdServicemstCollection;
    }

    public void setFhrdServicemstCollection(Collection<FhrdServicemst> fhrdServicemstCollection) {
        this.fhrdServicemstCollection = fhrdServicemstCollection;
    }

    @XmlTransient
    public Collection<FhrdServicemst> getFhrdServicemstCollection1() {
        return fhrdServicemstCollection1;
    }

    public void setFhrdServicemstCollection1(Collection<FhrdServicemst> fhrdServicemstCollection1) {
        this.fhrdServicemstCollection1 = fhrdServicemstCollection1;
    }

    @XmlTransient
    public Collection<FhrdDegreedtl> getFhrdDegreedtlCollection() {
        return fhrdDegreedtlCollection;
    }

    public void setFhrdDegreedtlCollection(Collection<FhrdDegreedtl> fhrdDegreedtlCollection) {
        this.fhrdDegreedtlCollection = fhrdDegreedtlCollection;
    }

    @XmlTransient
    public Collection<FhrdDegreedtl> getFhrdDegreedtlCollection1() {
        return fhrdDegreedtlCollection1;
    }

    public void setFhrdDegreedtlCollection1(Collection<FhrdDegreedtl> fhrdDegreedtlCollection1) {
        this.fhrdDegreedtlCollection1 = fhrdDegreedtlCollection1;
    }

    @XmlTransient
    public Collection<FhrdRuleApplGroupMst> getFhrdRuleApplGroupMstCollection() {
        return fhrdRuleApplGroupMstCollection;
    }

    public void setFhrdRuleApplGroupMstCollection(Collection<FhrdRuleApplGroupMst> fhrdRuleApplGroupMstCollection) {
        this.fhrdRuleApplGroupMstCollection = fhrdRuleApplGroupMstCollection;
    }

    @XmlTransient
    public Collection<FhrdRuleApplGroupMst> getFhrdRuleApplGroupMstCollection1() {
        return fhrdRuleApplGroupMstCollection1;
    }

    public void setFhrdRuleApplGroupMstCollection1(Collection<FhrdRuleApplGroupMst> fhrdRuleApplGroupMstCollection1) {
        this.fhrdRuleApplGroupMstCollection1 = fhrdRuleApplGroupMstCollection1;
    }

    @XmlTransient
    public Collection<FhrdPaytransleaveDtl> getFhrdPaytransleaveDtlCollection() {
        return fhrdPaytransleaveDtlCollection;
    }

    public void setFhrdPaytransleaveDtlCollection(Collection<FhrdPaytransleaveDtl> fhrdPaytransleaveDtlCollection) {
        this.fhrdPaytransleaveDtlCollection = fhrdPaytransleaveDtlCollection;
    }

    @XmlTransient
    public Collection<FhrdPaytransleaveDtl> getFhrdPaytransleaveDtlCollection1() {
        return fhrdPaytransleaveDtlCollection1;
    }

    public void setFhrdPaytransleaveDtlCollection1(Collection<FhrdPaytransleaveDtl> fhrdPaytransleaveDtlCollection1) {
        this.fhrdPaytransleaveDtlCollection1 = fhrdPaytransleaveDtlCollection1;
    }

    @XmlTransient
    public Collection<FhrdPaytransleaveDtl> getFhrdPaytransleaveDtlCollection2() {
        return fhrdPaytransleaveDtlCollection2;
    }

    public void setFhrdPaytransleaveDtlCollection2(Collection<FhrdPaytransleaveDtl> fhrdPaytransleaveDtlCollection2) {
        this.fhrdPaytransleaveDtlCollection2 = fhrdPaytransleaveDtlCollection2;
    }

    @XmlTransient
    public Collection<BmwpFundAgencyMst> getBmwpFundAgencyMstCollection() {
        return bmwpFundAgencyMstCollection;
    }

    public void setBmwpFundAgencyMstCollection(Collection<BmwpFundAgencyMst> bmwpFundAgencyMstCollection) {
        this.bmwpFundAgencyMstCollection = bmwpFundAgencyMstCollection;
    }

    @XmlTransient
    public Collection<BmwpFundAgencyMst> getBmwpFundAgencyMstCollection1() {
        return bmwpFundAgencyMstCollection1;
    }

    public void setBmwpFundAgencyMstCollection1(Collection<BmwpFundAgencyMst> bmwpFundAgencyMstCollection1) {
        this.bmwpFundAgencyMstCollection1 = bmwpFundAgencyMstCollection1;
    }

    @XmlTransient
    public Collection<FtasDaily> getFtasDailyCollection() {
        return ftasDailyCollection;
    }

    public void setFtasDailyCollection(Collection<FtasDaily> ftasDailyCollection) {
        this.ftasDailyCollection = ftasDailyCollection;
    }

    @XmlTransient
    public Collection<FtasDaily> getFtasDailyCollection1() {
        return ftasDailyCollection1;
    }

    public void setFtasDailyCollection1(Collection<FtasDaily> ftasDailyCollection1) {
        this.ftasDailyCollection1 = ftasDailyCollection1;
    }

    @XmlTransient
    public Collection<FtasDaily> getFtasDailyCollection2() {
        return ftasDailyCollection2;
    }

    public void setFtasDailyCollection2(Collection<FtasDaily> ftasDailyCollection2) {
        this.ftasDailyCollection2 = ftasDailyCollection2;
    }

    @XmlTransient
    public Collection<FhrdEmpleavebal> getFhrdEmpleavebalCollection() {
        return fhrdEmpleavebalCollection;
    }

    public void setFhrdEmpleavebalCollection(Collection<FhrdEmpleavebal> fhrdEmpleavebalCollection) {
        this.fhrdEmpleavebalCollection = fhrdEmpleavebalCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpleavebal> getFhrdEmpleavebalCollection1() {
        return fhrdEmpleavebalCollection1;
    }

    public void setFhrdEmpleavebalCollection1(Collection<FhrdEmpleavebal> fhrdEmpleavebalCollection1) {
        this.fhrdEmpleavebalCollection1 = fhrdEmpleavebalCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpleavebal> getFhrdEmpleavebalCollection2() {
        return fhrdEmpleavebalCollection2;
    }

    public void setFhrdEmpleavebalCollection2(Collection<FhrdEmpleavebal> fhrdEmpleavebalCollection2) {
        this.fhrdEmpleavebalCollection2 = fhrdEmpleavebalCollection2;
    }

    @XmlTransient
    public Collection<BmwpPartyTypeMst> getBmwpPartyTypeMstCollection() {
        return bmwpPartyTypeMstCollection;
    }

    public void setBmwpPartyTypeMstCollection(Collection<BmwpPartyTypeMst> bmwpPartyTypeMstCollection) {
        this.bmwpPartyTypeMstCollection = bmwpPartyTypeMstCollection;
    }

    @XmlTransient
    public Collection<BmwpPartyTypeMst> getBmwpPartyTypeMstCollection1() {
        return bmwpPartyTypeMstCollection1;
    }

    public void setBmwpPartyTypeMstCollection1(Collection<BmwpPartyTypeMst> bmwpPartyTypeMstCollection1) {
        this.bmwpPartyTypeMstCollection1 = bmwpPartyTypeMstCollection1;
    }

    @XmlTransient
    public Collection<FhrdReligionMst> getFhrdReligionMstCollection() {
        return fhrdReligionMstCollection;
    }

    public void setFhrdReligionMstCollection(Collection<FhrdReligionMst> fhrdReligionMstCollection) {
        this.fhrdReligionMstCollection = fhrdReligionMstCollection;
    }

    @XmlTransient
    public Collection<FhrdReligionMst> getFhrdReligionMstCollection1() {
        return fhrdReligionMstCollection1;
    }

    public void setFhrdReligionMstCollection1(Collection<FhrdReligionMst> fhrdReligionMstCollection1) {
        this.fhrdReligionMstCollection1 = fhrdReligionMstCollection1;
    }

    @XmlTransient
    public Collection<FhrdPaytransMonvar> getFhrdPaytransMonvarCollection() {
        return fhrdPaytransMonvarCollection;
    }

    public void setFhrdPaytransMonvarCollection(Collection<FhrdPaytransMonvar> fhrdPaytransMonvarCollection) {
        this.fhrdPaytransMonvarCollection = fhrdPaytransMonvarCollection;
    }

    @XmlTransient
    public Collection<FhrdPaytransMonvar> getFhrdPaytransMonvarCollection1() {
        return fhrdPaytransMonvarCollection1;
    }

    public void setFhrdPaytransMonvarCollection1(Collection<FhrdPaytransMonvar> fhrdPaytransMonvarCollection1) {
        this.fhrdPaytransMonvarCollection1 = fhrdPaytransMonvarCollection1;
    }

    @XmlTransient
    public Collection<FhrdPaytransMonvar> getFhrdPaytransMonvarCollection2() {
        return fhrdPaytransMonvarCollection2;
    }

    public void setFhrdPaytransMonvarCollection2(Collection<FhrdPaytransMonvar> fhrdPaytransMonvarCollection2) {
        this.fhrdPaytransMonvarCollection2 = fhrdPaytransMonvarCollection2;
    }

    @XmlTransient
    public Collection<FcadPersDtl> getFcadPersDtlCollection() {
        return fcadPersDtlCollection;
    }

    public void setFcadPersDtlCollection(Collection<FcadPersDtl> fcadPersDtlCollection) {
        this.fcadPersDtlCollection = fcadPersDtlCollection;
    }

    @XmlTransient
    public Collection<FcadPersDtl> getFcadPersDtlCollection1() {
        return fcadPersDtlCollection1;
    }

    public void setFcadPersDtlCollection1(Collection<FcadPersDtl> fcadPersDtlCollection1) {
        this.fcadPersDtlCollection1 = fcadPersDtlCollection1;
    }

    @XmlTransient
    public Collection<FtasTadaGradeCityLink> getFtasTadaGradeCityLinkCollection() {
        return ftasTadaGradeCityLinkCollection;
    }

    public void setFtasTadaGradeCityLinkCollection(Collection<FtasTadaGradeCityLink> ftasTadaGradeCityLinkCollection) {
        this.ftasTadaGradeCityLinkCollection = ftasTadaGradeCityLinkCollection;
    }

    @XmlTransient
    public Collection<FtasTadaGradeCityLink> getFtasTadaGradeCityLinkCollection1() {
        return ftasTadaGradeCityLinkCollection1;
    }

    public void setFtasTadaGradeCityLinkCollection1(Collection<FtasTadaGradeCityLink> ftasTadaGradeCityLinkCollection1) {
        this.ftasTadaGradeCityLinkCollection1 = ftasTadaGradeCityLinkCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpleaverule> getFhrdEmpleaveruleCollection() {
        return fhrdEmpleaveruleCollection;
    }

    public void setFhrdEmpleaveruleCollection(Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection) {
        this.fhrdEmpleaveruleCollection = fhrdEmpleaveruleCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpleaverule> getFhrdEmpleaveruleCollection1() {
        return fhrdEmpleaveruleCollection1;
    }

    public void setFhrdEmpleaveruleCollection1(Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection1) {
        this.fhrdEmpleaveruleCollection1 = fhrdEmpleaveruleCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpleaverule> getFhrdEmpleaveruleCollection2() {
        return fhrdEmpleaveruleCollection2;
    }

    public void setFhrdEmpleaveruleCollection2(Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection2) {
        this.fhrdEmpleaveruleCollection2 = fhrdEmpleaveruleCollection2;
    }

    @XmlTransient
    public Collection<BmwpActivityBudgetLink> getBmwpActivityBudgetLinkCollection() {
        return bmwpActivityBudgetLinkCollection;
    }

    public void setBmwpActivityBudgetLinkCollection(Collection<BmwpActivityBudgetLink> bmwpActivityBudgetLinkCollection) {
        this.bmwpActivityBudgetLinkCollection = bmwpActivityBudgetLinkCollection;
    }

    @XmlTransient
    public Collection<BmwpActivityBudgetLink> getBmwpActivityBudgetLinkCollection1() {
        return bmwpActivityBudgetLinkCollection1;
    }

    public void setBmwpActivityBudgetLinkCollection1(Collection<BmwpActivityBudgetLink> bmwpActivityBudgetLinkCollection1) {
        this.bmwpActivityBudgetLinkCollection1 = bmwpActivityBudgetLinkCollection1;
    }

    @XmlTransient
    public Collection<FcadAddrJobMst> getFcadAddrJobMstCollection() {
        return fcadAddrJobMstCollection;
    }

    public void setFcadAddrJobMstCollection(Collection<FcadAddrJobMst> fcadAddrJobMstCollection) {
        this.fcadAddrJobMstCollection = fcadAddrJobMstCollection;
    }

    @XmlTransient
    public Collection<FcadAddrJobMst> getFcadAddrJobMstCollection1() {
        return fcadAddrJobMstCollection1;
    }

    public void setFcadAddrJobMstCollection1(Collection<FcadAddrJobMst> fcadAddrJobMstCollection1) {
        this.fcadAddrJobMstCollection1 = fcadAddrJobMstCollection1;
    }

    @XmlTransient
    public Collection<FtasTadaFareDtl> getFtasTadaFareDtlCollection() {
        return ftasTadaFareDtlCollection;
    }

    public void setFtasTadaFareDtlCollection(Collection<FtasTadaFareDtl> ftasTadaFareDtlCollection) {
        this.ftasTadaFareDtlCollection = ftasTadaFareDtlCollection;
    }

    @XmlTransient
    public Collection<FtasTadaFareDtl> getFtasTadaFareDtlCollection1() {
        return ftasTadaFareDtlCollection1;
    }

    public void setFtasTadaFareDtlCollection1(Collection<FtasTadaFareDtl> ftasTadaFareDtlCollection1) {
        this.ftasTadaFareDtlCollection1 = ftasTadaFareDtlCollection1;
    }

    @XmlTransient
    public Collection<FtasLockDtl> getFtasLockDtlCollection() {
        return ftasLockDtlCollection;
    }

    public void setFtasLockDtlCollection(Collection<FtasLockDtl> ftasLockDtlCollection) {
        this.ftasLockDtlCollection = ftasLockDtlCollection;
    }

    @XmlTransient
    public Collection<FtasLockDtl> getFtasLockDtlCollection1() {
        return ftasLockDtlCollection1;
    }

    public void setFtasLockDtlCollection1(Collection<FtasLockDtl> ftasLockDtlCollection1) {
        this.ftasLockDtlCollection1 = ftasLockDtlCollection1;
    }

    @XmlTransient
    public Collection<FtasBulkAttTemp> getFtasBulkAttTempCollection() {
        return ftasBulkAttTempCollection;
    }

    public void setFtasBulkAttTempCollection(Collection<FtasBulkAttTemp> ftasBulkAttTempCollection) {
        this.ftasBulkAttTempCollection = ftasBulkAttTempCollection;
    }

    @XmlTransient
    public Collection<FhrdRuleGroupCategory> getFhrdRuleGroupCategoryCollection() {
        return fhrdRuleGroupCategoryCollection;
    }

    public void setFhrdRuleGroupCategoryCollection(Collection<FhrdRuleGroupCategory> fhrdRuleGroupCategoryCollection) {
        this.fhrdRuleGroupCategoryCollection = fhrdRuleGroupCategoryCollection;
    }

    @XmlTransient
    public Collection<FhrdRuleGroupCategory> getFhrdRuleGroupCategoryCollection1() {
        return fhrdRuleGroupCategoryCollection1;
    }

    public void setFhrdRuleGroupCategoryCollection1(Collection<FhrdRuleGroupCategory> fhrdRuleGroupCategoryCollection1) {
        this.fhrdRuleGroupCategoryCollection1 = fhrdRuleGroupCategoryCollection1;
    }

    @XmlTransient
    public Collection<FhrdDegreeLevelMst> getFhrdDegreeLevelMstCollection() {
        return fhrdDegreeLevelMstCollection;
    }

    public void setFhrdDegreeLevelMstCollection(Collection<FhrdDegreeLevelMst> fhrdDegreeLevelMstCollection) {
        this.fhrdDegreeLevelMstCollection = fhrdDegreeLevelMstCollection;
    }

    @XmlTransient
    public Collection<FhrdDegreeLevelMst> getFhrdDegreeLevelMstCollection1() {
        return fhrdDegreeLevelMstCollection1;
    }

    public void setFhrdDegreeLevelMstCollection1(Collection<FhrdDegreeLevelMst> fhrdDegreeLevelMstCollection1) {
        this.fhrdDegreeLevelMstCollection1 = fhrdDegreeLevelMstCollection1;
    }

    @XmlTransient
    public Collection<FtasTrainingScheduleDtl> getFtasTrainingScheduleDtlCollection() {
        return ftasTrainingScheduleDtlCollection;
    }

    public void setFtasTrainingScheduleDtlCollection(Collection<FtasTrainingScheduleDtl> ftasTrainingScheduleDtlCollection) {
        this.ftasTrainingScheduleDtlCollection = ftasTrainingScheduleDtlCollection;
    }

    @XmlTransient
    public Collection<FtasTrainingScheduleDtl> getFtasTrainingScheduleDtlCollection1() {
        return ftasTrainingScheduleDtlCollection1;
    }

    public void setFtasTrainingScheduleDtlCollection1(Collection<FtasTrainingScheduleDtl> ftasTrainingScheduleDtlCollection1) {
        this.ftasTrainingScheduleDtlCollection1 = ftasTrainingScheduleDtlCollection1;
    }

    @XmlTransient
    public Collection<FhrdEarndednmst> getFhrdEarndednmstCollection() {
        return fhrdEarndednmstCollection;
    }

    public void setFhrdEarndednmstCollection(Collection<FhrdEarndednmst> fhrdEarndednmstCollection) {
        this.fhrdEarndednmstCollection = fhrdEarndednmstCollection;
    }

    @XmlTransient
    public Collection<FhrdEarndednmst> getFhrdEarndednmstCollection1() {
        return fhrdEarndednmstCollection1;
    }

    public void setFhrdEarndednmstCollection1(Collection<FhrdEarndednmst> fhrdEarndednmstCollection1) {
        this.fhrdEarndednmstCollection1 = fhrdEarndednmstCollection1;
    }

    @XmlTransient
    public Collection<FtasTourScheduleDtl> getFtasTourScheduleDtlCollection() {
        return ftasTourScheduleDtlCollection;
    }

    public void setFtasTourScheduleDtlCollection(Collection<FtasTourScheduleDtl> ftasTourScheduleDtlCollection) {
        this.ftasTourScheduleDtlCollection = ftasTourScheduleDtlCollection;
    }

    @XmlTransient
    public Collection<FtasTourScheduleDtl> getFtasTourScheduleDtlCollection1() {
        return ftasTourScheduleDtlCollection1;
    }

    public void setFtasTourScheduleDtlCollection1(Collection<FtasTourScheduleDtl> ftasTourScheduleDtlCollection1) {
        this.ftasTourScheduleDtlCollection1 = ftasTourScheduleDtlCollection1;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetDtl> getBmwpApActivityBudgetDtlCollection() {
        return bmwpApActivityBudgetDtlCollection;
    }

    public void setBmwpApActivityBudgetDtlCollection(Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection) {
        this.bmwpApActivityBudgetDtlCollection = bmwpApActivityBudgetDtlCollection;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetDtl> getBmwpApActivityBudgetDtlCollection1() {
        return bmwpApActivityBudgetDtlCollection1;
    }

    public void setBmwpApActivityBudgetDtlCollection1(Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection1) {
        this.bmwpApActivityBudgetDtlCollection1 = bmwpApActivityBudgetDtlCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpEventDtl> getFhrdEmpEventDtlCollection() {
        return fhrdEmpEventDtlCollection;
    }

    public void setFhrdEmpEventDtlCollection(Collection<FhrdEmpEventDtl> fhrdEmpEventDtlCollection) {
        this.fhrdEmpEventDtlCollection = fhrdEmpEventDtlCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpEventDtl> getFhrdEmpEventDtlCollection1() {
        return fhrdEmpEventDtlCollection1;
    }

    public void setFhrdEmpEventDtlCollection1(Collection<FhrdEmpEventDtl> fhrdEmpEventDtlCollection1) {
        this.fhrdEmpEventDtlCollection1 = fhrdEmpEventDtlCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpEventDtl> getFhrdEmpEventDtlCollection2() {
        return fhrdEmpEventDtlCollection2;
    }

    public void setFhrdEmpEventDtlCollection2(Collection<FhrdEmpEventDtl> fhrdEmpEventDtlCollection2) {
        this.fhrdEmpEventDtlCollection2 = fhrdEmpEventDtlCollection2;
    }

    @XmlTransient
    public Collection<FhrdDegreeStreamMst> getFhrdDegreeStreamMstCollection() {
        return fhrdDegreeStreamMstCollection;
    }

    public void setFhrdDegreeStreamMstCollection(Collection<FhrdDegreeStreamMst> fhrdDegreeStreamMstCollection) {
        this.fhrdDegreeStreamMstCollection = fhrdDegreeStreamMstCollection;
    }

    @XmlTransient
    public Collection<FhrdDegreeStreamMst> getFhrdDegreeStreamMstCollection1() {
        return fhrdDegreeStreamMstCollection1;
    }

    public void setFhrdDegreeStreamMstCollection1(Collection<FhrdDegreeStreamMst> fhrdDegreeStreamMstCollection1) {
        this.fhrdDegreeStreamMstCollection1 = fhrdDegreeStreamMstCollection1;
    }

    @XmlTransient
    public Collection<FhrdPaytransleaveMst> getFhrdPaytransleaveMstCollection() {
        return fhrdPaytransleaveMstCollection;
    }

    public void setFhrdPaytransleaveMstCollection(Collection<FhrdPaytransleaveMst> fhrdPaytransleaveMstCollection) {
        this.fhrdPaytransleaveMstCollection = fhrdPaytransleaveMstCollection;
    }

    @XmlTransient
    public Collection<FhrdPaytransleaveMst> getFhrdPaytransleaveMstCollection1() {
        return fhrdPaytransleaveMstCollection1;
    }

    public void setFhrdPaytransleaveMstCollection1(Collection<FhrdPaytransleaveMst> fhrdPaytransleaveMstCollection1) {
        this.fhrdPaytransleaveMstCollection1 = fhrdPaytransleaveMstCollection1;
    }

    @XmlTransient
    public Collection<FhrdPaytransleaveMst> getFhrdPaytransleaveMstCollection2() {
        return fhrdPaytransleaveMstCollection2;
    }

    public void setFhrdPaytransleaveMstCollection2(Collection<FhrdPaytransleaveMst> fhrdPaytransleaveMstCollection2) {
        this.fhrdPaytransleaveMstCollection2 = fhrdPaytransleaveMstCollection2;
    }

    @XmlTransient
    public Collection<FcadAddrGrpMst> getFcadAddrGrpMstCollection() {
        return fcadAddrGrpMstCollection;
    }

    public void setFcadAddrGrpMstCollection(Collection<FcadAddrGrpMst> fcadAddrGrpMstCollection) {
        this.fcadAddrGrpMstCollection = fcadAddrGrpMstCollection;
    }

    @XmlTransient
    public Collection<FcadAddrGrpMst> getFcadAddrGrpMstCollection1() {
        return fcadAddrGrpMstCollection1;
    }

    public void setFcadAddrGrpMstCollection1(Collection<FcadAddrGrpMst> fcadAddrGrpMstCollection1) {
        this.fcadAddrGrpMstCollection1 = fcadAddrGrpMstCollection1;
    }

    public SystUserTypeMst getSystUserTypeMst() {
        return systUserTypeMst;
    }

    public void setSystUserTypeMst(SystUserTypeMst systUserTypeMst) {
        this.systUserTypeMst = systUserTypeMst;
    }

    public SystOrgUnitMst getPostingOumUnitSrno() {
        return postingOumUnitSrno;
    }

    public void setPostingOumUnitSrno(SystOrgUnitMst postingOumUnitSrno) {
        this.postingOumUnitSrno = postingOumUnitSrno;
    }

    public FhrdPortfolioMst getPmUniqueSrno() {
        return pmUniqueSrno;
    }

    public void setPmUniqueSrno(FhrdPortfolioMst pmUniqueSrno) {
        this.pmUniqueSrno = pmUniqueSrno;
    }

    @XmlTransient
    public Collection<FhrdEmpmst> getFhrdEmpmstCollection() {
        return fhrdEmpmstCollection;
    }

    public void setFhrdEmpmstCollection(Collection<FhrdEmpmst> fhrdEmpmstCollection) {
        this.fhrdEmpmstCollection = fhrdEmpmstCollection;
    }

    public FhrdEmpmst getReportingUserId() {
        return reportingUserId;
    }

    public void setReportingUserId(FhrdEmpmst reportingUserId) {
        this.reportingUserId = reportingUserId;
    }

    public FhrdDesigmst getDsgmSrgKey() {
        return dsgmSrgKey;
    }

    public void setDsgmSrgKey(FhrdDesigmst dsgmSrgKey) {
        this.dsgmSrgKey = dsgmSrgKey;
    }

    public FhrdCasteCategoryMst getCcmSrgKey() {
        return ccmSrgKey;
    }

    public void setCcmSrgKey(FhrdCasteCategoryMst ccmSrgKey) {
        this.ccmSrgKey = ccmSrgKey;
    }

    public FcadAddrMst getAddressno() {
        return addressno;
    }

    public void setAddressno(FcadAddrMst addressno) {
        this.addressno = addressno;
    }

    @XmlTransient
    public Collection<BmwpFundSrcMst> getBmwpFundSrcMstCollection() {
        return bmwpFundSrcMstCollection;
    }

    public void setBmwpFundSrcMstCollection(Collection<BmwpFundSrcMst> bmwpFundSrcMstCollection) {
        this.bmwpFundSrcMstCollection = bmwpFundSrcMstCollection;
    }

    @XmlTransient
    public Collection<BmwpFundSrcMst> getBmwpFundSrcMstCollection1() {
        return bmwpFundSrcMstCollection1;
    }

    public void setBmwpFundSrcMstCollection1(Collection<BmwpFundSrcMst> bmwpFundSrcMstCollection1) {
        this.bmwpFundSrcMstCollection1 = bmwpFundSrcMstCollection1;
    }

    @XmlTransient
    public Collection<FtasHolidayDtl> getFtasHolidayDtlCollection() {
        return ftasHolidayDtlCollection;
    }

    public void setFtasHolidayDtlCollection(Collection<FtasHolidayDtl> ftasHolidayDtlCollection) {
        this.ftasHolidayDtlCollection = ftasHolidayDtlCollection;
    }

    @XmlTransient
    public Collection<FtasHolidayDtl> getFtasHolidayDtlCollection1() {
        return ftasHolidayDtlCollection1;
    }

    public void setFtasHolidayDtlCollection1(Collection<FtasHolidayDtl> ftasHolidayDtlCollection1) {
        this.ftasHolidayDtlCollection1 = ftasHolidayDtlCollection1;
    }

    @XmlTransient
    public Collection<BmwpBudgetMst> getBmwpBudgetMstCollection() {
        return bmwpBudgetMstCollection;
    }

    public void setBmwpBudgetMstCollection(Collection<BmwpBudgetMst> bmwpBudgetMstCollection) {
        this.bmwpBudgetMstCollection = bmwpBudgetMstCollection;
    }

    @XmlTransient
    public Collection<BmwpBudgetMst> getBmwpBudgetMstCollection1() {
        return bmwpBudgetMstCollection1;
    }

    public void setBmwpBudgetMstCollection1(Collection<BmwpBudgetMst> bmwpBudgetMstCollection1) {
        this.bmwpBudgetMstCollection1 = bmwpBudgetMstCollection1;
    }

    @XmlTransient
    public Collection<FhrdPortfolioMst> getFhrdPortfolioMstCollection() {
        return fhrdPortfolioMstCollection;
    }

    public void setFhrdPortfolioMstCollection(Collection<FhrdPortfolioMst> fhrdPortfolioMstCollection) {
        this.fhrdPortfolioMstCollection = fhrdPortfolioMstCollection;
    }

    @XmlTransient
    public Collection<FhrdPortfolioMst> getFhrdPortfolioMstCollection1() {
        return fhrdPortfolioMstCollection1;
    }

    public void setFhrdPortfolioMstCollection1(Collection<FhrdPortfolioMst> fhrdPortfolioMstCollection1) {
        this.fhrdPortfolioMstCollection1 = fhrdPortfolioMstCollection1;
    }

    @XmlTransient
    public Collection<BmwpAgencyTypeMst> getBmwpAgencyTypeMstCollection() {
        return bmwpAgencyTypeMstCollection;
    }

    public void setBmwpAgencyTypeMstCollection(Collection<BmwpAgencyTypeMst> bmwpAgencyTypeMstCollection) {
        this.bmwpAgencyTypeMstCollection = bmwpAgencyTypeMstCollection;
    }

    @XmlTransient
    public Collection<BmwpAgencyTypeMst> getBmwpAgencyTypeMstCollection1() {
        return bmwpAgencyTypeMstCollection1;
    }

    public void setBmwpAgencyTypeMstCollection1(Collection<BmwpAgencyTypeMst> bmwpAgencyTypeMstCollection1) {
        this.bmwpAgencyTypeMstCollection1 = bmwpAgencyTypeMstCollection1;
    }

    @XmlTransient
    public Collection<BmwpAgenSecMst> getBmwpAgenSecMstCollection() {
        return bmwpAgenSecMstCollection;
    }

    public void setBmwpAgenSecMstCollection(Collection<BmwpAgenSecMst> bmwpAgenSecMstCollection) {
        this.bmwpAgenSecMstCollection = bmwpAgenSecMstCollection;
    }

    @XmlTransient
    public Collection<BmwpAgenSecMst> getBmwpAgenSecMstCollection1() {
        return bmwpAgenSecMstCollection1;
    }

    public void setBmwpAgenSecMstCollection1(Collection<BmwpAgenSecMst> bmwpAgenSecMstCollection1) {
        this.bmwpAgenSecMstCollection1 = bmwpAgenSecMstCollection1;
    }

    @XmlTransient
    public Collection<FtasTadaFacilityDtl> getFtasTadaFacilityDtlCollection() {
        return ftasTadaFacilityDtlCollection;
    }

    public void setFtasTadaFacilityDtlCollection(Collection<FtasTadaFacilityDtl> ftasTadaFacilityDtlCollection) {
        this.ftasTadaFacilityDtlCollection = ftasTadaFacilityDtlCollection;
    }

    @XmlTransient
    public Collection<FtasTadaFacilityDtl> getFtasTadaFacilityDtlCollection1() {
        return ftasTadaFacilityDtlCollection1;
    }

    public void setFtasTadaFacilityDtlCollection1(Collection<FtasTadaFacilityDtl> ftasTadaFacilityDtlCollection1) {
        this.ftasTadaFacilityDtlCollection1 = ftasTadaFacilityDtlCollection1;
    }

    @XmlTransient
    public Collection<FhrdEventMst> getFhrdEventMstCollection() {
        return fhrdEventMstCollection;
    }

    public void setFhrdEventMstCollection(Collection<FhrdEventMst> fhrdEventMstCollection) {
        this.fhrdEventMstCollection = fhrdEventMstCollection;
    }

    @XmlTransient
    public Collection<FhrdEventMst> getFhrdEventMstCollection1() {
        return fhrdEventMstCollection1;
    }

    public void setFhrdEventMstCollection1(Collection<FhrdEventMst> fhrdEventMstCollection1) {
        this.fhrdEventMstCollection1 = fhrdEventMstCollection1;
    }

    @XmlTransient
    public Collection<FhrdEventMst> getFhrdEventMstCollection2() {
        return fhrdEventMstCollection2;
    }

    public void setFhrdEventMstCollection2(Collection<FhrdEventMst> fhrdEventMstCollection2) {
        this.fhrdEventMstCollection2 = fhrdEventMstCollection2;
    }

    @XmlTransient
    public Collection<FhrdEventMst> getFhrdEventMstCollection3() {
        return fhrdEventMstCollection3;
    }

    public void setFhrdEventMstCollection3(Collection<FhrdEventMst> fhrdEventMstCollection3) {
        this.fhrdEventMstCollection3 = fhrdEventMstCollection3;
    }

    @XmlTransient
    public Collection<FhrdDegreemst> getFhrdDegreemstCollection() {
        return fhrdDegreemstCollection;
    }

    public void setFhrdDegreemstCollection(Collection<FhrdDegreemst> fhrdDegreemstCollection) {
        this.fhrdDegreemstCollection = fhrdDegreemstCollection;
    }

    @XmlTransient
    public Collection<FhrdDegreemst> getFhrdDegreemstCollection1() {
        return fhrdDegreemstCollection1;
    }

    public void setFhrdDegreemstCollection1(Collection<FhrdDegreemst> fhrdDegreemstCollection1) {
        this.fhrdDegreemstCollection1 = fhrdDegreemstCollection1;
    }

    @XmlTransient
    public Collection<FtasHolidayMst> getFtasHolidayMstCollection() {
        return ftasHolidayMstCollection;
    }

    public void setFtasHolidayMstCollection(Collection<FtasHolidayMst> ftasHolidayMstCollection) {
        this.ftasHolidayMstCollection = ftasHolidayMstCollection;
    }

    @XmlTransient
    public Collection<FtasHolidayMst> getFtasHolidayMstCollection1() {
        return ftasHolidayMstCollection1;
    }

    public void setFtasHolidayMstCollection1(Collection<FtasHolidayMst> ftasHolidayMstCollection1) {
        this.ftasHolidayMstCollection1 = ftasHolidayMstCollection1;
    }

    @XmlTransient
    public Collection<BmwpApActivityMst> getBmwpApActivityMstCollection() {
        return bmwpApActivityMstCollection;
    }

    public void setBmwpApActivityMstCollection(Collection<BmwpApActivityMst> bmwpApActivityMstCollection) {
        this.bmwpApActivityMstCollection = bmwpApActivityMstCollection;
    }

    @XmlTransient
    public Collection<BmwpApActivityMst> getBmwpApActivityMstCollection1() {
        return bmwpApActivityMstCollection1;
    }

    public void setBmwpApActivityMstCollection1(Collection<BmwpApActivityMst> bmwpApActivityMstCollection1) {
        this.bmwpApActivityMstCollection1 = bmwpApActivityMstCollection1;
    }

    @XmlTransient
    public Collection<BmwpActivityMst> getBmwpActivityMstCollection() {
        return bmwpActivityMstCollection;
    }

    public void setBmwpActivityMstCollection(Collection<BmwpActivityMst> bmwpActivityMstCollection) {
        this.bmwpActivityMstCollection = bmwpActivityMstCollection;
    }

    @XmlTransient
    public Collection<BmwpActivityMst> getBmwpActivityMstCollection1() {
        return bmwpActivityMstCollection1;
    }

    public void setBmwpActivityMstCollection1(Collection<BmwpActivityMst> bmwpActivityMstCollection1) {
        this.bmwpActivityMstCollection1 = bmwpActivityMstCollection1;
    }

    @XmlTransient
    public Collection<BmwpPartyTypeDtl> getBmwpPartyTypeDtlCollection() {
        return bmwpPartyTypeDtlCollection;
    }

    public void setBmwpPartyTypeDtlCollection(Collection<BmwpPartyTypeDtl> bmwpPartyTypeDtlCollection) {
        this.bmwpPartyTypeDtlCollection = bmwpPartyTypeDtlCollection;
    }

    @XmlTransient
    public Collection<BmwpPartyTypeDtl> getBmwpPartyTypeDtlCollection1() {
        return bmwpPartyTypeDtlCollection1;
    }

    public void setBmwpPartyTypeDtlCollection1(Collection<BmwpPartyTypeDtl> bmwpPartyTypeDtlCollection1) {
        this.bmwpPartyTypeDtlCollection1 = bmwpPartyTypeDtlCollection1;
    }

    @XmlTransient
    public Collection<FhrdPaytrans> getFhrdPaytransCollection() {
        return fhrdPaytransCollection;
    }

    public void setFhrdPaytransCollection(Collection<FhrdPaytrans> fhrdPaytransCollection) {
        this.fhrdPaytransCollection = fhrdPaytransCollection;
    }

    @XmlTransient
    public Collection<FhrdPaytrans> getFhrdPaytransCollection1() {
        return fhrdPaytransCollection1;
    }

    public void setFhrdPaytransCollection1(Collection<FhrdPaytrans> fhrdPaytransCollection1) {
        this.fhrdPaytransCollection1 = fhrdPaytransCollection1;
    }

    @XmlTransient
    public Collection<FhrdPaytrans> getFhrdPaytransCollection2() {
        return fhrdPaytransCollection2;
    }

    public void setFhrdPaytransCollection2(Collection<FhrdPaytrans> fhrdPaytransCollection2) {
        this.fhrdPaytransCollection2 = fhrdPaytransCollection2;
    }

    @XmlTransient
    public Collection<FhrdEmpEncash> getFhrdEmpEncashCollection() {
        return fhrdEmpEncashCollection;
    }

    public void setFhrdEmpEncashCollection(Collection<FhrdEmpEncash> fhrdEmpEncashCollection) {
        this.fhrdEmpEncashCollection = fhrdEmpEncashCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpEncash> getFhrdEmpEncashCollection1() {
        return fhrdEmpEncashCollection1;
    }

    public void setFhrdEmpEncashCollection1(Collection<FhrdEmpEncash> fhrdEmpEncashCollection1) {
        this.fhrdEmpEncashCollection1 = fhrdEmpEncashCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpEncash> getFhrdEmpEncashCollection2() {
        return fhrdEmpEncashCollection2;
    }

    public void setFhrdEmpEncashCollection2(Collection<FhrdEmpEncash> fhrdEmpEncashCollection2) {
        this.fhrdEmpEncashCollection2 = fhrdEmpEncashCollection2;
    }

    @XmlTransient
    public Collection<FhrdEmpEncash> getFhrdEmpEncashCollection3() {
        return fhrdEmpEncashCollection3;
    }

    public void setFhrdEmpEncashCollection3(Collection<FhrdEmpEncash> fhrdEmpEncashCollection3) {
        this.fhrdEmpEncashCollection3 = fhrdEmpEncashCollection3;
    }

    @XmlTransient
    public Collection<FhrdEmpEncash> getFhrdEmpEncashCollection4() {
        return fhrdEmpEncashCollection4;
    }

    public void setFhrdEmpEncashCollection4(Collection<FhrdEmpEncash> fhrdEmpEncashCollection4) {
        this.fhrdEmpEncashCollection4 = fhrdEmpEncashCollection4;
    }

    @XmlTransient
    public Collection<BmwpApBudgetMst> getBmwpApBudgetMstCollection() {
        return bmwpApBudgetMstCollection;
    }

    public void setBmwpApBudgetMstCollection(Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection) {
        this.bmwpApBudgetMstCollection = bmwpApBudgetMstCollection;
    }

    @XmlTransient
    public Collection<BmwpApBudgetMst> getBmwpApBudgetMstCollection1() {
        return bmwpApBudgetMstCollection1;
    }

    public void setBmwpApBudgetMstCollection1(Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection1) {
        this.bmwpApBudgetMstCollection1 = bmwpApBudgetMstCollection1;
    }

    @XmlTransient
    public Collection<BmwpWorkplan> getBmwpWorkplanCollection() {
        return bmwpWorkplanCollection;
    }

    public void setBmwpWorkplanCollection(Collection<BmwpWorkplan> bmwpWorkplanCollection) {
        this.bmwpWorkplanCollection = bmwpWorkplanCollection;
    }

    @XmlTransient
    public Collection<BmwpWorkplan> getBmwpWorkplanCollection1() {
        return bmwpWorkplanCollection1;
    }

    public void setBmwpWorkplanCollection1(Collection<BmwpWorkplan> bmwpWorkplanCollection1) {
        this.bmwpWorkplanCollection1 = bmwpWorkplanCollection1;
    }

    @XmlTransient
    public Collection<FhrdPaytransSummary> getFhrdPaytransSummaryCollection() {
        return fhrdPaytransSummaryCollection;
    }

    public void setFhrdPaytransSummaryCollection(Collection<FhrdPaytransSummary> fhrdPaytransSummaryCollection) {
        this.fhrdPaytransSummaryCollection = fhrdPaytransSummaryCollection;
    }

    @XmlTransient
    public Collection<FtasTrainingOrgUnitDtl> getFtasTrainingOrgUnitDtlCollection() {
        return ftasTrainingOrgUnitDtlCollection;
    }

    public void setFtasTrainingOrgUnitDtlCollection(Collection<FtasTrainingOrgUnitDtl> ftasTrainingOrgUnitDtlCollection) {
        this.ftasTrainingOrgUnitDtlCollection = ftasTrainingOrgUnitDtlCollection;
    }

    @XmlTransient
    public Collection<FtasTrainingOrgUnitDtl> getFtasTrainingOrgUnitDtlCollection1() {
        return ftasTrainingOrgUnitDtlCollection1;
    }

    public void setFtasTrainingOrgUnitDtlCollection1(Collection<FtasTrainingOrgUnitDtl> ftasTrainingOrgUnitDtlCollection1) {
        this.ftasTrainingOrgUnitDtlCollection1 = ftasTrainingOrgUnitDtlCollection1;
    }

    @XmlTransient
    public Collection<FhrdQualificationLevel> getFhrdQualificationLevelCollection() {
        return fhrdQualificationLevelCollection;
    }

    public void setFhrdQualificationLevelCollection(Collection<FhrdQualificationLevel> fhrdQualificationLevelCollection) {
        this.fhrdQualificationLevelCollection = fhrdQualificationLevelCollection;
    }

    @XmlTransient
    public Collection<FhrdQualificationLevel> getFhrdQualificationLevelCollection1() {
        return fhrdQualificationLevelCollection1;
    }

    public void setFhrdQualificationLevelCollection1(Collection<FhrdQualificationLevel> fhrdQualificationLevelCollection1) {
        this.fhrdQualificationLevelCollection1 = fhrdQualificationLevelCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpeventPromotion> getFhrdEmpeventPromotionCollection() {
        return fhrdEmpeventPromotionCollection;
    }

    public void setFhrdEmpeventPromotionCollection(Collection<FhrdEmpeventPromotion> fhrdEmpeventPromotionCollection) {
        this.fhrdEmpeventPromotionCollection = fhrdEmpeventPromotionCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpeventPromotion> getFhrdEmpeventPromotionCollection1() {
        return fhrdEmpeventPromotionCollection1;
    }

    public void setFhrdEmpeventPromotionCollection1(Collection<FhrdEmpeventPromotion> fhrdEmpeventPromotionCollection1) {
        this.fhrdEmpeventPromotionCollection1 = fhrdEmpeventPromotionCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpeventPromotion> getFhrdEmpeventPromotionCollection2() {
        return fhrdEmpeventPromotionCollection2;
    }

    public void setFhrdEmpeventPromotionCollection2(Collection<FhrdEmpeventPromotion> fhrdEmpeventPromotionCollection2) {
        this.fhrdEmpeventPromotionCollection2 = fhrdEmpeventPromotionCollection2;
    }

    @XmlTransient
    public Collection<FhrdGrademst> getFhrdGrademstCollection() {
        return fhrdGrademstCollection;
    }

    public void setFhrdGrademstCollection(Collection<FhrdGrademst> fhrdGrademstCollection) {
        this.fhrdGrademstCollection = fhrdGrademstCollection;
    }

    @XmlTransient
    public Collection<FhrdGrademst> getFhrdGrademstCollection1() {
        return fhrdGrademstCollection1;
    }

    public void setFhrdGrademstCollection1(Collection<FhrdGrademst> fhrdGrademstCollection1) {
        this.fhrdGrademstCollection1 = fhrdGrademstCollection1;
    }

    @XmlTransient
    public Collection<FhrdEarndednConfig> getFhrdEarndednConfigCollection() {
        return fhrdEarndednConfigCollection;
    }

    public void setFhrdEarndednConfigCollection(Collection<FhrdEarndednConfig> fhrdEarndednConfigCollection) {
        this.fhrdEarndednConfigCollection = fhrdEarndednConfigCollection;
    }

    @XmlTransient
    public Collection<FhrdEarndednConfig> getFhrdEarndednConfigCollection1() {
        return fhrdEarndednConfigCollection1;
    }

    public void setFhrdEarndednConfigCollection1(Collection<FhrdEarndednConfig> fhrdEarndednConfigCollection1) {
        this.fhrdEarndednConfigCollection1 = fhrdEarndednConfigCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpeventTransfer> getFhrdEmpeventTransferCollection() {
        return fhrdEmpeventTransferCollection;
    }

    public void setFhrdEmpeventTransferCollection(Collection<FhrdEmpeventTransfer> fhrdEmpeventTransferCollection) {
        this.fhrdEmpeventTransferCollection = fhrdEmpeventTransferCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpeventTransfer> getFhrdEmpeventTransferCollection1() {
        return fhrdEmpeventTransferCollection1;
    }

    public void setFhrdEmpeventTransferCollection1(Collection<FhrdEmpeventTransfer> fhrdEmpeventTransferCollection1) {
        this.fhrdEmpeventTransferCollection1 = fhrdEmpeventTransferCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpeventTransfer> getFhrdEmpeventTransferCollection2() {
        return fhrdEmpeventTransferCollection2;
    }

    public void setFhrdEmpeventTransferCollection2(Collection<FhrdEmpeventTransfer> fhrdEmpeventTransferCollection2) {
        this.fhrdEmpeventTransferCollection2 = fhrdEmpeventTransferCollection2;
    }

    @XmlTransient
    public Collection<FhrdEmpeventTransfer> getFhrdEmpeventTransferCollection3() {
        return fhrdEmpeventTransferCollection3;
    }

    public void setFhrdEmpeventTransferCollection3(Collection<FhrdEmpeventTransfer> fhrdEmpeventTransferCollection3) {
        this.fhrdEmpeventTransferCollection3 = fhrdEmpeventTransferCollection3;
    }

    @XmlTransient
    public Collection<FhrdEmpeventTransfer> getFhrdEmpeventTransferCollection4() {
        return fhrdEmpeventTransferCollection4;
    }

    public void setFhrdEmpeventTransferCollection4(Collection<FhrdEmpeventTransfer> fhrdEmpeventTransferCollection4) {
        this.fhrdEmpeventTransferCollection4 = fhrdEmpeventTransferCollection4;
    }

    @XmlTransient
    public Collection<BmwpActivityBudgetDtl> getBmwpActivityBudgetDtlCollection() {
        return bmwpActivityBudgetDtlCollection;
    }

    public void setBmwpActivityBudgetDtlCollection(Collection<BmwpActivityBudgetDtl> bmwpActivityBudgetDtlCollection) {
        this.bmwpActivityBudgetDtlCollection = bmwpActivityBudgetDtlCollection;
    }

    @XmlTransient
    public Collection<BmwpActivityBudgetDtl> getBmwpActivityBudgetDtlCollection1() {
        return bmwpActivityBudgetDtlCollection1;
    }

    public void setBmwpActivityBudgetDtlCollection1(Collection<BmwpActivityBudgetDtl> bmwpActivityBudgetDtlCollection1) {
        this.bmwpActivityBudgetDtlCollection1 = bmwpActivityBudgetDtlCollection1;
    }

    @XmlTransient
    public Collection<FtasTrainingAnnDetail> getFtasTrainingAnnDetailCollection() {
        return ftasTrainingAnnDetailCollection;
    }

    public void setFtasTrainingAnnDetailCollection(Collection<FtasTrainingAnnDetail> ftasTrainingAnnDetailCollection) {
        this.ftasTrainingAnnDetailCollection = ftasTrainingAnnDetailCollection;
    }

    @XmlTransient
    public Collection<FtasTrainingAnnDetail> getFtasTrainingAnnDetailCollection1() {
        return ftasTrainingAnnDetailCollection1;
    }

    public void setFtasTrainingAnnDetailCollection1(Collection<FtasTrainingAnnDetail> ftasTrainingAnnDetailCollection1) {
        this.ftasTrainingAnnDetailCollection1 = ftasTrainingAnnDetailCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpContractMst> getFhrdEmpContractMstCollection() {
        return fhrdEmpContractMstCollection;
    }

    public void setFhrdEmpContractMstCollection(Collection<FhrdEmpContractMst> fhrdEmpContractMstCollection) {
        this.fhrdEmpContractMstCollection = fhrdEmpContractMstCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpContractMst> getFhrdEmpContractMstCollection1() {
        return fhrdEmpContractMstCollection1;
    }

    public void setFhrdEmpContractMstCollection1(Collection<FhrdEmpContractMst> fhrdEmpContractMstCollection1) {
        this.fhrdEmpContractMstCollection1 = fhrdEmpContractMstCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpContractMst> getFhrdEmpContractMstCollection2() {
        return fhrdEmpContractMstCollection2;
    }

    public void setFhrdEmpContractMstCollection2(Collection<FhrdEmpContractMst> fhrdEmpContractMstCollection2) {
        this.fhrdEmpContractMstCollection2 = fhrdEmpContractMstCollection2;
    }

    @XmlTransient
    public Collection<FtasTadaCashallowRuleMst> getFtasTadaCashallowRuleMstCollection() {
        return ftasTadaCashallowRuleMstCollection;
    }

    public void setFtasTadaCashallowRuleMstCollection(Collection<FtasTadaCashallowRuleMst> ftasTadaCashallowRuleMstCollection) {
        this.ftasTadaCashallowRuleMstCollection = ftasTadaCashallowRuleMstCollection;
    }

    @XmlTransient
    public Collection<FtasTadaCashallowRuleMst> getFtasTadaCashallowRuleMstCollection1() {
        return ftasTadaCashallowRuleMstCollection1;
    }

    public void setFtasTadaCashallowRuleMstCollection1(Collection<FtasTadaCashallowRuleMst> ftasTadaCashallowRuleMstCollection1) {
        this.ftasTadaCashallowRuleMstCollection1 = ftasTadaCashallowRuleMstCollection1;
    }

    @XmlTransient
    public Collection<FcadAddrMst> getFcadAddrMstCollection() {
        return fcadAddrMstCollection;
    }

    public void setFcadAddrMstCollection(Collection<FcadAddrMst> fcadAddrMstCollection) {
        this.fcadAddrMstCollection = fcadAddrMstCollection;
    }

    @XmlTransient
    public Collection<FcadAddrMst> getFcadAddrMstCollection1() {
        return fcadAddrMstCollection1;
    }

    public void setFcadAddrMstCollection1(Collection<FcadAddrMst> fcadAddrMstCollection1) {
        this.fcadAddrMstCollection1 = fcadAddrMstCollection1;
    }

    @XmlTransient
    public Collection<BmwpProjectMst> getBmwpProjectMstCollection() {
        return bmwpProjectMstCollection;
    }

    public void setBmwpProjectMstCollection(Collection<BmwpProjectMst> bmwpProjectMstCollection) {
        this.bmwpProjectMstCollection = bmwpProjectMstCollection;
    }

    @XmlTransient
    public Collection<BmwpProjectMst> getBmwpProjectMstCollection1() {
        return bmwpProjectMstCollection1;
    }

    public void setBmwpProjectMstCollection1(Collection<BmwpProjectMst> bmwpProjectMstCollection1) {
        this.bmwpProjectMstCollection1 = bmwpProjectMstCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpearndedn> getFhrdEmpearndednCollection() {
        return fhrdEmpearndednCollection;
    }

    public void setFhrdEmpearndednCollection(Collection<FhrdEmpearndedn> fhrdEmpearndednCollection) {
        this.fhrdEmpearndednCollection = fhrdEmpearndednCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpearndedn> getFhrdEmpearndednCollection1() {
        return fhrdEmpearndednCollection1;
    }

    public void setFhrdEmpearndednCollection1(Collection<FhrdEmpearndedn> fhrdEmpearndednCollection1) {
        this.fhrdEmpearndednCollection1 = fhrdEmpearndednCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpearndedn> getFhrdEmpearndednCollection2() {
        return fhrdEmpearndednCollection2;
    }

    public void setFhrdEmpearndednCollection2(Collection<FhrdEmpearndedn> fhrdEmpearndednCollection2) {
        this.fhrdEmpearndednCollection2 = fhrdEmpearndednCollection2;
    }

    @XmlTransient
    public Collection<FhrdLeaverule> getFhrdLeaveruleCollection() {
        return fhrdLeaveruleCollection;
    }

    public void setFhrdLeaveruleCollection(Collection<FhrdLeaverule> fhrdLeaveruleCollection) {
        this.fhrdLeaveruleCollection = fhrdLeaveruleCollection;
    }

    @XmlTransient
    public Collection<FhrdLeaverule> getFhrdLeaveruleCollection1() {
        return fhrdLeaveruleCollection1;
    }

    public void setFhrdLeaveruleCollection1(Collection<FhrdLeaverule> fhrdLeaveruleCollection1) {
        this.fhrdLeaveruleCollection1 = fhrdLeaveruleCollection1;
    }

    @XmlTransient
    public Collection<FtasTadaConvExpDtl> getFtasTadaConvExpDtlCollection() {
        return ftasTadaConvExpDtlCollection;
    }

    public void setFtasTadaConvExpDtlCollection(Collection<FtasTadaConvExpDtl> ftasTadaConvExpDtlCollection) {
        this.ftasTadaConvExpDtlCollection = ftasTadaConvExpDtlCollection;
    }

    @XmlTransient
    public Collection<FtasTadaConvExpDtl> getFtasTadaConvExpDtlCollection1() {
        return ftasTadaConvExpDtlCollection1;
    }

    public void setFtasTadaConvExpDtlCollection1(Collection<FtasTadaConvExpDtl> ftasTadaConvExpDtlCollection1) {
        this.ftasTadaConvExpDtlCollection1 = ftasTadaConvExpDtlCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpCancEncash> getFhrdEmpCancEncashCollection() {
        return fhrdEmpCancEncashCollection;
    }

    public void setFhrdEmpCancEncashCollection(Collection<FhrdEmpCancEncash> fhrdEmpCancEncashCollection) {
        this.fhrdEmpCancEncashCollection = fhrdEmpCancEncashCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpCancEncash> getFhrdEmpCancEncashCollection1() {
        return fhrdEmpCancEncashCollection1;
    }

    public void setFhrdEmpCancEncashCollection1(Collection<FhrdEmpCancEncash> fhrdEmpCancEncashCollection1) {
        this.fhrdEmpCancEncashCollection1 = fhrdEmpCancEncashCollection1;
    }

    @XmlTransient
    public Collection<FhrdEmpCancEncash> getFhrdEmpCancEncashCollection2() {
        return fhrdEmpCancEncashCollection2;
    }

    public void setFhrdEmpCancEncashCollection2(Collection<FhrdEmpCancEncash> fhrdEmpCancEncashCollection2) {
        this.fhrdEmpCancEncashCollection2 = fhrdEmpCancEncashCollection2;
    }

    @XmlTransient
    public Collection<FhrdEmpCancEncash> getFhrdEmpCancEncashCollection3() {
        return fhrdEmpCancEncashCollection3;
    }

    public void setFhrdEmpCancEncashCollection3(Collection<FhrdEmpCancEncash> fhrdEmpCancEncashCollection3) {
        this.fhrdEmpCancEncashCollection3 = fhrdEmpCancEncashCollection3;
    }

    @XmlTransient
    public Collection<FhrdEmpCancEncash> getFhrdEmpCancEncashCollection4() {
        return fhrdEmpCancEncashCollection4;
    }

    public void setFhrdEmpCancEncashCollection4(Collection<FhrdEmpCancEncash> fhrdEmpCancEncashCollection4) {
        this.fhrdEmpCancEncashCollection4 = fhrdEmpCancEncashCollection4;
    }

    @XmlTransient
    public Collection<FtasCancelabs> getFtasCancelabsCollection() {
        return ftasCancelabsCollection;
    }

    public void setFtasCancelabsCollection(Collection<FtasCancelabs> ftasCancelabsCollection) {
        this.ftasCancelabsCollection = ftasCancelabsCollection;
    }

    @XmlTransient
    public Collection<FtasCancelabs> getFtasCancelabsCollection1() {
        return ftasCancelabsCollection1;
    }

    public void setFtasCancelabsCollection1(Collection<FtasCancelabs> ftasCancelabsCollection1) {
        this.ftasCancelabsCollection1 = ftasCancelabsCollection1;
    }

    @XmlTransient
    public Collection<FtasCancelabs> getFtasCancelabsCollection2() {
        return ftasCancelabsCollection2;
    }

    public void setFtasCancelabsCollection2(Collection<FtasCancelabs> ftasCancelabsCollection2) {
        this.ftasCancelabsCollection2 = ftasCancelabsCollection2;
    }

    @XmlTransient
    public Collection<FtasCancelabs> getFtasCancelabsCollection3() {
        return ftasCancelabsCollection3;
    }

    public void setFtasCancelabsCollection3(Collection<FtasCancelabs> ftasCancelabsCollection3) {
        this.ftasCancelabsCollection3 = ftasCancelabsCollection3;
    }

    @XmlTransient
    public Collection<FtasCancelabs> getFtasCancelabsCollection4() {
        return ftasCancelabsCollection4;
    }

    public void setFtasCancelabsCollection4(Collection<FtasCancelabs> ftasCancelabsCollection4) {
        this.ftasCancelabsCollection4 = ftasCancelabsCollection4;
    }

    @XmlTransient
    public Collection<FhrdDependent> getFhrdDependentCollection() {
        return fhrdDependentCollection;
    }

    public void setFhrdDependentCollection(Collection<FhrdDependent> fhrdDependentCollection) {
        this.fhrdDependentCollection = fhrdDependentCollection;
    }

    @XmlTransient
    public Collection<FhrdDependent> getFhrdDependentCollection1() {
        return fhrdDependentCollection1;
    }

    public void setFhrdDependentCollection1(Collection<FhrdDependent> fhrdDependentCollection1) {
        this.fhrdDependentCollection1 = fhrdDependentCollection1;
    }

    @XmlTransient
    public Collection<FhrdDependent> getFhrdDependentCollection2() {
        return fhrdDependentCollection2;
    }

    public void setFhrdDependentCollection2(Collection<FhrdDependent> fhrdDependentCollection2) {
        this.fhrdDependentCollection2 = fhrdDependentCollection2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdEmpmst)) {
            return false;
        }
        FhrdEmpmst other = (FhrdEmpmst) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdEmpmst[ userId=" + userId + " ]";
    }
}
