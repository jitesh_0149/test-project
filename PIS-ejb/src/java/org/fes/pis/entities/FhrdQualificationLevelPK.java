/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author anu
 */
@Embeddable
public class FhrdQualificationLevelPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "OUM_UNIT_SRNO")
    private int oumUnitSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LEVEL_SRNO")
    private short levelSrno;

    public FhrdQualificationLevelPK() {
    }

    public FhrdQualificationLevelPK(int oumUnitSrno, short levelSrno) {
        this.oumUnitSrno = oumUnitSrno;
        this.levelSrno = levelSrno;
    }

    public int getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(int oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public short getLevelSrno() {
        return levelSrno;
    }

    public void setLevelSrno(short levelSrno) {
        this.levelSrno = levelSrno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) oumUnitSrno;
        hash += (int) levelSrno;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdQualificationLevelPK)) {
            return false;
        }
        FhrdQualificationLevelPK other = (FhrdQualificationLevelPK) object;
        if (this.oumUnitSrno != other.oumUnitSrno) {
            return false;
        }
        if (this.levelSrno != other.levelSrno) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdQualificationLevelPK[ oumUnitSrno=" + oumUnitSrno + ", levelSrno=" + levelSrno + " ]";
    }
    
}
