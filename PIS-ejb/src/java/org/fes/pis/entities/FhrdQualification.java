/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_QUALIFICATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdQualification.findAll", query = "SELECT f FROM FhrdQualification f"),
    @NamedQuery(name = "FhrdQualification.findByUserId", query = "SELECT f FROM FhrdQualification f WHERE f.fhrdQualificationPK.userId = :userId"),
    @NamedQuery(name = "FhrdQualification.findByQualSrno", query = "SELECT f FROM FhrdQualification f WHERE f.fhrdQualificationPK.qualSrno = :qualSrno"),
    @NamedQuery(name = "FhrdQualification.findByCollegeName", query = "SELECT f FROM FhrdQualification f WHERE f.collegeName = :collegeName"),
    @NamedQuery(name = "FhrdQualification.findByGrade", query = "SELECT f FROM FhrdQualification f WHERE f.grade = :grade"),
    @NamedQuery(name = "FhrdQualification.findByPercentage", query = "SELECT f FROM FhrdQualification f WHERE f.percentage = :percentage"),
    @NamedQuery(name = "FhrdQualification.findByPassMonth", query = "SELECT f FROM FhrdQualification f WHERE f.passMonth = :passMonth"),
    @NamedQuery(name = "FhrdQualification.findByPassYear", query = "SELECT f FROM FhrdQualification f WHERE f.passYear = :passYear"),
    @NamedQuery(name = "FhrdQualification.findByRemarks", query = "SELECT f FROM FhrdQualification f WHERE f.remarks = :remarks"),
    @NamedQuery(name = "FhrdQualification.findByCreatedt", query = "SELECT f FROM FhrdQualification f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdQualification.findByUpdatedt", query = "SELECT f FROM FhrdQualification f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdQualification.findByExportFlag", query = "SELECT f FROM FhrdQualification f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdQualification.findByImportFlag", query = "SELECT f FROM FhrdQualification f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdQualification.findBySystemUserid", query = "SELECT f FROM FhrdQualification f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdQualification.findByEmpno", query = "SELECT f FROM FhrdQualification f WHERE f.empno = :empno")})
public class FhrdQualification implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FhrdQualificationPK fhrdQualificationPK;
    @Size(max = 100)
    @Column(name = "COLLEGE_NAME")
    private String collegeName;
    @Size(max = 10)
    @Column(name = "GRADE")
    private String grade;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PERCENTAGE")
    private BigDecimal percentage;
    @Size(max = 3)
    @Column(name = "PASS_MONTH")
    private String passMonth;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PASS_YEAR")
    private short passYear;
    @Size(max = 255)
    @Column(name = "REMARKS")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Column(name = "EMPNO")
    private Integer empno;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UM_SRG_KEY", referencedColumnName = "UM_SRG_KEY")
    @ManyToOne
    private FhrdUniversitymst umSrgKey;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FhrdEmpmst fhrdEmpmst;
    @JoinColumn(name = "DSM_SRG_KEY", referencedColumnName = "DSM_SRG_KEY")
    @ManyToOne
    private FhrdDegreeStreamMst dsmSrgKey;
    @JoinColumn(name = "DGRM_SRG_KEY", referencedColumnName = "DGRM_SRG_KEY", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FhrdDegreemst dgrmSrgKey;
    @JoinColumns({
        @JoinColumn(name = "DGRM_SRG_KEY", referencedColumnName = "DGRM_SRG_KEY"),
        @JoinColumn(name = "SUBJECT_SRNO", referencedColumnName = "SUBJECT_SRNO")})
    @ManyToOne(optional = false)
    private FhrdDegreedtl fhrdDegreedtl;

    public FhrdQualification() {
    }

    public FhrdQualification(FhrdQualificationPK fhrdQualificationPK) {
        this.fhrdQualificationPK = fhrdQualificationPK;
    }

    public FhrdQualification(FhrdQualificationPK fhrdQualificationPK, short passYear, Date createdt) {
        this.fhrdQualificationPK = fhrdQualificationPK;
        this.passYear = passYear;
        this.createdt = createdt;
    }

    public FhrdQualification(int userId, int qualSrno) {
        this.fhrdQualificationPK = new FhrdQualificationPK(userId, qualSrno);
    }

    public FhrdQualificationPK getFhrdQualificationPK() {
        return fhrdQualificationPK;
    }

    public void setFhrdQualificationPK(FhrdQualificationPK fhrdQualificationPK) {
        this.fhrdQualificationPK = fhrdQualificationPK;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public String getPassMonth() {
        return passMonth;
    }

    public void setPassMonth(String passMonth) {
        this.passMonth = passMonth;
    }

    public short getPassYear() {
        return passYear;
    }

    public void setPassYear(short passYear) {
        this.passYear = passYear;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public Integer getEmpno() {
        return empno;
    }

    public void setEmpno(Integer empno) {
        this.empno = empno;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdUniversitymst getUmSrgKey() {
        return umSrgKey;
    }

    public void setUmSrgKey(FhrdUniversitymst umSrgKey) {
        this.umSrgKey = umSrgKey;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getFhrdEmpmst() {
        return fhrdEmpmst;
    }

    public void setFhrdEmpmst(FhrdEmpmst fhrdEmpmst) {
        this.fhrdEmpmst = fhrdEmpmst;
    }

    public FhrdDegreeStreamMst getDsmSrgKey() {
        return dsmSrgKey;
    }

    public void setDsmSrgKey(FhrdDegreeStreamMst dsmSrgKey) {
        this.dsmSrgKey = dsmSrgKey;
    }

    public FhrdDegreemst getDgrmSrgKey() {
        return dgrmSrgKey;
    }

    public void setDgrmSrgKey(FhrdDegreemst dgrmSrgKey) {
        this.dgrmSrgKey = dgrmSrgKey;
    }

    public FhrdDegreedtl getFhrdDegreedtl() {
        return fhrdDegreedtl;
    }

    public void setFhrdDegreedtl(FhrdDegreedtl fhrdDegreedtl) {
        this.fhrdDegreedtl = fhrdDegreedtl;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fhrdQualificationPK != null ? fhrdQualificationPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdQualification)) {
            return false;
        }
        FhrdQualification other = (FhrdQualification) object;
        if ((this.fhrdQualificationPK == null && other.fhrdQualificationPK != null) || (this.fhrdQualificationPK != null && !this.fhrdQualificationPK.equals(other.fhrdQualificationPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdQualification[ fhrdQualificationPK=" + fhrdQualificationPK + " ]";
    }
}
