/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "BMWP_AP_ACTIVITY_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmwpApActivityMst.findAll", query = "SELECT b FROM BmwpApActivityMst b"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamUniqueSrno", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamUniqueSrno = :apamUniqueSrno"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamSrno", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamSrno = :apamSrno"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamActivitySrno", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamActivitySrno = :apamActivitySrno"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamActivityShortName", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamActivityShortName = :apamActivityShortName"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamActivityDesc", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamActivityDesc = :apamActivityDesc"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamParentActivitySrno", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamParentActivitySrno = :apamParentActivitySrno"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamActivitySrnoLevel", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamActivitySrnoLevel = :apamActivitySrnoLevel"),
    @NamedQuery(name = "BmwpApActivityMst.findByCreatedt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.createdt = :createdt"),
    @NamedQuery(name = "BmwpApActivityMst.findByUpdatedt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.updatedt = :updatedt"),
    @NamedQuery(name = "BmwpApActivityMst.findBySystemUserid", query = "SELECT b FROM BmwpApActivityMst b WHERE b.systemUserid = :systemUserid"),
    @NamedQuery(name = "BmwpApActivityMst.findByExportFlag", query = "SELECT b FROM BmwpApActivityMst b WHERE b.exportFlag = :exportFlag"),
    @NamedQuery(name = "BmwpApActivityMst.findByImportFlag", query = "SELECT b FROM BmwpApActivityMst b WHERE b.importFlag = :importFlag"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamStartDate", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamStartDate = :apamStartDate"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamEndDate", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamEndDate = :apamEndDate"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamEndDatePossible", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamEndDatePossible = :apamEndDatePossible"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamReCreateFlag", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamReCreateFlag = :apamReCreateFlag"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamChildOfferFlag", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamChildOfferFlag = :apamChildOfferFlag"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamTransactionDate", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamTransactionDate = :apamTransactionDate"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamWorkplanAmt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamWorkplanAmt = :apamWorkplanAmt"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamWpSurrenderAmt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamWpSurrenderAmt = :apamWpSurrenderAmt"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamReAllotWpAmt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamReAllotWpAmt = :apamReAllotWpAmt"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamReviseWpAmt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamReviseWpAmt = :apamReviseWpAmt"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamCommitedAmt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamCommitedAmt = :apamCommitedAmt"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamPipelinePayAmt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamPipelinePayAmt = :apamPipelinePayAmt"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamPipeCommitedAmt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamPipeCommitedAmt = :apamPipeCommitedAmt"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamAdvancePayAmt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamAdvancePayAmt = :apamAdvancePayAmt"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamAdvanceCommitAmt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamAdvanceCommitAmt = :apamAdvanceCommitAmt"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamPipeAdvadjPayAmt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamPipeAdvadjPayAmt = :apamPipeAdvadjPayAmt"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamPipeAdvadjCommitAmt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamPipeAdvadjCommitAmt = :apamPipeAdvadjCommitAmt"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamReceiptPayAmt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamReceiptPayAmt = :apamReceiptPayAmt"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamReceiptCommitAmt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamReceiptCommitAmt = :apamReceiptCommitAmt"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamProCrExpAmt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamProCrExpAmt = :apamProCrExpAmt"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamCommContriAmt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamCommContriAmt = :apamCommContriAmt"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamExpenseAmt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamExpenseAmt = :apamExpenseAmt"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamUsableBalanceAmt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamUsableBalanceAmt = :apamUsableBalanceAmt"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamBalanceAmt", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamBalanceAmt = :apamBalanceAmt"),
    @NamedQuery(name = "BmwpApActivityMst.findByApamPmFundMonitorFlag", query = "SELECT b FROM BmwpApActivityMst b WHERE b.apamPmFundMonitorFlag = :apamPmFundMonitorFlag"),
    @NamedQuery(name = "BmwpApActivityMst.findByPmFundType", query = "SELECT b FROM BmwpApActivityMst b WHERE b.pmFundType = :pmFundType")})
public class BmwpApActivityMst implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_UNIQUE_SRNO")
    private BigDecimal apamUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_SRNO")
    private int apamSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_ACTIVITY_SRNO")
    private BigInteger apamActivitySrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "APAM_ACTIVITY_SHORT_NAME")
    private String apamActivityShortName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "APAM_ACTIVITY_DESC")
    private String apamActivityDesc;
    @Column(name = "APAM_PARENT_ACTIVITY_SRNO")
    private BigInteger apamParentActivitySrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_ACTIVITY_SRNO_LEVEL")
    private int apamActivitySrnoLevel;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date apamStartDate;
    @Column(name = "APAM_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date apamEndDate;
    @Column(name = "APAM_END_DATE_POSSIBLE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date apamEndDatePossible;
    @Size(max = 1)
    @Column(name = "APAM_RE_CREATE_FLAG")
    private String apamReCreateFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "APAM_CHILD_OFFER_FLAG")
    private String apamChildOfferFlag;
    @Column(name = "APAM_TRANSACTION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date apamTransactionDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_WORKPLAN_AMT")
    private BigDecimal apamWorkplanAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_WP_SURRENDER_AMT")
    private BigDecimal apamWpSurrenderAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_RE_ALLOT_WP_AMT")
    private BigDecimal apamReAllotWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_REVISE_WP_AMT")
    private BigDecimal apamReviseWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_COMMITED_AMT")
    private BigDecimal apamCommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_PIPELINE_PAY_AMT")
    private BigDecimal apamPipelinePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_PIPE_COMMITED_AMT")
    private BigDecimal apamPipeCommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_ADVANCE_PAY_AMT")
    private BigDecimal apamAdvancePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_ADVANCE_COMMIT_AMT")
    private BigDecimal apamAdvanceCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_PIPE_ADVADJ_PAY_AMT")
    private BigDecimal apamPipeAdvadjPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_PIPE_ADVADJ_COMMIT_AMT")
    private BigDecimal apamPipeAdvadjCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_RECEIPT_PAY_AMT")
    private BigDecimal apamReceiptPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_RECEIPT_COMMIT_AMT")
    private BigDecimal apamReceiptCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_PRO_CR_EXP_AMT")
    private BigDecimal apamProCrExpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_COMM_CONTRI_AMT")
    private BigDecimal apamCommContriAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_EXPENSE_AMT")
    private BigDecimal apamExpenseAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_USABLE_BALANCE_AMT")
    private BigDecimal apamUsableBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APAM_BALANCE_AMT")
    private BigDecimal apamBalanceAmt;
    @Size(max = 1)
    @Column(name = "APAM_PM_FUND_MONITOR_FLAG")
    private String apamPmFundMonitorFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PM_FUND_TYPE")
    private String pmFundType;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "apablApamUniqueSrno")
    private Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection;
    @OneToMany(mappedBy = "apabdApamUniqueSrno")
    private Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "APAM_PM_UNIQUE_SRNO", referencedColumnName = "PM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpProjectMst apamPmUniqueSrno;
    @JoinColumn(name = "APAM_FAM_UNIQUE_SRNO", referencedColumnName = "FAM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpFundAgencyMst apamFamUniqueSrno;
    @OneToMany(mappedBy = "apamParentUniqueSrno")
    private Collection<BmwpApActivityMst> bmwpApActivityMstCollection;
    @JoinColumn(name = "APAM_PARENT_UNIQUE_SRNO", referencedColumnName = "APAM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpApActivityMst apamParentUniqueSrno;
    @JoinColumn(name = "APAM_ATM_UNIQUE_SRNO", referencedColumnName = "ATM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpAgencyTypeMst apamAtmUniqueSrno;
    @JoinColumn(name = "APAM_APL_UNIQUE_SRNO", referencedColumnName = "APL_UNIQUE_SRNO")
    @ManyToOne(optional = false)
    private BmwpAgencyProjectLink apamAplUniqueSrno;
    @OneToMany(mappedBy = "wpApamUniqueSrno")
    private Collection<BmwpWorkplan> bmwpWorkplanCollection;

    public BmwpApActivityMst() {
    }

    public BmwpApActivityMst(BigDecimal apamUniqueSrno) {
        this.apamUniqueSrno = apamUniqueSrno;
    }

    public BmwpApActivityMst(BigDecimal apamUniqueSrno, int apamSrno, BigInteger apamActivitySrno, String apamActivityShortName, String apamActivityDesc, int apamActivitySrnoLevel, Date createdt, Date apamStartDate, String apamChildOfferFlag, BigDecimal apamWorkplanAmt, BigDecimal apamWpSurrenderAmt, BigDecimal apamReAllotWpAmt, BigDecimal apamReviseWpAmt, BigDecimal apamCommitedAmt, BigDecimal apamPipelinePayAmt, BigDecimal apamPipeCommitedAmt, BigDecimal apamAdvancePayAmt, BigDecimal apamAdvanceCommitAmt, BigDecimal apamPipeAdvadjPayAmt, BigDecimal apamPipeAdvadjCommitAmt, BigDecimal apamReceiptPayAmt, BigDecimal apamReceiptCommitAmt, BigDecimal apamProCrExpAmt, BigDecimal apamCommContriAmt, BigDecimal apamExpenseAmt, BigDecimal apamUsableBalanceAmt, BigDecimal apamBalanceAmt, String pmFundType) {
        this.apamUniqueSrno = apamUniqueSrno;
        this.apamSrno = apamSrno;
        this.apamActivitySrno = apamActivitySrno;
        this.apamActivityShortName = apamActivityShortName;
        this.apamActivityDesc = apamActivityDesc;
        this.apamActivitySrnoLevel = apamActivitySrnoLevel;
        this.createdt = createdt;
        this.apamStartDate = apamStartDate;
        this.apamChildOfferFlag = apamChildOfferFlag;
        this.apamWorkplanAmt = apamWorkplanAmt;
        this.apamWpSurrenderAmt = apamWpSurrenderAmt;
        this.apamReAllotWpAmt = apamReAllotWpAmt;
        this.apamReviseWpAmt = apamReviseWpAmt;
        this.apamCommitedAmt = apamCommitedAmt;
        this.apamPipelinePayAmt = apamPipelinePayAmt;
        this.apamPipeCommitedAmt = apamPipeCommitedAmt;
        this.apamAdvancePayAmt = apamAdvancePayAmt;
        this.apamAdvanceCommitAmt = apamAdvanceCommitAmt;
        this.apamPipeAdvadjPayAmt = apamPipeAdvadjPayAmt;
        this.apamPipeAdvadjCommitAmt = apamPipeAdvadjCommitAmt;
        this.apamReceiptPayAmt = apamReceiptPayAmt;
        this.apamReceiptCommitAmt = apamReceiptCommitAmt;
        this.apamProCrExpAmt = apamProCrExpAmt;
        this.apamCommContriAmt = apamCommContriAmt;
        this.apamExpenseAmt = apamExpenseAmt;
        this.apamUsableBalanceAmt = apamUsableBalanceAmt;
        this.apamBalanceAmt = apamBalanceAmt;
        this.pmFundType = pmFundType;
    }

    public BigDecimal getApamUniqueSrno() {
        return apamUniqueSrno;
    }

    public void setApamUniqueSrno(BigDecimal apamUniqueSrno) {
        this.apamUniqueSrno = apamUniqueSrno;
    }

    public int getApamSrno() {
        return apamSrno;
    }

    public void setApamSrno(int apamSrno) {
        this.apamSrno = apamSrno;
    }

    public BigInteger getApamActivitySrno() {
        return apamActivitySrno;
    }

    public void setApamActivitySrno(BigInteger apamActivitySrno) {
        this.apamActivitySrno = apamActivitySrno;
    }

    public String getApamActivityShortName() {
        return apamActivityShortName;
    }

    public void setApamActivityShortName(String apamActivityShortName) {
        this.apamActivityShortName = apamActivityShortName;
    }

    public String getApamActivityDesc() {
        return apamActivityDesc;
    }

    public void setApamActivityDesc(String apamActivityDesc) {
        this.apamActivityDesc = apamActivityDesc;
    }

    public BigInteger getApamParentActivitySrno() {
        return apamParentActivitySrno;
    }

    public void setApamParentActivitySrno(BigInteger apamParentActivitySrno) {
        this.apamParentActivitySrno = apamParentActivitySrno;
    }

    public int getApamActivitySrnoLevel() {
        return apamActivitySrnoLevel;
    }

    public void setApamActivitySrnoLevel(int apamActivitySrnoLevel) {
        this.apamActivitySrnoLevel = apamActivitySrnoLevel;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Date getApamStartDate() {
        return apamStartDate;
    }

    public void setApamStartDate(Date apamStartDate) {
        this.apamStartDate = apamStartDate;
    }

    public Date getApamEndDate() {
        return apamEndDate;
    }

    public void setApamEndDate(Date apamEndDate) {
        this.apamEndDate = apamEndDate;
    }

    public Date getApamEndDatePossible() {
        return apamEndDatePossible;
    }

    public void setApamEndDatePossible(Date apamEndDatePossible) {
        this.apamEndDatePossible = apamEndDatePossible;
    }

    public String getApamReCreateFlag() {
        return apamReCreateFlag;
    }

    public void setApamReCreateFlag(String apamReCreateFlag) {
        this.apamReCreateFlag = apamReCreateFlag;
    }

    public String getApamChildOfferFlag() {
        return apamChildOfferFlag;
    }

    public void setApamChildOfferFlag(String apamChildOfferFlag) {
        this.apamChildOfferFlag = apamChildOfferFlag;
    }

    public Date getApamTransactionDate() {
        return apamTransactionDate;
    }

    public void setApamTransactionDate(Date apamTransactionDate) {
        this.apamTransactionDate = apamTransactionDate;
    }

    public BigDecimal getApamWorkplanAmt() {
        return apamWorkplanAmt;
    }

    public void setApamWorkplanAmt(BigDecimal apamWorkplanAmt) {
        this.apamWorkplanAmt = apamWorkplanAmt;
    }

    public BigDecimal getApamWpSurrenderAmt() {
        return apamWpSurrenderAmt;
    }

    public void setApamWpSurrenderAmt(BigDecimal apamWpSurrenderAmt) {
        this.apamWpSurrenderAmt = apamWpSurrenderAmt;
    }

    public BigDecimal getApamReAllotWpAmt() {
        return apamReAllotWpAmt;
    }

    public void setApamReAllotWpAmt(BigDecimal apamReAllotWpAmt) {
        this.apamReAllotWpAmt = apamReAllotWpAmt;
    }

    public BigDecimal getApamReviseWpAmt() {
        return apamReviseWpAmt;
    }

    public void setApamReviseWpAmt(BigDecimal apamReviseWpAmt) {
        this.apamReviseWpAmt = apamReviseWpAmt;
    }

    public BigDecimal getApamCommitedAmt() {
        return apamCommitedAmt;
    }

    public void setApamCommitedAmt(BigDecimal apamCommitedAmt) {
        this.apamCommitedAmt = apamCommitedAmt;
    }

    public BigDecimal getApamPipelinePayAmt() {
        return apamPipelinePayAmt;
    }

    public void setApamPipelinePayAmt(BigDecimal apamPipelinePayAmt) {
        this.apamPipelinePayAmt = apamPipelinePayAmt;
    }

    public BigDecimal getApamPipeCommitedAmt() {
        return apamPipeCommitedAmt;
    }

    public void setApamPipeCommitedAmt(BigDecimal apamPipeCommitedAmt) {
        this.apamPipeCommitedAmt = apamPipeCommitedAmt;
    }

    public BigDecimal getApamAdvancePayAmt() {
        return apamAdvancePayAmt;
    }

    public void setApamAdvancePayAmt(BigDecimal apamAdvancePayAmt) {
        this.apamAdvancePayAmt = apamAdvancePayAmt;
    }

    public BigDecimal getApamAdvanceCommitAmt() {
        return apamAdvanceCommitAmt;
    }

    public void setApamAdvanceCommitAmt(BigDecimal apamAdvanceCommitAmt) {
        this.apamAdvanceCommitAmt = apamAdvanceCommitAmt;
    }

    public BigDecimal getApamPipeAdvadjPayAmt() {
        return apamPipeAdvadjPayAmt;
    }

    public void setApamPipeAdvadjPayAmt(BigDecimal apamPipeAdvadjPayAmt) {
        this.apamPipeAdvadjPayAmt = apamPipeAdvadjPayAmt;
    }

    public BigDecimal getApamPipeAdvadjCommitAmt() {
        return apamPipeAdvadjCommitAmt;
    }

    public void setApamPipeAdvadjCommitAmt(BigDecimal apamPipeAdvadjCommitAmt) {
        this.apamPipeAdvadjCommitAmt = apamPipeAdvadjCommitAmt;
    }

    public BigDecimal getApamReceiptPayAmt() {
        return apamReceiptPayAmt;
    }

    public void setApamReceiptPayAmt(BigDecimal apamReceiptPayAmt) {
        this.apamReceiptPayAmt = apamReceiptPayAmt;
    }

    public BigDecimal getApamReceiptCommitAmt() {
        return apamReceiptCommitAmt;
    }

    public void setApamReceiptCommitAmt(BigDecimal apamReceiptCommitAmt) {
        this.apamReceiptCommitAmt = apamReceiptCommitAmt;
    }

    public BigDecimal getApamProCrExpAmt() {
        return apamProCrExpAmt;
    }

    public void setApamProCrExpAmt(BigDecimal apamProCrExpAmt) {
        this.apamProCrExpAmt = apamProCrExpAmt;
    }

    public BigDecimal getApamCommContriAmt() {
        return apamCommContriAmt;
    }

    public void setApamCommContriAmt(BigDecimal apamCommContriAmt) {
        this.apamCommContriAmt = apamCommContriAmt;
    }

    public BigDecimal getApamExpenseAmt() {
        return apamExpenseAmt;
    }

    public void setApamExpenseAmt(BigDecimal apamExpenseAmt) {
        this.apamExpenseAmt = apamExpenseAmt;
    }

    public BigDecimal getApamUsableBalanceAmt() {
        return apamUsableBalanceAmt;
    }

    public void setApamUsableBalanceAmt(BigDecimal apamUsableBalanceAmt) {
        this.apamUsableBalanceAmt = apamUsableBalanceAmt;
    }

    public BigDecimal getApamBalanceAmt() {
        return apamBalanceAmt;
    }

    public void setApamBalanceAmt(BigDecimal apamBalanceAmt) {
        this.apamBalanceAmt = apamBalanceAmt;
    }

    public String getApamPmFundMonitorFlag() {
        return apamPmFundMonitorFlag;
    }

    public void setApamPmFundMonitorFlag(String apamPmFundMonitorFlag) {
        this.apamPmFundMonitorFlag = apamPmFundMonitorFlag;
    }

    public String getPmFundType() {
        return pmFundType;
    }

    public void setPmFundType(String pmFundType) {
        this.pmFundType = pmFundType;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetLink> getBmwpApActivityBudgetLinkCollection() {
        return bmwpApActivityBudgetLinkCollection;
    }

    public void setBmwpApActivityBudgetLinkCollection(Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection) {
        this.bmwpApActivityBudgetLinkCollection = bmwpApActivityBudgetLinkCollection;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetDtl> getBmwpApActivityBudgetDtlCollection() {
        return bmwpApActivityBudgetDtlCollection;
    }

    public void setBmwpApActivityBudgetDtlCollection(Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection) {
        this.bmwpApActivityBudgetDtlCollection = bmwpApActivityBudgetDtlCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public BmwpProjectMst getApamPmUniqueSrno() {
        return apamPmUniqueSrno;
    }

    public void setApamPmUniqueSrno(BmwpProjectMst apamPmUniqueSrno) {
        this.apamPmUniqueSrno = apamPmUniqueSrno;
    }

    public BmwpFundAgencyMst getApamFamUniqueSrno() {
        return apamFamUniqueSrno;
    }

    public void setApamFamUniqueSrno(BmwpFundAgencyMst apamFamUniqueSrno) {
        this.apamFamUniqueSrno = apamFamUniqueSrno;
    }

    @XmlTransient
    public Collection<BmwpApActivityMst> getBmwpApActivityMstCollection() {
        return bmwpApActivityMstCollection;
    }

    public void setBmwpApActivityMstCollection(Collection<BmwpApActivityMst> bmwpApActivityMstCollection) {
        this.bmwpApActivityMstCollection = bmwpApActivityMstCollection;
    }

    public BmwpApActivityMst getApamParentUniqueSrno() {
        return apamParentUniqueSrno;
    }

    public void setApamParentUniqueSrno(BmwpApActivityMst apamParentUniqueSrno) {
        this.apamParentUniqueSrno = apamParentUniqueSrno;
    }

    public BmwpAgencyTypeMst getApamAtmUniqueSrno() {
        return apamAtmUniqueSrno;
    }

    public void setApamAtmUniqueSrno(BmwpAgencyTypeMst apamAtmUniqueSrno) {
        this.apamAtmUniqueSrno = apamAtmUniqueSrno;
    }

    public BmwpAgencyProjectLink getApamAplUniqueSrno() {
        return apamAplUniqueSrno;
    }

    public void setApamAplUniqueSrno(BmwpAgencyProjectLink apamAplUniqueSrno) {
        this.apamAplUniqueSrno = apamAplUniqueSrno;
    }

    @XmlTransient
    public Collection<BmwpWorkplan> getBmwpWorkplanCollection() {
        return bmwpWorkplanCollection;
    }

    public void setBmwpWorkplanCollection(Collection<BmwpWorkplan> bmwpWorkplanCollection) {
        this.bmwpWorkplanCollection = bmwpWorkplanCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (apamUniqueSrno != null ? apamUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmwpApActivityMst)) {
            return false;
        }
        BmwpApActivityMst other = (BmwpApActivityMst) object;
        if ((this.apamUniqueSrno == null && other.apamUniqueSrno != null) || (this.apamUniqueSrno != null && !this.apamUniqueSrno.equals(other.apamUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.BmwpApActivityMst[ apamUniqueSrno=" + apamUniqueSrno + " ]";
    }
    
}
