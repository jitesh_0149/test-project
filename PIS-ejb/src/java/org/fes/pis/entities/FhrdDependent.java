/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_DEPENDENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdDependent.findAll", query = "SELECT f FROM FhrdDependent f"),
    @NamedQuery(name = "FhrdDependent.findByUserId", query = "SELECT f FROM FhrdDependent f WHERE f.fhrdDependentPK.userId = :userId"),
    @NamedQuery(name = "FhrdDependent.findByDepSrno", query = "SELECT f FROM FhrdDependent f WHERE f.fhrdDependentPK.depSrno = :depSrno"),
    @NamedQuery(name = "FhrdDependent.findByName", query = "SELECT f FROM FhrdDependent f WHERE f.name = :name"),
    @NamedQuery(name = "FhrdDependent.findByRelation", query = "SELECT f FROM FhrdDependent f WHERE f.relation = :relation"),
    @NamedQuery(name = "FhrdDependent.findByGender", query = "SELECT f FROM FhrdDependent f WHERE f.gender = :gender"),
    @NamedQuery(name = "FhrdDependent.findByBirthdt", query = "SELECT f FROM FhrdDependent f WHERE f.birthdt = :birthdt"),
    @NamedQuery(name = "FhrdDependent.findByOccupation", query = "SELECT f FROM FhrdDependent f WHERE f.occupation = :occupation"),
    @NamedQuery(name = "FhrdDependent.findByCreatedt", query = "SELECT f FROM FhrdDependent f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdDependent.findByUpdatedt", query = "SELECT f FROM FhrdDependent f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdDependent.findByExportFlag", query = "SELECT f FROM FhrdDependent f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdDependent.findByImportFlag", query = "SELECT f FROM FhrdDependent f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdDependent.findBySystemUserid", query = "SELECT f FROM FhrdDependent f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdDependent.findByEmpno", query = "SELECT f FROM FhrdDependent f WHERE f.empno = :empno")})
public class FhrdDependent implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FhrdDependentPK fhrdDependentPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NAME")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "RELATION")
    private String relation;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "GENDER")
    private String gender;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BIRTHDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date birthdt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "OCCUPATION")
    private String occupation;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Column(name = "EMPNO")
    private Integer empno;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FhrdEmpmst fhrdEmpmst;

    public FhrdDependent() {
    }

    public FhrdDependent(FhrdDependentPK fhrdDependentPK) {
        this.fhrdDependentPK = fhrdDependentPK;
    }

    public FhrdDependent(FhrdDependentPK fhrdDependentPK, String name, String relation, String gender, Date birthdt, String occupation, Date createdt) {
        this.fhrdDependentPK = fhrdDependentPK;
        this.name = name;
        this.relation = relation;
        this.gender = gender;
        this.birthdt = birthdt;
        this.occupation = occupation;
        this.createdt = createdt;
    }

    public FhrdDependent(int userId, int depSrno) {
        this.fhrdDependentPK = new FhrdDependentPK(userId, depSrno);
    }

    public FhrdDependentPK getFhrdDependentPK() {
        return fhrdDependentPK;
    }

    public void setFhrdDependentPK(FhrdDependentPK fhrdDependentPK) {
        this.fhrdDependentPK = fhrdDependentPK;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirthdt() {
        return birthdt;
    }

    public void setBirthdt(Date birthdt) {
        this.birthdt = birthdt;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public Integer getEmpno() {
        return empno;
    }

    public void setEmpno(Integer empno) {
        this.empno = empno;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getFhrdEmpmst() {
        return fhrdEmpmst;
    }

    public void setFhrdEmpmst(FhrdEmpmst fhrdEmpmst) {
        this.fhrdEmpmst = fhrdEmpmst;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fhrdDependentPK != null ? fhrdDependentPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdDependent)) {
            return false;
        }
        FhrdDependent other = (FhrdDependent) object;
        if ((this.fhrdDependentPK == null && other.fhrdDependentPK != null) || (this.fhrdDependentPK != null && !this.fhrdDependentPK.equals(other.fhrdDependentPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdDependent[ fhrdDependentPK=" + fhrdDependentPK + " ]";
    }
    
}
