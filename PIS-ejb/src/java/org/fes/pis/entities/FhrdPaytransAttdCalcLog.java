/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_PAYTRANS_ATTD_CALC_LOG")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdPaytransAttdCalcLog.findAll", query = "SELECT f FROM FhrdPaytransAttdCalcLog f"),
    @NamedQuery(name = "FhrdPaytransAttdCalcLog.findByAttdSrgKey", query = "SELECT f FROM FhrdPaytransAttdCalcLog f WHERE f.fhrdPaytransAttdCalcLogPK.attdSrgKey = :attdSrgKey"),
    @NamedQuery(name = "FhrdPaytransAttdCalcLog.findByAttdUserId", query = "SELECT f FROM FhrdPaytransAttdCalcLog f WHERE f.fhrdPaytransAttdCalcLogPK.attdUserId = :attdUserId"),
    @NamedQuery(name = "FhrdPaytransAttdCalcLog.findByAttdErrorMessage", query = "SELECT f FROM FhrdPaytransAttdCalcLog f WHERE f.attdErrorMessage = :attdErrorMessage"),
    @NamedQuery(name = "FhrdPaytransAttdCalcLog.findByAttdPossibleReason", query = "SELECT f FROM FhrdPaytransAttdCalcLog f WHERE f.attdPossibleReason = :attdPossibleReason"),
    @NamedQuery(name = "FhrdPaytransAttdCalcLog.findByAttdFromDate", query = "SELECT f FROM FhrdPaytransAttdCalcLog f WHERE f.attdFromDate = :attdFromDate"),
    @NamedQuery(name = "FhrdPaytransAttdCalcLog.findByAttdToDate", query = "SELECT f FROM FhrdPaytransAttdCalcLog f WHERE f.attdToDate = :attdToDate"),
    @NamedQuery(name = "FhrdPaytransAttdCalcLog.findByAttdCreateDate", query = "SELECT f FROM FhrdPaytransAttdCalcLog f WHERE f.attdCreateDate = :attdCreateDate"),
    @NamedQuery(name = "FhrdPaytransAttdCalcLog.findByTranYear", query = "SELECT f FROM FhrdPaytransAttdCalcLog f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FhrdPaytransAttdCalcLog.findByLogLevel", query = "SELECT f FROM FhrdPaytransAttdCalcLog f WHERE f.logLevel = :logLevel")})
public class FhrdPaytransAttdCalcLog implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FhrdPaytransAttdCalcLogPK fhrdPaytransAttdCalcLogPK;
    @Size(max = 4000)
    @Column(name = "ATTD_ERROR_MESSAGE")
    private String attdErrorMessage;
    @Size(max = 2000)
    @Column(name = "ATTD_POSSIBLE_REASON")
    private String attdPossibleReason;
    @Column(name = "ATTD_FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date attdFromDate;
    @Column(name = "ATTD_TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date attdToDate;
    @Column(name = "ATTD_CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date attdCreateDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "LOG_LEVEL")
    private String logLevel;

    public FhrdPaytransAttdCalcLog() {
    }

    public FhrdPaytransAttdCalcLog(FhrdPaytransAttdCalcLogPK fhrdPaytransAttdCalcLogPK) {
        this.fhrdPaytransAttdCalcLogPK = fhrdPaytransAttdCalcLogPK;
    }

    public FhrdPaytransAttdCalcLog(FhrdPaytransAttdCalcLogPK fhrdPaytransAttdCalcLogPK, short tranYear, String logLevel) {
        this.fhrdPaytransAttdCalcLogPK = fhrdPaytransAttdCalcLogPK;
        this.tranYear = tranYear;
        this.logLevel = logLevel;
    }

    public FhrdPaytransAttdCalcLog(BigDecimal attdSrgKey, int attdUserId) {
        this.fhrdPaytransAttdCalcLogPK = new FhrdPaytransAttdCalcLogPK(attdSrgKey, attdUserId);
    }

    public FhrdPaytransAttdCalcLogPK getFhrdPaytransAttdCalcLogPK() {
        return fhrdPaytransAttdCalcLogPK;
    }

    public void setFhrdPaytransAttdCalcLogPK(FhrdPaytransAttdCalcLogPK fhrdPaytransAttdCalcLogPK) {
        this.fhrdPaytransAttdCalcLogPK = fhrdPaytransAttdCalcLogPK;
    }

    public String getAttdErrorMessage() {
        return attdErrorMessage;
    }

    public void setAttdErrorMessage(String attdErrorMessage) {
        this.attdErrorMessage = attdErrorMessage;
    }

    public String getAttdPossibleReason() {
        return attdPossibleReason;
    }

    public void setAttdPossibleReason(String attdPossibleReason) {
        this.attdPossibleReason = attdPossibleReason;
    }

    public Date getAttdFromDate() {
        return attdFromDate;
    }

    public void setAttdFromDate(Date attdFromDate) {
        this.attdFromDate = attdFromDate;
    }

    public Date getAttdToDate() {
        return attdToDate;
    }

    public void setAttdToDate(Date attdToDate) {
        this.attdToDate = attdToDate;
    }

    public Date getAttdCreateDate() {
        return attdCreateDate;
    }

    public void setAttdCreateDate(Date attdCreateDate) {
        this.attdCreateDate = attdCreateDate;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public String getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(String logLevel) {
        this.logLevel = logLevel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fhrdPaytransAttdCalcLogPK != null ? fhrdPaytransAttdCalcLogPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdPaytransAttdCalcLog)) {
            return false;
        }
        FhrdPaytransAttdCalcLog other = (FhrdPaytransAttdCalcLog) object;
        if ((this.fhrdPaytransAttdCalcLogPK == null && other.fhrdPaytransAttdCalcLogPK != null) || (this.fhrdPaytransAttdCalcLogPK != null && !this.fhrdPaytransAttdCalcLogPK.equals(other.fhrdPaytransAttdCalcLogPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdPaytransAttdCalcLog[ fhrdPaytransAttdCalcLogPK=" + fhrdPaytransAttdCalcLogPK + " ]";
    }
    
}
