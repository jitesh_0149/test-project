/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_TADA_FACILITY_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasTadaFacilityDtl.findAll", query = "SELECT f FROM FtasTadaFacilityDtl f"),
    @NamedQuery(name = "FtasTadaFacilityDtl.findByTbmSrgKey", query = "SELECT f FROM FtasTadaFacilityDtl f WHERE f.ftasTadaFacilityDtlPK.tbmSrgKey = :tbmSrgKey"),
    @NamedQuery(name = "FtasTadaFacilityDtl.findBySrno", query = "SELECT f FROM FtasTadaFacilityDtl f WHERE f.ftasTadaFacilityDtlPK.srno = :srno"),
    @NamedQuery(name = "FtasTadaFacilityDtl.findByUserId", query = "SELECT f FROM FtasTadaFacilityDtl f WHERE f.userId = :userId"),
    @NamedQuery(name = "FtasTadaFacilityDtl.findByFacilityDt", query = "SELECT f FROM FtasTadaFacilityDtl f WHERE f.facilityDt = :facilityDt"),
    @NamedQuery(name = "FtasTadaFacilityDtl.findByFacilityBy", query = "SELECT f FROM FtasTadaFacilityDtl f WHERE f.facilityBy = :facilityBy"),
    @NamedQuery(name = "FtasTadaFacilityDtl.findByFacilityDesc", query = "SELECT f FROM FtasTadaFacilityDtl f WHERE f.facilityDesc = :facilityDesc"),
    @NamedQuery(name = "FtasTadaFacilityDtl.findByCreatedt", query = "SELECT f FROM FtasTadaFacilityDtl f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasTadaFacilityDtl.findByUpdatedt", query = "SELECT f FROM FtasTadaFacilityDtl f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasTadaFacilityDtl.findBySystemUserid", query = "SELECT f FROM FtasTadaFacilityDtl f WHERE f.systemUserid = :systemUserid")})
public class FtasTadaFacilityDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FtasTadaFacilityDtlPK ftasTadaFacilityDtlPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_ID")
    private int userId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FACILITY_DT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date facilityDt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "FACILITY_BY")
    private String facilityBy;
    @Size(max = 500)
    @Column(name = "FACILITY_DESC")
    private String facilityDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "TBM_SRG_KEY", referencedColumnName = "TBM_SRG_KEY", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FtasTadaBillMst ftasTadaBillMst;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FtasTadaFacilityDtl() {
    }

    public FtasTadaFacilityDtl(FtasTadaFacilityDtlPK ftasTadaFacilityDtlPK) {
        this.ftasTadaFacilityDtlPK = ftasTadaFacilityDtlPK;
    }

    public FtasTadaFacilityDtl(FtasTadaFacilityDtlPK ftasTadaFacilityDtlPK, int userId, Date facilityDt, String facilityBy, Date createdt) {
        this.ftasTadaFacilityDtlPK = ftasTadaFacilityDtlPK;
        this.userId = userId;
        this.facilityDt = facilityDt;
        this.facilityBy = facilityBy;
        this.createdt = createdt;
    }

    public FtasTadaFacilityDtl(BigDecimal tbmSrgKey, int srno) {
        this.ftasTadaFacilityDtlPK = new FtasTadaFacilityDtlPK(tbmSrgKey, srno);
    }

    public FtasTadaFacilityDtlPK getFtasTadaFacilityDtlPK() {
        return ftasTadaFacilityDtlPK;
    }

    public void setFtasTadaFacilityDtlPK(FtasTadaFacilityDtlPK ftasTadaFacilityDtlPK) {
        this.ftasTadaFacilityDtlPK = ftasTadaFacilityDtlPK;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getFacilityDt() {
        return facilityDt;
    }

    public void setFacilityDt(Date facilityDt) {
        this.facilityDt = facilityDt;
    }

    public String getFacilityBy() {
        return facilityBy;
    }

    public void setFacilityBy(String facilityBy) {
        this.facilityBy = facilityBy;
    }

    public String getFacilityDesc() {
        return facilityDesc;
    }

    public void setFacilityDesc(String facilityDesc) {
        this.facilityDesc = facilityDesc;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasTadaBillMst getFtasTadaBillMst() {
        return ftasTadaBillMst;
    }

    public void setFtasTadaBillMst(FtasTadaBillMst ftasTadaBillMst) {
        this.ftasTadaBillMst = ftasTadaBillMst;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ftasTadaFacilityDtlPK != null ? ftasTadaFacilityDtlPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasTadaFacilityDtl)) {
            return false;
        }
        FtasTadaFacilityDtl other = (FtasTadaFacilityDtl) object;
        if ((this.ftasTadaFacilityDtlPK == null && other.ftasTadaFacilityDtlPK != null) || (this.ftasTadaFacilityDtlPK != null && !this.ftasTadaFacilityDtlPK.equals(other.ftasTadaFacilityDtlPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasTadaFacilityDtl[ ftasTadaFacilityDtlPK=" + ftasTadaFacilityDtlPK + " ]";
    }
    
}
