/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FCAD_ADDR_JOB_STAGE_TITLE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FcadAddrJobStageTitle.findAll", query = "SELECT f FROM FcadAddrJobStageTitle f"),
    @NamedQuery(name = "FcadAddrJobStageTitle.findByStageTitle", query = "SELECT f FROM FcadAddrJobStageTitle f WHERE f.stageTitle = :stageTitle"),
    @NamedQuery(name = "FcadAddrJobStageTitle.findByStagePreference", query = "SELECT f FROM FcadAddrJobStageTitle f WHERE f.stagePreference = :stagePreference"),
    @NamedQuery(name = "FcadAddrJobStageTitle.findByCreatedt", query = "SELECT f FROM FcadAddrJobStageTitle f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FcadAddrJobStageTitle.findByUpdatedt", query = "SELECT f FROM FcadAddrJobStageTitle f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FcadAddrJobStageTitle.findBySystemUserid", query = "SELECT f FROM FcadAddrJobStageTitle f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FcadAddrJobStageTitle.findByExportFlag", query = "SELECT f FROM FcadAddrJobStageTitle f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FcadAddrJobStageTitle.findByImportFlag", query = "SELECT f FROM FcadAddrJobStageTitle f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FcadAddrJobStageTitle.findByRecordDeleteFlag", query = "SELECT f FROM FcadAddrJobStageTitle f WHERE f.recordDeleteFlag = :recordDeleteFlag"),
    @NamedQuery(name = "FcadAddrJobStageTitle.findByStartDate", query = "SELECT f FROM FcadAddrJobStageTitle f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FcadAddrJobStageTitle.findByEndDate", query = "SELECT f FROM FcadAddrJobStageTitle f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FcadAddrJobStageTitle.findByStUniqueSrno", query = "SELECT f FROM FcadAddrJobStageTitle f WHERE f.stUniqueSrno = :stUniqueSrno"),
    @NamedQuery(name = "FcadAddrJobStageTitle.findByCloneFlag", query = "SELECT f FROM FcadAddrJobStageTitle f WHERE f.cloneFlag = :cloneFlag"),
    @NamedQuery(name = "FcadAddrJobStageTitle.findByReuseStUniqueSrno", query = "SELECT f FROM FcadAddrJobStageTitle f WHERE f.reuseStUniqueSrno = :reuseStUniqueSrno"),
    @NamedQuery(name = "FcadAddrJobStageTitle.findByStageStatus", query = "SELECT f FROM FcadAddrJobStageTitle f WHERE f.stageStatus = :stageStatus")})
public class FcadAddrJobStageTitle implements Serializable {
    private static final long serialVersionUID = 1L;
    @Size(max = 200)
    @Column(name = "STAGE_TITLE")
    private String stageTitle;
    @Column(name = "STAGE_PREFERENCE")
    private Short stagePreference;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Size(max = 1)
    @Column(name = "RECORD_DELETE_FLAG")
    private String recordDeleteFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ST_UNIQUE_SRNO")
    private Long stUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "CLONE_FLAG")
    private String cloneFlag;
    @Column(name = "REUSE_ST_UNIQUE_SRNO")
    private Long reuseStUniqueSrno;
    @Size(max = 1)
    @Column(name = "STAGE_STATUS")
    private String stageStatus;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "JOB_UNIQUE_SRNO", referencedColumnName = "JOB_UNIQUE_SRNO")
    @ManyToOne
    private FcadAddrJobMst jobUniqueSrno;

    public FcadAddrJobStageTitle() {
    }

    public FcadAddrJobStageTitle(Long stUniqueSrno) {
        this.stUniqueSrno = stUniqueSrno;
    }

    public FcadAddrJobStageTitle(Long stUniqueSrno, Date createdt, Date startDate, String cloneFlag) {
        this.stUniqueSrno = stUniqueSrno;
        this.createdt = createdt;
        this.startDate = startDate;
        this.cloneFlag = cloneFlag;
    }

    public String getStageTitle() {
        return stageTitle;
    }

    public void setStageTitle(String stageTitle) {
        this.stageTitle = stageTitle;
    }

    public Short getStagePreference() {
        return stagePreference;
    }

    public void setStagePreference(Short stagePreference) {
        this.stagePreference = stagePreference;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public String getRecordDeleteFlag() {
        return recordDeleteFlag;
    }

    public void setRecordDeleteFlag(String recordDeleteFlag) {
        this.recordDeleteFlag = recordDeleteFlag;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Long getStUniqueSrno() {
        return stUniqueSrno;
    }

    public void setStUniqueSrno(Long stUniqueSrno) {
        this.stUniqueSrno = stUniqueSrno;
    }

    public String getCloneFlag() {
        return cloneFlag;
    }

    public void setCloneFlag(String cloneFlag) {
        this.cloneFlag = cloneFlag;
    }

    public Long getReuseStUniqueSrno() {
        return reuseStUniqueSrno;
    }

    public void setReuseStUniqueSrno(Long reuseStUniqueSrno) {
        this.reuseStUniqueSrno = reuseStUniqueSrno;
    }

    public String getStageStatus() {
        return stageStatus;
    }

    public void setStageStatus(String stageStatus) {
        this.stageStatus = stageStatus;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FcadAddrJobMst getJobUniqueSrno() {
        return jobUniqueSrno;
    }

    public void setJobUniqueSrno(FcadAddrJobMst jobUniqueSrno) {
        this.jobUniqueSrno = jobUniqueSrno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (stUniqueSrno != null ? stUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FcadAddrJobStageTitle)) {
            return false;
        }
        FcadAddrJobStageTitle other = (FcadAddrJobStageTitle) object;
        if ((this.stUniqueSrno == null && other.stUniqueSrno != null) || (this.stUniqueSrno != null && !this.stUniqueSrno.equals(other.stUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FcadAddrJobStageTitle[ stUniqueSrno=" + stUniqueSrno + " ]";
    }
    
}
