/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 *
 * @author anu
 */
@Embeddable
public class FhrdPaytransearndednPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_ID")
    private int userId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_SALARY_FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgSalaryFromDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_SALARY_FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date empSalaryFromDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "PTED_EDM_SRG_KEY")
    private BigDecimal ptedEdmSrgKey;

    public FhrdPaytransearndednPK() {
    }

    public FhrdPaytransearndednPK(int userId, Date orgSalaryFromDate, Date empSalaryFromDate, BigDecimal ptedEdmSrgKey) {
        this.userId = userId;
        this.orgSalaryFromDate = orgSalaryFromDate;
        this.empSalaryFromDate = empSalaryFromDate;
        this.ptedEdmSrgKey = ptedEdmSrgKey;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getOrgSalaryFromDate() {
        return orgSalaryFromDate;
    }

    public void setOrgSalaryFromDate(Date orgSalaryFromDate) {
        this.orgSalaryFromDate = orgSalaryFromDate;
    }

    public Date getEmpSalaryFromDate() {
        return empSalaryFromDate;
    }

    public void setEmpSalaryFromDate(Date empSalaryFromDate) {
        this.empSalaryFromDate = empSalaryFromDate;
    }

    public BigDecimal getPtedEdmSrgKey() {
        return ptedEdmSrgKey;
    }

    public void setPtedEdmSrgKey(BigDecimal ptedEdmSrgKey) {
        this.ptedEdmSrgKey = ptedEdmSrgKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) userId;
        hash += (orgSalaryFromDate != null ? orgSalaryFromDate.hashCode() : 0);
        hash += (empSalaryFromDate != null ? empSalaryFromDate.hashCode() : 0);
        hash += (ptedEdmSrgKey != null ? ptedEdmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdPaytransearndednPK)) {
            return false;
        }
        FhrdPaytransearndednPK other = (FhrdPaytransearndednPK) object;
        if (this.userId != other.userId) {
            return false;
        }
        if ((this.orgSalaryFromDate == null && other.orgSalaryFromDate != null) || (this.orgSalaryFromDate != null && !this.orgSalaryFromDate.equals(other.orgSalaryFromDate))) {
            return false;
        }
        if ((this.empSalaryFromDate == null && other.empSalaryFromDate != null) || (this.empSalaryFromDate != null && !this.empSalaryFromDate.equals(other.empSalaryFromDate))) {
            return false;
        }
        if ((this.ptedEdmSrgKey == null && other.ptedEdmSrgKey != null) || (this.ptedEdmSrgKey != null && !this.ptedEdmSrgKey.equals(other.ptedEdmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdPaytransearndednPK[ userId=" + userId + ", orgSalaryFromDate=" + orgSalaryFromDate + ", empSalaryFromDate=" + empSalaryFromDate + ", ptedEdmSrgKey=" + ptedEdmSrgKey + " ]";
    }
    
}
