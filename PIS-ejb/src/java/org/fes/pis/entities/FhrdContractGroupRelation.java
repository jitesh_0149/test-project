/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_CONTRACT_GROUP_RELATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdContractGroupRelation.findAll", query = "SELECT f FROM FhrdContractGroupRelation f"),
    @NamedQuery(name = "FhrdContractGroupRelation.findByStartDate", query = "SELECT f FROM FhrdContractGroupRelation f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdContractGroupRelation.findByEndDate", query = "SELECT f FROM FhrdContractGroupRelation f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FhrdContractGroupRelation.findByCreatedt", query = "SELECT f FROM FhrdContractGroupRelation f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdContractGroupRelation.findByUpdatedt", query = "SELECT f FROM FhrdContractGroupRelation f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdContractGroupRelation.findBySystemUserid", query = "SELECT f FROM FhrdContractGroupRelation f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdContractGroupRelation.findByCgrSrgKey", query = "SELECT f FROM FhrdContractGroupRelation f WHERE f.cgrSrgKey = :cgrSrgKey")})
public class FhrdContractGroupRelation implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CGR_SRG_KEY")
    private BigDecimal cgrSrgKey;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "RAGM_SRG_KEY", referencedColumnName = "RAGM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdRuleApplGroupMst ragmSrgKey;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CM_SRG_KEY", referencedColumnName = "CM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdContractMst cmSrgKey;

    public FhrdContractGroupRelation() {
    }

    public FhrdContractGroupRelation(BigDecimal cgrSrgKey) {
        this.cgrSrgKey = cgrSrgKey;
    }

    public FhrdContractGroupRelation(BigDecimal cgrSrgKey, Date startDate, Date createdt) {
        this.cgrSrgKey = cgrSrgKey;
        this.startDate = startDate;
        this.createdt = createdt;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public BigDecimal getCgrSrgKey() {
        return cgrSrgKey;
    }

    public void setCgrSrgKey(BigDecimal cgrSrgKey) {
        this.cgrSrgKey = cgrSrgKey;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdRuleApplGroupMst getRagmSrgKey() {
        return ragmSrgKey;
    }

    public void setRagmSrgKey(FhrdRuleApplGroupMst ragmSrgKey) {
        this.ragmSrgKey = ragmSrgKey;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdContractMst getCmSrgKey() {
        return cmSrgKey;
    }

    public void setCmSrgKey(FhrdContractMst cmSrgKey) {
        this.cmSrgKey = cmSrgKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cgrSrgKey != null ? cgrSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdContractGroupRelation)) {
            return false;
        }
        FhrdContractGroupRelation other = (FhrdContractGroupRelation) object;
        if ((this.cgrSrgKey == null && other.cgrSrgKey != null) || (this.cgrSrgKey != null && !this.cgrSrgKey.equals(other.cgrSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdContractGroupRelation[ cgrSrgKey=" + cgrSrgKey + " ]";
    }
    
}
