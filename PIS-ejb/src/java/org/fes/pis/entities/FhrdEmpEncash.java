/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_EMP_ENCASH")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdEmpEncash.findAll", query = "SELECT f FROM FhrdEmpEncash f"),
    @NamedQuery(name = "FhrdEmpEncash.findByEncSrgKey", query = "SELECT f FROM FhrdEmpEncash f WHERE f.encSrgKey = :encSrgKey"),
    @NamedQuery(name = "FhrdEmpEncash.findByNoOfDays", query = "SELECT f FROM FhrdEmpEncash f WHERE f.noOfDays = :noOfDays"),
    @NamedQuery(name = "FhrdEmpEncash.findByAvailableBalance", query = "SELECT f FROM FhrdEmpEncash f WHERE f.availableBalance = :availableBalance"),
    @NamedQuery(name = "FhrdEmpEncash.findByRemarks", query = "SELECT f FROM FhrdEmpEncash f WHERE f.remarks = :remarks"),
    @NamedQuery(name = "FhrdEmpEncash.findByAprvDate", query = "SELECT f FROM FhrdEmpEncash f WHERE f.aprvDate = :aprvDate"),
    @NamedQuery(name = "FhrdEmpEncash.findByApplicationFlag", query = "SELECT f FROM FhrdEmpEncash f WHERE f.applicationFlag = :applicationFlag"),
    @NamedQuery(name = "FhrdEmpEncash.findByCreatedt", query = "SELECT f FROM FhrdEmpEncash f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdEmpEncash.findByUpdatedt", query = "SELECT f FROM FhrdEmpEncash f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdEmpEncash.findBySystemUserid", query = "SELECT f FROM FhrdEmpEncash f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdEmpEncash.findByAprvcomm", query = "SELECT f FROM FhrdEmpEncash f WHERE f.aprvcomm = :aprvcomm"),
    @NamedQuery(name = "FhrdEmpEncash.findByTranYear", query = "SELECT f FROM FhrdEmpEncash f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FhrdEmpEncash.findByExportFlag", query = "SELECT f FROM FhrdEmpEncash f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdEmpEncash.findByImportFlag", query = "SELECT f FROM FhrdEmpEncash f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdEmpEncash.findByApplicationDate", query = "SELECT f FROM FhrdEmpEncash f WHERE f.applicationDate = :applicationDate"),
    @NamedQuery(name = "FhrdEmpEncash.findByAttdCalcTimeEntryFlag", query = "SELECT f FROM FhrdEmpEncash f WHERE f.attdCalcTimeEntryFlag = :attdCalcTimeEntryFlag"),
    @NamedQuery(name = "FhrdEmpEncash.findByCarryForwardFromOldBal", query = "SELECT f FROM FhrdEmpEncash f WHERE f.carryForwardFromOldBal = :carryForwardFromOldBal"),
    @NamedQuery(name = "FhrdEmpEncash.findByLeavecd", query = "SELECT f FROM FhrdEmpEncash f WHERE f.leavecd = :leavecd"),
    @NamedQuery(name = "FhrdEmpEncash.findByCancEncAprvcomm", query = "SELECT f FROM FhrdEmpEncash f WHERE f.cancEncAprvcomm = :cancEncAprvcomm")})
public class FhrdEmpEncash implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ENC_SRG_KEY")
    private BigDecimal encSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NO_OF_DAYS")
    private BigDecimal noOfDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AVAILABLE_BALANCE")
    private BigDecimal availableBalance;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "REMARKS")
    private String remarks;
    @Column(name = "APRV_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date aprvDate;
    @Size(max = 1)
    @Column(name = "APPLICATION_FLAG")
    private String applicationFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "APRVCOMM")
    private String aprvcomm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APPLICATION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date applicationDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "ATTD_CALC_TIME_ENTRY_FLAG")
    private String attdCalcTimeEntryFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CARRY_FORWARD_FROM_OLD_BAL")
    private BigDecimal carryForwardFromOldBal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "LEAVECD")
    private String leavecd;
    @Size(max = 1)
    @Column(name = "CANC_ENC_APRVCOMM")
    private String cancEncAprvcomm;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne(optional = false)
    private FtasLeavemst lmSrgKey;
    @JoinColumn(name = "LR_SRG_KEY", referencedColumnName = "LR_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdLeaverule lrSrgKey;
    @JoinColumn(name = "CANC_ENC_SRG_KEY", referencedColumnName = "CANC_ENC_SRG_KEY")
    @ManyToOne
    private FhrdEmpCancEncash cancEncSrgKey;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst userId;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "ACTNBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst actnby;
    @JoinColumn(name = "APRVBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst aprvby;
    @JoinColumn(name = "EMPLB_SRG_KEY", referencedColumnName = "EMPLB_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdEmpleavebal emplbSrgKey;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "encSrgKey")
    private Collection<FhrdEmpCancEncash> fhrdEmpCancEncashCollection;

    public FhrdEmpEncash() {
    }

    public FhrdEmpEncash(BigDecimal encSrgKey) {
        this.encSrgKey = encSrgKey;
    }

    public FhrdEmpEncash(BigDecimal encSrgKey, BigDecimal noOfDays, BigDecimal availableBalance, String remarks, Date createdt, short tranYear, Date applicationDate, String attdCalcTimeEntryFlag, BigDecimal carryForwardFromOldBal, String leavecd) {
        this.encSrgKey = encSrgKey;
        this.noOfDays = noOfDays;
        this.availableBalance = availableBalance;
        this.remarks = remarks;
        this.createdt = createdt;
        this.tranYear = tranYear;
        this.applicationDate = applicationDate;
        this.attdCalcTimeEntryFlag = attdCalcTimeEntryFlag;
        this.carryForwardFromOldBal = carryForwardFromOldBal;
        this.leavecd = leavecd;
    }

    public BigDecimal getEncSrgKey() {
        return encSrgKey;
    }

    public void setEncSrgKey(BigDecimal encSrgKey) {
        this.encSrgKey = encSrgKey;
    }

    public BigDecimal getNoOfDays() {
        return noOfDays;
    }

    public void setNoOfDays(BigDecimal noOfDays) {
        this.noOfDays = noOfDays;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getAprvDate() {
        return aprvDate;
    }

    public void setAprvDate(Date aprvDate) {
        this.aprvDate = aprvDate;
    }

    public String getApplicationFlag() {
        return applicationFlag;
    }

    public void setApplicationFlag(String applicationFlag) {
        this.applicationFlag = applicationFlag;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getAprvcomm() {
        return aprvcomm;
    }

    public void setAprvcomm(String aprvcomm) {
        this.aprvcomm = aprvcomm;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getAttdCalcTimeEntryFlag() {
        return attdCalcTimeEntryFlag;
    }

    public void setAttdCalcTimeEntryFlag(String attdCalcTimeEntryFlag) {
        this.attdCalcTimeEntryFlag = attdCalcTimeEntryFlag;
    }

    public BigDecimal getCarryForwardFromOldBal() {
        return carryForwardFromOldBal;
    }

    public void setCarryForwardFromOldBal(BigDecimal carryForwardFromOldBal) {
        this.carryForwardFromOldBal = carryForwardFromOldBal;
    }

    public String getLeavecd() {
        return leavecd;
    }

    public void setLeavecd(String leavecd) {
        this.leavecd = leavecd;
    }

    public String getCancEncAprvcomm() {
        return cancEncAprvcomm;
    }

    public void setCancEncAprvcomm(String cancEncAprvcomm) {
        this.cancEncAprvcomm = cancEncAprvcomm;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasLeavemst getLmSrgKey() {
        return lmSrgKey;
    }

    public void setLmSrgKey(FtasLeavemst lmSrgKey) {
        this.lmSrgKey = lmSrgKey;
    }

    public FhrdLeaverule getLrSrgKey() {
        return lrSrgKey;
    }

    public void setLrSrgKey(FhrdLeaverule lrSrgKey) {
        this.lrSrgKey = lrSrgKey;
    }

    public FhrdEmpCancEncash getCancEncSrgKey() {
        return cancEncSrgKey;
    }

    public void setCancEncSrgKey(FhrdEmpCancEncash cancEncSrgKey) {
        this.cancEncSrgKey = cancEncSrgKey;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getUserId() {
        return userId;
    }

    public void setUserId(FhrdEmpmst userId) {
        this.userId = userId;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getActnby() {
        return actnby;
    }

    public void setActnby(FhrdEmpmst actnby) {
        this.actnby = actnby;
    }

    public FhrdEmpmst getAprvby() {
        return aprvby;
    }

    public void setAprvby(FhrdEmpmst aprvby) {
        this.aprvby = aprvby;
    }

    public FhrdEmpleavebal getEmplbSrgKey() {
        return emplbSrgKey;
    }

    public void setEmplbSrgKey(FhrdEmpleavebal emplbSrgKey) {
        this.emplbSrgKey = emplbSrgKey;
    }

    @XmlTransient
    public Collection<FhrdEmpCancEncash> getFhrdEmpCancEncashCollection() {
        return fhrdEmpCancEncashCollection;
    }

    public void setFhrdEmpCancEncashCollection(Collection<FhrdEmpCancEncash> fhrdEmpCancEncashCollection) {
        this.fhrdEmpCancEncashCollection = fhrdEmpCancEncashCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (encSrgKey != null ? encSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdEmpEncash)) {
            return false;
        }
        FhrdEmpEncash other = (FhrdEmpEncash) object;
        if ((this.encSrgKey == null && other.encSrgKey != null) || (this.encSrgKey != null && !this.encSrgKey.equals(other.encSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdEmpEncash[ encSrgKey=" + encSrgKey + " ]";
    }
    
}
