/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_EMP_CONTRACT_GROUP_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdEmpContractGroupMst.findAll", query = "SELECT f FROM FhrdEmpContractGroupMst f"),
    @NamedQuery(name = "FhrdEmpContractGroupMst.findByOumUnitSrno", query = "SELECT f FROM FhrdEmpContractGroupMst f WHERE f.oumUnitSrno = :oumUnitSrno"),
    @NamedQuery(name = "FhrdEmpContractGroupMst.findByUserId", query = "SELECT f FROM FhrdEmpContractGroupMst f WHERE f.userId = :userId"),
    @NamedQuery(name = "FhrdEmpContractGroupMst.findByEcgmSrgKey", query = "SELECT f FROM FhrdEmpContractGroupMst f WHERE f.ecgmSrgKey = :ecgmSrgKey"),
    @NamedQuery(name = "FhrdEmpContractGroupMst.findByStartDate", query = "SELECT f FROM FhrdEmpContractGroupMst f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdEmpContractGroupMst.findByEndDate", query = "SELECT f FROM FhrdEmpContractGroupMst f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FhrdEmpContractGroupMst.findByCreatedt", query = "SELECT f FROM FhrdEmpContractGroupMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdEmpContractGroupMst.findByCreateby", query = "SELECT f FROM FhrdEmpContractGroupMst f WHERE f.createby = :createby"),
    @NamedQuery(name = "FhrdEmpContractGroupMst.findByUpdatedt", query = "SELECT f FROM FhrdEmpContractGroupMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdEmpContractGroupMst.findByUpdateby", query = "SELECT f FROM FhrdEmpContractGroupMst f WHERE f.updateby = :updateby"),
    @NamedQuery(name = "FhrdEmpContractGroupMst.findBySystemUserid", query = "SELECT f FROM FhrdEmpContractGroupMst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdEmpContractGroupMst.findByRmgmAltSrno", query = "SELECT f FROM FhrdEmpContractGroupMst f WHERE f.rmgmAltSrno = :rmgmAltSrno"),
    @NamedQuery(name = "FhrdEmpContractGroupMst.findByRefOumUnitSrno", query = "SELECT f FROM FhrdEmpContractGroupMst f WHERE f.refOumUnitSrno = :refOumUnitSrno"),
    @NamedQuery(name = "FhrdEmpContractGroupMst.findByTranYear", query = "SELECT f FROM FhrdEmpContractGroupMst f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FhrdEmpContractGroupMst.findBySystemUseridFlag", query = "SELECT f FROM FhrdEmpContractGroupMst f WHERE f.systemUseridFlag = :systemUseridFlag"),
    @NamedQuery(name = "FhrdEmpContractGroupMst.findByContStartDate", query = "SELECT f FROM FhrdEmpContractGroupMst f WHERE f.contStartDate = :contStartDate"),
    @NamedQuery(name = "FhrdEmpContractGroupMst.findByContEndDate", query = "SELECT f FROM FhrdEmpContractGroupMst f WHERE f.contEndDate = :contEndDate")})
public class FhrdEmpContractGroupMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OUM_UNIT_SRNO")
    private int oumUnitSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_ID")
    private int userId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ECGM_SRG_KEY")
    private BigDecimal ecgmSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEBY")
    private int createby;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "UPDATEBY")
    private Integer updateby;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Column(name = "RMGM_ALT_SRNO")
    private Integer rmgmAltSrno;
    @Column(name = "REF_OUM_UNIT_SRNO")
    private Integer refOumUnitSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Size(max = 1)
    @Column(name = "SYSTEM_USERID_FLAG")
    private String systemUseridFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONT_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date contStartDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONT_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date contEndDate;
    @JoinColumn(name = "RMGM_SRG_KEY", referencedColumnName = "RMGM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdRuleManagGroupMst rmgmSrgKey;
    @JoinColumn(name = "RGC_SRG_KEY", referencedColumnName = "RGC_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdRuleGroupCategory rgcSrgKey;
    @JoinColumn(name = "RAGM_SRG_KEY", referencedColumnName = "RAGM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdRuleApplGroupMst ragmSrgKey;
    @JoinColumn(name = "ECM_SRG_KEY", referencedColumnName = "ECM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdEmpContractMst ecmSrgKey;
    @JoinColumn(name = "CM_SRG_KEY", referencedColumnName = "CM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdContractMst cmSrgKey;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ecgmSrgKey")
    private Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ecgmSrgKey")
    private Collection<FhrdEmpearndedn> fhrdEmpearndednCollection;

    public FhrdEmpContractGroupMst() {
    }

    public FhrdEmpContractGroupMst(BigDecimal ecgmSrgKey) {
        this.ecgmSrgKey = ecgmSrgKey;
    }

    public FhrdEmpContractGroupMst(BigDecimal ecgmSrgKey, int oumUnitSrno, int userId, Date startDate, Date endDate, Date createdt, int createby, short tranYear, Date contStartDate, Date contEndDate) {
        this.ecgmSrgKey = ecgmSrgKey;
        this.oumUnitSrno = oumUnitSrno;
        this.userId = userId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.createdt = createdt;
        this.createby = createby;
        this.tranYear = tranYear;
        this.contStartDate = contStartDate;
        this.contEndDate = contEndDate;
    }

    public int getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(int oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public BigDecimal getEcgmSrgKey() {
        return ecgmSrgKey;
    }

    public void setEcgmSrgKey(BigDecimal ecgmSrgKey) {
        this.ecgmSrgKey = ecgmSrgKey;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public int getCreateby() {
        return createby;
    }

    public void setCreateby(int createby) {
        this.createby = createby;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getUpdateby() {
        return updateby;
    }

    public void setUpdateby(Integer updateby) {
        this.updateby = updateby;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public Integer getRmgmAltSrno() {
        return rmgmAltSrno;
    }

    public void setRmgmAltSrno(Integer rmgmAltSrno) {
        this.rmgmAltSrno = rmgmAltSrno;
    }

    public Integer getRefOumUnitSrno() {
        return refOumUnitSrno;
    }

    public void setRefOumUnitSrno(Integer refOumUnitSrno) {
        this.refOumUnitSrno = refOumUnitSrno;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public String getSystemUseridFlag() {
        return systemUseridFlag;
    }

    public void setSystemUseridFlag(String systemUseridFlag) {
        this.systemUseridFlag = systemUseridFlag;
    }

    public Date getContStartDate() {
        return contStartDate;
    }

    public void setContStartDate(Date contStartDate) {
        this.contStartDate = contStartDate;
    }

    public Date getContEndDate() {
        return contEndDate;
    }

    public void setContEndDate(Date contEndDate) {
        this.contEndDate = contEndDate;
    }

    public FhrdRuleManagGroupMst getRmgmSrgKey() {
        return rmgmSrgKey;
    }

    public void setRmgmSrgKey(FhrdRuleManagGroupMst rmgmSrgKey) {
        this.rmgmSrgKey = rmgmSrgKey;
    }

    public FhrdRuleGroupCategory getRgcSrgKey() {
        return rgcSrgKey;
    }

    public void setRgcSrgKey(FhrdRuleGroupCategory rgcSrgKey) {
        this.rgcSrgKey = rgcSrgKey;
    }

    public FhrdRuleApplGroupMst getRagmSrgKey() {
        return ragmSrgKey;
    }

    public void setRagmSrgKey(FhrdRuleApplGroupMst ragmSrgKey) {
        this.ragmSrgKey = ragmSrgKey;
    }

    public FhrdEmpContractMst getEcmSrgKey() {
        return ecmSrgKey;
    }

    public void setEcmSrgKey(FhrdEmpContractMst ecmSrgKey) {
        this.ecmSrgKey = ecmSrgKey;
    }

    public FhrdContractMst getCmSrgKey() {
        return cmSrgKey;
    }

    public void setCmSrgKey(FhrdContractMst cmSrgKey) {
        this.cmSrgKey = cmSrgKey;
    }

    @XmlTransient
    public Collection<FhrdEmpleaverule> getFhrdEmpleaveruleCollection() {
        return fhrdEmpleaveruleCollection;
    }

    public void setFhrdEmpleaveruleCollection(Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection) {
        this.fhrdEmpleaveruleCollection = fhrdEmpleaveruleCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpearndedn> getFhrdEmpearndednCollection() {
        return fhrdEmpearndednCollection;
    }

    public void setFhrdEmpearndednCollection(Collection<FhrdEmpearndedn> fhrdEmpearndednCollection) {
        this.fhrdEmpearndednCollection = fhrdEmpearndednCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ecgmSrgKey != null ? ecgmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdEmpContractGroupMst)) {
            return false;
        }
        FhrdEmpContractGroupMst other = (FhrdEmpContractGroupMst) object;
        if ((this.ecgmSrgKey == null && other.ecgmSrgKey != null) || (this.ecgmSrgKey != null && !this.ecgmSrgKey.equals(other.ecgmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdEmpContractGroupMst[ ecgmSrgKey=" + ecgmSrgKey + " ]";
    }
    
}
