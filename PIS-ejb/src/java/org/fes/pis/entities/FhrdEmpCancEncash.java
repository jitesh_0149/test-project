/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_EMP_CANC_ENCASH")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdEmpCancEncash.findAll", query = "SELECT f FROM FhrdEmpCancEncash f"),
    @NamedQuery(name = "FhrdEmpCancEncash.findByCancEncSrgKey", query = "SELECT f FROM FhrdEmpCancEncash f WHERE f.cancEncSrgKey = :cancEncSrgKey"),
    @NamedQuery(name = "FhrdEmpCancEncash.findByEmplbSrgKey", query = "SELECT f FROM FhrdEmpCancEncash f WHERE f.emplbSrgKey = :emplbSrgKey"),
    @NamedQuery(name = "FhrdEmpCancEncash.findByReason", query = "SELECT f FROM FhrdEmpCancEncash f WHERE f.reason = :reason"),
    @NamedQuery(name = "FhrdEmpCancEncash.findByAprvdt", query = "SELECT f FROM FhrdEmpCancEncash f WHERE f.aprvdt = :aprvdt"),
    @NamedQuery(name = "FhrdEmpCancEncash.findByAprvcomm", query = "SELECT f FROM FhrdEmpCancEncash f WHERE f.aprvcomm = :aprvcomm"),
    @NamedQuery(name = "FhrdEmpCancEncash.findByCreatedt", query = "SELECT f FROM FhrdEmpCancEncash f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdEmpCancEncash.findByUpdatedt", query = "SELECT f FROM FhrdEmpCancEncash f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdEmpCancEncash.findBySystemUserid", query = "SELECT f FROM FhrdEmpCancEncash f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdEmpCancEncash.findByTranYear", query = "SELECT f FROM FhrdEmpCancEncash f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FhrdEmpCancEncash.findByExportFlag", query = "SELECT f FROM FhrdEmpCancEncash f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdEmpCancEncash.findByImportFlag", query = "SELECT f FROM FhrdEmpCancEncash f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdEmpCancEncash.findByApplicationDate", query = "SELECT f FROM FhrdEmpCancEncash f WHERE f.applicationDate = :applicationDate")})
public class FhrdEmpCancEncash implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CANC_ENC_SRG_KEY")
    private BigDecimal cancEncSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMPLB_SRG_KEY")
    private BigDecimal emplbSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "REASON")
    private String reason;
    @Column(name = "APRVDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date aprvdt;
    @Size(max = 1)
    @Column(name = "APRVCOMM")
    private String aprvcomm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APPLICATION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date applicationDate;
    @OneToMany(mappedBy = "cancEncSrgKey")
    private Collection<FhrdEmpEncash> fhrdEmpEncashCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "ENC_SRG_KEY", referencedColumnName = "ENC_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdEmpEncash encSrgKey;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst userId;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "ACTNBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst actnby;
    @JoinColumn(name = "APRVBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst aprvby;

    public FhrdEmpCancEncash() {
    }

    public FhrdEmpCancEncash(BigDecimal cancEncSrgKey) {
        this.cancEncSrgKey = cancEncSrgKey;
    }

    public FhrdEmpCancEncash(BigDecimal cancEncSrgKey, BigDecimal emplbSrgKey, String reason, Date createdt, short tranYear, Date applicationDate) {
        this.cancEncSrgKey = cancEncSrgKey;
        this.emplbSrgKey = emplbSrgKey;
        this.reason = reason;
        this.createdt = createdt;
        this.tranYear = tranYear;
        this.applicationDate = applicationDate;
    }

    public BigDecimal getCancEncSrgKey() {
        return cancEncSrgKey;
    }

    public void setCancEncSrgKey(BigDecimal cancEncSrgKey) {
        this.cancEncSrgKey = cancEncSrgKey;
    }

    public BigDecimal getEmplbSrgKey() {
        return emplbSrgKey;
    }

    public void setEmplbSrgKey(BigDecimal emplbSrgKey) {
        this.emplbSrgKey = emplbSrgKey;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getAprvdt() {
        return aprvdt;
    }

    public void setAprvdt(Date aprvdt) {
        this.aprvdt = aprvdt;
    }

    public String getAprvcomm() {
        return aprvcomm;
    }

    public void setAprvcomm(String aprvcomm) {
        this.aprvcomm = aprvcomm;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    @XmlTransient
    public Collection<FhrdEmpEncash> getFhrdEmpEncashCollection() {
        return fhrdEmpEncashCollection;
    }

    public void setFhrdEmpEncashCollection(Collection<FhrdEmpEncash> fhrdEmpEncashCollection) {
        this.fhrdEmpEncashCollection = fhrdEmpEncashCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpEncash getEncSrgKey() {
        return encSrgKey;
    }

    public void setEncSrgKey(FhrdEmpEncash encSrgKey) {
        this.encSrgKey = encSrgKey;
    }

    public FhrdEmpmst getUserId() {
        return userId;
    }

    public void setUserId(FhrdEmpmst userId) {
        this.userId = userId;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getActnby() {
        return actnby;
    }

    public void setActnby(FhrdEmpmst actnby) {
        this.actnby = actnby;
    }

    public FhrdEmpmst getAprvby() {
        return aprvby;
    }

    public void setAprvby(FhrdEmpmst aprvby) {
        this.aprvby = aprvby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cancEncSrgKey != null ? cancEncSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdEmpCancEncash)) {
            return false;
        }
        FhrdEmpCancEncash other = (FhrdEmpCancEncash) object;
        if ((this.cancEncSrgKey == null && other.cancEncSrgKey != null) || (this.cancEncSrgKey != null && !this.cancEncSrgKey.equals(other.cancEncSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdEmpCancEncash[ cancEncSrgKey=" + cancEncSrgKey + " ]";
    }
    
}
