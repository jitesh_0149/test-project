/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_UNIVERSITYMST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdUniversitymst.findAll", query = "SELECT f FROM FhrdUniversitymst f"),
    @NamedQuery(name = "FhrdUniversitymst.findByUniversitySrno", query = "SELECT f FROM FhrdUniversitymst f WHERE f.universitySrno = :universitySrno"),
    @NamedQuery(name = "FhrdUniversitymst.findByUniversitycd", query = "SELECT f FROM FhrdUniversitymst f WHERE f.universitycd = :universitycd"),
    @NamedQuery(name = "FhrdUniversitymst.findByUniversityName", query = "SELECT f FROM FhrdUniversitymst f WHERE f.universityName = :universityName"),
    @NamedQuery(name = "FhrdUniversitymst.findByCity", query = "SELECT f FROM FhrdUniversitymst f WHERE f.city = :city"),
    @NamedQuery(name = "FhrdUniversitymst.findByState", query = "SELECT f FROM FhrdUniversitymst f WHERE f.state = :state"),
    @NamedQuery(name = "FhrdUniversitymst.findByCountry", query = "SELECT f FROM FhrdUniversitymst f WHERE f.country = :country"),
    @NamedQuery(name = "FhrdUniversitymst.findByCreatedt", query = "SELECT f FROM FhrdUniversitymst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdUniversitymst.findByUpdatedt", query = "SELECT f FROM FhrdUniversitymst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdUniversitymst.findByExportFlag", query = "SELECT f FROM FhrdUniversitymst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdUniversitymst.findByImportFlag", query = "SELECT f FROM FhrdUniversitymst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdUniversitymst.findBySystemUserid", query = "SELECT f FROM FhrdUniversitymst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdUniversitymst.findByUmSrgKey", query = "SELECT f FROM FhrdUniversitymst f WHERE f.umSrgKey = :umSrgKey"),
    @NamedQuery(name = "FhrdUniversitymst.findByPincode", query = "SELECT f FROM FhrdUniversitymst f WHERE f.pincode = :pincode")})
public class FhrdUniversitymst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UNIVERSITY_SRNO")
    private int universitySrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "UNIVERSITYCD")
    private String universitycd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "UNIVERSITY_NAME")
    private String universityName;
    @Size(max = 50)
    @Column(name = "CITY")
    private String city;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "STATE")
    private String state;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "COUNTRY")
    private String country;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "UM_SRG_KEY")
    private BigDecimal umSrgKey;
    @Size(max = 10)
    @Column(name = "PINCODE")
    private String pincode;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @OneToMany(mappedBy = "umSrgKey")
    private Collection<FhrdQualification> fhrdQualificationCollection;

    public FhrdUniversitymst() {
    }

    public FhrdUniversitymst(BigDecimal umSrgKey) {
        this.umSrgKey = umSrgKey;
    }

    public FhrdUniversitymst(BigDecimal umSrgKey, int universitySrno, String universitycd, String universityName, String state, String country, Date createdt) {
        this.umSrgKey = umSrgKey;
        this.universitySrno = universitySrno;
        this.universitycd = universitycd;
        this.universityName = universityName;
        this.state = state;
        this.country = country;
        this.createdt = createdt;
    }

    public int getUniversitySrno() {
        return universitySrno;
    }

    public void setUniversitySrno(int universitySrno) {
        this.universitySrno = universitySrno;
    }

    public String getUniversitycd() {
        return universitycd;
    }

    public void setUniversitycd(String universitycd) {
        this.universitycd = universitycd;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public BigDecimal getUmSrgKey() {
        return umSrgKey;
    }

    public void setUmSrgKey(BigDecimal umSrgKey) {
        this.umSrgKey = umSrgKey;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @XmlTransient
    public Collection<FhrdQualification> getFhrdQualificationCollection() {
        return fhrdQualificationCollection;
    }

    public void setFhrdQualificationCollection(Collection<FhrdQualification> fhrdQualificationCollection) {
        this.fhrdQualificationCollection = fhrdQualificationCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (umSrgKey != null ? umSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdUniversitymst)) {
            return false;
        }
        FhrdUniversitymst other = (FhrdUniversitymst) object;
        if ((this.umSrgKey == null && other.umSrgKey != null) || (this.umSrgKey != null && !this.umSrgKey.equals(other.umSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdUniversitymst[ umSrgKey=" + umSrgKey + " ]";
    }
    
}
