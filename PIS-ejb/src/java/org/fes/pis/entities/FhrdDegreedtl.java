/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_DEGREEDTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdDegreedtl.findAll", query = "SELECT f FROM FhrdDegreedtl f"),
    @NamedQuery(name = "FhrdDegreedtl.findBySubjectName", query = "SELECT f FROM FhrdDegreedtl f WHERE f.subjectName = :subjectName"),
    @NamedQuery(name = "FhrdDegreedtl.findByCreatedt", query = "SELECT f FROM FhrdDegreedtl f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdDegreedtl.findByUpdatedt", query = "SELECT f FROM FhrdDegreedtl f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdDegreedtl.findByExportFlag", query = "SELECT f FROM FhrdDegreedtl f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdDegreedtl.findByImportFlag", query = "SELECT f FROM FhrdDegreedtl f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdDegreedtl.findBySystemUserid", query = "SELECT f FROM FhrdDegreedtl f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdDegreedtl.findBySubjectSrno", query = "SELECT f FROM FhrdDegreedtl f WHERE f.fhrdDegreedtlPK.subjectSrno = :subjectSrno"),
    @NamedQuery(name = "FhrdDegreedtl.findByDgrmSrgKey", query = "SELECT f FROM FhrdDegreedtl f WHERE f.fhrdDegreedtlPK.dgrmSrgKey = :dgrmSrgKey")})
public class FhrdDegreedtl implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FhrdDegreedtlPK fhrdDegreedtlPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "SUBJECT_NAME")
    private String subjectName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fhrdDegreedtl")
    private Collection<FhrdQualification> fhrdQualificationCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "DGRM_SRG_KEY", referencedColumnName = "DGRM_SRG_KEY", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FhrdDegreemst fhrdDegreemst;

    public FhrdDegreedtl() {
    }

    public FhrdDegreedtl(FhrdDegreedtlPK fhrdDegreedtlPK) {
        this.fhrdDegreedtlPK = fhrdDegreedtlPK;
    }

    public FhrdDegreedtl(FhrdDegreedtlPK fhrdDegreedtlPK, String subjectName, Date createdt) {
        this.fhrdDegreedtlPK = fhrdDegreedtlPK;
        this.subjectName = subjectName;
        this.createdt = createdt;
    }

    public FhrdDegreedtl(BigDecimal subjectSrno, BigDecimal dgrmSrgKey) {
        this.fhrdDegreedtlPK = new FhrdDegreedtlPK(subjectSrno, dgrmSrgKey);
    }

    public FhrdDegreedtlPK getFhrdDegreedtlPK() {
        return fhrdDegreedtlPK;
    }

    public void setFhrdDegreedtlPK(FhrdDegreedtlPK fhrdDegreedtlPK) {
        this.fhrdDegreedtlPK = fhrdDegreedtlPK;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    @XmlTransient
    public Collection<FhrdQualification> getFhrdQualificationCollection() {
        return fhrdQualificationCollection;
    }

    public void setFhrdQualificationCollection(Collection<FhrdQualification> fhrdQualificationCollection) {
        this.fhrdQualificationCollection = fhrdQualificationCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdDegreemst getFhrdDegreemst() {
        return fhrdDegreemst;
    }

    public void setFhrdDegreemst(FhrdDegreemst fhrdDegreemst) {
        this.fhrdDegreemst = fhrdDegreemst;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fhrdDegreedtlPK != null ? fhrdDegreedtlPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdDegreedtl)) {
            return false;
        }
        FhrdDegreedtl other = (FhrdDegreedtl) object;
        if ((this.fhrdDegreedtlPK == null && other.fhrdDegreedtlPK != null) || (this.fhrdDegreedtlPK != null && !this.fhrdDegreedtlPK.equals(other.fhrdDegreedtlPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdDegreedtl[ fhrdDegreedtlPK=" + fhrdDegreedtlPK + " ]";
    }
    
}
