/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author anu
 */
@Embeddable
public class FhrdQualificationPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_ID")
    private int userId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "QUAL_SRNO")
    private int qualSrno;

    public FhrdQualificationPK() {
    }

    public FhrdQualificationPK(int userId, int qualSrno) {
        this.userId = userId;
        this.qualSrno = qualSrno;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getQualSrno() {
        return qualSrno;
    }

    public void setQualSrno(int qualSrno) {
        this.qualSrno = qualSrno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) userId;
        hash += (int) qualSrno;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdQualificationPK)) {
            return false;
        }
        FhrdQualificationPK other = (FhrdQualificationPK) object;
        if (this.userId != other.userId) {
            return false;
        }
        if (this.qualSrno != other.qualSrno) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdQualificationPK[ userId=" + userId + ", qualSrno=" + qualSrno + " ]";
    }
    
}
