/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author anu
 */
@Embeddable
public class FtasTadaFareDtlPK implements Serializable {
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "TBM_SRG_KEY")
    private BigDecimal tbmSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SRNO")
    private int srno;

    public FtasTadaFareDtlPK() {
    }

    public FtasTadaFareDtlPK(BigDecimal tbmSrgKey, int srno) {
        this.tbmSrgKey = tbmSrgKey;
        this.srno = srno;
    }

    public BigDecimal getTbmSrgKey() {
        return tbmSrgKey;
    }

    public void setTbmSrgKey(BigDecimal tbmSrgKey) {
        this.tbmSrgKey = tbmSrgKey;
    }

    public int getSrno() {
        return srno;
    }

    public void setSrno(int srno) {
        this.srno = srno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tbmSrgKey != null ? tbmSrgKey.hashCode() : 0);
        hash += (int) srno;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasTadaFareDtlPK)) {
            return false;
        }
        FtasTadaFareDtlPK other = (FtasTadaFareDtlPK) object;
        if ((this.tbmSrgKey == null && other.tbmSrgKey != null) || (this.tbmSrgKey != null && !this.tbmSrgKey.equals(other.tbmSrgKey))) {
            return false;
        }
        if (this.srno != other.srno) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasTadaFareDtlPK[ tbmSrgKey=" + tbmSrgKey + ", srno=" + srno + " ]";
    }
    
}
