/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "BMWP_FUND_SRC_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmwpFundSrcMst.findAll", query = "SELECT b FROM BmwpFundSrcMst b"),
    @NamedQuery(name = "BmwpFundSrcMst.findByFsmUniqueSrno", query = "SELECT b FROM BmwpFundSrcMst b WHERE b.fsmUniqueSrno = :fsmUniqueSrno"),
    @NamedQuery(name = "BmwpFundSrcMst.findByFsmShortDesc", query = "SELECT b FROM BmwpFundSrcMst b WHERE b.fsmShortDesc = :fsmShortDesc"),
    @NamedQuery(name = "BmwpFundSrcMst.findByFsmTypeDesc", query = "SELECT b FROM BmwpFundSrcMst b WHERE b.fsmTypeDesc = :fsmTypeDesc"),
    @NamedQuery(name = "BmwpFundSrcMst.findByFsmStartDate", query = "SELECT b FROM BmwpFundSrcMst b WHERE b.fsmStartDate = :fsmStartDate"),
    @NamedQuery(name = "BmwpFundSrcMst.findByFsmEndDate", query = "SELECT b FROM BmwpFundSrcMst b WHERE b.fsmEndDate = :fsmEndDate"),
    @NamedQuery(name = "BmwpFundSrcMst.findByFsmEndDatePossible", query = "SELECT b FROM BmwpFundSrcMst b WHERE b.fsmEndDatePossible = :fsmEndDatePossible"),
    @NamedQuery(name = "BmwpFundSrcMst.findByFsmTransactionDate", query = "SELECT b FROM BmwpFundSrcMst b WHERE b.fsmTransactionDate = :fsmTransactionDate"),
    @NamedQuery(name = "BmwpFundSrcMst.findByCreatedt", query = "SELECT b FROM BmwpFundSrcMst b WHERE b.createdt = :createdt"),
    @NamedQuery(name = "BmwpFundSrcMst.findByUpdatedt", query = "SELECT b FROM BmwpFundSrcMst b WHERE b.updatedt = :updatedt"),
    @NamedQuery(name = "BmwpFundSrcMst.findBySystemUserid", query = "SELECT b FROM BmwpFundSrcMst b WHERE b.systemUserid = :systemUserid"),
    @NamedQuery(name = "BmwpFundSrcMst.findByExportFlag", query = "SELECT b FROM BmwpFundSrcMst b WHERE b.exportFlag = :exportFlag"),
    @NamedQuery(name = "BmwpFundSrcMst.findByImportFlag", query = "SELECT b FROM BmwpFundSrcMst b WHERE b.importFlag = :importFlag")})
public class BmwpFundSrcMst implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "FSM_UNIQUE_SRNO")
    private BigDecimal fsmUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "FSM_SHORT_DESC")
    private String fsmShortDesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "FSM_TYPE_DESC")
    private String fsmTypeDesc;
    @Column(name = "FSM_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fsmStartDate;
    @Column(name = "FSM_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fsmEndDate;
    @Column(name = "FSM_END_DATE_POSSIBLE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fsmEndDatePossible;
    @Column(name = "FSM_TRANSACTION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fsmTransactionDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @OneToMany(mappedBy = "pmFsmUniqueSrno")
    private Collection<BmwpProjectMst> bmwpProjectMstCollection;

    public BmwpFundSrcMst() {
    }

    public BmwpFundSrcMst(BigDecimal fsmUniqueSrno) {
        this.fsmUniqueSrno = fsmUniqueSrno;
    }

    public BmwpFundSrcMst(BigDecimal fsmUniqueSrno, String fsmShortDesc, String fsmTypeDesc, Date createdt) {
        this.fsmUniqueSrno = fsmUniqueSrno;
        this.fsmShortDesc = fsmShortDesc;
        this.fsmTypeDesc = fsmTypeDesc;
        this.createdt = createdt;
    }

    public BigDecimal getFsmUniqueSrno() {
        return fsmUniqueSrno;
    }

    public void setFsmUniqueSrno(BigDecimal fsmUniqueSrno) {
        this.fsmUniqueSrno = fsmUniqueSrno;
    }

    public String getFsmShortDesc() {
        return fsmShortDesc;
    }

    public void setFsmShortDesc(String fsmShortDesc) {
        this.fsmShortDesc = fsmShortDesc;
    }

    public String getFsmTypeDesc() {
        return fsmTypeDesc;
    }

    public void setFsmTypeDesc(String fsmTypeDesc) {
        this.fsmTypeDesc = fsmTypeDesc;
    }

    public Date getFsmStartDate() {
        return fsmStartDate;
    }

    public void setFsmStartDate(Date fsmStartDate) {
        this.fsmStartDate = fsmStartDate;
    }

    public Date getFsmEndDate() {
        return fsmEndDate;
    }

    public void setFsmEndDate(Date fsmEndDate) {
        this.fsmEndDate = fsmEndDate;
    }

    public Date getFsmEndDatePossible() {
        return fsmEndDatePossible;
    }

    public void setFsmEndDatePossible(Date fsmEndDatePossible) {
        this.fsmEndDatePossible = fsmEndDatePossible;
    }

    public Date getFsmTransactionDate() {
        return fsmTransactionDate;
    }

    public void setFsmTransactionDate(Date fsmTransactionDate) {
        this.fsmTransactionDate = fsmTransactionDate;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @XmlTransient
    public Collection<BmwpProjectMst> getBmwpProjectMstCollection() {
        return bmwpProjectMstCollection;
    }

    public void setBmwpProjectMstCollection(Collection<BmwpProjectMst> bmwpProjectMstCollection) {
        this.bmwpProjectMstCollection = bmwpProjectMstCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fsmUniqueSrno != null ? fsmUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmwpFundSrcMst)) {
            return false;
        }
        BmwpFundSrcMst other = (BmwpFundSrcMst) object;
        if ((this.fsmUniqueSrno == null && other.fsmUniqueSrno != null) || (this.fsmUniqueSrno != null && !this.fsmUniqueSrno.equals(other.fsmUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.BmwpFundSrcMst[ fsmUniqueSrno=" + fsmUniqueSrno + " ]";
    }
    
}
