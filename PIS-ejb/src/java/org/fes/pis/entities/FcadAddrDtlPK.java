/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author anu
 */
@Embeddable
public class FcadAddrDtlPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "AD_AM_UNIQUE_SRNO")
    private BigInteger adAmUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AD_SRNO")
    private int adSrno;

    public FcadAddrDtlPK() {
    }

    public FcadAddrDtlPK(BigInteger adAmUniqueSrno, int adSrno) {
        this.adAmUniqueSrno = adAmUniqueSrno;
        this.adSrno = adSrno;
    }

    public BigInteger getAdAmUniqueSrno() {
        return adAmUniqueSrno;
    }

    public void setAdAmUniqueSrno(BigInteger adAmUniqueSrno) {
        this.adAmUniqueSrno = adAmUniqueSrno;
    }

    public int getAdSrno() {
        return adSrno;
    }

    public void setAdSrno(int adSrno) {
        this.adSrno = adSrno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (adAmUniqueSrno != null ? adAmUniqueSrno.hashCode() : 0);
        hash += (int) adSrno;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FcadAddrDtlPK)) {
            return false;
        }
        FcadAddrDtlPK other = (FcadAddrDtlPK) object;
        if ((this.adAmUniqueSrno == null && other.adAmUniqueSrno != null) || (this.adAmUniqueSrno != null && !this.adAmUniqueSrno.equals(other.adAmUniqueSrno))) {
            return false;
        }
        if (this.adSrno != other.adSrno) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FcadAddrDtlPK[ adAmUniqueSrno=" + adAmUniqueSrno + ", adSrno=" + adSrno + " ]";
    }
    
}
