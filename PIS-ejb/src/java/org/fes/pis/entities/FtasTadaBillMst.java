/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_TADA_BILL_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasTadaBillMst.findAll", query = "SELECT f FROM FtasTadaBillMst f"),
    @NamedQuery(name = "FtasTadaBillMst.findByTbmSrgKey", query = "SELECT f FROM FtasTadaBillMst f WHERE f.tbmSrgKey = :tbmSrgKey"),
    @NamedQuery(name = "FtasTadaBillMst.findByLeaveDesc", query = "SELECT f FROM FtasTadaBillMst f WHERE f.leaveDesc = :leaveDesc"),
    @NamedQuery(name = "FtasTadaBillMst.findByFromDate", query = "SELECT f FROM FtasTadaBillMst f WHERE f.fromDate = :fromDate"),
    @NamedQuery(name = "FtasTadaBillMst.findByToDate", query = "SELECT f FROM FtasTadaBillMst f WHERE f.toDate = :toDate"),
    @NamedQuery(name = "FtasTadaBillMst.findByTotalDays", query = "SELECT f FROM FtasTadaBillMst f WHERE f.totalDays = :totalDays"),
    @NamedQuery(name = "FtasTadaBillMst.findByPurpose", query = "SELECT f FROM FtasTadaBillMst f WHERE f.purpose = :purpose"),
    @NamedQuery(name = "FtasTadaBillMst.findByAdvance", query = "SELECT f FROM FtasTadaBillMst f WHERE f.advance = :advance"),
    @NamedQuery(name = "FtasTadaBillMst.findByTransportAllow", query = "SELECT f FROM FtasTadaBillMst f WHERE f.transportAllow = :transportAllow"),
    @NamedQuery(name = "FtasTadaBillMst.findByHotel", query = "SELECT f FROM FtasTadaBillMst f WHERE f.hotel = :hotel"),
    @NamedQuery(name = "FtasTadaBillMst.findByLodge", query = "SELECT f FROM FtasTadaBillMst f WHERE f.lodge = :lodge"),
    @NamedQuery(name = "FtasTadaBillMst.findByTotFare", query = "SELECT f FROM FtasTadaBillMst f WHERE f.totFare = :totFare"),
    @NamedQuery(name = "FtasTadaBillMst.findByTotConvExp", query = "SELECT f FROM FtasTadaBillMst f WHERE f.totConvExp = :totConvExp"),
    @NamedQuery(name = "FtasTadaBillMst.findByTotOthExp", query = "SELECT f FROM FtasTadaBillMst f WHERE f.totOthExp = :totOthExp"),
    @NamedQuery(name = "FtasTadaBillMst.findByTotCashAllow", query = "SELECT f FROM FtasTadaBillMst f WHERE f.totCashAllow = :totCashAllow"),
    @NamedQuery(name = "FtasTadaBillMst.findByTotNetClaim", query = "SELECT f FROM FtasTadaBillMst f WHERE f.totNetClaim = :totNetClaim"),
    @NamedQuery(name = "FtasTadaBillMst.findByTotRefund", query = "SELECT f FROM FtasTadaBillMst f WHERE f.totRefund = :totRefund"),
    @NamedQuery(name = "FtasTadaBillMst.findByActnpurp", query = "SELECT f FROM FtasTadaBillMst f WHERE f.actnpurp = :actnpurp"),
    @NamedQuery(name = "FtasTadaBillMst.findByAprvDate", query = "SELECT f FROM FtasTadaBillMst f WHERE f.aprvDate = :aprvDate"),
    @NamedQuery(name = "FtasTadaBillMst.findByAprvcomm", query = "SELECT f FROM FtasTadaBillMst f WHERE f.aprvcomm = :aprvcomm"),
    @NamedQuery(name = "FtasTadaBillMst.findByCreatedt", query = "SELECT f FROM FtasTadaBillMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasTadaBillMst.findByUpdatedt", query = "SELECT f FROM FtasTadaBillMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasTadaBillMst.findBySystemUserid", query = "SELECT f FROM FtasTadaBillMst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FtasTadaBillMst.findByEmpCc", query = "SELECT f FROM FtasTadaBillMst f WHERE f.empCc = :empCc"),
    @NamedQuery(name = "FtasTadaBillMst.findByWpUniqueSrno", query = "SELECT f FROM FtasTadaBillMst f WHERE f.wpUniqueSrno = :wpUniqueSrno")})
public class FtasTadaBillMst implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "TBM_SRG_KEY")
    private BigDecimal tbmSrgKey;
    @Size(max = 20)
    @Column(name = "LEAVE_DESC")
    private String leaveDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date toDate;
    @Column(name = "TOTAL_DAYS")
    private BigDecimal totalDays;
    @Size(max = 4000)
    @Column(name = "PURPOSE")
    private String purpose;
    @Column(name = "ADVANCE")
    private BigDecimal advance;
    @Column(name = "TRANSPORT_ALLOW")
    private BigDecimal transportAllow;
    @Column(name = "HOTEL")
    private BigDecimal hotel;
    @Column(name = "LODGE")
    private BigDecimal lodge;
    @Column(name = "TOT_FARE")
    private BigDecimal totFare;
    @Column(name = "TOT_CONV_EXP")
    private BigDecimal totConvExp;
    @Column(name = "TOT_OTH_EXP")
    private BigDecimal totOthExp;
    @Column(name = "TOT_CASH_ALLOW")
    private BigDecimal totCashAllow;
    @Column(name = "TOT_NET_CLAIM")
    private BigDecimal totNetClaim;
    @Column(name = "TOT_REFUND")
    private BigDecimal totRefund;
    @Size(max = 1)
    @Column(name = "ACTNPURP")
    private String actnpurp;
    @Column(name = "APRV_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date aprvDate;
    @Size(max = 1)
    @Column(name = "APRVCOMM")
    private String aprvcomm;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Column(name = "EMP_CC")
    private BigDecimal empCc;
    @Column(name = "WP_UNIQUE_SRNO")
    private BigDecimal wpUniqueSrno;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ftasTadaBillMst")
    private Collection<FtasTadaOtherDtl> ftasTadaOtherDtlCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "ABS_SRG_KEY", referencedColumnName = "ABS_SRG_KEY")
    @ManyToOne(optional = false)
    private FtasAbsentees absSrgKey;
    @JoinColumn(name = "APRVBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst aprvby;
    @JoinColumn(name = "ACTNBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst actnby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst userId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ftasTadaBillMst")
    private Collection<FtasTadaCashDednDtl> ftasTadaCashDednDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ftasTadaBillMst")
    private Collection<FtasTadaFareDtl> ftasTadaFareDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ftasTadaBillMst")
    private Collection<FtasTadaFacilityDtl> ftasTadaFacilityDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ftasTadaBillMst")
    private Collection<FtasTadaConvExpDtl> ftasTadaConvExpDtlCollection;

    public FtasTadaBillMst() {
    }

    public FtasTadaBillMst(BigDecimal tbmSrgKey) {
        this.tbmSrgKey = tbmSrgKey;
    }

    public FtasTadaBillMst(BigDecimal tbmSrgKey, Date fromDate, Date toDate, Date createdt) {
        this.tbmSrgKey = tbmSrgKey;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.createdt = createdt;
    }

    public BigDecimal getTbmSrgKey() {
        return tbmSrgKey;
    }

    public void setTbmSrgKey(BigDecimal tbmSrgKey) {
        this.tbmSrgKey = tbmSrgKey;
    }

    public String getLeaveDesc() {
        return leaveDesc;
    }

    public void setLeaveDesc(String leaveDesc) {
        this.leaveDesc = leaveDesc;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public BigDecimal getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(BigDecimal totalDays) {
        this.totalDays = totalDays;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public BigDecimal getAdvance() {
        return advance;
    }

    public void setAdvance(BigDecimal advance) {
        this.advance = advance;
    }

    public BigDecimal getTransportAllow() {
        return transportAllow;
    }

    public void setTransportAllow(BigDecimal transportAllow) {
        this.transportAllow = transportAllow;
    }

    public BigDecimal getHotel() {
        return hotel;
    }

    public void setHotel(BigDecimal hotel) {
        this.hotel = hotel;
    }

    public BigDecimal getLodge() {
        return lodge;
    }

    public void setLodge(BigDecimal lodge) {
        this.lodge = lodge;
    }

    public BigDecimal getTotFare() {
        return totFare;
    }

    public void setTotFare(BigDecimal totFare) {
        this.totFare = totFare;
    }

    public BigDecimal getTotConvExp() {
        return totConvExp;
    }

    public void setTotConvExp(BigDecimal totConvExp) {
        this.totConvExp = totConvExp;
    }

    public BigDecimal getTotOthExp() {
        return totOthExp;
    }

    public void setTotOthExp(BigDecimal totOthExp) {
        this.totOthExp = totOthExp;
    }

    public BigDecimal getTotCashAllow() {
        return totCashAllow;
    }

    public void setTotCashAllow(BigDecimal totCashAllow) {
        this.totCashAllow = totCashAllow;
    }

    public BigDecimal getTotNetClaim() {
        return totNetClaim;
    }

    public void setTotNetClaim(BigDecimal totNetClaim) {
        this.totNetClaim = totNetClaim;
    }

    public BigDecimal getTotRefund() {
        return totRefund;
    }

    public void setTotRefund(BigDecimal totRefund) {
        this.totRefund = totRefund;
    }

    public String getActnpurp() {
        return actnpurp;
    }

    public void setActnpurp(String actnpurp) {
        this.actnpurp = actnpurp;
    }

    public Date getAprvDate() {
        return aprvDate;
    }

    public void setAprvDate(Date aprvDate) {
        this.aprvDate = aprvDate;
    }

    public String getAprvcomm() {
        return aprvcomm;
    }

    public void setAprvcomm(String aprvcomm) {
        this.aprvcomm = aprvcomm;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public BigDecimal getEmpCc() {
        return empCc;
    }

    public void setEmpCc(BigDecimal empCc) {
        this.empCc = empCc;
    }

    public BigDecimal getWpUniqueSrno() {
        return wpUniqueSrno;
    }

    public void setWpUniqueSrno(BigDecimal wpUniqueSrno) {
        this.wpUniqueSrno = wpUniqueSrno;
    }

    @XmlTransient
    public Collection<FtasTadaOtherDtl> getFtasTadaOtherDtlCollection() {
        return ftasTadaOtherDtlCollection;
    }

    public void setFtasTadaOtherDtlCollection(Collection<FtasTadaOtherDtl> ftasTadaOtherDtlCollection) {
        this.ftasTadaOtherDtlCollection = ftasTadaOtherDtlCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasAbsentees getAbsSrgKey() {
        return absSrgKey;
    }

    public void setAbsSrgKey(FtasAbsentees absSrgKey) {
        this.absSrgKey = absSrgKey;
    }

    public FhrdEmpmst getAprvby() {
        return aprvby;
    }

    public void setAprvby(FhrdEmpmst aprvby) {
        this.aprvby = aprvby;
    }

    public FhrdEmpmst getActnby() {
        return actnby;
    }

    public void setActnby(FhrdEmpmst actnby) {
        this.actnby = actnby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getUserId() {
        return userId;
    }

    public void setUserId(FhrdEmpmst userId) {
        this.userId = userId;
    }

    @XmlTransient
    public Collection<FtasTadaCashDednDtl> getFtasTadaCashDednDtlCollection() {
        return ftasTadaCashDednDtlCollection;
    }

    public void setFtasTadaCashDednDtlCollection(Collection<FtasTadaCashDednDtl> ftasTadaCashDednDtlCollection) {
        this.ftasTadaCashDednDtlCollection = ftasTadaCashDednDtlCollection;
    }

    @XmlTransient
    public Collection<FtasTadaFareDtl> getFtasTadaFareDtlCollection() {
        return ftasTadaFareDtlCollection;
    }

    public void setFtasTadaFareDtlCollection(Collection<FtasTadaFareDtl> ftasTadaFareDtlCollection) {
        this.ftasTadaFareDtlCollection = ftasTadaFareDtlCollection;
    }

    @XmlTransient
    public Collection<FtasTadaFacilityDtl> getFtasTadaFacilityDtlCollection() {
        return ftasTadaFacilityDtlCollection;
    }

    public void setFtasTadaFacilityDtlCollection(Collection<FtasTadaFacilityDtl> ftasTadaFacilityDtlCollection) {
        this.ftasTadaFacilityDtlCollection = ftasTadaFacilityDtlCollection;
    }

    @XmlTransient
    public Collection<FtasTadaConvExpDtl> getFtasTadaConvExpDtlCollection() {
        return ftasTadaConvExpDtlCollection;
    }

    public void setFtasTadaConvExpDtlCollection(Collection<FtasTadaConvExpDtl> ftasTadaConvExpDtlCollection) {
        this.ftasTadaConvExpDtlCollection = ftasTadaConvExpDtlCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tbmSrgKey != null ? tbmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasTadaBillMst)) {
            return false;
        }
        FtasTadaBillMst other = (FtasTadaBillMst) object;
        if ((this.tbmSrgKey == null && other.tbmSrgKey != null) || (this.tbmSrgKey != null && !this.tbmSrgKey.equals(other.tbmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasTadaBillMst[ tbmSrgKey=" + tbmSrgKey + " ]";
    }
    
}
