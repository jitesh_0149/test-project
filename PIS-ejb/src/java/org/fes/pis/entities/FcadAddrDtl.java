/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FCAD_ADDR_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FcadAddrDtl.findAll", query = "SELECT f FROM FcadAddrDtl f"),
    @NamedQuery(name = "FcadAddrDtl.findByAdAmUniqueSrno", query = "SELECT f FROM FcadAddrDtl f WHERE f.fcadAddrDtlPK.adAmUniqueSrno = :adAmUniqueSrno"),
    @NamedQuery(name = "FcadAddrDtl.findByAdSrno", query = "SELECT f FROM FcadAddrDtl f WHERE f.fcadAddrDtlPK.adSrno = :adSrno"),
    @NamedQuery(name = "FcadAddrDtl.findByAdWorkPlace", query = "SELECT f FROM FcadAddrDtl f WHERE f.adWorkPlace = :adWorkPlace"),
    @NamedQuery(name = "FcadAddrDtl.findByAdAddr1", query = "SELECT f FROM FcadAddrDtl f WHERE f.adAddr1 = :adAddr1"),
    @NamedQuery(name = "FcadAddrDtl.findByAdAddr2", query = "SELECT f FROM FcadAddrDtl f WHERE f.adAddr2 = :adAddr2"),
    @NamedQuery(name = "FcadAddrDtl.findByAdAddr3", query = "SELECT f FROM FcadAddrDtl f WHERE f.adAddr3 = :adAddr3"),
    @NamedQuery(name = "FcadAddrDtl.findByAdCity", query = "SELECT f FROM FcadAddrDtl f WHERE f.adCity = :adCity"),
    @NamedQuery(name = "FcadAddrDtl.findByAdState", query = "SELECT f FROM FcadAddrDtl f WHERE f.adState = :adState"),
    @NamedQuery(name = "FcadAddrDtl.findByAdPincode", query = "SELECT f FROM FcadAddrDtl f WHERE f.adPincode = :adPincode"),
    @NamedQuery(name = "FcadAddrDtl.findByAdCountry", query = "SELECT f FROM FcadAddrDtl f WHERE f.adCountry = :adCountry"),
    @NamedQuery(name = "FcadAddrDtl.findByAdConStatus", query = "SELECT f FROM FcadAddrDtl f WHERE f.adConStatus = :adConStatus"),
    @NamedQuery(name = "FcadAddrDtl.findByAdPresAddr1", query = "SELECT f FROM FcadAddrDtl f WHERE f.adPresAddr1 = :adPresAddr1"),
    @NamedQuery(name = "FcadAddrDtl.findByAdPresAddr2", query = "SELECT f FROM FcadAddrDtl f WHERE f.adPresAddr2 = :adPresAddr2"),
    @NamedQuery(name = "FcadAddrDtl.findByAdPresAddr3", query = "SELECT f FROM FcadAddrDtl f WHERE f.adPresAddr3 = :adPresAddr3"),
    @NamedQuery(name = "FcadAddrDtl.findByAdPresCity", query = "SELECT f FROM FcadAddrDtl f WHERE f.adPresCity = :adPresCity"),
    @NamedQuery(name = "FcadAddrDtl.findByAdPresState", query = "SELECT f FROM FcadAddrDtl f WHERE f.adPresState = :adPresState"),
    @NamedQuery(name = "FcadAddrDtl.findByAdPresPincode", query = "SELECT f FROM FcadAddrDtl f WHERE f.adPresPincode = :adPresPincode"),
    @NamedQuery(name = "FcadAddrDtl.findByAdPresCountry", query = "SELECT f FROM FcadAddrDtl f WHERE f.adPresCountry = :adPresCountry"),
    @NamedQuery(name = "FcadAddrDtl.findByAdPresStatus", query = "SELECT f FROM FcadAddrDtl f WHERE f.adPresStatus = :adPresStatus"),
    @NamedQuery(name = "FcadAddrDtl.findByAdPermAddr1", query = "SELECT f FROM FcadAddrDtl f WHERE f.adPermAddr1 = :adPermAddr1"),
    @NamedQuery(name = "FcadAddrDtl.findByAdPermAddr2", query = "SELECT f FROM FcadAddrDtl f WHERE f.adPermAddr2 = :adPermAddr2"),
    @NamedQuery(name = "FcadAddrDtl.findByAdPermAddr3", query = "SELECT f FROM FcadAddrDtl f WHERE f.adPermAddr3 = :adPermAddr3"),
    @NamedQuery(name = "FcadAddrDtl.findByAdPermCity", query = "SELECT f FROM FcadAddrDtl f WHERE f.adPermCity = :adPermCity"),
    @NamedQuery(name = "FcadAddrDtl.findByAdPermState", query = "SELECT f FROM FcadAddrDtl f WHERE f.adPermState = :adPermState"),
    @NamedQuery(name = "FcadAddrDtl.findByAdPermPincode", query = "SELECT f FROM FcadAddrDtl f WHERE f.adPermPincode = :adPermPincode"),
    @NamedQuery(name = "FcadAddrDtl.findByAdPermCountry", query = "SELECT f FROM FcadAddrDtl f WHERE f.adPermCountry = :adPermCountry"),
    @NamedQuery(name = "FcadAddrDtl.findByAdPermStatus", query = "SELECT f FROM FcadAddrDtl f WHERE f.adPermStatus = :adPermStatus"),
    @NamedQuery(name = "FcadAddrDtl.findByAdEmail", query = "SELECT f FROM FcadAddrDtl f WHERE f.adEmail = :adEmail"),
    @NamedQuery(name = "FcadAddrDtl.findByAdWebsite", query = "SELECT f FROM FcadAddrDtl f WHERE f.adWebsite = :adWebsite"),
    @NamedQuery(name = "FcadAddrDtl.findByAdRemarks1", query = "SELECT f FROM FcadAddrDtl f WHERE f.adRemarks1 = :adRemarks1"),
    @NamedQuery(name = "FcadAddrDtl.findByAdRemarks2", query = "SELECT f FROM FcadAddrDtl f WHERE f.adRemarks2 = :adRemarks2"),
    @NamedQuery(name = "FcadAddrDtl.findByAdDynamicFlag", query = "SELECT f FROM FcadAddrDtl f WHERE f.adDynamicFlag = :adDynamicFlag"),
    @NamedQuery(name = "FcadAddrDtl.findByAdDesignation", query = "SELECT f FROM FcadAddrDtl f WHERE f.adDesignation = :adDesignation"),
    @NamedQuery(name = "FcadAddrDtl.findByAdSalute", query = "SELECT f FROM FcadAddrDtl f WHERE f.adSalute = :adSalute"),
    @NamedQuery(name = "FcadAddrDtl.findByAdDesigsrno", query = "SELECT f FROM FcadAddrDtl f WHERE f.adDesigsrno = :adDesigsrno"),
    @NamedQuery(name = "FcadAddrDtl.findByAdDesigflag", query = "SELECT f FROM FcadAddrDtl f WHERE f.adDesigflag = :adDesigflag"),
    @NamedQuery(name = "FcadAddrDtl.findByAdAddr4", query = "SELECT f FROM FcadAddrDtl f WHERE f.adAddr4 = :adAddr4"),
    @NamedQuery(name = "FcadAddrDtl.findByAdAddr5", query = "SELECT f FROM FcadAddrDtl f WHERE f.adAddr5 = :adAddr5"),
    @NamedQuery(name = "FcadAddrDtl.findByAdAddr6", query = "SELECT f FROM FcadAddrDtl f WHERE f.adAddr6 = :adAddr6"),
    @NamedQuery(name = "FcadAddrDtl.findByAdAddr7", query = "SELECT f FROM FcadAddrDtl f WHERE f.adAddr7 = :adAddr7"),
    @NamedQuery(name = "FcadAddrDtl.findByAdStartDate", query = "SELECT f FROM FcadAddrDtl f WHERE f.adStartDate = :adStartDate"),
    @NamedQuery(name = "FcadAddrDtl.findByAdEndDate", query = "SELECT f FROM FcadAddrDtl f WHERE f.adEndDate = :adEndDate"),
    @NamedQuery(name = "FcadAddrDtl.findByCreatedt", query = "SELECT f FROM FcadAddrDtl f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FcadAddrDtl.findByUpdatedt", query = "SELECT f FROM FcadAddrDtl f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FcadAddrDtl.findBySystemUserid", query = "SELECT f FROM FcadAddrDtl f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FcadAddrDtl.findByExportFlag", query = "SELECT f FROM FcadAddrDtl f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FcadAddrDtl.findByImportFlag", query = "SELECT f FROM FcadAddrDtl f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FcadAddrDtl.findByAdEmailAlt", query = "SELECT f FROM FcadAddrDtl f WHERE f.adEmailAlt = :adEmailAlt"),
    @NamedQuery(name = "FcadAddrDtl.findByAdSourceCont", query = "SELECT f FROM FcadAddrDtl f WHERE f.adSourceCont = :adSourceCont"),
    @NamedQuery(name = "FcadAddrDtl.findByAdPlace", query = "SELECT f FROM FcadAddrDtl f WHERE f.adPlace = :adPlace"),
    @NamedQuery(name = "FcadAddrDtl.findByAdContDate", query = "SELECT f FROM FcadAddrDtl f WHERE f.adContDate = :adContDate"),
    @NamedQuery(name = "FcadAddrDtl.findByAdOccasion", query = "SELECT f FROM FcadAddrDtl f WHERE f.adOccasion = :adOccasion")})
public class FcadAddrDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FcadAddrDtlPK fcadAddrDtlPK;
    @Size(max = 20)
    @Column(name = "AD_WORK_PLACE")
    private String adWorkPlace;
    @Size(max = 200)
    @Column(name = "AD_ADDR_1")
    private String adAddr1;
    @Size(max = 200)
    @Column(name = "AD_ADDR_2")
    private String adAddr2;
    @Size(max = 200)
    @Column(name = "AD_ADDR_3")
    private String adAddr3;
    @Size(max = 100)
    @Column(name = "AD_CITY")
    private String adCity;
    @Size(max = 100)
    @Column(name = "AD_STATE")
    private String adState;
    @Size(max = 15)
    @Column(name = "AD_PINCODE")
    private String adPincode;
    @Size(max = 100)
    @Column(name = "AD_COUNTRY")
    private String adCountry;
    @Size(max = 1)
    @Column(name = "AD_CON_STATUS")
    private String adConStatus;
    @Size(max = 200)
    @Column(name = "AD_PRES_ADDR_1")
    private String adPresAddr1;
    @Size(max = 200)
    @Column(name = "AD_PRES_ADDR_2")
    private String adPresAddr2;
    @Size(max = 200)
    @Column(name = "AD_PRES_ADDR_3")
    private String adPresAddr3;
    @Size(max = 100)
    @Column(name = "AD_PRES_CITY")
    private String adPresCity;
    @Size(max = 100)
    @Column(name = "AD_PRES_STATE")
    private String adPresState;
    @Size(max = 15)
    @Column(name = "AD_PRES_PINCODE")
    private String adPresPincode;
    @Size(max = 100)
    @Column(name = "AD_PRES_COUNTRY")
    private String adPresCountry;
    @Size(max = 1)
    @Column(name = "AD_PRES_STATUS")
    private String adPresStatus;
    @Size(max = 200)
    @Column(name = "AD_PERM_ADDR_1")
    private String adPermAddr1;
    @Size(max = 200)
    @Column(name = "AD_PERM_ADDR_2")
    private String adPermAddr2;
    @Size(max = 200)
    @Column(name = "AD_PERM_ADDR_3")
    private String adPermAddr3;
    @Size(max = 100)
    @Column(name = "AD_PERM_CITY")
    private String adPermCity;
    @Size(max = 100)
    @Column(name = "AD_PERM_STATE")
    private String adPermState;
    @Size(max = 15)
    @Column(name = "AD_PERM_PINCODE")
    private String adPermPincode;
    @Size(max = 100)
    @Column(name = "AD_PERM_COUNTRY")
    private String adPermCountry;
    @Size(max = 1)
    @Column(name = "AD_PERM_STATUS")
    private String adPermStatus;
    @Size(max = 100)
    @Column(name = "AD_EMAIL")
    private String adEmail;
    @Size(max = 100)
    @Column(name = "AD_WEBSITE")
    private String adWebsite;
    @Size(max = 200)
    @Column(name = "AD_REMARKS1")
    private String adRemarks1;
    @Size(max = 200)
    @Column(name = "AD_REMARKS2")
    private String adRemarks2;
    @Size(max = 1)
    @Column(name = "AD_DYNAMIC_FLAG")
    private String adDynamicFlag;
    @Size(max = 100)
    @Column(name = "AD_DESIGNATION")
    private String adDesignation;
    @Size(max = 100)
    @Column(name = "AD_SALUTE")
    private String adSalute;
    @Column(name = "AD_DESIGSRNO")
    private Short adDesigsrno;
    @Size(max = 1)
    @Column(name = "AD_DESIGFLAG")
    private String adDesigflag;
    @Size(max = 200)
    @Column(name = "AD_ADDR_4")
    private String adAddr4;
    @Size(max = 200)
    @Column(name = "AD_ADDR_5")
    private String adAddr5;
    @Size(max = 200)
    @Column(name = "AD_ADDR_6")
    private String adAddr6;
    @Size(max = 200)
    @Column(name = "AD_ADDR_7")
    private String adAddr7;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AD_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date adStartDate;
    @Column(name = "AD_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date adEndDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Size(max = 100)
    @Column(name = "AD_EMAIL_ALT")
    private String adEmailAlt;
    @Size(max = 100)
    @Column(name = "AD_SOURCE_CONT")
    private String adSourceCont;
    @Size(max = 100)
    @Column(name = "AD_PLACE")
    private String adPlace;
    @Column(name = "AD_CONT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date adContDate;
    @Lob
    @Column(name = "AD_SCAN")
    private Serializable adScan;
    @Size(max = 100)
    @Column(name = "AD_OCCASION")
    private String adOccasion;
    @JoinColumn(name = "AD_TRANS_OU", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst adTransOu;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "AD_AM_UNIQUE_SRNO", referencedColumnName = "AM_UNIQUE_SRNO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FcadAddrMst fcadAddrMst;

    public FcadAddrDtl() {
    }

    public FcadAddrDtl(FcadAddrDtlPK fcadAddrDtlPK) {
        this.fcadAddrDtlPK = fcadAddrDtlPK;
    }

    public FcadAddrDtl(FcadAddrDtlPK fcadAddrDtlPK, Date adStartDate, Date createdt) {
        this.fcadAddrDtlPK = fcadAddrDtlPK;
        this.adStartDate = adStartDate;
        this.createdt = createdt;
    }

    public FcadAddrDtl(BigInteger adAmUniqueSrno, int adSrno) {
        this.fcadAddrDtlPK = new FcadAddrDtlPK(adAmUniqueSrno, adSrno);
    }

    public FcadAddrDtlPK getFcadAddrDtlPK() {
        return fcadAddrDtlPK;
    }

    public void setFcadAddrDtlPK(FcadAddrDtlPK fcadAddrDtlPK) {
        this.fcadAddrDtlPK = fcadAddrDtlPK;
    }

    public String getAdWorkPlace() {
        return adWorkPlace;
    }

    public void setAdWorkPlace(String adWorkPlace) {
        this.adWorkPlace = adWorkPlace;
    }

    public String getAdAddr1() {
        return adAddr1;
    }

    public void setAdAddr1(String adAddr1) {
        this.adAddr1 = adAddr1;
    }

    public String getAdAddr2() {
        return adAddr2;
    }

    public void setAdAddr2(String adAddr2) {
        this.adAddr2 = adAddr2;
    }

    public String getAdAddr3() {
        return adAddr3;
    }

    public void setAdAddr3(String adAddr3) {
        this.adAddr3 = adAddr3;
    }

    public String getAdCity() {
        return adCity;
    }

    public void setAdCity(String adCity) {
        this.adCity = adCity;
    }

    public String getAdState() {
        return adState;
    }

    public void setAdState(String adState) {
        this.adState = adState;
    }

    public String getAdPincode() {
        return adPincode;
    }

    public void setAdPincode(String adPincode) {
        this.adPincode = adPincode;
    }

    public String getAdCountry() {
        return adCountry;
    }

    public void setAdCountry(String adCountry) {
        this.adCountry = adCountry;
    }

    public String getAdConStatus() {
        return adConStatus;
    }

    public void setAdConStatus(String adConStatus) {
        this.adConStatus = adConStatus;
    }

    public String getAdPresAddr1() {
        return adPresAddr1;
    }

    public void setAdPresAddr1(String adPresAddr1) {
        this.adPresAddr1 = adPresAddr1;
    }

    public String getAdPresAddr2() {
        return adPresAddr2;
    }

    public void setAdPresAddr2(String adPresAddr2) {
        this.adPresAddr2 = adPresAddr2;
    }

    public String getAdPresAddr3() {
        return adPresAddr3;
    }

    public void setAdPresAddr3(String adPresAddr3) {
        this.adPresAddr3 = adPresAddr3;
    }

    public String getAdPresCity() {
        return adPresCity;
    }

    public void setAdPresCity(String adPresCity) {
        this.adPresCity = adPresCity;
    }

    public String getAdPresState() {
        return adPresState;
    }

    public void setAdPresState(String adPresState) {
        this.adPresState = adPresState;
    }

    public String getAdPresPincode() {
        return adPresPincode;
    }

    public void setAdPresPincode(String adPresPincode) {
        this.adPresPincode = adPresPincode;
    }

    public String getAdPresCountry() {
        return adPresCountry;
    }

    public void setAdPresCountry(String adPresCountry) {
        this.adPresCountry = adPresCountry;
    }

    public String getAdPresStatus() {
        return adPresStatus;
    }

    public void setAdPresStatus(String adPresStatus) {
        this.adPresStatus = adPresStatus;
    }

    public String getAdPermAddr1() {
        return adPermAddr1;
    }

    public void setAdPermAddr1(String adPermAddr1) {
        this.adPermAddr1 = adPermAddr1;
    }

    public String getAdPermAddr2() {
        return adPermAddr2;
    }

    public void setAdPermAddr2(String adPermAddr2) {
        this.adPermAddr2 = adPermAddr2;
    }

    public String getAdPermAddr3() {
        return adPermAddr3;
    }

    public void setAdPermAddr3(String adPermAddr3) {
        this.adPermAddr3 = adPermAddr3;
    }

    public String getAdPermCity() {
        return adPermCity;
    }

    public void setAdPermCity(String adPermCity) {
        this.adPermCity = adPermCity;
    }

    public String getAdPermState() {
        return adPermState;
    }

    public void setAdPermState(String adPermState) {
        this.adPermState = adPermState;
    }

    public String getAdPermPincode() {
        return adPermPincode;
    }

    public void setAdPermPincode(String adPermPincode) {
        this.adPermPincode = adPermPincode;
    }

    public String getAdPermCountry() {
        return adPermCountry;
    }

    public void setAdPermCountry(String adPermCountry) {
        this.adPermCountry = adPermCountry;
    }

    public String getAdPermStatus() {
        return adPermStatus;
    }

    public void setAdPermStatus(String adPermStatus) {
        this.adPermStatus = adPermStatus;
    }

    public String getAdEmail() {
        return adEmail;
    }

    public void setAdEmail(String adEmail) {
        this.adEmail = adEmail;
    }

    public String getAdWebsite() {
        return adWebsite;
    }

    public void setAdWebsite(String adWebsite) {
        this.adWebsite = adWebsite;
    }

    public String getAdRemarks1() {
        return adRemarks1;
    }

    public void setAdRemarks1(String adRemarks1) {
        this.adRemarks1 = adRemarks1;
    }

    public String getAdRemarks2() {
        return adRemarks2;
    }

    public void setAdRemarks2(String adRemarks2) {
        this.adRemarks2 = adRemarks2;
    }

    public String getAdDynamicFlag() {
        return adDynamicFlag;
    }

    public void setAdDynamicFlag(String adDynamicFlag) {
        this.adDynamicFlag = adDynamicFlag;
    }

    public String getAdDesignation() {
        return adDesignation;
    }

    public void setAdDesignation(String adDesignation) {
        this.adDesignation = adDesignation;
    }

    public String getAdSalute() {
        return adSalute;
    }

    public void setAdSalute(String adSalute) {
        this.adSalute = adSalute;
    }

    public Short getAdDesigsrno() {
        return adDesigsrno;
    }

    public void setAdDesigsrno(Short adDesigsrno) {
        this.adDesigsrno = adDesigsrno;
    }

    public String getAdDesigflag() {
        return adDesigflag;
    }

    public void setAdDesigflag(String adDesigflag) {
        this.adDesigflag = adDesigflag;
    }

    public String getAdAddr4() {
        return adAddr4;
    }

    public void setAdAddr4(String adAddr4) {
        this.adAddr4 = adAddr4;
    }

    public String getAdAddr5() {
        return adAddr5;
    }

    public void setAdAddr5(String adAddr5) {
        this.adAddr5 = adAddr5;
    }

    public String getAdAddr6() {
        return adAddr6;
    }

    public void setAdAddr6(String adAddr6) {
        this.adAddr6 = adAddr6;
    }

    public String getAdAddr7() {
        return adAddr7;
    }

    public void setAdAddr7(String adAddr7) {
        this.adAddr7 = adAddr7;
    }

    public Date getAdStartDate() {
        return adStartDate;
    }

    public void setAdStartDate(Date adStartDate) {
        this.adStartDate = adStartDate;
    }

    public Date getAdEndDate() {
        return adEndDate;
    }

    public void setAdEndDate(Date adEndDate) {
        this.adEndDate = adEndDate;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public String getAdEmailAlt() {
        return adEmailAlt;
    }

    public void setAdEmailAlt(String adEmailAlt) {
        this.adEmailAlt = adEmailAlt;
    }

    public String getAdSourceCont() {
        return adSourceCont;
    }

    public void setAdSourceCont(String adSourceCont) {
        this.adSourceCont = adSourceCont;
    }

    public String getAdPlace() {
        return adPlace;
    }

    public void setAdPlace(String adPlace) {
        this.adPlace = adPlace;
    }

    public Date getAdContDate() {
        return adContDate;
    }

    public void setAdContDate(Date adContDate) {
        this.adContDate = adContDate;
    }

    public Serializable getAdScan() {
        return adScan;
    }

    public void setAdScan(Serializable adScan) {
        this.adScan = adScan;
    }

    public String getAdOccasion() {
        return adOccasion;
    }

    public void setAdOccasion(String adOccasion) {
        this.adOccasion = adOccasion;
    }

    public SystOrgUnitMst getAdTransOu() {
        return adTransOu;
    }

    public void setAdTransOu(SystOrgUnitMst adTransOu) {
        this.adTransOu = adTransOu;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FcadAddrMst getFcadAddrMst() {
        return fcadAddrMst;
    }

    public void setFcadAddrMst(FcadAddrMst fcadAddrMst) {
        this.fcadAddrMst = fcadAddrMst;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fcadAddrDtlPK != null ? fcadAddrDtlPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FcadAddrDtl)) {
            return false;
        }
        FcadAddrDtl other = (FcadAddrDtl) object;
        if ((this.fcadAddrDtlPK == null && other.fcadAddrDtlPK != null) || (this.fcadAddrDtlPK != null && !this.fcadAddrDtlPK.equals(other.fcadAddrDtlPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FcadAddrDtl[ fcadAddrDtlPK=" + fcadAddrDtlPK + " ]";
    }
    
}
