/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_PAYTRANS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdPaytrans.findAll", query = "SELECT f FROM FhrdPaytrans f"),
    @NamedQuery(name = "FhrdPaytrans.findByOrgSalaryFromDate", query = "SELECT f FROM FhrdPaytrans f WHERE f.orgSalaryFromDate = :orgSalaryFromDate"),
    @NamedQuery(name = "FhrdPaytrans.findByEmpSalaryFromDate", query = "SELECT f FROM FhrdPaytrans f WHERE f.empSalaryFromDate = :empSalaryFromDate"),
    @NamedQuery(name = "FhrdPaytrans.findByEmpSalaryToDate", query = "SELECT f FROM FhrdPaytrans f WHERE f.empSalaryToDate = :empSalaryToDate"),
    @NamedQuery(name = "FhrdPaytrans.findByOrgSalaryToDate", query = "SELECT f FROM FhrdPaytrans f WHERE f.orgSalaryToDate = :orgSalaryToDate"),
    @NamedQuery(name = "FhrdPaytrans.findByConsiderAsAttendance", query = "SELECT f FROM FhrdPaytrans f WHERE f.considerAsAttendance = :considerAsAttendance"),
    @NamedQuery(name = "FhrdPaytrans.findByEmpNonPayableDays", query = "SELECT f FROM FhrdPaytrans f WHERE f.empNonPayableDays = :empNonPayableDays"),
    @NamedQuery(name = "FhrdPaytrans.findByOrgPayableDays", query = "SELECT f FROM FhrdPaytrans f WHERE f.orgPayableDays = :orgPayableDays"),
    @NamedQuery(name = "FhrdPaytrans.findByOrgWorkingDays", query = "SELECT f FROM FhrdPaytrans f WHERE f.orgWorkingDays = :orgWorkingDays"),
    @NamedQuery(name = "FhrdPaytrans.findByEmpPayableDays", query = "SELECT f FROM FhrdPaytrans f WHERE f.empPayableDays = :empPayableDays"),
    @NamedQuery(name = "FhrdPaytrans.findByEmpWorkingDays", query = "SELECT f FROM FhrdPaytrans f WHERE f.empWorkingDays = :empWorkingDays"),
    @NamedQuery(name = "FhrdPaytrans.findByTotalEarning", query = "SELECT f FROM FhrdPaytrans f WHERE f.totalEarning = :totalEarning"),
    @NamedQuery(name = "FhrdPaytrans.findByTotalDeduction", query = "SELECT f FROM FhrdPaytrans f WHERE f.totalDeduction = :totalDeduction"),
    @NamedQuery(name = "FhrdPaytrans.findByNetPay", query = "SELECT f FROM FhrdPaytrans f WHERE f.netPay = :netPay"),
    @NamedQuery(name = "FhrdPaytrans.findByGrossPay", query = "SELECT f FROM FhrdPaytrans f WHERE f.grossPay = :grossPay"),
    @NamedQuery(name = "FhrdPaytrans.findByActualAttendance", query = "SELECT f FROM FhrdPaytrans f WHERE f.actualAttendance = :actualAttendance"),
    @NamedQuery(name = "FhrdPaytrans.findByNoOfHolidays", query = "SELECT f FROM FhrdPaytrans f WHERE f.noOfHolidays = :noOfHolidays"),
    @NamedQuery(name = "FhrdPaytrans.findByRecalculateFlag", query = "SELECT f FROM FhrdPaytrans f WHERE f.recalculateFlag = :recalculateFlag"),
    @NamedQuery(name = "FhrdPaytrans.findByOrgAttendanceFromDate", query = "SELECT f FROM FhrdPaytrans f WHERE f.orgAttendanceFromDate = :orgAttendanceFromDate"),
    @NamedQuery(name = "FhrdPaytrans.findByOrgAttendanceToDate", query = "SELECT f FROM FhrdPaytrans f WHERE f.orgAttendanceToDate = :orgAttendanceToDate"),
    @NamedQuery(name = "FhrdPaytrans.findByCreatedt", query = "SELECT f FROM FhrdPaytrans f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdPaytrans.findByUpdatedt", query = "SELECT f FROM FhrdPaytrans f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdPaytrans.findBySystemUserid", query = "SELECT f FROM FhrdPaytrans f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdPaytrans.findByExportFlag", query = "SELECT f FROM FhrdPaytrans f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdPaytrans.findByImportFlag", query = "SELECT f FROM FhrdPaytrans f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdPaytrans.findByPtSrgKey", query = "SELECT f FROM FhrdPaytrans f WHERE f.ptSrgKey = :ptSrgKey"),
    @NamedQuery(name = "FhrdPaytrans.findByTotalReimbursment", query = "SELECT f FROM FhrdPaytrans f WHERE f.totalReimbursment = :totalReimbursment"),
    @NamedQuery(name = "FhrdPaytrans.findByTotalEmployerContri", query = "SELECT f FROM FhrdPaytrans f WHERE f.totalEmployerContri = :totalEmployerContri"),
    @NamedQuery(name = "FhrdPaytrans.findByTranYear", query = "SELECT f FROM FhrdPaytrans f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FhrdPaytrans.findBySalaryTransactionType", query = "SELECT f FROM FhrdPaytrans f WHERE f.salaryTransactionType = :salaryTransactionType"),
    @NamedQuery(name = "FhrdPaytrans.findByTotalCto", query = "SELECT f FROM FhrdPaytrans f WHERE f.totalCto = :totalCto"),
    @NamedQuery(name = "FhrdPaytrans.findByTotalPay", query = "SELECT f FROM FhrdPaytrans f WHERE f.totalPay = :totalPay"),
    @NamedQuery(name = "FhrdPaytrans.findBySalarySrgKey", query = "SELECT f FROM FhrdPaytrans f WHERE f.salarySrgKey = :salarySrgKey"),
    @NamedQuery(name = "FhrdPaytrans.findByNoOfLeaveEncash", query = "SELECT f FROM FhrdPaytrans f WHERE f.noOfLeaveEncash = :noOfLeaveEncash")})
public class FhrdPaytrans implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_SALARY_FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgSalaryFromDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_SALARY_FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date empSalaryFromDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_SALARY_TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date empSalaryToDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_SALARY_TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgSalaryToDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONSIDER_AS_ATTENDANCE")
    private BigDecimal considerAsAttendance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_NON_PAYABLE_DAYS")
    private BigDecimal empNonPayableDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_PAYABLE_DAYS")
    private BigDecimal orgPayableDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_WORKING_DAYS")
    private BigDecimal orgWorkingDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_PAYABLE_DAYS")
    private BigDecimal empPayableDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_WORKING_DAYS")
    private BigDecimal empWorkingDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_EARNING")
    private BigInteger totalEarning;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_DEDUCTION")
    private BigInteger totalDeduction;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NET_PAY")
    private BigInteger netPay;
    @Basic(optional = false)
    @NotNull
    @Column(name = "GROSS_PAY")
    private BigInteger grossPay;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTUAL_ATTENDANCE")
    private BigDecimal actualAttendance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NO_OF_HOLIDAYS")
    private BigDecimal noOfHolidays;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "RECALCULATE_FLAG")
    private String recalculateFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_ATTENDANCE_FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgAttendanceFromDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_ATTENDANCE_TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgAttendanceToDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PT_SRG_KEY")
    private BigDecimal ptSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_REIMBURSMENT")
    private BigInteger totalReimbursment;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_EMPLOYER_CONTRI")
    private BigInteger totalEmployerContri;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "SALARY_TRANSACTION_TYPE")
    private String salaryTransactionType;
    @Column(name = "TOTAL_CTO")
    private BigInteger totalCto;
    @Column(name = "TOTAL_PAY")
    private BigInteger totalPay;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SALARY_SRG_KEY")
    private BigInteger salarySrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NO_OF_LEAVE_ENCASH")
    private BigDecimal noOfLeaveEncash;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst userId;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "DSGM_SRG_KEY", referencedColumnName = "DSGM_SRG_KEY")
    @ManyToOne
    private FhrdDesigmst dsgmSrgKey;

    public FhrdPaytrans() {
    }

    public FhrdPaytrans(BigDecimal ptSrgKey) {
        this.ptSrgKey = ptSrgKey;
    }

    public FhrdPaytrans(BigDecimal ptSrgKey, Date orgSalaryFromDate, Date empSalaryFromDate, Date empSalaryToDate, Date orgSalaryToDate, BigDecimal considerAsAttendance, BigDecimal empNonPayableDays, BigDecimal orgPayableDays, BigDecimal orgWorkingDays, BigDecimal empPayableDays, BigDecimal empWorkingDays, BigInteger totalEarning, BigInteger totalDeduction, BigInteger netPay, BigInteger grossPay, BigDecimal actualAttendance, BigDecimal noOfHolidays, String recalculateFlag, Date orgAttendanceFromDate, Date orgAttendanceToDate, Date createdt, BigInteger totalReimbursment, BigInteger totalEmployerContri, short tranYear, String salaryTransactionType, BigInteger salarySrgKey, BigDecimal noOfLeaveEncash) {
        this.ptSrgKey = ptSrgKey;
        this.orgSalaryFromDate = orgSalaryFromDate;
        this.empSalaryFromDate = empSalaryFromDate;
        this.empSalaryToDate = empSalaryToDate;
        this.orgSalaryToDate = orgSalaryToDate;
        this.considerAsAttendance = considerAsAttendance;
        this.empNonPayableDays = empNonPayableDays;
        this.orgPayableDays = orgPayableDays;
        this.orgWorkingDays = orgWorkingDays;
        this.empPayableDays = empPayableDays;
        this.empWorkingDays = empWorkingDays;
        this.totalEarning = totalEarning;
        this.totalDeduction = totalDeduction;
        this.netPay = netPay;
        this.grossPay = grossPay;
        this.actualAttendance = actualAttendance;
        this.noOfHolidays = noOfHolidays;
        this.recalculateFlag = recalculateFlag;
        this.orgAttendanceFromDate = orgAttendanceFromDate;
        this.orgAttendanceToDate = orgAttendanceToDate;
        this.createdt = createdt;
        this.totalReimbursment = totalReimbursment;
        this.totalEmployerContri = totalEmployerContri;
        this.tranYear = tranYear;
        this.salaryTransactionType = salaryTransactionType;
        this.salarySrgKey = salarySrgKey;
        this.noOfLeaveEncash = noOfLeaveEncash;
    }

    public Date getOrgSalaryFromDate() {
        return orgSalaryFromDate;
    }

    public void setOrgSalaryFromDate(Date orgSalaryFromDate) {
        this.orgSalaryFromDate = orgSalaryFromDate;
    }

    public Date getEmpSalaryFromDate() {
        return empSalaryFromDate;
    }

    public void setEmpSalaryFromDate(Date empSalaryFromDate) {
        this.empSalaryFromDate = empSalaryFromDate;
    }

    public Date getEmpSalaryToDate() {
        return empSalaryToDate;
    }

    public void setEmpSalaryToDate(Date empSalaryToDate) {
        this.empSalaryToDate = empSalaryToDate;
    }

    public Date getOrgSalaryToDate() {
        return orgSalaryToDate;
    }

    public void setOrgSalaryToDate(Date orgSalaryToDate) {
        this.orgSalaryToDate = orgSalaryToDate;
    }

    public BigDecimal getConsiderAsAttendance() {
        return considerAsAttendance;
    }

    public void setConsiderAsAttendance(BigDecimal considerAsAttendance) {
        this.considerAsAttendance = considerAsAttendance;
    }

    public BigDecimal getEmpNonPayableDays() {
        return empNonPayableDays;
    }

    public void setEmpNonPayableDays(BigDecimal empNonPayableDays) {
        this.empNonPayableDays = empNonPayableDays;
    }

    public BigDecimal getOrgPayableDays() {
        return orgPayableDays;
    }

    public void setOrgPayableDays(BigDecimal orgPayableDays) {
        this.orgPayableDays = orgPayableDays;
    }

    public BigDecimal getOrgWorkingDays() {
        return orgWorkingDays;
    }

    public void setOrgWorkingDays(BigDecimal orgWorkingDays) {
        this.orgWorkingDays = orgWorkingDays;
    }

    public BigDecimal getEmpPayableDays() {
        return empPayableDays;
    }

    public void setEmpPayableDays(BigDecimal empPayableDays) {
        this.empPayableDays = empPayableDays;
    }

    public BigDecimal getEmpWorkingDays() {
        return empWorkingDays;
    }

    public void setEmpWorkingDays(BigDecimal empWorkingDays) {
        this.empWorkingDays = empWorkingDays;
    }

    public BigInteger getTotalEarning() {
        return totalEarning;
    }

    public void setTotalEarning(BigInteger totalEarning) {
        this.totalEarning = totalEarning;
    }

    public BigInteger getTotalDeduction() {
        return totalDeduction;
    }

    public void setTotalDeduction(BigInteger totalDeduction) {
        this.totalDeduction = totalDeduction;
    }

    public BigInteger getNetPay() {
        return netPay;
    }

    public void setNetPay(BigInteger netPay) {
        this.netPay = netPay;
    }

    public BigInteger getGrossPay() {
        return grossPay;
    }

    public void setGrossPay(BigInteger grossPay) {
        this.grossPay = grossPay;
    }

    public BigDecimal getActualAttendance() {
        return actualAttendance;
    }

    public void setActualAttendance(BigDecimal actualAttendance) {
        this.actualAttendance = actualAttendance;
    }

    public BigDecimal getNoOfHolidays() {
        return noOfHolidays;
    }

    public void setNoOfHolidays(BigDecimal noOfHolidays) {
        this.noOfHolidays = noOfHolidays;
    }

    public String getRecalculateFlag() {
        return recalculateFlag;
    }

    public void setRecalculateFlag(String recalculateFlag) {
        this.recalculateFlag = recalculateFlag;
    }

    public Date getOrgAttendanceFromDate() {
        return orgAttendanceFromDate;
    }

    public void setOrgAttendanceFromDate(Date orgAttendanceFromDate) {
        this.orgAttendanceFromDate = orgAttendanceFromDate;
    }

    public Date getOrgAttendanceToDate() {
        return orgAttendanceToDate;
    }

    public void setOrgAttendanceToDate(Date orgAttendanceToDate) {
        this.orgAttendanceToDate = orgAttendanceToDate;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public BigDecimal getPtSrgKey() {
        return ptSrgKey;
    }

    public void setPtSrgKey(BigDecimal ptSrgKey) {
        this.ptSrgKey = ptSrgKey;
    }

    public BigInteger getTotalReimbursment() {
        return totalReimbursment;
    }

    public void setTotalReimbursment(BigInteger totalReimbursment) {
        this.totalReimbursment = totalReimbursment;
    }

    public BigInteger getTotalEmployerContri() {
        return totalEmployerContri;
    }

    public void setTotalEmployerContri(BigInteger totalEmployerContri) {
        this.totalEmployerContri = totalEmployerContri;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public String getSalaryTransactionType() {
        return salaryTransactionType;
    }

    public void setSalaryTransactionType(String salaryTransactionType) {
        this.salaryTransactionType = salaryTransactionType;
    }

    public BigInteger getTotalCto() {
        return totalCto;
    }

    public void setTotalCto(BigInteger totalCto) {
        this.totalCto = totalCto;
    }

    public BigInteger getTotalPay() {
        return totalPay;
    }

    public void setTotalPay(BigInteger totalPay) {
        this.totalPay = totalPay;
    }

    public BigInteger getSalarySrgKey() {
        return salarySrgKey;
    }

    public void setSalarySrgKey(BigInteger salarySrgKey) {
        this.salarySrgKey = salarySrgKey;
    }

    public BigDecimal getNoOfLeaveEncash() {
        return noOfLeaveEncash;
    }

    public void setNoOfLeaveEncash(BigDecimal noOfLeaveEncash) {
        this.noOfLeaveEncash = noOfLeaveEncash;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getUserId() {
        return userId;
    }

    public void setUserId(FhrdEmpmst userId) {
        this.userId = userId;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdDesigmst getDsgmSrgKey() {
        return dsgmSrgKey;
    }

    public void setDsgmSrgKey(FhrdDesigmst dsgmSrgKey) {
        this.dsgmSrgKey = dsgmSrgKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ptSrgKey != null ? ptSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdPaytrans)) {
            return false;
        }
        FhrdPaytrans other = (FhrdPaytrans) object;
        if ((this.ptSrgKey == null && other.ptSrgKey != null) || (this.ptSrgKey != null && !this.ptSrgKey.equals(other.ptSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdPaytrans[ ptSrgKey=" + ptSrgKey + " ]";
    }
    
}
