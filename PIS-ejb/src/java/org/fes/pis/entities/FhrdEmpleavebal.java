/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_EMPLEAVEBAL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdEmpleavebal.findAll", query = "SELECT f FROM FhrdEmpleavebal f"),
    @NamedQuery(name = "FhrdEmpleavebal.findByLeavecd", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.leavecd = :leavecd"),
    @NamedQuery(name = "FhrdEmpleavebal.findByRulesrno", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.rulesrno = :rulesrno"),
    @NamedQuery(name = "FhrdEmpleavebal.findByStartDate", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdEmpleavebal.findByOpeningDate", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.openingDate = :openingDate"),
    @NamedQuery(name = "FhrdEmpleavebal.findByClosingDate", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.closingDate = :closingDate"),
    @NamedQuery(name = "FhrdEmpleavebal.findByEndDate", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FhrdEmpleavebal.findByProposedOpeningbal", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.proposedOpeningbal = :proposedOpeningbal"),
    @NamedQuery(name = "FhrdEmpleavebal.findByOpeningBal", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.openingBal = :openingBal"),
    @NamedQuery(name = "FhrdEmpleavebal.findByEarnedBal", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.earnedBal = :earnedBal"),
    @NamedQuery(name = "FhrdEmpleavebal.findByAddNewLeavebal", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.addNewLeavebal = :addNewLeavebal"),
    @NamedQuery(name = "FhrdEmpleavebal.findByLeavebalCarryforward", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.leavebalCarryforward = :leavebalCarryforward"),
    @NamedQuery(name = "FhrdEmpleavebal.findByConsumedBal", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.consumedBal = :consumedBal"),
    @NamedQuery(name = "FhrdEmpleavebal.findByLeaveBalLaps", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.leaveBalLaps = :leaveBalLaps"),
    @NamedQuery(name = "FhrdEmpleavebal.findByNoOfLeaveEncash", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.noOfLeaveEncash = :noOfLeaveEncash"),
    @NamedQuery(name = "FhrdEmpleavebal.findByCurrentBal", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.currentBal = :currentBal"),
    @NamedQuery(name = "FhrdEmpleavebal.findByClosingBal", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.closingBal = :closingBal"),
    @NamedQuery(name = "FhrdEmpleavebal.findByAppliBalPipe", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.appliBalPipe = :appliBalPipe"),
    @NamedQuery(name = "FhrdEmpleavebal.findByCancPipeBal", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.cancPipeBal = :cancPipeBal"),
    @NamedQuery(name = "FhrdEmpleavebal.findByConsumedFreq", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.consumedFreq = :consumedFreq"),
    @NamedQuery(name = "FhrdEmpleavebal.findByCreatedt", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdEmpleavebal.findByUpdatedt", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdEmpleavebal.findBySystemUserid", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdEmpleavebal.findByExportFlag", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdEmpleavebal.findByImportFlag", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdEmpleavebal.findByEmplbSrgKey", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.emplbSrgKey = :emplbSrgKey"),
    @NamedQuery(name = "FhrdEmpleavebal.findByBalCheckedInAppliFlag", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.balCheckedInAppliFlag = :balCheckedInAppliFlag"),
    @NamedQuery(name = "FhrdEmpleavebal.findByParentEmplbSrgKey", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.parentEmplbSrgKey = :parentEmplbSrgKey"),
    @NamedQuery(name = "FhrdEmpleavebal.findBySrno", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.srno = :srno"),
    @NamedQuery(name = "FhrdEmpleavebal.findByTranYear", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FhrdEmpleavebal.findByLeaveBalLapsPipe", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.leaveBalLapsPipe = :leaveBalLapsPipe"),
    @NamedQuery(name = "FhrdEmpleavebal.findByFirstTimeEditFlag", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.firstTimeEditFlag = :firstTimeEditFlag"),
    @NamedQuery(name = "FhrdEmpleavebal.findByNetLeaveBalance", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.netLeaveBalance = :netLeaveBalance"),
    @NamedQuery(name = "FhrdEmpleavebal.findByTotalAddNewLeavebal", query = "SELECT f FROM FhrdEmpleavebal f WHERE f.totalAddNewLeavebal = :totalAddNewLeavebal")})
public class FhrdEmpleavebal implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "LEAVECD")
    private String leavecd;
    @Column(name = "RULESRNO")
    private Integer rulesrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OPENING_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date openingDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CLOSING_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date closingDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "PROPOSED_OPENINGBAL")
    private BigDecimal proposedOpeningbal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OPENING_BAL")
    private BigDecimal openingBal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EARNED_BAL")
    private BigDecimal earnedBal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ADD_NEW_LEAVEBAL")
    private BigDecimal addNewLeavebal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LEAVEBAL_CARRYFORWARD")
    private BigDecimal leavebalCarryforward;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONSUMED_BAL")
    private BigDecimal consumedBal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LEAVE_BAL_LAPS")
    private BigDecimal leaveBalLaps;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NO_OF_LEAVE_ENCASH")
    private BigDecimal noOfLeaveEncash;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CURRENT_BAL")
    private BigDecimal currentBal;
    @Column(name = "CLOSING_BAL")
    private BigDecimal closingBal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APPLI_BAL_PIPE")
    private BigDecimal appliBalPipe;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CANC_PIPE_BAL")
    private BigDecimal cancPipeBal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONSUMED_FREQ")
    private long consumedFreq;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMPLB_SRG_KEY")
    private BigDecimal emplbSrgKey;
    @Size(max = 1)
    @Column(name = "BAL_CHECKED_IN_APPLI_FLAG")
    private String balCheckedInAppliFlag;
    @Column(name = "PARENT_EMPLB_SRG_KEY")
    private BigDecimal parentEmplbSrgKey;
    @Column(name = "SRNO")
    private Integer srno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LEAVE_BAL_LAPS_PIPE")
    private BigDecimal leaveBalLapsPipe;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "FIRST_TIME_EDIT_FLAG")
    private String firstTimeEditFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NET_LEAVE_BALANCE")
    private BigDecimal netLeaveBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_ADD_NEW_LEAVEBAL")
    private BigDecimal totalAddNewLeavebal;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "emplbSrgKey")
    private Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection;
    @OneToMany(mappedBy = "childEmplbSrgKey")
    private Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "actualEmplbSrgKey")
    private Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection2;
    @OneToMany(mappedBy = "parentEmplbSrgKey")
    private Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection3;
    @OneToMany(mappedBy = "ttrEmplbSrgKey")
    private Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection4;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "emplbSrgKey")
    private Collection<FhrdPaytransleaveDtl> fhrdPaytransleaveDtlCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne
    private FtasLeavemst lmSrgKey;
    @JoinColumn(name = "LR_SRG_KEY", referencedColumnName = "LR_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdLeaverule lrSrgKey;
    @JoinColumn(name = "PARENT_LR_SRG_KEY", referencedColumnName = "LR_SRG_KEY")
    @ManyToOne
    private FhrdLeaverule parentLrSrgKey;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst userId;
    @JoinColumn(name = "PARENT_ELR_SRG_KEY", referencedColumnName = "ELR_SRG_KEY")
    @ManyToOne
    private FhrdEmpleaverule parentElrSrgKey;
    @JoinColumn(name = "ELR_SRG_KEY", referencedColumnName = "ELR_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdEmpleaverule elrSrgKey;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "emplbSrgKey")
    private Collection<FhrdEmpEncash> fhrdEmpEncashCollection;

    public FhrdEmpleavebal() {
    }

    public FhrdEmpleavebal(BigDecimal emplbSrgKey) {
        this.emplbSrgKey = emplbSrgKey;
    }

    public FhrdEmpleavebal(BigDecimal emplbSrgKey, String leavecd, Date startDate, Date openingDate, Date closingDate, BigDecimal proposedOpeningbal, BigDecimal openingBal, BigDecimal earnedBal, BigDecimal addNewLeavebal, BigDecimal leavebalCarryforward, BigDecimal consumedBal, BigDecimal leaveBalLaps, BigDecimal noOfLeaveEncash, BigDecimal currentBal, BigDecimal appliBalPipe, BigDecimal cancPipeBal, long consumedFreq, Date createdt, short tranYear, BigDecimal leaveBalLapsPipe, String firstTimeEditFlag, BigDecimal netLeaveBalance, BigDecimal totalAddNewLeavebal) {
        this.emplbSrgKey = emplbSrgKey;
        this.leavecd = leavecd;
        this.startDate = startDate;
        this.openingDate = openingDate;
        this.closingDate = closingDate;
        this.proposedOpeningbal = proposedOpeningbal;
        this.openingBal = openingBal;
        this.earnedBal = earnedBal;
        this.addNewLeavebal = addNewLeavebal;
        this.leavebalCarryforward = leavebalCarryforward;
        this.consumedBal = consumedBal;
        this.leaveBalLaps = leaveBalLaps;
        this.noOfLeaveEncash = noOfLeaveEncash;
        this.currentBal = currentBal;
        this.appliBalPipe = appliBalPipe;
        this.cancPipeBal = cancPipeBal;
        this.consumedFreq = consumedFreq;
        this.createdt = createdt;
        this.tranYear = tranYear;
        this.leaveBalLapsPipe = leaveBalLapsPipe;
        this.firstTimeEditFlag = firstTimeEditFlag;
        this.netLeaveBalance = netLeaveBalance;
        this.totalAddNewLeavebal = totalAddNewLeavebal;
    }

    public String getLeavecd() {
        return leavecd;
    }

    public void setLeavecd(String leavecd) {
        this.leavecd = leavecd;
    }

    public Integer getRulesrno() {
        return rulesrno;
    }

    public void setRulesrno(Integer rulesrno) {
        this.rulesrno = rulesrno;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(Date openingDate) {
        this.openingDate = openingDate;
    }

    public Date getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(Date closingDate) {
        this.closingDate = closingDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getProposedOpeningbal() {
        return proposedOpeningbal;
    }

    public void setProposedOpeningbal(BigDecimal proposedOpeningbal) {
        this.proposedOpeningbal = proposedOpeningbal;
    }

    public BigDecimal getOpeningBal() {
        return openingBal;
    }

    public void setOpeningBal(BigDecimal openingBal) {
        this.openingBal = openingBal;
    }

    public BigDecimal getEarnedBal() {
        return earnedBal;
    }

    public void setEarnedBal(BigDecimal earnedBal) {
        this.earnedBal = earnedBal;
    }

    public BigDecimal getAddNewLeavebal() {
        return addNewLeavebal;
    }

    public void setAddNewLeavebal(BigDecimal addNewLeavebal) {
        this.addNewLeavebal = addNewLeavebal;
    }

    public BigDecimal getLeavebalCarryforward() {
        return leavebalCarryforward;
    }

    public void setLeavebalCarryforward(BigDecimal leavebalCarryforward) {
        this.leavebalCarryforward = leavebalCarryforward;
    }

    public BigDecimal getConsumedBal() {
        return consumedBal;
    }

    public void setConsumedBal(BigDecimal consumedBal) {
        this.consumedBal = consumedBal;
    }

    public BigDecimal getLeaveBalLaps() {
        return leaveBalLaps;
    }

    public void setLeaveBalLaps(BigDecimal leaveBalLaps) {
        this.leaveBalLaps = leaveBalLaps;
    }

    public BigDecimal getNoOfLeaveEncash() {
        return noOfLeaveEncash;
    }

    public void setNoOfLeaveEncash(BigDecimal noOfLeaveEncash) {
        this.noOfLeaveEncash = noOfLeaveEncash;
    }

    public BigDecimal getCurrentBal() {
        return currentBal;
    }

    public void setCurrentBal(BigDecimal currentBal) {
        this.currentBal = currentBal;
    }

    public BigDecimal getClosingBal() {
        return closingBal;
    }

    public void setClosingBal(BigDecimal closingBal) {
        this.closingBal = closingBal;
    }

    public BigDecimal getAppliBalPipe() {
        return appliBalPipe;
    }

    public void setAppliBalPipe(BigDecimal appliBalPipe) {
        this.appliBalPipe = appliBalPipe;
    }

    public BigDecimal getCancPipeBal() {
        return cancPipeBal;
    }

    public void setCancPipeBal(BigDecimal cancPipeBal) {
        this.cancPipeBal = cancPipeBal;
    }

    public long getConsumedFreq() {
        return consumedFreq;
    }

    public void setConsumedFreq(long consumedFreq) {
        this.consumedFreq = consumedFreq;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public BigDecimal getEmplbSrgKey() {
        return emplbSrgKey;
    }

    public void setEmplbSrgKey(BigDecimal emplbSrgKey) {
        this.emplbSrgKey = emplbSrgKey;
    }

    public String getBalCheckedInAppliFlag() {
        return balCheckedInAppliFlag;
    }

    public void setBalCheckedInAppliFlag(String balCheckedInAppliFlag) {
        this.balCheckedInAppliFlag = balCheckedInAppliFlag;
    }

    public BigDecimal getParentEmplbSrgKey() {
        return parentEmplbSrgKey;
    }

    public void setParentEmplbSrgKey(BigDecimal parentEmplbSrgKey) {
        this.parentEmplbSrgKey = parentEmplbSrgKey;
    }

    public Integer getSrno() {
        return srno;
    }

    public void setSrno(Integer srno) {
        this.srno = srno;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public BigDecimal getLeaveBalLapsPipe() {
        return leaveBalLapsPipe;
    }

    public void setLeaveBalLapsPipe(BigDecimal leaveBalLapsPipe) {
        this.leaveBalLapsPipe = leaveBalLapsPipe;
    }

    public String getFirstTimeEditFlag() {
        return firstTimeEditFlag;
    }

    public void setFirstTimeEditFlag(String firstTimeEditFlag) {
        this.firstTimeEditFlag = firstTimeEditFlag;
    }

    public BigDecimal getNetLeaveBalance() {
        return netLeaveBalance;
    }

    public void setNetLeaveBalance(BigDecimal netLeaveBalance) {
        this.netLeaveBalance = netLeaveBalance;
    }

    public BigDecimal getTotalAddNewLeavebal() {
        return totalAddNewLeavebal;
    }

    public void setTotalAddNewLeavebal(BigDecimal totalAddNewLeavebal) {
        this.totalAddNewLeavebal = totalAddNewLeavebal;
    }

    @XmlTransient
    public Collection<FtasAbsenteesDtl> getFtasAbsenteesDtlCollection() {
        return ftasAbsenteesDtlCollection;
    }

    public void setFtasAbsenteesDtlCollection(Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection) {
        this.ftasAbsenteesDtlCollection = ftasAbsenteesDtlCollection;
    }

    @XmlTransient
    public Collection<FtasAbsenteesDtl> getFtasAbsenteesDtlCollection1() {
        return ftasAbsenteesDtlCollection1;
    }

    public void setFtasAbsenteesDtlCollection1(Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection1) {
        this.ftasAbsenteesDtlCollection1 = ftasAbsenteesDtlCollection1;
    }

    @XmlTransient
    public Collection<FtasAbsenteesDtl> getFtasAbsenteesDtlCollection2() {
        return ftasAbsenteesDtlCollection2;
    }

    public void setFtasAbsenteesDtlCollection2(Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection2) {
        this.ftasAbsenteesDtlCollection2 = ftasAbsenteesDtlCollection2;
    }

    @XmlTransient
    public Collection<FtasAbsenteesDtl> getFtasAbsenteesDtlCollection3() {
        return ftasAbsenteesDtlCollection3;
    }

    public void setFtasAbsenteesDtlCollection3(Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection3) {
        this.ftasAbsenteesDtlCollection3 = ftasAbsenteesDtlCollection3;
    }

    @XmlTransient
    public Collection<FtasAbsenteesDtl> getFtasAbsenteesDtlCollection4() {
        return ftasAbsenteesDtlCollection4;
    }

    public void setFtasAbsenteesDtlCollection4(Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection4) {
        this.ftasAbsenteesDtlCollection4 = ftasAbsenteesDtlCollection4;
    }

    @XmlTransient
    public Collection<FhrdPaytransleaveDtl> getFhrdPaytransleaveDtlCollection() {
        return fhrdPaytransleaveDtlCollection;
    }

    public void setFhrdPaytransleaveDtlCollection(Collection<FhrdPaytransleaveDtl> fhrdPaytransleaveDtlCollection) {
        this.fhrdPaytransleaveDtlCollection = fhrdPaytransleaveDtlCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasLeavemst getLmSrgKey() {
        return lmSrgKey;
    }

    public void setLmSrgKey(FtasLeavemst lmSrgKey) {
        this.lmSrgKey = lmSrgKey;
    }

    public FhrdLeaverule getLrSrgKey() {
        return lrSrgKey;
    }

    public void setLrSrgKey(FhrdLeaverule lrSrgKey) {
        this.lrSrgKey = lrSrgKey;
    }

    public FhrdLeaverule getParentLrSrgKey() {
        return parentLrSrgKey;
    }

    public void setParentLrSrgKey(FhrdLeaverule parentLrSrgKey) {
        this.parentLrSrgKey = parentLrSrgKey;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getUserId() {
        return userId;
    }

    public void setUserId(FhrdEmpmst userId) {
        this.userId = userId;
    }

    public FhrdEmpleaverule getParentElrSrgKey() {
        return parentElrSrgKey;
    }

    public void setParentElrSrgKey(FhrdEmpleaverule parentElrSrgKey) {
        this.parentElrSrgKey = parentElrSrgKey;
    }

    public FhrdEmpleaverule getElrSrgKey() {
        return elrSrgKey;
    }

    public void setElrSrgKey(FhrdEmpleaverule elrSrgKey) {
        this.elrSrgKey = elrSrgKey;
    }

    @XmlTransient
    public Collection<FhrdEmpEncash> getFhrdEmpEncashCollection() {
        return fhrdEmpEncashCollection;
    }

    public void setFhrdEmpEncashCollection(Collection<FhrdEmpEncash> fhrdEmpEncashCollection) {
        this.fhrdEmpEncashCollection = fhrdEmpEncashCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (emplbSrgKey != null ? emplbSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdEmpleavebal)) {
            return false;
        }
        FhrdEmpleavebal other = (FhrdEmpleavebal) object;
        if ((this.emplbSrgKey == null && other.emplbSrgKey != null) || (this.emplbSrgKey != null && !this.emplbSrgKey.equals(other.emplbSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdEmpleavebal[ emplbSrgKey=" + emplbSrgKey + " ]";
    }
    
}
