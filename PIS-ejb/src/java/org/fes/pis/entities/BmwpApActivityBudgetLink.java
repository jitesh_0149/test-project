/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "BMWP_AP_ACTIVITY_BUDGET_LINK")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmwpApActivityBudgetLink.findAll", query = "SELECT b FROM BmwpApActivityBudgetLink b"),
    @NamedQuery(name = "BmwpApActivityBudgetLink.findByApablUniqueSrno", query = "SELECT b FROM BmwpApActivityBudgetLink b WHERE b.apablUniqueSrno = :apablUniqueSrno"),
    @NamedQuery(name = "BmwpApActivityBudgetLink.findByApablSrno", query = "SELECT b FROM BmwpApActivityBudgetLink b WHERE b.apablSrno = :apablSrno"),
    @NamedQuery(name = "BmwpApActivityBudgetLink.findByCreatedt", query = "SELECT b FROM BmwpApActivityBudgetLink b WHERE b.createdt = :createdt"),
    @NamedQuery(name = "BmwpApActivityBudgetLink.findByUpdatedt", query = "SELECT b FROM BmwpApActivityBudgetLink b WHERE b.updatedt = :updatedt"),
    @NamedQuery(name = "BmwpApActivityBudgetLink.findBySystemUserid", query = "SELECT b FROM BmwpApActivityBudgetLink b WHERE b.systemUserid = :systemUserid"),
    @NamedQuery(name = "BmwpApActivityBudgetLink.findByExportFlag", query = "SELECT b FROM BmwpApActivityBudgetLink b WHERE b.exportFlag = :exportFlag"),
    @NamedQuery(name = "BmwpApActivityBudgetLink.findByImportFlag", query = "SELECT b FROM BmwpApActivityBudgetLink b WHERE b.importFlag = :importFlag"),
    @NamedQuery(name = "BmwpApActivityBudgetLink.findByRemark", query = "SELECT b FROM BmwpApActivityBudgetLink b WHERE b.remark = :remark"),
    @NamedQuery(name = "BmwpApActivityBudgetLink.findByApablStartDate", query = "SELECT b FROM BmwpApActivityBudgetLink b WHERE b.apablStartDate = :apablStartDate"),
    @NamedQuery(name = "BmwpApActivityBudgetLink.findByApablEndDate", query = "SELECT b FROM BmwpApActivityBudgetLink b WHERE b.apablEndDate = :apablEndDate"),
    @NamedQuery(name = "BmwpApActivityBudgetLink.findByApablEndDatePossible", query = "SELECT b FROM BmwpApActivityBudgetLink b WHERE b.apablEndDatePossible = :apablEndDatePossible"),
    @NamedQuery(name = "BmwpApActivityBudgetLink.findByPmFundType", query = "SELECT b FROM BmwpApActivityBudgetLink b WHERE b.pmFundType = :pmFundType")})
public class BmwpApActivityBudgetLink implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "APABL_UNIQUE_SRNO")
    private BigDecimal apablUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APABL_SRNO")
    private int apablSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Size(max = 1000)
    @Column(name = "REMARK")
    private String remark;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APABL_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date apablStartDate;
    @Column(name = "APABL_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date apablEndDate;
    @Column(name = "APABL_END_DATE_POSSIBLE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date apablEndDatePossible;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PM_FUND_TYPE")
    private String pmFundType;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "APABL_PM_UNIQUE_SRNO", referencedColumnName = "PM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpProjectMst apablPmUniqueSrno;
    @JoinColumn(name = "APABL_FAM_UNIQUE_SRNO", referencedColumnName = "FAM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpFundAgencyMst apablFamUniqueSrno;
    @JoinColumn(name = "APABL_APBM_UNIQUE_SRNO", referencedColumnName = "APBM_UNIQUE_SRNO")
    @ManyToOne(optional = false)
    private BmwpApBudgetMst apablApbmUniqueSrno;
    @JoinColumn(name = "APABL_APAM_UNIQUE_SRNO", referencedColumnName = "APAM_UNIQUE_SRNO")
    @ManyToOne(optional = false)
    private BmwpApActivityMst apablApamUniqueSrno;
    @OneToMany(mappedBy = "apablParentUniqueSrno")
    private Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection;
    @JoinColumn(name = "APABL_PARENT_UNIQUE_SRNO", referencedColumnName = "APABL_UNIQUE_SRNO")
    @ManyToOne
    private BmwpApActivityBudgetLink apablParentUniqueSrno;
    @JoinColumn(name = "APABL_ATM_UNIQUE_SRNO", referencedColumnName = "ATM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpAgencyTypeMst apablAtmUniqueSrno;
    @JoinColumn(name = "APABL_APL_UNIQUE_SRNO", referencedColumnName = "APL_UNIQUE_SRNO")
    @ManyToOne
    private BmwpAgencyProjectLink apablAplUniqueSrno;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "apabdApablUniqueSrno")
    private Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection;

    public BmwpApActivityBudgetLink() {
    }

    public BmwpApActivityBudgetLink(BigDecimal apablUniqueSrno) {
        this.apablUniqueSrno = apablUniqueSrno;
    }

    public BmwpApActivityBudgetLink(BigDecimal apablUniqueSrno, int apablSrno, Date createdt, Date apablStartDate, String pmFundType) {
        this.apablUniqueSrno = apablUniqueSrno;
        this.apablSrno = apablSrno;
        this.createdt = createdt;
        this.apablStartDate = apablStartDate;
        this.pmFundType = pmFundType;
    }

    public BigDecimal getApablUniqueSrno() {
        return apablUniqueSrno;
    }

    public void setApablUniqueSrno(BigDecimal apablUniqueSrno) {
        this.apablUniqueSrno = apablUniqueSrno;
    }

    public int getApablSrno() {
        return apablSrno;
    }

    public void setApablSrno(int apablSrno) {
        this.apablSrno = apablSrno;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getApablStartDate() {
        return apablStartDate;
    }

    public void setApablStartDate(Date apablStartDate) {
        this.apablStartDate = apablStartDate;
    }

    public Date getApablEndDate() {
        return apablEndDate;
    }

    public void setApablEndDate(Date apablEndDate) {
        this.apablEndDate = apablEndDate;
    }

    public Date getApablEndDatePossible() {
        return apablEndDatePossible;
    }

    public void setApablEndDatePossible(Date apablEndDatePossible) {
        this.apablEndDatePossible = apablEndDatePossible;
    }

    public String getPmFundType() {
        return pmFundType;
    }

    public void setPmFundType(String pmFundType) {
        this.pmFundType = pmFundType;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public BmwpProjectMst getApablPmUniqueSrno() {
        return apablPmUniqueSrno;
    }

    public void setApablPmUniqueSrno(BmwpProjectMst apablPmUniqueSrno) {
        this.apablPmUniqueSrno = apablPmUniqueSrno;
    }

    public BmwpFundAgencyMst getApablFamUniqueSrno() {
        return apablFamUniqueSrno;
    }

    public void setApablFamUniqueSrno(BmwpFundAgencyMst apablFamUniqueSrno) {
        this.apablFamUniqueSrno = apablFamUniqueSrno;
    }

    public BmwpApBudgetMst getApablApbmUniqueSrno() {
        return apablApbmUniqueSrno;
    }

    public void setApablApbmUniqueSrno(BmwpApBudgetMst apablApbmUniqueSrno) {
        this.apablApbmUniqueSrno = apablApbmUniqueSrno;
    }

    public BmwpApActivityMst getApablApamUniqueSrno() {
        return apablApamUniqueSrno;
    }

    public void setApablApamUniqueSrno(BmwpApActivityMst apablApamUniqueSrno) {
        this.apablApamUniqueSrno = apablApamUniqueSrno;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetLink> getBmwpApActivityBudgetLinkCollection() {
        return bmwpApActivityBudgetLinkCollection;
    }

    public void setBmwpApActivityBudgetLinkCollection(Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection) {
        this.bmwpApActivityBudgetLinkCollection = bmwpApActivityBudgetLinkCollection;
    }

    public BmwpApActivityBudgetLink getApablParentUniqueSrno() {
        return apablParentUniqueSrno;
    }

    public void setApablParentUniqueSrno(BmwpApActivityBudgetLink apablParentUniqueSrno) {
        this.apablParentUniqueSrno = apablParentUniqueSrno;
    }

    public BmwpAgencyTypeMst getApablAtmUniqueSrno() {
        return apablAtmUniqueSrno;
    }

    public void setApablAtmUniqueSrno(BmwpAgencyTypeMst apablAtmUniqueSrno) {
        this.apablAtmUniqueSrno = apablAtmUniqueSrno;
    }

    public BmwpAgencyProjectLink getApablAplUniqueSrno() {
        return apablAplUniqueSrno;
    }

    public void setApablAplUniqueSrno(BmwpAgencyProjectLink apablAplUniqueSrno) {
        this.apablAplUniqueSrno = apablAplUniqueSrno;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetDtl> getBmwpApActivityBudgetDtlCollection() {
        return bmwpApActivityBudgetDtlCollection;
    }

    public void setBmwpApActivityBudgetDtlCollection(Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection) {
        this.bmwpApActivityBudgetDtlCollection = bmwpApActivityBudgetDtlCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (apablUniqueSrno != null ? apablUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmwpApActivityBudgetLink)) {
            return false;
        }
        BmwpApActivityBudgetLink other = (BmwpApActivityBudgetLink) object;
        if ((this.apablUniqueSrno == null && other.apablUniqueSrno != null) || (this.apablUniqueSrno != null && !this.apablUniqueSrno.equals(other.apablUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.BmwpApActivityBudgetLink[ apablUniqueSrno=" + apablUniqueSrno + " ]";
    }
    
}
