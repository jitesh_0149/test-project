/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_RULE_MANAG_GROUP_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdRuleManagGroupMst.findAll", query = "SELECT f FROM FhrdRuleManagGroupMst f"),
    @NamedQuery(name = "FhrdRuleManagGroupMst.findByManagGroupSrno", query = "SELECT f FROM FhrdRuleManagGroupMst f WHERE f.managGroupSrno = :managGroupSrno"),
    @NamedQuery(name = "FhrdRuleManagGroupMst.findByManagGroupDesc", query = "SELECT f FROM FhrdRuleManagGroupMst f WHERE f.managGroupDesc = :managGroupDesc"),
    @NamedQuery(name = "FhrdRuleManagGroupMst.findByCreatedt", query = "SELECT f FROM FhrdRuleManagGroupMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdRuleManagGroupMst.findByUpdatedt", query = "SELECT f FROM FhrdRuleManagGroupMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdRuleManagGroupMst.findBySystemUserid", query = "SELECT f FROM FhrdRuleManagGroupMst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdRuleManagGroupMst.findByRmgmSrgKey", query = "SELECT f FROM FhrdRuleManagGroupMst f WHERE f.rmgmSrgKey = :rmgmSrgKey"),
    @NamedQuery(name = "FhrdRuleManagGroupMst.findByStartDate", query = "SELECT f FROM FhrdRuleManagGroupMst f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdRuleManagGroupMst.findByEndDate", query = "SELECT f FROM FhrdRuleManagGroupMst f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FhrdRuleManagGroupMst.findByContractTypeFlag", query = "SELECT f FROM FhrdRuleManagGroupMst f WHERE f.contractTypeFlag = :contractTypeFlag"),
    @NamedQuery(name = "FhrdRuleManagGroupMst.findByRmgmAltSrno", query = "SELECT f FROM FhrdRuleManagGroupMst f WHERE f.rmgmAltSrno = :rmgmAltSrno"),
    @NamedQuery(name = "FhrdRuleManagGroupMst.findByRmgmAltSubsrno", query = "SELECT f FROM FhrdRuleManagGroupMst f WHERE f.rmgmAltSubsrno = :rmgmAltSubsrno")})
public class FhrdRuleManagGroupMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MANAG_GROUP_SRNO")
    private int managGroupSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "MANAG_GROUP_DESC")
    private String managGroupDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "RMGM_SRG_KEY")
    private BigDecimal rmgmSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "CONTRACT_TYPE_FLAG")
    private String contractTypeFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RMGM_ALT_SRNO")
    private int rmgmAltSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RMGM_ALT_SUBSRNO")
    private int rmgmAltSubsrno;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "RGC_SRG_KEY", referencedColumnName = "RGC_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdRuleGroupCategory rgcSrgKey;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rmgmSrgKey")
    private Collection<FhrdEmpContractGroupMst> fhrdEmpContractGroupMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rmgmSrgKey")
    private Collection<FhrdEarndedn> fhrdEarndednCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rmgmSrgKey")
    private Collection<FhrdRuleGroupRelation> fhrdRuleGroupRelationCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rmgmSrgKey")
    private Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rmgmSrgKey")
    private Collection<FhrdEmpearndedn> fhrdEmpearndednCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rmgmSrgKey")
    private Collection<FhrdLeaverule> fhrdLeaveruleCollection;

    public FhrdRuleManagGroupMst() {
    }

    public FhrdRuleManagGroupMst(BigDecimal rmgmSrgKey) {
        this.rmgmSrgKey = rmgmSrgKey;
    }

    public FhrdRuleManagGroupMst(BigDecimal rmgmSrgKey, int managGroupSrno, String managGroupDesc, Date createdt, Date startDate, String contractTypeFlag, int rmgmAltSrno, int rmgmAltSubsrno) {
        this.rmgmSrgKey = rmgmSrgKey;
        this.managGroupSrno = managGroupSrno;
        this.managGroupDesc = managGroupDesc;
        this.createdt = createdt;
        this.startDate = startDate;
        this.contractTypeFlag = contractTypeFlag;
        this.rmgmAltSrno = rmgmAltSrno;
        this.rmgmAltSubsrno = rmgmAltSubsrno;
    }

    public int getManagGroupSrno() {
        return managGroupSrno;
    }

    public void setManagGroupSrno(int managGroupSrno) {
        this.managGroupSrno = managGroupSrno;
    }

    public String getManagGroupDesc() {
        return managGroupDesc;
    }

    public void setManagGroupDesc(String managGroupDesc) {
        this.managGroupDesc = managGroupDesc;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public BigDecimal getRmgmSrgKey() {
        return rmgmSrgKey;
    }

    public void setRmgmSrgKey(BigDecimal rmgmSrgKey) {
        this.rmgmSrgKey = rmgmSrgKey;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getContractTypeFlag() {
        return contractTypeFlag;
    }

    public void setContractTypeFlag(String contractTypeFlag) {
        this.contractTypeFlag = contractTypeFlag;
    }

    public int getRmgmAltSrno() {
        return rmgmAltSrno;
    }

    public void setRmgmAltSrno(int rmgmAltSrno) {
        this.rmgmAltSrno = rmgmAltSrno;
    }

    public int getRmgmAltSubsrno() {
        return rmgmAltSubsrno;
    }

    public void setRmgmAltSubsrno(int rmgmAltSubsrno) {
        this.rmgmAltSubsrno = rmgmAltSubsrno;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdRuleGroupCategory getRgcSrgKey() {
        return rgcSrgKey;
    }

    public void setRgcSrgKey(FhrdRuleGroupCategory rgcSrgKey) {
        this.rgcSrgKey = rgcSrgKey;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @XmlTransient
    public Collection<FhrdEmpContractGroupMst> getFhrdEmpContractGroupMstCollection() {
        return fhrdEmpContractGroupMstCollection;
    }

    public void setFhrdEmpContractGroupMstCollection(Collection<FhrdEmpContractGroupMst> fhrdEmpContractGroupMstCollection) {
        this.fhrdEmpContractGroupMstCollection = fhrdEmpContractGroupMstCollection;
    }

    @XmlTransient
    public Collection<FhrdEarndedn> getFhrdEarndednCollection() {
        return fhrdEarndednCollection;
    }

    public void setFhrdEarndednCollection(Collection<FhrdEarndedn> fhrdEarndednCollection) {
        this.fhrdEarndednCollection = fhrdEarndednCollection;
    }

    @XmlTransient
    public Collection<FhrdRuleGroupRelation> getFhrdRuleGroupRelationCollection() {
        return fhrdRuleGroupRelationCollection;
    }

    public void setFhrdRuleGroupRelationCollection(Collection<FhrdRuleGroupRelation> fhrdRuleGroupRelationCollection) {
        this.fhrdRuleGroupRelationCollection = fhrdRuleGroupRelationCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpleaverule> getFhrdEmpleaveruleCollection() {
        return fhrdEmpleaveruleCollection;
    }

    public void setFhrdEmpleaveruleCollection(Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection) {
        this.fhrdEmpleaveruleCollection = fhrdEmpleaveruleCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpearndedn> getFhrdEmpearndednCollection() {
        return fhrdEmpearndednCollection;
    }

    public void setFhrdEmpearndednCollection(Collection<FhrdEmpearndedn> fhrdEmpearndednCollection) {
        this.fhrdEmpearndednCollection = fhrdEmpearndednCollection;
    }

    @XmlTransient
    public Collection<FhrdLeaverule> getFhrdLeaveruleCollection() {
        return fhrdLeaveruleCollection;
    }

    public void setFhrdLeaveruleCollection(Collection<FhrdLeaverule> fhrdLeaveruleCollection) {
        this.fhrdLeaveruleCollection = fhrdLeaveruleCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rmgmSrgKey != null ? rmgmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdRuleManagGroupMst)) {
            return false;
        }
        FhrdRuleManagGroupMst other = (FhrdRuleManagGroupMst) object;
        if ((this.rmgmSrgKey == null && other.rmgmSrgKey != null) || (this.rmgmSrgKey != null && !this.rmgmSrgKey.equals(other.rmgmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdRuleManagGroupMst[ rmgmSrgKey=" + rmgmSrgKey + " ]";
    }
    
}
