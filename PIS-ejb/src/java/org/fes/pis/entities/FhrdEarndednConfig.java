/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_EARNDEDN_CONFIG")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdEarndednConfig.findAll", query = "SELECT f FROM FhrdEarndednConfig f"),
    @NamedQuery(name = "FhrdEarndednConfig.findByEdcSrgKey", query = "SELECT f FROM FhrdEarndednConfig f WHERE f.edcSrgKey = :edcSrgKey"),
    @NamedQuery(name = "FhrdEarndednConfig.findByEarndedncd", query = "SELECT f FROM FhrdEarndednConfig f WHERE f.earndedncd = :earndedncd"),
    @NamedQuery(name = "FhrdEarndednConfig.findByEarndednFlag", query = "SELECT f FROM FhrdEarndednConfig f WHERE f.earndednFlag = :earndednFlag"),
    @NamedQuery(name = "FhrdEarndednConfig.findByFormulaAmtFlag", query = "SELECT f FROM FhrdEarndednConfig f WHERE f.formulaAmtFlag = :formulaAmtFlag"),
    @NamedQuery(name = "FhrdEarndednConfig.findByFormulaAmtValue", query = "SELECT f FROM FhrdEarndednConfig f WHERE f.formulaAmtValue = :formulaAmtValue"),
    @NamedQuery(name = "FhrdEarndednConfig.findByStartDate", query = "SELECT f FROM FhrdEarndednConfig f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdEarndednConfig.findByEndDate", query = "SELECT f FROM FhrdEarndednConfig f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FhrdEarndednConfig.findByCreatedt", query = "SELECT f FROM FhrdEarndednConfig f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdEarndednConfig.findByUpdatedt", query = "SELECT f FROM FhrdEarndednConfig f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdEarndednConfig.findBySystemUserid", query = "SELECT f FROM FhrdEarndednConfig f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdEarndednConfig.findByExportFlag", query = "SELECT f FROM FhrdEarndednConfig f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdEarndednConfig.findByImportFlag", query = "SELECT f FROM FhrdEarndednConfig f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdEarndednConfig.findByStatesrno", query = "SELECT f FROM FhrdEarndednConfig f WHERE f.statesrno = :statesrno"),
    @NamedQuery(name = "FhrdEarndednConfig.findByMinValue", query = "SELECT f FROM FhrdEarndednConfig f WHERE f.minValue = :minValue"),
    @NamedQuery(name = "FhrdEarndednConfig.findByMaxValue", query = "SELECT f FROM FhrdEarndednConfig f WHERE f.maxValue = :maxValue"),
    @NamedQuery(name = "FhrdEarndednConfig.findByFromMonth", query = "SELECT f FROM FhrdEarndednConfig f WHERE f.fromMonth = :fromMonth"),
    @NamedQuery(name = "FhrdEarndednConfig.findByToMonth", query = "SELECT f FROM FhrdEarndednConfig f WHERE f.toMonth = :toMonth"),
    @NamedQuery(name = "FhrdEarndednConfig.findByToMonthIsGreaterFlag", query = "SELECT f FROM FhrdEarndednConfig f WHERE f.toMonthIsGreaterFlag = :toMonthIsGreaterFlag"),
    @NamedQuery(name = "FhrdEarndednConfig.findByDesigDependent", query = "SELECT f FROM FhrdEarndednConfig f WHERE f.desigDependent = :desigDependent")})
public class FhrdEarndednConfig implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "EDC_SRG_KEY")
    private BigDecimal edcSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "EARNDEDNCD")
    private String earndedncd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EARNDEDN_FLAG")
    private String earndednFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "FORMULA_AMT_FLAG")
    private String formulaAmtFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "FORMULA_AMT_VALUE")
    private String formulaAmtValue;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "STATESRNO")
    private Integer statesrno;
    @Column(name = "MIN_VALUE")
    private Long minValue;
    @Column(name = "MAX_VALUE")
    private Long maxValue;
    @Size(max = 3)
    @Column(name = "FROM_MONTH")
    private String fromMonth;
    @Size(max = 3)
    @Column(name = "TO_MONTH")
    private String toMonth;
    @Size(max = 1)
    @Column(name = "TO_MONTH_IS_GREATER_FLAG")
    private String toMonthIsGreaterFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "DESIG_DEPENDENT")
    private String desigDependent;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "APPL_OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst applOumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "ED_SRG_KEY", referencedColumnName = "ED_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdEarndedn edSrgKey;
    @JoinColumn(name = "DSGM_SRG_KEY", referencedColumnName = "DSGM_SRG_KEY")
    @ManyToOne
    private FhrdDesigmst dsgmSrgKey;

    public FhrdEarndednConfig() {
    }

    public FhrdEarndednConfig(BigDecimal edcSrgKey) {
        this.edcSrgKey = edcSrgKey;
    }

    public FhrdEarndednConfig(BigDecimal edcSrgKey, String earndedncd, String earndednFlag, String formulaAmtFlag, String formulaAmtValue, Date startDate, Date createdt, String desigDependent) {
        this.edcSrgKey = edcSrgKey;
        this.earndedncd = earndedncd;
        this.earndednFlag = earndednFlag;
        this.formulaAmtFlag = formulaAmtFlag;
        this.formulaAmtValue = formulaAmtValue;
        this.startDate = startDate;
        this.createdt = createdt;
        this.desigDependent = desigDependent;
    }

    public BigDecimal getEdcSrgKey() {
        return edcSrgKey;
    }

    public void setEdcSrgKey(BigDecimal edcSrgKey) {
        this.edcSrgKey = edcSrgKey;
    }

    public String getEarndedncd() {
        return earndedncd;
    }

    public void setEarndedncd(String earndedncd) {
        this.earndedncd = earndedncd;
    }

    public String getEarndednFlag() {
        return earndednFlag;
    }

    public void setEarndednFlag(String earndednFlag) {
        this.earndednFlag = earndednFlag;
    }

    public String getFormulaAmtFlag() {
        return formulaAmtFlag;
    }

    public void setFormulaAmtFlag(String formulaAmtFlag) {
        this.formulaAmtFlag = formulaAmtFlag;
    }

    public String getFormulaAmtValue() {
        return formulaAmtValue;
    }

    public void setFormulaAmtValue(String formulaAmtValue) {
        this.formulaAmtValue = formulaAmtValue;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getStatesrno() {
        return statesrno;
    }

    public void setStatesrno(Integer statesrno) {
        this.statesrno = statesrno;
    }

    public Long getMinValue() {
        return minValue;
    }

    public void setMinValue(Long minValue) {
        this.minValue = minValue;
    }

    public Long getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Long maxValue) {
        this.maxValue = maxValue;
    }

    public String getFromMonth() {
        return fromMonth;
    }

    public void setFromMonth(String fromMonth) {
        this.fromMonth = fromMonth;
    }

    public String getToMonth() {
        return toMonth;
    }

    public void setToMonth(String toMonth) {
        this.toMonth = toMonth;
    }

    public String getToMonthIsGreaterFlag() {
        return toMonthIsGreaterFlag;
    }

    public void setToMonthIsGreaterFlag(String toMonthIsGreaterFlag) {
        this.toMonthIsGreaterFlag = toMonthIsGreaterFlag;
    }

    public String getDesigDependent() {
        return desigDependent;
    }

    public void setDesigDependent(String desigDependent) {
        this.desigDependent = desigDependent;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public SystOrgUnitMst getApplOumUnitSrno() {
        return applOumUnitSrno;
    }

    public void setApplOumUnitSrno(SystOrgUnitMst applOumUnitSrno) {
        this.applOumUnitSrno = applOumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEarndedn getEdSrgKey() {
        return edSrgKey;
    }

    public void setEdSrgKey(FhrdEarndedn edSrgKey) {
        this.edSrgKey = edSrgKey;
    }

    public FhrdDesigmst getDsgmSrgKey() {
        return dsgmSrgKey;
    }

    public void setDsgmSrgKey(FhrdDesigmst dsgmSrgKey) {
        this.dsgmSrgKey = dsgmSrgKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (edcSrgKey != null ? edcSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdEarndednConfig)) {
            return false;
        }
        FhrdEarndednConfig other = (FhrdEarndednConfig) object;
        if ((this.edcSrgKey == null && other.edcSrgKey != null) || (this.edcSrgKey != null && !this.edcSrgKey.equals(other.edcSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdEarndednConfig[ edcSrgKey=" + edcSrgKey + " ]";
    }
    
}
