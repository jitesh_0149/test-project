/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_GRADEMST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdGrademst.findAll", query = "SELECT f FROM FhrdGrademst f"),
    @NamedQuery(name = "FhrdGrademst.findByGradesrno", query = "SELECT f FROM FhrdGrademst f WHERE f.gradesrno = :gradesrno"),
    @NamedQuery(name = "FhrdGrademst.findByGradecd", query = "SELECT f FROM FhrdGrademst f WHERE f.gradecd = :gradecd"),
    @NamedQuery(name = "FhrdGrademst.findByStartbasic", query = "SELECT f FROM FhrdGrademst f WHERE f.startbasic = :startbasic"),
    @NamedQuery(name = "FhrdGrademst.findByMaxbasic", query = "SELECT f FROM FhrdGrademst f WHERE f.maxbasic = :maxbasic"),
    @NamedQuery(name = "FhrdGrademst.findByIncrperiod", query = "SELECT f FROM FhrdGrademst f WHERE f.incrperiod = :incrperiod"),
    @NamedQuery(name = "FhrdGrademst.findByNoticeperiod", query = "SELECT f FROM FhrdGrademst f WHERE f.noticeperiod = :noticeperiod"),
    @NamedQuery(name = "FhrdGrademst.findByStartdt", query = "SELECT f FROM FhrdGrademst f WHERE f.startdt = :startdt"),
    @NamedQuery(name = "FhrdGrademst.findByEnddt", query = "SELECT f FROM FhrdGrademst f WHERE f.enddt = :enddt"),
    @NamedQuery(name = "FhrdGrademst.findByWorkingflag", query = "SELECT f FROM FhrdGrademst f WHERE f.workingflag = :workingflag"),
    @NamedQuery(name = "FhrdGrademst.findByCreatedt", query = "SELECT f FROM FhrdGrademst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdGrademst.findByUpdatedt", query = "SELECT f FROM FhrdGrademst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdGrademst.findByExportFlag", query = "SELECT f FROM FhrdGrademst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdGrademst.findByImportFlag", query = "SELECT f FROM FhrdGrademst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdGrademst.findBySystemUserid", query = "SELECT f FROM FhrdGrademst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdGrademst.findByGmSrgKey", query = "SELECT f FROM FhrdGrademst f WHERE f.gmSrgKey = :gmSrgKey")})
public class FhrdGrademst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "GRADESRNO")
    private short gradesrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "GRADECD")
    private short gradecd;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "STARTBASIC")
    private BigDecimal startbasic;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MAXBASIC")
    private BigDecimal maxbasic;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INCRPERIOD")
    private BigDecimal incrperiod;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NOTICEPERIOD")
    private long noticeperiod;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STARTDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startdt;
    @Column(name = "ENDDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enddt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "WORKINGFLAG")
    private String workingflag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "GM_SRG_KEY")
    private BigDecimal gmSrgKey;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FhrdGrademst() {
    }

    public FhrdGrademst(BigDecimal gmSrgKey) {
        this.gmSrgKey = gmSrgKey;
    }

    public FhrdGrademst(BigDecimal gmSrgKey, short gradesrno, short gradecd, BigDecimal startbasic, BigDecimal maxbasic, BigDecimal incrperiod, long noticeperiod, Date startdt, String workingflag, Date createdt) {
        this.gmSrgKey = gmSrgKey;
        this.gradesrno = gradesrno;
        this.gradecd = gradecd;
        this.startbasic = startbasic;
        this.maxbasic = maxbasic;
        this.incrperiod = incrperiod;
        this.noticeperiod = noticeperiod;
        this.startdt = startdt;
        this.workingflag = workingflag;
        this.createdt = createdt;
    }

    public short getGradesrno() {
        return gradesrno;
    }

    public void setGradesrno(short gradesrno) {
        this.gradesrno = gradesrno;
    }

    public short getGradecd() {
        return gradecd;
    }

    public void setGradecd(short gradecd) {
        this.gradecd = gradecd;
    }

    public BigDecimal getStartbasic() {
        return startbasic;
    }

    public void setStartbasic(BigDecimal startbasic) {
        this.startbasic = startbasic;
    }

    public BigDecimal getMaxbasic() {
        return maxbasic;
    }

    public void setMaxbasic(BigDecimal maxbasic) {
        this.maxbasic = maxbasic;
    }

    public BigDecimal getIncrperiod() {
        return incrperiod;
    }

    public void setIncrperiod(BigDecimal incrperiod) {
        this.incrperiod = incrperiod;
    }

    public long getNoticeperiod() {
        return noticeperiod;
    }

    public void setNoticeperiod(long noticeperiod) {
        this.noticeperiod = noticeperiod;
    }

    public Date getStartdt() {
        return startdt;
    }

    public void setStartdt(Date startdt) {
        this.startdt = startdt;
    }

    public Date getEnddt() {
        return enddt;
    }

    public void setEnddt(Date enddt) {
        this.enddt = enddt;
    }

    public String getWorkingflag() {
        return workingflag;
    }

    public void setWorkingflag(String workingflag) {
        this.workingflag = workingflag;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public BigDecimal getGmSrgKey() {
        return gmSrgKey;
    }

    public void setGmSrgKey(BigDecimal gmSrgKey) {
        this.gmSrgKey = gmSrgKey;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gmSrgKey != null ? gmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdGrademst)) {
            return false;
        }
        FhrdGrademst other = (FhrdGrademst) object;
        if ((this.gmSrgKey == null && other.gmSrgKey != null) || (this.gmSrgKey != null && !this.gmSrgKey.equals(other.gmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdGrademst[ gmSrgKey=" + gmSrgKey + " ]";
    }
    
}
