/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author anu
 */
@Embeddable
public class FhrdExperiencePK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_ID")
    private int userId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EXP_SRNO")
    private int expSrno;

    public FhrdExperiencePK() {
    }

    public FhrdExperiencePK(int userId, int expSrno) {
        this.userId = userId;
        this.expSrno = expSrno;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getExpSrno() {
        return expSrno;
    }

    public void setExpSrno(int expSrno) {
        this.expSrno = expSrno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) userId;
        hash += (int) expSrno;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdExperiencePK)) {
            return false;
        }
        FhrdExperiencePK other = (FhrdExperiencePK) object;
        if (this.userId != other.userId) {
            return false;
        }
        if (this.expSrno != other.expSrno) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdExperiencePK[ userId=" + userId + ", expSrno=" + expSrno + " ]";
    }
    
}
