/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_DESIGMST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdDesigmst.findAll", query = "SELECT f FROM FhrdDesigmst f"),
    @NamedQuery(name = "FhrdDesigmst.findByDesigSrno", query = "SELECT f FROM FhrdDesigmst f WHERE f.desigSrno = :desigSrno"),
    @NamedQuery(name = "FhrdDesigmst.findByDesigDesc", query = "SELECT f FROM FhrdDesigmst f WHERE f.desigDesc = :desigDesc"),
    @NamedQuery(name = "FhrdDesigmst.findByCreateby", query = "SELECT f FROM FhrdDesigmst f WHERE f.createby = :createby"),
    @NamedQuery(name = "FhrdDesigmst.findByCreatedt", query = "SELECT f FROM FhrdDesigmst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdDesigmst.findByUpdateby", query = "SELECT f FROM FhrdDesigmst f WHERE f.updateby = :updateby"),
    @NamedQuery(name = "FhrdDesigmst.findByUpdatedt", query = "SELECT f FROM FhrdDesigmst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdDesigmst.findByExportFlag", query = "SELECT f FROM FhrdDesigmst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdDesigmst.findByImportFlag", query = "SELECT f FROM FhrdDesigmst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdDesigmst.findBySystemUserid", query = "SELECT f FROM FhrdDesigmst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdDesigmst.findByStartDate", query = "SELECT f FROM FhrdDesigmst f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdDesigmst.findByEndDate", query = "SELECT f FROM FhrdDesigmst f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FhrdDesigmst.findByDsgmSrgKey", query = "SELECT f FROM FhrdDesigmst f WHERE f.dsgmSrgKey = :dsgmSrgKey"),
    @NamedQuery(name = "FhrdDesigmst.findByMaxTransDate", query = "SELECT f FROM FhrdDesigmst f WHERE f.maxTransDate = :maxTransDate")})
public class FhrdDesigmst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DESIG_SRNO")
    private int desigSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "DESIG_DESC")
    private String desigDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEBY")
    private int createby;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEBY")
    private Integer updateby;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DSGM_SRG_KEY")
    private BigDecimal dsgmSrgKey;
    @Column(name = "MAX_TRANS_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date maxTransDate;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @OneToMany(mappedBy = "dsgmSrgKey")
    private Collection<FhrdEmpmst> fhrdEmpmstCollection;
    @OneToMany(mappedBy = "dsgmSrgKey")
    private Collection<FhrdPaytrans> fhrdPaytransCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dsgmSrgKey")
    private Collection<FhrdPaytransSummary> fhrdPaytransSummaryCollection;
    @OneToMany(mappedBy = "dsgmSrgKey")
    private Collection<FhrdEarndednConfig> fhrdEarndednConfigCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dsgmSrgKey")
    private Collection<FhrdEmpContractMst> fhrdEmpContractMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dsgmSrgKey")
    private Collection<FtasTadaCashallowRuleMst> ftasTadaCashallowRuleMstCollection;

    public FhrdDesigmst() {
    }

    public FhrdDesigmst(BigDecimal dsgmSrgKey) {
        this.dsgmSrgKey = dsgmSrgKey;
    }

    public FhrdDesigmst(BigDecimal dsgmSrgKey, int desigSrno, String desigDesc, int createby, Date createdt, Date startDate) {
        this.dsgmSrgKey = dsgmSrgKey;
        this.desigSrno = desigSrno;
        this.desigDesc = desigDesc;
        this.createby = createby;
        this.createdt = createdt;
        this.startDate = startDate;
    }

    public int getDesigSrno() {
        return desigSrno;
    }

    public void setDesigSrno(int desigSrno) {
        this.desigSrno = desigSrno;
    }

    public String getDesigDesc() {
        return desigDesc;
    }

    public void setDesigDesc(String desigDesc) {
        this.desigDesc = desigDesc;
    }

    public int getCreateby() {
        return createby;
    }

    public void setCreateby(int createby) {
        this.createby = createby;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Integer getUpdateby() {
        return updateby;
    }

    public void setUpdateby(Integer updateby) {
        this.updateby = updateby;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getDsgmSrgKey() {
        return dsgmSrgKey;
    }

    public void setDsgmSrgKey(BigDecimal dsgmSrgKey) {
        this.dsgmSrgKey = dsgmSrgKey;
    }

    public Date getMaxTransDate() {
        return maxTransDate;
    }

    public void setMaxTransDate(Date maxTransDate) {
        this.maxTransDate = maxTransDate;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    @XmlTransient
    public Collection<FhrdEmpmst> getFhrdEmpmstCollection() {
        return fhrdEmpmstCollection;
    }

    public void setFhrdEmpmstCollection(Collection<FhrdEmpmst> fhrdEmpmstCollection) {
        this.fhrdEmpmstCollection = fhrdEmpmstCollection;
    }

    @XmlTransient
    public Collection<FhrdPaytrans> getFhrdPaytransCollection() {
        return fhrdPaytransCollection;
    }

    public void setFhrdPaytransCollection(Collection<FhrdPaytrans> fhrdPaytransCollection) {
        this.fhrdPaytransCollection = fhrdPaytransCollection;
    }

    @XmlTransient
    public Collection<FhrdPaytransSummary> getFhrdPaytransSummaryCollection() {
        return fhrdPaytransSummaryCollection;
    }

    public void setFhrdPaytransSummaryCollection(Collection<FhrdPaytransSummary> fhrdPaytransSummaryCollection) {
        this.fhrdPaytransSummaryCollection = fhrdPaytransSummaryCollection;
    }

    @XmlTransient
    public Collection<FhrdEarndednConfig> getFhrdEarndednConfigCollection() {
        return fhrdEarndednConfigCollection;
    }

    public void setFhrdEarndednConfigCollection(Collection<FhrdEarndednConfig> fhrdEarndednConfigCollection) {
        this.fhrdEarndednConfigCollection = fhrdEarndednConfigCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpContractMst> getFhrdEmpContractMstCollection() {
        return fhrdEmpContractMstCollection;
    }

    public void setFhrdEmpContractMstCollection(Collection<FhrdEmpContractMst> fhrdEmpContractMstCollection) {
        this.fhrdEmpContractMstCollection = fhrdEmpContractMstCollection;
    }

    @XmlTransient
    public Collection<FtasTadaCashallowRuleMst> getFtasTadaCashallowRuleMstCollection() {
        return ftasTadaCashallowRuleMstCollection;
    }

    public void setFtasTadaCashallowRuleMstCollection(Collection<FtasTadaCashallowRuleMst> ftasTadaCashallowRuleMstCollection) {
        this.ftasTadaCashallowRuleMstCollection = ftasTadaCashallowRuleMstCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dsgmSrgKey != null ? dsgmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdDesigmst)) {
            return false;
        }
        FhrdDesigmst other = (FhrdDesigmst) object;
        if ((this.dsgmSrgKey == null && other.dsgmSrgKey != null) || (this.dsgmSrgKey != null && !this.dsgmSrgKey.equals(other.dsgmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdDesigmst[ dsgmSrgKey=" + dsgmSrgKey + " ]";
    }
    
}
