/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "SYST_ORG_UNIT_TYPE_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SystOrgUnitTypeMst.findAll", query = "SELECT s FROM SystOrgUnitTypeMst s"),
    @NamedQuery(name = "SystOrgUnitTypeMst.findByUtmTypeSrno", query = "SELECT s FROM SystOrgUnitTypeMst s WHERE s.systOrgUnitTypeMstPK.utmTypeSrno = :utmTypeSrno"),
    @NamedQuery(name = "SystOrgUnitTypeMst.findByUtmTypeCode", query = "SELECT s FROM SystOrgUnitTypeMst s WHERE s.systOrgUnitTypeMstPK.utmTypeCode = :utmTypeCode"),
    @NamedQuery(name = "SystOrgUnitTypeMst.findByUtmTypeDescription", query = "SELECT s FROM SystOrgUnitTypeMst s WHERE s.utmTypeDescription = :utmTypeDescription"),
    @NamedQuery(name = "SystOrgUnitTypeMst.findByUtmTypeLevel", query = "SELECT s FROM SystOrgUnitTypeMst s WHERE s.utmTypeLevel = :utmTypeLevel"),
    @NamedQuery(name = "SystOrgUnitTypeMst.findByCreatedt", query = "SELECT s FROM SystOrgUnitTypeMst s WHERE s.createdt = :createdt"),
    @NamedQuery(name = "SystOrgUnitTypeMst.findByUpdatedt", query = "SELECT s FROM SystOrgUnitTypeMst s WHERE s.updatedt = :updatedt"),
    @NamedQuery(name = "SystOrgUnitTypeMst.findByExportFlag", query = "SELECT s FROM SystOrgUnitTypeMst s WHERE s.exportFlag = :exportFlag"),
    @NamedQuery(name = "SystOrgUnitTypeMst.findByImportFlag", query = "SELECT s FROM SystOrgUnitTypeMst s WHERE s.importFlag = :importFlag"),
    @NamedQuery(name = "SystOrgUnitTypeMst.findBySystemUserid", query = "SELECT s FROM SystOrgUnitTypeMst s WHERE s.systemUserid = :systemUserid")})
public class SystOrgUnitTypeMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SystOrgUnitTypeMstPK systOrgUnitTypeMstPK;
    @Size(max = 200)
    @Column(name = "UTM_TYPE_DESCRIPTION")
    private String utmTypeDescription;
    @Column(name = "UTM_TYPE_LEVEL")
    private Short utmTypeLevel;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @OneToMany(mappedBy = "systOrgUnitTypeMst")
    private Collection<SystOrgUnitMst> systOrgUnitMstCollection;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public SystOrgUnitTypeMst() {
    }

    public SystOrgUnitTypeMst(SystOrgUnitTypeMstPK systOrgUnitTypeMstPK) {
        this.systOrgUnitTypeMstPK = systOrgUnitTypeMstPK;
    }

    public SystOrgUnitTypeMst(SystOrgUnitTypeMstPK systOrgUnitTypeMstPK, Date createdt) {
        this.systOrgUnitTypeMstPK = systOrgUnitTypeMstPK;
        this.createdt = createdt;
    }

    public SystOrgUnitTypeMst(int utmTypeSrno, String utmTypeCode) {
        this.systOrgUnitTypeMstPK = new SystOrgUnitTypeMstPK(utmTypeSrno, utmTypeCode);
    }

    public SystOrgUnitTypeMstPK getSystOrgUnitTypeMstPK() {
        return systOrgUnitTypeMstPK;
    }

    public void setSystOrgUnitTypeMstPK(SystOrgUnitTypeMstPK systOrgUnitTypeMstPK) {
        this.systOrgUnitTypeMstPK = systOrgUnitTypeMstPK;
    }

    public String getUtmTypeDescription() {
        return utmTypeDescription;
    }

    public void setUtmTypeDescription(String utmTypeDescription) {
        this.utmTypeDescription = utmTypeDescription;
    }

    public Short getUtmTypeLevel() {
        return utmTypeLevel;
    }

    public void setUtmTypeLevel(Short utmTypeLevel) {
        this.utmTypeLevel = utmTypeLevel;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    @XmlTransient
    public Collection<SystOrgUnitMst> getSystOrgUnitMstCollection() {
        return systOrgUnitMstCollection;
    }

    public void setSystOrgUnitMstCollection(Collection<SystOrgUnitMst> systOrgUnitMstCollection) {
        this.systOrgUnitMstCollection = systOrgUnitMstCollection;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (systOrgUnitTypeMstPK != null ? systOrgUnitTypeMstPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SystOrgUnitTypeMst)) {
            return false;
        }
        SystOrgUnitTypeMst other = (SystOrgUnitTypeMst) object;
        if ((this.systOrgUnitTypeMstPK == null && other.systOrgUnitTypeMstPK != null) || (this.systOrgUnitTypeMstPK != null && !this.systOrgUnitTypeMstPK.equals(other.systOrgUnitTypeMstPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.SystOrgUnitTypeMst[ systOrgUnitTypeMstPK=" + systOrgUnitTypeMstPK + " ]";
    }
    
}
