/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_EMP_CONTRACT_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdEmpContractMst.findAll", query = "SELECT f FROM FhrdEmpContractMst f"),
    @NamedQuery(name = "FhrdEmpContractMst.findByOumUnitSrno", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.oumUnitSrno = :oumUnitSrno"),
    @NamedQuery(name = "FhrdEmpContractMst.findByStartDate", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdEmpContractMst.findByEndDate", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FhrdEmpContractMst.findByPmSrno", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.pmSrno = :pmSrno"),
    @NamedQuery(name = "FhrdEmpContractMst.findByCreatedt", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdEmpContractMst.findByUpdatedt", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdEmpContractMst.findBySystemUserid", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdEmpContractMst.findByExportFlag", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdEmpContractMst.findByImportFlag", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdEmpContractMst.findByEcmSrgKey", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.ecmSrgKey = :ecmSrgKey"),
    @NamedQuery(name = "FhrdEmpContractMst.findByCmAltSrno", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.cmAltSrno = :cmAltSrno"),
    @NamedQuery(name = "FhrdEmpContractMst.findByRefOumUnitSrno", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.refOumUnitSrno = :refOumUnitSrno"),
    @NamedQuery(name = "FhrdEmpContractMst.findByTranYear", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FhrdEmpContractMst.findByContSuddenEndFlag", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.contSuddenEndFlag = :contSuddenEndFlag"),
    @NamedQuery(name = "FhrdEmpContractMst.findBySystemUseridFlag", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.systemUseridFlag = :systemUseridFlag"),
    @NamedQuery(name = "FhrdEmpContractMst.findByTranStartDate", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.tranStartDate = :tranStartDate"),
    @NamedQuery(name = "FhrdEmpContractMst.findByTranEndDate", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.tranEndDate = :tranEndDate"),
    @NamedQuery(name = "FhrdEmpContractMst.findByContExtnType", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.contExtnType = :contExtnType"),
    @NamedQuery(name = "FhrdEmpContractMst.findByNoticePeriod", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.noticePeriod = :noticePeriod"),
    @NamedQuery(name = "FhrdEmpContractMst.findByContractCategory", query = "SELECT f FROM FhrdEmpContractMst f WHERE f.contractCategory = :contractCategory")})
public class FhrdEmpContractMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OUM_UNIT_SRNO")
    private int oumUnitSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Column(name = "PM_SRNO")
    private Integer pmSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ECM_SRG_KEY")
    private BigDecimal ecmSrgKey;
    @Column(name = "CM_ALT_SRNO")
    private Integer cmAltSrno;
    @Column(name = "REF_OUM_UNIT_SRNO")
    private Integer refOumUnitSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "CONT_SUDDEN_END_FLAG")
    private String contSuddenEndFlag;
    @Size(max = 1)
    @Column(name = "SYSTEM_USERID_FLAG")
    private String systemUseridFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tranStartDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tranEndDate;
    @Size(max = 1)
    @Column(name = "CONT_EXTN_TYPE")
    private String contExtnType;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NOTICE_PERIOD")
    private BigInteger noticePeriod;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "CONTRACT_CATEGORY")
    private String contractCategory;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ecmSrgKey")
    private Collection<FhrdEmpContractGroupMst> fhrdEmpContractGroupMstCollection;
    @JoinColumn(name = "EMPCM_SRG_KEY", referencedColumnName = "EMPCM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdEmpCategoryMst empcmSrgKey;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst userId;
    @JoinColumn(name = "DSGM_SRG_KEY", referencedColumnName = "DSGM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdDesigmst dsgmSrgKey;
    @JoinColumn(name = "CM_SRG_KEY", referencedColumnName = "CM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdContractMst cmSrgKey;

    public FhrdEmpContractMst() {
    }

    public FhrdEmpContractMst(BigDecimal ecmSrgKey) {
        this.ecmSrgKey = ecmSrgKey;
    }

    public FhrdEmpContractMst(BigDecimal ecmSrgKey, int oumUnitSrno, Date startDate, Date endDate, Date createdt, short tranYear, String contSuddenEndFlag, Date tranStartDate, Date tranEndDate, BigInteger noticePeriod, String contractCategory) {
        this.ecmSrgKey = ecmSrgKey;
        this.oumUnitSrno = oumUnitSrno;
        this.startDate = startDate;
        this.endDate = endDate;
        this.createdt = createdt;
        this.tranYear = tranYear;
        this.contSuddenEndFlag = contSuddenEndFlag;
        this.tranStartDate = tranStartDate;
        this.tranEndDate = tranEndDate;
        this.noticePeriod = noticePeriod;
        this.contractCategory = contractCategory;
    }

    public int getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(int oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getPmSrno() {
        return pmSrno;
    }

    public void setPmSrno(Integer pmSrno) {
        this.pmSrno = pmSrno;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public BigDecimal getEcmSrgKey() {
        return ecmSrgKey;
    }

    public void setEcmSrgKey(BigDecimal ecmSrgKey) {
        this.ecmSrgKey = ecmSrgKey;
    }

    public Integer getCmAltSrno() {
        return cmAltSrno;
    }

    public void setCmAltSrno(Integer cmAltSrno) {
        this.cmAltSrno = cmAltSrno;
    }

    public Integer getRefOumUnitSrno() {
        return refOumUnitSrno;
    }

    public void setRefOumUnitSrno(Integer refOumUnitSrno) {
        this.refOumUnitSrno = refOumUnitSrno;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public String getContSuddenEndFlag() {
        return contSuddenEndFlag;
    }

    public void setContSuddenEndFlag(String contSuddenEndFlag) {
        this.contSuddenEndFlag = contSuddenEndFlag;
    }

    public String getSystemUseridFlag() {
        return systemUseridFlag;
    }

    public void setSystemUseridFlag(String systemUseridFlag) {
        this.systemUseridFlag = systemUseridFlag;
    }

    public Date getTranStartDate() {
        return tranStartDate;
    }

    public void setTranStartDate(Date tranStartDate) {
        this.tranStartDate = tranStartDate;
    }

    public Date getTranEndDate() {
        return tranEndDate;
    }

    public void setTranEndDate(Date tranEndDate) {
        this.tranEndDate = tranEndDate;
    }

    public String getContExtnType() {
        return contExtnType;
    }

    public void setContExtnType(String contExtnType) {
        this.contExtnType = contExtnType;
    }

    public BigInteger getNoticePeriod() {
        return noticePeriod;
    }

    public void setNoticePeriod(BigInteger noticePeriod) {
        this.noticePeriod = noticePeriod;
    }

    public String getContractCategory() {
        return contractCategory;
    }

    public void setContractCategory(String contractCategory) {
        this.contractCategory = contractCategory;
    }

    @XmlTransient
    public Collection<FhrdEmpContractGroupMst> getFhrdEmpContractGroupMstCollection() {
        return fhrdEmpContractGroupMstCollection;
    }

    public void setFhrdEmpContractGroupMstCollection(Collection<FhrdEmpContractGroupMst> fhrdEmpContractGroupMstCollection) {
        this.fhrdEmpContractGroupMstCollection = fhrdEmpContractGroupMstCollection;
    }

    public FhrdEmpCategoryMst getEmpcmSrgKey() {
        return empcmSrgKey;
    }

    public void setEmpcmSrgKey(FhrdEmpCategoryMst empcmSrgKey) {
        this.empcmSrgKey = empcmSrgKey;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getUserId() {
        return userId;
    }

    public void setUserId(FhrdEmpmst userId) {
        this.userId = userId;
    }

    public FhrdDesigmst getDsgmSrgKey() {
        return dsgmSrgKey;
    }

    public void setDsgmSrgKey(FhrdDesigmst dsgmSrgKey) {
        this.dsgmSrgKey = dsgmSrgKey;
    }

    public FhrdContractMst getCmSrgKey() {
        return cmSrgKey;
    }

    public void setCmSrgKey(FhrdContractMst cmSrgKey) {
        this.cmSrgKey = cmSrgKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ecmSrgKey != null ? ecmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdEmpContractMst)) {
            return false;
        }
        FhrdEmpContractMst other = (FhrdEmpContractMst) object;
        if ((this.ecmSrgKey == null && other.ecmSrgKey != null) || (this.ecmSrgKey != null && !this.ecmSrgKey.equals(other.ecmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdEmpContractMst[ ecmSrgKey=" + ecmSrgKey + " ]";
    }
    
}
