/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author anu
 */
@Embeddable
public class FhrdPaytransSalaryErrlogPK implements Serializable {
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "SALARY_SRG_KEY")
    private BigDecimal salarySrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SEL_USER_ID")
    private int selUserId;

    public FhrdPaytransSalaryErrlogPK() {
    }

    public FhrdPaytransSalaryErrlogPK(BigDecimal salarySrgKey, int selUserId) {
        this.salarySrgKey = salarySrgKey;
        this.selUserId = selUserId;
    }

    public BigDecimal getSalarySrgKey() {
        return salarySrgKey;
    }

    public void setSalarySrgKey(BigDecimal salarySrgKey) {
        this.salarySrgKey = salarySrgKey;
    }

    public int getSelUserId() {
        return selUserId;
    }

    public void setSelUserId(int selUserId) {
        this.selUserId = selUserId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (salarySrgKey != null ? salarySrgKey.hashCode() : 0);
        hash += (int) selUserId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdPaytransSalaryErrlogPK)) {
            return false;
        }
        FhrdPaytransSalaryErrlogPK other = (FhrdPaytransSalaryErrlogPK) object;
        if ((this.salarySrgKey == null && other.salarySrgKey != null) || (this.salarySrgKey != null && !this.salarySrgKey.equals(other.salarySrgKey))) {
            return false;
        }
        if (this.selUserId != other.selUserId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdPaytransSalaryErrlogPK[ salarySrgKey=" + salarySrgKey + ", selUserId=" + selUserId + " ]";
    }
    
}
