/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_SERVICEMST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdServicemst.findAll", query = "SELECT f FROM FhrdServicemst f"),
    @NamedQuery(name = "FhrdServicemst.findByServicetype", query = "SELECT f FROM FhrdServicemst f WHERE f.servicetype = :servicetype"),
    @NamedQuery(name = "FhrdServicemst.findByServicedesc", query = "SELECT f FROM FhrdServicemst f WHERE f.servicedesc = :servicedesc"),
    @NamedQuery(name = "FhrdServicemst.findByStartdt", query = "SELECT f FROM FhrdServicemst f WHERE f.startdt = :startdt"),
    @NamedQuery(name = "FhrdServicemst.findByEnddt", query = "SELECT f FROM FhrdServicemst f WHERE f.enddt = :enddt"),
    @NamedQuery(name = "FhrdServicemst.findByWorkingflag", query = "SELECT f FROM FhrdServicemst f WHERE f.workingflag = :workingflag"),
    @NamedQuery(name = "FhrdServicemst.findByCreatedt", query = "SELECT f FROM FhrdServicemst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdServicemst.findByUpdatedt", query = "SELECT f FROM FhrdServicemst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdServicemst.findByExportFlag", query = "SELECT f FROM FhrdServicemst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdServicemst.findByImportFlag", query = "SELECT f FROM FhrdServicemst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdServicemst.findBySystemUserid", query = "SELECT f FROM FhrdServicemst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdServicemst.findBySmSrgKey", query = "SELECT f FROM FhrdServicemst f WHERE f.smSrgKey = :smSrgKey")})
public class FhrdServicemst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Size(max = 1)
    @Column(name = "SERVICETYPE")
    private String servicetype;
    @Size(max = 30)
    @Column(name = "SERVICEDESC")
    private String servicedesc;
    @Column(name = "STARTDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startdt;
    @Column(name = "ENDDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date enddt;
    @Size(max = 1)
    @Column(name = "WORKINGFLAG")
    private String workingflag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "SM_SRG_KEY")
    private BigDecimal smSrgKey;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;

    public FhrdServicemst() {
    }

    public FhrdServicemst(BigDecimal smSrgKey) {
        this.smSrgKey = smSrgKey;
    }

    public FhrdServicemst(BigDecimal smSrgKey, Date createdt) {
        this.smSrgKey = smSrgKey;
        this.createdt = createdt;
    }

    public String getServicetype() {
        return servicetype;
    }

    public void setServicetype(String servicetype) {
        this.servicetype = servicetype;
    }

    public String getServicedesc() {
        return servicedesc;
    }

    public void setServicedesc(String servicedesc) {
        this.servicedesc = servicedesc;
    }

    public Date getStartdt() {
        return startdt;
    }

    public void setStartdt(Date startdt) {
        this.startdt = startdt;
    }

    public Date getEnddt() {
        return enddt;
    }

    public void setEnddt(Date enddt) {
        this.enddt = enddt;
    }

    public String getWorkingflag() {
        return workingflag;
    }

    public void setWorkingflag(String workingflag) {
        this.workingflag = workingflag;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public BigDecimal getSmSrgKey() {
        return smSrgKey;
    }

    public void setSmSrgKey(BigDecimal smSrgKey) {
        this.smSrgKey = smSrgKey;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (smSrgKey != null ? smSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdServicemst)) {
            return false;
        }
        FhrdServicemst other = (FhrdServicemst) object;
        if ((this.smSrgKey == null && other.smSrgKey != null) || (this.smSrgKey != null && !this.smSrgKey.equals(other.smSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdServicemst[ smSrgKey=" + smSrgKey + " ]";
    }
    
}
