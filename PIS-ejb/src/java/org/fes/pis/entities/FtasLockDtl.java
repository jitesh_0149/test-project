/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_LOCK_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasLockDtl.findAll", query = "SELECT f FROM FtasLockDtl f"),
    @NamedQuery(name = "FtasLockDtl.findByFromDate", query = "SELECT f FROM FtasLockDtl f WHERE f.fromDate = :fromDate"),
    @NamedQuery(name = "FtasLockDtl.findByToDate", query = "SELECT f FROM FtasLockDtl f WHERE f.toDate = :toDate"),
    @NamedQuery(name = "FtasLockDtl.findByUserId", query = "SELECT f FROM FtasLockDtl f WHERE f.userId = :userId"),
    @NamedQuery(name = "FtasLockDtl.findBySrno", query = "SELECT f FROM FtasLockDtl f WHERE f.srno = :srno"),
    @NamedQuery(name = "FtasLockDtl.findBySelfStatusFlag", query = "SELECT f FROM FtasLockDtl f WHERE f.selfStatusFlag = :selfStatusFlag"),
    @NamedQuery(name = "FtasLockDtl.findBySelfActionBy", query = "SELECT f FROM FtasLockDtl f WHERE f.selfActionBy = :selfActionBy"),
    @NamedQuery(name = "FtasLockDtl.findBySelfActionDate", query = "SELECT f FROM FtasLockDtl f WHERE f.selfActionDate = :selfActionDate"),
    @NamedQuery(name = "FtasLockDtl.findBySelfComment", query = "SELECT f FROM FtasLockDtl f WHERE f.selfComment = :selfComment"),
    @NamedQuery(name = "FtasLockDtl.findByAdminStatusFlag", query = "SELECT f FROM FtasLockDtl f WHERE f.adminStatusFlag = :adminStatusFlag"),
    @NamedQuery(name = "FtasLockDtl.findByAdminActionBy", query = "SELECT f FROM FtasLockDtl f WHERE f.adminActionBy = :adminActionBy"),
    @NamedQuery(name = "FtasLockDtl.findByAdminActionDate", query = "SELECT f FROM FtasLockDtl f WHERE f.adminActionDate = :adminActionDate"),
    @NamedQuery(name = "FtasLockDtl.findByAdminComment", query = "SELECT f FROM FtasLockDtl f WHERE f.adminComment = :adminComment"),
    @NamedQuery(name = "FtasLockDtl.findByAuditStatusFlag", query = "SELECT f FROM FtasLockDtl f WHERE f.auditStatusFlag = :auditStatusFlag"),
    @NamedQuery(name = "FtasLockDtl.findByAuditActionBy", query = "SELECT f FROM FtasLockDtl f WHERE f.auditActionBy = :auditActionBy"),
    @NamedQuery(name = "FtasLockDtl.findByAuditActionDate", query = "SELECT f FROM FtasLockDtl f WHERE f.auditActionDate = :auditActionDate"),
    @NamedQuery(name = "FtasLockDtl.findByAuditComment", query = "SELECT f FROM FtasLockDtl f WHERE f.auditComment = :auditComment"),
    @NamedQuery(name = "FtasLockDtl.findByCreatedt", query = "SELECT f FROM FtasLockDtl f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasLockDtl.findByUpdatedt", query = "SELECT f FROM FtasLockDtl f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasLockDtl.findByExportFlag", query = "SELECT f FROM FtasLockDtl f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FtasLockDtl.findByImportFlag", query = "SELECT f FROM FtasLockDtl f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FtasLockDtl.findBySystemUserid", query = "SELECT f FROM FtasLockDtl f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FtasLockDtl.findByLckdSrgKey", query = "SELECT f FROM FtasLockDtl f WHERE f.lckdSrgKey = :lckdSrgKey"),
    @NamedQuery(name = "FtasLockDtl.findByLckmSrgKey", query = "SELECT f FROM FtasLockDtl f WHERE f.lckmSrgKey = :lckmSrgKey")})
public class FtasLockDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDate;
    @Column(name = "TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date toDate;
    @Column(name = "USER_ID")
    private Integer userId;
    @Column(name = "SRNO")
    private Integer srno;
    @Size(max = 3)
    @Column(name = "SELF_STATUS_FLAG")
    private String selfStatusFlag;
    @Column(name = "SELF_ACTION_BY")
    private Integer selfActionBy;
    @Column(name = "SELF_ACTION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date selfActionDate;
    @Size(max = 100)
    @Column(name = "SELF_COMMENT")
    private String selfComment;
    @Size(max = 1)
    @Column(name = "ADMIN_STATUS_FLAG")
    private String adminStatusFlag;
    @Column(name = "ADMIN_ACTION_BY")
    private Integer adminActionBy;
    @Column(name = "ADMIN_ACTION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date adminActionDate;
    @Size(max = 100)
    @Column(name = "ADMIN_COMMENT")
    private String adminComment;
    @Size(max = 1)
    @Column(name = "AUDIT_STATUS_FLAG")
    private String auditStatusFlag;
    @Column(name = "AUDIT_ACTION_BY")
    private Integer auditActionBy;
    @Column(name = "AUDIT_ACTION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date auditActionDate;
    @Size(max = 100)
    @Column(name = "AUDIT_COMMENT")
    private String auditComment;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "LCKD_SRG_KEY")
    private BigDecimal lckdSrgKey;
    @Column(name = "LCKM_SRG_KEY")
    private BigDecimal lckmSrgKey;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;

    public FtasLockDtl() {
    }

    public FtasLockDtl(BigDecimal lckdSrgKey) {
        this.lckdSrgKey = lckdSrgKey;
    }

    public FtasLockDtl(BigDecimal lckdSrgKey, Date createdt) {
        this.lckdSrgKey = lckdSrgKey;
        this.createdt = createdt;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getSrno() {
        return srno;
    }

    public void setSrno(Integer srno) {
        this.srno = srno;
    }

    public String getSelfStatusFlag() {
        return selfStatusFlag;
    }

    public void setSelfStatusFlag(String selfStatusFlag) {
        this.selfStatusFlag = selfStatusFlag;
    }

    public Integer getSelfActionBy() {
        return selfActionBy;
    }

    public void setSelfActionBy(Integer selfActionBy) {
        this.selfActionBy = selfActionBy;
    }

    public Date getSelfActionDate() {
        return selfActionDate;
    }

    public void setSelfActionDate(Date selfActionDate) {
        this.selfActionDate = selfActionDate;
    }

    public String getSelfComment() {
        return selfComment;
    }

    public void setSelfComment(String selfComment) {
        this.selfComment = selfComment;
    }

    public String getAdminStatusFlag() {
        return adminStatusFlag;
    }

    public void setAdminStatusFlag(String adminStatusFlag) {
        this.adminStatusFlag = adminStatusFlag;
    }

    public Integer getAdminActionBy() {
        return adminActionBy;
    }

    public void setAdminActionBy(Integer adminActionBy) {
        this.adminActionBy = adminActionBy;
    }

    public Date getAdminActionDate() {
        return adminActionDate;
    }

    public void setAdminActionDate(Date adminActionDate) {
        this.adminActionDate = adminActionDate;
    }

    public String getAdminComment() {
        return adminComment;
    }

    public void setAdminComment(String adminComment) {
        this.adminComment = adminComment;
    }

    public String getAuditStatusFlag() {
        return auditStatusFlag;
    }

    public void setAuditStatusFlag(String auditStatusFlag) {
        this.auditStatusFlag = auditStatusFlag;
    }

    public Integer getAuditActionBy() {
        return auditActionBy;
    }

    public void setAuditActionBy(Integer auditActionBy) {
        this.auditActionBy = auditActionBy;
    }

    public Date getAuditActionDate() {
        return auditActionDate;
    }

    public void setAuditActionDate(Date auditActionDate) {
        this.auditActionDate = auditActionDate;
    }

    public String getAuditComment() {
        return auditComment;
    }

    public void setAuditComment(String auditComment) {
        this.auditComment = auditComment;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public BigDecimal getLckdSrgKey() {
        return lckdSrgKey;
    }

    public void setLckdSrgKey(BigDecimal lckdSrgKey) {
        this.lckdSrgKey = lckdSrgKey;
    }

    public BigDecimal getLckmSrgKey() {
        return lckmSrgKey;
    }

    public void setLckmSrgKey(BigDecimal lckmSrgKey) {
        this.lckmSrgKey = lckmSrgKey;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lckdSrgKey != null ? lckdSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasLockDtl)) {
            return false;
        }
        FtasLockDtl other = (FtasLockDtl) object;
        if ((this.lckdSrgKey == null && other.lckdSrgKey != null) || (this.lckdSrgKey != null && !this.lckdSrgKey.equals(other.lckdSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasLockDtl[ lckdSrgKey=" + lckdSrgKey + " ]";
    }
    
}
