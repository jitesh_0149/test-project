/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 *
 * @author anu
 */
@Embeddable
public class FtasHolidayDtlPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "SRNO")
    private short srno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "HOLI_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date holiDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "HM_SRG_KEY")
    private BigDecimal hmSrgKey;

    public FtasHolidayDtlPK() {
    }

    public FtasHolidayDtlPK(short srno, Date holiDate, BigDecimal hmSrgKey) {
        this.srno = srno;
        this.holiDate = holiDate;
        this.hmSrgKey = hmSrgKey;
    }

    public short getSrno() {
        return srno;
    }

    public void setSrno(short srno) {
        this.srno = srno;
    }

    public Date getHoliDate() {
        return holiDate;
    }

    public void setHoliDate(Date holiDate) {
        this.holiDate = holiDate;
    }

    public BigDecimal getHmSrgKey() {
        return hmSrgKey;
    }

    public void setHmSrgKey(BigDecimal hmSrgKey) {
        this.hmSrgKey = hmSrgKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) srno;
        hash += (holiDate != null ? holiDate.hashCode() : 0);
        hash += (hmSrgKey != null ? hmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasHolidayDtlPK)) {
            return false;
        }
        FtasHolidayDtlPK other = (FtasHolidayDtlPK) object;
        if (this.srno != other.srno) {
            return false;
        }
        if ((this.holiDate == null && other.holiDate != null) || (this.holiDate != null && !this.holiDate.equals(other.holiDate))) {
            return false;
        }
        if ((this.hmSrgKey == null && other.hmSrgKey != null) || (this.hmSrgKey != null && !this.hmSrgKey.equals(other.hmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasHolidayDtlPK[ srno=" + srno + ", holiDate=" + holiDate + ", hmSrgKey=" + hmSrgKey + " ]";
    }
    
}
