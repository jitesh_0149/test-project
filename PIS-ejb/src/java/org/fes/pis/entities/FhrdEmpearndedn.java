/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_EMPEARNDEDN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdEmpearndedn.findAll", query = "SELECT f FROM FhrdEmpearndedn f"),
    @NamedQuery(name = "FhrdEmpearndedn.findByEarndednSrno", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.earndednSrno = :earndednSrno"),
    @NamedQuery(name = "FhrdEmpearndedn.findByEarndedncd", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.earndedncd = :earndedncd"),
    @NamedQuery(name = "FhrdEmpearndedn.findBySrno", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.srno = :srno"),
    @NamedQuery(name = "FhrdEmpearndedn.findByEarndednFlag", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.earndednFlag = :earndednFlag"),
    @NamedQuery(name = "FhrdEmpearndedn.findByEarndednType", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.earndednType = :earndednType"),
    @NamedQuery(name = "FhrdEmpearndedn.findByFormulaAmtFlag", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.formulaAmtFlag = :formulaAmtFlag"),
    @NamedQuery(name = "FhrdEmpearndedn.findByFormulaAmtValue", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.formulaAmtValue = :formulaAmtValue"),
    @NamedQuery(name = "FhrdEmpearndedn.findByStartDate", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdEmpearndedn.findByOpeningDate", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.openingDate = :openingDate"),
    @NamedQuery(name = "FhrdEmpearndedn.findByClosingDate", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.closingDate = :closingDate"),
    @NamedQuery(name = "FhrdEmpearndedn.findByEndDate", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FhrdEmpearndedn.findByRulesrno", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.rulesrno = :rulesrno"),
    @NamedQuery(name = "FhrdEmpearndedn.findByLevelNo", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.levelNo = :levelNo"),
    @NamedQuery(name = "FhrdEmpearndedn.findByPreferenceNo", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.preferenceNo = :preferenceNo"),
    @NamedQuery(name = "FhrdEmpearndedn.findByPrintInPaySlip", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.printInPaySlip = :printInPaySlip"),
    @NamedQuery(name = "FhrdEmpearndedn.findByPrintIf0Flag", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.printIf0Flag = :printIf0Flag"),
    @NamedQuery(name = "FhrdEmpearndedn.findByFirstEntryFlag", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.firstEntryFlag = :firstEntryFlag"),
    @NamedQuery(name = "FhrdEmpearndedn.findByPayableOnValue", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.payableOnValue = :payableOnValue"),
    @NamedQuery(name = "FhrdEmpearndedn.findByCreatedt", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdEmpearndedn.findByUpdatedt", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdEmpearndedn.findBySystemUserid", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdEmpearndedn.findByExportFlag", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdEmpearndedn.findByImportFlag", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdEmpearndedn.findByRuleConfigUnit", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.ruleConfigUnit = :ruleConfigUnit"),
    @NamedQuery(name = "FhrdEmpearndedn.findByEedSrgKey", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.eedSrgKey = :eedSrgKey"),
    @NamedQuery(name = "FhrdEmpearndedn.findByWefDate", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.wefDate = :wefDate"),
    @NamedQuery(name = "FhrdEmpearndedn.findByReplacedRuleSrno", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.replacedRuleSrno = :replacedRuleSrno"),
    @NamedQuery(name = "FhrdEmpearndedn.findByBasecodeFlag", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.basecodeFlag = :basecodeFlag"),
    @NamedQuery(name = "FhrdEmpearndedn.findByPrintOrder", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.printOrder = :printOrder"),
    @NamedQuery(name = "FhrdEmpearndedn.findByEdmAltSrno", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.edmAltSrno = :edmAltSrno"),
    @NamedQuery(name = "FhrdEmpearndedn.findByTranYear", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FhrdEmpearndedn.findByDesigDependent", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.desigDependent = :desigDependent"),
    @NamedQuery(name = "FhrdEmpearndedn.findByFirstTimeEditDone", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.firstTimeEditDone = :firstTimeEditDone"),
    @NamedQuery(name = "FhrdEmpearndedn.findByContStartDate", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.contStartDate = :contStartDate"),
    @NamedQuery(name = "FhrdEmpearndedn.findByContEndDate", query = "SELECT f FROM FhrdEmpearndedn f WHERE f.contEndDate = :contEndDate")})
public class FhrdEmpearndedn implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EARNDEDN_SRNO")
    private int earndednSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "EARNDEDNCD")
    private String earndedncd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SRNO")
    private int srno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EARNDEDN_FLAG")
    private String earndednFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EARNDEDN_TYPE")
    private String earndednType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "FORMULA_AMT_FLAG")
    private String formulaAmtFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "FORMULA_AMT_VALUE")
    private String formulaAmtValue;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OPENING_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date openingDate;
    @Column(name = "CLOSING_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date closingDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RULESRNO")
    private int rulesrno;
    @Column(name = "LEVEL_NO")
    private Short levelNo;
    @Column(name = "PREFERENCE_NO")
    private Short preferenceNo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PRINT_IN_PAY_SLIP")
    private String printInPaySlip;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PRINT_IF_0_FLAG")
    private String printIf0Flag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "FIRST_ENTRY_FLAG")
    private String firstEntryFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "PAYABLE_ON_VALUE")
    private String payableOnValue;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "RULE_CONFIG_UNIT")
    private String ruleConfigUnit;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "EED_SRG_KEY")
    private BigDecimal eedSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WEF_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date wefDate;
    @Column(name = "REPLACED_RULE_SRNO")
    private BigDecimal replacedRuleSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "BASECODE_FLAG")
    private String basecodeFlag;
    @Column(name = "PRINT_ORDER")
    private Short printOrder;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EDM_ALT_SRNO")
    private int edmAltSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "DESIG_DEPENDENT")
    private String desigDependent;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "FIRST_TIME_EDIT_DONE")
    private String firstTimeEditDone;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONT_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date contStartDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONT_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date contEndDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ptedEedSrgKey")
    private Collection<FhrdPaytransearndedn> fhrdPaytransearndednCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "eedSrgKey")
    private Collection<FhrdPaytransMonvar> fhrdPaytransMonvarCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "RMGM_SRG_KEY", referencedColumnName = "RMGM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdRuleManagGroupMst rmgmSrgKey;
    @JoinColumn(name = "ECGM_SRG_KEY", referencedColumnName = "ECGM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdEmpContractGroupMst ecgmSrgKey;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst userId;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "EDM_SRG_KEY", referencedColumnName = "EDM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdEarndednmst edmSrgKey;
    @JoinColumn(name = "ED_SRG_KEY", referencedColumnName = "ED_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdEarndedn edSrgKey;

    public FhrdEmpearndedn() {
    }

    public FhrdEmpearndedn(BigDecimal eedSrgKey) {
        this.eedSrgKey = eedSrgKey;
    }

    public FhrdEmpearndedn(BigDecimal eedSrgKey, int earndednSrno, String earndedncd, int srno, String earndednFlag, String earndednType, String formulaAmtFlag, String formulaAmtValue, Date startDate, Date openingDate, int rulesrno, String printInPaySlip, String printIf0Flag, String firstEntryFlag, String payableOnValue, Date createdt, String ruleConfigUnit, Date wefDate, String basecodeFlag, int edmAltSrno, short tranYear, String desigDependent, String firstTimeEditDone, Date contStartDate, Date contEndDate) {
        this.eedSrgKey = eedSrgKey;
        this.earndednSrno = earndednSrno;
        this.earndedncd = earndedncd;
        this.srno = srno;
        this.earndednFlag = earndednFlag;
        this.earndednType = earndednType;
        this.formulaAmtFlag = formulaAmtFlag;
        this.formulaAmtValue = formulaAmtValue;
        this.startDate = startDate;
        this.openingDate = openingDate;
        this.rulesrno = rulesrno;
        this.printInPaySlip = printInPaySlip;
        this.printIf0Flag = printIf0Flag;
        this.firstEntryFlag = firstEntryFlag;
        this.payableOnValue = payableOnValue;
        this.createdt = createdt;
        this.ruleConfigUnit = ruleConfigUnit;
        this.wefDate = wefDate;
        this.basecodeFlag = basecodeFlag;
        this.edmAltSrno = edmAltSrno;
        this.tranYear = tranYear;
        this.desigDependent = desigDependent;
        this.firstTimeEditDone = firstTimeEditDone;
        this.contStartDate = contStartDate;
        this.contEndDate = contEndDate;
    }

    public int getEarndednSrno() {
        return earndednSrno;
    }

    public void setEarndednSrno(int earndednSrno) {
        this.earndednSrno = earndednSrno;
    }

    public String getEarndedncd() {
        return earndedncd;
    }

    public void setEarndedncd(String earndedncd) {
        this.earndedncd = earndedncd;
    }

    public int getSrno() {
        return srno;
    }

    public void setSrno(int srno) {
        this.srno = srno;
    }

    public String getEarndednFlag() {
        return earndednFlag;
    }

    public void setEarndednFlag(String earndednFlag) {
        this.earndednFlag = earndednFlag;
    }

    public String getEarndednType() {
        return earndednType;
    }

    public void setEarndednType(String earndednType) {
        this.earndednType = earndednType;
    }

    public String getFormulaAmtFlag() {
        return formulaAmtFlag;
    }

    public void setFormulaAmtFlag(String formulaAmtFlag) {
        this.formulaAmtFlag = formulaAmtFlag;
    }

    public String getFormulaAmtValue() {
        return formulaAmtValue;
    }

    public void setFormulaAmtValue(String formulaAmtValue) {
        this.formulaAmtValue = formulaAmtValue;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(Date openingDate) {
        this.openingDate = openingDate;
    }

    public Date getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(Date closingDate) {
        this.closingDate = closingDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getRulesrno() {
        return rulesrno;
    }

    public void setRulesrno(int rulesrno) {
        this.rulesrno = rulesrno;
    }

    public Short getLevelNo() {
        return levelNo;
    }

    public void setLevelNo(Short levelNo) {
        this.levelNo = levelNo;
    }

    public Short getPreferenceNo() {
        return preferenceNo;
    }

    public void setPreferenceNo(Short preferenceNo) {
        this.preferenceNo = preferenceNo;
    }

    public String getPrintInPaySlip() {
        return printInPaySlip;
    }

    public void setPrintInPaySlip(String printInPaySlip) {
        this.printInPaySlip = printInPaySlip;
    }

    public String getPrintIf0Flag() {
        return printIf0Flag;
    }

    public void setPrintIf0Flag(String printIf0Flag) {
        this.printIf0Flag = printIf0Flag;
    }

    public String getFirstEntryFlag() {
        return firstEntryFlag;
    }

    public void setFirstEntryFlag(String firstEntryFlag) {
        this.firstEntryFlag = firstEntryFlag;
    }

    public String getPayableOnValue() {
        return payableOnValue;
    }

    public void setPayableOnValue(String payableOnValue) {
        this.payableOnValue = payableOnValue;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public String getRuleConfigUnit() {
        return ruleConfigUnit;
    }

    public void setRuleConfigUnit(String ruleConfigUnit) {
        this.ruleConfigUnit = ruleConfigUnit;
    }

    public BigDecimal getEedSrgKey() {
        return eedSrgKey;
    }

    public void setEedSrgKey(BigDecimal eedSrgKey) {
        this.eedSrgKey = eedSrgKey;
    }

    public Date getWefDate() {
        return wefDate;
    }

    public void setWefDate(Date wefDate) {
        this.wefDate = wefDate;
    }

    public BigDecimal getReplacedRuleSrno() {
        return replacedRuleSrno;
    }

    public void setReplacedRuleSrno(BigDecimal replacedRuleSrno) {
        this.replacedRuleSrno = replacedRuleSrno;
    }

    public String getBasecodeFlag() {
        return basecodeFlag;
    }

    public void setBasecodeFlag(String basecodeFlag) {
        this.basecodeFlag = basecodeFlag;
    }

    public Short getPrintOrder() {
        return printOrder;
    }

    public void setPrintOrder(Short printOrder) {
        this.printOrder = printOrder;
    }

    public int getEdmAltSrno() {
        return edmAltSrno;
    }

    public void setEdmAltSrno(int edmAltSrno) {
        this.edmAltSrno = edmAltSrno;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public String getDesigDependent() {
        return desigDependent;
    }

    public void setDesigDependent(String desigDependent) {
        this.desigDependent = desigDependent;
    }

    public String getFirstTimeEditDone() {
        return firstTimeEditDone;
    }

    public void setFirstTimeEditDone(String firstTimeEditDone) {
        this.firstTimeEditDone = firstTimeEditDone;
    }

    public Date getContStartDate() {
        return contStartDate;
    }

    public void setContStartDate(Date contStartDate) {
        this.contStartDate = contStartDate;
    }

    public Date getContEndDate() {
        return contEndDate;
    }

    public void setContEndDate(Date contEndDate) {
        this.contEndDate = contEndDate;
    }

    @XmlTransient
    public Collection<FhrdPaytransearndedn> getFhrdPaytransearndednCollection() {
        return fhrdPaytransearndednCollection;
    }

    public void setFhrdPaytransearndednCollection(Collection<FhrdPaytransearndedn> fhrdPaytransearndednCollection) {
        this.fhrdPaytransearndednCollection = fhrdPaytransearndednCollection;
    }

    @XmlTransient
    public Collection<FhrdPaytransMonvar> getFhrdPaytransMonvarCollection() {
        return fhrdPaytransMonvarCollection;
    }

    public void setFhrdPaytransMonvarCollection(Collection<FhrdPaytransMonvar> fhrdPaytransMonvarCollection) {
        this.fhrdPaytransMonvarCollection = fhrdPaytransMonvarCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdRuleManagGroupMst getRmgmSrgKey() {
        return rmgmSrgKey;
    }

    public void setRmgmSrgKey(FhrdRuleManagGroupMst rmgmSrgKey) {
        this.rmgmSrgKey = rmgmSrgKey;
    }

    public FhrdEmpContractGroupMst getEcgmSrgKey() {
        return ecgmSrgKey;
    }

    public void setEcgmSrgKey(FhrdEmpContractGroupMst ecgmSrgKey) {
        this.ecgmSrgKey = ecgmSrgKey;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getUserId() {
        return userId;
    }

    public void setUserId(FhrdEmpmst userId) {
        this.userId = userId;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEarndednmst getEdmSrgKey() {
        return edmSrgKey;
    }

    public void setEdmSrgKey(FhrdEarndednmst edmSrgKey) {
        this.edmSrgKey = edmSrgKey;
    }

    public FhrdEarndedn getEdSrgKey() {
        return edSrgKey;
    }

    public void setEdSrgKey(FhrdEarndedn edSrgKey) {
        this.edSrgKey = edSrgKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (eedSrgKey != null ? eedSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdEmpearndedn)) {
            return false;
        }
        FhrdEmpearndedn other = (FhrdEmpearndedn) object;
        if ((this.eedSrgKey == null && other.eedSrgKey != null) || (this.eedSrgKey != null && !this.eedSrgKey.equals(other.eedSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdEmpearndedn[ eedSrgKey=" + eedSrgKey + " ]";
    }
    
}
