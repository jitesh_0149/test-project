/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_PAYTRANSLEAVE_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdPaytransleaveMst.findAll", query = "SELECT f FROM FhrdPaytransleaveMst f"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByOrgAttendanceFromDate", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.orgAttendanceFromDate = :orgAttendanceFromDate"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByEmpAttendanceFromDate", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.empAttendanceFromDate = :empAttendanceFromDate"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByEmpAttendanceToDate", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.empAttendanceToDate = :empAttendanceToDate"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByOrgAttendanceToDate", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.orgAttendanceToDate = :orgAttendanceToDate"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByOrgPayableDays", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.orgPayableDays = :orgPayableDays"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByOrgWorkingDays", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.orgWorkingDays = :orgWorkingDays"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByEmpPayableDays", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.empPayableDays = :empPayableDays"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByEmpWorkingDays", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.empWorkingDays = :empWorkingDays"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByActualAttendance", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.actualAttendance = :actualAttendance"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByNoOfHolidays", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.noOfHolidays = :noOfHolidays"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByUpdateCause", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.updateCause = :updateCause"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByCreatedt", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByUpdatedt", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findBySystemUserid", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByExportFlag", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByImportFlag", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByEmpNonPayableDays", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.empNonPayableDays = :empNonPayableDays"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByConsiderAsAttendance", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.considerAsAttendance = :considerAsAttendance"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByLeaveEarnableDays", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.leaveEarnableDays = :leaveEarnableDays"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByPlmSrgKey", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.plmSrgKey = :plmSrgKey"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByDeductDays", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.deductDays = :deductDays"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByTranYear", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByAttdSrgKey", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.attdSrgKey = :attdSrgKey"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByRelieveNonPayableDays", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.relieveNonPayableDays = :relieveNonPayableDays"),
    @NamedQuery(name = "FhrdPaytransleaveMst.findByNoOfLeaveEncash", query = "SELECT f FROM FhrdPaytransleaveMst f WHERE f.noOfLeaveEncash = :noOfLeaveEncash")})
public class FhrdPaytransleaveMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_ATTENDANCE_FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgAttendanceFromDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_ATTENDANCE_FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date empAttendanceFromDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_ATTENDANCE_TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date empAttendanceToDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_ATTENDANCE_TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgAttendanceToDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_PAYABLE_DAYS")
    private BigDecimal orgPayableDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_WORKING_DAYS")
    private BigDecimal orgWorkingDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_PAYABLE_DAYS")
    private BigDecimal empPayableDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_WORKING_DAYS")
    private BigDecimal empWorkingDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTUAL_ATTENDANCE")
    private BigDecimal actualAttendance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NO_OF_HOLIDAYS")
    private BigDecimal noOfHolidays;
    @Size(max = 1)
    @Column(name = "UPDATE_CAUSE")
    private String updateCause;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_NON_PAYABLE_DAYS")
    private BigDecimal empNonPayableDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONSIDER_AS_ATTENDANCE")
    private BigDecimal considerAsAttendance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LEAVE_EARNABLE_DAYS")
    private BigDecimal leaveEarnableDays;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PLM_SRG_KEY")
    private BigDecimal plmSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DEDUCT_DAYS")
    private BigDecimal deductDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATTD_SRG_KEY")
    private BigInteger attdSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RELIEVE_NON_PAYABLE_DAYS")
    private BigDecimal relieveNonPayableDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NO_OF_LEAVE_ENCASH")
    private BigDecimal noOfLeaveEncash;
    @OneToMany(mappedBy = "plmSrgKey")
    private Collection<FhrdPaytransleaveEarn> fhrdPaytransleaveEarnCollection;
    @OneToMany(mappedBy = "plmSrgKey")
    private Collection<FhrdPaytransleaveDtl> fhrdPaytransleaveDtlCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst userId;

    public FhrdPaytransleaveMst() {
    }

    public FhrdPaytransleaveMst(BigDecimal plmSrgKey) {
        this.plmSrgKey = plmSrgKey;
    }

    public FhrdPaytransleaveMst(BigDecimal plmSrgKey, Date orgAttendanceFromDate, Date empAttendanceFromDate, Date empAttendanceToDate, Date orgAttendanceToDate, BigDecimal orgPayableDays, BigDecimal orgWorkingDays, BigDecimal empPayableDays, BigDecimal empWorkingDays, BigDecimal actualAttendance, BigDecimal noOfHolidays, Date createdt, BigDecimal empNonPayableDays, BigDecimal considerAsAttendance, BigDecimal leaveEarnableDays, BigDecimal deductDays, short tranYear, BigInteger attdSrgKey, BigDecimal relieveNonPayableDays, BigDecimal noOfLeaveEncash) {
        this.plmSrgKey = plmSrgKey;
        this.orgAttendanceFromDate = orgAttendanceFromDate;
        this.empAttendanceFromDate = empAttendanceFromDate;
        this.empAttendanceToDate = empAttendanceToDate;
        this.orgAttendanceToDate = orgAttendanceToDate;
        this.orgPayableDays = orgPayableDays;
        this.orgWorkingDays = orgWorkingDays;
        this.empPayableDays = empPayableDays;
        this.empWorkingDays = empWorkingDays;
        this.actualAttendance = actualAttendance;
        this.noOfHolidays = noOfHolidays;
        this.createdt = createdt;
        this.empNonPayableDays = empNonPayableDays;
        this.considerAsAttendance = considerAsAttendance;
        this.leaveEarnableDays = leaveEarnableDays;
        this.deductDays = deductDays;
        this.tranYear = tranYear;
        this.attdSrgKey = attdSrgKey;
        this.relieveNonPayableDays = relieveNonPayableDays;
        this.noOfLeaveEncash = noOfLeaveEncash;
    }

    public Date getOrgAttendanceFromDate() {
        return orgAttendanceFromDate;
    }

    public void setOrgAttendanceFromDate(Date orgAttendanceFromDate) {
        this.orgAttendanceFromDate = orgAttendanceFromDate;
    }

    public Date getEmpAttendanceFromDate() {
        return empAttendanceFromDate;
    }

    public void setEmpAttendanceFromDate(Date empAttendanceFromDate) {
        this.empAttendanceFromDate = empAttendanceFromDate;
    }

    public Date getEmpAttendanceToDate() {
        return empAttendanceToDate;
    }

    public void setEmpAttendanceToDate(Date empAttendanceToDate) {
        this.empAttendanceToDate = empAttendanceToDate;
    }

    public Date getOrgAttendanceToDate() {
        return orgAttendanceToDate;
    }

    public void setOrgAttendanceToDate(Date orgAttendanceToDate) {
        this.orgAttendanceToDate = orgAttendanceToDate;
    }

    public BigDecimal getOrgPayableDays() {
        return orgPayableDays;
    }

    public void setOrgPayableDays(BigDecimal orgPayableDays) {
        this.orgPayableDays = orgPayableDays;
    }

    public BigDecimal getOrgWorkingDays() {
        return orgWorkingDays;
    }

    public void setOrgWorkingDays(BigDecimal orgWorkingDays) {
        this.orgWorkingDays = orgWorkingDays;
    }

    public BigDecimal getEmpPayableDays() {
        return empPayableDays;
    }

    public void setEmpPayableDays(BigDecimal empPayableDays) {
        this.empPayableDays = empPayableDays;
    }

    public BigDecimal getEmpWorkingDays() {
        return empWorkingDays;
    }

    public void setEmpWorkingDays(BigDecimal empWorkingDays) {
        this.empWorkingDays = empWorkingDays;
    }

    public BigDecimal getActualAttendance() {
        return actualAttendance;
    }

    public void setActualAttendance(BigDecimal actualAttendance) {
        this.actualAttendance = actualAttendance;
    }

    public BigDecimal getNoOfHolidays() {
        return noOfHolidays;
    }

    public void setNoOfHolidays(BigDecimal noOfHolidays) {
        this.noOfHolidays = noOfHolidays;
    }

    public String getUpdateCause() {
        return updateCause;
    }

    public void setUpdateCause(String updateCause) {
        this.updateCause = updateCause;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public BigDecimal getEmpNonPayableDays() {
        return empNonPayableDays;
    }

    public void setEmpNonPayableDays(BigDecimal empNonPayableDays) {
        this.empNonPayableDays = empNonPayableDays;
    }

    public BigDecimal getConsiderAsAttendance() {
        return considerAsAttendance;
    }

    public void setConsiderAsAttendance(BigDecimal considerAsAttendance) {
        this.considerAsAttendance = considerAsAttendance;
    }

    public BigDecimal getLeaveEarnableDays() {
        return leaveEarnableDays;
    }

    public void setLeaveEarnableDays(BigDecimal leaveEarnableDays) {
        this.leaveEarnableDays = leaveEarnableDays;
    }

    public BigDecimal getPlmSrgKey() {
        return plmSrgKey;
    }

    public void setPlmSrgKey(BigDecimal plmSrgKey) {
        this.plmSrgKey = plmSrgKey;
    }

    public BigDecimal getDeductDays() {
        return deductDays;
    }

    public void setDeductDays(BigDecimal deductDays) {
        this.deductDays = deductDays;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public BigInteger getAttdSrgKey() {
        return attdSrgKey;
    }

    public void setAttdSrgKey(BigInteger attdSrgKey) {
        this.attdSrgKey = attdSrgKey;
    }

    public BigDecimal getRelieveNonPayableDays() {
        return relieveNonPayableDays;
    }

    public void setRelieveNonPayableDays(BigDecimal relieveNonPayableDays) {
        this.relieveNonPayableDays = relieveNonPayableDays;
    }

    public BigDecimal getNoOfLeaveEncash() {
        return noOfLeaveEncash;
    }

    public void setNoOfLeaveEncash(BigDecimal noOfLeaveEncash) {
        this.noOfLeaveEncash = noOfLeaveEncash;
    }

    @XmlTransient
    public Collection<FhrdPaytransleaveEarn> getFhrdPaytransleaveEarnCollection() {
        return fhrdPaytransleaveEarnCollection;
    }

    public void setFhrdPaytransleaveEarnCollection(Collection<FhrdPaytransleaveEarn> fhrdPaytransleaveEarnCollection) {
        this.fhrdPaytransleaveEarnCollection = fhrdPaytransleaveEarnCollection;
    }

    @XmlTransient
    public Collection<FhrdPaytransleaveDtl> getFhrdPaytransleaveDtlCollection() {
        return fhrdPaytransleaveDtlCollection;
    }

    public void setFhrdPaytransleaveDtlCollection(Collection<FhrdPaytransleaveDtl> fhrdPaytransleaveDtlCollection) {
        this.fhrdPaytransleaveDtlCollection = fhrdPaytransleaveDtlCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUserId() {
        return userId;
    }

    public void setUserId(FhrdEmpmst userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (plmSrgKey != null ? plmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdPaytransleaveMst)) {
            return false;
        }
        FhrdPaytransleaveMst other = (FhrdPaytransleaveMst) object;
        if ((this.plmSrgKey == null && other.plmSrgKey != null) || (this.plmSrgKey != null && !this.plmSrgKey.equals(other.plmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdPaytransleaveMst[ plmSrgKey=" + plmSrgKey + " ]";
    }
    
}
