/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_TRAINING_ORG_UNIT_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasTrainingOrgUnitDtl.findAll", query = "SELECT f FROM FtasTrainingOrgUnitDtl f"),
    @NamedQuery(name = "FtasTrainingOrgUnitDtl.findBySystOrgUnitSrno", query = "SELECT f FROM FtasTrainingOrgUnitDtl f WHERE f.systOrgUnitSrno = :systOrgUnitSrno"),
    @NamedQuery(name = "FtasTrainingOrgUnitDtl.findBySubSrno", query = "SELECT f FROM FtasTrainingOrgUnitDtl f WHERE f.subSrno = :subSrno"),
    @NamedQuery(name = "FtasTrainingOrgUnitDtl.findByCreatedt", query = "SELECT f FROM FtasTrainingOrgUnitDtl f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasTrainingOrgUnitDtl.findByUpdatedt", query = "SELECT f FROM FtasTrainingOrgUnitDtl f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasTrainingOrgUnitDtl.findByTroudSrgKey", query = "SELECT f FROM FtasTrainingOrgUnitDtl f WHERE f.troudSrgKey = :troudSrgKey"),
    @NamedQuery(name = "FtasTrainingOrgUnitDtl.findByExportFlag", query = "SELECT f FROM FtasTrainingOrgUnitDtl f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FtasTrainingOrgUnitDtl.findByImportFlag", query = "SELECT f FROM FtasTrainingOrgUnitDtl f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FtasTrainingOrgUnitDtl.findBySystemUserid", query = "SELECT f FROM FtasTrainingOrgUnitDtl f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FtasTrainingOrgUnitDtl.findByTranYear", query = "SELECT f FROM FtasTrainingOrgUnitDtl f WHERE f.tranYear = :tranYear")})
public class FtasTrainingOrgUnitDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SYST_ORG_UNIT_SRNO")
    private int systOrgUnitSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SUB_SRNO")
    private int subSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "TROUD_SRG_KEY")
    private BigDecimal troudSrgKey;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @OneToMany(mappedBy = "troudSrgKey")
    private Collection<FtasAbsentees> ftasAbsenteesCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "TRANN_SRG_KEY", referencedColumnName = "TRANN_SRG_KEY")
    @ManyToOne(optional = false)
    private FtasTrainingMst trannSrgKey;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FtasTrainingOrgUnitDtl() {
    }

    public FtasTrainingOrgUnitDtl(BigDecimal troudSrgKey) {
        this.troudSrgKey = troudSrgKey;
    }

    public FtasTrainingOrgUnitDtl(BigDecimal troudSrgKey, int systOrgUnitSrno, int subSrno, Date createdt, short tranYear) {
        this.troudSrgKey = troudSrgKey;
        this.systOrgUnitSrno = systOrgUnitSrno;
        this.subSrno = subSrno;
        this.createdt = createdt;
        this.tranYear = tranYear;
    }

    public int getSystOrgUnitSrno() {
        return systOrgUnitSrno;
    }

    public void setSystOrgUnitSrno(int systOrgUnitSrno) {
        this.systOrgUnitSrno = systOrgUnitSrno;
    }

    public int getSubSrno() {
        return subSrno;
    }

    public void setSubSrno(int subSrno) {
        this.subSrno = subSrno;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public BigDecimal getTroudSrgKey() {
        return troudSrgKey;
    }

    public void setTroudSrgKey(BigDecimal troudSrgKey) {
        this.troudSrgKey = troudSrgKey;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    @XmlTransient
    public Collection<FtasAbsentees> getFtasAbsenteesCollection() {
        return ftasAbsenteesCollection;
    }

    public void setFtasAbsenteesCollection(Collection<FtasAbsentees> ftasAbsenteesCollection) {
        this.ftasAbsenteesCollection = ftasAbsenteesCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasTrainingMst getTrannSrgKey() {
        return trannSrgKey;
    }

    public void setTrannSrgKey(FtasTrainingMst trannSrgKey) {
        this.trannSrgKey = trannSrgKey;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (troudSrgKey != null ? troudSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasTrainingOrgUnitDtl)) {
            return false;
        }
        FtasTrainingOrgUnitDtl other = (FtasTrainingOrgUnitDtl) object;
        if ((this.troudSrgKey == null && other.troudSrgKey != null) || (this.troudSrgKey != null && !this.troudSrgKey.equals(other.troudSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasTrainingOrgUnitDtl[ troudSrgKey=" + troudSrgKey + " ]";
    }
    
}
