/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "BMWP_ACTIVITY_BUDGET_LINK")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmwpActivityBudgetLink.findAll", query = "SELECT b FROM BmwpActivityBudgetLink b"),
    @NamedQuery(name = "BmwpActivityBudgetLink.findByAblUniqueSrno", query = "SELECT b FROM BmwpActivityBudgetLink b WHERE b.ablUniqueSrno = :ablUniqueSrno"),
    @NamedQuery(name = "BmwpActivityBudgetLink.findByAblSrno", query = "SELECT b FROM BmwpActivityBudgetLink b WHERE b.ablSrno = :ablSrno"),
    @NamedQuery(name = "BmwpActivityBudgetLink.findByCreatedt", query = "SELECT b FROM BmwpActivityBudgetLink b WHERE b.createdt = :createdt"),
    @NamedQuery(name = "BmwpActivityBudgetLink.findByUpdatedt", query = "SELECT b FROM BmwpActivityBudgetLink b WHERE b.updatedt = :updatedt"),
    @NamedQuery(name = "BmwpActivityBudgetLink.findBySystemUserid", query = "SELECT b FROM BmwpActivityBudgetLink b WHERE b.systemUserid = :systemUserid"),
    @NamedQuery(name = "BmwpActivityBudgetLink.findByExportFlag", query = "SELECT b FROM BmwpActivityBudgetLink b WHERE b.exportFlag = :exportFlag"),
    @NamedQuery(name = "BmwpActivityBudgetLink.findByImportFlag", query = "SELECT b FROM BmwpActivityBudgetLink b WHERE b.importFlag = :importFlag"),
    @NamedQuery(name = "BmwpActivityBudgetLink.findByAblStartDate", query = "SELECT b FROM BmwpActivityBudgetLink b WHERE b.ablStartDate = :ablStartDate"),
    @NamedQuery(name = "BmwpActivityBudgetLink.findByAblEndDate", query = "SELECT b FROM BmwpActivityBudgetLink b WHERE b.ablEndDate = :ablEndDate"),
    @NamedQuery(name = "BmwpActivityBudgetLink.findByAblEndDatePossible", query = "SELECT b FROM BmwpActivityBudgetLink b WHERE b.ablEndDatePossible = :ablEndDatePossible"),
    @NamedQuery(name = "BmwpActivityBudgetLink.findByRemark", query = "SELECT b FROM BmwpActivityBudgetLink b WHERE b.remark = :remark")})
public class BmwpActivityBudgetLink implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ABL_UNIQUE_SRNO")
    private BigDecimal ablUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ABL_SRNO")
    private int ablSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ABL_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ablStartDate;
    @Column(name = "ABL_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ablEndDate;
    @Column(name = "ABL_END_DATE_POSSIBLE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ablEndDatePossible;
    @Size(max = 1000)
    @Column(name = "REMARK")
    private String remark;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "ABL_BM_UNIQUE_SRNO", referencedColumnName = "BM_UNIQUE_SRNO")
    @ManyToOne(optional = false)
    private BmwpBudgetMst ablBmUniqueSrno;
    @JoinColumn(name = "ABL_AM_UNIQUE_SRNO", referencedColumnName = "AM_UNIQUE_SRNO")
    @ManyToOne(optional = false)
    private BmwpActivityMst ablAmUniqueSrno;
    @OneToMany(mappedBy = "ablParentUniqueSrno")
    private Collection<BmwpActivityBudgetLink> bmwpActivityBudgetLinkCollection;
    @JoinColumn(name = "ABL_PARENT_UNIQUE_SRNO", referencedColumnName = "ABL_UNIQUE_SRNO")
    @ManyToOne
    private BmwpActivityBudgetLink ablParentUniqueSrno;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "abdAblUniqueSrno")
    private Collection<BmwpActivityBudgetDtl> bmwpActivityBudgetDtlCollection;

    public BmwpActivityBudgetLink() {
    }

    public BmwpActivityBudgetLink(BigDecimal ablUniqueSrno) {
        this.ablUniqueSrno = ablUniqueSrno;
    }

    public BmwpActivityBudgetLink(BigDecimal ablUniqueSrno, int ablSrno, Date createdt, Date ablStartDate) {
        this.ablUniqueSrno = ablUniqueSrno;
        this.ablSrno = ablSrno;
        this.createdt = createdt;
        this.ablStartDate = ablStartDate;
    }

    public BigDecimal getAblUniqueSrno() {
        return ablUniqueSrno;
    }

    public void setAblUniqueSrno(BigDecimal ablUniqueSrno) {
        this.ablUniqueSrno = ablUniqueSrno;
    }

    public int getAblSrno() {
        return ablSrno;
    }

    public void setAblSrno(int ablSrno) {
        this.ablSrno = ablSrno;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Date getAblStartDate() {
        return ablStartDate;
    }

    public void setAblStartDate(Date ablStartDate) {
        this.ablStartDate = ablStartDate;
    }

    public Date getAblEndDate() {
        return ablEndDate;
    }

    public void setAblEndDate(Date ablEndDate) {
        this.ablEndDate = ablEndDate;
    }

    public Date getAblEndDatePossible() {
        return ablEndDatePossible;
    }

    public void setAblEndDatePossible(Date ablEndDatePossible) {
        this.ablEndDatePossible = ablEndDatePossible;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public BmwpBudgetMst getAblBmUniqueSrno() {
        return ablBmUniqueSrno;
    }

    public void setAblBmUniqueSrno(BmwpBudgetMst ablBmUniqueSrno) {
        this.ablBmUniqueSrno = ablBmUniqueSrno;
    }

    public BmwpActivityMst getAblAmUniqueSrno() {
        return ablAmUniqueSrno;
    }

    public void setAblAmUniqueSrno(BmwpActivityMst ablAmUniqueSrno) {
        this.ablAmUniqueSrno = ablAmUniqueSrno;
    }

    @XmlTransient
    public Collection<BmwpActivityBudgetLink> getBmwpActivityBudgetLinkCollection() {
        return bmwpActivityBudgetLinkCollection;
    }

    public void setBmwpActivityBudgetLinkCollection(Collection<BmwpActivityBudgetLink> bmwpActivityBudgetLinkCollection) {
        this.bmwpActivityBudgetLinkCollection = bmwpActivityBudgetLinkCollection;
    }

    public BmwpActivityBudgetLink getAblParentUniqueSrno() {
        return ablParentUniqueSrno;
    }

    public void setAblParentUniqueSrno(BmwpActivityBudgetLink ablParentUniqueSrno) {
        this.ablParentUniqueSrno = ablParentUniqueSrno;
    }

    @XmlTransient
    public Collection<BmwpActivityBudgetDtl> getBmwpActivityBudgetDtlCollection() {
        return bmwpActivityBudgetDtlCollection;
    }

    public void setBmwpActivityBudgetDtlCollection(Collection<BmwpActivityBudgetDtl> bmwpActivityBudgetDtlCollection) {
        this.bmwpActivityBudgetDtlCollection = bmwpActivityBudgetDtlCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ablUniqueSrno != null ? ablUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmwpActivityBudgetLink)) {
            return false;
        }
        BmwpActivityBudgetLink other = (BmwpActivityBudgetLink) object;
        if ((this.ablUniqueSrno == null && other.ablUniqueSrno != null) || (this.ablUniqueSrno != null && !this.ablUniqueSrno.equals(other.ablUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.BmwpActivityBudgetLink[ ablUniqueSrno=" + ablUniqueSrno + " ]";
    }
    
}
