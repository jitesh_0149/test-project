/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author anu
 */
@Embeddable
public class FhrdDependentPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_ID")
    private int userId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DEP_SRNO")
    private int depSrno;

    public FhrdDependentPK() {
    }

    public FhrdDependentPK(int userId, int depSrno) {
        this.userId = userId;
        this.depSrno = depSrno;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getDepSrno() {
        return depSrno;
    }

    public void setDepSrno(int depSrno) {
        this.depSrno = depSrno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) userId;
        hash += (int) depSrno;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdDependentPK)) {
            return false;
        }
        FhrdDependentPK other = (FhrdDependentPK) object;
        if (this.userId != other.userId) {
            return false;
        }
        if (this.depSrno != other.depSrno) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdDependentPK[ userId=" + userId + ", depSrno=" + depSrno + " ]";
    }
    
}
