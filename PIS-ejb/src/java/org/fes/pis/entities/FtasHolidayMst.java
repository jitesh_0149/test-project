/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_HOLIDAY_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasHolidayMst.findAll", query = "SELECT f FROM FtasHolidayMst f"),
    @NamedQuery(name = "FtasHolidayMst.findByHoliYear", query = "SELECT f FROM FtasHolidayMst f WHERE f.holiYear = :holiYear"),
    @NamedQuery(name = "FtasHolidayMst.findByTotalWeekendDay", query = "SELECT f FROM FtasHolidayMst f WHERE f.totalWeekendDay = :totalWeekendDay"),
    @NamedQuery(name = "FtasHolidayMst.findByTotalScheduleDay", query = "SELECT f FROM FtasHolidayMst f WHERE f.totalScheduleDay = :totalScheduleDay"),
    @NamedQuery(name = "FtasHolidayMst.findByTotalSuddenDeclareDay", query = "SELECT f FROM FtasHolidayMst f WHERE f.totalSuddenDeclareDay = :totalSuddenDeclareDay"),
    @NamedQuery(name = "FtasHolidayMst.findByTotalApproved", query = "SELECT f FROM FtasHolidayMst f WHERE f.totalApproved = :totalApproved"),
    @NamedQuery(name = "FtasHolidayMst.findByTotalRejected", query = "SELECT f FROM FtasHolidayMst f WHERE f.totalRejected = :totalRejected"),
    @NamedQuery(name = "FtasHolidayMst.findByTotalAwaitedApproval", query = "SELECT f FROM FtasHolidayMst f WHERE f.totalAwaitedApproval = :totalAwaitedApproval"),
    @NamedQuery(name = "FtasHolidayMst.findByCreatedt", query = "SELECT f FROM FtasHolidayMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasHolidayMst.findByUpdatedt", query = "SELECT f FROM FtasHolidayMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasHolidayMst.findByExportFlag", query = "SELECT f FROM FtasHolidayMst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FtasHolidayMst.findByImportFlag", query = "SELECT f FROM FtasHolidayMst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FtasHolidayMst.findBySystemUserid", query = "SELECT f FROM FtasHolidayMst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FtasHolidayMst.findByHmSrgKey", query = "SELECT f FROM FtasHolidayMst f WHERE f.hmSrgKey = :hmSrgKey"),
    @NamedQuery(name = "FtasHolidayMst.findByTranYear", query = "SELECT f FROM FtasHolidayMst f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FtasHolidayMst.findByFinalHolidayLastDate", query = "SELECT f FROM FtasHolidayMst f WHERE f.finalHolidayLastDate = :finalHolidayLastDate")})
public class FtasHolidayMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "HOLI_YEAR")
    private short holiYear;
    @Column(name = "TOTAL_WEEKEND_DAY")
    private Integer totalWeekendDay;
    @Column(name = "TOTAL_SCHEDULE_DAY")
    private Integer totalScheduleDay;
    @Column(name = "TOTAL_SUDDEN_DECLARE_DAY")
    private Integer totalSuddenDeclareDay;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_APPROVED")
    private int totalApproved;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_REJECTED")
    private int totalRejected;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_AWAITED_APPROVAL")
    private int totalAwaitedApproval;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "HM_SRG_KEY")
    private BigDecimal hmSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FINAL_HOLIDAY_LAST_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finalHolidayLastDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ftasHolidayMst")
    private Collection<FtasHolidayDtl> ftasHolidayDtlCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;

    public FtasHolidayMst() {
    }

    public FtasHolidayMst(BigDecimal hmSrgKey) {
        this.hmSrgKey = hmSrgKey;
    }

    public FtasHolidayMst(BigDecimal hmSrgKey, short holiYear, int totalApproved, int totalRejected, int totalAwaitedApproval, Date createdt, short tranYear, Date finalHolidayLastDate) {
        this.hmSrgKey = hmSrgKey;
        this.holiYear = holiYear;
        this.totalApproved = totalApproved;
        this.totalRejected = totalRejected;
        this.totalAwaitedApproval = totalAwaitedApproval;
        this.createdt = createdt;
        this.tranYear = tranYear;
        this.finalHolidayLastDate = finalHolidayLastDate;
    }

    public short getHoliYear() {
        return holiYear;
    }

    public void setHoliYear(short holiYear) {
        this.holiYear = holiYear;
    }

    public Integer getTotalWeekendDay() {
        return totalWeekendDay;
    }

    public void setTotalWeekendDay(Integer totalWeekendDay) {
        this.totalWeekendDay = totalWeekendDay;
    }

    public Integer getTotalScheduleDay() {
        return totalScheduleDay;
    }

    public void setTotalScheduleDay(Integer totalScheduleDay) {
        this.totalScheduleDay = totalScheduleDay;
    }

    public Integer getTotalSuddenDeclareDay() {
        return totalSuddenDeclareDay;
    }

    public void setTotalSuddenDeclareDay(Integer totalSuddenDeclareDay) {
        this.totalSuddenDeclareDay = totalSuddenDeclareDay;
    }

    public int getTotalApproved() {
        return totalApproved;
    }

    public void setTotalApproved(int totalApproved) {
        this.totalApproved = totalApproved;
    }

    public int getTotalRejected() {
        return totalRejected;
    }

    public void setTotalRejected(int totalRejected) {
        this.totalRejected = totalRejected;
    }

    public int getTotalAwaitedApproval() {
        return totalAwaitedApproval;
    }

    public void setTotalAwaitedApproval(int totalAwaitedApproval) {
        this.totalAwaitedApproval = totalAwaitedApproval;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public BigDecimal getHmSrgKey() {
        return hmSrgKey;
    }

    public void setHmSrgKey(BigDecimal hmSrgKey) {
        this.hmSrgKey = hmSrgKey;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public Date getFinalHolidayLastDate() {
        return finalHolidayLastDate;
    }

    public void setFinalHolidayLastDate(Date finalHolidayLastDate) {
        this.finalHolidayLastDate = finalHolidayLastDate;
    }

    @XmlTransient
    public Collection<FtasHolidayDtl> getFtasHolidayDtlCollection() {
        return ftasHolidayDtlCollection;
    }

    public void setFtasHolidayDtlCollection(Collection<FtasHolidayDtl> ftasHolidayDtlCollection) {
        this.ftasHolidayDtlCollection = ftasHolidayDtlCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (hmSrgKey != null ? hmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasHolidayMst)) {
            return false;
        }
        FtasHolidayMst other = (FtasHolidayMst) object;
        if ((this.hmSrgKey == null && other.hmSrgKey != null) || (this.hmSrgKey != null && !this.hmSrgKey.equals(other.hmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasHolidayMst[ hmSrgKey=" + hmSrgKey + " ]";
    }
    
}
