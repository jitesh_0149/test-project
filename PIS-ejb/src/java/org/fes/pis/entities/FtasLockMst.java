/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_LOCK_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasLockMst.findAll", query = "SELECT f FROM FtasLockMst f"),
    @NamedQuery(name = "FtasLockMst.findByFromDate", query = "SELECT f FROM FtasLockMst f WHERE f.fromDate = :fromDate"),
    @NamedQuery(name = "FtasLockMst.findByToDate", query = "SELECT f FROM FtasLockMst f WHERE f.toDate = :toDate"),
    @NamedQuery(name = "FtasLockMst.findByLockStatus", query = "SELECT f FROM FtasLockMst f WHERE f.lockStatus = :lockStatus"),
    @NamedQuery(name = "FtasLockMst.findByCreatedt", query = "SELECT f FROM FtasLockMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasLockMst.findByUpdatedt", query = "SELECT f FROM FtasLockMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasLockMst.findByExportFlag", query = "SELECT f FROM FtasLockMst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FtasLockMst.findByImportFlag", query = "SELECT f FROM FtasLockMst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FtasLockMst.findBySystemUserid", query = "SELECT f FROM FtasLockMst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FtasLockMst.findByLckmSrgKey", query = "SELECT f FROM FtasLockMst f WHERE f.lckmSrgKey = :lckmSrgKey")})
public class FtasLockMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date toDate;
    @Size(max = 1)
    @Column(name = "LOCK_STATUS")
    private String lockStatus;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "LCKM_SRG_KEY")
    private BigDecimal lckmSrgKey;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;

    public FtasLockMst() {
    }

    public FtasLockMst(BigDecimal lckmSrgKey) {
        this.lckmSrgKey = lckmSrgKey;
    }

    public FtasLockMst(BigDecimal lckmSrgKey, Date fromDate, Date toDate, Date createdt) {
        this.lckmSrgKey = lckmSrgKey;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.createdt = createdt;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getLockStatus() {
        return lockStatus;
    }

    public void setLockStatus(String lockStatus) {
        this.lockStatus = lockStatus;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public BigDecimal getLckmSrgKey() {
        return lckmSrgKey;
    }

    public void setLckmSrgKey(BigDecimal lckmSrgKey) {
        this.lckmSrgKey = lckmSrgKey;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lckmSrgKey != null ? lckmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasLockMst)) {
            return false;
        }
        FtasLockMst other = (FtasLockMst) object;
        if ((this.lckmSrgKey == null && other.lckmSrgKey != null) || (this.lckmSrgKey != null && !this.lckmSrgKey.equals(other.lckmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasLockMst[ lckmSrgKey=" + lckmSrgKey + " ]";
    }
    
}
