/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_TRAINING_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasTrainingMst.findAll", query = "SELECT f FROM FtasTrainingMst f"),
    @NamedQuery(name = "FtasTrainingMst.findByTrannSrgKey", query = "SELECT f FROM FtasTrainingMst f WHERE f.trannSrgKey = :trannSrgKey"),
    @NamedQuery(name = "FtasTrainingMst.findByFromDate", query = "SELECT f FROM FtasTrainingMst f WHERE f.fromDate = :fromDate"),
    @NamedQuery(name = "FtasTrainingMst.findByToDate", query = "SELECT f FROM FtasTrainingMst f WHERE f.toDate = :toDate"),
    @NamedQuery(name = "FtasTrainingMst.findByTotalDays", query = "SELECT f FROM FtasTrainingMst f WHERE f.totalDays = :totalDays"),
    @NamedQuery(name = "FtasTrainingMst.findByCoOrdinatorFName", query = "SELECT f FROM FtasTrainingMst f WHERE f.coOrdinatorFName = :coOrdinatorFName"),
    @NamedQuery(name = "FtasTrainingMst.findByCoOrdinatorLName", query = "SELECT f FROM FtasTrainingMst f WHERE f.coOrdinatorLName = :coOrdinatorLName"),
    @NamedQuery(name = "FtasTrainingMst.findByCoOrdinatorContact", query = "SELECT f FROM FtasTrainingMst f WHERE f.coOrdinatorContact = :coOrdinatorContact"),
    @NamedQuery(name = "FtasTrainingMst.findByInstituteName", query = "SELECT f FROM FtasTrainingMst f WHERE f.instituteName = :instituteName"),
    @NamedQuery(name = "FtasTrainingMst.findByInstituteAddress", query = "SELECT f FROM FtasTrainingMst f WHERE f.instituteAddress = :instituteAddress"),
    @NamedQuery(name = "FtasTrainingMst.findByVenueAddress", query = "SELECT f FROM FtasTrainingMst f WHERE f.venueAddress = :venueAddress"),
    @NamedQuery(name = "FtasTrainingMst.findByVenueCity", query = "SELECT f FROM FtasTrainingMst f WHERE f.venueCity = :venueCity"),
    @NamedQuery(name = "FtasTrainingMst.findByVenueState", query = "SELECT f FROM FtasTrainingMst f WHERE f.venueState = :venueState"),
    @NamedQuery(name = "FtasTrainingMst.findByVenueCountry", query = "SELECT f FROM FtasTrainingMst f WHERE f.venueCountry = :venueCountry"),
    @NamedQuery(name = "FtasTrainingMst.findByObjective", query = "SELECT f FROM FtasTrainingMst f WHERE f.objective = :objective"),
    @NamedQuery(name = "FtasTrainingMst.findByTopicsIncluded", query = "SELECT f FROM FtasTrainingMst f WHERE f.topicsIncluded = :topicsIncluded"),
    @NamedQuery(name = "FtasTrainingMst.findByBenefits", query = "SELECT f FROM FtasTrainingMst f WHERE f.benefits = :benefits"),
    @NamedQuery(name = "FtasTrainingMst.findByEligibility", query = "SELECT f FROM FtasTrainingMst f WHERE f.eligibility = :eligibility"),
    @NamedQuery(name = "FtasTrainingMst.findByTuitionFees", query = "SELECT f FROM FtasTrainingMst f WHERE f.tuitionFees = :tuitionFees"),
    @NamedQuery(name = "FtasTrainingMst.findByTuitionLodgingFees", query = "SELECT f FROM FtasTrainingMst f WHERE f.tuitionLodgingFees = :tuitionLodgingFees"),
    @NamedQuery(name = "FtasTrainingMst.findByTuitionLodgingBoardingFees", query = "SELECT f FROM FtasTrainingMst f WHERE f.tuitionLodgingBoardingFees = :tuitionLodgingBoardingFees"),
    @NamedQuery(name = "FtasTrainingMst.findByOtherFeesDesc", query = "SELECT f FROM FtasTrainingMst f WHERE f.otherFeesDesc = :otherFeesDesc"),
    @NamedQuery(name = "FtasTrainingMst.findByOtherFeesAmount", query = "SELECT f FROM FtasTrainingMst f WHERE f.otherFeesAmount = :otherFeesAmount"),
    @NamedQuery(name = "FtasTrainingMst.findByCreatedt", query = "SELECT f FROM FtasTrainingMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasTrainingMst.findByUpdatedt", query = "SELECT f FROM FtasTrainingMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasTrainingMst.findByInstituteCity", query = "SELECT f FROM FtasTrainingMst f WHERE f.instituteCity = :instituteCity"),
    @NamedQuery(name = "FtasTrainingMst.findByInstituteCountry", query = "SELECT f FROM FtasTrainingMst f WHERE f.instituteCountry = :instituteCountry"),
    @NamedQuery(name = "FtasTrainingMst.findByInstituteState", query = "SELECT f FROM FtasTrainingMst f WHERE f.instituteState = :instituteState"),
    @NamedQuery(name = "FtasTrainingMst.findByInstitutePincode", query = "SELECT f FROM FtasTrainingMst f WHERE f.institutePincode = :institutePincode"),
    @NamedQuery(name = "FtasTrainingMst.findByInstituteContact", query = "SELECT f FROM FtasTrainingMst f WHERE f.instituteContact = :instituteContact"),
    @NamedQuery(name = "FtasTrainingMst.findByVenueContact", query = "SELECT f FROM FtasTrainingMst f WHERE f.venueContact = :venueContact"),
    @NamedQuery(name = "FtasTrainingMst.findByVenuePincode", query = "SELECT f FROM FtasTrainingMst f WHERE f.venuePincode = :venuePincode"),
    @NamedQuery(name = "FtasTrainingMst.findBySubject", query = "SELECT f FROM FtasTrainingMst f WHERE f.subject = :subject"),
    @NamedQuery(name = "FtasTrainingMst.findByAnnouncementFlag", query = "SELECT f FROM FtasTrainingMst f WHERE f.announcementFlag = :announcementFlag"),
    @NamedQuery(name = "FtasTrainingMst.findByExportFlag", query = "SELECT f FROM FtasTrainingMst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FtasTrainingMst.findByImportFlag", query = "SELECT f FROM FtasTrainingMst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FtasTrainingMst.findBySystemUserid", query = "SELECT f FROM FtasTrainingMst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FtasTrainingMst.findByOumUnitSrno", query = "SELECT f FROM FtasTrainingMst f WHERE f.oumUnitSrno = :oumUnitSrno"),
    @NamedQuery(name = "FtasTrainingMst.findByTranYear", query = "SELECT f FROM FtasTrainingMst f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FtasTrainingMst.findByVenueName", query = "SELECT f FROM FtasTrainingMst f WHERE f.venueName = :venueName")})
public class FtasTrainingMst implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRANN_SRG_KEY")
    private BigDecimal trannSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date toDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_DAYS")
    private BigDecimal totalDays;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "CO_ORDINATOR_F_NAME")
    private String coOrdinatorFName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "CO_ORDINATOR_L_NAME")
    private String coOrdinatorLName;
    @Size(max = 1000)
    @Column(name = "CO_ORDINATOR_CONTACT")
    private String coOrdinatorContact;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "INSTITUTE_NAME")
    private String instituteName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "INSTITUTE_ADDRESS")
    private String instituteAddress;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 70)
    @Column(name = "VENUE_ADDRESS")
    private String venueAddress;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "VENUE_CITY")
    private String venueCity;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "VENUE_STATE")
    private String venueState;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "VENUE_COUNTRY")
    private String venueCountry;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "OBJECTIVE")
    private String objective;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "TOPICS_INCLUDED")
    private String topicsIncluded;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "BENEFITS")
    private String benefits;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "ELIGIBILITY")
    private String eligibility;
    @Column(name = "TUITION_FEES")
    private Long tuitionFees;
    @Column(name = "TUITION_LODGING_FEES")
    private Long tuitionLodgingFees;
    @Column(name = "TUITION_LODGING_BOARDING_FEES")
    private Long tuitionLodgingBoardingFees;
    @Size(max = 30)
    @Column(name = "OTHER_FEES_DESC")
    private String otherFeesDesc;
    @Column(name = "OTHER_FEES_AMOUNT")
    private Long otherFeesAmount;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "INSTITUTE_CITY")
    private String instituteCity;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "INSTITUTE_COUNTRY")
    private String instituteCountry;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "INSTITUTE_STATE")
    private String instituteState;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "INSTITUTE_PINCODE")
    private String institutePincode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "INSTITUTE_CONTACT")
    private String instituteContact;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "VENUE_CONTACT")
    private String venueContact;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "VENUE_PINCODE")
    private String venuePincode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "SUBJECT")
    private String subject;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ANNOUNCEMENT_FLAG")
    private char announcementFlag;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Column(name = "OUM_UNIT_SRNO")
    private Integer oumUnitSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Size(max = 100)
    @Column(name = "VENUE_NAME")
    private String venueName;
    @JoinColumn(name = "CATEGORY_TYPE", referencedColumnName = "CATM_SRG_KEY")
    @ManyToOne(optional = false)
    private FtasCategoryMst categoryType;
    @JoinColumn(name = "CO_ORDINATOR_SALUT", referencedColumnName = "CATM_SRG_KEY")
    @ManyToOne(optional = false)
    private FtasCategoryMst coOrdinatorSalut;
    @JoinColumn(name = "ANNOUNCEMENT_TYPE", referencedColumnName = "CATM_SRG_KEY")
    @ManyToOne(optional = false)
    private FtasCategoryMst announcementType;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "ANNOUNCE_BY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst announceBy;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "trannSrgKey")
    private Collection<FtasTrainingOrgUnitDtl> ftasTrainingOrgUnitDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "trannSrgKey")
    private Collection<FtasTrainingAnnDetail> ftasTrainingAnnDetailCollection;

    public FtasTrainingMst() {
    }

    public FtasTrainingMst(BigDecimal trannSrgKey) {
        this.trannSrgKey = trannSrgKey;
    }

    public FtasTrainingMst(BigDecimal trannSrgKey, Date fromDate, Date toDate, BigDecimal totalDays, String coOrdinatorFName, String coOrdinatorLName, String instituteName, String instituteAddress, String venueAddress, String venueCity, String venueState, String venueCountry, String objective, String topicsIncluded, String benefits, String eligibility, Date createdt, String instituteCity, String instituteCountry, String instituteState, String institutePincode, String instituteContact, String venueContact, String venuePincode, String subject, char announcementFlag, short tranYear) {
        this.trannSrgKey = trannSrgKey;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.totalDays = totalDays;
        this.coOrdinatorFName = coOrdinatorFName;
        this.coOrdinatorLName = coOrdinatorLName;
        this.instituteName = instituteName;
        this.instituteAddress = instituteAddress;
        this.venueAddress = venueAddress;
        this.venueCity = venueCity;
        this.venueState = venueState;
        this.venueCountry = venueCountry;
        this.objective = objective;
        this.topicsIncluded = topicsIncluded;
        this.benefits = benefits;
        this.eligibility = eligibility;
        this.createdt = createdt;
        this.instituteCity = instituteCity;
        this.instituteCountry = instituteCountry;
        this.instituteState = instituteState;
        this.institutePincode = institutePincode;
        this.instituteContact = instituteContact;
        this.venueContact = venueContact;
        this.venuePincode = venuePincode;
        this.subject = subject;
        this.announcementFlag = announcementFlag;
        this.tranYear = tranYear;
    }

    public BigDecimal getTrannSrgKey() {
        return trannSrgKey;
    }

    public void setTrannSrgKey(BigDecimal trannSrgKey) {
        this.trannSrgKey = trannSrgKey;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public BigDecimal getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(BigDecimal totalDays) {
        this.totalDays = totalDays;
    }

    public String getCoOrdinatorFName() {
        return coOrdinatorFName;
    }

    public void setCoOrdinatorFName(String coOrdinatorFName) {
        this.coOrdinatorFName = coOrdinatorFName;
    }

    public String getCoOrdinatorLName() {
        return coOrdinatorLName;
    }

    public void setCoOrdinatorLName(String coOrdinatorLName) {
        this.coOrdinatorLName = coOrdinatorLName;
    }

    public String getCoOrdinatorContact() {
        return coOrdinatorContact;
    }

    public void setCoOrdinatorContact(String coOrdinatorContact) {
        this.coOrdinatorContact = coOrdinatorContact;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public String getInstituteAddress() {
        return instituteAddress;
    }

    public void setInstituteAddress(String instituteAddress) {
        this.instituteAddress = instituteAddress;
    }

    public String getVenueAddress() {
        return venueAddress;
    }

    public void setVenueAddress(String venueAddress) {
        this.venueAddress = venueAddress;
    }

    public String getVenueCity() {
        return venueCity;
    }

    public void setVenueCity(String venueCity) {
        this.venueCity = venueCity;
    }

    public String getVenueState() {
        return venueState;
    }

    public void setVenueState(String venueState) {
        this.venueState = venueState;
    }

    public String getVenueCountry() {
        return venueCountry;
    }

    public void setVenueCountry(String venueCountry) {
        this.venueCountry = venueCountry;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getTopicsIncluded() {
        return topicsIncluded;
    }

    public void setTopicsIncluded(String topicsIncluded) {
        this.topicsIncluded = topicsIncluded;
    }

    public String getBenefits() {
        return benefits;
    }

    public void setBenefits(String benefits) {
        this.benefits = benefits;
    }

    public String getEligibility() {
        return eligibility;
    }

    public void setEligibility(String eligibility) {
        this.eligibility = eligibility;
    }

    public Long getTuitionFees() {
        return tuitionFees;
    }

    public void setTuitionFees(Long tuitionFees) {
        this.tuitionFees = tuitionFees;
    }

    public Long getTuitionLodgingFees() {
        return tuitionLodgingFees;
    }

    public void setTuitionLodgingFees(Long tuitionLodgingFees) {
        this.tuitionLodgingFees = tuitionLodgingFees;
    }

    public Long getTuitionLodgingBoardingFees() {
        return tuitionLodgingBoardingFees;
    }

    public void setTuitionLodgingBoardingFees(Long tuitionLodgingBoardingFees) {
        this.tuitionLodgingBoardingFees = tuitionLodgingBoardingFees;
    }

    public String getOtherFeesDesc() {
        return otherFeesDesc;
    }

    public void setOtherFeesDesc(String otherFeesDesc) {
        this.otherFeesDesc = otherFeesDesc;
    }

    public Long getOtherFeesAmount() {
        return otherFeesAmount;
    }

    public void setOtherFeesAmount(Long otherFeesAmount) {
        this.otherFeesAmount = otherFeesAmount;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getInstituteCity() {
        return instituteCity;
    }

    public void setInstituteCity(String instituteCity) {
        this.instituteCity = instituteCity;
    }

    public String getInstituteCountry() {
        return instituteCountry;
    }

    public void setInstituteCountry(String instituteCountry) {
        this.instituteCountry = instituteCountry;
    }

    public String getInstituteState() {
        return instituteState;
    }

    public void setInstituteState(String instituteState) {
        this.instituteState = instituteState;
    }

    public String getInstitutePincode() {
        return institutePincode;
    }

    public void setInstitutePincode(String institutePincode) {
        this.institutePincode = institutePincode;
    }

    public String getInstituteContact() {
        return instituteContact;
    }

    public void setInstituteContact(String instituteContact) {
        this.instituteContact = instituteContact;
    }

    public String getVenueContact() {
        return venueContact;
    }

    public void setVenueContact(String venueContact) {
        this.venueContact = venueContact;
    }

    public String getVenuePincode() {
        return venuePincode;
    }

    public void setVenuePincode(String venuePincode) {
        this.venuePincode = venuePincode;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public char getAnnouncementFlag() {
        return announcementFlag;
    }

    public void setAnnouncementFlag(char announcementFlag) {
        this.announcementFlag = announcementFlag;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public Integer getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(Integer oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    public FtasCategoryMst getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(FtasCategoryMst categoryType) {
        this.categoryType = categoryType;
    }

    public FtasCategoryMst getCoOrdinatorSalut() {
        return coOrdinatorSalut;
    }

    public void setCoOrdinatorSalut(FtasCategoryMst coOrdinatorSalut) {
        this.coOrdinatorSalut = coOrdinatorSalut;
    }

    public FtasCategoryMst getAnnouncementType() {
        return announcementType;
    }

    public void setAnnouncementType(FtasCategoryMst announcementType) {
        this.announcementType = announcementType;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getAnnounceBy() {
        return announceBy;
    }

    public void setAnnounceBy(FhrdEmpmst announceBy) {
        this.announceBy = announceBy;
    }

    @XmlTransient
    public Collection<FtasTrainingOrgUnitDtl> getFtasTrainingOrgUnitDtlCollection() {
        return ftasTrainingOrgUnitDtlCollection;
    }

    public void setFtasTrainingOrgUnitDtlCollection(Collection<FtasTrainingOrgUnitDtl> ftasTrainingOrgUnitDtlCollection) {
        this.ftasTrainingOrgUnitDtlCollection = ftasTrainingOrgUnitDtlCollection;
    }

    @XmlTransient
    public Collection<FtasTrainingAnnDetail> getFtasTrainingAnnDetailCollection() {
        return ftasTrainingAnnDetailCollection;
    }

    public void setFtasTrainingAnnDetailCollection(Collection<FtasTrainingAnnDetail> ftasTrainingAnnDetailCollection) {
        this.ftasTrainingAnnDetailCollection = ftasTrainingAnnDetailCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (trannSrgKey != null ? trannSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasTrainingMst)) {
            return false;
        }
        FtasTrainingMst other = (FtasTrainingMst) object;
        if ((this.trannSrgKey == null && other.trannSrgKey != null) || (this.trannSrgKey != null && !this.trannSrgKey.equals(other.trannSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasTrainingMst[ trannSrgKey=" + trannSrgKey + " ]";
    }
    
}
