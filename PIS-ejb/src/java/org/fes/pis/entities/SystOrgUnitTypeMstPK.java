/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author anu
 */
@Embeddable
public class SystOrgUnitTypeMstPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "UTM_TYPE_SRNO")
    private int utmTypeSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "UTM_TYPE_CODE")
    private String utmTypeCode;

    public SystOrgUnitTypeMstPK() {
    }

    public SystOrgUnitTypeMstPK(int utmTypeSrno, String utmTypeCode) {
        this.utmTypeSrno = utmTypeSrno;
        this.utmTypeCode = utmTypeCode;
    }

    public int getUtmTypeSrno() {
        return utmTypeSrno;
    }

    public void setUtmTypeSrno(int utmTypeSrno) {
        this.utmTypeSrno = utmTypeSrno;
    }

    public String getUtmTypeCode() {
        return utmTypeCode;
    }

    public void setUtmTypeCode(String utmTypeCode) {
        this.utmTypeCode = utmTypeCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) utmTypeSrno;
        hash += (utmTypeCode != null ? utmTypeCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SystOrgUnitTypeMstPK)) {
            return false;
        }
        SystOrgUnitTypeMstPK other = (SystOrgUnitTypeMstPK) object;
        if (this.utmTypeSrno != other.utmTypeSrno) {
            return false;
        }
        if ((this.utmTypeCode == null && other.utmTypeCode != null) || (this.utmTypeCode != null && !this.utmTypeCode.equals(other.utmTypeCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.SystOrgUnitTypeMstPK[ utmTypeSrno=" + utmTypeSrno + ", utmTypeCode=" + utmTypeCode + " ]";
    }
    
}
