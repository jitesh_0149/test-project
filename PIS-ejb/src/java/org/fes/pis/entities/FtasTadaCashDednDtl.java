/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_TADA_CASH_DEDN_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasTadaCashDednDtl.findAll", query = "SELECT f FROM FtasTadaCashDednDtl f"),
    @NamedQuery(name = "FtasTadaCashDednDtl.findByTbmSrgKey", query = "SELECT f FROM FtasTadaCashDednDtl f WHERE f.ftasTadaCashDednDtlPK.tbmSrgKey = :tbmSrgKey"),
    @NamedQuery(name = "FtasTadaCashDednDtl.findBySrno", query = "SELECT f FROM FtasTadaCashDednDtl f WHERE f.ftasTadaCashDednDtlPK.srno = :srno"),
    @NamedQuery(name = "FtasTadaCashDednDtl.findByUserId", query = "SELECT f FROM FtasTadaCashDednDtl f WHERE f.userId = :userId"),
    @NamedQuery(name = "FtasTadaCashDednDtl.findByDednAmt", query = "SELECT f FROM FtasTadaCashDednDtl f WHERE f.dednAmt = :dednAmt"),
    @NamedQuery(name = "FtasTadaCashDednDtl.findByDednDesc", query = "SELECT f FROM FtasTadaCashDednDtl f WHERE f.dednDesc = :dednDesc"),
    @NamedQuery(name = "FtasTadaCashDednDtl.findByCreatedt", query = "SELECT f FROM FtasTadaCashDednDtl f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasTadaCashDednDtl.findByUpdatedt", query = "SELECT f FROM FtasTadaCashDednDtl f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasTadaCashDednDtl.findBySystemUserid", query = "SELECT f FROM FtasTadaCashDednDtl f WHERE f.systemUserid = :systemUserid")})
public class FtasTadaCashDednDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FtasTadaCashDednDtlPK ftasTadaCashDednDtlPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_ID")
    private int userId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "DEDN_AMT")
    private BigDecimal dednAmt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "DEDN_DESC")
    private String dednDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "TBM_SRG_KEY", referencedColumnName = "TBM_SRG_KEY", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FtasTadaBillMst ftasTadaBillMst;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FtasTadaCashDednDtl() {
    }

    public FtasTadaCashDednDtl(FtasTadaCashDednDtlPK ftasTadaCashDednDtlPK) {
        this.ftasTadaCashDednDtlPK = ftasTadaCashDednDtlPK;
    }

    public FtasTadaCashDednDtl(FtasTadaCashDednDtlPK ftasTadaCashDednDtlPK, int userId, BigDecimal dednAmt, String dednDesc, Date createdt) {
        this.ftasTadaCashDednDtlPK = ftasTadaCashDednDtlPK;
        this.userId = userId;
        this.dednAmt = dednAmt;
        this.dednDesc = dednDesc;
        this.createdt = createdt;
    }

    public FtasTadaCashDednDtl(BigDecimal tbmSrgKey, int srno) {
        this.ftasTadaCashDednDtlPK = new FtasTadaCashDednDtlPK(tbmSrgKey, srno);
    }

    public FtasTadaCashDednDtlPK getFtasTadaCashDednDtlPK() {
        return ftasTadaCashDednDtlPK;
    }

    public void setFtasTadaCashDednDtlPK(FtasTadaCashDednDtlPK ftasTadaCashDednDtlPK) {
        this.ftasTadaCashDednDtlPK = ftasTadaCashDednDtlPK;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public BigDecimal getDednAmt() {
        return dednAmt;
    }

    public void setDednAmt(BigDecimal dednAmt) {
        this.dednAmt = dednAmt;
    }

    public String getDednDesc() {
        return dednDesc;
    }

    public void setDednDesc(String dednDesc) {
        this.dednDesc = dednDesc;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasTadaBillMst getFtasTadaBillMst() {
        return ftasTadaBillMst;
    }

    public void setFtasTadaBillMst(FtasTadaBillMst ftasTadaBillMst) {
        this.ftasTadaBillMst = ftasTadaBillMst;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ftasTadaCashDednDtlPK != null ? ftasTadaCashDednDtlPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasTadaCashDednDtl)) {
            return false;
        }
        FtasTadaCashDednDtl other = (FtasTadaCashDednDtl) object;
        if ((this.ftasTadaCashDednDtlPK == null && other.ftasTadaCashDednDtlPK != null) || (this.ftasTadaCashDednDtlPK != null && !this.ftasTadaCashDednDtlPK.equals(other.ftasTadaCashDednDtlPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasTadaCashDednDtl[ ftasTadaCashDednDtlPK=" + ftasTadaCashDednDtlPK + " ]";
    }
    
}
