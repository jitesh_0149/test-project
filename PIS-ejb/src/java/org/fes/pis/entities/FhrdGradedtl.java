/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_GRADEDTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdGradedtl.findAll", query = "SELECT f FROM FhrdGradedtl f"),
    @NamedQuery(name = "FhrdGradedtl.findByGradesrno", query = "SELECT f FROM FhrdGradedtl f WHERE f.gradesrno = :gradesrno"),
    @NamedQuery(name = "FhrdGradedtl.findByGradecd", query = "SELECT f FROM FhrdGradedtl f WHERE f.gradecd = :gradecd"),
    @NamedQuery(name = "FhrdGradedtl.findBySubsrno", query = "SELECT f FROM FhrdGradedtl f WHERE f.subsrno = :subsrno"),
    @NamedQuery(name = "FhrdGradedtl.findByIncramt", query = "SELECT f FROM FhrdGradedtl f WHERE f.incramt = :incramt"),
    @NamedQuery(name = "FhrdGradedtl.findByStep", query = "SELECT f FROM FhrdGradedtl f WHERE f.step = :step"),
    @NamedQuery(name = "FhrdGradedtl.findByCreatedt", query = "SELECT f FROM FhrdGradedtl f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdGradedtl.findByUpdatedt", query = "SELECT f FROM FhrdGradedtl f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdGradedtl.findByExportFlag", query = "SELECT f FROM FhrdGradedtl f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdGradedtl.findByImportFlag", query = "SELECT f FROM FhrdGradedtl f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdGradedtl.findBySystemUserid", query = "SELECT f FROM FhrdGradedtl f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdGradedtl.findByGdSrgKey", query = "SELECT f FROM FhrdGradedtl f WHERE f.gdSrgKey = :gdSrgKey"),
    @NamedQuery(name = "FhrdGradedtl.findByGmSrgKey", query = "SELECT f FROM FhrdGradedtl f WHERE f.gmSrgKey = :gmSrgKey")})
public class FhrdGradedtl implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "GRADESRNO")
    private Short gradesrno;
    @Column(name = "GRADECD")
    private Short gradecd;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SUBSRNO")
    private BigDecimal subsrno;
    @Column(name = "INCRAMT")
    private BigDecimal incramt;
    @Column(name = "STEP")
    private BigDecimal step;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "GD_SRG_KEY")
    private BigDecimal gdSrgKey;
    @Column(name = "GM_SRG_KEY")
    private BigDecimal gmSrgKey;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FhrdGradedtl() {
    }

    public FhrdGradedtl(BigDecimal gdSrgKey) {
        this.gdSrgKey = gdSrgKey;
    }

    public FhrdGradedtl(BigDecimal gdSrgKey, Date createdt) {
        this.gdSrgKey = gdSrgKey;
        this.createdt = createdt;
    }

    public Short getGradesrno() {
        return gradesrno;
    }

    public void setGradesrno(Short gradesrno) {
        this.gradesrno = gradesrno;
    }

    public Short getGradecd() {
        return gradecd;
    }

    public void setGradecd(Short gradecd) {
        this.gradecd = gradecd;
    }

    public BigDecimal getSubsrno() {
        return subsrno;
    }

    public void setSubsrno(BigDecimal subsrno) {
        this.subsrno = subsrno;
    }

    public BigDecimal getIncramt() {
        return incramt;
    }

    public void setIncramt(BigDecimal incramt) {
        this.incramt = incramt;
    }

    public BigDecimal getStep() {
        return step;
    }

    public void setStep(BigDecimal step) {
        this.step = step;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public BigDecimal getGdSrgKey() {
        return gdSrgKey;
    }

    public void setGdSrgKey(BigDecimal gdSrgKey) {
        this.gdSrgKey = gdSrgKey;
    }

    public BigDecimal getGmSrgKey() {
        return gmSrgKey;
    }

    public void setGmSrgKey(BigDecimal gmSrgKey) {
        this.gmSrgKey = gmSrgKey;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gdSrgKey != null ? gdSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdGradedtl)) {
            return false;
        }
        FhrdGradedtl other = (FhrdGradedtl) object;
        if ((this.gdSrgKey == null && other.gdSrgKey != null) || (this.gdSrgKey != null && !this.gdSrgKey.equals(other.gdSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdGradedtl[ gdSrgKey=" + gdSrgKey + " ]";
    }
    
}
