/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_EMPEVENT_TRANSFER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdEmpeventTransfer.findAll", query = "SELECT f FROM FhrdEmpeventTransfer f"),
    @NamedQuery(name = "FhrdEmpeventTransfer.findByEmpetSrgKey", query = "SELECT f FROM FhrdEmpeventTransfer f WHERE f.empetSrgKey = :empetSrgKey"),
    @NamedQuery(name = "FhrdEmpeventTransfer.findByTransferDate", query = "SELECT f FROM FhrdEmpeventTransfer f WHERE f.transferDate = :transferDate"),
    @NamedQuery(name = "FhrdEmpeventTransfer.findByRemarks", query = "SELECT f FROM FhrdEmpeventTransfer f WHERE f.remarks = :remarks"),
    @NamedQuery(name = "FhrdEmpeventTransfer.findByCreatedt", query = "SELECT f FROM FhrdEmpeventTransfer f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdEmpeventTransfer.findByUpdatedt", query = "SELECT f FROM FhrdEmpeventTransfer f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdEmpeventTransfer.findBySystemUserid", query = "SELECT f FROM FhrdEmpeventTransfer f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdEmpeventTransfer.findByExportFlag", query = "SELECT f FROM FhrdEmpeventTransfer f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdEmpeventTransfer.findByImportFlag", query = "SELECT f FROM FhrdEmpeventTransfer f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdEmpeventTransfer.findByRefOumUnitSrno", query = "SELECT f FROM FhrdEmpeventTransfer f WHERE f.refOumUnitSrno = :refOumUnitSrno"),
    @NamedQuery(name = "FhrdEmpeventTransfer.findByOuJoinDate", query = "SELECT f FROM FhrdEmpeventTransfer f WHERE f.ouJoinDate = :ouJoinDate")})
public class FhrdEmpeventTransfer implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMPET_SRG_KEY")
    private BigDecimal empetSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRANSFER_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date transferDate;
    @Size(max = 500)
    @Column(name = "REMARKS")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REF_OUM_UNIT_SRNO")
    private int refOumUnitSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OU_JOIN_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ouJoinDate;
    @JoinColumn(name = "FROM_OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst fromOumUnitSrno;
    @JoinColumn(name = "TO_OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst toOumUnitSrno;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CURRENT_REPORTING_USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst currentReportingUserId;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst userId;
    @JoinColumn(name = "NEW_REPORTING_USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst newReportingUserId;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;

    public FhrdEmpeventTransfer() {
    }

    public FhrdEmpeventTransfer(BigDecimal empetSrgKey) {
        this.empetSrgKey = empetSrgKey;
    }

    public FhrdEmpeventTransfer(BigDecimal empetSrgKey, Date transferDate, Date createdt, int refOumUnitSrno, Date ouJoinDate) {
        this.empetSrgKey = empetSrgKey;
        this.transferDate = transferDate;
        this.createdt = createdt;
        this.refOumUnitSrno = refOumUnitSrno;
        this.ouJoinDate = ouJoinDate;
    }

    public BigDecimal getEmpetSrgKey() {
        return empetSrgKey;
    }

    public void setEmpetSrgKey(BigDecimal empetSrgKey) {
        this.empetSrgKey = empetSrgKey;
    }

    public Date getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Date transferDate) {
        this.transferDate = transferDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public int getRefOumUnitSrno() {
        return refOumUnitSrno;
    }

    public void setRefOumUnitSrno(int refOumUnitSrno) {
        this.refOumUnitSrno = refOumUnitSrno;
    }

    public Date getOuJoinDate() {
        return ouJoinDate;
    }

    public void setOuJoinDate(Date ouJoinDate) {
        this.ouJoinDate = ouJoinDate;
    }

    public SystOrgUnitMst getFromOumUnitSrno() {
        return fromOumUnitSrno;
    }

    public void setFromOumUnitSrno(SystOrgUnitMst fromOumUnitSrno) {
        this.fromOumUnitSrno = fromOumUnitSrno;
    }

    public SystOrgUnitMst getToOumUnitSrno() {
        return toOumUnitSrno;
    }

    public void setToOumUnitSrno(SystOrgUnitMst toOumUnitSrno) {
        this.toOumUnitSrno = toOumUnitSrno;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCurrentReportingUserId() {
        return currentReportingUserId;
    }

    public void setCurrentReportingUserId(FhrdEmpmst currentReportingUserId) {
        this.currentReportingUserId = currentReportingUserId;
    }

    public FhrdEmpmst getUserId() {
        return userId;
    }

    public void setUserId(FhrdEmpmst userId) {
        this.userId = userId;
    }

    public FhrdEmpmst getNewReportingUserId() {
        return newReportingUserId;
    }

    public void setNewReportingUserId(FhrdEmpmst newReportingUserId) {
        this.newReportingUserId = newReportingUserId;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (empetSrgKey != null ? empetSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdEmpeventTransfer)) {
            return false;
        }
        FhrdEmpeventTransfer other = (FhrdEmpeventTransfer) object;
        if ((this.empetSrgKey == null && other.empetSrgKey != null) || (this.empetSrgKey != null && !this.empetSrgKey.equals(other.empetSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdEmpeventTransfer[ empetSrgKey=" + empetSrgKey + " ]";
    }
    
}
