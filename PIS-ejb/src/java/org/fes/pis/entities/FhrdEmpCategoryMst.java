/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_EMP_CATEGORY_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdEmpCategoryMst.findAll", query = "SELECT f FROM FhrdEmpCategoryMst f"),
    @NamedQuery(name = "FhrdEmpCategoryMst.findByCatName", query = "SELECT f FROM FhrdEmpCategoryMst f WHERE f.catName = :catName"),
    @NamedQuery(name = "FhrdEmpCategoryMst.findByStartDate", query = "SELECT f FROM FhrdEmpCategoryMst f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdEmpCategoryMst.findByEndDate", query = "SELECT f FROM FhrdEmpCategoryMst f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FhrdEmpCategoryMst.findByCreatedt", query = "SELECT f FROM FhrdEmpCategoryMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdEmpCategoryMst.findByUpdatedt", query = "SELECT f FROM FhrdEmpCategoryMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdEmpCategoryMst.findByExportFlag", query = "SELECT f FROM FhrdEmpCategoryMst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdEmpCategoryMst.findByImportFlag", query = "SELECT f FROM FhrdEmpCategoryMst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdEmpCategoryMst.findBySystemUserid", query = "SELECT f FROM FhrdEmpCategoryMst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdEmpCategoryMst.findByEmpcmSrgKey", query = "SELECT f FROM FhrdEmpCategoryMst f WHERE f.empcmSrgKey = :empcmSrgKey"),
    @NamedQuery(name = "FhrdEmpCategoryMst.findBySrno", query = "SELECT f FROM FhrdEmpCategoryMst f WHERE f.srno = :srno")})
public class FhrdEmpCategoryMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CAT_NAME")
    private String catName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMPCM_SRG_KEY")
    private BigDecimal empcmSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SRNO")
    private BigInteger srno;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empcmSrgKey")
    private Collection<FhrdEmpContractMst> fhrdEmpContractMstCollection;

    public FhrdEmpCategoryMst() {
    }

    public FhrdEmpCategoryMst(BigDecimal empcmSrgKey) {
        this.empcmSrgKey = empcmSrgKey;
    }

    public FhrdEmpCategoryMst(BigDecimal empcmSrgKey, String catName, Date startDate, Date createdt, BigInteger srno) {
        this.empcmSrgKey = empcmSrgKey;
        this.catName = catName;
        this.startDate = startDate;
        this.createdt = createdt;
        this.srno = srno;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public BigDecimal getEmpcmSrgKey() {
        return empcmSrgKey;
    }

    public void setEmpcmSrgKey(BigDecimal empcmSrgKey) {
        this.empcmSrgKey = empcmSrgKey;
    }

    public BigInteger getSrno() {
        return srno;
    }

    public void setSrno(BigInteger srno) {
        this.srno = srno;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @XmlTransient
    public Collection<FhrdEmpContractMst> getFhrdEmpContractMstCollection() {
        return fhrdEmpContractMstCollection;
    }

    public void setFhrdEmpContractMstCollection(Collection<FhrdEmpContractMst> fhrdEmpContractMstCollection) {
        this.fhrdEmpContractMstCollection = fhrdEmpContractMstCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (empcmSrgKey != null ? empcmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdEmpCategoryMst)) {
            return false;
        }
        FhrdEmpCategoryMst other = (FhrdEmpCategoryMst) object;
        if ((this.empcmSrgKey == null && other.empcmSrgKey != null) || (this.empcmSrgKey != null && !this.empcmSrgKey.equals(other.empcmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdEmpCategoryMst[ empcmSrgKey=" + empcmSrgKey + " ]";
    }
    
}
