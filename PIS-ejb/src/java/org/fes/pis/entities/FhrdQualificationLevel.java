/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_QUALIFICATION_LEVEL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdQualificationLevel.findAll", query = "SELECT f FROM FhrdQualificationLevel f"),
    @NamedQuery(name = "FhrdQualificationLevel.findByOumUnitSrno", query = "SELECT f FROM FhrdQualificationLevel f WHERE f.fhrdQualificationLevelPK.oumUnitSrno = :oumUnitSrno"),
    @NamedQuery(name = "FhrdQualificationLevel.findByLevelSrno", query = "SELECT f FROM FhrdQualificationLevel f WHERE f.fhrdQualificationLevelPK.levelSrno = :levelSrno"),
    @NamedQuery(name = "FhrdQualificationLevel.findByLevelDesc", query = "SELECT f FROM FhrdQualificationLevel f WHERE f.levelDesc = :levelDesc"),
    @NamedQuery(name = "FhrdQualificationLevel.findByLevelOrder", query = "SELECT f FROM FhrdQualificationLevel f WHERE f.levelOrder = :levelOrder"),
    @NamedQuery(name = "FhrdQualificationLevel.findByCreatedt", query = "SELECT f FROM FhrdQualificationLevel f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdQualificationLevel.findByUpdatedt", query = "SELECT f FROM FhrdQualificationLevel f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdQualificationLevel.findBySystemUserid", query = "SELECT f FROM FhrdQualificationLevel f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdQualificationLevel.findByExportFlag", query = "SELECT f FROM FhrdQualificationLevel f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdQualificationLevel.findByImportFlag", query = "SELECT f FROM FhrdQualificationLevel f WHERE f.importFlag = :importFlag")})
public class FhrdQualificationLevel implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FhrdQualificationLevelPK fhrdQualificationLevelPK;
    @Size(max = 500)
    @Column(name = "LEVEL_DESC")
    private String levelDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LEVEL_ORDER")
    private short levelOrder;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private SystOrgUnitMst systOrgUnitMst;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;

    public FhrdQualificationLevel() {
    }

    public FhrdQualificationLevel(FhrdQualificationLevelPK fhrdQualificationLevelPK) {
        this.fhrdQualificationLevelPK = fhrdQualificationLevelPK;
    }

    public FhrdQualificationLevel(FhrdQualificationLevelPK fhrdQualificationLevelPK, short levelOrder, Date createdt) {
        this.fhrdQualificationLevelPK = fhrdQualificationLevelPK;
        this.levelOrder = levelOrder;
        this.createdt = createdt;
    }

    public FhrdQualificationLevel(int oumUnitSrno, short levelSrno) {
        this.fhrdQualificationLevelPK = new FhrdQualificationLevelPK(oumUnitSrno, levelSrno);
    }

    public FhrdQualificationLevelPK getFhrdQualificationLevelPK() {
        return fhrdQualificationLevelPK;
    }

    public void setFhrdQualificationLevelPK(FhrdQualificationLevelPK fhrdQualificationLevelPK) {
        this.fhrdQualificationLevelPK = fhrdQualificationLevelPK;
    }

    public String getLevelDesc() {
        return levelDesc;
    }

    public void setLevelDesc(String levelDesc) {
        this.levelDesc = levelDesc;
    }

    public short getLevelOrder() {
        return levelOrder;
    }

    public void setLevelOrder(short levelOrder) {
        this.levelOrder = levelOrder;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public SystOrgUnitMst getSystOrgUnitMst() {
        return systOrgUnitMst;
    }

    public void setSystOrgUnitMst(SystOrgUnitMst systOrgUnitMst) {
        this.systOrgUnitMst = systOrgUnitMst;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fhrdQualificationLevelPK != null ? fhrdQualificationLevelPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdQualificationLevel)) {
            return false;
        }
        FhrdQualificationLevel other = (FhrdQualificationLevel) object;
        if ((this.fhrdQualificationLevelPK == null && other.fhrdQualificationLevelPK != null) || (this.fhrdQualificationLevelPK != null && !this.fhrdQualificationLevelPK.equals(other.fhrdQualificationLevelPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdQualificationLevel[ fhrdQualificationLevelPK=" + fhrdQualificationLevelPK + " ]";
    }
    
}
