/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_TADA_FARE_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasTadaFareDtl.findAll", query = "SELECT f FROM FtasTadaFareDtl f"),
    @NamedQuery(name = "FtasTadaFareDtl.findByTbmSrgKey", query = "SELECT f FROM FtasTadaFareDtl f WHERE f.ftasTadaFareDtlPK.tbmSrgKey = :tbmSrgKey"),
    @NamedQuery(name = "FtasTadaFareDtl.findBySrno", query = "SELECT f FROM FtasTadaFareDtl f WHERE f.ftasTadaFareDtlPK.srno = :srno"),
    @NamedQuery(name = "FtasTadaFareDtl.findByUserId", query = "SELECT f FROM FtasTadaFareDtl f WHERE f.userId = :userId"),
    @NamedQuery(name = "FtasTadaFareDtl.findByConvRate", query = "SELECT f FROM FtasTadaFareDtl f WHERE f.convRate = :convRate"),
    @NamedQuery(name = "FtasTadaFareDtl.findByKm", query = "SELECT f FROM FtasTadaFareDtl f WHERE f.km = :km"),
    @NamedQuery(name = "FtasTadaFareDtl.findByTicketNo", query = "SELECT f FROM FtasTadaFareDtl f WHERE f.ticketNo = :ticketNo"),
    @NamedQuery(name = "FtasTadaFareDtl.findByFromDateTime", query = "SELECT f FROM FtasTadaFareDtl f WHERE f.fromDateTime = :fromDateTime"),
    @NamedQuery(name = "FtasTadaFareDtl.findByFromStation", query = "SELECT f FROM FtasTadaFareDtl f WHERE f.fromStation = :fromStation"),
    @NamedQuery(name = "FtasTadaFareDtl.findByToDateTime", query = "SELECT f FROM FtasTadaFareDtl f WHERE f.toDateTime = :toDateTime"),
    @NamedQuery(name = "FtasTadaFareDtl.findByToStation", query = "SELECT f FROM FtasTadaFareDtl f WHERE f.toStation = :toStation"),
    @NamedQuery(name = "FtasTadaFareDtl.findByCreatedt", query = "SELECT f FROM FtasTadaFareDtl f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasTadaFareDtl.findByUpdatedt", query = "SELECT f FROM FtasTadaFareDtl f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasTadaFareDtl.findBySystemUserid", query = "SELECT f FROM FtasTadaFareDtl f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FtasTadaFareDtl.findByFareAmount", query = "SELECT f FROM FtasTadaFareDtl f WHERE f.fareAmount = :fareAmount"),
    @NamedQuery(name = "FtasTadaFareDtl.findByClaimFlag", query = "SELECT f FROM FtasTadaFareDtl f WHERE f.claimFlag = :claimFlag")})
public class FtasTadaFareDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FtasTadaFareDtlPK ftasTadaFareDtlPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_ID")
    private int userId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "CONV_RATE")
    private BigDecimal convRate;
    @Column(name = "KM")
    private BigDecimal km;
    @Size(max = 50)
    @Column(name = "TICKET_NO")
    private String ticketNo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FROM_DATE_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDateTime;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "FROM_STATION")
    private String fromStation;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TO_DATE_TIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date toDateTime;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "TO_STATION")
    private String toStation;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FARE_AMOUNT")
    private BigDecimal fareAmount;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "CLAIM_FLAG")
    private String claimFlag;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CM_SRG_KEY", referencedColumnName = "CM_SRG_KEY")
    @ManyToOne(optional = false)
    private FtasTadaConvMst cmSrgKey;
    @JoinColumn(name = "TBM_SRG_KEY", referencedColumnName = "TBM_SRG_KEY", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FtasTadaBillMst ftasTadaBillMst;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FtasTadaFareDtl() {
    }

    public FtasTadaFareDtl(FtasTadaFareDtlPK ftasTadaFareDtlPK) {
        this.ftasTadaFareDtlPK = ftasTadaFareDtlPK;
    }

    public FtasTadaFareDtl(FtasTadaFareDtlPK ftasTadaFareDtlPK, int userId, Date fromDateTime, String fromStation, Date toDateTime, String toStation, Date createdt, BigDecimal fareAmount, String claimFlag) {
        this.ftasTadaFareDtlPK = ftasTadaFareDtlPK;
        this.userId = userId;
        this.fromDateTime = fromDateTime;
        this.fromStation = fromStation;
        this.toDateTime = toDateTime;
        this.toStation = toStation;
        this.createdt = createdt;
        this.fareAmount = fareAmount;
        this.claimFlag = claimFlag;
    }

    public FtasTadaFareDtl(BigDecimal tbmSrgKey, int srno) {
        this.ftasTadaFareDtlPK = new FtasTadaFareDtlPK(tbmSrgKey, srno);
    }

    public FtasTadaFareDtlPK getFtasTadaFareDtlPK() {
        return ftasTadaFareDtlPK;
    }

    public void setFtasTadaFareDtlPK(FtasTadaFareDtlPK ftasTadaFareDtlPK) {
        this.ftasTadaFareDtlPK = ftasTadaFareDtlPK;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public BigDecimal getConvRate() {
        return convRate;
    }

    public void setConvRate(BigDecimal convRate) {
        this.convRate = convRate;
    }

    public BigDecimal getKm() {
        return km;
    }

    public void setKm(BigDecimal km) {
        this.km = km;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public Date getFromDateTime() {
        return fromDateTime;
    }

    public void setFromDateTime(Date fromDateTime) {
        this.fromDateTime = fromDateTime;
    }

    public String getFromStation() {
        return fromStation;
    }

    public void setFromStation(String fromStation) {
        this.fromStation = fromStation;
    }

    public Date getToDateTime() {
        return toDateTime;
    }

    public void setToDateTime(Date toDateTime) {
        this.toDateTime = toDateTime;
    }

    public String getToStation() {
        return toStation;
    }

    public void setToStation(String toStation) {
        this.toStation = toStation;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public BigDecimal getFareAmount() {
        return fareAmount;
    }

    public void setFareAmount(BigDecimal fareAmount) {
        this.fareAmount = fareAmount;
    }

    public String getClaimFlag() {
        return claimFlag;
    }

    public void setClaimFlag(String claimFlag) {
        this.claimFlag = claimFlag;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasTadaConvMst getCmSrgKey() {
        return cmSrgKey;
    }

    public void setCmSrgKey(FtasTadaConvMst cmSrgKey) {
        this.cmSrgKey = cmSrgKey;
    }

    public FtasTadaBillMst getFtasTadaBillMst() {
        return ftasTadaBillMst;
    }

    public void setFtasTadaBillMst(FtasTadaBillMst ftasTadaBillMst) {
        this.ftasTadaBillMst = ftasTadaBillMst;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ftasTadaFareDtlPK != null ? ftasTadaFareDtlPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasTadaFareDtl)) {
            return false;
        }
        FtasTadaFareDtl other = (FtasTadaFareDtl) object;
        if ((this.ftasTadaFareDtlPK == null && other.ftasTadaFareDtlPK != null) || (this.ftasTadaFareDtlPK != null && !this.ftasTadaFareDtlPK.equals(other.ftasTadaFareDtlPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasTadaFareDtl[ ftasTadaFareDtlPK=" + ftasTadaFareDtlPK + " ]";
    }
    
}
