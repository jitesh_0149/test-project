/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_EMPEVENT_RELIEVE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdEmpeventRelieve.findAll", query = "SELECT f FROM FhrdEmpeventRelieve f"),
    @NamedQuery(name = "FhrdEmpeventRelieve.findByEmperSrgKey", query = "SELECT f FROM FhrdEmpeventRelieve f WHERE f.emperSrgKey = :emperSrgKey"),
    @NamedQuery(name = "FhrdEmpeventRelieve.findByEmSrgKey", query = "SELECT f FROM FhrdEmpeventRelieve f WHERE f.emSrgKey = :emSrgKey"),
    @NamedQuery(name = "FhrdEmpeventRelieve.findByEventDate", query = "SELECT f FROM FhrdEmpeventRelieve f WHERE f.eventDate = :eventDate"),
    @NamedQuery(name = "FhrdEmpeventRelieve.findByRemarks", query = "SELECT f FROM FhrdEmpeventRelieve f WHERE f.remarks = :remarks"),
    @NamedQuery(name = "FhrdEmpeventRelieve.findByCreatedt", query = "SELECT f FROM FhrdEmpeventRelieve f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdEmpeventRelieve.findByUpdatedt", query = "SELECT f FROM FhrdEmpeventRelieve f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdEmpeventRelieve.findBySystemUserid", query = "SELECT f FROM FhrdEmpeventRelieve f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdEmpeventRelieve.findByEventType", query = "SELECT f FROM FhrdEmpeventRelieve f WHERE f.eventType = :eventType"),
    @NamedQuery(name = "FhrdEmpeventRelieve.findByRefOumUnitSrno", query = "SELECT f FROM FhrdEmpeventRelieve f WHERE f.refOumUnitSrno = :refOumUnitSrno"),
    @NamedQuery(name = "FhrdEmpeventRelieve.findByImportFlag", query = "SELECT f FROM FhrdEmpeventRelieve f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdEmpeventRelieve.findByExportFlag", query = "SELECT f FROM FhrdEmpeventRelieve f WHERE f.exportFlag = :exportFlag")})
public class FhrdEmpeventRelieve implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMPER_SRG_KEY")
    private BigDecimal emperSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EM_SRG_KEY")
    private BigDecimal emSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EVENT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventDate;
    @Size(max = 500)
    @Column(name = "REMARKS")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EVENT_TYPE")
    private String eventType;
    @Basic(optional = false)
    @NotNull
    @Column(name = "REF_OUM_UNIT_SRNO")
    private int refOumUnitSrno;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst userId;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FhrdEmpeventRelieve() {
    }

    public FhrdEmpeventRelieve(BigDecimal emperSrgKey) {
        this.emperSrgKey = emperSrgKey;
    }

    public FhrdEmpeventRelieve(BigDecimal emperSrgKey, BigDecimal emSrgKey, Date eventDate, Date createdt, String eventType, int refOumUnitSrno) {
        this.emperSrgKey = emperSrgKey;
        this.emSrgKey = emSrgKey;
        this.eventDate = eventDate;
        this.createdt = createdt;
        this.eventType = eventType;
        this.refOumUnitSrno = refOumUnitSrno;
    }

    public BigDecimal getEmperSrgKey() {
        return emperSrgKey;
    }

    public void setEmperSrgKey(BigDecimal emperSrgKey) {
        this.emperSrgKey = emperSrgKey;
    }

    public BigDecimal getEmSrgKey() {
        return emSrgKey;
    }

    public void setEmSrgKey(BigDecimal emSrgKey) {
        this.emSrgKey = emSrgKey;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public int getRefOumUnitSrno() {
        return refOumUnitSrno;
    }

    public void setRefOumUnitSrno(int refOumUnitSrno) {
        this.refOumUnitSrno = refOumUnitSrno;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUserId() {
        return userId;
    }

    public void setUserId(FhrdEmpmst userId) {
        this.userId = userId;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (emperSrgKey != null ? emperSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdEmpeventRelieve)) {
            return false;
        }
        FhrdEmpeventRelieve other = (FhrdEmpeventRelieve) object;
        if ((this.emperSrgKey == null && other.emperSrgKey != null) || (this.emperSrgKey != null && !this.emperSrgKey.equals(other.emperSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdEmpeventRelieve[ emperSrgKey=" + emperSrgKey + " ]";
    }
    
}
