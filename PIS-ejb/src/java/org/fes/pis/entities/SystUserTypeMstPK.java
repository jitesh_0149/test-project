/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author anu
 */
@Embeddable
public class SystUserTypeMstPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "OUM_UNIT_SRNO")
    private int oumUnitSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_TYPE_SRNO")
    private int userTypeSrno;

    public SystUserTypeMstPK() {
    }

    public SystUserTypeMstPK(int oumUnitSrno, int userTypeSrno) {
        this.oumUnitSrno = oumUnitSrno;
        this.userTypeSrno = userTypeSrno;
    }

    public int getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(int oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public int getUserTypeSrno() {
        return userTypeSrno;
    }

    public void setUserTypeSrno(int userTypeSrno) {
        this.userTypeSrno = userTypeSrno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) oumUnitSrno;
        hash += (int) userTypeSrno;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SystUserTypeMstPK)) {
            return false;
        }
        SystUserTypeMstPK other = (SystUserTypeMstPK) object;
        if (this.oumUnitSrno != other.oumUnitSrno) {
            return false;
        }
        if (this.userTypeSrno != other.userTypeSrno) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.SystUserTypeMstPK[ oumUnitSrno=" + oumUnitSrno + ", userTypeSrno=" + userTypeSrno + " ]";
    }
    
}
