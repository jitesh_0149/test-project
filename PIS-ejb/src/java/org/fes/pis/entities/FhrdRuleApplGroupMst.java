/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_RULE_APPL_GROUP_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdRuleApplGroupMst.findAll", query = "SELECT f FROM FhrdRuleApplGroupMst f"),
    @NamedQuery(name = "FhrdRuleApplGroupMst.findByApplGroupSrno", query = "SELECT f FROM FhrdRuleApplGroupMst f WHERE f.applGroupSrno = :applGroupSrno"),
    @NamedQuery(name = "FhrdRuleApplGroupMst.findByApplGroupDesc", query = "SELECT f FROM FhrdRuleApplGroupMst f WHERE f.applGroupDesc = :applGroupDesc"),
    @NamedQuery(name = "FhrdRuleApplGroupMst.findByCreatedt", query = "SELECT f FROM FhrdRuleApplGroupMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdRuleApplGroupMst.findByUpdatedt", query = "SELECT f FROM FhrdRuleApplGroupMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdRuleApplGroupMst.findBySystemUserid", query = "SELECT f FROM FhrdRuleApplGroupMst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdRuleApplGroupMst.findByExportFlag", query = "SELECT f FROM FhrdRuleApplGroupMst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdRuleApplGroupMst.findByImportFlag", query = "SELECT f FROM FhrdRuleApplGroupMst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdRuleApplGroupMst.findByStartDate", query = "SELECT f FROM FhrdRuleApplGroupMst f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdRuleApplGroupMst.findByEndDate", query = "SELECT f FROM FhrdRuleApplGroupMst f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FhrdRuleApplGroupMst.findByRagmSrgKey", query = "SELECT f FROM FhrdRuleApplGroupMst f WHERE f.ragmSrgKey = :ragmSrgKey"),
    @NamedQuery(name = "FhrdRuleApplGroupMst.findByContractTypeFlag", query = "SELECT f FROM FhrdRuleApplGroupMst f WHERE f.contractTypeFlag = :contractTypeFlag")})
public class FhrdRuleApplGroupMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APPL_GROUP_SRNO")
    private int applGroupSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "APPL_GROUP_DESC")
    private String applGroupDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "RAGM_SRG_KEY")
    private BigDecimal ragmSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "CONTRACT_TYPE_FLAG")
    private String contractTypeFlag;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ragmSrgKey")
    private Collection<FhrdContractGroupRelation> fhrdContractGroupRelationCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ragmSrgKey")
    private Collection<FhrdEmpContractGroupMst> fhrdEmpContractGroupMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ragmSrgKey")
    private Collection<FhrdRuleGroupRelation> fhrdRuleGroupRelationCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;

    public FhrdRuleApplGroupMst() {
    }

    public FhrdRuleApplGroupMst(BigDecimal ragmSrgKey) {
        this.ragmSrgKey = ragmSrgKey;
    }

    public FhrdRuleApplGroupMst(BigDecimal ragmSrgKey, int applGroupSrno, String applGroupDesc, Date createdt, Date startDate, String contractTypeFlag) {
        this.ragmSrgKey = ragmSrgKey;
        this.applGroupSrno = applGroupSrno;
        this.applGroupDesc = applGroupDesc;
        this.createdt = createdt;
        this.startDate = startDate;
        this.contractTypeFlag = contractTypeFlag;
    }

    public int getApplGroupSrno() {
        return applGroupSrno;
    }

    public void setApplGroupSrno(int applGroupSrno) {
        this.applGroupSrno = applGroupSrno;
    }

    public String getApplGroupDesc() {
        return applGroupDesc;
    }

    public void setApplGroupDesc(String applGroupDesc) {
        this.applGroupDesc = applGroupDesc;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getRagmSrgKey() {
        return ragmSrgKey;
    }

    public void setRagmSrgKey(BigDecimal ragmSrgKey) {
        this.ragmSrgKey = ragmSrgKey;
    }

    public String getContractTypeFlag() {
        return contractTypeFlag;
    }

    public void setContractTypeFlag(String contractTypeFlag) {
        this.contractTypeFlag = contractTypeFlag;
    }

    @XmlTransient
    public Collection<FhrdContractGroupRelation> getFhrdContractGroupRelationCollection() {
        return fhrdContractGroupRelationCollection;
    }

    public void setFhrdContractGroupRelationCollection(Collection<FhrdContractGroupRelation> fhrdContractGroupRelationCollection) {
        this.fhrdContractGroupRelationCollection = fhrdContractGroupRelationCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpContractGroupMst> getFhrdEmpContractGroupMstCollection() {
        return fhrdEmpContractGroupMstCollection;
    }

    public void setFhrdEmpContractGroupMstCollection(Collection<FhrdEmpContractGroupMst> fhrdEmpContractGroupMstCollection) {
        this.fhrdEmpContractGroupMstCollection = fhrdEmpContractGroupMstCollection;
    }

    @XmlTransient
    public Collection<FhrdRuleGroupRelation> getFhrdRuleGroupRelationCollection() {
        return fhrdRuleGroupRelationCollection;
    }

    public void setFhrdRuleGroupRelationCollection(Collection<FhrdRuleGroupRelation> fhrdRuleGroupRelationCollection) {
        this.fhrdRuleGroupRelationCollection = fhrdRuleGroupRelationCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ragmSrgKey != null ? ragmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdRuleApplGroupMst)) {
            return false;
        }
        FhrdRuleApplGroupMst other = (FhrdRuleApplGroupMst) object;
        if ((this.ragmSrgKey == null && other.ragmSrgKey != null) || (this.ragmSrgKey != null && !this.ragmSrgKey.equals(other.ragmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdRuleApplGroupMst[ ragmSrgKey=" + ragmSrgKey + " ]";
    }
    
}
