/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_CATEGORY_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasCategoryMst.findAll", query = "SELECT f FROM FtasCategoryMst f"),
    @NamedQuery(name = "FtasCategoryMst.findByCatType", query = "SELECT f FROM FtasCategoryMst f WHERE f.catType = :catType"),
    @NamedQuery(name = "FtasCategoryMst.findByCatDesc", query = "SELECT f FROM FtasCategoryMst f WHERE f.catDesc = :catDesc"),
    @NamedQuery(name = "FtasCategoryMst.findByCatValue", query = "SELECT f FROM FtasCategoryMst f WHERE f.catValue = :catValue"),
    @NamedQuery(name = "FtasCategoryMst.findByCatValueDesc", query = "SELECT f FROM FtasCategoryMst f WHERE f.catValueDesc = :catValueDesc"),
    @NamedQuery(name = "FtasCategoryMst.findByAliveFlag", query = "SELECT f FROM FtasCategoryMst f WHERE f.aliveFlag = :aliveFlag"),
    @NamedQuery(name = "FtasCategoryMst.findByCreatedt", query = "SELECT f FROM FtasCategoryMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasCategoryMst.findByUpdatedt", query = "SELECT f FROM FtasCategoryMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasCategoryMst.findByCatmSrgKey", query = "SELECT f FROM FtasCategoryMst f WHERE f.catmSrgKey = :catmSrgKey")})
public class FtasCategoryMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "CAT_TYPE")
    private String catType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CAT_DESC")
    private String catDesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "CAT_VALUE")
    private String catValue;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CAT_VALUE_DESC")
    private String catValueDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ALIVE_FLAG")
    private char aliveFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CATM_SRG_KEY")
    private BigDecimal catmSrgKey;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categoryType")
    private Collection<FtasTrainingMst> ftasTrainingMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "coOrdinatorSalut")
    private Collection<FtasTrainingMst> ftasTrainingMstCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "announcementType")
    private Collection<FtasTrainingMst> ftasTrainingMstCollection2;

    public FtasCategoryMst() {
    }

    public FtasCategoryMst(BigDecimal catmSrgKey) {
        this.catmSrgKey = catmSrgKey;
    }

    public FtasCategoryMst(BigDecimal catmSrgKey, String catType, String catDesc, String catValue, String catValueDesc, char aliveFlag, Date createdt) {
        this.catmSrgKey = catmSrgKey;
        this.catType = catType;
        this.catDesc = catDesc;
        this.catValue = catValue;
        this.catValueDesc = catValueDesc;
        this.aliveFlag = aliveFlag;
        this.createdt = createdt;
    }

    public String getCatType() {
        return catType;
    }

    public void setCatType(String catType) {
        this.catType = catType;
    }

    public String getCatDesc() {
        return catDesc;
    }

    public void setCatDesc(String catDesc) {
        this.catDesc = catDesc;
    }

    public String getCatValue() {
        return catValue;
    }

    public void setCatValue(String catValue) {
        this.catValue = catValue;
    }

    public String getCatValueDesc() {
        return catValueDesc;
    }

    public void setCatValueDesc(String catValueDesc) {
        this.catValueDesc = catValueDesc;
    }

    public char getAliveFlag() {
        return aliveFlag;
    }

    public void setAliveFlag(char aliveFlag) {
        this.aliveFlag = aliveFlag;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public BigDecimal getCatmSrgKey() {
        return catmSrgKey;
    }

    public void setCatmSrgKey(BigDecimal catmSrgKey) {
        this.catmSrgKey = catmSrgKey;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @XmlTransient
    public Collection<FtasTrainingMst> getFtasTrainingMstCollection() {
        return ftasTrainingMstCollection;
    }

    public void setFtasTrainingMstCollection(Collection<FtasTrainingMst> ftasTrainingMstCollection) {
        this.ftasTrainingMstCollection = ftasTrainingMstCollection;
    }

    @XmlTransient
    public Collection<FtasTrainingMst> getFtasTrainingMstCollection1() {
        return ftasTrainingMstCollection1;
    }

    public void setFtasTrainingMstCollection1(Collection<FtasTrainingMst> ftasTrainingMstCollection1) {
        this.ftasTrainingMstCollection1 = ftasTrainingMstCollection1;
    }

    @XmlTransient
    public Collection<FtasTrainingMst> getFtasTrainingMstCollection2() {
        return ftasTrainingMstCollection2;
    }

    public void setFtasTrainingMstCollection2(Collection<FtasTrainingMst> ftasTrainingMstCollection2) {
        this.ftasTrainingMstCollection2 = ftasTrainingMstCollection2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catmSrgKey != null ? catmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasCategoryMst)) {
            return false;
        }
        FtasCategoryMst other = (FtasCategoryMst) object;
        if ((this.catmSrgKey == null && other.catmSrgKey != null) || (this.catmSrgKey != null && !this.catmSrgKey.equals(other.catmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasCategoryMst[ catmSrgKey=" + catmSrgKey + " ]";
    }
    
}
