/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_HOLIDAY_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasHolidayDtl.findAll", query = "SELECT f FROM FtasHolidayDtl f"),
    @NamedQuery(name = "FtasHolidayDtl.findByHoliYear", query = "SELECT f FROM FtasHolidayDtl f WHERE f.holiYear = :holiYear"),
    @NamedQuery(name = "FtasHolidayDtl.findBySrno", query = "SELECT f FROM FtasHolidayDtl f WHERE f.ftasHolidayDtlPK.srno = :srno"),
    @NamedQuery(name = "FtasHolidayDtl.findByHoliDate", query = "SELECT f FROM FtasHolidayDtl f WHERE f.ftasHolidayDtlPK.holiDate = :holiDate"),
    @NamedQuery(name = "FtasHolidayDtl.findByHoliDesc", query = "SELECT f FROM FtasHolidayDtl f WHERE f.holiDesc = :holiDesc"),
    @NamedQuery(name = "FtasHolidayDtl.findByWeekendFlag", query = "SELECT f FROM FtasHolidayDtl f WHERE f.weekendFlag = :weekendFlag"),
    @NamedQuery(name = "FtasHolidayDtl.findByScheduleFlag", query = "SELECT f FROM FtasHolidayDtl f WHERE f.scheduleFlag = :scheduleFlag"),
    @NamedQuery(name = "FtasHolidayDtl.findBySuddenDeclareFlag", query = "SELECT f FROM FtasHolidayDtl f WHERE f.suddenDeclareFlag = :suddenDeclareFlag"),
    @NamedQuery(name = "FtasHolidayDtl.findByActionFlag", query = "SELECT f FROM FtasHolidayDtl f WHERE f.actionFlag = :actionFlag"),
    @NamedQuery(name = "FtasHolidayDtl.findByRemarks", query = "SELECT f FROM FtasHolidayDtl f WHERE f.remarks = :remarks"),
    @NamedQuery(name = "FtasHolidayDtl.findByCreatedt", query = "SELECT f FROM FtasHolidayDtl f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasHolidayDtl.findByUpdatedt", query = "SELECT f FROM FtasHolidayDtl f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasHolidayDtl.findByExportFlag", query = "SELECT f FROM FtasHolidayDtl f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FtasHolidayDtl.findByImportFlag", query = "SELECT f FROM FtasHolidayDtl f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FtasHolidayDtl.findBySystemUserid", query = "SELECT f FROM FtasHolidayDtl f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FtasHolidayDtl.findByHmSrgKey", query = "SELECT f FROM FtasHolidayDtl f WHERE f.ftasHolidayDtlPK.hmSrgKey = :hmSrgKey"),
    @NamedQuery(name = "FtasHolidayDtl.findByTranYear", query = "SELECT f FROM FtasHolidayDtl f WHERE f.tranYear = :tranYear")})
public class FtasHolidayDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FtasHolidayDtlPK ftasHolidayDtlPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "HOLI_YEAR")
    private short holiYear;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "HOLI_DESC")
    private String holiDesc;
    @Size(max = 1)
    @Column(name = "WEEKEND_FLAG")
    private String weekendFlag;
    @Size(max = 1)
    @Column(name = "SCHEDULE_FLAG")
    private String scheduleFlag;
    @Size(max = 1)
    @Column(name = "SUDDEN_DECLARE_FLAG")
    private String suddenDeclareFlag;
    @Size(max = 1)
    @Column(name = "ACTION_FLAG")
    private String actionFlag;
    @Size(max = 1000)
    @Column(name = "REMARKS")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "HM_SRG_KEY", referencedColumnName = "HM_SRG_KEY", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FtasHolidayMst ftasHolidayMst;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FtasHolidayDtl() {
    }

    public FtasHolidayDtl(FtasHolidayDtlPK ftasHolidayDtlPK) {
        this.ftasHolidayDtlPK = ftasHolidayDtlPK;
    }

    public FtasHolidayDtl(FtasHolidayDtlPK ftasHolidayDtlPK, short holiYear, String holiDesc, Date createdt, short tranYear) {
        this.ftasHolidayDtlPK = ftasHolidayDtlPK;
        this.holiYear = holiYear;
        this.holiDesc = holiDesc;
        this.createdt = createdt;
        this.tranYear = tranYear;
    }

    public FtasHolidayDtl(short srno, Date holiDate, BigDecimal hmSrgKey) {
        this.ftasHolidayDtlPK = new FtasHolidayDtlPK(srno, holiDate, hmSrgKey);
    }

    public FtasHolidayDtlPK getFtasHolidayDtlPK() {
        return ftasHolidayDtlPK;
    }

    public void setFtasHolidayDtlPK(FtasHolidayDtlPK ftasHolidayDtlPK) {
        this.ftasHolidayDtlPK = ftasHolidayDtlPK;
    }

    public short getHoliYear() {
        return holiYear;
    }

    public void setHoliYear(short holiYear) {
        this.holiYear = holiYear;
    }

    public String getHoliDesc() {
        return holiDesc;
    }

    public void setHoliDesc(String holiDesc) {
        this.holiDesc = holiDesc;
    }

    public String getWeekendFlag() {
        return weekendFlag;
    }

    public void setWeekendFlag(String weekendFlag) {
        this.weekendFlag = weekendFlag;
    }

    public String getScheduleFlag() {
        return scheduleFlag;
    }

    public void setScheduleFlag(String scheduleFlag) {
        this.scheduleFlag = scheduleFlag;
    }

    public String getSuddenDeclareFlag() {
        return suddenDeclareFlag;
    }

    public void setSuddenDeclareFlag(String suddenDeclareFlag) {
        this.suddenDeclareFlag = suddenDeclareFlag;
    }

    public String getActionFlag() {
        return actionFlag;
    }

    public void setActionFlag(String actionFlag) {
        this.actionFlag = actionFlag;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasHolidayMst getFtasHolidayMst() {
        return ftasHolidayMst;
    }

    public void setFtasHolidayMst(FtasHolidayMst ftasHolidayMst) {
        this.ftasHolidayMst = ftasHolidayMst;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ftasHolidayDtlPK != null ? ftasHolidayDtlPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasHolidayDtl)) {
            return false;
        }
        FtasHolidayDtl other = (FtasHolidayDtl) object;
        if ((this.ftasHolidayDtlPK == null && other.ftasHolidayDtlPK != null) || (this.ftasHolidayDtlPK != null && !this.ftasHolidayDtlPK.equals(other.ftasHolidayDtlPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasHolidayDtl[ ftasHolidayDtlPK=" + ftasHolidayDtlPK + " ]";
    }
    
}
