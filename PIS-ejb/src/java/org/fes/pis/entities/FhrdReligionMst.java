/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_RELIGION_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdReligionMst.findAll", query = "SELECT f FROM FhrdReligionMst f"),
    @NamedQuery(name = "FhrdReligionMst.findByRgSrgKey", query = "SELECT f FROM FhrdReligionMst f WHERE f.rgSrgKey = :rgSrgKey"),
    @NamedQuery(name = "FhrdReligionMst.findByRgDescription", query = "SELECT f FROM FhrdReligionMst f WHERE f.rgDescription = :rgDescription"),
    @NamedQuery(name = "FhrdReligionMst.findByCreatedt", query = "SELECT f FROM FhrdReligionMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdReligionMst.findByUpdatedt", query = "SELECT f FROM FhrdReligionMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdReligionMst.findByExportFlag", query = "SELECT f FROM FhrdReligionMst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdReligionMst.findByImportFlag", query = "SELECT f FROM FhrdReligionMst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdReligionMst.findBySystemUserid", query = "SELECT f FROM FhrdReligionMst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdReligionMst.findByStartDate", query = "SELECT f FROM FhrdReligionMst f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdReligionMst.findByEndDate", query = "SELECT f FROM FhrdReligionMst f WHERE f.endDate = :endDate")})
public class FhrdReligionMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "RG_SRG_KEY")
    private Integer rgSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "RG_DESCRIPTION")
    private String rgDescription;
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FhrdReligionMst() {
    }

    public FhrdReligionMst(Integer rgSrgKey) {
        this.rgSrgKey = rgSrgKey;
    }

    public FhrdReligionMst(Integer rgSrgKey, String rgDescription, Date startDate) {
        this.rgSrgKey = rgSrgKey;
        this.rgDescription = rgDescription;
        this.startDate = startDate;
    }

    public Integer getRgSrgKey() {
        return rgSrgKey;
    }

    public void setRgSrgKey(Integer rgSrgKey) {
        this.rgSrgKey = rgSrgKey;
    }

    public String getRgDescription() {
        return rgDescription;
    }

    public void setRgDescription(String rgDescription) {
        this.rgDescription = rgDescription;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rgSrgKey != null ? rgSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdReligionMst)) {
            return false;
        }
        FhrdReligionMst other = (FhrdReligionMst) object;
        if ((this.rgSrgKey == null && other.rgSrgKey != null) || (this.rgSrgKey != null && !this.rgSrgKey.equals(other.rgSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdReligionMst[ rgSrgKey=" + rgSrgKey + " ]";
    }
    
}
