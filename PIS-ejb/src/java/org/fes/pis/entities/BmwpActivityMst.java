/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "BMWP_ACTIVITY_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmwpActivityMst.findAll", query = "SELECT b FROM BmwpActivityMst b"),
    @NamedQuery(name = "BmwpActivityMst.findByAmUniqueSrno", query = "SELECT b FROM BmwpActivityMst b WHERE b.amUniqueSrno = :amUniqueSrno"),
    @NamedQuery(name = "BmwpActivityMst.findByAmSrno", query = "SELECT b FROM BmwpActivityMst b WHERE b.amSrno = :amSrno"),
    @NamedQuery(name = "BmwpActivityMst.findByAmActivitySrno", query = "SELECT b FROM BmwpActivityMst b WHERE b.amActivitySrno = :amActivitySrno"),
    @NamedQuery(name = "BmwpActivityMst.findByAmActivityShortName", query = "SELECT b FROM BmwpActivityMst b WHERE b.amActivityShortName = :amActivityShortName"),
    @NamedQuery(name = "BmwpActivityMst.findByAmActivityDesc", query = "SELECT b FROM BmwpActivityMst b WHERE b.amActivityDesc = :amActivityDesc"),
    @NamedQuery(name = "BmwpActivityMst.findByAmParentActivitySrno", query = "SELECT b FROM BmwpActivityMst b WHERE b.amParentActivitySrno = :amParentActivitySrno"),
    @NamedQuery(name = "BmwpActivityMst.findByAmActivitySrnoLevel", query = "SELECT b FROM BmwpActivityMst b WHERE b.amActivitySrnoLevel = :amActivitySrnoLevel"),
    @NamedQuery(name = "BmwpActivityMst.findByCreatedt", query = "SELECT b FROM BmwpActivityMst b WHERE b.createdt = :createdt"),
    @NamedQuery(name = "BmwpActivityMst.findByUpdatedt", query = "SELECT b FROM BmwpActivityMst b WHERE b.updatedt = :updatedt"),
    @NamedQuery(name = "BmwpActivityMst.findBySystemUserid", query = "SELECT b FROM BmwpActivityMst b WHERE b.systemUserid = :systemUserid"),
    @NamedQuery(name = "BmwpActivityMst.findByExportFlag", query = "SELECT b FROM BmwpActivityMst b WHERE b.exportFlag = :exportFlag"),
    @NamedQuery(name = "BmwpActivityMst.findByImportFlag", query = "SELECT b FROM BmwpActivityMst b WHERE b.importFlag = :importFlag"),
    @NamedQuery(name = "BmwpActivityMst.findByAmStartDate", query = "SELECT b FROM BmwpActivityMst b WHERE b.amStartDate = :amStartDate"),
    @NamedQuery(name = "BmwpActivityMst.findByAmEndDate", query = "SELECT b FROM BmwpActivityMst b WHERE b.amEndDate = :amEndDate"),
    @NamedQuery(name = "BmwpActivityMst.findByAmTransactionAppliId", query = "SELECT b FROM BmwpActivityMst b WHERE b.amTransactionAppliId = :amTransactionAppliId"),
    @NamedQuery(name = "BmwpActivityMst.findByAmEndDatePossible", query = "SELECT b FROM BmwpActivityMst b WHERE b.amEndDatePossible = :amEndDatePossible"),
    @NamedQuery(name = "BmwpActivityMst.findByAmReCreateFlag", query = "SELECT b FROM BmwpActivityMst b WHERE b.amReCreateFlag = :amReCreateFlag"),
    @NamedQuery(name = "BmwpActivityMst.findByAmChildOfferFlag", query = "SELECT b FROM BmwpActivityMst b WHERE b.amChildOfferFlag = :amChildOfferFlag"),
    @NamedQuery(name = "BmwpActivityMst.findByAmTransactionDate", query = "SELECT b FROM BmwpActivityMst b WHERE b.amTransactionDate = :amTransactionDate"),
    @NamedQuery(name = "BmwpActivityMst.findByAmWorkplanAmt", query = "SELECT b FROM BmwpActivityMst b WHERE b.amWorkplanAmt = :amWorkplanAmt"),
    @NamedQuery(name = "BmwpActivityMst.findByAmWpSurrenderAmt", query = "SELECT b FROM BmwpActivityMst b WHERE b.amWpSurrenderAmt = :amWpSurrenderAmt"),
    @NamedQuery(name = "BmwpActivityMst.findByAmReAllotWpAmt", query = "SELECT b FROM BmwpActivityMst b WHERE b.amReAllotWpAmt = :amReAllotWpAmt"),
    @NamedQuery(name = "BmwpActivityMst.findByAmReviseWpAmt", query = "SELECT b FROM BmwpActivityMst b WHERE b.amReviseWpAmt = :amReviseWpAmt"),
    @NamedQuery(name = "BmwpActivityMst.findByAmCommitedAmt", query = "SELECT b FROM BmwpActivityMst b WHERE b.amCommitedAmt = :amCommitedAmt"),
    @NamedQuery(name = "BmwpActivityMst.findByAmPipelinePayAmt", query = "SELECT b FROM BmwpActivityMst b WHERE b.amPipelinePayAmt = :amPipelinePayAmt"),
    @NamedQuery(name = "BmwpActivityMst.findByAmPipeCommitedAmt", query = "SELECT b FROM BmwpActivityMst b WHERE b.amPipeCommitedAmt = :amPipeCommitedAmt"),
    @NamedQuery(name = "BmwpActivityMst.findByAmAdvancePayAmt", query = "SELECT b FROM BmwpActivityMst b WHERE b.amAdvancePayAmt = :amAdvancePayAmt"),
    @NamedQuery(name = "BmwpActivityMst.findByAmAdvanceCommitAmt", query = "SELECT b FROM BmwpActivityMst b WHERE b.amAdvanceCommitAmt = :amAdvanceCommitAmt"),
    @NamedQuery(name = "BmwpActivityMst.findByAmPipeAdvadjPayAmt", query = "SELECT b FROM BmwpActivityMst b WHERE b.amPipeAdvadjPayAmt = :amPipeAdvadjPayAmt"),
    @NamedQuery(name = "BmwpActivityMst.findByAmPipeAdvadjCommitAmt", query = "SELECT b FROM BmwpActivityMst b WHERE b.amPipeAdvadjCommitAmt = :amPipeAdvadjCommitAmt"),
    @NamedQuery(name = "BmwpActivityMst.findByAmReceiptPayAmt", query = "SELECT b FROM BmwpActivityMst b WHERE b.amReceiptPayAmt = :amReceiptPayAmt"),
    @NamedQuery(name = "BmwpActivityMst.findByAmReceiptCommitAmt", query = "SELECT b FROM BmwpActivityMst b WHERE b.amReceiptCommitAmt = :amReceiptCommitAmt"),
    @NamedQuery(name = "BmwpActivityMst.findByAmProCrExpAmt", query = "SELECT b FROM BmwpActivityMst b WHERE b.amProCrExpAmt = :amProCrExpAmt"),
    @NamedQuery(name = "BmwpActivityMst.findByAmExpenseAmt", query = "SELECT b FROM BmwpActivityMst b WHERE b.amExpenseAmt = :amExpenseAmt"),
    @NamedQuery(name = "BmwpActivityMst.findByAmCommContriAmt", query = "SELECT b FROM BmwpActivityMst b WHERE b.amCommContriAmt = :amCommContriAmt"),
    @NamedQuery(name = "BmwpActivityMst.findByAmUsableBalanceAmt", query = "SELECT b FROM BmwpActivityMst b WHERE b.amUsableBalanceAmt = :amUsableBalanceAmt"),
    @NamedQuery(name = "BmwpActivityMst.findByAmBalanceAmt", query = "SELECT b FROM BmwpActivityMst b WHERE b.amBalanceAmt = :amBalanceAmt"),
    @NamedQuery(name = "BmwpActivityMst.findByAmPmFundMonitorFlag", query = "SELECT b FROM BmwpActivityMst b WHERE b.amPmFundMonitorFlag = :amPmFundMonitorFlag")})
public class BmwpActivityMst implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_UNIQUE_SRNO")
    private BigDecimal amUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_SRNO")
    private int amSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_ACTIVITY_SRNO")
    private BigInteger amActivitySrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "AM_ACTIVITY_SHORT_NAME")
    private String amActivityShortName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "AM_ACTIVITY_DESC")
    private String amActivityDesc;
    @Column(name = "AM_PARENT_ACTIVITY_SRNO")
    private BigInteger amParentActivitySrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_ACTIVITY_SRNO_LEVEL")
    private int amActivitySrnoLevel;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date amStartDate;
    @Column(name = "AM_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date amEndDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_TRANSACTION_APPLI_ID")
    private short amTransactionAppliId;
    @Column(name = "AM_END_DATE_POSSIBLE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date amEndDatePossible;
    @Size(max = 1)
    @Column(name = "AM_RE_CREATE_FLAG")
    private String amReCreateFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AM_CHILD_OFFER_FLAG")
    private String amChildOfferFlag;
    @Column(name = "AM_TRANSACTION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date amTransactionDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_WORKPLAN_AMT")
    private BigDecimal amWorkplanAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_WP_SURRENDER_AMT")
    private BigDecimal amWpSurrenderAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_RE_ALLOT_WP_AMT")
    private BigDecimal amReAllotWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_REVISE_WP_AMT")
    private BigDecimal amReviseWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_COMMITED_AMT")
    private BigDecimal amCommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_PIPELINE_PAY_AMT")
    private BigDecimal amPipelinePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_PIPE_COMMITED_AMT")
    private BigDecimal amPipeCommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_ADVANCE_PAY_AMT")
    private BigDecimal amAdvancePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_ADVANCE_COMMIT_AMT")
    private BigDecimal amAdvanceCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_PIPE_ADVADJ_PAY_AMT")
    private BigDecimal amPipeAdvadjPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_PIPE_ADVADJ_COMMIT_AMT")
    private BigDecimal amPipeAdvadjCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_RECEIPT_PAY_AMT")
    private BigDecimal amReceiptPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_RECEIPT_COMMIT_AMT")
    private BigDecimal amReceiptCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_PRO_CR_EXP_AMT")
    private BigDecimal amProCrExpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_EXPENSE_AMT")
    private BigDecimal amExpenseAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_COMM_CONTRI_AMT")
    private BigDecimal amCommContriAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_USABLE_BALANCE_AMT")
    private BigDecimal amUsableBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_BALANCE_AMT")
    private BigDecimal amBalanceAmt;
    @Size(max = 1)
    @Column(name = "AM_PM_FUND_MONITOR_FLAG")
    private String amPmFundMonitorFlag;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ablAmUniqueSrno")
    private Collection<BmwpActivityBudgetLink> bmwpActivityBudgetLinkCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst createby;
    @JoinColumn(name = "AM_UM_UNIQUE_UNIT_SRNO", referencedColumnName = "UM_UNIQUE_SRNO")
    @ManyToOne(optional = false)
    private BmwpUnitMst amUmUniqueUnitSrno;
    @OneToMany(mappedBy = "amParentUniqueSrno")
    private Collection<BmwpActivityMst> bmwpActivityMstCollection;
    @JoinColumn(name = "AM_PARENT_UNIQUE_SRNO", referencedColumnName = "AM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpActivityMst amParentUniqueSrno;
    @OneToMany(mappedBy = "wpAmUniqueSrno")
    private Collection<BmwpWorkplan> bmwpWorkplanCollection;
    @OneToMany(mappedBy = "abdAmUniqueSrno")
    private Collection<BmwpActivityBudgetDtl> bmwpActivityBudgetDtlCollection;

    public BmwpActivityMst() {
    }

    public BmwpActivityMst(BigDecimal amUniqueSrno) {
        this.amUniqueSrno = amUniqueSrno;
    }

    public BmwpActivityMst(BigDecimal amUniqueSrno, int amSrno, BigInteger amActivitySrno, String amActivityShortName, String amActivityDesc, int amActivitySrnoLevel, Date createdt, Date amStartDate, short amTransactionAppliId, String amChildOfferFlag, BigDecimal amWorkplanAmt, BigDecimal amWpSurrenderAmt, BigDecimal amReAllotWpAmt, BigDecimal amReviseWpAmt, BigDecimal amCommitedAmt, BigDecimal amPipelinePayAmt, BigDecimal amPipeCommitedAmt, BigDecimal amAdvancePayAmt, BigDecimal amAdvanceCommitAmt, BigDecimal amPipeAdvadjPayAmt, BigDecimal amPipeAdvadjCommitAmt, BigDecimal amReceiptPayAmt, BigDecimal amReceiptCommitAmt, BigDecimal amProCrExpAmt, BigDecimal amExpenseAmt, BigDecimal amCommContriAmt, BigDecimal amUsableBalanceAmt, BigDecimal amBalanceAmt) {
        this.amUniqueSrno = amUniqueSrno;
        this.amSrno = amSrno;
        this.amActivitySrno = amActivitySrno;
        this.amActivityShortName = amActivityShortName;
        this.amActivityDesc = amActivityDesc;
        this.amActivitySrnoLevel = amActivitySrnoLevel;
        this.createdt = createdt;
        this.amStartDate = amStartDate;
        this.amTransactionAppliId = amTransactionAppliId;
        this.amChildOfferFlag = amChildOfferFlag;
        this.amWorkplanAmt = amWorkplanAmt;
        this.amWpSurrenderAmt = amWpSurrenderAmt;
        this.amReAllotWpAmt = amReAllotWpAmt;
        this.amReviseWpAmt = amReviseWpAmt;
        this.amCommitedAmt = amCommitedAmt;
        this.amPipelinePayAmt = amPipelinePayAmt;
        this.amPipeCommitedAmt = amPipeCommitedAmt;
        this.amAdvancePayAmt = amAdvancePayAmt;
        this.amAdvanceCommitAmt = amAdvanceCommitAmt;
        this.amPipeAdvadjPayAmt = amPipeAdvadjPayAmt;
        this.amPipeAdvadjCommitAmt = amPipeAdvadjCommitAmt;
        this.amReceiptPayAmt = amReceiptPayAmt;
        this.amReceiptCommitAmt = amReceiptCommitAmt;
        this.amProCrExpAmt = amProCrExpAmt;
        this.amExpenseAmt = amExpenseAmt;
        this.amCommContriAmt = amCommContriAmt;
        this.amUsableBalanceAmt = amUsableBalanceAmt;
        this.amBalanceAmt = amBalanceAmt;
    }

    public BigDecimal getAmUniqueSrno() {
        return amUniqueSrno;
    }

    public void setAmUniqueSrno(BigDecimal amUniqueSrno) {
        this.amUniqueSrno = amUniqueSrno;
    }

    public int getAmSrno() {
        return amSrno;
    }

    public void setAmSrno(int amSrno) {
        this.amSrno = amSrno;
    }

    public BigInteger getAmActivitySrno() {
        return amActivitySrno;
    }

    public void setAmActivitySrno(BigInteger amActivitySrno) {
        this.amActivitySrno = amActivitySrno;
    }

    public String getAmActivityShortName() {
        return amActivityShortName;
    }

    public void setAmActivityShortName(String amActivityShortName) {
        this.amActivityShortName = amActivityShortName;
    }

    public String getAmActivityDesc() {
        return amActivityDesc;
    }

    public void setAmActivityDesc(String amActivityDesc) {
        this.amActivityDesc = amActivityDesc;
    }

    public BigInteger getAmParentActivitySrno() {
        return amParentActivitySrno;
    }

    public void setAmParentActivitySrno(BigInteger amParentActivitySrno) {
        this.amParentActivitySrno = amParentActivitySrno;
    }

    public int getAmActivitySrnoLevel() {
        return amActivitySrnoLevel;
    }

    public void setAmActivitySrnoLevel(int amActivitySrnoLevel) {
        this.amActivitySrnoLevel = amActivitySrnoLevel;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Date getAmStartDate() {
        return amStartDate;
    }

    public void setAmStartDate(Date amStartDate) {
        this.amStartDate = amStartDate;
    }

    public Date getAmEndDate() {
        return amEndDate;
    }

    public void setAmEndDate(Date amEndDate) {
        this.amEndDate = amEndDate;
    }

    public short getAmTransactionAppliId() {
        return amTransactionAppliId;
    }

    public void setAmTransactionAppliId(short amTransactionAppliId) {
        this.amTransactionAppliId = amTransactionAppliId;
    }

    public Date getAmEndDatePossible() {
        return amEndDatePossible;
    }

    public void setAmEndDatePossible(Date amEndDatePossible) {
        this.amEndDatePossible = amEndDatePossible;
    }

    public String getAmReCreateFlag() {
        return amReCreateFlag;
    }

    public void setAmReCreateFlag(String amReCreateFlag) {
        this.amReCreateFlag = amReCreateFlag;
    }

    public String getAmChildOfferFlag() {
        return amChildOfferFlag;
    }

    public void setAmChildOfferFlag(String amChildOfferFlag) {
        this.amChildOfferFlag = amChildOfferFlag;
    }

    public Date getAmTransactionDate() {
        return amTransactionDate;
    }

    public void setAmTransactionDate(Date amTransactionDate) {
        this.amTransactionDate = amTransactionDate;
    }

    public BigDecimal getAmWorkplanAmt() {
        return amWorkplanAmt;
    }

    public void setAmWorkplanAmt(BigDecimal amWorkplanAmt) {
        this.amWorkplanAmt = amWorkplanAmt;
    }

    public BigDecimal getAmWpSurrenderAmt() {
        return amWpSurrenderAmt;
    }

    public void setAmWpSurrenderAmt(BigDecimal amWpSurrenderAmt) {
        this.amWpSurrenderAmt = amWpSurrenderAmt;
    }

    public BigDecimal getAmReAllotWpAmt() {
        return amReAllotWpAmt;
    }

    public void setAmReAllotWpAmt(BigDecimal amReAllotWpAmt) {
        this.amReAllotWpAmt = amReAllotWpAmt;
    }

    public BigDecimal getAmReviseWpAmt() {
        return amReviseWpAmt;
    }

    public void setAmReviseWpAmt(BigDecimal amReviseWpAmt) {
        this.amReviseWpAmt = amReviseWpAmt;
    }

    public BigDecimal getAmCommitedAmt() {
        return amCommitedAmt;
    }

    public void setAmCommitedAmt(BigDecimal amCommitedAmt) {
        this.amCommitedAmt = amCommitedAmt;
    }

    public BigDecimal getAmPipelinePayAmt() {
        return amPipelinePayAmt;
    }

    public void setAmPipelinePayAmt(BigDecimal amPipelinePayAmt) {
        this.amPipelinePayAmt = amPipelinePayAmt;
    }

    public BigDecimal getAmPipeCommitedAmt() {
        return amPipeCommitedAmt;
    }

    public void setAmPipeCommitedAmt(BigDecimal amPipeCommitedAmt) {
        this.amPipeCommitedAmt = amPipeCommitedAmt;
    }

    public BigDecimal getAmAdvancePayAmt() {
        return amAdvancePayAmt;
    }

    public void setAmAdvancePayAmt(BigDecimal amAdvancePayAmt) {
        this.amAdvancePayAmt = amAdvancePayAmt;
    }

    public BigDecimal getAmAdvanceCommitAmt() {
        return amAdvanceCommitAmt;
    }

    public void setAmAdvanceCommitAmt(BigDecimal amAdvanceCommitAmt) {
        this.amAdvanceCommitAmt = amAdvanceCommitAmt;
    }

    public BigDecimal getAmPipeAdvadjPayAmt() {
        return amPipeAdvadjPayAmt;
    }

    public void setAmPipeAdvadjPayAmt(BigDecimal amPipeAdvadjPayAmt) {
        this.amPipeAdvadjPayAmt = amPipeAdvadjPayAmt;
    }

    public BigDecimal getAmPipeAdvadjCommitAmt() {
        return amPipeAdvadjCommitAmt;
    }

    public void setAmPipeAdvadjCommitAmt(BigDecimal amPipeAdvadjCommitAmt) {
        this.amPipeAdvadjCommitAmt = amPipeAdvadjCommitAmt;
    }

    public BigDecimal getAmReceiptPayAmt() {
        return amReceiptPayAmt;
    }

    public void setAmReceiptPayAmt(BigDecimal amReceiptPayAmt) {
        this.amReceiptPayAmt = amReceiptPayAmt;
    }

    public BigDecimal getAmReceiptCommitAmt() {
        return amReceiptCommitAmt;
    }

    public void setAmReceiptCommitAmt(BigDecimal amReceiptCommitAmt) {
        this.amReceiptCommitAmt = amReceiptCommitAmt;
    }

    public BigDecimal getAmProCrExpAmt() {
        return amProCrExpAmt;
    }

    public void setAmProCrExpAmt(BigDecimal amProCrExpAmt) {
        this.amProCrExpAmt = amProCrExpAmt;
    }

    public BigDecimal getAmExpenseAmt() {
        return amExpenseAmt;
    }

    public void setAmExpenseAmt(BigDecimal amExpenseAmt) {
        this.amExpenseAmt = amExpenseAmt;
    }

    public BigDecimal getAmCommContriAmt() {
        return amCommContriAmt;
    }

    public void setAmCommContriAmt(BigDecimal amCommContriAmt) {
        this.amCommContriAmt = amCommContriAmt;
    }

    public BigDecimal getAmUsableBalanceAmt() {
        return amUsableBalanceAmt;
    }

    public void setAmUsableBalanceAmt(BigDecimal amUsableBalanceAmt) {
        this.amUsableBalanceAmt = amUsableBalanceAmt;
    }

    public BigDecimal getAmBalanceAmt() {
        return amBalanceAmt;
    }

    public void setAmBalanceAmt(BigDecimal amBalanceAmt) {
        this.amBalanceAmt = amBalanceAmt;
    }

    public String getAmPmFundMonitorFlag() {
        return amPmFundMonitorFlag;
    }

    public void setAmPmFundMonitorFlag(String amPmFundMonitorFlag) {
        this.amPmFundMonitorFlag = amPmFundMonitorFlag;
    }

    @XmlTransient
    public Collection<BmwpActivityBudgetLink> getBmwpActivityBudgetLinkCollection() {
        return bmwpActivityBudgetLinkCollection;
    }

    public void setBmwpActivityBudgetLinkCollection(Collection<BmwpActivityBudgetLink> bmwpActivityBudgetLinkCollection) {
        this.bmwpActivityBudgetLinkCollection = bmwpActivityBudgetLinkCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public BmwpUnitMst getAmUmUniqueUnitSrno() {
        return amUmUniqueUnitSrno;
    }

    public void setAmUmUniqueUnitSrno(BmwpUnitMst amUmUniqueUnitSrno) {
        this.amUmUniqueUnitSrno = amUmUniqueUnitSrno;
    }

    @XmlTransient
    public Collection<BmwpActivityMst> getBmwpActivityMstCollection() {
        return bmwpActivityMstCollection;
    }

    public void setBmwpActivityMstCollection(Collection<BmwpActivityMst> bmwpActivityMstCollection) {
        this.bmwpActivityMstCollection = bmwpActivityMstCollection;
    }

    public BmwpActivityMst getAmParentUniqueSrno() {
        return amParentUniqueSrno;
    }

    public void setAmParentUniqueSrno(BmwpActivityMst amParentUniqueSrno) {
        this.amParentUniqueSrno = amParentUniqueSrno;
    }

    @XmlTransient
    public Collection<BmwpWorkplan> getBmwpWorkplanCollection() {
        return bmwpWorkplanCollection;
    }

    public void setBmwpWorkplanCollection(Collection<BmwpWorkplan> bmwpWorkplanCollection) {
        this.bmwpWorkplanCollection = bmwpWorkplanCollection;
    }

    @XmlTransient
    public Collection<BmwpActivityBudgetDtl> getBmwpActivityBudgetDtlCollection() {
        return bmwpActivityBudgetDtlCollection;
    }

    public void setBmwpActivityBudgetDtlCollection(Collection<BmwpActivityBudgetDtl> bmwpActivityBudgetDtlCollection) {
        this.bmwpActivityBudgetDtlCollection = bmwpActivityBudgetDtlCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (amUniqueSrno != null ? amUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmwpActivityMst)) {
            return false;
        }
        BmwpActivityMst other = (BmwpActivityMst) object;
        if ((this.amUniqueSrno == null && other.amUniqueSrno != null) || (this.amUniqueSrno != null && !this.amUniqueSrno.equals(other.amUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.BmwpActivityMst[ amUniqueSrno=" + amUniqueSrno + " ]";
    }
    
}
