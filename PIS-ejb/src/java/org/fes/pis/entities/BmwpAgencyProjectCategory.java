/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "BMWP_AGENCY_PROJECT_CATEGORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmwpAgencyProjectCategory.findAll", query = "SELECT b FROM BmwpAgencyProjectCategory b"),
    @NamedQuery(name = "BmwpAgencyProjectCategory.findByApcUniqueSrno", query = "SELECT b FROM BmwpAgencyProjectCategory b WHERE b.apcUniqueSrno = :apcUniqueSrno"),
    @NamedQuery(name = "BmwpAgencyProjectCategory.findByApcCategorySrno", query = "SELECT b FROM BmwpAgencyProjectCategory b WHERE b.apcCategorySrno = :apcCategorySrno"),
    @NamedQuery(name = "BmwpAgencyProjectCategory.findByApcCategoryName", query = "SELECT b FROM BmwpAgencyProjectCategory b WHERE b.apcCategoryName = :apcCategoryName"),
    @NamedQuery(name = "BmwpAgencyProjectCategory.findByCreatedt", query = "SELECT b FROM BmwpAgencyProjectCategory b WHERE b.createdt = :createdt"),
    @NamedQuery(name = "BmwpAgencyProjectCategory.findByUpdatedt", query = "SELECT b FROM BmwpAgencyProjectCategory b WHERE b.updatedt = :updatedt"),
    @NamedQuery(name = "BmwpAgencyProjectCategory.findBySystemUserid", query = "SELECT b FROM BmwpAgencyProjectCategory b WHERE b.systemUserid = :systemUserid"),
    @NamedQuery(name = "BmwpAgencyProjectCategory.findByExportFlag", query = "SELECT b FROM BmwpAgencyProjectCategory b WHERE b.exportFlag = :exportFlag"),
    @NamedQuery(name = "BmwpAgencyProjectCategory.findByImportFlag", query = "SELECT b FROM BmwpAgencyProjectCategory b WHERE b.importFlag = :importFlag")})
public class BmwpAgencyProjectCategory implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "APC_UNIQUE_SRNO")
    private BigDecimal apcUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APC_CATEGORY_SRNO")
    private int apcCategorySrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "APC_CATEGORY_NAME")
    private String apcCategoryName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @OneToMany(mappedBy = "aplApcUniqueSrno")
    private Collection<BmwpAgencyProjectLink> bmwpAgencyProjectLinkCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public BmwpAgencyProjectCategory() {
    }

    public BmwpAgencyProjectCategory(BigDecimal apcUniqueSrno) {
        this.apcUniqueSrno = apcUniqueSrno;
    }

    public BmwpAgencyProjectCategory(BigDecimal apcUniqueSrno, int apcCategorySrno, String apcCategoryName, Date createdt) {
        this.apcUniqueSrno = apcUniqueSrno;
        this.apcCategorySrno = apcCategorySrno;
        this.apcCategoryName = apcCategoryName;
        this.createdt = createdt;
    }

    public BigDecimal getApcUniqueSrno() {
        return apcUniqueSrno;
    }

    public void setApcUniqueSrno(BigDecimal apcUniqueSrno) {
        this.apcUniqueSrno = apcUniqueSrno;
    }

    public int getApcCategorySrno() {
        return apcCategorySrno;
    }

    public void setApcCategorySrno(int apcCategorySrno) {
        this.apcCategorySrno = apcCategorySrno;
    }

    public String getApcCategoryName() {
        return apcCategoryName;
    }

    public void setApcCategoryName(String apcCategoryName) {
        this.apcCategoryName = apcCategoryName;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    @XmlTransient
    public Collection<BmwpAgencyProjectLink> getBmwpAgencyProjectLinkCollection() {
        return bmwpAgencyProjectLinkCollection;
    }

    public void setBmwpAgencyProjectLinkCollection(Collection<BmwpAgencyProjectLink> bmwpAgencyProjectLinkCollection) {
        this.bmwpAgencyProjectLinkCollection = bmwpAgencyProjectLinkCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (apcUniqueSrno != null ? apcUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmwpAgencyProjectCategory)) {
            return false;
        }
        BmwpAgencyProjectCategory other = (BmwpAgencyProjectCategory) object;
        if ((this.apcUniqueSrno == null && other.apcUniqueSrno != null) || (this.apcUniqueSrno != null && !this.apcUniqueSrno.equals(other.apcUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.BmwpAgencyProjectCategory[ apcUniqueSrno=" + apcUniqueSrno + " ]";
    }
    
}
