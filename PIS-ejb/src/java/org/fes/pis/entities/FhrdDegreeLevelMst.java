/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_DEGREE_LEVEL_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdDegreeLevelMst.findAll", query = "SELECT f FROM FhrdDegreeLevelMst f"),
    @NamedQuery(name = "FhrdDegreeLevelMst.findByDlmSrgKey", query = "SELECT f FROM FhrdDegreeLevelMst f WHERE f.dlmSrgKey = :dlmSrgKey"),
    @NamedQuery(name = "FhrdDegreeLevelMst.findByDlmDescription", query = "SELECT f FROM FhrdDegreeLevelMst f WHERE f.dlmDescription = :dlmDescription"),
    @NamedQuery(name = "FhrdDegreeLevelMst.findByStartDate", query = "SELECT f FROM FhrdDegreeLevelMst f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdDegreeLevelMst.findByEndDate", query = "SELECT f FROM FhrdDegreeLevelMst f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FhrdDegreeLevelMst.findByCreatedt", query = "SELECT f FROM FhrdDegreeLevelMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdDegreeLevelMst.findByUpdatedt", query = "SELECT f FROM FhrdDegreeLevelMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdDegreeLevelMst.findByExportFlag", query = "SELECT f FROM FhrdDegreeLevelMst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdDegreeLevelMst.findByImportFlag", query = "SELECT f FROM FhrdDegreeLevelMst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdDegreeLevelMst.findBySystemUserid", query = "SELECT f FROM FhrdDegreeLevelMst f WHERE f.systemUserid = :systemUserid")})
public class FhrdDegreeLevelMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DLM_SRG_KEY")
    private Integer dlmSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "DLM_DESCRIPTION")
    private String dlmDescription;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dlmSrgKey")
    private Collection<FhrdDegreemst> fhrdDegreemstCollection;

    public FhrdDegreeLevelMst() {
    }

    public FhrdDegreeLevelMst(Integer dlmSrgKey) {
        this.dlmSrgKey = dlmSrgKey;
    }

    public FhrdDegreeLevelMst(Integer dlmSrgKey, String dlmDescription, Date startDate, Date createdt) {
        this.dlmSrgKey = dlmSrgKey;
        this.dlmDescription = dlmDescription;
        this.startDate = startDate;
        this.createdt = createdt;
    }

    public Integer getDlmSrgKey() {
        return dlmSrgKey;
    }

    public void setDlmSrgKey(Integer dlmSrgKey) {
        this.dlmSrgKey = dlmSrgKey;
    }

    public String getDlmDescription() {
        return dlmDescription;
    }

    public void setDlmDescription(String dlmDescription) {
        this.dlmDescription = dlmDescription;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @XmlTransient
    public Collection<FhrdDegreemst> getFhrdDegreemstCollection() {
        return fhrdDegreemstCollection;
    }

    public void setFhrdDegreemstCollection(Collection<FhrdDegreemst> fhrdDegreemstCollection) {
        this.fhrdDegreemstCollection = fhrdDegreemstCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dlmSrgKey != null ? dlmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdDegreeLevelMst)) {
            return false;
        }
        FhrdDegreeLevelMst other = (FhrdDegreeLevelMst) object;
        if ((this.dlmSrgKey == null && other.dlmSrgKey != null) || (this.dlmSrgKey != null && !this.dlmSrgKey.equals(other.dlmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdDegreeLevelMst[ dlmSrgKey=" + dlmSrgKey + " ]";
    }
    
}
