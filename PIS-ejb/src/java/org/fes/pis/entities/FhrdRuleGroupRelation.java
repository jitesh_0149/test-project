/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_RULE_GROUP_RELATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdRuleGroupRelation.findAll", query = "SELECT f FROM FhrdRuleGroupRelation f"),
    @NamedQuery(name = "FhrdRuleGroupRelation.findByRgrSrgKey", query = "SELECT f FROM FhrdRuleGroupRelation f WHERE f.rgrSrgKey = :rgrSrgKey"),
    @NamedQuery(name = "FhrdRuleGroupRelation.findByCreatedt", query = "SELECT f FROM FhrdRuleGroupRelation f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdRuleGroupRelation.findByUpdatedt", query = "SELECT f FROM FhrdRuleGroupRelation f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdRuleGroupRelation.findBySystemUserid", query = "SELECT f FROM FhrdRuleGroupRelation f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdRuleGroupRelation.findByStartDate", query = "SELECT f FROM FhrdRuleGroupRelation f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdRuleGroupRelation.findByEndDate", query = "SELECT f FROM FhrdRuleGroupRelation f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FhrdRuleGroupRelation.findByRmgmAltSrno", query = "SELECT f FROM FhrdRuleGroupRelation f WHERE f.rmgmAltSrno = :rmgmAltSrno"),
    @NamedQuery(name = "FhrdRuleGroupRelation.findBySystemUseridFlag", query = "SELECT f FROM FhrdRuleGroupRelation f WHERE f.systemUseridFlag = :systemUseridFlag")})
public class FhrdRuleGroupRelation implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "RGR_SRG_KEY")
    private BigDecimal rgrSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Column(name = "RMGM_ALT_SRNO")
    private Integer rmgmAltSrno;
    @Size(max = 1)
    @Column(name = "SYSTEM_USERID_FLAG")
    private String systemUseridFlag;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "RMGM_SRG_KEY", referencedColumnName = "RMGM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdRuleManagGroupMst rmgmSrgKey;
    @JoinColumn(name = "RGC_SRG_KEY", referencedColumnName = "RGC_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdRuleGroupCategory rgcSrgKey;
    @JoinColumn(name = "RAGM_SRG_KEY", referencedColumnName = "RAGM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdRuleApplGroupMst ragmSrgKey;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FhrdRuleGroupRelation() {
    }

    public FhrdRuleGroupRelation(BigDecimal rgrSrgKey) {
        this.rgrSrgKey = rgrSrgKey;
    }

    public FhrdRuleGroupRelation(BigDecimal rgrSrgKey, Date createdt, Date startDate) {
        this.rgrSrgKey = rgrSrgKey;
        this.createdt = createdt;
        this.startDate = startDate;
    }

    public BigDecimal getRgrSrgKey() {
        return rgrSrgKey;
    }

    public void setRgrSrgKey(BigDecimal rgrSrgKey) {
        this.rgrSrgKey = rgrSrgKey;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getRmgmAltSrno() {
        return rmgmAltSrno;
    }

    public void setRmgmAltSrno(Integer rmgmAltSrno) {
        this.rmgmAltSrno = rmgmAltSrno;
    }

    public String getSystemUseridFlag() {
        return systemUseridFlag;
    }

    public void setSystemUseridFlag(String systemUseridFlag) {
        this.systemUseridFlag = systemUseridFlag;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdRuleManagGroupMst getRmgmSrgKey() {
        return rmgmSrgKey;
    }

    public void setRmgmSrgKey(FhrdRuleManagGroupMst rmgmSrgKey) {
        this.rmgmSrgKey = rmgmSrgKey;
    }

    public FhrdRuleGroupCategory getRgcSrgKey() {
        return rgcSrgKey;
    }

    public void setRgcSrgKey(FhrdRuleGroupCategory rgcSrgKey) {
        this.rgcSrgKey = rgcSrgKey;
    }

    public FhrdRuleApplGroupMst getRagmSrgKey() {
        return ragmSrgKey;
    }

    public void setRagmSrgKey(FhrdRuleApplGroupMst ragmSrgKey) {
        this.ragmSrgKey = ragmSrgKey;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rgrSrgKey != null ? rgrSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdRuleGroupRelation)) {
            return false;
        }
        FhrdRuleGroupRelation other = (FhrdRuleGroupRelation) object;
        if ((this.rgrSrgKey == null && other.rgrSrgKey != null) || (this.rgrSrgKey != null && !this.rgrSrgKey.equals(other.rgrSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdRuleGroupRelation[ rgrSrgKey=" + rgrSrgKey + " ]";
    }
    
}
