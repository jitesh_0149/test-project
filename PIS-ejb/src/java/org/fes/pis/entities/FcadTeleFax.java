/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FCAD_TELE_FAX")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FcadTeleFax.findAll", query = "SELECT f FROM FcadTeleFax f"),
    @NamedQuery(name = "FcadTeleFax.findByTelefax", query = "SELECT f FROM FcadTeleFax f WHERE f.telefax = :telefax"),
    @NamedQuery(name = "FcadTeleFax.findByTelefaxno", query = "SELECT f FROM FcadTeleFax f WHERE f.telefaxno = :telefaxno"),
    @NamedQuery(name = "FcadTeleFax.findByLocation", query = "SELECT f FROM FcadTeleFax f WHERE f.location = :location"),
    @NamedQuery(name = "FcadTeleFax.findByType", query = "SELECT f FROM FcadTeleFax f WHERE f.type = :type"),
    @NamedQuery(name = "FcadTeleFax.findByCreatedt", query = "SELECT f FROM FcadTeleFax f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FcadTeleFax.findByUpdatedt", query = "SELECT f FROM FcadTeleFax f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FcadTeleFax.findBySystemUserid", query = "SELECT f FROM FcadTeleFax f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FcadTeleFax.findByExportFlag", query = "SELECT f FROM FcadTeleFax f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FcadTeleFax.findByImportFlag", query = "SELECT f FROM FcadTeleFax f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FcadTeleFax.findByTelFaxUniqueSrno", query = "SELECT f FROM FcadTeleFax f WHERE f.telFaxUniqueSrno = :telFaxUniqueSrno"),
    @NamedQuery(name = "FcadTeleFax.findByCountryCode", query = "SELECT f FROM FcadTeleFax f WHERE f.countryCode = :countryCode")})
public class FcadTeleFax implements Serializable {
    private static final long serialVersionUID = 1L;
    @Size(max = 20)
    @Column(name = "TELEFAX")
    private String telefax;
    @Size(max = 15)
    @Column(name = "TELEFAXNO")
    private String telefaxno;
    @Size(max = 20)
    @Column(name = "LOCATION")
    private String location;
    @Size(max = 1)
    @Column(name = "TYPE")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "TEL_FAX_UNIQUE_SRNO")
    private Integer telFaxUniqueSrno;
    @Size(max = 50)
    @Column(name = "COUNTRY_CODE")
    private String countryCode;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "PD_UNIQUE_SRNO", referencedColumnName = "PD_UNIQUE_SRNO")
    @ManyToOne
    private FcadPersDtl pdUniqueSrno;
    @JoinColumn(name = "AM_UNIQUE_SRNO", referencedColumnName = "AM_UNIQUE_SRNO")
    @ManyToOne
    private FcadAddrMst amUniqueSrno;

    public FcadTeleFax() {
    }

    public FcadTeleFax(Integer telFaxUniqueSrno) {
        this.telFaxUniqueSrno = telFaxUniqueSrno;
    }

    public FcadTeleFax(Integer telFaxUniqueSrno, Date createdt) {
        this.telFaxUniqueSrno = telFaxUniqueSrno;
        this.createdt = createdt;
    }

    public String getTelefax() {
        return telefax;
    }

    public void setTelefax(String telefax) {
        this.telefax = telefax;
    }

    public String getTelefaxno() {
        return telefaxno;
    }

    public void setTelefaxno(String telefaxno) {
        this.telefaxno = telefaxno;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getTelFaxUniqueSrno() {
        return telFaxUniqueSrno;
    }

    public void setTelFaxUniqueSrno(Integer telFaxUniqueSrno) {
        this.telFaxUniqueSrno = telFaxUniqueSrno;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FcadPersDtl getPdUniqueSrno() {
        return pdUniqueSrno;
    }

    public void setPdUniqueSrno(FcadPersDtl pdUniqueSrno) {
        this.pdUniqueSrno = pdUniqueSrno;
    }

    public FcadAddrMst getAmUniqueSrno() {
        return amUniqueSrno;
    }

    public void setAmUniqueSrno(FcadAddrMst amUniqueSrno) {
        this.amUniqueSrno = amUniqueSrno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (telFaxUniqueSrno != null ? telFaxUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FcadTeleFax)) {
            return false;
        }
        FcadTeleFax other = (FcadTeleFax) object;
        if ((this.telFaxUniqueSrno == null && other.telFaxUniqueSrno != null) || (this.telFaxUniqueSrno != null && !this.telFaxUniqueSrno.equals(other.telFaxUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FcadTeleFax[ telFaxUniqueSrno=" + telFaxUniqueSrno + " ]";
    }
    
}
