/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author anu
 */
@Embeddable
public class FhrdDegreedtlPK implements Serializable {
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "SUBJECT_SRNO")
    private BigDecimal subjectSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DGRM_SRG_KEY")
    private BigDecimal dgrmSrgKey;

    public FhrdDegreedtlPK() {
    }

    public FhrdDegreedtlPK(BigDecimal subjectSrno, BigDecimal dgrmSrgKey) {
        this.subjectSrno = subjectSrno;
        this.dgrmSrgKey = dgrmSrgKey;
    }

    public BigDecimal getSubjectSrno() {
        return subjectSrno;
    }

    public void setSubjectSrno(BigDecimal subjectSrno) {
        this.subjectSrno = subjectSrno;
    }

    public BigDecimal getDgrmSrgKey() {
        return dgrmSrgKey;
    }

    public void setDgrmSrgKey(BigDecimal dgrmSrgKey) {
        this.dgrmSrgKey = dgrmSrgKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (subjectSrno != null ? subjectSrno.hashCode() : 0);
        hash += (dgrmSrgKey != null ? dgrmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdDegreedtlPK)) {
            return false;
        }
        FhrdDegreedtlPK other = (FhrdDegreedtlPK) object;
        if ((this.subjectSrno == null && other.subjectSrno != null) || (this.subjectSrno != null && !this.subjectSrno.equals(other.subjectSrno))) {
            return false;
        }
        if ((this.dgrmSrgKey == null && other.dgrmSrgKey != null) || (this.dgrmSrgKey != null && !this.dgrmSrgKey.equals(other.dgrmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdDegreedtlPK[ subjectSrno=" + subjectSrno + ", dgrmSrgKey=" + dgrmSrgKey + " ]";
    }
    
}
