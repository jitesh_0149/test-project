/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_TOUR_SCHEDULE_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasTourScheduleDtl.findAll", query = "SELECT f FROM FtasTourScheduleDtl f"),
    @NamedQuery(name = "FtasTourScheduleDtl.findBySubSrno", query = "SELECT f FROM FtasTourScheduleDtl f WHERE f.subSrno = :subSrno"),
    @NamedQuery(name = "FtasTourScheduleDtl.findByFromDatetime", query = "SELECT f FROM FtasTourScheduleDtl f WHERE f.fromDatetime = :fromDatetime"),
    @NamedQuery(name = "FtasTourScheduleDtl.findByToDatetime", query = "SELECT f FROM FtasTourScheduleDtl f WHERE f.toDatetime = :toDatetime"),
    @NamedQuery(name = "FtasTourScheduleDtl.findByTimezone", query = "SELECT f FROM FtasTourScheduleDtl f WHERE f.timezone = :timezone"),
    @NamedQuery(name = "FtasTourScheduleDtl.findByFromplace", query = "SELECT f FROM FtasTourScheduleDtl f WHERE f.fromplace = :fromplace"),
    @NamedQuery(name = "FtasTourScheduleDtl.findByPurpose", query = "SELECT f FROM FtasTourScheduleDtl f WHERE f.purpose = :purpose"),
    @NamedQuery(name = "FtasTourScheduleDtl.findByCreatedt", query = "SELECT f FROM FtasTourScheduleDtl f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasTourScheduleDtl.findByUpdatedt", query = "SELECT f FROM FtasTourScheduleDtl f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasTourScheduleDtl.findByTosdSrgKey", query = "SELECT f FROM FtasTourScheduleDtl f WHERE f.tosdSrgKey = :tosdSrgKey"),
    @NamedQuery(name = "FtasTourScheduleDtl.findByExportFlag", query = "SELECT f FROM FtasTourScheduleDtl f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FtasTourScheduleDtl.findByImportFlag", query = "SELECT f FROM FtasTourScheduleDtl f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FtasTourScheduleDtl.findBySystemUserid", query = "SELECT f FROM FtasTourScheduleDtl f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FtasTourScheduleDtl.findByTranYear", query = "SELECT f FROM FtasTourScheduleDtl f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FtasTourScheduleDtl.findByToplace", query = "SELECT f FROM FtasTourScheduleDtl f WHERE f.toplace = :toplace")})
public class FtasTourScheduleDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SUB_SRNO")
    private int subSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FROM_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDatetime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TO_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date toDatetime;
    @Size(max = 10)
    @Column(name = "TIMEZONE")
    private String timezone;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "FROMPLACE")
    private String fromplace;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "PURPOSE")
    private String purpose;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOSD_SRG_KEY")
    private BigDecimal tosdSrgKey;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "TOPLACE")
    private String toplace;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "ABS_SRG_KEY", referencedColumnName = "ABS_SRG_KEY")
    @ManyToOne(optional = false)
    private FtasAbsentees absSrgKey;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FtasTourScheduleDtl() {
    }

    public FtasTourScheduleDtl(BigDecimal tosdSrgKey) {
        this.tosdSrgKey = tosdSrgKey;
    }

    public FtasTourScheduleDtl(BigDecimal tosdSrgKey, int subSrno, Date fromDatetime, Date toDatetime, String fromplace, String purpose, Date createdt, short tranYear, String toplace) {
        this.tosdSrgKey = tosdSrgKey;
        this.subSrno = subSrno;
        this.fromDatetime = fromDatetime;
        this.toDatetime = toDatetime;
        this.fromplace = fromplace;
        this.purpose = purpose;
        this.createdt = createdt;
        this.tranYear = tranYear;
        this.toplace = toplace;
    }

    public int getSubSrno() {
        return subSrno;
    }

    public void setSubSrno(int subSrno) {
        this.subSrno = subSrno;
    }

    public Date getFromDatetime() {
        return fromDatetime;
    }

    public void setFromDatetime(Date fromDatetime) {
        this.fromDatetime = fromDatetime;
    }

    public Date getToDatetime() {
        return toDatetime;
    }

    public void setToDatetime(Date toDatetime) {
        this.toDatetime = toDatetime;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getFromplace() {
        return fromplace;
    }

    public void setFromplace(String fromplace) {
        this.fromplace = fromplace;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public BigDecimal getTosdSrgKey() {
        return tosdSrgKey;
    }

    public void setTosdSrgKey(BigDecimal tosdSrgKey) {
        this.tosdSrgKey = tosdSrgKey;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public String getToplace() {
        return toplace;
    }

    public void setToplace(String toplace) {
        this.toplace = toplace;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasAbsentees getAbsSrgKey() {
        return absSrgKey;
    }

    public void setAbsSrgKey(FtasAbsentees absSrgKey) {
        this.absSrgKey = absSrgKey;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tosdSrgKey != null ? tosdSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasTourScheduleDtl)) {
            return false;
        }
        FtasTourScheduleDtl other = (FtasTourScheduleDtl) object;
        if ((this.tosdSrgKey == null && other.tosdSrgKey != null) || (this.tosdSrgKey != null && !this.tosdSrgKey.equals(other.tosdSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasTourScheduleDtl[ tosdSrgKey=" + tosdSrgKey + " ]";
    }
    
}
