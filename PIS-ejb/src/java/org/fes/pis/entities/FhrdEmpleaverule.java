/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_EMPLEAVERULE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdEmpleaverule.findAll", query = "SELECT f FROM FhrdEmpleaverule f"),
    @NamedQuery(name = "FhrdEmpleaverule.findBySrno", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.srno = :srno"),
    @NamedQuery(name = "FhrdEmpleaverule.findByLeavesrno", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.leavesrno = :leavesrno"),
    @NamedQuery(name = "FhrdEmpleaverule.findByLeavecd", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.leavecd = :leavecd"),
    @NamedQuery(name = "FhrdEmpleaverule.findByStartDate", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdEmpleaverule.findByOpeningDate", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.openingDate = :openingDate"),
    @NamedQuery(name = "FhrdEmpleaverule.findByClosingDate", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.closingDate = :closingDate"),
    @NamedQuery(name = "FhrdEmpleaverule.findByBalanceRenewalOpeningDate", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.balanceRenewalOpeningDate = :balanceRenewalOpeningDate"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEndDate", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FhrdEmpleaverule.findByWefDate", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.wefDate = :wefDate"),
    @NamedQuery(name = "FhrdEmpleaverule.findByBalanceEarnType", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.balanceEarnType = :balanceEarnType"),
    @NamedQuery(name = "FhrdEmpleaverule.findByNoOfLeave", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.noOfLeave = :noOfLeave"),
    @NamedQuery(name = "FhrdEmpleaverule.findByProportionateBalance", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.proportionateBalance = :proportionateBalance"),
    @NamedQuery(name = "FhrdEmpleaverule.findByParentLeavesrno", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.parentLeavesrno = :parentLeavesrno"),
    @NamedQuery(name = "FhrdEmpleaverule.findByRulesrno", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.rulesrno = :rulesrno"),
    @NamedQuery(name = "FhrdEmpleaverule.findByParentRulesrno", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.parentRulesrno = :parentRulesrno"),
    @NamedQuery(name = "FhrdEmpleaverule.findByCarryForward", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.carryForward = :carryForward"),
    @NamedQuery(name = "FhrdEmpleaverule.findByCarryForwardMax", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.carryForwardMax = :carryForwardMax"),
    @NamedQuery(name = "FhrdEmpleaverule.findByCarryForwardRemaining", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.carryForwardRemaining = :carryForwardRemaining"),
    @NamedQuery(name = "FhrdEmpleaverule.findByDaysPerLeaveBalComparison", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.daysPerLeaveBalComparison = :daysPerLeaveBalComparison"),
    @NamedQuery(name = "FhrdEmpleaverule.findByLeaveOpenBal", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.leaveOpenBal = :leaveOpenBal"),
    @NamedQuery(name = "FhrdEmpleaverule.findByBalCheckedInAppliFlag", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.balCheckedInAppliFlag = :balCheckedInAppliFlag"),
    @NamedQuery(name = "FhrdEmpleaverule.findByProposedOpeningbal", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.proposedOpeningbal = :proposedOpeningbal"),
    @NamedQuery(name = "FhrdEmpleaverule.findByParentCarryForward", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.parentCarryForward = :parentCarryForward"),
    @NamedQuery(name = "FhrdEmpleaverule.findByParentCarryForwardMax", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.parentCarryForwardMax = :parentCarryForwardMax"),
    @NamedQuery(name = "FhrdEmpleaverule.findByParentCarryforwardRemaining", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.parentCarryforwardRemaining = :parentCarryforwardRemaining"),
    @NamedQuery(name = "FhrdEmpleaverule.findByParentLeaveRatio", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.parentLeaveRatio = :parentLeaveRatio"),
    @NamedQuery(name = "FhrdEmpleaverule.findByDeductDays", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.deductDays = :deductDays"),
    @NamedQuery(name = "FhrdEmpleaverule.findByDeductLeaves", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.deductLeaves = :deductLeaves"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEarnLeavePeriod", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.earnLeavePeriod = :earnLeavePeriod"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEarnLeavePeriodUnit", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.earnLeavePeriodUnit = :earnLeavePeriodUnit"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEarnLeavePerPeriod", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.earnLeavePerPeriod = :earnLeavePerPeriod"),
    @NamedQuery(name = "FhrdEmpleaverule.findByAccumulation", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.accumulation = :accumulation"),
    @NamedQuery(name = "FhrdEmpleaverule.findByGenderFlag", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.genderFlag = :genderFlag"),
    @NamedQuery(name = "FhrdEmpleaverule.findByMaxSpecialApprove", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.maxSpecialApprove = :maxSpecialApprove"),
    @NamedQuery(name = "FhrdEmpleaverule.findByFreqPeriod", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.freqPeriod = :freqPeriod"),
    @NamedQuery(name = "FhrdEmpleaverule.findByFreqPeriodUnit", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.freqPeriodUnit = :freqPeriodUnit"),
    @NamedQuery(name = "FhrdEmpleaverule.findByFreqPerPeriod", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.freqPerPeriod = :freqPerPeriod"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEncashFormula", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.encashFormula = :encashFormula"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEncashment", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.encashment = :encashment"),
    @NamedQuery(name = "FhrdEmpleaverule.findByClubWithOther", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.clubWithOther = :clubWithOther"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEarnConfigFlag", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.earnConfigFlag = :earnConfigFlag"),
    @NamedQuery(name = "FhrdEmpleaverule.findByHolidayEmbedded", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.holidayEmbedded = :holidayEmbedded"),
    @NamedQuery(name = "FhrdEmpleaverule.findByCommuted", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.commuted = :commuted"),
    @NamedQuery(name = "FhrdEmpleaverule.findByMaxTaken", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.maxTaken = :maxTaken"),
    @NamedQuery(name = "FhrdEmpleaverule.findByMinTaken", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.minTaken = :minTaken"),
    @NamedQuery(name = "FhrdEmpleaverule.findByLeaveFormula", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.leaveFormula = :leaveFormula"),
    @NamedQuery(name = "FhrdEmpleaverule.findByLeaveBalDedPerLeave", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.leaveBalDedPerLeave = :leaveBalDedPerLeave"),
    @NamedQuery(name = "FhrdEmpleaverule.findByPartFulfillSubsysFlag", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.partFulfillSubsysFlag = :partFulfillSubsysFlag"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEncashMinLeave", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.encashMinLeave = :encashMinLeave"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEncashMaxLeave", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.encashMaxLeave = :encashMaxLeave"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEncashExtraMinLeave", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.encashExtraMinLeave = :encashExtraMinLeave"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEncashApprovalMinLeave", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.encashApprovalMinLeave = :encashApprovalMinLeave"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEncashFreq", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.encashFreq = :encashFreq"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEncashFreqPeriod", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.encashFreqPeriod = :encashFreqPeriod"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEncashFreqPeriodUnit", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.encashFreqPeriodUnit = :encashFreqPeriodUnit"),
    @NamedQuery(name = "FhrdEmpleaverule.findByCommuteMinLeave", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.commuteMinLeave = :commuteMinLeave"),
    @NamedQuery(name = "FhrdEmpleaverule.findByCommuteDeductDays", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.commuteDeductDays = :commuteDeductDays"),
    @NamedQuery(name = "FhrdEmpleaverule.findByCommuteDeductLeaves", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.commuteDeductLeaves = :commuteDeductLeaves"),
    @NamedQuery(name = "FhrdEmpleaverule.findByLeaveBalDedPerComutleave", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.leaveBalDedPerComutleave = :leaveBalDedPerComutleave"),
    @NamedQuery(name = "FhrdEmpleaverule.findByRemarks", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.remarks = :remarks"),
    @NamedQuery(name = "FhrdEmpleaverule.findByCreatedt", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdEmpleaverule.findByUpdatedt", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdEmpleaverule.findBySystemUserid", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdEmpleaverule.findByExportFlag", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdEmpleaverule.findByImportFlag", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdEmpleaverule.findByContrdueCarryForwrd", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.contrdueCarryForwrd = :contrdueCarryForwrd"),
    @NamedQuery(name = "FhrdEmpleaverule.findByContrdueCarryForwrdmax", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.contrdueCarryForwrdmax = :contrdueCarryForwrdmax"),
    @NamedQuery(name = "FhrdEmpleaverule.findByContrdueCarryforwrdRemaining", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.contrdueCarryforwrdRemaining = :contrdueCarryforwrdRemaining"),
    @NamedQuery(name = "FhrdEmpleaverule.findByHalfdayApplicable", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.halfdayApplicable = :halfdayApplicable"),
    @NamedQuery(name = "FhrdEmpleaverule.findByElrSrgKey", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.elrSrgKey = :elrSrgKey"),
    @NamedQuery(name = "FhrdEmpleaverule.findByStartMonth", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.startMonth = :startMonth"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEndMonth", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.endMonth = :endMonth"),
    @NamedQuery(name = "FhrdEmpleaverule.findByLmAltSrno", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.lmAltSrno = :lmAltSrno"),
    @NamedQuery(name = "FhrdEmpleaverule.findByParentElrSrgKey", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.parentElrSrgKey = :parentElrSrgKey"),
    @NamedQuery(name = "FhrdEmpleaverule.findByTranYear", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FhrdEmpleaverule.findByGroupLeaveDesc", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.groupLeaveDesc = :groupLeaveDesc"),
    @NamedQuery(name = "FhrdEmpleaverule.findByBalanceRenewalClosingDate", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.balanceRenewalClosingDate = :balanceRenewalClosingDate"),
    @NamedQuery(name = "FhrdEmpleaverule.findByFixMonth", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.fixMonth = :fixMonth"),
    @NamedQuery(name = "FhrdEmpleaverule.findByFreqConsFrom", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.freqConsFrom = :freqConsFrom"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEligibilityApplicable", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.eligibilityApplicable = :eligibilityApplicable"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEligibilityConsidered", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.eligibilityConsidered = :eligibilityConsidered"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEligibilityPeriod", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.eligibilityPeriod = :eligibilityPeriod"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEligibilityUnit", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.eligibilityUnit = :eligibilityUnit"),
    @NamedQuery(name = "FhrdEmpleaverule.findByPrevContrElrSrgKey", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.prevContrElrSrgKey = :prevContrElrSrgKey"),
    @NamedQuery(name = "FhrdEmpleaverule.findByVirtualLeave", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.virtualLeave = :virtualLeave"),
    @NamedQuery(name = "FhrdEmpleaverule.findBySystemUseridFlag", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.systemUseridFlag = :systemUseridFlag"),
    @NamedQuery(name = "FhrdEmpleaverule.findByLeaveLapsInClubFlag", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.leaveLapsInClubFlag = :leaveLapsInClubFlag"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEarnIncludeFlag", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.earnIncludeFlag = :earnIncludeFlag"),
    @NamedQuery(name = "FhrdEmpleaverule.findByMultipleRuleFlag", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.multipleRuleFlag = :multipleRuleFlag"),
    @NamedQuery(name = "FhrdEmpleaverule.findByExhaustOtherLeaveFlag", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.exhaustOtherLeaveFlag = :exhaustOtherLeaveFlag"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEncashAutoFlag", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.encashAutoFlag = :encashAutoFlag"),
    @NamedQuery(name = "FhrdEmpleaverule.findByEncashAutoMonth", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.encashAutoMonth = :encashAutoMonth"),
    @NamedQuery(name = "FhrdEmpleaverule.findByReqBalOnRelieve", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.reqBalOnRelieve = :reqBalOnRelieve"),
    @NamedQuery(name = "FhrdEmpleaverule.findByLastRenewalOpeningDate", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.lastRenewalOpeningDate = :lastRenewalOpeningDate"),
    @NamedQuery(name = "FhrdEmpleaverule.findByContStartDate", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.contStartDate = :contStartDate"),
    @NamedQuery(name = "FhrdEmpleaverule.findByContEndDate", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.contEndDate = :contEndDate"),
    @NamedQuery(name = "FhrdEmpleaverule.findByProportionateAtJoinOnly", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.proportionateAtJoinOnly = :proportionateAtJoinOnly"),
    @NamedQuery(name = "FhrdEmpleaverule.findByContAllowedFlag", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.contAllowedFlag = :contAllowedFlag"),
    @NamedQuery(name = "FhrdEmpleaverule.findByUserEntryFlag", query = "SELECT f FROM FhrdEmpleaverule f WHERE f.userEntryFlag = :userEntryFlag")})
public class FhrdEmpleaverule implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SRNO")
    private int srno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LEAVESRNO")
    private int leavesrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "LEAVECD")
    private String leavecd;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OPENING_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date openingDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CLOSING_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date closingDate;
    @Column(name = "BALANCE_RENEWAL_OPENING_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date balanceRenewalOpeningDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WEF_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date wefDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "BALANCE_EARN_TYPE")
    private String balanceEarnType;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "NO_OF_LEAVE")
    private BigDecimal noOfLeave;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PROPORTIONATE_BALANCE")
    private String proportionateBalance;
    @Column(name = "PARENT_LEAVESRNO")
    private Integer parentLeavesrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RULESRNO")
    private int rulesrno;
    @Column(name = "PARENT_RULESRNO")
    private Integer parentRulesrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "CARRY_FORWARD")
    private String carryForward;
    @Column(name = "CARRY_FORWARD_MAX")
    private BigDecimal carryForwardMax;
    @Size(max = 1)
    @Column(name = "CARRY_FORWARD_REMAINING")
    private String carryForwardRemaining;
    @Column(name = "DAYS_PER_LEAVE_BAL_COMPARISON")
    private BigDecimal daysPerLeaveBalComparison;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LEAVE_OPEN_BAL")
    private BigDecimal leaveOpenBal;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "BAL_CHECKED_IN_APPLI_FLAG")
    private String balCheckedInAppliFlag;
    @Size(max = 1)
    @Column(name = "PROPOSED_OPENINGBAL")
    private String proposedOpeningbal;
    @Size(max = 1)
    @Column(name = "PARENT_CARRY_FORWARD")
    private String parentCarryForward;
    @Column(name = "PARENT_CARRY_FORWARD_MAX")
    private BigDecimal parentCarryForwardMax;
    @Size(max = 1)
    @Column(name = "PARENT_CARRYFORWARD_REMAINING")
    private String parentCarryforwardRemaining;
    @Column(name = "PARENT_LEAVE_RATIO")
    private BigDecimal parentLeaveRatio;
    @Column(name = "DEDUCT_DAYS")
    private BigDecimal deductDays;
    @Column(name = "DEDUCT_LEAVES")
    private BigDecimal deductLeaves;
    @Column(name = "EARN_LEAVE_PERIOD")
    private BigDecimal earnLeavePeriod;
    @Size(max = 1)
    @Column(name = "EARN_LEAVE_PERIOD_UNIT")
    private String earnLeavePeriodUnit;
    @Column(name = "EARN_LEAVE_PER_PERIOD")
    private BigDecimal earnLeavePerPeriod;
    @Column(name = "ACCUMULATION")
    private BigDecimal accumulation;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "GENDER_FLAG")
    private String genderFlag;
    @Column(name = "MAX_SPECIAL_APPROVE")
    private BigDecimal maxSpecialApprove;
    @Column(name = "FREQ_PERIOD")
    private Integer freqPeriod;
    @Size(max = 1)
    @Column(name = "FREQ_PERIOD_UNIT")
    private String freqPeriodUnit;
    @Column(name = "FREQ_PER_PERIOD")
    private Integer freqPerPeriod;
    @Size(max = 50)
    @Column(name = "ENCASH_FORMULA")
    private String encashFormula;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "ENCASHMENT")
    private String encashment;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "CLUB_WITH_OTHER")
    private String clubWithOther;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EARN_CONFIG_FLAG")
    private String earnConfigFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "HOLIDAY_EMBEDDED")
    private String holidayEmbedded;
    @Size(max = 1)
    @Column(name = "COMMUTED")
    private String commuted;
    @Column(name = "MAX_TAKEN")
    private BigDecimal maxTaken;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MIN_TAKEN")
    private BigDecimal minTaken;
    @Size(max = 120)
    @Column(name = "LEAVE_FORMULA")
    private String leaveFormula;
    @Column(name = "LEAVE_BAL_DED_PER_LEAVE")
    private BigDecimal leaveBalDedPerLeave;
    @Size(max = 1)
    @Column(name = "PART_FULFILL_SUBSYS_FLAG")
    private String partFulfillSubsysFlag;
    @Column(name = "ENCASH_MIN_LEAVE")
    private BigDecimal encashMinLeave;
    @Column(name = "ENCASH_MAX_LEAVE")
    private BigDecimal encashMaxLeave;
    @Column(name = "ENCASH_EXTRA_MIN_LEAVE")
    private BigDecimal encashExtraMinLeave;
    @Column(name = "ENCASH_APPROVAL_MIN_LEAVE")
    private BigDecimal encashApprovalMinLeave;
    @Column(name = "ENCASH_FREQ")
    private BigDecimal encashFreq;
    @Column(name = "ENCASH_FREQ_PERIOD")
    private BigDecimal encashFreqPeriod;
    @Size(max = 1)
    @Column(name = "ENCASH_FREQ_PERIOD_UNIT")
    private String encashFreqPeriodUnit;
    @Column(name = "COMMUTE_MIN_LEAVE")
    private BigDecimal commuteMinLeave;
    @Column(name = "COMMUTE_DEDUCT_DAYS")
    private BigDecimal commuteDeductDays;
    @Column(name = "COMMUTE_DEDUCT_LEAVES")
    private BigDecimal commuteDeductLeaves;
    @Column(name = "LEAVE_BAL_DED_PER_COMUTLEAVE")
    private BigDecimal leaveBalDedPerComutleave;
    @Size(max = 255)
    @Column(name = "REMARKS")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "CONTRDUE_CARRY_FORWRD")
    private String contrdueCarryForwrd;
    @Column(name = "CONTRDUE_CARRY_FORWRDMAX")
    private BigDecimal contrdueCarryForwrdmax;
    @Size(max = 1)
    @Column(name = "CONTRDUE_CARRYFORWRD_REMAINING")
    private String contrdueCarryforwrdRemaining;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "HALFDAY_APPLICABLE")
    private String halfdayApplicable;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ELR_SRG_KEY")
    private BigDecimal elrSrgKey;
    @Size(max = 3)
    @Column(name = "START_MONTH")
    private String startMonth;
    @Size(max = 3)
    @Column(name = "END_MONTH")
    private String endMonth;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LM_ALT_SRNO")
    private int lmAltSrno;
    @Column(name = "PARENT_ELR_SRG_KEY")
    private BigDecimal parentElrSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "GROUP_LEAVE_DESC")
    private String groupLeaveDesc;
    @Column(name = "BALANCE_RENEWAL_CLOSING_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date balanceRenewalClosingDate;
    @Size(max = 1)
    @Column(name = "FIX_MONTH")
    private String fixMonth;
    @Size(max = 1)
    @Column(name = "FREQ_CONS_FROM")
    private String freqConsFrom;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "ELIGIBILITY_APPLICABLE")
    private String eligibilityApplicable;
    @Size(max = 1)
    @Column(name = "ELIGIBILITY_CONSIDERED")
    private String eligibilityConsidered;
    @Column(name = "ELIGIBILITY_PERIOD")
    private Short eligibilityPeriod;
    @Size(max = 1)
    @Column(name = "ELIGIBILITY_UNIT")
    private String eligibilityUnit;
    @Column(name = "PREV_CONTR_ELR_SRG_KEY")
    private BigDecimal prevContrElrSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "VIRTUAL_LEAVE")
    private String virtualLeave;
    @Size(max = 1)
    @Column(name = "SYSTEM_USERID_FLAG")
    private String systemUseridFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "LEAVE_LAPS_IN_CLUB_FLAG")
    private String leaveLapsInClubFlag;
    @Size(max = 1)
    @Column(name = "EARN_INCLUDE_FLAG")
    private String earnIncludeFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "MULTIPLE_RULE_FLAG")
    private String multipleRuleFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EXHAUST_OTHER_LEAVE_FLAG")
    private String exhaustOtherLeaveFlag;
    @Size(max = 1)
    @Column(name = "ENCASH_AUTO_FLAG")
    private String encashAutoFlag;
    @Size(max = 3)
    @Column(name = "ENCASH_AUTO_MONTH")
    private String encashAutoMonth;
    @Column(name = "REQ_BAL_ON_RELIEVE")
    private Integer reqBalOnRelieve;
    @Column(name = "LAST_RENEWAL_OPENING_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastRenewalOpeningDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONT_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date contStartDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONT_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date contEndDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PROPORTIONATE_AT_JOIN_ONLY")
    private String proportionateAtJoinOnly;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "CONT_ALLOWED_FLAG")
    private String contAllowedFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "USER_ENTRY_FLAG")
    private String userEntryFlag;
    @OneToMany(mappedBy = "elrSrgKey")
    private Collection<FhrdPaytransleaveEarn> fhrdPaytransleaveEarnCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "elrSrgKey")
    private Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection;
    @OneToMany(mappedBy = "childElrSrgKey")
    private Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "actualElrSrgKey")
    private Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection2;
    @OneToMany(mappedBy = "parentElrSrgKey")
    private Collection<FhrdEmpleavebal> fhrdEmpleavebalCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "elrSrgKey")
    private Collection<FhrdEmpleavebal> fhrdEmpleavebalCollection1;
    @JoinColumn(name = "REF_OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst refOumUnitSrno;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne(optional = false)
    private FtasLeavemst lmSrgKey;
    @JoinColumn(name = "RMGM_SRG_KEY", referencedColumnName = "RMGM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdRuleManagGroupMst rmgmSrgKey;
    @JoinColumn(name = "CONTRDUE_CARRY_LR_SRG_KEY", referencedColumnName = "LR_SRG_KEY")
    @ManyToOne
    private FhrdLeaverule contrdueCarryLrSrgKey;
    @JoinColumn(name = "LR_SRG_KEY", referencedColumnName = "LR_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdLeaverule lrSrgKey;
    @JoinColumn(name = "PARENT_LR_SRG_KEY", referencedColumnName = "LR_SRG_KEY")
    @ManyToOne
    private FhrdLeaverule parentLrSrgKey;
    @JoinColumn(name = "ECGM_SRG_KEY", referencedColumnName = "ECGM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdEmpContractGroupMst ecgmSrgKey;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst userId;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FhrdEmpleaverule() {
    }

    public FhrdEmpleaverule(BigDecimal elrSrgKey) {
        this.elrSrgKey = elrSrgKey;
    }

    public FhrdEmpleaverule(BigDecimal elrSrgKey, int srno, int leavesrno, String leavecd, Date startDate, Date openingDate, Date closingDate, Date wefDate, String balanceEarnType, String proportionateBalance, int rulesrno, String carryForward, BigDecimal leaveOpenBal, String balCheckedInAppliFlag, String genderFlag, String encashment, String clubWithOther, String earnConfigFlag, String holidayEmbedded, BigDecimal minTaken, Date createdt, String contrdueCarryForwrd, String halfdayApplicable, int lmAltSrno, short tranYear, String groupLeaveDesc, String eligibilityApplicable, String virtualLeave, String leaveLapsInClubFlag, String multipleRuleFlag, String exhaustOtherLeaveFlag, Date contStartDate, Date contEndDate, String proportionateAtJoinOnly, String contAllowedFlag, String userEntryFlag) {
        this.elrSrgKey = elrSrgKey;
        this.srno = srno;
        this.leavesrno = leavesrno;
        this.leavecd = leavecd;
        this.startDate = startDate;
        this.openingDate = openingDate;
        this.closingDate = closingDate;
        this.wefDate = wefDate;
        this.balanceEarnType = balanceEarnType;
        this.proportionateBalance = proportionateBalance;
        this.rulesrno = rulesrno;
        this.carryForward = carryForward;
        this.leaveOpenBal = leaveOpenBal;
        this.balCheckedInAppliFlag = balCheckedInAppliFlag;
        this.genderFlag = genderFlag;
        this.encashment = encashment;
        this.clubWithOther = clubWithOther;
        this.earnConfigFlag = earnConfigFlag;
        this.holidayEmbedded = holidayEmbedded;
        this.minTaken = minTaken;
        this.createdt = createdt;
        this.contrdueCarryForwrd = contrdueCarryForwrd;
        this.halfdayApplicable = halfdayApplicable;
        this.lmAltSrno = lmAltSrno;
        this.tranYear = tranYear;
        this.groupLeaveDesc = groupLeaveDesc;
        this.eligibilityApplicable = eligibilityApplicable;
        this.virtualLeave = virtualLeave;
        this.leaveLapsInClubFlag = leaveLapsInClubFlag;
        this.multipleRuleFlag = multipleRuleFlag;
        this.exhaustOtherLeaveFlag = exhaustOtherLeaveFlag;
        this.contStartDate = contStartDate;
        this.contEndDate = contEndDate;
        this.proportionateAtJoinOnly = proportionateAtJoinOnly;
        this.contAllowedFlag = contAllowedFlag;
        this.userEntryFlag = userEntryFlag;
    }

    public int getSrno() {
        return srno;
    }

    public void setSrno(int srno) {
        this.srno = srno;
    }

    public int getLeavesrno() {
        return leavesrno;
    }

    public void setLeavesrno(int leavesrno) {
        this.leavesrno = leavesrno;
    }

    public String getLeavecd() {
        return leavecd;
    }

    public void setLeavecd(String leavecd) {
        this.leavecd = leavecd;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(Date openingDate) {
        this.openingDate = openingDate;
    }

    public Date getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(Date closingDate) {
        this.closingDate = closingDate;
    }

    public Date getBalanceRenewalOpeningDate() {
        return balanceRenewalOpeningDate;
    }

    public void setBalanceRenewalOpeningDate(Date balanceRenewalOpeningDate) {
        this.balanceRenewalOpeningDate = balanceRenewalOpeningDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getWefDate() {
        return wefDate;
    }

    public void setWefDate(Date wefDate) {
        this.wefDate = wefDate;
    }

    public String getBalanceEarnType() {
        return balanceEarnType;
    }

    public void setBalanceEarnType(String balanceEarnType) {
        this.balanceEarnType = balanceEarnType;
    }

    public BigDecimal getNoOfLeave() {
        return noOfLeave;
    }

    public void setNoOfLeave(BigDecimal noOfLeave) {
        this.noOfLeave = noOfLeave;
    }

    public String getProportionateBalance() {
        return proportionateBalance;
    }

    public void setProportionateBalance(String proportionateBalance) {
        this.proportionateBalance = proportionateBalance;
    }

    public Integer getParentLeavesrno() {
        return parentLeavesrno;
    }

    public void setParentLeavesrno(Integer parentLeavesrno) {
        this.parentLeavesrno = parentLeavesrno;
    }

    public int getRulesrno() {
        return rulesrno;
    }

    public void setRulesrno(int rulesrno) {
        this.rulesrno = rulesrno;
    }

    public Integer getParentRulesrno() {
        return parentRulesrno;
    }

    public void setParentRulesrno(Integer parentRulesrno) {
        this.parentRulesrno = parentRulesrno;
    }

    public String getCarryForward() {
        return carryForward;
    }

    public void setCarryForward(String carryForward) {
        this.carryForward = carryForward;
    }

    public BigDecimal getCarryForwardMax() {
        return carryForwardMax;
    }

    public void setCarryForwardMax(BigDecimal carryForwardMax) {
        this.carryForwardMax = carryForwardMax;
    }

    public String getCarryForwardRemaining() {
        return carryForwardRemaining;
    }

    public void setCarryForwardRemaining(String carryForwardRemaining) {
        this.carryForwardRemaining = carryForwardRemaining;
    }

    public BigDecimal getDaysPerLeaveBalComparison() {
        return daysPerLeaveBalComparison;
    }

    public void setDaysPerLeaveBalComparison(BigDecimal daysPerLeaveBalComparison) {
        this.daysPerLeaveBalComparison = daysPerLeaveBalComparison;
    }

    public BigDecimal getLeaveOpenBal() {
        return leaveOpenBal;
    }

    public void setLeaveOpenBal(BigDecimal leaveOpenBal) {
        this.leaveOpenBal = leaveOpenBal;
    }

    public String getBalCheckedInAppliFlag() {
        return balCheckedInAppliFlag;
    }

    public void setBalCheckedInAppliFlag(String balCheckedInAppliFlag) {
        this.balCheckedInAppliFlag = balCheckedInAppliFlag;
    }

    public String getProposedOpeningbal() {
        return proposedOpeningbal;
    }

    public void setProposedOpeningbal(String proposedOpeningbal) {
        this.proposedOpeningbal = proposedOpeningbal;
    }

    public String getParentCarryForward() {
        return parentCarryForward;
    }

    public void setParentCarryForward(String parentCarryForward) {
        this.parentCarryForward = parentCarryForward;
    }

    public BigDecimal getParentCarryForwardMax() {
        return parentCarryForwardMax;
    }

    public void setParentCarryForwardMax(BigDecimal parentCarryForwardMax) {
        this.parentCarryForwardMax = parentCarryForwardMax;
    }

    public String getParentCarryforwardRemaining() {
        return parentCarryforwardRemaining;
    }

    public void setParentCarryforwardRemaining(String parentCarryforwardRemaining) {
        this.parentCarryforwardRemaining = parentCarryforwardRemaining;
    }

    public BigDecimal getParentLeaveRatio() {
        return parentLeaveRatio;
    }

    public void setParentLeaveRatio(BigDecimal parentLeaveRatio) {
        this.parentLeaveRatio = parentLeaveRatio;
    }

    public BigDecimal getDeductDays() {
        return deductDays;
    }

    public void setDeductDays(BigDecimal deductDays) {
        this.deductDays = deductDays;
    }

    public BigDecimal getDeductLeaves() {
        return deductLeaves;
    }

    public void setDeductLeaves(BigDecimal deductLeaves) {
        this.deductLeaves = deductLeaves;
    }

    public BigDecimal getEarnLeavePeriod() {
        return earnLeavePeriod;
    }

    public void setEarnLeavePeriod(BigDecimal earnLeavePeriod) {
        this.earnLeavePeriod = earnLeavePeriod;
    }

    public String getEarnLeavePeriodUnit() {
        return earnLeavePeriodUnit;
    }

    public void setEarnLeavePeriodUnit(String earnLeavePeriodUnit) {
        this.earnLeavePeriodUnit = earnLeavePeriodUnit;
    }

    public BigDecimal getEarnLeavePerPeriod() {
        return earnLeavePerPeriod;
    }

    public void setEarnLeavePerPeriod(BigDecimal earnLeavePerPeriod) {
        this.earnLeavePerPeriod = earnLeavePerPeriod;
    }

    public BigDecimal getAccumulation() {
        return accumulation;
    }

    public void setAccumulation(BigDecimal accumulation) {
        this.accumulation = accumulation;
    }

    public String getGenderFlag() {
        return genderFlag;
    }

    public void setGenderFlag(String genderFlag) {
        this.genderFlag = genderFlag;
    }

    public BigDecimal getMaxSpecialApprove() {
        return maxSpecialApprove;
    }

    public void setMaxSpecialApprove(BigDecimal maxSpecialApprove) {
        this.maxSpecialApprove = maxSpecialApprove;
    }

    public Integer getFreqPeriod() {
        return freqPeriod;
    }

    public void setFreqPeriod(Integer freqPeriod) {
        this.freqPeriod = freqPeriod;
    }

    public String getFreqPeriodUnit() {
        return freqPeriodUnit;
    }

    public void setFreqPeriodUnit(String freqPeriodUnit) {
        this.freqPeriodUnit = freqPeriodUnit;
    }

    public Integer getFreqPerPeriod() {
        return freqPerPeriod;
    }

    public void setFreqPerPeriod(Integer freqPerPeriod) {
        this.freqPerPeriod = freqPerPeriod;
    }

    public String getEncashFormula() {
        return encashFormula;
    }

    public void setEncashFormula(String encashFormula) {
        this.encashFormula = encashFormula;
    }

    public String getEncashment() {
        return encashment;
    }

    public void setEncashment(String encashment) {
        this.encashment = encashment;
    }

    public String getClubWithOther() {
        return clubWithOther;
    }

    public void setClubWithOther(String clubWithOther) {
        this.clubWithOther = clubWithOther;
    }

    public String getEarnConfigFlag() {
        return earnConfigFlag;
    }

    public void setEarnConfigFlag(String earnConfigFlag) {
        this.earnConfigFlag = earnConfigFlag;
    }

    public String getHolidayEmbedded() {
        return holidayEmbedded;
    }

    public void setHolidayEmbedded(String holidayEmbedded) {
        this.holidayEmbedded = holidayEmbedded;
    }

    public String getCommuted() {
        return commuted;
    }

    public void setCommuted(String commuted) {
        this.commuted = commuted;
    }

    public BigDecimal getMaxTaken() {
        return maxTaken;
    }

    public void setMaxTaken(BigDecimal maxTaken) {
        this.maxTaken = maxTaken;
    }

    public BigDecimal getMinTaken() {
        return minTaken;
    }

    public void setMinTaken(BigDecimal minTaken) {
        this.minTaken = minTaken;
    }

    public String getLeaveFormula() {
        return leaveFormula;
    }

    public void setLeaveFormula(String leaveFormula) {
        this.leaveFormula = leaveFormula;
    }

    public BigDecimal getLeaveBalDedPerLeave() {
        return leaveBalDedPerLeave;
    }

    public void setLeaveBalDedPerLeave(BigDecimal leaveBalDedPerLeave) {
        this.leaveBalDedPerLeave = leaveBalDedPerLeave;
    }

    public String getPartFulfillSubsysFlag() {
        return partFulfillSubsysFlag;
    }

    public void setPartFulfillSubsysFlag(String partFulfillSubsysFlag) {
        this.partFulfillSubsysFlag = partFulfillSubsysFlag;
    }

    public BigDecimal getEncashMinLeave() {
        return encashMinLeave;
    }

    public void setEncashMinLeave(BigDecimal encashMinLeave) {
        this.encashMinLeave = encashMinLeave;
    }

    public BigDecimal getEncashMaxLeave() {
        return encashMaxLeave;
    }

    public void setEncashMaxLeave(BigDecimal encashMaxLeave) {
        this.encashMaxLeave = encashMaxLeave;
    }

    public BigDecimal getEncashExtraMinLeave() {
        return encashExtraMinLeave;
    }

    public void setEncashExtraMinLeave(BigDecimal encashExtraMinLeave) {
        this.encashExtraMinLeave = encashExtraMinLeave;
    }

    public BigDecimal getEncashApprovalMinLeave() {
        return encashApprovalMinLeave;
    }

    public void setEncashApprovalMinLeave(BigDecimal encashApprovalMinLeave) {
        this.encashApprovalMinLeave = encashApprovalMinLeave;
    }

    public BigDecimal getEncashFreq() {
        return encashFreq;
    }

    public void setEncashFreq(BigDecimal encashFreq) {
        this.encashFreq = encashFreq;
    }

    public BigDecimal getEncashFreqPeriod() {
        return encashFreqPeriod;
    }

    public void setEncashFreqPeriod(BigDecimal encashFreqPeriod) {
        this.encashFreqPeriod = encashFreqPeriod;
    }

    public String getEncashFreqPeriodUnit() {
        return encashFreqPeriodUnit;
    }

    public void setEncashFreqPeriodUnit(String encashFreqPeriodUnit) {
        this.encashFreqPeriodUnit = encashFreqPeriodUnit;
    }

    public BigDecimal getCommuteMinLeave() {
        return commuteMinLeave;
    }

    public void setCommuteMinLeave(BigDecimal commuteMinLeave) {
        this.commuteMinLeave = commuteMinLeave;
    }

    public BigDecimal getCommuteDeductDays() {
        return commuteDeductDays;
    }

    public void setCommuteDeductDays(BigDecimal commuteDeductDays) {
        this.commuteDeductDays = commuteDeductDays;
    }

    public BigDecimal getCommuteDeductLeaves() {
        return commuteDeductLeaves;
    }

    public void setCommuteDeductLeaves(BigDecimal commuteDeductLeaves) {
        this.commuteDeductLeaves = commuteDeductLeaves;
    }

    public BigDecimal getLeaveBalDedPerComutleave() {
        return leaveBalDedPerComutleave;
    }

    public void setLeaveBalDedPerComutleave(BigDecimal leaveBalDedPerComutleave) {
        this.leaveBalDedPerComutleave = leaveBalDedPerComutleave;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public String getContrdueCarryForwrd() {
        return contrdueCarryForwrd;
    }

    public void setContrdueCarryForwrd(String contrdueCarryForwrd) {
        this.contrdueCarryForwrd = contrdueCarryForwrd;
    }

    public BigDecimal getContrdueCarryForwrdmax() {
        return contrdueCarryForwrdmax;
    }

    public void setContrdueCarryForwrdmax(BigDecimal contrdueCarryForwrdmax) {
        this.contrdueCarryForwrdmax = contrdueCarryForwrdmax;
    }

    public String getContrdueCarryforwrdRemaining() {
        return contrdueCarryforwrdRemaining;
    }

    public void setContrdueCarryforwrdRemaining(String contrdueCarryforwrdRemaining) {
        this.contrdueCarryforwrdRemaining = contrdueCarryforwrdRemaining;
    }

    public String getHalfdayApplicable() {
        return halfdayApplicable;
    }

    public void setHalfdayApplicable(String halfdayApplicable) {
        this.halfdayApplicable = halfdayApplicable;
    }

    public BigDecimal getElrSrgKey() {
        return elrSrgKey;
    }

    public void setElrSrgKey(BigDecimal elrSrgKey) {
        this.elrSrgKey = elrSrgKey;
    }

    public String getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(String startMonth) {
        this.startMonth = startMonth;
    }

    public String getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(String endMonth) {
        this.endMonth = endMonth;
    }

    public int getLmAltSrno() {
        return lmAltSrno;
    }

    public void setLmAltSrno(int lmAltSrno) {
        this.lmAltSrno = lmAltSrno;
    }

    public BigDecimal getParentElrSrgKey() {
        return parentElrSrgKey;
    }

    public void setParentElrSrgKey(BigDecimal parentElrSrgKey) {
        this.parentElrSrgKey = parentElrSrgKey;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public String getGroupLeaveDesc() {
        return groupLeaveDesc;
    }

    public void setGroupLeaveDesc(String groupLeaveDesc) {
        this.groupLeaveDesc = groupLeaveDesc;
    }

    public Date getBalanceRenewalClosingDate() {
        return balanceRenewalClosingDate;
    }

    public void setBalanceRenewalClosingDate(Date balanceRenewalClosingDate) {
        this.balanceRenewalClosingDate = balanceRenewalClosingDate;
    }

    public String getFixMonth() {
        return fixMonth;
    }

    public void setFixMonth(String fixMonth) {
        this.fixMonth = fixMonth;
    }

    public String getFreqConsFrom() {
        return freqConsFrom;
    }

    public void setFreqConsFrom(String freqConsFrom) {
        this.freqConsFrom = freqConsFrom;
    }

    public String getEligibilityApplicable() {
        return eligibilityApplicable;
    }

    public void setEligibilityApplicable(String eligibilityApplicable) {
        this.eligibilityApplicable = eligibilityApplicable;
    }

    public String getEligibilityConsidered() {
        return eligibilityConsidered;
    }

    public void setEligibilityConsidered(String eligibilityConsidered) {
        this.eligibilityConsidered = eligibilityConsidered;
    }

    public Short getEligibilityPeriod() {
        return eligibilityPeriod;
    }

    public void setEligibilityPeriod(Short eligibilityPeriod) {
        this.eligibilityPeriod = eligibilityPeriod;
    }

    public String getEligibilityUnit() {
        return eligibilityUnit;
    }

    public void setEligibilityUnit(String eligibilityUnit) {
        this.eligibilityUnit = eligibilityUnit;
    }

    public BigDecimal getPrevContrElrSrgKey() {
        return prevContrElrSrgKey;
    }

    public void setPrevContrElrSrgKey(BigDecimal prevContrElrSrgKey) {
        this.prevContrElrSrgKey = prevContrElrSrgKey;
    }

    public String getVirtualLeave() {
        return virtualLeave;
    }

    public void setVirtualLeave(String virtualLeave) {
        this.virtualLeave = virtualLeave;
    }

    public String getSystemUseridFlag() {
        return systemUseridFlag;
    }

    public void setSystemUseridFlag(String systemUseridFlag) {
        this.systemUseridFlag = systemUseridFlag;
    }

    public String getLeaveLapsInClubFlag() {
        return leaveLapsInClubFlag;
    }

    public void setLeaveLapsInClubFlag(String leaveLapsInClubFlag) {
        this.leaveLapsInClubFlag = leaveLapsInClubFlag;
    }

    public String getEarnIncludeFlag() {
        return earnIncludeFlag;
    }

    public void setEarnIncludeFlag(String earnIncludeFlag) {
        this.earnIncludeFlag = earnIncludeFlag;
    }

    public String getMultipleRuleFlag() {
        return multipleRuleFlag;
    }

    public void setMultipleRuleFlag(String multipleRuleFlag) {
        this.multipleRuleFlag = multipleRuleFlag;
    }

    public String getExhaustOtherLeaveFlag() {
        return exhaustOtherLeaveFlag;
    }

    public void setExhaustOtherLeaveFlag(String exhaustOtherLeaveFlag) {
        this.exhaustOtherLeaveFlag = exhaustOtherLeaveFlag;
    }

    public String getEncashAutoFlag() {
        return encashAutoFlag;
    }

    public void setEncashAutoFlag(String encashAutoFlag) {
        this.encashAutoFlag = encashAutoFlag;
    }

    public String getEncashAutoMonth() {
        return encashAutoMonth;
    }

    public void setEncashAutoMonth(String encashAutoMonth) {
        this.encashAutoMonth = encashAutoMonth;
    }

    public Integer getReqBalOnRelieve() {
        return reqBalOnRelieve;
    }

    public void setReqBalOnRelieve(Integer reqBalOnRelieve) {
        this.reqBalOnRelieve = reqBalOnRelieve;
    }

    public Date getLastRenewalOpeningDate() {
        return lastRenewalOpeningDate;
    }

    public void setLastRenewalOpeningDate(Date lastRenewalOpeningDate) {
        this.lastRenewalOpeningDate = lastRenewalOpeningDate;
    }

    public Date getContStartDate() {
        return contStartDate;
    }

    public void setContStartDate(Date contStartDate) {
        this.contStartDate = contStartDate;
    }

    public Date getContEndDate() {
        return contEndDate;
    }

    public void setContEndDate(Date contEndDate) {
        this.contEndDate = contEndDate;
    }

    public String getProportionateAtJoinOnly() {
        return proportionateAtJoinOnly;
    }

    public void setProportionateAtJoinOnly(String proportionateAtJoinOnly) {
        this.proportionateAtJoinOnly = proportionateAtJoinOnly;
    }

    public String getContAllowedFlag() {
        return contAllowedFlag;
    }

    public void setContAllowedFlag(String contAllowedFlag) {
        this.contAllowedFlag = contAllowedFlag;
    }

    public String getUserEntryFlag() {
        return userEntryFlag;
    }

    public void setUserEntryFlag(String userEntryFlag) {
        this.userEntryFlag = userEntryFlag;
    }

    @XmlTransient
    public Collection<FhrdPaytransleaveEarn> getFhrdPaytransleaveEarnCollection() {
        return fhrdPaytransleaveEarnCollection;
    }

    public void setFhrdPaytransleaveEarnCollection(Collection<FhrdPaytransleaveEarn> fhrdPaytransleaveEarnCollection) {
        this.fhrdPaytransleaveEarnCollection = fhrdPaytransleaveEarnCollection;
    }

    @XmlTransient
    public Collection<FtasAbsenteesDtl> getFtasAbsenteesDtlCollection() {
        return ftasAbsenteesDtlCollection;
    }

    public void setFtasAbsenteesDtlCollection(Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection) {
        this.ftasAbsenteesDtlCollection = ftasAbsenteesDtlCollection;
    }

    @XmlTransient
    public Collection<FtasAbsenteesDtl> getFtasAbsenteesDtlCollection1() {
        return ftasAbsenteesDtlCollection1;
    }

    public void setFtasAbsenteesDtlCollection1(Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection1) {
        this.ftasAbsenteesDtlCollection1 = ftasAbsenteesDtlCollection1;
    }

    @XmlTransient
    public Collection<FtasAbsenteesDtl> getFtasAbsenteesDtlCollection2() {
        return ftasAbsenteesDtlCollection2;
    }

    public void setFtasAbsenteesDtlCollection2(Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection2) {
        this.ftasAbsenteesDtlCollection2 = ftasAbsenteesDtlCollection2;
    }

    @XmlTransient
    public Collection<FhrdEmpleavebal> getFhrdEmpleavebalCollection() {
        return fhrdEmpleavebalCollection;
    }

    public void setFhrdEmpleavebalCollection(Collection<FhrdEmpleavebal> fhrdEmpleavebalCollection) {
        this.fhrdEmpleavebalCollection = fhrdEmpleavebalCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpleavebal> getFhrdEmpleavebalCollection1() {
        return fhrdEmpleavebalCollection1;
    }

    public void setFhrdEmpleavebalCollection1(Collection<FhrdEmpleavebal> fhrdEmpleavebalCollection1) {
        this.fhrdEmpleavebalCollection1 = fhrdEmpleavebalCollection1;
    }

    public SystOrgUnitMst getRefOumUnitSrno() {
        return refOumUnitSrno;
    }

    public void setRefOumUnitSrno(SystOrgUnitMst refOumUnitSrno) {
        this.refOumUnitSrno = refOumUnitSrno;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasLeavemst getLmSrgKey() {
        return lmSrgKey;
    }

    public void setLmSrgKey(FtasLeavemst lmSrgKey) {
        this.lmSrgKey = lmSrgKey;
    }

    public FhrdRuleManagGroupMst getRmgmSrgKey() {
        return rmgmSrgKey;
    }

    public void setRmgmSrgKey(FhrdRuleManagGroupMst rmgmSrgKey) {
        this.rmgmSrgKey = rmgmSrgKey;
    }

    public FhrdLeaverule getContrdueCarryLrSrgKey() {
        return contrdueCarryLrSrgKey;
    }

    public void setContrdueCarryLrSrgKey(FhrdLeaverule contrdueCarryLrSrgKey) {
        this.contrdueCarryLrSrgKey = contrdueCarryLrSrgKey;
    }

    public FhrdLeaverule getLrSrgKey() {
        return lrSrgKey;
    }

    public void setLrSrgKey(FhrdLeaverule lrSrgKey) {
        this.lrSrgKey = lrSrgKey;
    }

    public FhrdLeaverule getParentLrSrgKey() {
        return parentLrSrgKey;
    }

    public void setParentLrSrgKey(FhrdLeaverule parentLrSrgKey) {
        this.parentLrSrgKey = parentLrSrgKey;
    }

    public FhrdEmpContractGroupMst getEcgmSrgKey() {
        return ecgmSrgKey;
    }

    public void setEcgmSrgKey(FhrdEmpContractGroupMst ecgmSrgKey) {
        this.ecgmSrgKey = ecgmSrgKey;
    }

    public FhrdEmpmst getUserId() {
        return userId;
    }

    public void setUserId(FhrdEmpmst userId) {
        this.userId = userId;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (elrSrgKey != null ? elrSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdEmpleaverule)) {
            return false;
        }
        FhrdEmpleaverule other = (FhrdEmpleaverule) object;
        if ((this.elrSrgKey == null && other.elrSrgKey != null) || (this.elrSrgKey != null && !this.elrSrgKey.equals(other.elrSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdEmpleaverule[ elrSrgKey=" + elrSrgKey + " ]";
    }
    
}
