/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "BMWP_AP_ACTIVITY_BUDGET_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmwpApActivityBudgetDtl.findAll", query = "SELECT b FROM BmwpApActivityBudgetDtl b"),
    @NamedQuery(name = "BmwpApActivityBudgetDtl.findByApabdUniqueSrno", query = "SELECT b FROM BmwpApActivityBudgetDtl b WHERE b.apabdUniqueSrno = :apabdUniqueSrno"),
    @NamedQuery(name = "BmwpApActivityBudgetDtl.findByApabdSrno", query = "SELECT b FROM BmwpApActivityBudgetDtl b WHERE b.apabdSrno = :apabdSrno"),
    @NamedQuery(name = "BmwpApActivityBudgetDtl.findByCreatedt", query = "SELECT b FROM BmwpApActivityBudgetDtl b WHERE b.createdt = :createdt"),
    @NamedQuery(name = "BmwpApActivityBudgetDtl.findByUpdatedt", query = "SELECT b FROM BmwpApActivityBudgetDtl b WHERE b.updatedt = :updatedt"),
    @NamedQuery(name = "BmwpApActivityBudgetDtl.findBySystemUserid", query = "SELECT b FROM BmwpApActivityBudgetDtl b WHERE b.systemUserid = :systemUserid"),
    @NamedQuery(name = "BmwpApActivityBudgetDtl.findByExportFlag", query = "SELECT b FROM BmwpApActivityBudgetDtl b WHERE b.exportFlag = :exportFlag"),
    @NamedQuery(name = "BmwpApActivityBudgetDtl.findByImportFlag", query = "SELECT b FROM BmwpApActivityBudgetDtl b WHERE b.importFlag = :importFlag"),
    @NamedQuery(name = "BmwpApActivityBudgetDtl.findByApabdStartDate", query = "SELECT b FROM BmwpApActivityBudgetDtl b WHERE b.apabdStartDate = :apabdStartDate"),
    @NamedQuery(name = "BmwpApActivityBudgetDtl.findByApabdEndDate", query = "SELECT b FROM BmwpApActivityBudgetDtl b WHERE b.apabdEndDate = :apabdEndDate"),
    @NamedQuery(name = "BmwpApActivityBudgetDtl.findByApabdEndDatePossible", query = "SELECT b FROM BmwpApActivityBudgetDtl b WHERE b.apabdEndDatePossible = :apabdEndDatePossible"),
    @NamedQuery(name = "BmwpApActivityBudgetDtl.findByPmFundType", query = "SELECT b FROM BmwpApActivityBudgetDtl b WHERE b.pmFundType = :pmFundType")})
public class BmwpApActivityBudgetDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "APABD_UNIQUE_SRNO")
    private BigDecimal apabdUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APABD_SRNO")
    private int apabdSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APABD_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date apabdStartDate;
    @Column(name = "APABD_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date apabdEndDate;
    @Column(name = "APABD_END_DATE_POSSIBLE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date apabdEndDatePossible;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PM_FUND_TYPE")
    private String pmFundType;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "APABD_PM_UNIQUE_SRNO", referencedColumnName = "PM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpProjectMst apabdPmUniqueSrno;
    @JoinColumn(name = "APABD_FAM_UNIQUE_SRNO", referencedColumnName = "FAM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpFundAgencyMst apabdFamUniqueSrno;
    @JoinColumn(name = "APABD_APBM_UNIQUE_SRNO", referencedColumnName = "APBM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpApBudgetMst apabdApbmUniqueSrno;
    @JoinColumn(name = "APABD_APAM_UNIQUE_SRNO", referencedColumnName = "APAM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpApActivityMst apabdApamUniqueSrno;
    @JoinColumn(name = "APABD_APABL_UNIQUE_SRNO", referencedColumnName = "APABL_UNIQUE_SRNO")
    @ManyToOne(optional = false)
    private BmwpApActivityBudgetLink apabdApablUniqueSrno;
    @JoinColumn(name = "APABD_ATM_UNIQUE_SRNO", referencedColumnName = "ATM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpAgencyTypeMst apabdAtmUniqueSrno;
    @JoinColumn(name = "APABD_APL_UNIQUE_SRNO", referencedColumnName = "APL_UNIQUE_SRNO")
    @ManyToOne
    private BmwpAgencyProjectLink apabdAplUniqueSrno;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "wpApabdUniqueSrno")
    private Collection<BmwpWorkplan> bmwpWorkplanCollection;

    public BmwpApActivityBudgetDtl() {
    }

    public BmwpApActivityBudgetDtl(BigDecimal apabdUniqueSrno) {
        this.apabdUniqueSrno = apabdUniqueSrno;
    }

    public BmwpApActivityBudgetDtl(BigDecimal apabdUniqueSrno, int apabdSrno, Date createdt, Date apabdStartDate, String pmFundType) {
        this.apabdUniqueSrno = apabdUniqueSrno;
        this.apabdSrno = apabdSrno;
        this.createdt = createdt;
        this.apabdStartDate = apabdStartDate;
        this.pmFundType = pmFundType;
    }

    public BigDecimal getApabdUniqueSrno() {
        return apabdUniqueSrno;
    }

    public void setApabdUniqueSrno(BigDecimal apabdUniqueSrno) {
        this.apabdUniqueSrno = apabdUniqueSrno;
    }

    public int getApabdSrno() {
        return apabdSrno;
    }

    public void setApabdSrno(int apabdSrno) {
        this.apabdSrno = apabdSrno;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Date getApabdStartDate() {
        return apabdStartDate;
    }

    public void setApabdStartDate(Date apabdStartDate) {
        this.apabdStartDate = apabdStartDate;
    }

    public Date getApabdEndDate() {
        return apabdEndDate;
    }

    public void setApabdEndDate(Date apabdEndDate) {
        this.apabdEndDate = apabdEndDate;
    }

    public Date getApabdEndDatePossible() {
        return apabdEndDatePossible;
    }

    public void setApabdEndDatePossible(Date apabdEndDatePossible) {
        this.apabdEndDatePossible = apabdEndDatePossible;
    }

    public String getPmFundType() {
        return pmFundType;
    }

    public void setPmFundType(String pmFundType) {
        this.pmFundType = pmFundType;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public BmwpProjectMst getApabdPmUniqueSrno() {
        return apabdPmUniqueSrno;
    }

    public void setApabdPmUniqueSrno(BmwpProjectMst apabdPmUniqueSrno) {
        this.apabdPmUniqueSrno = apabdPmUniqueSrno;
    }

    public BmwpFundAgencyMst getApabdFamUniqueSrno() {
        return apabdFamUniqueSrno;
    }

    public void setApabdFamUniqueSrno(BmwpFundAgencyMst apabdFamUniqueSrno) {
        this.apabdFamUniqueSrno = apabdFamUniqueSrno;
    }

    public BmwpApBudgetMst getApabdApbmUniqueSrno() {
        return apabdApbmUniqueSrno;
    }

    public void setApabdApbmUniqueSrno(BmwpApBudgetMst apabdApbmUniqueSrno) {
        this.apabdApbmUniqueSrno = apabdApbmUniqueSrno;
    }

    public BmwpApActivityMst getApabdApamUniqueSrno() {
        return apabdApamUniqueSrno;
    }

    public void setApabdApamUniqueSrno(BmwpApActivityMst apabdApamUniqueSrno) {
        this.apabdApamUniqueSrno = apabdApamUniqueSrno;
    }

    public BmwpApActivityBudgetLink getApabdApablUniqueSrno() {
        return apabdApablUniqueSrno;
    }

    public void setApabdApablUniqueSrno(BmwpApActivityBudgetLink apabdApablUniqueSrno) {
        this.apabdApablUniqueSrno = apabdApablUniqueSrno;
    }

    public BmwpAgencyTypeMst getApabdAtmUniqueSrno() {
        return apabdAtmUniqueSrno;
    }

    public void setApabdAtmUniqueSrno(BmwpAgencyTypeMst apabdAtmUniqueSrno) {
        this.apabdAtmUniqueSrno = apabdAtmUniqueSrno;
    }

    public BmwpAgencyProjectLink getApabdAplUniqueSrno() {
        return apabdAplUniqueSrno;
    }

    public void setApabdAplUniqueSrno(BmwpAgencyProjectLink apabdAplUniqueSrno) {
        this.apabdAplUniqueSrno = apabdAplUniqueSrno;
    }

    @XmlTransient
    public Collection<BmwpWorkplan> getBmwpWorkplanCollection() {
        return bmwpWorkplanCollection;
    }

    public void setBmwpWorkplanCollection(Collection<BmwpWorkplan> bmwpWorkplanCollection) {
        this.bmwpWorkplanCollection = bmwpWorkplanCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (apabdUniqueSrno != null ? apabdUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmwpApActivityBudgetDtl)) {
            return false;
        }
        BmwpApActivityBudgetDtl other = (BmwpApActivityBudgetDtl) object;
        if ((this.apabdUniqueSrno == null && other.apabdUniqueSrno != null) || (this.apabdUniqueSrno != null && !this.apabdUniqueSrno.equals(other.apabdUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.BmwpApActivityBudgetDtl[ apabdUniqueSrno=" + apabdUniqueSrno + " ]";
    }
    
}
