/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 *
 * @author anu
 */
@Embeddable
public class FtasBulkAttTempPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_ID")
    private int userId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATTDDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date attddt;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "BAT_SRNO")
    private BigDecimal batSrno;

    public FtasBulkAttTempPK() {
    }

    public FtasBulkAttTempPK(int userId, Date attddt, BigDecimal batSrno) {
        this.userId = userId;
        this.attddt = attddt;
        this.batSrno = batSrno;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getAttddt() {
        return attddt;
    }

    public void setAttddt(Date attddt) {
        this.attddt = attddt;
    }

    public BigDecimal getBatSrno() {
        return batSrno;
    }

    public void setBatSrno(BigDecimal batSrno) {
        this.batSrno = batSrno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) userId;
        hash += (attddt != null ? attddt.hashCode() : 0);
        hash += (batSrno != null ? batSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasBulkAttTempPK)) {
            return false;
        }
        FtasBulkAttTempPK other = (FtasBulkAttTempPK) object;
        if (this.userId != other.userId) {
            return false;
        }
        if ((this.attddt == null && other.attddt != null) || (this.attddt != null && !this.attddt.equals(other.attddt))) {
            return false;
        }
        if ((this.batSrno == null && other.batSrno != null) || (this.batSrno != null && !this.batSrno.equals(other.batSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasBulkAttTempPK[ userId=" + userId + ", attddt=" + attddt + ", batSrno=" + batSrno + " ]";
    }
    
}
