/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "BMWP_BUDGET_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmwpBudgetMst.findAll", query = "SELECT b FROM BmwpBudgetMst b"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmUniqueSrno", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmUniqueSrno = :bmUniqueSrno"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmSrno", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmSrno = :bmSrno"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmBudgetSrno", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmBudgetSrno = :bmBudgetSrno"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmBudgetShortName", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmBudgetShortName = :bmBudgetShortName"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmBudgetDesc", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmBudgetDesc = :bmBudgetDesc"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmParentBudgetSrno", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmParentBudgetSrno = :bmParentBudgetSrno"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmBudgetSrnoLevel", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmBudgetSrnoLevel = :bmBudgetSrnoLevel"),
    @NamedQuery(name = "BmwpBudgetMst.findByCreatedt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.createdt = :createdt"),
    @NamedQuery(name = "BmwpBudgetMst.findByUpdatedt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.updatedt = :updatedt"),
    @NamedQuery(name = "BmwpBudgetMst.findBySystemUserid", query = "SELECT b FROM BmwpBudgetMst b WHERE b.systemUserid = :systemUserid"),
    @NamedQuery(name = "BmwpBudgetMst.findByExportFlag", query = "SELECT b FROM BmwpBudgetMst b WHERE b.exportFlag = :exportFlag"),
    @NamedQuery(name = "BmwpBudgetMst.findByImportFlag", query = "SELECT b FROM BmwpBudgetMst b WHERE b.importFlag = :importFlag"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmStartDate", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmStartDate = :bmStartDate"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmEndDate", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmEndDate = :bmEndDate"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmCarryFwdAdvAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmCarryFwdAdvAmt = :bmCarryFwdAdvAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmReceivedFundAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmReceivedFundAmt = :bmReceivedFundAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmReceivedAdvanceAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmReceivedAdvanceAmt = :bmReceivedAdvanceAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmSurrenderFundAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmSurrenderFundAmt = :bmSurrenderFundAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmWorkplanAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmWorkplanAmt = :bmWorkplanAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmWpSurrenderAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmWpSurrenderAmt = :bmWpSurrenderAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmReAllotWpAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmReAllotWpAmt = :bmReAllotWpAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmReviseWpAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmReviseWpAmt = :bmReviseWpAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmCommitedAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmCommitedAmt = :bmCommitedAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmPipelinePayAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmPipelinePayAmt = :bmPipelinePayAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmAdvanceCommitAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmAdvanceCommitAmt = :bmAdvanceCommitAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmAdvancePayAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmAdvancePayAmt = :bmAdvancePayAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmPipeAdvadjCommitAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmPipeAdvadjCommitAmt = :bmPipeAdvadjCommitAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmPipeAdvadjPayAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmPipeAdvadjPayAmt = :bmPipeAdvadjPayAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmReceiptPayAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmReceiptPayAmt = :bmReceiptPayAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmReceiptCommitAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmReceiptCommitAmt = :bmReceiptCommitAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmExpenseAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmExpenseAmt = :bmExpenseAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmUsableBalanceAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmUsableBalanceAmt = :bmUsableBalanceAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmBalanceAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmBalanceAmt = :bmBalanceAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmPipeCommitedAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmPipeCommitedAmt = :bmPipeCommitedAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmEndDatePossible", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmEndDatePossible = :bmEndDatePossible"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmLedgerCodeTypeFlag", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmLedgerCodeTypeFlag = :bmLedgerCodeTypeFlag"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmDrAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmDrAmt = :bmDrAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmCrAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmCrAmt = :bmCrAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmChildOfferFlag", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmChildOfferFlag = :bmChildOfferFlag"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmVirtualBudgetFlag", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmVirtualBudgetFlag = :bmVirtualBudgetFlag"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmLdrUsableBalanceAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmLdrUsableBalanceAmt = :bmLdrUsableBalanceAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmLdrBalanceAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmLdrBalanceAmt = :bmLdrBalanceAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmLedgerCodeSubTypeFlag", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmLedgerCodeSubTypeFlag = :bmLedgerCodeSubTypeFlag"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmReCreateFlag", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmReCreateFlag = :bmReCreateFlag"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmBlockByWpAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmBlockByWpAmt = :bmBlockByWpAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmApplicableForTeams", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmApplicableForTeams = :bmApplicableForTeams"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmTransactionDate", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmTransactionDate = :bmTransactionDate"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmAdvanceWithWpFlag", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmAdvanceWithWpFlag = :bmAdvanceWithWpFlag"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmProCrExpAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmProCrExpAmt = :bmProCrExpAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmCommContriAmt", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmCommContriAmt = :bmCommContriAmt"),
    @NamedQuery(name = "BmwpBudgetMst.findByBmPmFundMonitorFlag", query = "SELECT b FROM BmwpBudgetMst b WHERE b.bmPmFundMonitorFlag = :bmPmFundMonitorFlag")})
public class BmwpBudgetMst implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_UNIQUE_SRNO")
    private BigDecimal bmUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_SRNO")
    private int bmSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_BUDGET_SRNO")
    private BigInteger bmBudgetSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "BM_BUDGET_SHORT_NAME")
    private String bmBudgetShortName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "BM_BUDGET_DESC")
    private String bmBudgetDesc;
    @Column(name = "BM_PARENT_BUDGET_SRNO")
    private BigInteger bmParentBudgetSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_BUDGET_SRNO_LEVEL")
    private int bmBudgetSrnoLevel;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date bmStartDate;
    @Column(name = "BM_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date bmEndDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_CARRY_FWD_ADV_AMT")
    private BigDecimal bmCarryFwdAdvAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_RECEIVED_FUND_AMT")
    private BigDecimal bmReceivedFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_RECEIVED_ADVANCE_AMT")
    private BigDecimal bmReceivedAdvanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_SURRENDER_FUND_AMT")
    private BigDecimal bmSurrenderFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_WORKPLAN_AMT")
    private BigDecimal bmWorkplanAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_WP_SURRENDER_AMT")
    private BigDecimal bmWpSurrenderAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_RE_ALLOT_WP_AMT")
    private BigDecimal bmReAllotWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_REVISE_WP_AMT")
    private BigDecimal bmReviseWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_COMMITED_AMT")
    private BigDecimal bmCommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_PIPELINE_PAY_AMT")
    private BigDecimal bmPipelinePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_ADVANCE_COMMIT_AMT")
    private BigDecimal bmAdvanceCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_ADVANCE_PAY_AMT")
    private BigDecimal bmAdvancePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_PIPE_ADVADJ_COMMIT_AMT")
    private BigDecimal bmPipeAdvadjCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_PIPE_ADVADJ_PAY_AMT")
    private BigDecimal bmPipeAdvadjPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_RECEIPT_PAY_AMT")
    private BigDecimal bmReceiptPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_RECEIPT_COMMIT_AMT")
    private BigDecimal bmReceiptCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_EXPENSE_AMT")
    private BigDecimal bmExpenseAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_USABLE_BALANCE_AMT")
    private BigDecimal bmUsableBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_BALANCE_AMT")
    private BigDecimal bmBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_PIPE_COMMITED_AMT")
    private BigDecimal bmPipeCommitedAmt;
    @Column(name = "BM_END_DATE_POSSIBLE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date bmEndDatePossible;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "BM_LEDGER_CODE_TYPE_FLAG")
    private String bmLedgerCodeTypeFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_DR_AMT")
    private BigDecimal bmDrAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_CR_AMT")
    private BigDecimal bmCrAmt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "BM_CHILD_OFFER_FLAG")
    private String bmChildOfferFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "BM_VIRTUAL_BUDGET_FLAG")
    private String bmVirtualBudgetFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_LDR_USABLE_BALANCE_AMT")
    private BigDecimal bmLdrUsableBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_LDR_BALANCE_AMT")
    private BigDecimal bmLdrBalanceAmt;
    @Size(max = 1)
    @Column(name = "BM_LEDGER_CODE_SUB_TYPE_FLAG")
    private String bmLedgerCodeSubTypeFlag;
    @Size(max = 1)
    @Column(name = "BM_RE_CREATE_FLAG")
    private String bmReCreateFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_BLOCK_BY_WP_AMT")
    private BigDecimal bmBlockByWpAmt;
    @Size(max = 1)
    @Column(name = "BM_APPLICABLE_FOR_TEAMS")
    private String bmApplicableForTeams;
    @Column(name = "BM_TRANSACTION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date bmTransactionDate;
    @Size(max = 1)
    @Column(name = "BM_ADVANCE_WITH_WP_FLAG")
    private String bmAdvanceWithWpFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_PRO_CR_EXP_AMT")
    private BigDecimal bmProCrExpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "BM_COMM_CONTRI_AMT")
    private BigDecimal bmCommContriAmt;
    @Size(max = 1)
    @Column(name = "BM_PM_FUND_MONITOR_FLAG")
    private String bmPmFundMonitorFlag;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ablBmUniqueSrno")
    private Collection<BmwpActivityBudgetLink> bmwpActivityBudgetLinkCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "BM_PTM_UNIQUE_SRNO", referencedColumnName = "PTM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpPartyTypeMst bmPtmUniqueSrno;
    @OneToMany(mappedBy = "bmParentUniqueSrno")
    private Collection<BmwpBudgetMst> bmwpBudgetMstCollection;
    @JoinColumn(name = "BM_PARENT_UNIQUE_SRNO", referencedColumnName = "BM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpBudgetMst bmParentUniqueSrno;
    @OneToMany(mappedBy = "apbmBmUniqueSrno")
    private Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection;
    @OneToMany(mappedBy = "wpBmUniqueSrno")
    private Collection<BmwpWorkplan> bmwpWorkplanCollection;
    @OneToMany(mappedBy = "abdBmUniqueSrno")
    private Collection<BmwpActivityBudgetDtl> bmwpActivityBudgetDtlCollection;

    public BmwpBudgetMst() {
    }

    public BmwpBudgetMst(BigDecimal bmUniqueSrno) {
        this.bmUniqueSrno = bmUniqueSrno;
    }

    public BmwpBudgetMst(BigDecimal bmUniqueSrno, int bmSrno, BigInteger bmBudgetSrno, String bmBudgetShortName, String bmBudgetDesc, int bmBudgetSrnoLevel, Date createdt, Date bmStartDate, BigDecimal bmCarryFwdAdvAmt, BigDecimal bmReceivedFundAmt, BigDecimal bmReceivedAdvanceAmt, BigDecimal bmSurrenderFundAmt, BigDecimal bmWorkplanAmt, BigDecimal bmWpSurrenderAmt, BigDecimal bmReAllotWpAmt, BigDecimal bmReviseWpAmt, BigDecimal bmCommitedAmt, BigDecimal bmPipelinePayAmt, BigDecimal bmAdvanceCommitAmt, BigDecimal bmAdvancePayAmt, BigDecimal bmPipeAdvadjCommitAmt, BigDecimal bmPipeAdvadjPayAmt, BigDecimal bmReceiptPayAmt, BigDecimal bmReceiptCommitAmt, BigDecimal bmExpenseAmt, BigDecimal bmUsableBalanceAmt, BigDecimal bmBalanceAmt, BigDecimal bmPipeCommitedAmt, String bmLedgerCodeTypeFlag, BigDecimal bmDrAmt, BigDecimal bmCrAmt, String bmChildOfferFlag, String bmVirtualBudgetFlag, BigDecimal bmLdrUsableBalanceAmt, BigDecimal bmLdrBalanceAmt, BigDecimal bmBlockByWpAmt, BigDecimal bmProCrExpAmt, BigDecimal bmCommContriAmt) {
        this.bmUniqueSrno = bmUniqueSrno;
        this.bmSrno = bmSrno;
        this.bmBudgetSrno = bmBudgetSrno;
        this.bmBudgetShortName = bmBudgetShortName;
        this.bmBudgetDesc = bmBudgetDesc;
        this.bmBudgetSrnoLevel = bmBudgetSrnoLevel;
        this.createdt = createdt;
        this.bmStartDate = bmStartDate;
        this.bmCarryFwdAdvAmt = bmCarryFwdAdvAmt;
        this.bmReceivedFundAmt = bmReceivedFundAmt;
        this.bmReceivedAdvanceAmt = bmReceivedAdvanceAmt;
        this.bmSurrenderFundAmt = bmSurrenderFundAmt;
        this.bmWorkplanAmt = bmWorkplanAmt;
        this.bmWpSurrenderAmt = bmWpSurrenderAmt;
        this.bmReAllotWpAmt = bmReAllotWpAmt;
        this.bmReviseWpAmt = bmReviseWpAmt;
        this.bmCommitedAmt = bmCommitedAmt;
        this.bmPipelinePayAmt = bmPipelinePayAmt;
        this.bmAdvanceCommitAmt = bmAdvanceCommitAmt;
        this.bmAdvancePayAmt = bmAdvancePayAmt;
        this.bmPipeAdvadjCommitAmt = bmPipeAdvadjCommitAmt;
        this.bmPipeAdvadjPayAmt = bmPipeAdvadjPayAmt;
        this.bmReceiptPayAmt = bmReceiptPayAmt;
        this.bmReceiptCommitAmt = bmReceiptCommitAmt;
        this.bmExpenseAmt = bmExpenseAmt;
        this.bmUsableBalanceAmt = bmUsableBalanceAmt;
        this.bmBalanceAmt = bmBalanceAmt;
        this.bmPipeCommitedAmt = bmPipeCommitedAmt;
        this.bmLedgerCodeTypeFlag = bmLedgerCodeTypeFlag;
        this.bmDrAmt = bmDrAmt;
        this.bmCrAmt = bmCrAmt;
        this.bmChildOfferFlag = bmChildOfferFlag;
        this.bmVirtualBudgetFlag = bmVirtualBudgetFlag;
        this.bmLdrUsableBalanceAmt = bmLdrUsableBalanceAmt;
        this.bmLdrBalanceAmt = bmLdrBalanceAmt;
        this.bmBlockByWpAmt = bmBlockByWpAmt;
        this.bmProCrExpAmt = bmProCrExpAmt;
        this.bmCommContriAmt = bmCommContriAmt;
    }

    public BigDecimal getBmUniqueSrno() {
        return bmUniqueSrno;
    }

    public void setBmUniqueSrno(BigDecimal bmUniqueSrno) {
        this.bmUniqueSrno = bmUniqueSrno;
    }

    public int getBmSrno() {
        return bmSrno;
    }

    public void setBmSrno(int bmSrno) {
        this.bmSrno = bmSrno;
    }

    public BigInteger getBmBudgetSrno() {
        return bmBudgetSrno;
    }

    public void setBmBudgetSrno(BigInteger bmBudgetSrno) {
        this.bmBudgetSrno = bmBudgetSrno;
    }

    public String getBmBudgetShortName() {
        return bmBudgetShortName;
    }

    public void setBmBudgetShortName(String bmBudgetShortName) {
        this.bmBudgetShortName = bmBudgetShortName;
    }

    public String getBmBudgetDesc() {
        return bmBudgetDesc;
    }

    public void setBmBudgetDesc(String bmBudgetDesc) {
        this.bmBudgetDesc = bmBudgetDesc;
    }

    public BigInteger getBmParentBudgetSrno() {
        return bmParentBudgetSrno;
    }

    public void setBmParentBudgetSrno(BigInteger bmParentBudgetSrno) {
        this.bmParentBudgetSrno = bmParentBudgetSrno;
    }

    public int getBmBudgetSrnoLevel() {
        return bmBudgetSrnoLevel;
    }

    public void setBmBudgetSrnoLevel(int bmBudgetSrnoLevel) {
        this.bmBudgetSrnoLevel = bmBudgetSrnoLevel;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Date getBmStartDate() {
        return bmStartDate;
    }

    public void setBmStartDate(Date bmStartDate) {
        this.bmStartDate = bmStartDate;
    }

    public Date getBmEndDate() {
        return bmEndDate;
    }

    public void setBmEndDate(Date bmEndDate) {
        this.bmEndDate = bmEndDate;
    }

    public BigDecimal getBmCarryFwdAdvAmt() {
        return bmCarryFwdAdvAmt;
    }

    public void setBmCarryFwdAdvAmt(BigDecimal bmCarryFwdAdvAmt) {
        this.bmCarryFwdAdvAmt = bmCarryFwdAdvAmt;
    }

    public BigDecimal getBmReceivedFundAmt() {
        return bmReceivedFundAmt;
    }

    public void setBmReceivedFundAmt(BigDecimal bmReceivedFundAmt) {
        this.bmReceivedFundAmt = bmReceivedFundAmt;
    }

    public BigDecimal getBmReceivedAdvanceAmt() {
        return bmReceivedAdvanceAmt;
    }

    public void setBmReceivedAdvanceAmt(BigDecimal bmReceivedAdvanceAmt) {
        this.bmReceivedAdvanceAmt = bmReceivedAdvanceAmt;
    }

    public BigDecimal getBmSurrenderFundAmt() {
        return bmSurrenderFundAmt;
    }

    public void setBmSurrenderFundAmt(BigDecimal bmSurrenderFundAmt) {
        this.bmSurrenderFundAmt = bmSurrenderFundAmt;
    }

    public BigDecimal getBmWorkplanAmt() {
        return bmWorkplanAmt;
    }

    public void setBmWorkplanAmt(BigDecimal bmWorkplanAmt) {
        this.bmWorkplanAmt = bmWorkplanAmt;
    }

    public BigDecimal getBmWpSurrenderAmt() {
        return bmWpSurrenderAmt;
    }

    public void setBmWpSurrenderAmt(BigDecimal bmWpSurrenderAmt) {
        this.bmWpSurrenderAmt = bmWpSurrenderAmt;
    }

    public BigDecimal getBmReAllotWpAmt() {
        return bmReAllotWpAmt;
    }

    public void setBmReAllotWpAmt(BigDecimal bmReAllotWpAmt) {
        this.bmReAllotWpAmt = bmReAllotWpAmt;
    }

    public BigDecimal getBmReviseWpAmt() {
        return bmReviseWpAmt;
    }

    public void setBmReviseWpAmt(BigDecimal bmReviseWpAmt) {
        this.bmReviseWpAmt = bmReviseWpAmt;
    }

    public BigDecimal getBmCommitedAmt() {
        return bmCommitedAmt;
    }

    public void setBmCommitedAmt(BigDecimal bmCommitedAmt) {
        this.bmCommitedAmt = bmCommitedAmt;
    }

    public BigDecimal getBmPipelinePayAmt() {
        return bmPipelinePayAmt;
    }

    public void setBmPipelinePayAmt(BigDecimal bmPipelinePayAmt) {
        this.bmPipelinePayAmt = bmPipelinePayAmt;
    }

    public BigDecimal getBmAdvanceCommitAmt() {
        return bmAdvanceCommitAmt;
    }

    public void setBmAdvanceCommitAmt(BigDecimal bmAdvanceCommitAmt) {
        this.bmAdvanceCommitAmt = bmAdvanceCommitAmt;
    }

    public BigDecimal getBmAdvancePayAmt() {
        return bmAdvancePayAmt;
    }

    public void setBmAdvancePayAmt(BigDecimal bmAdvancePayAmt) {
        this.bmAdvancePayAmt = bmAdvancePayAmt;
    }

    public BigDecimal getBmPipeAdvadjCommitAmt() {
        return bmPipeAdvadjCommitAmt;
    }

    public void setBmPipeAdvadjCommitAmt(BigDecimal bmPipeAdvadjCommitAmt) {
        this.bmPipeAdvadjCommitAmt = bmPipeAdvadjCommitAmt;
    }

    public BigDecimal getBmPipeAdvadjPayAmt() {
        return bmPipeAdvadjPayAmt;
    }

    public void setBmPipeAdvadjPayAmt(BigDecimal bmPipeAdvadjPayAmt) {
        this.bmPipeAdvadjPayAmt = bmPipeAdvadjPayAmt;
    }

    public BigDecimal getBmReceiptPayAmt() {
        return bmReceiptPayAmt;
    }

    public void setBmReceiptPayAmt(BigDecimal bmReceiptPayAmt) {
        this.bmReceiptPayAmt = bmReceiptPayAmt;
    }

    public BigDecimal getBmReceiptCommitAmt() {
        return bmReceiptCommitAmt;
    }

    public void setBmReceiptCommitAmt(BigDecimal bmReceiptCommitAmt) {
        this.bmReceiptCommitAmt = bmReceiptCommitAmt;
    }

    public BigDecimal getBmExpenseAmt() {
        return bmExpenseAmt;
    }

    public void setBmExpenseAmt(BigDecimal bmExpenseAmt) {
        this.bmExpenseAmt = bmExpenseAmt;
    }

    public BigDecimal getBmUsableBalanceAmt() {
        return bmUsableBalanceAmt;
    }

    public void setBmUsableBalanceAmt(BigDecimal bmUsableBalanceAmt) {
        this.bmUsableBalanceAmt = bmUsableBalanceAmt;
    }

    public BigDecimal getBmBalanceAmt() {
        return bmBalanceAmt;
    }

    public void setBmBalanceAmt(BigDecimal bmBalanceAmt) {
        this.bmBalanceAmt = bmBalanceAmt;
    }

    public BigDecimal getBmPipeCommitedAmt() {
        return bmPipeCommitedAmt;
    }

    public void setBmPipeCommitedAmt(BigDecimal bmPipeCommitedAmt) {
        this.bmPipeCommitedAmt = bmPipeCommitedAmt;
    }

    public Date getBmEndDatePossible() {
        return bmEndDatePossible;
    }

    public void setBmEndDatePossible(Date bmEndDatePossible) {
        this.bmEndDatePossible = bmEndDatePossible;
    }

    public String getBmLedgerCodeTypeFlag() {
        return bmLedgerCodeTypeFlag;
    }

    public void setBmLedgerCodeTypeFlag(String bmLedgerCodeTypeFlag) {
        this.bmLedgerCodeTypeFlag = bmLedgerCodeTypeFlag;
    }

    public BigDecimal getBmDrAmt() {
        return bmDrAmt;
    }

    public void setBmDrAmt(BigDecimal bmDrAmt) {
        this.bmDrAmt = bmDrAmt;
    }

    public BigDecimal getBmCrAmt() {
        return bmCrAmt;
    }

    public void setBmCrAmt(BigDecimal bmCrAmt) {
        this.bmCrAmt = bmCrAmt;
    }

    public String getBmChildOfferFlag() {
        return bmChildOfferFlag;
    }

    public void setBmChildOfferFlag(String bmChildOfferFlag) {
        this.bmChildOfferFlag = bmChildOfferFlag;
    }

    public String getBmVirtualBudgetFlag() {
        return bmVirtualBudgetFlag;
    }

    public void setBmVirtualBudgetFlag(String bmVirtualBudgetFlag) {
        this.bmVirtualBudgetFlag = bmVirtualBudgetFlag;
    }

    public BigDecimal getBmLdrUsableBalanceAmt() {
        return bmLdrUsableBalanceAmt;
    }

    public void setBmLdrUsableBalanceAmt(BigDecimal bmLdrUsableBalanceAmt) {
        this.bmLdrUsableBalanceAmt = bmLdrUsableBalanceAmt;
    }

    public BigDecimal getBmLdrBalanceAmt() {
        return bmLdrBalanceAmt;
    }

    public void setBmLdrBalanceAmt(BigDecimal bmLdrBalanceAmt) {
        this.bmLdrBalanceAmt = bmLdrBalanceAmt;
    }

    public String getBmLedgerCodeSubTypeFlag() {
        return bmLedgerCodeSubTypeFlag;
    }

    public void setBmLedgerCodeSubTypeFlag(String bmLedgerCodeSubTypeFlag) {
        this.bmLedgerCodeSubTypeFlag = bmLedgerCodeSubTypeFlag;
    }

    public String getBmReCreateFlag() {
        return bmReCreateFlag;
    }

    public void setBmReCreateFlag(String bmReCreateFlag) {
        this.bmReCreateFlag = bmReCreateFlag;
    }

    public BigDecimal getBmBlockByWpAmt() {
        return bmBlockByWpAmt;
    }

    public void setBmBlockByWpAmt(BigDecimal bmBlockByWpAmt) {
        this.bmBlockByWpAmt = bmBlockByWpAmt;
    }

    public String getBmApplicableForTeams() {
        return bmApplicableForTeams;
    }

    public void setBmApplicableForTeams(String bmApplicableForTeams) {
        this.bmApplicableForTeams = bmApplicableForTeams;
    }

    public Date getBmTransactionDate() {
        return bmTransactionDate;
    }

    public void setBmTransactionDate(Date bmTransactionDate) {
        this.bmTransactionDate = bmTransactionDate;
    }

    public String getBmAdvanceWithWpFlag() {
        return bmAdvanceWithWpFlag;
    }

    public void setBmAdvanceWithWpFlag(String bmAdvanceWithWpFlag) {
        this.bmAdvanceWithWpFlag = bmAdvanceWithWpFlag;
    }

    public BigDecimal getBmProCrExpAmt() {
        return bmProCrExpAmt;
    }

    public void setBmProCrExpAmt(BigDecimal bmProCrExpAmt) {
        this.bmProCrExpAmt = bmProCrExpAmt;
    }

    public BigDecimal getBmCommContriAmt() {
        return bmCommContriAmt;
    }

    public void setBmCommContriAmt(BigDecimal bmCommContriAmt) {
        this.bmCommContriAmt = bmCommContriAmt;
    }

    public String getBmPmFundMonitorFlag() {
        return bmPmFundMonitorFlag;
    }

    public void setBmPmFundMonitorFlag(String bmPmFundMonitorFlag) {
        this.bmPmFundMonitorFlag = bmPmFundMonitorFlag;
    }

    @XmlTransient
    public Collection<BmwpActivityBudgetLink> getBmwpActivityBudgetLinkCollection() {
        return bmwpActivityBudgetLinkCollection;
    }

    public void setBmwpActivityBudgetLinkCollection(Collection<BmwpActivityBudgetLink> bmwpActivityBudgetLinkCollection) {
        this.bmwpActivityBudgetLinkCollection = bmwpActivityBudgetLinkCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public BmwpPartyTypeMst getBmPtmUniqueSrno() {
        return bmPtmUniqueSrno;
    }

    public void setBmPtmUniqueSrno(BmwpPartyTypeMst bmPtmUniqueSrno) {
        this.bmPtmUniqueSrno = bmPtmUniqueSrno;
    }

    @XmlTransient
    public Collection<BmwpBudgetMst> getBmwpBudgetMstCollection() {
        return bmwpBudgetMstCollection;
    }

    public void setBmwpBudgetMstCollection(Collection<BmwpBudgetMst> bmwpBudgetMstCollection) {
        this.bmwpBudgetMstCollection = bmwpBudgetMstCollection;
    }

    public BmwpBudgetMst getBmParentUniqueSrno() {
        return bmParentUniqueSrno;
    }

    public void setBmParentUniqueSrno(BmwpBudgetMst bmParentUniqueSrno) {
        this.bmParentUniqueSrno = bmParentUniqueSrno;
    }

    @XmlTransient
    public Collection<BmwpApBudgetMst> getBmwpApBudgetMstCollection() {
        return bmwpApBudgetMstCollection;
    }

    public void setBmwpApBudgetMstCollection(Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection) {
        this.bmwpApBudgetMstCollection = bmwpApBudgetMstCollection;
    }

    @XmlTransient
    public Collection<BmwpWorkplan> getBmwpWorkplanCollection() {
        return bmwpWorkplanCollection;
    }

    public void setBmwpWorkplanCollection(Collection<BmwpWorkplan> bmwpWorkplanCollection) {
        this.bmwpWorkplanCollection = bmwpWorkplanCollection;
    }

    @XmlTransient
    public Collection<BmwpActivityBudgetDtl> getBmwpActivityBudgetDtlCollection() {
        return bmwpActivityBudgetDtlCollection;
    }

    public void setBmwpActivityBudgetDtlCollection(Collection<BmwpActivityBudgetDtl> bmwpActivityBudgetDtlCollection) {
        this.bmwpActivityBudgetDtlCollection = bmwpActivityBudgetDtlCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bmUniqueSrno != null ? bmUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmwpBudgetMst)) {
            return false;
        }
        BmwpBudgetMst other = (BmwpBudgetMst) object;
        if ((this.bmUniqueSrno == null && other.bmUniqueSrno != null) || (this.bmUniqueSrno != null && !this.bmUniqueSrno.equals(other.bmUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.BmwpBudgetMst[ bmUniqueSrno=" + bmUniqueSrno + " ]";
    }
    
}
