/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author anu
 */
@Embeddable
public class FhrdPaytransearndednSummaryPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "OUM_UNIT_SRNO")
    private int oumUnitSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_SALARY_FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgSalaryFromDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_SALARY_TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgSalaryToDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_ID")
    private int userId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "EARNDEDNCD")
    private String earndedncd;

    public FhrdPaytransearndednSummaryPK() {
    }

    public FhrdPaytransearndednSummaryPK(int oumUnitSrno, Date orgSalaryFromDate, Date orgSalaryToDate, int userId, String earndedncd) {
        this.oumUnitSrno = oumUnitSrno;
        this.orgSalaryFromDate = orgSalaryFromDate;
        this.orgSalaryToDate = orgSalaryToDate;
        this.userId = userId;
        this.earndedncd = earndedncd;
    }

    public int getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(int oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public Date getOrgSalaryFromDate() {
        return orgSalaryFromDate;
    }

    public void setOrgSalaryFromDate(Date orgSalaryFromDate) {
        this.orgSalaryFromDate = orgSalaryFromDate;
    }

    public Date getOrgSalaryToDate() {
        return orgSalaryToDate;
    }

    public void setOrgSalaryToDate(Date orgSalaryToDate) {
        this.orgSalaryToDate = orgSalaryToDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getEarndedncd() {
        return earndedncd;
    }

    public void setEarndedncd(String earndedncd) {
        this.earndedncd = earndedncd;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) oumUnitSrno;
        hash += (orgSalaryFromDate != null ? orgSalaryFromDate.hashCode() : 0);
        hash += (orgSalaryToDate != null ? orgSalaryToDate.hashCode() : 0);
        hash += (int) userId;
        hash += (earndedncd != null ? earndedncd.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdPaytransearndednSummaryPK)) {
            return false;
        }
        FhrdPaytransearndednSummaryPK other = (FhrdPaytransearndednSummaryPK) object;
        if (this.oumUnitSrno != other.oumUnitSrno) {
            return false;
        }
        if ((this.orgSalaryFromDate == null && other.orgSalaryFromDate != null) || (this.orgSalaryFromDate != null && !this.orgSalaryFromDate.equals(other.orgSalaryFromDate))) {
            return false;
        }
        if ((this.orgSalaryToDate == null && other.orgSalaryToDate != null) || (this.orgSalaryToDate != null && !this.orgSalaryToDate.equals(other.orgSalaryToDate))) {
            return false;
        }
        if (this.userId != other.userId) {
            return false;
        }
        if ((this.earndedncd == null && other.earndedncd != null) || (this.earndedncd != null && !this.earndedncd.equals(other.earndedncd))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdPaytransearndednSummaryPK[ oumUnitSrno=" + oumUnitSrno + ", orgSalaryFromDate=" + orgSalaryFromDate + ", orgSalaryToDate=" + orgSalaryToDate + ", userId=" + userId + ", earndedncd=" + earndedncd + " ]";
    }
    
}
