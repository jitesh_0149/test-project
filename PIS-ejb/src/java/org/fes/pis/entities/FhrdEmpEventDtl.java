/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_EMP_EVENT_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdEmpEventDtl.findAll", query = "SELECT f FROM FhrdEmpEventDtl f"),
    @NamedQuery(name = "FhrdEmpEventDtl.findByOumUnitSrno", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.oumUnitSrno = :oumUnitSrno"),
    @NamedQuery(name = "FhrdEmpEventDtl.findBySrno", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.srno = :srno"),
    @NamedQuery(name = "FhrdEmpEventDtl.findByEventPeriodNo", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.eventPeriodNo = :eventPeriodNo"),
    @NamedQuery(name = "FhrdEmpEventDtl.findByEventPeriodUnit", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.eventPeriodUnit = :eventPeriodUnit"),
    @NamedQuery(name = "FhrdEmpEventDtl.findByEventDate", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.eventDate = :eventDate"),
    @NamedQuery(name = "FhrdEmpEventDtl.findByEventExecuteDate", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.eventExecuteDate = :eventExecuteDate"),
    @NamedQuery(name = "FhrdEmpEventDtl.findByEventFirstAlertDate", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.eventFirstAlertDate = :eventFirstAlertDate"),
    @NamedQuery(name = "FhrdEmpEventDtl.findByEventTwoAlertsGapNo", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.eventTwoAlertsGapNo = :eventTwoAlertsGapNo"),
    @NamedQuery(name = "FhrdEmpEventDtl.findByEventTwoAlertsGapUnit", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.eventTwoAlertsGapUnit = :eventTwoAlertsGapUnit"),
    @NamedQuery(name = "FhrdEmpEventDtl.findByEventHandling", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.eventHandling = :eventHandling"),
    @NamedQuery(name = "FhrdEmpEventDtl.findByEventStatusFlag", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.eventStatusFlag = :eventStatusFlag"),
    @NamedQuery(name = "FhrdEmpEventDtl.findByEventDependColName", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.eventDependColName = :eventDependColName"),
    @NamedQuery(name = "FhrdEmpEventDtl.findByEventDependColValue", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.eventDependColValue = :eventDependColValue"),
    @NamedQuery(name = "FhrdEmpEventDtl.findByCreatedt", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdEmpEventDtl.findByUpdatedt", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdEmpEventDtl.findBySystemUserid", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdEmpEventDtl.findByExportFlag", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdEmpEventDtl.findByImportFlag", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdEmpEventDtl.findByEventDocRefNo", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.eventDocRefNo = :eventDocRefNo"),
    @NamedQuery(name = "FhrdEmpEventDtl.findByEmpedSrgKey", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.empedSrgKey = :empedSrgKey"),
    @NamedQuery(name = "FhrdEmpEventDtl.findByEventEffectTime", query = "SELECT f FROM FhrdEmpEventDtl f WHERE f.eventEffectTime = :eventEffectTime")})
public class FhrdEmpEventDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OUM_UNIT_SRNO")
    private int oumUnitSrno;
    @Column(name = "SRNO")
    private Integer srno;
    @Column(name = "EVENT_PERIOD_NO")
    private Integer eventPeriodNo;
    @Size(max = 1)
    @Column(name = "EVENT_PERIOD_UNIT")
    private String eventPeriodUnit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EVENT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventDate;
    @Column(name = "EVENT_EXECUTE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventExecuteDate;
    @Column(name = "EVENT_FIRST_ALERT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date eventFirstAlertDate;
    @Column(name = "EVENT_TWO_ALERTS_GAP_NO")
    private Integer eventTwoAlertsGapNo;
    @Size(max = 1)
    @Column(name = "EVENT_TWO_ALERTS_GAP_UNIT")
    private String eventTwoAlertsGapUnit;
    @Size(max = 1)
    @Column(name = "EVENT_HANDLING")
    private String eventHandling;
    @Size(max = 1)
    @Column(name = "EVENT_STATUS_FLAG")
    private String eventStatusFlag;
    @Size(max = 30)
    @Column(name = "EVENT_DEPEND_COL_NAME")
    private String eventDependColName;
    @Column(name = "EVENT_DEPEND_COL_VALUE")
    private Integer eventDependColValue;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Size(max = 100)
    @Column(name = "EVENT_DOC_REF_NO")
    private String eventDocRefNo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMPED_SRG_KEY")
    private BigDecimal empedSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EVENT_EFFECT_TIME")
    private String eventEffectTime;
    @JoinColumn(name = "EM_SRG_KEY", referencedColumnName = "EM_SRG_KEY")
    @ManyToOne
    private FhrdEventMst emSrgKey;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst userId;

    public FhrdEmpEventDtl() {
    }

    public FhrdEmpEventDtl(BigDecimal empedSrgKey) {
        this.empedSrgKey = empedSrgKey;
    }

    public FhrdEmpEventDtl(BigDecimal empedSrgKey, int oumUnitSrno, Date eventDate, Date createdt, String eventEffectTime) {
        this.empedSrgKey = empedSrgKey;
        this.oumUnitSrno = oumUnitSrno;
        this.eventDate = eventDate;
        this.createdt = createdt;
        this.eventEffectTime = eventEffectTime;
    }

    public int getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(int oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public Integer getSrno() {
        return srno;
    }

    public void setSrno(Integer srno) {
        this.srno = srno;
    }

    public Integer getEventPeriodNo() {
        return eventPeriodNo;
    }

    public void setEventPeriodNo(Integer eventPeriodNo) {
        this.eventPeriodNo = eventPeriodNo;
    }

    public String getEventPeriodUnit() {
        return eventPeriodUnit;
    }

    public void setEventPeriodUnit(String eventPeriodUnit) {
        this.eventPeriodUnit = eventPeriodUnit;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public Date getEventExecuteDate() {
        return eventExecuteDate;
    }

    public void setEventExecuteDate(Date eventExecuteDate) {
        this.eventExecuteDate = eventExecuteDate;
    }

    public Date getEventFirstAlertDate() {
        return eventFirstAlertDate;
    }

    public void setEventFirstAlertDate(Date eventFirstAlertDate) {
        this.eventFirstAlertDate = eventFirstAlertDate;
    }

    public Integer getEventTwoAlertsGapNo() {
        return eventTwoAlertsGapNo;
    }

    public void setEventTwoAlertsGapNo(Integer eventTwoAlertsGapNo) {
        this.eventTwoAlertsGapNo = eventTwoAlertsGapNo;
    }

    public String getEventTwoAlertsGapUnit() {
        return eventTwoAlertsGapUnit;
    }

    public void setEventTwoAlertsGapUnit(String eventTwoAlertsGapUnit) {
        this.eventTwoAlertsGapUnit = eventTwoAlertsGapUnit;
    }

    public String getEventHandling() {
        return eventHandling;
    }

    public void setEventHandling(String eventHandling) {
        this.eventHandling = eventHandling;
    }

    public String getEventStatusFlag() {
        return eventStatusFlag;
    }

    public void setEventStatusFlag(String eventStatusFlag) {
        this.eventStatusFlag = eventStatusFlag;
    }

    public String getEventDependColName() {
        return eventDependColName;
    }

    public void setEventDependColName(String eventDependColName) {
        this.eventDependColName = eventDependColName;
    }

    public Integer getEventDependColValue() {
        return eventDependColValue;
    }

    public void setEventDependColValue(Integer eventDependColValue) {
        this.eventDependColValue = eventDependColValue;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public String getEventDocRefNo() {
        return eventDocRefNo;
    }

    public void setEventDocRefNo(String eventDocRefNo) {
        this.eventDocRefNo = eventDocRefNo;
    }

    public BigDecimal getEmpedSrgKey() {
        return empedSrgKey;
    }

    public void setEmpedSrgKey(BigDecimal empedSrgKey) {
        this.empedSrgKey = empedSrgKey;
    }

    public String getEventEffectTime() {
        return eventEffectTime;
    }

    public void setEventEffectTime(String eventEffectTime) {
        this.eventEffectTime = eventEffectTime;
    }

    public FhrdEventMst getEmSrgKey() {
        return emSrgKey;
    }

    public void setEmSrgKey(FhrdEventMst emSrgKey) {
        this.emSrgKey = emSrgKey;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUserId() {
        return userId;
    }

    public void setUserId(FhrdEmpmst userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (empedSrgKey != null ? empedSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdEmpEventDtl)) {
            return false;
        }
        FhrdEmpEventDtl other = (FhrdEmpEventDtl) object;
        if ((this.empedSrgKey == null && other.empedSrgKey != null) || (this.empedSrgKey != null && !this.empedSrgKey.equals(other.empedSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdEmpEventDtl[ empedSrgKey=" + empedSrgKey + " ]";
    }
    
}
