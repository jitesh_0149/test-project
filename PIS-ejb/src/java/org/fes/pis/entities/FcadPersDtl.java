/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FCAD_PERS_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FcadPersDtl.findAll", query = "SELECT f FROM FcadPersDtl f"),
    @NamedQuery(name = "FcadPersDtl.findByPdUniqueSrno", query = "SELECT f FROM FcadPersDtl f WHERE f.pdUniqueSrno = :pdUniqueSrno"),
    @NamedQuery(name = "FcadPersDtl.findByLevelno", query = "SELECT f FROM FcadPersDtl f WHERE f.levelno = :levelno"),
    @NamedQuery(name = "FcadPersDtl.findByName", query = "SELECT f FROM FcadPersDtl f WHERE f.name = :name"),
    @NamedQuery(name = "FcadPersDtl.findByDesig", query = "SELECT f FROM FcadPersDtl f WHERE f.desig = :desig"),
    @NamedQuery(name = "FcadPersDtl.findByTitle", query = "SELECT f FROM FcadPersDtl f WHERE f.title = :title"),
    @NamedQuery(name = "FcadPersDtl.findByEmail", query = "SELECT f FROM FcadPersDtl f WHERE f.email = :email"),
    @NamedQuery(name = "FcadPersDtl.findByCreatedt", query = "SELECT f FROM FcadPersDtl f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FcadPersDtl.findByUpdatedt", query = "SELECT f FROM FcadPersDtl f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FcadPersDtl.findBySystemUserid", query = "SELECT f FROM FcadPersDtl f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FcadPersDtl.findByExportFlag", query = "SELECT f FROM FcadPersDtl f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FcadPersDtl.findByImportFlag", query = "SELECT f FROM FcadPersDtl f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FcadPersDtl.findByStartDate", query = "SELECT f FROM FcadPersDtl f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FcadPersDtl.findByEndDate", query = "SELECT f FROM FcadPersDtl f WHERE f.endDate = :endDate")})
public class FcadPersDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PD_UNIQUE_SRNO")
    private Long pdUniqueSrno;
    @Column(name = "LEVELNO")
    private Short levelno;
    @Size(max = 30)
    @Column(name = "NAME")
    private String name;
    @Size(max = 30)
    @Column(name = "DESIG")
    private String desig;
    @Size(max = 6)
    @Column(name = "TITLE")
    private String title;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 50)
    @Column(name = "EMAIL")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @OneToMany(mappedBy = "pdUniqueSrno")
    private Collection<FcadTeleFax> fcadTeleFaxCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "AM_UNIQUE_SRNO", referencedColumnName = "AM_UNIQUE_SRNO")
    @ManyToOne(optional = false)
    private FcadAddrMst amUniqueSrno;

    public FcadPersDtl() {
    }

    public FcadPersDtl(Long pdUniqueSrno) {
        this.pdUniqueSrno = pdUniqueSrno;
    }

    public FcadPersDtl(Long pdUniqueSrno, Date createdt, Date startDate) {
        this.pdUniqueSrno = pdUniqueSrno;
        this.createdt = createdt;
        this.startDate = startDate;
    }

    public Long getPdUniqueSrno() {
        return pdUniqueSrno;
    }

    public void setPdUniqueSrno(Long pdUniqueSrno) {
        this.pdUniqueSrno = pdUniqueSrno;
    }

    public Short getLevelno() {
        return levelno;
    }

    public void setLevelno(Short levelno) {
        this.levelno = levelno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesig() {
        return desig;
    }

    public void setDesig(String desig) {
        this.desig = desig;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @XmlTransient
    public Collection<FcadTeleFax> getFcadTeleFaxCollection() {
        return fcadTeleFaxCollection;
    }

    public void setFcadTeleFaxCollection(Collection<FcadTeleFax> fcadTeleFaxCollection) {
        this.fcadTeleFaxCollection = fcadTeleFaxCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FcadAddrMst getAmUniqueSrno() {
        return amUniqueSrno;
    }

    public void setAmUniqueSrno(FcadAddrMst amUniqueSrno) {
        this.amUniqueSrno = amUniqueSrno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pdUniqueSrno != null ? pdUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FcadPersDtl)) {
            return false;
        }
        FcadPersDtl other = (FcadPersDtl) object;
        if ((this.pdUniqueSrno == null && other.pdUniqueSrno != null) || (this.pdUniqueSrno != null && !this.pdUniqueSrno.equals(other.pdUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FcadPersDtl[ pdUniqueSrno=" + pdUniqueSrno + " ]";
    }
    
}
