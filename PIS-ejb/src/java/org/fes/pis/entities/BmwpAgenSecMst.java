/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "BMWP_AGEN_SEC_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmwpAgenSecMst.findAll", query = "SELECT b FROM BmwpAgenSecMst b"),
    @NamedQuery(name = "BmwpAgenSecMst.findByAsmUniqueSrno", query = "SELECT b FROM BmwpAgenSecMst b WHERE b.asmUniqueSrno = :asmUniqueSrno"),
    @NamedQuery(name = "BmwpAgenSecMst.findByAsmShortDesc", query = "SELECT b FROM BmwpAgenSecMst b WHERE b.asmShortDesc = :asmShortDesc"),
    @NamedQuery(name = "BmwpAgenSecMst.findByAsmTypeDesc", query = "SELECT b FROM BmwpAgenSecMst b WHERE b.asmTypeDesc = :asmTypeDesc"),
    @NamedQuery(name = "BmwpAgenSecMst.findByAsmStartDate", query = "SELECT b FROM BmwpAgenSecMst b WHERE b.asmStartDate = :asmStartDate"),
    @NamedQuery(name = "BmwpAgenSecMst.findByAsmEndDate", query = "SELECT b FROM BmwpAgenSecMst b WHERE b.asmEndDate = :asmEndDate"),
    @NamedQuery(name = "BmwpAgenSecMst.findByAsmEndDatePossible", query = "SELECT b FROM BmwpAgenSecMst b WHERE b.asmEndDatePossible = :asmEndDatePossible"),
    @NamedQuery(name = "BmwpAgenSecMst.findByAsmTransactionDate", query = "SELECT b FROM BmwpAgenSecMst b WHERE b.asmTransactionDate = :asmTransactionDate"),
    @NamedQuery(name = "BmwpAgenSecMst.findByCreatedt", query = "SELECT b FROM BmwpAgenSecMst b WHERE b.createdt = :createdt"),
    @NamedQuery(name = "BmwpAgenSecMst.findByUpdatedt", query = "SELECT b FROM BmwpAgenSecMst b WHERE b.updatedt = :updatedt"),
    @NamedQuery(name = "BmwpAgenSecMst.findBySystemUserid", query = "SELECT b FROM BmwpAgenSecMst b WHERE b.systemUserid = :systemUserid"),
    @NamedQuery(name = "BmwpAgenSecMst.findByExportFlag", query = "SELECT b FROM BmwpAgenSecMst b WHERE b.exportFlag = :exportFlag"),
    @NamedQuery(name = "BmwpAgenSecMst.findByImportFlag", query = "SELECT b FROM BmwpAgenSecMst b WHERE b.importFlag = :importFlag")})
public class BmwpAgenSecMst implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ASM_UNIQUE_SRNO")
    private BigDecimal asmUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "ASM_SHORT_DESC")
    private String asmShortDesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "ASM_TYPE_DESC")
    private String asmTypeDesc;
    @Column(name = "ASM_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date asmStartDate;
    @Column(name = "ASM_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date asmEndDate;
    @Column(name = "ASM_END_DATE_POSSIBLE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date asmEndDatePossible;
    @Column(name = "ASM_TRANSACTION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date asmTransactionDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @OneToMany(mappedBy = "famAsmUniqueSrno")
    private Collection<BmwpFundAgencyMst> bmwpFundAgencyMstCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;

    public BmwpAgenSecMst() {
    }

    public BmwpAgenSecMst(BigDecimal asmUniqueSrno) {
        this.asmUniqueSrno = asmUniqueSrno;
    }

    public BmwpAgenSecMst(BigDecimal asmUniqueSrno, String asmShortDesc, String asmTypeDesc, Date createdt) {
        this.asmUniqueSrno = asmUniqueSrno;
        this.asmShortDesc = asmShortDesc;
        this.asmTypeDesc = asmTypeDesc;
        this.createdt = createdt;
    }

    public BigDecimal getAsmUniqueSrno() {
        return asmUniqueSrno;
    }

    public void setAsmUniqueSrno(BigDecimal asmUniqueSrno) {
        this.asmUniqueSrno = asmUniqueSrno;
    }

    public String getAsmShortDesc() {
        return asmShortDesc;
    }

    public void setAsmShortDesc(String asmShortDesc) {
        this.asmShortDesc = asmShortDesc;
    }

    public String getAsmTypeDesc() {
        return asmTypeDesc;
    }

    public void setAsmTypeDesc(String asmTypeDesc) {
        this.asmTypeDesc = asmTypeDesc;
    }

    public Date getAsmStartDate() {
        return asmStartDate;
    }

    public void setAsmStartDate(Date asmStartDate) {
        this.asmStartDate = asmStartDate;
    }

    public Date getAsmEndDate() {
        return asmEndDate;
    }

    public void setAsmEndDate(Date asmEndDate) {
        this.asmEndDate = asmEndDate;
    }

    public Date getAsmEndDatePossible() {
        return asmEndDatePossible;
    }

    public void setAsmEndDatePossible(Date asmEndDatePossible) {
        this.asmEndDatePossible = asmEndDatePossible;
    }

    public Date getAsmTransactionDate() {
        return asmTransactionDate;
    }

    public void setAsmTransactionDate(Date asmTransactionDate) {
        this.asmTransactionDate = asmTransactionDate;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    @XmlTransient
    public Collection<BmwpFundAgencyMst> getBmwpFundAgencyMstCollection() {
        return bmwpFundAgencyMstCollection;
    }

    public void setBmwpFundAgencyMstCollection(Collection<BmwpFundAgencyMst> bmwpFundAgencyMstCollection) {
        this.bmwpFundAgencyMstCollection = bmwpFundAgencyMstCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (asmUniqueSrno != null ? asmUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmwpAgenSecMst)) {
            return false;
        }
        BmwpAgenSecMst other = (BmwpAgenSecMst) object;
        if ((this.asmUniqueSrno == null && other.asmUniqueSrno != null) || (this.asmUniqueSrno != null && !this.asmUniqueSrno.equals(other.asmUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.BmwpAgenSecMst[ asmUniqueSrno=" + asmUniqueSrno + " ]";
    }
    
}
