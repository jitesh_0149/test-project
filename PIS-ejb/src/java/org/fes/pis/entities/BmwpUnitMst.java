/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "BMWP_UNIT_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmwpUnitMst.findAll", query = "SELECT b FROM BmwpUnitMst b"),
    @NamedQuery(name = "BmwpUnitMst.findByUmUniqueSrno", query = "SELECT b FROM BmwpUnitMst b WHERE b.umUniqueSrno = :umUniqueSrno"),
    @NamedQuery(name = "BmwpUnitMst.findByUmUnitSrno", query = "SELECT b FROM BmwpUnitMst b WHERE b.umUnitSrno = :umUnitSrno"),
    @NamedQuery(name = "BmwpUnitMst.findByUmUnitShortDesc", query = "SELECT b FROM BmwpUnitMst b WHERE b.umUnitShortDesc = :umUnitShortDesc"),
    @NamedQuery(name = "BmwpUnitMst.findByUmUnitDesc", query = "SELECT b FROM BmwpUnitMst b WHERE b.umUnitDesc = :umUnitDesc"),
    @NamedQuery(name = "BmwpUnitMst.findByUmRemarks", query = "SELECT b FROM BmwpUnitMst b WHERE b.umRemarks = :umRemarks"),
    @NamedQuery(name = "BmwpUnitMst.findByCreatedt", query = "SELECT b FROM BmwpUnitMst b WHERE b.createdt = :createdt"),
    @NamedQuery(name = "BmwpUnitMst.findByUpdatedt", query = "SELECT b FROM BmwpUnitMst b WHERE b.updatedt = :updatedt"),
    @NamedQuery(name = "BmwpUnitMst.findBySystemUserid", query = "SELECT b FROM BmwpUnitMst b WHERE b.systemUserid = :systemUserid"),
    @NamedQuery(name = "BmwpUnitMst.findByExportFlag", query = "SELECT b FROM BmwpUnitMst b WHERE b.exportFlag = :exportFlag"),
    @NamedQuery(name = "BmwpUnitMst.findByImportFlag", query = "SELECT b FROM BmwpUnitMst b WHERE b.importFlag = :importFlag")})
public class BmwpUnitMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "UM_UNIQUE_SRNO")
    private Long umUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "UM_UNIT_SRNO")
    private int umUnitSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "UM_UNIT_SHORT_DESC")
    private String umUnitShortDesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "UM_UNIT_DESC")
    private String umUnitDesc;
    @Size(max = 256)
    @Column(name = "UM_REMARKS")
    private String umRemarks;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "amUmUniqueUnitSrno")
    private Collection<BmwpActivityMst> bmwpActivityMstCollection;
    @OneToMany(mappedBy = "wpActivityUmUniqueSrno")
    private Collection<BmwpWorkplan> bmwpWorkplanCollection;

    public BmwpUnitMst() {
    }

    public BmwpUnitMst(Long umUniqueSrno) {
        this.umUniqueSrno = umUniqueSrno;
    }

    public BmwpUnitMst(Long umUniqueSrno, int umUnitSrno, String umUnitShortDesc, String umUnitDesc, Date createdt) {
        this.umUniqueSrno = umUniqueSrno;
        this.umUnitSrno = umUnitSrno;
        this.umUnitShortDesc = umUnitShortDesc;
        this.umUnitDesc = umUnitDesc;
        this.createdt = createdt;
    }

    public Long getUmUniqueSrno() {
        return umUniqueSrno;
    }

    public void setUmUniqueSrno(Long umUniqueSrno) {
        this.umUniqueSrno = umUniqueSrno;
    }

    public int getUmUnitSrno() {
        return umUnitSrno;
    }

    public void setUmUnitSrno(int umUnitSrno) {
        this.umUnitSrno = umUnitSrno;
    }

    public String getUmUnitShortDesc() {
        return umUnitShortDesc;
    }

    public void setUmUnitShortDesc(String umUnitShortDesc) {
        this.umUnitShortDesc = umUnitShortDesc;
    }

    public String getUmUnitDesc() {
        return umUnitDesc;
    }

    public void setUmUnitDesc(String umUnitDesc) {
        this.umUnitDesc = umUnitDesc;
    }

    public String getUmRemarks() {
        return umRemarks;
    }

    public void setUmRemarks(String umRemarks) {
        this.umRemarks = umRemarks;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @XmlTransient
    public Collection<BmwpActivityMst> getBmwpActivityMstCollection() {
        return bmwpActivityMstCollection;
    }

    public void setBmwpActivityMstCollection(Collection<BmwpActivityMst> bmwpActivityMstCollection) {
        this.bmwpActivityMstCollection = bmwpActivityMstCollection;
    }

    @XmlTransient
    public Collection<BmwpWorkplan> getBmwpWorkplanCollection() {
        return bmwpWorkplanCollection;
    }

    public void setBmwpWorkplanCollection(Collection<BmwpWorkplan> bmwpWorkplanCollection) {
        this.bmwpWorkplanCollection = bmwpWorkplanCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (umUniqueSrno != null ? umUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmwpUnitMst)) {
            return false;
        }
        BmwpUnitMst other = (BmwpUnitMst) object;
        if ((this.umUniqueSrno == null && other.umUniqueSrno != null) || (this.umUniqueSrno != null && !this.umUniqueSrno.equals(other.umUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.BmwpUnitMst[ umUniqueSrno=" + umUniqueSrno + " ]";
    }
    
}
