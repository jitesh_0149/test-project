/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author anu
 */
@Embeddable
public class SystMenumstPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 13)
    @Column(name = "MENUID")
    private String menuid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OUM_UNIT_SRNO")
    private int oumUnitSrno;

    public SystMenumstPK() {
    }

    public SystMenumstPK(String menuid, int oumUnitSrno) {
        this.menuid = menuid;
        this.oumUnitSrno = oumUnitSrno;
    }

    public String getMenuid() {
        return menuid;
    }

    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }

    public int getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(int oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (menuid != null ? menuid.hashCode() : 0);
        hash += (int) oumUnitSrno;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SystMenumstPK)) {
            return false;
        }
        SystMenumstPK other = (SystMenumstPK) object;
        if ((this.menuid == null && other.menuid != null) || (this.menuid != null && !this.menuid.equals(other.menuid))) {
            return false;
        }
        if (this.oumUnitSrno != other.oumUnitSrno) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.SystMenumstPK[ menuid=" + menuid + ", oumUnitSrno=" + oumUnitSrno + " ]";
    }
    
}
