/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "BMWP_AGENCY_PROJECT_LINK")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmwpAgencyProjectLink.findAll", query = "SELECT b FROM BmwpAgencyProjectLink b"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplUniqueSrno", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplUniqueSrno = :aplUniqueSrno"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplSrno", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplSrno = :aplSrno"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplStartDate", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplStartDate = :aplStartDate"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplEndDate", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplEndDate = :aplEndDate"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplMouFundAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplMouFundAmt = :aplMouFundAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplReceivedFundAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplReceivedFundAmt = :aplReceivedFundAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplToReceivedFundAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplToReceivedFundAmt = :aplToReceivedFundAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplSurrenderFundAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplSurrenderFundAmt = :aplSurrenderFundAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplTotalFundAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplTotalFundAmt = :aplTotalFundAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplWorkplanAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplWorkplanAmt = :aplWorkplanAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplWpSurrenderAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplWpSurrenderAmt = :aplWpSurrenderAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplReAllotWpAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplReAllotWpAmt = :aplReAllotWpAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplOfferWpAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplOfferWpAmt = :aplOfferWpAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplReviseWpAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplReviseWpAmt = :aplReviseWpAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplCommitedAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplCommitedAmt = :aplCommitedAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplPipelinePayAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplPipelinePayAmt = :aplPipelinePayAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplAdvanceCommitAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplAdvanceCommitAmt = :aplAdvanceCommitAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplAdvancePayAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplAdvancePayAmt = :aplAdvancePayAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplPipeAdvadjCommitAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplPipeAdvadjCommitAmt = :aplPipeAdvadjCommitAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplPipeAdvadjPayAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplPipeAdvadjPayAmt = :aplPipeAdvadjPayAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplReceiptPayAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplReceiptPayAmt = :aplReceiptPayAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplReceiptCommitAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplReceiptCommitAmt = :aplReceiptCommitAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplExpenseAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplExpenseAmt = :aplExpenseAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplUsableBalanceAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplUsableBalanceAmt = :aplUsableBalanceAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplBalanceAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplBalanceAmt = :aplBalanceAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByCreatedt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.createdt = :createdt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByUpdatedt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.updatedt = :updatedt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findBySystemUserid", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.systemUserid = :systemUserid"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByExportFlag", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.exportFlag = :exportFlag"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByImportFlag", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.importFlag = :importFlag"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplPipeCommitedAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplPipeCommitedAmt = :aplPipeCommitedAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplEndDatePossible", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplEndDatePossible = :aplEndDatePossible"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByMisApcSrno", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.misApcSrno = :misApcSrno"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplTransactionDate", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplTransactionDate = :aplTransactionDate"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplProCrExpAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplProCrExpAmt = :aplProCrExpAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplReceivedAdvanceAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplReceivedAdvanceAmt = :aplReceivedAdvanceAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplCarryFwdAdvAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplCarryFwdAdvAmt = :aplCarryFwdAdvAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplCommContriAmt", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplCommContriAmt = :aplCommContriAmt"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplPmFundMonitorFlag", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplPmFundMonitorFlag = :aplPmFundMonitorFlag"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByPmFundType", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.pmFundType = :pmFundType"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplMouVersion", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplMouVersion = :aplMouVersion"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplShortName", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplShortName = :aplShortName"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplNabardExclFlag", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplNabardExclFlag = :aplNabardExclFlag"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplNabardCbpFlag", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplNabardCbpFlag = :aplNabardCbpFlag"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplNabardFsrFlag", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplNabardFsrFlag = :aplNabardFsrFlag"),
    @NamedQuery(name = "BmwpAgencyProjectLink.findByAplNabardFipFlag", query = "SELECT b FROM BmwpAgencyProjectLink b WHERE b.aplNabardFipFlag = :aplNabardFipFlag")})
public class BmwpAgencyProjectLink implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_UNIQUE_SRNO")
    private BigDecimal aplUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_SRNO")
    private int aplSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date aplStartDate;
    @Column(name = "APL_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date aplEndDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_MOU_FUND_AMT")
    private BigDecimal aplMouFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_RECEIVED_FUND_AMT")
    private BigDecimal aplReceivedFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_TO_RECEIVED_FUND_AMT")
    private BigDecimal aplToReceivedFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_SURRENDER_FUND_AMT")
    private BigDecimal aplSurrenderFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_TOTAL_FUND_AMT")
    private BigDecimal aplTotalFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_WORKPLAN_AMT")
    private BigDecimal aplWorkplanAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_WP_SURRENDER_AMT")
    private BigDecimal aplWpSurrenderAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_RE_ALLOT_WP_AMT")
    private BigDecimal aplReAllotWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_OFFER_WP_AMT")
    private BigDecimal aplOfferWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_REVISE_WP_AMT")
    private BigDecimal aplReviseWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_COMMITED_AMT")
    private BigDecimal aplCommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_PIPELINE_PAY_AMT")
    private BigDecimal aplPipelinePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_ADVANCE_COMMIT_AMT")
    private BigDecimal aplAdvanceCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_ADVANCE_PAY_AMT")
    private BigDecimal aplAdvancePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_PIPE_ADVADJ_COMMIT_AMT")
    private BigDecimal aplPipeAdvadjCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_PIPE_ADVADJ_PAY_AMT")
    private BigDecimal aplPipeAdvadjPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_RECEIPT_PAY_AMT")
    private BigDecimal aplReceiptPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_RECEIPT_COMMIT_AMT")
    private BigDecimal aplReceiptCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_EXPENSE_AMT")
    private BigDecimal aplExpenseAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_USABLE_BALANCE_AMT")
    private BigDecimal aplUsableBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_BALANCE_AMT")
    private BigDecimal aplBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_PIPE_COMMITED_AMT")
    private BigDecimal aplPipeCommitedAmt;
    @Column(name = "APL_END_DATE_POSSIBLE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date aplEndDatePossible;
    @Column(name = "MIS_APC_SRNO")
    private Integer misApcSrno;
    @Column(name = "APL_TRANSACTION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date aplTransactionDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_PRO_CR_EXP_AMT")
    private BigDecimal aplProCrExpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_RECEIVED_ADVANCE_AMT")
    private BigDecimal aplReceivedAdvanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_CARRY_FWD_ADV_AMT")
    private BigDecimal aplCarryFwdAdvAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APL_COMM_CONTRI_AMT")
    private BigDecimal aplCommContriAmt;
    @Size(max = 1)
    @Column(name = "APL_PM_FUND_MONITOR_FLAG")
    private String aplPmFundMonitorFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PM_FUND_TYPE")
    private String pmFundType;
    @Column(name = "APL_MOU_VERSION")
    private Short aplMouVersion;
    @Size(max = 41)
    @Column(name = "APL_SHORT_NAME")
    private String aplShortName;
    @Size(max = 1)
    @Column(name = "APL_NABARD_EXCL_FLAG")
    private String aplNabardExclFlag;
    @Size(max = 1)
    @Column(name = "APL_NABARD_CBP_FLAG")
    private String aplNabardCbpFlag;
    @Size(max = 1)
    @Column(name = "APL_NABARD_FSR_FLAG")
    private String aplNabardFsrFlag;
    @Size(max = 1)
    @Column(name = "APL_NABARD_FIP_FLAG")
    private String aplNabardFipFlag;
    @OneToMany(mappedBy = "apablAplUniqueSrno")
    private Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "APL_PM_UNIQUE_SRNO", referencedColumnName = "PM_UNIQUE_SRNO")
    @ManyToOne(optional = false)
    private BmwpProjectMst aplPmUniqueSrno;
    @JoinColumn(name = "APL_FAM_UNIQUE_SRNO", referencedColumnName = "FAM_UNIQUE_SRNO")
    @ManyToOne(optional = false)
    private BmwpFundAgencyMst aplFamUniqueSrno;
    @JoinColumn(name = "APL_ATM_UNIQUE_SRNO", referencedColumnName = "ATM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpAgencyTypeMst aplAtmUniqueSrno;
    @JoinColumn(name = "APL_APC_UNIQUE_SRNO", referencedColumnName = "APC_UNIQUE_SRNO")
    @ManyToOne
    private BmwpAgencyProjectCategory aplApcUniqueSrno;
    @OneToMany(mappedBy = "apabdAplUniqueSrno")
    private Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "apamAplUniqueSrno")
    private Collection<BmwpApActivityMst> bmwpApActivityMstCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "apbmAplUniqueSrno")
    private Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection;
    @OneToMany(mappedBy = "wpAplUniqueSrno")
    private Collection<BmwpWorkplan> bmwpWorkplanCollection;

    public BmwpAgencyProjectLink() {
    }

    public BmwpAgencyProjectLink(BigDecimal aplUniqueSrno) {
        this.aplUniqueSrno = aplUniqueSrno;
    }

    public BmwpAgencyProjectLink(BigDecimal aplUniqueSrno, int aplSrno, Date aplStartDate, BigDecimal aplMouFundAmt, BigDecimal aplReceivedFundAmt, BigDecimal aplToReceivedFundAmt, BigDecimal aplSurrenderFundAmt, BigDecimal aplTotalFundAmt, BigDecimal aplWorkplanAmt, BigDecimal aplWpSurrenderAmt, BigDecimal aplReAllotWpAmt, BigDecimal aplOfferWpAmt, BigDecimal aplReviseWpAmt, BigDecimal aplCommitedAmt, BigDecimal aplPipelinePayAmt, BigDecimal aplAdvanceCommitAmt, BigDecimal aplAdvancePayAmt, BigDecimal aplPipeAdvadjCommitAmt, BigDecimal aplPipeAdvadjPayAmt, BigDecimal aplReceiptPayAmt, BigDecimal aplReceiptCommitAmt, BigDecimal aplExpenseAmt, BigDecimal aplUsableBalanceAmt, BigDecimal aplBalanceAmt, Date createdt, BigDecimal aplPipeCommitedAmt, BigDecimal aplProCrExpAmt, BigDecimal aplReceivedAdvanceAmt, BigDecimal aplCarryFwdAdvAmt, BigDecimal aplCommContriAmt, String pmFundType) {
        this.aplUniqueSrno = aplUniqueSrno;
        this.aplSrno = aplSrno;
        this.aplStartDate = aplStartDate;
        this.aplMouFundAmt = aplMouFundAmt;
        this.aplReceivedFundAmt = aplReceivedFundAmt;
        this.aplToReceivedFundAmt = aplToReceivedFundAmt;
        this.aplSurrenderFundAmt = aplSurrenderFundAmt;
        this.aplTotalFundAmt = aplTotalFundAmt;
        this.aplWorkplanAmt = aplWorkplanAmt;
        this.aplWpSurrenderAmt = aplWpSurrenderAmt;
        this.aplReAllotWpAmt = aplReAllotWpAmt;
        this.aplOfferWpAmt = aplOfferWpAmt;
        this.aplReviseWpAmt = aplReviseWpAmt;
        this.aplCommitedAmt = aplCommitedAmt;
        this.aplPipelinePayAmt = aplPipelinePayAmt;
        this.aplAdvanceCommitAmt = aplAdvanceCommitAmt;
        this.aplAdvancePayAmt = aplAdvancePayAmt;
        this.aplPipeAdvadjCommitAmt = aplPipeAdvadjCommitAmt;
        this.aplPipeAdvadjPayAmt = aplPipeAdvadjPayAmt;
        this.aplReceiptPayAmt = aplReceiptPayAmt;
        this.aplReceiptCommitAmt = aplReceiptCommitAmt;
        this.aplExpenseAmt = aplExpenseAmt;
        this.aplUsableBalanceAmt = aplUsableBalanceAmt;
        this.aplBalanceAmt = aplBalanceAmt;
        this.createdt = createdt;
        this.aplPipeCommitedAmt = aplPipeCommitedAmt;
        this.aplProCrExpAmt = aplProCrExpAmt;
        this.aplReceivedAdvanceAmt = aplReceivedAdvanceAmt;
        this.aplCarryFwdAdvAmt = aplCarryFwdAdvAmt;
        this.aplCommContriAmt = aplCommContriAmt;
        this.pmFundType = pmFundType;
    }

    public BigDecimal getAplUniqueSrno() {
        return aplUniqueSrno;
    }

    public void setAplUniqueSrno(BigDecimal aplUniqueSrno) {
        this.aplUniqueSrno = aplUniqueSrno;
    }

    public int getAplSrno() {
        return aplSrno;
    }

    public void setAplSrno(int aplSrno) {
        this.aplSrno = aplSrno;
    }

    public Date getAplStartDate() {
        return aplStartDate;
    }

    public void setAplStartDate(Date aplStartDate) {
        this.aplStartDate = aplStartDate;
    }

    public Date getAplEndDate() {
        return aplEndDate;
    }

    public void setAplEndDate(Date aplEndDate) {
        this.aplEndDate = aplEndDate;
    }

    public BigDecimal getAplMouFundAmt() {
        return aplMouFundAmt;
    }

    public void setAplMouFundAmt(BigDecimal aplMouFundAmt) {
        this.aplMouFundAmt = aplMouFundAmt;
    }

    public BigDecimal getAplReceivedFundAmt() {
        return aplReceivedFundAmt;
    }

    public void setAplReceivedFundAmt(BigDecimal aplReceivedFundAmt) {
        this.aplReceivedFundAmt = aplReceivedFundAmt;
    }

    public BigDecimal getAplToReceivedFundAmt() {
        return aplToReceivedFundAmt;
    }

    public void setAplToReceivedFundAmt(BigDecimal aplToReceivedFundAmt) {
        this.aplToReceivedFundAmt = aplToReceivedFundAmt;
    }

    public BigDecimal getAplSurrenderFundAmt() {
        return aplSurrenderFundAmt;
    }

    public void setAplSurrenderFundAmt(BigDecimal aplSurrenderFundAmt) {
        this.aplSurrenderFundAmt = aplSurrenderFundAmt;
    }

    public BigDecimal getAplTotalFundAmt() {
        return aplTotalFundAmt;
    }

    public void setAplTotalFundAmt(BigDecimal aplTotalFundAmt) {
        this.aplTotalFundAmt = aplTotalFundAmt;
    }

    public BigDecimal getAplWorkplanAmt() {
        return aplWorkplanAmt;
    }

    public void setAplWorkplanAmt(BigDecimal aplWorkplanAmt) {
        this.aplWorkplanAmt = aplWorkplanAmt;
    }

    public BigDecimal getAplWpSurrenderAmt() {
        return aplWpSurrenderAmt;
    }

    public void setAplWpSurrenderAmt(BigDecimal aplWpSurrenderAmt) {
        this.aplWpSurrenderAmt = aplWpSurrenderAmt;
    }

    public BigDecimal getAplReAllotWpAmt() {
        return aplReAllotWpAmt;
    }

    public void setAplReAllotWpAmt(BigDecimal aplReAllotWpAmt) {
        this.aplReAllotWpAmt = aplReAllotWpAmt;
    }

    public BigDecimal getAplOfferWpAmt() {
        return aplOfferWpAmt;
    }

    public void setAplOfferWpAmt(BigDecimal aplOfferWpAmt) {
        this.aplOfferWpAmt = aplOfferWpAmt;
    }

    public BigDecimal getAplReviseWpAmt() {
        return aplReviseWpAmt;
    }

    public void setAplReviseWpAmt(BigDecimal aplReviseWpAmt) {
        this.aplReviseWpAmt = aplReviseWpAmt;
    }

    public BigDecimal getAplCommitedAmt() {
        return aplCommitedAmt;
    }

    public void setAplCommitedAmt(BigDecimal aplCommitedAmt) {
        this.aplCommitedAmt = aplCommitedAmt;
    }

    public BigDecimal getAplPipelinePayAmt() {
        return aplPipelinePayAmt;
    }

    public void setAplPipelinePayAmt(BigDecimal aplPipelinePayAmt) {
        this.aplPipelinePayAmt = aplPipelinePayAmt;
    }

    public BigDecimal getAplAdvanceCommitAmt() {
        return aplAdvanceCommitAmt;
    }

    public void setAplAdvanceCommitAmt(BigDecimal aplAdvanceCommitAmt) {
        this.aplAdvanceCommitAmt = aplAdvanceCommitAmt;
    }

    public BigDecimal getAplAdvancePayAmt() {
        return aplAdvancePayAmt;
    }

    public void setAplAdvancePayAmt(BigDecimal aplAdvancePayAmt) {
        this.aplAdvancePayAmt = aplAdvancePayAmt;
    }

    public BigDecimal getAplPipeAdvadjCommitAmt() {
        return aplPipeAdvadjCommitAmt;
    }

    public void setAplPipeAdvadjCommitAmt(BigDecimal aplPipeAdvadjCommitAmt) {
        this.aplPipeAdvadjCommitAmt = aplPipeAdvadjCommitAmt;
    }

    public BigDecimal getAplPipeAdvadjPayAmt() {
        return aplPipeAdvadjPayAmt;
    }

    public void setAplPipeAdvadjPayAmt(BigDecimal aplPipeAdvadjPayAmt) {
        this.aplPipeAdvadjPayAmt = aplPipeAdvadjPayAmt;
    }

    public BigDecimal getAplReceiptPayAmt() {
        return aplReceiptPayAmt;
    }

    public void setAplReceiptPayAmt(BigDecimal aplReceiptPayAmt) {
        this.aplReceiptPayAmt = aplReceiptPayAmt;
    }

    public BigDecimal getAplReceiptCommitAmt() {
        return aplReceiptCommitAmt;
    }

    public void setAplReceiptCommitAmt(BigDecimal aplReceiptCommitAmt) {
        this.aplReceiptCommitAmt = aplReceiptCommitAmt;
    }

    public BigDecimal getAplExpenseAmt() {
        return aplExpenseAmt;
    }

    public void setAplExpenseAmt(BigDecimal aplExpenseAmt) {
        this.aplExpenseAmt = aplExpenseAmt;
    }

    public BigDecimal getAplUsableBalanceAmt() {
        return aplUsableBalanceAmt;
    }

    public void setAplUsableBalanceAmt(BigDecimal aplUsableBalanceAmt) {
        this.aplUsableBalanceAmt = aplUsableBalanceAmt;
    }

    public BigDecimal getAplBalanceAmt() {
        return aplBalanceAmt;
    }

    public void setAplBalanceAmt(BigDecimal aplBalanceAmt) {
        this.aplBalanceAmt = aplBalanceAmt;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public BigDecimal getAplPipeCommitedAmt() {
        return aplPipeCommitedAmt;
    }

    public void setAplPipeCommitedAmt(BigDecimal aplPipeCommitedAmt) {
        this.aplPipeCommitedAmt = aplPipeCommitedAmt;
    }

    public Date getAplEndDatePossible() {
        return aplEndDatePossible;
    }

    public void setAplEndDatePossible(Date aplEndDatePossible) {
        this.aplEndDatePossible = aplEndDatePossible;
    }

    public Integer getMisApcSrno() {
        return misApcSrno;
    }

    public void setMisApcSrno(Integer misApcSrno) {
        this.misApcSrno = misApcSrno;
    }

    public Date getAplTransactionDate() {
        return aplTransactionDate;
    }

    public void setAplTransactionDate(Date aplTransactionDate) {
        this.aplTransactionDate = aplTransactionDate;
    }

    public BigDecimal getAplProCrExpAmt() {
        return aplProCrExpAmt;
    }

    public void setAplProCrExpAmt(BigDecimal aplProCrExpAmt) {
        this.aplProCrExpAmt = aplProCrExpAmt;
    }

    public BigDecimal getAplReceivedAdvanceAmt() {
        return aplReceivedAdvanceAmt;
    }

    public void setAplReceivedAdvanceAmt(BigDecimal aplReceivedAdvanceAmt) {
        this.aplReceivedAdvanceAmt = aplReceivedAdvanceAmt;
    }

    public BigDecimal getAplCarryFwdAdvAmt() {
        return aplCarryFwdAdvAmt;
    }

    public void setAplCarryFwdAdvAmt(BigDecimal aplCarryFwdAdvAmt) {
        this.aplCarryFwdAdvAmt = aplCarryFwdAdvAmt;
    }

    public BigDecimal getAplCommContriAmt() {
        return aplCommContriAmt;
    }

    public void setAplCommContriAmt(BigDecimal aplCommContriAmt) {
        this.aplCommContriAmt = aplCommContriAmt;
    }

    public String getAplPmFundMonitorFlag() {
        return aplPmFundMonitorFlag;
    }

    public void setAplPmFundMonitorFlag(String aplPmFundMonitorFlag) {
        this.aplPmFundMonitorFlag = aplPmFundMonitorFlag;
    }

    public String getPmFundType() {
        return pmFundType;
    }

    public void setPmFundType(String pmFundType) {
        this.pmFundType = pmFundType;
    }

    public Short getAplMouVersion() {
        return aplMouVersion;
    }

    public void setAplMouVersion(Short aplMouVersion) {
        this.aplMouVersion = aplMouVersion;
    }

    public String getAplShortName() {
        return aplShortName;
    }

    public void setAplShortName(String aplShortName) {
        this.aplShortName = aplShortName;
    }

    public String getAplNabardExclFlag() {
        return aplNabardExclFlag;
    }

    public void setAplNabardExclFlag(String aplNabardExclFlag) {
        this.aplNabardExclFlag = aplNabardExclFlag;
    }

    public String getAplNabardCbpFlag() {
        return aplNabardCbpFlag;
    }

    public void setAplNabardCbpFlag(String aplNabardCbpFlag) {
        this.aplNabardCbpFlag = aplNabardCbpFlag;
    }

    public String getAplNabardFsrFlag() {
        return aplNabardFsrFlag;
    }

    public void setAplNabardFsrFlag(String aplNabardFsrFlag) {
        this.aplNabardFsrFlag = aplNabardFsrFlag;
    }

    public String getAplNabardFipFlag() {
        return aplNabardFipFlag;
    }

    public void setAplNabardFipFlag(String aplNabardFipFlag) {
        this.aplNabardFipFlag = aplNabardFipFlag;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetLink> getBmwpApActivityBudgetLinkCollection() {
        return bmwpApActivityBudgetLinkCollection;
    }

    public void setBmwpApActivityBudgetLinkCollection(Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection) {
        this.bmwpApActivityBudgetLinkCollection = bmwpApActivityBudgetLinkCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public BmwpProjectMst getAplPmUniqueSrno() {
        return aplPmUniqueSrno;
    }

    public void setAplPmUniqueSrno(BmwpProjectMst aplPmUniqueSrno) {
        this.aplPmUniqueSrno = aplPmUniqueSrno;
    }

    public BmwpFundAgencyMst getAplFamUniqueSrno() {
        return aplFamUniqueSrno;
    }

    public void setAplFamUniqueSrno(BmwpFundAgencyMst aplFamUniqueSrno) {
        this.aplFamUniqueSrno = aplFamUniqueSrno;
    }

    public BmwpAgencyTypeMst getAplAtmUniqueSrno() {
        return aplAtmUniqueSrno;
    }

    public void setAplAtmUniqueSrno(BmwpAgencyTypeMst aplAtmUniqueSrno) {
        this.aplAtmUniqueSrno = aplAtmUniqueSrno;
    }

    public BmwpAgencyProjectCategory getAplApcUniqueSrno() {
        return aplApcUniqueSrno;
    }

    public void setAplApcUniqueSrno(BmwpAgencyProjectCategory aplApcUniqueSrno) {
        this.aplApcUniqueSrno = aplApcUniqueSrno;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetDtl> getBmwpApActivityBudgetDtlCollection() {
        return bmwpApActivityBudgetDtlCollection;
    }

    public void setBmwpApActivityBudgetDtlCollection(Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection) {
        this.bmwpApActivityBudgetDtlCollection = bmwpApActivityBudgetDtlCollection;
    }

    @XmlTransient
    public Collection<BmwpApActivityMst> getBmwpApActivityMstCollection() {
        return bmwpApActivityMstCollection;
    }

    public void setBmwpApActivityMstCollection(Collection<BmwpApActivityMst> bmwpApActivityMstCollection) {
        this.bmwpApActivityMstCollection = bmwpApActivityMstCollection;
    }

    @XmlTransient
    public Collection<BmwpApBudgetMst> getBmwpApBudgetMstCollection() {
        return bmwpApBudgetMstCollection;
    }

    public void setBmwpApBudgetMstCollection(Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection) {
        this.bmwpApBudgetMstCollection = bmwpApBudgetMstCollection;
    }

    @XmlTransient
    public Collection<BmwpWorkplan> getBmwpWorkplanCollection() {
        return bmwpWorkplanCollection;
    }

    public void setBmwpWorkplanCollection(Collection<BmwpWorkplan> bmwpWorkplanCollection) {
        this.bmwpWorkplanCollection = bmwpWorkplanCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (aplUniqueSrno != null ? aplUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmwpAgencyProjectLink)) {
            return false;
        }
        BmwpAgencyProjectLink other = (BmwpAgencyProjectLink) object;
        if ((this.aplUniqueSrno == null && other.aplUniqueSrno != null) || (this.aplUniqueSrno != null && !this.aplUniqueSrno.equals(other.aplUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.BmwpAgencyProjectLink[ aplUniqueSrno=" + aplUniqueSrno + " ]";
    }
    
}
