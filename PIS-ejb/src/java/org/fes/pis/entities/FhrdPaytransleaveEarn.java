/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_PAYTRANSLEAVE_EARN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdPaytransleaveEarn.findAll", query = "SELECT f FROM FhrdPaytransleaveEarn f"),
    @NamedQuery(name = "FhrdPaytransleaveEarn.findByOumUnitSrno", query = "SELECT f FROM FhrdPaytransleaveEarn f WHERE f.oumUnitSrno = :oumUnitSrno"),
    @NamedQuery(name = "FhrdPaytransleaveEarn.findByUserId", query = "SELECT f FROM FhrdPaytransleaveEarn f WHERE f.userId = :userId"),
    @NamedQuery(name = "FhrdPaytransleaveEarn.findByLeaveEarnableDays", query = "SELECT f FROM FhrdPaytransleaveEarn f WHERE f.leaveEarnableDays = :leaveEarnableDays"),
    @NamedQuery(name = "FhrdPaytransleaveEarn.findByLeaveEarned", query = "SELECT f FROM FhrdPaytransleaveEarn f WHERE f.leaveEarned = :leaveEarned"),
    @NamedQuery(name = "FhrdPaytransleaveEarn.findByPleSrgKey", query = "SELECT f FROM FhrdPaytransleaveEarn f WHERE f.pleSrgKey = :pleSrgKey"),
    @NamedQuery(name = "FhrdPaytransleaveEarn.findByCreateby", query = "SELECT f FROM FhrdPaytransleaveEarn f WHERE f.createby = :createby"),
    @NamedQuery(name = "FhrdPaytransleaveEarn.findByCreatedt", query = "SELECT f FROM FhrdPaytransleaveEarn f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdPaytransleaveEarn.findByUpdateby", query = "SELECT f FROM FhrdPaytransleaveEarn f WHERE f.updateby = :updateby"),
    @NamedQuery(name = "FhrdPaytransleaveEarn.findByUpdatedt", query = "SELECT f FROM FhrdPaytransleaveEarn f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdPaytransleaveEarn.findBySystemUserid", query = "SELECT f FROM FhrdPaytransleaveEarn f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdPaytransleaveEarn.findByExportFlag", query = "SELECT f FROM FhrdPaytransleaveEarn f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdPaytransleaveEarn.findByImportFlag", query = "SELECT f FROM FhrdPaytransleaveEarn f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdPaytransleaveEarn.findByTranYear", query = "SELECT f FROM FhrdPaytransleaveEarn f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FhrdPaytransleaveEarn.findByAttdSrgKey", query = "SELECT f FROM FhrdPaytransleaveEarn f WHERE f.attdSrgKey = :attdSrgKey")})
public class FhrdPaytransleaveEarn implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OUM_UNIT_SRNO")
    private int oumUnitSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_ID")
    private int userId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "LEAVE_EARNABLE_DAYS")
    private BigDecimal leaveEarnableDays;
    @Column(name = "LEAVE_EARNED")
    private BigDecimal leaveEarned;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PLE_SRG_KEY")
    private BigDecimal pleSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEBY")
    private int createby;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEBY")
    private Integer updateby;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATTD_SRG_KEY")
    private BigInteger attdSrgKey;
    @JoinColumn(name = "LM_SRG_KEY", referencedColumnName = "LM_SRG_KEY")
    @ManyToOne
    private FtasLeavemst lmSrgKey;
    @JoinColumn(name = "PLM_SRG_KEY", referencedColumnName = "PLM_SRG_KEY")
    @ManyToOne
    private FhrdPaytransleaveMst plmSrgKey;
    @JoinColumn(name = "ELR_SRG_KEY", referencedColumnName = "ELR_SRG_KEY")
    @ManyToOne
    private FhrdEmpleaverule elrSrgKey;

    public FhrdPaytransleaveEarn() {
    }

    public FhrdPaytransleaveEarn(BigDecimal pleSrgKey) {
        this.pleSrgKey = pleSrgKey;
    }

    public FhrdPaytransleaveEarn(BigDecimal pleSrgKey, int oumUnitSrno, int userId, int createby, Date createdt, short tranYear, BigInteger attdSrgKey) {
        this.pleSrgKey = pleSrgKey;
        this.oumUnitSrno = oumUnitSrno;
        this.userId = userId;
        this.createby = createby;
        this.createdt = createdt;
        this.tranYear = tranYear;
        this.attdSrgKey = attdSrgKey;
    }

    public int getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(int oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public BigDecimal getLeaveEarnableDays() {
        return leaveEarnableDays;
    }

    public void setLeaveEarnableDays(BigDecimal leaveEarnableDays) {
        this.leaveEarnableDays = leaveEarnableDays;
    }

    public BigDecimal getLeaveEarned() {
        return leaveEarned;
    }

    public void setLeaveEarned(BigDecimal leaveEarned) {
        this.leaveEarned = leaveEarned;
    }

    public BigDecimal getPleSrgKey() {
        return pleSrgKey;
    }

    public void setPleSrgKey(BigDecimal pleSrgKey) {
        this.pleSrgKey = pleSrgKey;
    }

    public int getCreateby() {
        return createby;
    }

    public void setCreateby(int createby) {
        this.createby = createby;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Integer getUpdateby() {
        return updateby;
    }

    public void setUpdateby(Integer updateby) {
        this.updateby = updateby;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public BigInteger getAttdSrgKey() {
        return attdSrgKey;
    }

    public void setAttdSrgKey(BigInteger attdSrgKey) {
        this.attdSrgKey = attdSrgKey;
    }

    public FtasLeavemst getLmSrgKey() {
        return lmSrgKey;
    }

    public void setLmSrgKey(FtasLeavemst lmSrgKey) {
        this.lmSrgKey = lmSrgKey;
    }

    public FhrdPaytransleaveMst getPlmSrgKey() {
        return plmSrgKey;
    }

    public void setPlmSrgKey(FhrdPaytransleaveMst plmSrgKey) {
        this.plmSrgKey = plmSrgKey;
    }

    public FhrdEmpleaverule getElrSrgKey() {
        return elrSrgKey;
    }

    public void setElrSrgKey(FhrdEmpleaverule elrSrgKey) {
        this.elrSrgKey = elrSrgKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pleSrgKey != null ? pleSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdPaytransleaveEarn)) {
            return false;
        }
        FhrdPaytransleaveEarn other = (FhrdPaytransleaveEarn) object;
        if ((this.pleSrgKey == null && other.pleSrgKey != null) || (this.pleSrgKey != null && !this.pleSrgKey.equals(other.pleSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdPaytransleaveEarn[ pleSrgKey=" + pleSrgKey + " ]";
    }
    
}
