/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_TRAINING_ANN_DETAIL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasTrainingAnnDetail.findAll", query = "SELECT f FROM FtasTrainingAnnDetail f"),
    @NamedQuery(name = "FtasTrainingAnnDetail.findByTradSrgKey", query = "SELECT f FROM FtasTrainingAnnDetail f WHERE f.tradSrgKey = :tradSrgKey"),
    @NamedQuery(name = "FtasTrainingAnnDetail.findByDescription", query = "SELECT f FROM FtasTrainingAnnDetail f WHERE f.description = :description"),
    @NamedQuery(name = "FtasTrainingAnnDetail.findByFromDatetime", query = "SELECT f FROM FtasTrainingAnnDetail f WHERE f.fromDatetime = :fromDatetime"),
    @NamedQuery(name = "FtasTrainingAnnDetail.findByToDatetime", query = "SELECT f FROM FtasTrainingAnnDetail f WHERE f.toDatetime = :toDatetime"),
    @NamedQuery(name = "FtasTrainingAnnDetail.findBySubSrno", query = "SELECT f FROM FtasTrainingAnnDetail f WHERE f.subSrno = :subSrno"),
    @NamedQuery(name = "FtasTrainingAnnDetail.findByTimezone", query = "SELECT f FROM FtasTrainingAnnDetail f WHERE f.timezone = :timezone"),
    @NamedQuery(name = "FtasTrainingAnnDetail.findByTranYear", query = "SELECT f FROM FtasTrainingAnnDetail f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FtasTrainingAnnDetail.findByCreatedt", query = "SELECT f FROM FtasTrainingAnnDetail f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasTrainingAnnDetail.findByImportFlag", query = "SELECT f FROM FtasTrainingAnnDetail f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FtasTrainingAnnDetail.findByExportFlag", query = "SELECT f FROM FtasTrainingAnnDetail f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FtasTrainingAnnDetail.findByUpdatedt", query = "SELECT f FROM FtasTrainingAnnDetail f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasTrainingAnnDetail.findBySystemUserid", query = "SELECT f FROM FtasTrainingAnnDetail f WHERE f.systemUserid = :systemUserid")})
public class FtasTrainingAnnDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAD_SRG_KEY")
    private BigDecimal tradSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FROM_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDatetime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TO_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date toDatetime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SUB_SRNO")
    private int subSrno;
    @Size(max = 10)
    @Column(name = "TIMEZONE")
    private String timezone;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "TRANN_SRG_KEY", referencedColumnName = "TRANN_SRG_KEY")
    @ManyToOne(optional = false)
    private FtasTrainingMst trannSrgKey;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FtasTrainingAnnDetail() {
    }

    public FtasTrainingAnnDetail(BigDecimal tradSrgKey) {
        this.tradSrgKey = tradSrgKey;
    }

    public FtasTrainingAnnDetail(BigDecimal tradSrgKey, String description, Date fromDatetime, Date toDatetime, int subSrno, short tranYear, Date createdt) {
        this.tradSrgKey = tradSrgKey;
        this.description = description;
        this.fromDatetime = fromDatetime;
        this.toDatetime = toDatetime;
        this.subSrno = subSrno;
        this.tranYear = tranYear;
        this.createdt = createdt;
    }

    public BigDecimal getTradSrgKey() {
        return tradSrgKey;
    }

    public void setTradSrgKey(BigDecimal tradSrgKey) {
        this.tradSrgKey = tradSrgKey;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getFromDatetime() {
        return fromDatetime;
    }

    public void setFromDatetime(Date fromDatetime) {
        this.fromDatetime = fromDatetime;
    }

    public Date getToDatetime() {
        return toDatetime;
    }

    public void setToDatetime(Date toDatetime) {
        this.toDatetime = toDatetime;
    }

    public int getSubSrno() {
        return subSrno;
    }

    public void setSubSrno(int subSrno) {
        this.subSrno = subSrno;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasTrainingMst getTrannSrgKey() {
        return trannSrgKey;
    }

    public void setTrannSrgKey(FtasTrainingMst trannSrgKey) {
        this.trannSrgKey = trannSrgKey;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tradSrgKey != null ? tradSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasTrainingAnnDetail)) {
            return false;
        }
        FtasTrainingAnnDetail other = (FtasTrainingAnnDetail) object;
        if ((this.tradSrgKey == null && other.tradSrgKey != null) || (this.tradSrgKey != null && !this.tradSrgKey.equals(other.tradSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasTrainingAnnDetail[ tradSrgKey=" + tradSrgKey + " ]";
    }
    
}
