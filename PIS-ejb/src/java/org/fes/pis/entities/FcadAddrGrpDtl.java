/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FCAD_ADDR_GRP_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FcadAddrGrpDtl.findAll", query = "SELECT f FROM FcadAddrGrpDtl f"),
    @NamedQuery(name = "FcadAddrGrpDtl.findByAgdUniqueSrno", query = "SELECT f FROM FcadAddrGrpDtl f WHERE f.agdUniqueSrno = :agdUniqueSrno"),
    @NamedQuery(name = "FcadAddrGrpDtl.findByAgdJobflag", query = "SELECT f FROM FcadAddrGrpDtl f WHERE f.agdJobflag = :agdJobflag"),
    @NamedQuery(name = "FcadAddrGrpDtl.findByCreatedt", query = "SELECT f FROM FcadAddrGrpDtl f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FcadAddrGrpDtl.findByUpdatedt", query = "SELECT f FROM FcadAddrGrpDtl f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FcadAddrGrpDtl.findBySystemUserid", query = "SELECT f FROM FcadAddrGrpDtl f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FcadAddrGrpDtl.findByExportFlag", query = "SELECT f FROM FcadAddrGrpDtl f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FcadAddrGrpDtl.findByImportFlag", query = "SELECT f FROM FcadAddrGrpDtl f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FcadAddrGrpDtl.findByNativeOu", query = "SELECT f FROM FcadAddrGrpDtl f WHERE f.nativeOu = :nativeOu"),
    @NamedQuery(name = "FcadAddrGrpDtl.findByRecordDeleteFlag", query = "SELECT f FROM FcadAddrGrpDtl f WHERE f.recordDeleteFlag = :recordDeleteFlag"),
    @NamedQuery(name = "FcadAddrGrpDtl.findByOrgSrno", query = "SELECT f FROM FcadAddrGrpDtl f WHERE f.orgSrno = :orgSrno"),
    @NamedQuery(name = "FcadAddrGrpDtl.findByStartDate", query = "SELECT f FROM FcadAddrGrpDtl f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FcadAddrGrpDtl.findByEndDate", query = "SELECT f FROM FcadAddrGrpDtl f WHERE f.endDate = :endDate")})
public class FcadAddrGrpDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "AGD_UNIQUE_SRNO")
    private Long agdUniqueSrno;
    @Column(name = "AGD_JOBFLAG")
    private Character agdJobflag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "NATIVE_OU")
    private Integer nativeOu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "RECORD_DELETE_FLAG")
    private String recordDeleteFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_SRNO")
    private int orgSrno;
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "AGD_AM_UNIQUE_SRNO", referencedColumnName = "AM_UNIQUE_SRNO")
    @ManyToOne(optional = false)
    private FcadAddrMst agdAmUniqueSrno;
    @JoinColumn(name = "AGD_AGM_UNIQUE_SRNO", referencedColumnName = "AGM_UNIQUE_SRNO")
    @ManyToOne(optional = false)
    private FcadAddrGrpMst agdAgmUniqueSrno;

    public FcadAddrGrpDtl() {
    }

    public FcadAddrGrpDtl(Long agdUniqueSrno) {
        this.agdUniqueSrno = agdUniqueSrno;
    }

    public FcadAddrGrpDtl(Long agdUniqueSrno, Date createdt, String recordDeleteFlag, int orgSrno) {
        this.agdUniqueSrno = agdUniqueSrno;
        this.createdt = createdt;
        this.recordDeleteFlag = recordDeleteFlag;
        this.orgSrno = orgSrno;
    }

    public Long getAgdUniqueSrno() {
        return agdUniqueSrno;
    }

    public void setAgdUniqueSrno(Long agdUniqueSrno) {
        this.agdUniqueSrno = agdUniqueSrno;
    }

    public Character getAgdJobflag() {
        return agdJobflag;
    }

    public void setAgdJobflag(Character agdJobflag) {
        this.agdJobflag = agdJobflag;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getNativeOu() {
        return nativeOu;
    }

    public void setNativeOu(Integer nativeOu) {
        this.nativeOu = nativeOu;
    }

    public String getRecordDeleteFlag() {
        return recordDeleteFlag;
    }

    public void setRecordDeleteFlag(String recordDeleteFlag) {
        this.recordDeleteFlag = recordDeleteFlag;
    }

    public int getOrgSrno() {
        return orgSrno;
    }

    public void setOrgSrno(int orgSrno) {
        this.orgSrno = orgSrno;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FcadAddrMst getAgdAmUniqueSrno() {
        return agdAmUniqueSrno;
    }

    public void setAgdAmUniqueSrno(FcadAddrMst agdAmUniqueSrno) {
        this.agdAmUniqueSrno = agdAmUniqueSrno;
    }

    public FcadAddrGrpMst getAgdAgmUniqueSrno() {
        return agdAgmUniqueSrno;
    }

    public void setAgdAgmUniqueSrno(FcadAddrGrpMst agdAgmUniqueSrno) {
        this.agdAgmUniqueSrno = agdAgmUniqueSrno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (agdUniqueSrno != null ? agdUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FcadAddrGrpDtl)) {
            return false;
        }
        FcadAddrGrpDtl other = (FcadAddrGrpDtl) object;
        if ((this.agdUniqueSrno == null && other.agdUniqueSrno != null) || (this.agdUniqueSrno != null && !this.agdUniqueSrno.equals(other.agdUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FcadAddrGrpDtl[ agdUniqueSrno=" + agdUniqueSrno + " ]";
    }
    
}
