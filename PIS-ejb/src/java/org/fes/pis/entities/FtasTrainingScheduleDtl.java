/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_TRAINING_SCHEDULE_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasTrainingScheduleDtl.findAll", query = "SELECT f FROM FtasTrainingScheduleDtl f"),
    @NamedQuery(name = "FtasTrainingScheduleDtl.findByFromDatetime", query = "SELECT f FROM FtasTrainingScheduleDtl f WHERE f.fromDatetime = :fromDatetime"),
    @NamedQuery(name = "FtasTrainingScheduleDtl.findByDescription", query = "SELECT f FROM FtasTrainingScheduleDtl f WHERE f.description = :description"),
    @NamedQuery(name = "FtasTrainingScheduleDtl.findByTimezone", query = "SELECT f FROM FtasTrainingScheduleDtl f WHERE f.timezone = :timezone"),
    @NamedQuery(name = "FtasTrainingScheduleDtl.findByCreatedt", query = "SELECT f FROM FtasTrainingScheduleDtl f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasTrainingScheduleDtl.findByUpdatedt", query = "SELECT f FROM FtasTrainingScheduleDtl f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasTrainingScheduleDtl.findByTrsdSrgKey", query = "SELECT f FROM FtasTrainingScheduleDtl f WHERE f.trsdSrgKey = :trsdSrgKey"),
    @NamedQuery(name = "FtasTrainingScheduleDtl.findBySubSrno", query = "SELECT f FROM FtasTrainingScheduleDtl f WHERE f.subSrno = :subSrno"),
    @NamedQuery(name = "FtasTrainingScheduleDtl.findByExportFlag", query = "SELECT f FROM FtasTrainingScheduleDtl f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FtasTrainingScheduleDtl.findByImportFlag", query = "SELECT f FROM FtasTrainingScheduleDtl f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FtasTrainingScheduleDtl.findBySystemUserid", query = "SELECT f FROM FtasTrainingScheduleDtl f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FtasTrainingScheduleDtl.findByToDatetime", query = "SELECT f FROM FtasTrainingScheduleDtl f WHERE f.toDatetime = :toDatetime"),
    @NamedQuery(name = "FtasTrainingScheduleDtl.findByTranYear", query = "SELECT f FROM FtasTrainingScheduleDtl f WHERE f.tranYear = :tranYear")})
public class FtasTrainingScheduleDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FROM_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDatetime;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "DESCRIPTION")
    private String description;
    @Size(max = 10)
    @Column(name = "TIMEZONE")
    private String timezone;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRSD_SRG_KEY")
    private BigDecimal trsdSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SUB_SRNO")
    private int subSrno;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Column(name = "TO_DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date toDatetime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "ABS_SRG_KEY", referencedColumnName = "ABS_SRG_KEY")
    @ManyToOne(optional = false)
    private FtasAbsentees absSrgKey;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FtasTrainingScheduleDtl() {
    }

    public FtasTrainingScheduleDtl(BigDecimal trsdSrgKey) {
        this.trsdSrgKey = trsdSrgKey;
    }

    public FtasTrainingScheduleDtl(BigDecimal trsdSrgKey, Date fromDatetime, String description, Date createdt, int subSrno, short tranYear) {
        this.trsdSrgKey = trsdSrgKey;
        this.fromDatetime = fromDatetime;
        this.description = description;
        this.createdt = createdt;
        this.subSrno = subSrno;
        this.tranYear = tranYear;
    }

    public Date getFromDatetime() {
        return fromDatetime;
    }

    public void setFromDatetime(Date fromDatetime) {
        this.fromDatetime = fromDatetime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public BigDecimal getTrsdSrgKey() {
        return trsdSrgKey;
    }

    public void setTrsdSrgKey(BigDecimal trsdSrgKey) {
        this.trsdSrgKey = trsdSrgKey;
    }

    public int getSubSrno() {
        return subSrno;
    }

    public void setSubSrno(int subSrno) {
        this.subSrno = subSrno;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public Date getToDatetime() {
        return toDatetime;
    }

    public void setToDatetime(Date toDatetime) {
        this.toDatetime = toDatetime;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasAbsentees getAbsSrgKey() {
        return absSrgKey;
    }

    public void setAbsSrgKey(FtasAbsentees absSrgKey) {
        this.absSrgKey = absSrgKey;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (trsdSrgKey != null ? trsdSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasTrainingScheduleDtl)) {
            return false;
        }
        FtasTrainingScheduleDtl other = (FtasTrainingScheduleDtl) object;
        if ((this.trsdSrgKey == null && other.trsdSrgKey != null) || (this.trsdSrgKey != null && !this.trsdSrgKey.equals(other.trsdSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasTrainingScheduleDtl[ trsdSrgKey=" + trsdSrgKey + " ]";
    }
    
}
