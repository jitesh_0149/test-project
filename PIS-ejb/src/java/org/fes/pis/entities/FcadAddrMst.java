/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FCAD_ADDR_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FcadAddrMst.findAll", query = "SELECT f FROM FcadAddrMst f"),
    @NamedQuery(name = "FcadAddrMst.findByAmUniqueSrno", query = "SELECT f FROM FcadAddrMst f WHERE f.amUniqueSrno = :amUniqueSrno"),
    @NamedQuery(name = "FcadAddrMst.findByAmOrgName", query = "SELECT f FROM FcadAddrMst f WHERE f.amOrgName = :amOrgName"),
    @NamedQuery(name = "FcadAddrMst.findByAmTitle", query = "SELECT f FROM FcadAddrMst f WHERE f.amTitle = :amTitle"),
    @NamedQuery(name = "FcadAddrMst.findByAmFirstName", query = "SELECT f FROM FcadAddrMst f WHERE f.amFirstName = :amFirstName"),
    @NamedQuery(name = "FcadAddrMst.findByAmMiddleName", query = "SELECT f FROM FcadAddrMst f WHERE f.amMiddleName = :amMiddleName"),
    @NamedQuery(name = "FcadAddrMst.findByAmLastName", query = "SELECT f FROM FcadAddrMst f WHERE f.amLastName = :amLastName"),
    @NamedQuery(name = "FcadAddrMst.findByCreatedt", query = "SELECT f FROM FcadAddrMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FcadAddrMst.findByUpdatedt", query = "SELECT f FROM FcadAddrMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FcadAddrMst.findBySystemUserid", query = "SELECT f FROM FcadAddrMst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FcadAddrMst.findByExportFlag", query = "SELECT f FROM FcadAddrMst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FcadAddrMst.findByImportFlag", query = "SELECT f FROM FcadAddrMst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FcadAddrMst.findByCreatebyOu", query = "SELECT f FROM FcadAddrMst f WHERE f.createbyOu = :createbyOu"),
    @NamedQuery(name = "FcadAddrMst.findByNativeOu", query = "SELECT f FROM FcadAddrMst f WHERE f.nativeOu = :nativeOu"),
    @NamedQuery(name = "FcadAddrMst.findByAmAddrType", query = "SELECT f FROM FcadAddrMst f WHERE f.amAddrType = :amAddrType"),
    @NamedQuery(name = "FcadAddrMst.findByAmUserManFlag", query = "SELECT f FROM FcadAddrMst f WHERE f.amUserManFlag = :amUserManFlag"),
    @NamedQuery(name = "FcadAddrMst.findByAmDesig", query = "SELECT f FROM FcadAddrMst f WHERE f.amDesig = :amDesig")})
public class FcadAddrMst implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "AM_UNIQUE_SRNO")
    private BigDecimal amUniqueSrno;
    @Size(max = 70)
    @Column(name = "AM_ORG_NAME")
    private String amOrgName;
    @Size(max = 20)
    @Column(name = "AM_TITLE")
    private String amTitle;
    @Size(max = 30)
    @Column(name = "AM_FIRST_NAME")
    private String amFirstName;
    @Size(max = 30)
    @Column(name = "AM_MIDDLE_NAME")
    private String amMiddleName;
    @Size(max = 30)
    @Column(name = "AM_LAST_NAME")
    private String amLastName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEBY_OU")
    private int createbyOu;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NATIVE_OU")
    private int nativeOu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AM_ADDR_TYPE")
    private String amAddrType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AM_USER_MAN_FLAG")
    private String amUserManFlag;
    @Size(max = 100)
    @Column(name = "AM_DESIG")
    private String amDesig;
    @OneToMany(mappedBy = "amUniqueSrno")
    private Collection<FcadTeleFax> fcadTeleFaxCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "agdAmUniqueSrno")
    private Collection<FcadAddrGrpDtl> fcadAddrGrpDtlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fcadAddrMst")
    private Collection<FcadAddrDtl> fcadAddrDtlCollection;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "famAddrno")
    private BmwpFundAgencyMst bmwpFundAgencyMst;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "amUniqueSrno")
    private Collection<FcadPersDtl> fcadPersDtlCollection;
    @OneToMany(mappedBy = "addressno")
    private Collection<FhrdEmpmst> fhrdEmpmstCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;

    public FcadAddrMst() {
    }

    public FcadAddrMst(BigDecimal amUniqueSrno) {
        this.amUniqueSrno = amUniqueSrno;
    }

    public FcadAddrMst(BigDecimal amUniqueSrno, Date createdt, int createbyOu, int nativeOu, String amAddrType, String amUserManFlag) {
        this.amUniqueSrno = amUniqueSrno;
        this.createdt = createdt;
        this.createbyOu = createbyOu;
        this.nativeOu = nativeOu;
        this.amAddrType = amAddrType;
        this.amUserManFlag = amUserManFlag;
    }

    public BigDecimal getAmUniqueSrno() {
        return amUniqueSrno;
    }

    public void setAmUniqueSrno(BigDecimal amUniqueSrno) {
        this.amUniqueSrno = amUniqueSrno;
    }

    public String getAmOrgName() {
        return amOrgName;
    }

    public void setAmOrgName(String amOrgName) {
        this.amOrgName = amOrgName;
    }

    public String getAmTitle() {
        return amTitle;
    }

    public void setAmTitle(String amTitle) {
        this.amTitle = amTitle;
    }

    public String getAmFirstName() {
        return amFirstName;
    }

    public void setAmFirstName(String amFirstName) {
        this.amFirstName = amFirstName;
    }

    public String getAmMiddleName() {
        return amMiddleName;
    }

    public void setAmMiddleName(String amMiddleName) {
        this.amMiddleName = amMiddleName;
    }

    public String getAmLastName() {
        return amLastName;
    }

    public void setAmLastName(String amLastName) {
        this.amLastName = amLastName;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public int getCreatebyOu() {
        return createbyOu;
    }

    public void setCreatebyOu(int createbyOu) {
        this.createbyOu = createbyOu;
    }

    public int getNativeOu() {
        return nativeOu;
    }

    public void setNativeOu(int nativeOu) {
        this.nativeOu = nativeOu;
    }

    public String getAmAddrType() {
        return amAddrType;
    }

    public void setAmAddrType(String amAddrType) {
        this.amAddrType = amAddrType;
    }

    public String getAmUserManFlag() {
        return amUserManFlag;
    }

    public void setAmUserManFlag(String amUserManFlag) {
        this.amUserManFlag = amUserManFlag;
    }

    public String getAmDesig() {
        return amDesig;
    }

    public void setAmDesig(String amDesig) {
        this.amDesig = amDesig;
    }

    @XmlTransient
    public Collection<FcadTeleFax> getFcadTeleFaxCollection() {
        return fcadTeleFaxCollection;
    }

    public void setFcadTeleFaxCollection(Collection<FcadTeleFax> fcadTeleFaxCollection) {
        this.fcadTeleFaxCollection = fcadTeleFaxCollection;
    }

    @XmlTransient
    public Collection<FcadAddrGrpDtl> getFcadAddrGrpDtlCollection() {
        return fcadAddrGrpDtlCollection;
    }

    public void setFcadAddrGrpDtlCollection(Collection<FcadAddrGrpDtl> fcadAddrGrpDtlCollection) {
        this.fcadAddrGrpDtlCollection = fcadAddrGrpDtlCollection;
    }

    @XmlTransient
    public Collection<FcadAddrDtl> getFcadAddrDtlCollection() {
        return fcadAddrDtlCollection;
    }

    public void setFcadAddrDtlCollection(Collection<FcadAddrDtl> fcadAddrDtlCollection) {
        this.fcadAddrDtlCollection = fcadAddrDtlCollection;
    }

    public BmwpFundAgencyMst getBmwpFundAgencyMst() {
        return bmwpFundAgencyMst;
    }

    public void setBmwpFundAgencyMst(BmwpFundAgencyMst bmwpFundAgencyMst) {
        this.bmwpFundAgencyMst = bmwpFundAgencyMst;
    }

    @XmlTransient
    public Collection<FcadPersDtl> getFcadPersDtlCollection() {
        return fcadPersDtlCollection;
    }

    public void setFcadPersDtlCollection(Collection<FcadPersDtl> fcadPersDtlCollection) {
        this.fcadPersDtlCollection = fcadPersDtlCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpmst> getFhrdEmpmstCollection() {
        return fhrdEmpmstCollection;
    }

    public void setFhrdEmpmstCollection(Collection<FhrdEmpmst> fhrdEmpmstCollection) {
        this.fhrdEmpmstCollection = fhrdEmpmstCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (amUniqueSrno != null ? amUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FcadAddrMst)) {
            return false;
        }
        FcadAddrMst other = (FcadAddrMst) object;
        if ((this.amUniqueSrno == null && other.amUniqueSrno != null) || (this.amUniqueSrno != null && !this.amUniqueSrno.equals(other.amUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FcadAddrMst[ amUniqueSrno=" + amUniqueSrno + " ]";
    }
    
}
