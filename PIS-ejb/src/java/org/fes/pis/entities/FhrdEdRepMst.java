/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_ED_REP_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdEdRepMst.findAll", query = "SELECT f FROM FhrdEdRepMst f"),
    @NamedQuery(name = "FhrdEdRepMst.findByErmSrgKey", query = "SELECT f FROM FhrdEdRepMst f WHERE f.ermSrgKey = :ermSrgKey"),
    @NamedQuery(name = "FhrdEdRepMst.findByErmName", query = "SELECT f FROM FhrdEdRepMst f WHERE f.ermName = :ermName"),
    @NamedQuery(name = "FhrdEdRepMst.findByCreatedt", query = "SELECT f FROM FhrdEdRepMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdEdRepMst.findByUpdatedt", query = "SELECT f FROM FhrdEdRepMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdEdRepMst.findByExportFlag", query = "SELECT f FROM FhrdEdRepMst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdEdRepMst.findByImportFlag", query = "SELECT f FROM FhrdEdRepMst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdEdRepMst.findBySystemUserid", query = "SELECT f FROM FhrdEdRepMst f WHERE f.systemUserid = :systemUserid")})
public class FhrdEdRepMst implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ERM_SRG_KEY")
    private BigDecimal ermSrgKey;
    @Size(max = 1000)
    @Column(name = "ERM_NAME")
    private String ermName;
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @OneToMany(mappedBy = "ermSrgKey")
    private Collection<FhrdEdRepDtl> fhrdEdRepDtlCollection;

    public FhrdEdRepMst() {
    }

    public FhrdEdRepMst(BigDecimal ermSrgKey) {
        this.ermSrgKey = ermSrgKey;
    }

    public BigDecimal getErmSrgKey() {
        return ermSrgKey;
    }

    public void setErmSrgKey(BigDecimal ermSrgKey) {
        this.ermSrgKey = ermSrgKey;
    }

    public String getErmName() {
        return ermName;
    }

    public void setErmName(String ermName) {
        this.ermName = ermName;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @XmlTransient
    public Collection<FhrdEdRepDtl> getFhrdEdRepDtlCollection() {
        return fhrdEdRepDtlCollection;
    }

    public void setFhrdEdRepDtlCollection(Collection<FhrdEdRepDtl> fhrdEdRepDtlCollection) {
        this.fhrdEdRepDtlCollection = fhrdEdRepDtlCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ermSrgKey != null ? ermSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdEdRepMst)) {
            return false;
        }
        FhrdEdRepMst other = (FhrdEdRepMst) object;
        if ((this.ermSrgKey == null && other.ermSrgKey != null) || (this.ermSrgKey != null && !this.ermSrgKey.equals(other.ermSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdEdRepMst[ ermSrgKey=" + ermSrgKey + " ]";
    }
    
}
