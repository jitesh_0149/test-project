/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "SYST_USER_TYPE_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SystUserTypeMst.findAll", query = "SELECT s FROM SystUserTypeMst s"),
    @NamedQuery(name = "SystUserTypeMst.findByOumUnitSrno", query = "SELECT s FROM SystUserTypeMst s WHERE s.systUserTypeMstPK.oumUnitSrno = :oumUnitSrno"),
    @NamedQuery(name = "SystUserTypeMst.findByUserTypeSrno", query = "SELECT s FROM SystUserTypeMst s WHERE s.systUserTypeMstPK.userTypeSrno = :userTypeSrno"),
    @NamedQuery(name = "SystUserTypeMst.findByUserTypeDesc", query = "SELECT s FROM SystUserTypeMst s WHERE s.userTypeDesc = :userTypeDesc"),
    @NamedQuery(name = "SystUserTypeMst.findByCreateby", query = "SELECT s FROM SystUserTypeMst s WHERE s.createby = :createby"),
    @NamedQuery(name = "SystUserTypeMst.findByCreatedt", query = "SELECT s FROM SystUserTypeMst s WHERE s.createdt = :createdt"),
    @NamedQuery(name = "SystUserTypeMst.findByUpdateby", query = "SELECT s FROM SystUserTypeMst s WHERE s.updateby = :updateby"),
    @NamedQuery(name = "SystUserTypeMst.findByUpdatedt", query = "SELECT s FROM SystUserTypeMst s WHERE s.updatedt = :updatedt"),
    @NamedQuery(name = "SystUserTypeMst.findByExportFlag", query = "SELECT s FROM SystUserTypeMst s WHERE s.exportFlag = :exportFlag"),
    @NamedQuery(name = "SystUserTypeMst.findByImportFlag", query = "SELECT s FROM SystUserTypeMst s WHERE s.importFlag = :importFlag"),
    @NamedQuery(name = "SystUserTypeMst.findBySystemUserid", query = "SELECT s FROM SystUserTypeMst s WHERE s.systemUserid = :systemUserid")})
public class SystUserTypeMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SystUserTypeMstPK systUserTypeMstPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "USER_TYPE_DESC")
    private String userTypeDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEBY")
    private int createby;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEBY")
    private Integer updateby;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "systUserTypeMst")
    private Collection<FhrdEmpmst> fhrdEmpmstCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private SystOrgUnitMst systOrgUnitMst;

    public SystUserTypeMst() {
    }

    public SystUserTypeMst(SystUserTypeMstPK systUserTypeMstPK) {
        this.systUserTypeMstPK = systUserTypeMstPK;
    }

    public SystUserTypeMst(SystUserTypeMstPK systUserTypeMstPK, String userTypeDesc, int createby, Date createdt) {
        this.systUserTypeMstPK = systUserTypeMstPK;
        this.userTypeDesc = userTypeDesc;
        this.createby = createby;
        this.createdt = createdt;
    }

    public SystUserTypeMst(int oumUnitSrno, int userTypeSrno) {
        this.systUserTypeMstPK = new SystUserTypeMstPK(oumUnitSrno, userTypeSrno);
    }

    public SystUserTypeMstPK getSystUserTypeMstPK() {
        return systUserTypeMstPK;
    }

    public void setSystUserTypeMstPK(SystUserTypeMstPK systUserTypeMstPK) {
        this.systUserTypeMstPK = systUserTypeMstPK;
    }

    public String getUserTypeDesc() {
        return userTypeDesc;
    }

    public void setUserTypeDesc(String userTypeDesc) {
        this.userTypeDesc = userTypeDesc;
    }

    public int getCreateby() {
        return createby;
    }

    public void setCreateby(int createby) {
        this.createby = createby;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Integer getUpdateby() {
        return updateby;
    }

    public void setUpdateby(Integer updateby) {
        this.updateby = updateby;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    @XmlTransient
    public Collection<FhrdEmpmst> getFhrdEmpmstCollection() {
        return fhrdEmpmstCollection;
    }

    public void setFhrdEmpmstCollection(Collection<FhrdEmpmst> fhrdEmpmstCollection) {
        this.fhrdEmpmstCollection = fhrdEmpmstCollection;
    }

    public SystOrgUnitMst getSystOrgUnitMst() {
        return systOrgUnitMst;
    }

    public void setSystOrgUnitMst(SystOrgUnitMst systOrgUnitMst) {
        this.systOrgUnitMst = systOrgUnitMst;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (systUserTypeMstPK != null ? systUserTypeMstPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SystUserTypeMst)) {
            return false;
        }
        SystUserTypeMst other = (SystUserTypeMst) object;
        if ((this.systUserTypeMstPK == null && other.systUserTypeMstPK != null) || (this.systUserTypeMstPK != null && !this.systUserTypeMstPK.equals(other.systUserTypeMstPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.SystUserTypeMst[ systUserTypeMstPK=" + systUserTypeMstPK + " ]";
    }
    
}
