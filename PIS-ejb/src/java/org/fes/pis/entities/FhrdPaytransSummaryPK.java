/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 *
 * @author anu
 */
@Embeddable
public class FhrdPaytransSummaryPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "OUM_UNIT_SRNO")
    private int oumUnitSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_SALARY_FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgSalaryFromDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_SALARY_TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgSalaryToDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_ID")
    private int userId;

    public FhrdPaytransSummaryPK() {
    }

    public FhrdPaytransSummaryPK(int oumUnitSrno, Date orgSalaryFromDate, Date orgSalaryToDate, int userId) {
        this.oumUnitSrno = oumUnitSrno;
        this.orgSalaryFromDate = orgSalaryFromDate;
        this.orgSalaryToDate = orgSalaryToDate;
        this.userId = userId;
    }

    public int getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(int oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public Date getOrgSalaryFromDate() {
        return orgSalaryFromDate;
    }

    public void setOrgSalaryFromDate(Date orgSalaryFromDate) {
        this.orgSalaryFromDate = orgSalaryFromDate;
    }

    public Date getOrgSalaryToDate() {
        return orgSalaryToDate;
    }

    public void setOrgSalaryToDate(Date orgSalaryToDate) {
        this.orgSalaryToDate = orgSalaryToDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) oumUnitSrno;
        hash += (orgSalaryFromDate != null ? orgSalaryFromDate.hashCode() : 0);
        hash += (orgSalaryToDate != null ? orgSalaryToDate.hashCode() : 0);
        hash += (int) userId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdPaytransSummaryPK)) {
            return false;
        }
        FhrdPaytransSummaryPK other = (FhrdPaytransSummaryPK) object;
        if (this.oumUnitSrno != other.oumUnitSrno) {
            return false;
        }
        if ((this.orgSalaryFromDate == null && other.orgSalaryFromDate != null) || (this.orgSalaryFromDate != null && !this.orgSalaryFromDate.equals(other.orgSalaryFromDate))) {
            return false;
        }
        if ((this.orgSalaryToDate == null && other.orgSalaryToDate != null) || (this.orgSalaryToDate != null && !this.orgSalaryToDate.equals(other.orgSalaryToDate))) {
            return false;
        }
        if (this.userId != other.userId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdPaytransSummaryPK[ oumUnitSrno=" + oumUnitSrno + ", orgSalaryFromDate=" + orgSalaryFromDate + ", orgSalaryToDate=" + orgSalaryToDate + ", userId=" + userId + " ]";
    }
    
}
