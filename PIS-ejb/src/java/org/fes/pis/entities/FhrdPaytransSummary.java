/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_PAYTRANS_SUMMARY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdPaytransSummary.findAll", query = "SELECT f FROM FhrdPaytransSummary f"),
    @NamedQuery(name = "FhrdPaytransSummary.findByOumUnitSrno", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.fhrdPaytransSummaryPK.oumUnitSrno = :oumUnitSrno"),
    @NamedQuery(name = "FhrdPaytransSummary.findByOrgSalaryFromDate", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.fhrdPaytransSummaryPK.orgSalaryFromDate = :orgSalaryFromDate"),
    @NamedQuery(name = "FhrdPaytransSummary.findByOrgSalaryToDate", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.fhrdPaytransSummaryPK.orgSalaryToDate = :orgSalaryToDate"),
    @NamedQuery(name = "FhrdPaytransSummary.findByUserId", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.fhrdPaytransSummaryPK.userId = :userId"),
    @NamedQuery(name = "FhrdPaytransSummary.findByEmpPayableDays", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.empPayableDays = :empPayableDays"),
    @NamedQuery(name = "FhrdPaytransSummary.findByEmpWorkingDays", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.empWorkingDays = :empWorkingDays"),
    @NamedQuery(name = "FhrdPaytransSummary.findByTotalEarning", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.totalEarning = :totalEarning"),
    @NamedQuery(name = "FhrdPaytransSummary.findByTotalDeduction", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.totalDeduction = :totalDeduction"),
    @NamedQuery(name = "FhrdPaytransSummary.findByTotalReimbursment", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.totalReimbursment = :totalReimbursment"),
    @NamedQuery(name = "FhrdPaytransSummary.findByTotalEmployerContri", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.totalEmployerContri = :totalEmployerContri"),
    @NamedQuery(name = "FhrdPaytransSummary.findByGrossPay", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.grossPay = :grossPay"),
    @NamedQuery(name = "FhrdPaytransSummary.findByNetPay", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.netPay = :netPay"),
    @NamedQuery(name = "FhrdPaytransSummary.findByActualAttendance", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.actualAttendance = :actualAttendance"),
    @NamedQuery(name = "FhrdPaytransSummary.findByNoOfHolidays", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.noOfHolidays = :noOfHolidays"),
    @NamedQuery(name = "FhrdPaytransSummary.findByOrgAttendanceFromDate", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.orgAttendanceFromDate = :orgAttendanceFromDate"),
    @NamedQuery(name = "FhrdPaytransSummary.findByOrgAttendanceToDate", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.orgAttendanceToDate = :orgAttendanceToDate"),
    @NamedQuery(name = "FhrdPaytransSummary.findByEmpNonPayableDays", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.empNonPayableDays = :empNonPayableDays"),
    @NamedQuery(name = "FhrdPaytransSummary.findByConsiderAsAttendance", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.considerAsAttendance = :considerAsAttendance"),
    @NamedQuery(name = "FhrdPaytransSummary.findByDisburseDate", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.disburseDate = :disburseDate"),
    @NamedQuery(name = "FhrdPaytransSummary.findByTotalCto", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.totalCto = :totalCto"),
    @NamedQuery(name = "FhrdPaytransSummary.findByTotalPay", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.totalPay = :totalPay"),
    @NamedQuery(name = "FhrdPaytransSummary.findBySalarySrgKey", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.salarySrgKey = :salarySrgKey"),
    @NamedQuery(name = "FhrdPaytransSummary.findByDisburseFlag", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.disburseFlag = :disburseFlag"),
    @NamedQuery(name = "FhrdPaytransSummary.findByOrgPayableDays", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.orgPayableDays = :orgPayableDays"),
    @NamedQuery(name = "FhrdPaytransSummary.findByOrgWorkingDays", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.orgWorkingDays = :orgWorkingDays"),
    @NamedQuery(name = "FhrdPaytransSummary.findByNoOfLeaveEncash", query = "SELECT f FROM FhrdPaytransSummary f WHERE f.noOfLeaveEncash = :noOfLeaveEncash")})
public class FhrdPaytransSummary implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FhrdPaytransSummaryPK fhrdPaytransSummaryPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_PAYABLE_DAYS")
    private BigInteger empPayableDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_WORKING_DAYS")
    private BigInteger empWorkingDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_EARNING")
    private BigInteger totalEarning;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_DEDUCTION")
    private BigInteger totalDeduction;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_REIMBURSMENT")
    private BigInteger totalReimbursment;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TOTAL_EMPLOYER_CONTRI")
    private BigInteger totalEmployerContri;
    @Basic(optional = false)
    @NotNull
    @Column(name = "GROSS_PAY")
    private BigInteger grossPay;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NET_PAY")
    private BigInteger netPay;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTUAL_ATTENDANCE")
    private BigInteger actualAttendance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NO_OF_HOLIDAYS")
    private BigInteger noOfHolidays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_ATTENDANCE_FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgAttendanceFromDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_ATTENDANCE_TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgAttendanceToDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EMP_NON_PAYABLE_DAYS")
    private BigInteger empNonPayableDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONSIDER_AS_ATTENDANCE")
    private BigInteger considerAsAttendance;
    @Column(name = "DISBURSE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date disburseDate;
    @Column(name = "TOTAL_CTO")
    private BigInteger totalCto;
    @Column(name = "TOTAL_PAY")
    private BigInteger totalPay;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SALARY_SRG_KEY")
    private BigInteger salarySrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "DISBURSE_FLAG")
    private String disburseFlag;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_PAYABLE_DAYS")
    private BigDecimal orgPayableDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_WORKING_DAYS")
    private BigDecimal orgWorkingDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NO_OF_LEAVE_ENCASH")
    private BigDecimal noOfLeaveEncash;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private SystOrgUnitMst systOrgUnitMst;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FhrdEmpmst fhrdEmpmst;
    @JoinColumn(name = "DSGM_SRG_KEY", referencedColumnName = "DSGM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdDesigmst dsgmSrgKey;

    public FhrdPaytransSummary() {
    }

    public FhrdPaytransSummary(FhrdPaytransSummaryPK fhrdPaytransSummaryPK) {
        this.fhrdPaytransSummaryPK = fhrdPaytransSummaryPK;
    }

    public FhrdPaytransSummary(FhrdPaytransSummaryPK fhrdPaytransSummaryPK, BigInteger empPayableDays, BigInteger empWorkingDays, BigInteger totalEarning, BigInteger totalDeduction, BigInteger totalReimbursment, BigInteger totalEmployerContri, BigInteger grossPay, BigInteger netPay, BigInteger actualAttendance, BigInteger noOfHolidays, Date orgAttendanceFromDate, Date orgAttendanceToDate, BigInteger empNonPayableDays, BigInteger considerAsAttendance, BigInteger salarySrgKey, String disburseFlag, BigDecimal orgPayableDays, BigDecimal orgWorkingDays, BigDecimal noOfLeaveEncash) {
        this.fhrdPaytransSummaryPK = fhrdPaytransSummaryPK;
        this.empPayableDays = empPayableDays;
        this.empWorkingDays = empWorkingDays;
        this.totalEarning = totalEarning;
        this.totalDeduction = totalDeduction;
        this.totalReimbursment = totalReimbursment;
        this.totalEmployerContri = totalEmployerContri;
        this.grossPay = grossPay;
        this.netPay = netPay;
        this.actualAttendance = actualAttendance;
        this.noOfHolidays = noOfHolidays;
        this.orgAttendanceFromDate = orgAttendanceFromDate;
        this.orgAttendanceToDate = orgAttendanceToDate;
        this.empNonPayableDays = empNonPayableDays;
        this.considerAsAttendance = considerAsAttendance;
        this.salarySrgKey = salarySrgKey;
        this.disburseFlag = disburseFlag;
        this.orgPayableDays = orgPayableDays;
        this.orgWorkingDays = orgWorkingDays;
        this.noOfLeaveEncash = noOfLeaveEncash;
    }

    public FhrdPaytransSummary(int oumUnitSrno, Date orgSalaryFromDate, Date orgSalaryToDate, int userId) {
        this.fhrdPaytransSummaryPK = new FhrdPaytransSummaryPK(oumUnitSrno, orgSalaryFromDate, orgSalaryToDate, userId);
    }

    public FhrdPaytransSummaryPK getFhrdPaytransSummaryPK() {
        return fhrdPaytransSummaryPK;
    }

    public void setFhrdPaytransSummaryPK(FhrdPaytransSummaryPK fhrdPaytransSummaryPK) {
        this.fhrdPaytransSummaryPK = fhrdPaytransSummaryPK;
    }

    public BigInteger getEmpPayableDays() {
        return empPayableDays;
    }

    public void setEmpPayableDays(BigInteger empPayableDays) {
        this.empPayableDays = empPayableDays;
    }

    public BigInteger getEmpWorkingDays() {
        return empWorkingDays;
    }

    public void setEmpWorkingDays(BigInteger empWorkingDays) {
        this.empWorkingDays = empWorkingDays;
    }

    public BigInteger getTotalEarning() {
        return totalEarning;
    }

    public void setTotalEarning(BigInteger totalEarning) {
        this.totalEarning = totalEarning;
    }

    public BigInteger getTotalDeduction() {
        return totalDeduction;
    }

    public void setTotalDeduction(BigInteger totalDeduction) {
        this.totalDeduction = totalDeduction;
    }

    public BigInteger getTotalReimbursment() {
        return totalReimbursment;
    }

    public void setTotalReimbursment(BigInteger totalReimbursment) {
        this.totalReimbursment = totalReimbursment;
    }

    public BigInteger getTotalEmployerContri() {
        return totalEmployerContri;
    }

    public void setTotalEmployerContri(BigInteger totalEmployerContri) {
        this.totalEmployerContri = totalEmployerContri;
    }

    public BigInteger getGrossPay() {
        return grossPay;
    }

    public void setGrossPay(BigInteger grossPay) {
        this.grossPay = grossPay;
    }

    public BigInteger getNetPay() {
        return netPay;
    }

    public void setNetPay(BigInteger netPay) {
        this.netPay = netPay;
    }

    public BigInteger getActualAttendance() {
        return actualAttendance;
    }

    public void setActualAttendance(BigInteger actualAttendance) {
        this.actualAttendance = actualAttendance;
    }

    public BigInteger getNoOfHolidays() {
        return noOfHolidays;
    }

    public void setNoOfHolidays(BigInteger noOfHolidays) {
        this.noOfHolidays = noOfHolidays;
    }

    public Date getOrgAttendanceFromDate() {
        return orgAttendanceFromDate;
    }

    public void setOrgAttendanceFromDate(Date orgAttendanceFromDate) {
        this.orgAttendanceFromDate = orgAttendanceFromDate;
    }

    public Date getOrgAttendanceToDate() {
        return orgAttendanceToDate;
    }

    public void setOrgAttendanceToDate(Date orgAttendanceToDate) {
        this.orgAttendanceToDate = orgAttendanceToDate;
    }

    public BigInteger getEmpNonPayableDays() {
        return empNonPayableDays;
    }

    public void setEmpNonPayableDays(BigInteger empNonPayableDays) {
        this.empNonPayableDays = empNonPayableDays;
    }

    public BigInteger getConsiderAsAttendance() {
        return considerAsAttendance;
    }

    public void setConsiderAsAttendance(BigInteger considerAsAttendance) {
        this.considerAsAttendance = considerAsAttendance;
    }

    public Date getDisburseDate() {
        return disburseDate;
    }

    public void setDisburseDate(Date disburseDate) {
        this.disburseDate = disburseDate;
    }

    public BigInteger getTotalCto() {
        return totalCto;
    }

    public void setTotalCto(BigInteger totalCto) {
        this.totalCto = totalCto;
    }

    public BigInteger getTotalPay() {
        return totalPay;
    }

    public void setTotalPay(BigInteger totalPay) {
        this.totalPay = totalPay;
    }

    public BigInteger getSalarySrgKey() {
        return salarySrgKey;
    }

    public void setSalarySrgKey(BigInteger salarySrgKey) {
        this.salarySrgKey = salarySrgKey;
    }

    public String getDisburseFlag() {
        return disburseFlag;
    }

    public void setDisburseFlag(String disburseFlag) {
        this.disburseFlag = disburseFlag;
    }

    public BigDecimal getOrgPayableDays() {
        return orgPayableDays;
    }

    public void setOrgPayableDays(BigDecimal orgPayableDays) {
        this.orgPayableDays = orgPayableDays;
    }

    public BigDecimal getOrgWorkingDays() {
        return orgWorkingDays;
    }

    public void setOrgWorkingDays(BigDecimal orgWorkingDays) {
        this.orgWorkingDays = orgWorkingDays;
    }

    public BigDecimal getNoOfLeaveEncash() {
        return noOfLeaveEncash;
    }

    public void setNoOfLeaveEncash(BigDecimal noOfLeaveEncash) {
        this.noOfLeaveEncash = noOfLeaveEncash;
    }

    public SystOrgUnitMst getSystOrgUnitMst() {
        return systOrgUnitMst;
    }

    public void setSystOrgUnitMst(SystOrgUnitMst systOrgUnitMst) {
        this.systOrgUnitMst = systOrgUnitMst;
    }

    public FhrdEmpmst getFhrdEmpmst() {
        return fhrdEmpmst;
    }

    public void setFhrdEmpmst(FhrdEmpmst fhrdEmpmst) {
        this.fhrdEmpmst = fhrdEmpmst;
    }

    public FhrdDesigmst getDsgmSrgKey() {
        return dsgmSrgKey;
    }

    public void setDsgmSrgKey(FhrdDesigmst dsgmSrgKey) {
        this.dsgmSrgKey = dsgmSrgKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fhrdPaytransSummaryPK != null ? fhrdPaytransSummaryPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdPaytransSummary)) {
            return false;
        }
        FhrdPaytransSummary other = (FhrdPaytransSummary) object;
        if ((this.fhrdPaytransSummaryPK == null && other.fhrdPaytransSummaryPK != null) || (this.fhrdPaytransSummaryPK != null && !this.fhrdPaytransSummaryPK.equals(other.fhrdPaytransSummaryPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdPaytransSummary[ fhrdPaytransSummaryPK=" + fhrdPaytransSummaryPK + " ]";
    }
    
}
