/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_LEAVEMST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasLeavemst.findAll", query = "SELECT f FROM FtasLeavemst f"),
    @NamedQuery(name = "FtasLeavemst.findByLeavecd", query = "SELECT f FROM FtasLeavemst f WHERE f.leavecd = :leavecd"),
    @NamedQuery(name = "FtasLeavemst.findByLeaveDesc", query = "SELECT f FROM FtasLeavemst f WHERE f.leaveDesc = :leaveDesc"),
    @NamedQuery(name = "FtasLeavemst.findByStartDate", query = "SELECT f FROM FtasLeavemst f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FtasLeavemst.findByEndDate", query = "SELECT f FROM FtasLeavemst f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FtasLeavemst.findByLeaveFlag", query = "SELECT f FROM FtasLeavemst f WHERE f.leaveFlag = :leaveFlag"),
    @NamedQuery(name = "FtasLeavemst.findByConsiderAsAttendanceFlag", query = "SELECT f FROM FtasLeavemst f WHERE f.considerAsAttendanceFlag = :considerAsAttendanceFlag"),
    @NamedQuery(name = "FtasLeavemst.findByActualAttendanceFlag", query = "SELECT f FROM FtasLeavemst f WHERE f.actualAttendanceFlag = :actualAttendanceFlag"),
    @NamedQuery(name = "FtasLeavemst.findByPrintInPaySlipFlag", query = "SELECT f FROM FtasLeavemst f WHERE f.printInPaySlipFlag = :printInPaySlipFlag"),
    @NamedQuery(name = "FtasLeavemst.findByPrintOrder", query = "SELECT f FROM FtasLeavemst f WHERE f.printOrder = :printOrder"),
    @NamedQuery(name = "FtasLeavemst.findByVirtualLeave", query = "SELECT f FROM FtasLeavemst f WHERE f.virtualLeave = :virtualLeave"),
    @NamedQuery(name = "FtasLeavemst.findByCreatedt", query = "SELECT f FROM FtasLeavemst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasLeavemst.findByUpdatedt", query = "SELECT f FROM FtasLeavemst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasLeavemst.findByExportFlag", query = "SELECT f FROM FtasLeavemst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FtasLeavemst.findByImportFlag", query = "SELECT f FROM FtasLeavemst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FtasLeavemst.findBySystemUserid", query = "SELECT f FROM FtasLeavemst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FtasLeavemst.findByApplicationRequired", query = "SELECT f FROM FtasLeavemst f WHERE f.applicationRequired = :applicationRequired"),
    @NamedQuery(name = "FtasLeavemst.findByParentable", query = "SELECT f FROM FtasLeavemst f WHERE f.parentable = :parentable"),
    @NamedQuery(name = "FtasLeavemst.findByShortName", query = "SELECT f FROM FtasLeavemst f WHERE f.shortName = :shortName"),
    @NamedQuery(name = "FtasLeavemst.findByLmSrgKey", query = "SELECT f FROM FtasLeavemst f WHERE f.lmSrgKey = :lmSrgKey"),
    @NamedQuery(name = "FtasLeavemst.findByLmAltSrno", query = "SELECT f FROM FtasLeavemst f WHERE f.lmAltSrno = :lmAltSrno"),
    @NamedQuery(name = "FtasLeavemst.findByLmAltSubsrno", query = "SELECT f FROM FtasLeavemst f WHERE f.lmAltSubsrno = :lmAltSubsrno"),
    @NamedQuery(name = "FtasLeavemst.findByTTrParent", query = "SELECT f FROM FtasLeavemst f WHERE f.tTrParent = :tTrParent"),
    @NamedQuery(name = "FtasLeavemst.findByConsiderAsActualAttdFlag", query = "SELECT f FROM FtasLeavemst f WHERE f.considerAsActualAttdFlag = :considerAsActualAttdFlag"),
    @NamedQuery(name = "FtasLeavemst.findByHolidayFlag", query = "SELECT f FROM FtasLeavemst f WHERE f.holidayFlag = :holidayFlag"),
    @NamedQuery(name = "FtasLeavemst.findByMultipleRuleFlag", query = "SELECT f FROM FtasLeavemst f WHERE f.multipleRuleFlag = :multipleRuleFlag"),
    @NamedQuery(name = "FtasLeavemst.findByResignRelieveConsFlag", query = "SELECT f FROM FtasLeavemst f WHERE f.resignRelieveConsFlag = :resignRelieveConsFlag"),
    @NamedQuery(name = "FtasLeavemst.findByPrintIf0Cons", query = "SELECT f FROM FtasLeavemst f WHERE f.printIf0Cons = :printIf0Cons")})
public class FtasLeavemst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "LEAVECD")
    private String leavecd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "LEAVE_DESC")
    private String leaveDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "LEAVE_FLAG")
    private String leaveFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "CONSIDER_AS_ATTENDANCE_FLAG")
    private String considerAsAttendanceFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "ACTUAL_ATTENDANCE_FLAG")
    private String actualAttendanceFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PRINT_IN_PAY_SLIP_FLAG")
    private String printInPaySlipFlag;
    @Column(name = "PRINT_ORDER")
    private Short printOrder;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "VIRTUAL_LEAVE")
    private String virtualLeave;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "APPLICATION_REQUIRED")
    private String applicationRequired;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PARENTABLE")
    private String parentable;
    @Size(max = 4)
    @Column(name = "SHORT_NAME")
    private String shortName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "LM_SRG_KEY")
    private BigDecimal lmSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LM_ALT_SRNO")
    private int lmAltSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LM_ALT_SUBSRNO")
    private int lmAltSubsrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "T_TR_PARENT")
    private String tTrParent;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "CONSIDER_AS_ACTUAL_ATTD_FLAG")
    private String considerAsActualAttdFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "HOLIDAY_FLAG")
    private String holidayFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "MULTIPLE_RULE_FLAG")
    private String multipleRuleFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "RESIGN_RELIEVE_CONS_FLAG")
    private String resignRelieveConsFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PRINT_IF_0_CONS")
    private String printIf0Cons;
    @OneToMany(mappedBy = "lmSrgKey")
    private Collection<FhrdPaytransleaveEarn> fhrdPaytransleaveEarnCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lmSrgKey")
    private Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection;
    @OneToMany(mappedBy = "ttrLmSrgKey")
    private Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection1;
    @OneToMany(mappedBy = "parentLmSrgKey")
    private Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection2;
    @OneToMany(mappedBy = "childLmSrgKey")
    private Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection3;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "actualLmSrgKey")
    private Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection4;
    @OneToMany(mappedBy = "lmSrgKey")
    private Collection<FhrdPaytransleaveDtl> fhrdPaytransleaveDtlCollection;
    @OneToMany(mappedBy = "childHalf2LmSrgKey")
    private Collection<FtasDaily> ftasDailyCollection;
    @OneToMany(mappedBy = "ttrHalf1LmSrgKey")
    private Collection<FtasDaily> ftasDailyCollection1;
    @OneToMany(mappedBy = "half1LmSrgKey")
    private Collection<FtasDaily> ftasDailyCollection2;
    @OneToMany(mappedBy = "parHalf2LmSrgKey")
    private Collection<FtasDaily> ftasDailyCollection3;
    @OneToMany(mappedBy = "parHalf1LmSrgKey")
    private Collection<FtasDaily> ftasDailyCollection4;
    @OneToMany(mappedBy = "half2LmSrgKey")
    private Collection<FtasDaily> ftasDailyCollection5;
    @OneToMany(mappedBy = "childHalf1LmSrgKey")
    private Collection<FtasDaily> ftasDailyCollection6;
    @OneToMany(mappedBy = "pendingHalf2LmSrgKey")
    private Collection<FtasDaily> ftasDailyCollection7;
    @OneToMany(mappedBy = "actualHalf2LmSrgKey")
    private Collection<FtasDaily> ftasDailyCollection8;
    @OneToMany(mappedBy = "actualHalf1LmSrgKey")
    private Collection<FtasDaily> ftasDailyCollection9;
    @OneToMany(mappedBy = "pendingHalf1LmSrgKey")
    private Collection<FtasDaily> ftasDailyCollection10;
    @OneToMany(mappedBy = "ttrHalf2LmSrgKey")
    private Collection<FtasDaily> ftasDailyCollection11;
    @OneToMany(mappedBy = "lmSrgKey")
    private Collection<FhrdEmpleavebal> fhrdEmpleavebalCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lmSrgKey")
    private Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lmSrgKey")
    private Collection<FhrdEmpEncash> fhrdEmpEncashCollection;
    @OneToMany(mappedBy = "lmSrgKey")
    private Collection<FhrdLeaverule> fhrdLeaveruleCollection;

    public FtasLeavemst() {
    }

    public FtasLeavemst(BigDecimal lmSrgKey) {
        this.lmSrgKey = lmSrgKey;
    }

    public FtasLeavemst(BigDecimal lmSrgKey, String leavecd, String leaveDesc, Date startDate, String leaveFlag, String considerAsAttendanceFlag, String actualAttendanceFlag, String printInPaySlipFlag, String virtualLeave, Date createdt, String applicationRequired, String parentable, int lmAltSrno, int lmAltSubsrno, String tTrParent, String considerAsActualAttdFlag, String holidayFlag, String multipleRuleFlag, String resignRelieveConsFlag, String printIf0Cons) {
        this.lmSrgKey = lmSrgKey;
        this.leavecd = leavecd;
        this.leaveDesc = leaveDesc;
        this.startDate = startDate;
        this.leaveFlag = leaveFlag;
        this.considerAsAttendanceFlag = considerAsAttendanceFlag;
        this.actualAttendanceFlag = actualAttendanceFlag;
        this.printInPaySlipFlag = printInPaySlipFlag;
        this.virtualLeave = virtualLeave;
        this.createdt = createdt;
        this.applicationRequired = applicationRequired;
        this.parentable = parentable;
        this.lmAltSrno = lmAltSrno;
        this.lmAltSubsrno = lmAltSubsrno;
        this.tTrParent = tTrParent;
        this.considerAsActualAttdFlag = considerAsActualAttdFlag;
        this.holidayFlag = holidayFlag;
        this.multipleRuleFlag = multipleRuleFlag;
        this.resignRelieveConsFlag = resignRelieveConsFlag;
        this.printIf0Cons = printIf0Cons;
    }

    public String getLeavecd() {
        return leavecd;
    }

    public void setLeavecd(String leavecd) {
        this.leavecd = leavecd;
    }

    public String getLeaveDesc() {
        return leaveDesc;
    }

    public void setLeaveDesc(String leaveDesc) {
        this.leaveDesc = leaveDesc;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getLeaveFlag() {
        return leaveFlag;
    }

    public void setLeaveFlag(String leaveFlag) {
        this.leaveFlag = leaveFlag;
    }

    public String getConsiderAsAttendanceFlag() {
        return considerAsAttendanceFlag;
    }

    public void setConsiderAsAttendanceFlag(String considerAsAttendanceFlag) {
        this.considerAsAttendanceFlag = considerAsAttendanceFlag;
    }

    public String getActualAttendanceFlag() {
        return actualAttendanceFlag;
    }

    public void setActualAttendanceFlag(String actualAttendanceFlag) {
        this.actualAttendanceFlag = actualAttendanceFlag;
    }

    public String getPrintInPaySlipFlag() {
        return printInPaySlipFlag;
    }

    public void setPrintInPaySlipFlag(String printInPaySlipFlag) {
        this.printInPaySlipFlag = printInPaySlipFlag;
    }

    public Short getPrintOrder() {
        return printOrder;
    }

    public void setPrintOrder(Short printOrder) {
        this.printOrder = printOrder;
    }

    public String getVirtualLeave() {
        return virtualLeave;
    }

    public void setVirtualLeave(String virtualLeave) {
        this.virtualLeave = virtualLeave;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getApplicationRequired() {
        return applicationRequired;
    }

    public void setApplicationRequired(String applicationRequired) {
        this.applicationRequired = applicationRequired;
    }

    public String getParentable() {
        return parentable;
    }

    public void setParentable(String parentable) {
        this.parentable = parentable;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public BigDecimal getLmSrgKey() {
        return lmSrgKey;
    }

    public void setLmSrgKey(BigDecimal lmSrgKey) {
        this.lmSrgKey = lmSrgKey;
    }

    public int getLmAltSrno() {
        return lmAltSrno;
    }

    public void setLmAltSrno(int lmAltSrno) {
        this.lmAltSrno = lmAltSrno;
    }

    public int getLmAltSubsrno() {
        return lmAltSubsrno;
    }

    public void setLmAltSubsrno(int lmAltSubsrno) {
        this.lmAltSubsrno = lmAltSubsrno;
    }

    public String getTTrParent() {
        return tTrParent;
    }

    public void setTTrParent(String tTrParent) {
        this.tTrParent = tTrParent;
    }

    public String getConsiderAsActualAttdFlag() {
        return considerAsActualAttdFlag;
    }

    public void setConsiderAsActualAttdFlag(String considerAsActualAttdFlag) {
        this.considerAsActualAttdFlag = considerAsActualAttdFlag;
    }

    public String getHolidayFlag() {
        return holidayFlag;
    }

    public void setHolidayFlag(String holidayFlag) {
        this.holidayFlag = holidayFlag;
    }

    public String getMultipleRuleFlag() {
        return multipleRuleFlag;
    }

    public void setMultipleRuleFlag(String multipleRuleFlag) {
        this.multipleRuleFlag = multipleRuleFlag;
    }

    public String getResignRelieveConsFlag() {
        return resignRelieveConsFlag;
    }

    public void setResignRelieveConsFlag(String resignRelieveConsFlag) {
        this.resignRelieveConsFlag = resignRelieveConsFlag;
    }

    public String getPrintIf0Cons() {
        return printIf0Cons;
    }

    public void setPrintIf0Cons(String printIf0Cons) {
        this.printIf0Cons = printIf0Cons;
    }

    @XmlTransient
    public Collection<FhrdPaytransleaveEarn> getFhrdPaytransleaveEarnCollection() {
        return fhrdPaytransleaveEarnCollection;
    }

    public void setFhrdPaytransleaveEarnCollection(Collection<FhrdPaytransleaveEarn> fhrdPaytransleaveEarnCollection) {
        this.fhrdPaytransleaveEarnCollection = fhrdPaytransleaveEarnCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @XmlTransient
    public Collection<FtasAbsenteesDtl> getFtasAbsenteesDtlCollection() {
        return ftasAbsenteesDtlCollection;
    }

    public void setFtasAbsenteesDtlCollection(Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection) {
        this.ftasAbsenteesDtlCollection = ftasAbsenteesDtlCollection;
    }

    @XmlTransient
    public Collection<FtasAbsenteesDtl> getFtasAbsenteesDtlCollection1() {
        return ftasAbsenteesDtlCollection1;
    }

    public void setFtasAbsenteesDtlCollection1(Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection1) {
        this.ftasAbsenteesDtlCollection1 = ftasAbsenteesDtlCollection1;
    }

    @XmlTransient
    public Collection<FtasAbsenteesDtl> getFtasAbsenteesDtlCollection2() {
        return ftasAbsenteesDtlCollection2;
    }

    public void setFtasAbsenteesDtlCollection2(Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection2) {
        this.ftasAbsenteesDtlCollection2 = ftasAbsenteesDtlCollection2;
    }

    @XmlTransient
    public Collection<FtasAbsenteesDtl> getFtasAbsenteesDtlCollection3() {
        return ftasAbsenteesDtlCollection3;
    }

    public void setFtasAbsenteesDtlCollection3(Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection3) {
        this.ftasAbsenteesDtlCollection3 = ftasAbsenteesDtlCollection3;
    }

    @XmlTransient
    public Collection<FtasAbsenteesDtl> getFtasAbsenteesDtlCollection4() {
        return ftasAbsenteesDtlCollection4;
    }

    public void setFtasAbsenteesDtlCollection4(Collection<FtasAbsenteesDtl> ftasAbsenteesDtlCollection4) {
        this.ftasAbsenteesDtlCollection4 = ftasAbsenteesDtlCollection4;
    }

    @XmlTransient
    public Collection<FhrdPaytransleaveDtl> getFhrdPaytransleaveDtlCollection() {
        return fhrdPaytransleaveDtlCollection;
    }

    public void setFhrdPaytransleaveDtlCollection(Collection<FhrdPaytransleaveDtl> fhrdPaytransleaveDtlCollection) {
        this.fhrdPaytransleaveDtlCollection = fhrdPaytransleaveDtlCollection;
    }

    @XmlTransient
    public Collection<FtasDaily> getFtasDailyCollection() {
        return ftasDailyCollection;
    }

    public void setFtasDailyCollection(Collection<FtasDaily> ftasDailyCollection) {
        this.ftasDailyCollection = ftasDailyCollection;
    }

    @XmlTransient
    public Collection<FtasDaily> getFtasDailyCollection1() {
        return ftasDailyCollection1;
    }

    public void setFtasDailyCollection1(Collection<FtasDaily> ftasDailyCollection1) {
        this.ftasDailyCollection1 = ftasDailyCollection1;
    }

    @XmlTransient
    public Collection<FtasDaily> getFtasDailyCollection2() {
        return ftasDailyCollection2;
    }

    public void setFtasDailyCollection2(Collection<FtasDaily> ftasDailyCollection2) {
        this.ftasDailyCollection2 = ftasDailyCollection2;
    }

    @XmlTransient
    public Collection<FtasDaily> getFtasDailyCollection3() {
        return ftasDailyCollection3;
    }

    public void setFtasDailyCollection3(Collection<FtasDaily> ftasDailyCollection3) {
        this.ftasDailyCollection3 = ftasDailyCollection3;
    }

    @XmlTransient
    public Collection<FtasDaily> getFtasDailyCollection4() {
        return ftasDailyCollection4;
    }

    public void setFtasDailyCollection4(Collection<FtasDaily> ftasDailyCollection4) {
        this.ftasDailyCollection4 = ftasDailyCollection4;
    }

    @XmlTransient
    public Collection<FtasDaily> getFtasDailyCollection5() {
        return ftasDailyCollection5;
    }

    public void setFtasDailyCollection5(Collection<FtasDaily> ftasDailyCollection5) {
        this.ftasDailyCollection5 = ftasDailyCollection5;
    }

    @XmlTransient
    public Collection<FtasDaily> getFtasDailyCollection6() {
        return ftasDailyCollection6;
    }

    public void setFtasDailyCollection6(Collection<FtasDaily> ftasDailyCollection6) {
        this.ftasDailyCollection6 = ftasDailyCollection6;
    }

    @XmlTransient
    public Collection<FtasDaily> getFtasDailyCollection7() {
        return ftasDailyCollection7;
    }

    public void setFtasDailyCollection7(Collection<FtasDaily> ftasDailyCollection7) {
        this.ftasDailyCollection7 = ftasDailyCollection7;
    }

    @XmlTransient
    public Collection<FtasDaily> getFtasDailyCollection8() {
        return ftasDailyCollection8;
    }

    public void setFtasDailyCollection8(Collection<FtasDaily> ftasDailyCollection8) {
        this.ftasDailyCollection8 = ftasDailyCollection8;
    }

    @XmlTransient
    public Collection<FtasDaily> getFtasDailyCollection9() {
        return ftasDailyCollection9;
    }

    public void setFtasDailyCollection9(Collection<FtasDaily> ftasDailyCollection9) {
        this.ftasDailyCollection9 = ftasDailyCollection9;
    }

    @XmlTransient
    public Collection<FtasDaily> getFtasDailyCollection10() {
        return ftasDailyCollection10;
    }

    public void setFtasDailyCollection10(Collection<FtasDaily> ftasDailyCollection10) {
        this.ftasDailyCollection10 = ftasDailyCollection10;
    }

    @XmlTransient
    public Collection<FtasDaily> getFtasDailyCollection11() {
        return ftasDailyCollection11;
    }

    public void setFtasDailyCollection11(Collection<FtasDaily> ftasDailyCollection11) {
        this.ftasDailyCollection11 = ftasDailyCollection11;
    }

    @XmlTransient
    public Collection<FhrdEmpleavebal> getFhrdEmpleavebalCollection() {
        return fhrdEmpleavebalCollection;
    }

    public void setFhrdEmpleavebalCollection(Collection<FhrdEmpleavebal> fhrdEmpleavebalCollection) {
        this.fhrdEmpleavebalCollection = fhrdEmpleavebalCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpleaverule> getFhrdEmpleaveruleCollection() {
        return fhrdEmpleaveruleCollection;
    }

    public void setFhrdEmpleaveruleCollection(Collection<FhrdEmpleaverule> fhrdEmpleaveruleCollection) {
        this.fhrdEmpleaveruleCollection = fhrdEmpleaveruleCollection;
    }

    @XmlTransient
    public Collection<FhrdEmpEncash> getFhrdEmpEncashCollection() {
        return fhrdEmpEncashCollection;
    }

    public void setFhrdEmpEncashCollection(Collection<FhrdEmpEncash> fhrdEmpEncashCollection) {
        this.fhrdEmpEncashCollection = fhrdEmpEncashCollection;
    }

    @XmlTransient
    public Collection<FhrdLeaverule> getFhrdLeaveruleCollection() {
        return fhrdLeaveruleCollection;
    }

    public void setFhrdLeaveruleCollection(Collection<FhrdLeaverule> fhrdLeaveruleCollection) {
        this.fhrdLeaveruleCollection = fhrdLeaveruleCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lmSrgKey != null ? lmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasLeavemst)) {
            return false;
        }
        FtasLeavemst other = (FtasLeavemst) object;
        if ((this.lmSrgKey == null && other.lmSrgKey != null) || (this.lmSrgKey != null && !this.lmSrgKey.equals(other.lmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasLeavemst[ lmSrgKey=" + lmSrgKey + " ]";
    }
    
}
