/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "BMWP_ACTIVITY_BUDGET_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmwpActivityBudgetDtl.findAll", query = "SELECT b FROM BmwpActivityBudgetDtl b"),
    @NamedQuery(name = "BmwpActivityBudgetDtl.findByAbdUniqueSrno", query = "SELECT b FROM BmwpActivityBudgetDtl b WHERE b.abdUniqueSrno = :abdUniqueSrno"),
    @NamedQuery(name = "BmwpActivityBudgetDtl.findByAbdSrno", query = "SELECT b FROM BmwpActivityBudgetDtl b WHERE b.abdSrno = :abdSrno"),
    @NamedQuery(name = "BmwpActivityBudgetDtl.findByCreatedt", query = "SELECT b FROM BmwpActivityBudgetDtl b WHERE b.createdt = :createdt"),
    @NamedQuery(name = "BmwpActivityBudgetDtl.findByUpdatedt", query = "SELECT b FROM BmwpActivityBudgetDtl b WHERE b.updatedt = :updatedt"),
    @NamedQuery(name = "BmwpActivityBudgetDtl.findBySystemUserid", query = "SELECT b FROM BmwpActivityBudgetDtl b WHERE b.systemUserid = :systemUserid"),
    @NamedQuery(name = "BmwpActivityBudgetDtl.findByExportFlag", query = "SELECT b FROM BmwpActivityBudgetDtl b WHERE b.exportFlag = :exportFlag"),
    @NamedQuery(name = "BmwpActivityBudgetDtl.findByImportFlag", query = "SELECT b FROM BmwpActivityBudgetDtl b WHERE b.importFlag = :importFlag"),
    @NamedQuery(name = "BmwpActivityBudgetDtl.findByAbdStartDate", query = "SELECT b FROM BmwpActivityBudgetDtl b WHERE b.abdStartDate = :abdStartDate"),
    @NamedQuery(name = "BmwpActivityBudgetDtl.findByAbdEndDate", query = "SELECT b FROM BmwpActivityBudgetDtl b WHERE b.abdEndDate = :abdEndDate"),
    @NamedQuery(name = "BmwpActivityBudgetDtl.findByAbdEndDatePossible", query = "SELECT b FROM BmwpActivityBudgetDtl b WHERE b.abdEndDatePossible = :abdEndDatePossible")})
public class BmwpActivityBudgetDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ABD_UNIQUE_SRNO")
    private BigDecimal abdUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ABD_SRNO")
    private int abdSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ABD_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date abdStartDate;
    @Column(name = "ABD_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date abdEndDate;
    @Column(name = "ABD_END_DATE_POSSIBLE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date abdEndDatePossible;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "wpAbdUniqueSrno")
    private Collection<BmwpWorkplan> bmwpWorkplanCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "ABD_BM_UNIQUE_SRNO", referencedColumnName = "BM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpBudgetMst abdBmUniqueSrno;
    @JoinColumn(name = "ABD_AM_UNIQUE_SRNO", referencedColumnName = "AM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpActivityMst abdAmUniqueSrno;
    @JoinColumn(name = "ABD_ABL_UNIQUE_SRNO", referencedColumnName = "ABL_UNIQUE_SRNO")
    @ManyToOne(optional = false)
    private BmwpActivityBudgetLink abdAblUniqueSrno;

    public BmwpActivityBudgetDtl() {
    }

    public BmwpActivityBudgetDtl(BigDecimal abdUniqueSrno) {
        this.abdUniqueSrno = abdUniqueSrno;
    }

    public BmwpActivityBudgetDtl(BigDecimal abdUniqueSrno, int abdSrno, Date createdt, Date abdStartDate) {
        this.abdUniqueSrno = abdUniqueSrno;
        this.abdSrno = abdSrno;
        this.createdt = createdt;
        this.abdStartDate = abdStartDate;
    }

    public BigDecimal getAbdUniqueSrno() {
        return abdUniqueSrno;
    }

    public void setAbdUniqueSrno(BigDecimal abdUniqueSrno) {
        this.abdUniqueSrno = abdUniqueSrno;
    }

    public int getAbdSrno() {
        return abdSrno;
    }

    public void setAbdSrno(int abdSrno) {
        this.abdSrno = abdSrno;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Date getAbdStartDate() {
        return abdStartDate;
    }

    public void setAbdStartDate(Date abdStartDate) {
        this.abdStartDate = abdStartDate;
    }

    public Date getAbdEndDate() {
        return abdEndDate;
    }

    public void setAbdEndDate(Date abdEndDate) {
        this.abdEndDate = abdEndDate;
    }

    public Date getAbdEndDatePossible() {
        return abdEndDatePossible;
    }

    public void setAbdEndDatePossible(Date abdEndDatePossible) {
        this.abdEndDatePossible = abdEndDatePossible;
    }

    @XmlTransient
    public Collection<BmwpWorkplan> getBmwpWorkplanCollection() {
        return bmwpWorkplanCollection;
    }

    public void setBmwpWorkplanCollection(Collection<BmwpWorkplan> bmwpWorkplanCollection) {
        this.bmwpWorkplanCollection = bmwpWorkplanCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public BmwpBudgetMst getAbdBmUniqueSrno() {
        return abdBmUniqueSrno;
    }

    public void setAbdBmUniqueSrno(BmwpBudgetMst abdBmUniqueSrno) {
        this.abdBmUniqueSrno = abdBmUniqueSrno;
    }

    public BmwpActivityMst getAbdAmUniqueSrno() {
        return abdAmUniqueSrno;
    }

    public void setAbdAmUniqueSrno(BmwpActivityMst abdAmUniqueSrno) {
        this.abdAmUniqueSrno = abdAmUniqueSrno;
    }

    public BmwpActivityBudgetLink getAbdAblUniqueSrno() {
        return abdAblUniqueSrno;
    }

    public void setAbdAblUniqueSrno(BmwpActivityBudgetLink abdAblUniqueSrno) {
        this.abdAblUniqueSrno = abdAblUniqueSrno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (abdUniqueSrno != null ? abdUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmwpActivityBudgetDtl)) {
            return false;
        }
        BmwpActivityBudgetDtl other = (BmwpActivityBudgetDtl) object;
        if ((this.abdUniqueSrno == null && other.abdUniqueSrno != null) || (this.abdUniqueSrno != null && !this.abdUniqueSrno.equals(other.abdUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.BmwpActivityBudgetDtl[ abdUniqueSrno=" + abdUniqueSrno + " ]";
    }
    
}
