/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FCAD_ADDR_JOB_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FcadAddrJobMst.findAll", query = "SELECT f FROM FcadAddrJobMst f"),
    @NamedQuery(name = "FcadAddrJobMst.findByJobUniqueSrno", query = "SELECT f FROM FcadAddrJobMst f WHERE f.jobUniqueSrno = :jobUniqueSrno"),
    @NamedQuery(name = "FcadAddrJobMst.findByJobdesc", query = "SELECT f FROM FcadAddrJobMst f WHERE f.jobdesc = :jobdesc"),
    @NamedQuery(name = "FcadAddrJobMst.findByRefdate", query = "SELECT f FROM FcadAddrJobMst f WHERE f.refdate = :refdate"),
    @NamedQuery(name = "FcadAddrJobMst.findByRefcode", query = "SELECT f FROM FcadAddrJobMst f WHERE f.refcode = :refcode"),
    @NamedQuery(name = "FcadAddrJobMst.findByReftitle", query = "SELECT f FROM FcadAddrJobMst f WHERE f.reftitle = :reftitle"),
    @NamedQuery(name = "FcadAddrJobMst.findByReusejobid", query = "SELECT f FROM FcadAddrJobMst f WHERE f.reusejobid = :reusejobid"),
    @NamedQuery(name = "FcadAddrJobMst.findByPasswd", query = "SELECT f FROM FcadAddrJobMst f WHERE f.passwd = :passwd"),
    @NamedQuery(name = "FcadAddrJobMst.findByMemo1a5", query = "SELECT f FROM FcadAddrJobMst f WHERE f.memo1a5 = :memo1a5"),
    @NamedQuery(name = "FcadAddrJobMst.findByMemo2a5", query = "SELECT f FROM FcadAddrJobMst f WHERE f.memo2a5 = :memo2a5"),
    @NamedQuery(name = "FcadAddrJobMst.findByMemo3a5", query = "SELECT f FROM FcadAddrJobMst f WHERE f.memo3a5 = :memo3a5"),
    @NamedQuery(name = "FcadAddrJobMst.findByMemo4a5", query = "SELECT f FROM FcadAddrJobMst f WHERE f.memo4a5 = :memo4a5"),
    @NamedQuery(name = "FcadAddrJobMst.findByRegarda5", query = "SELECT f FROM FcadAddrJobMst f WHERE f.regarda5 = :regarda5"),
    @NamedQuery(name = "FcadAddrJobMst.findByMemo1a4", query = "SELECT f FROM FcadAddrJobMst f WHERE f.memo1a4 = :memo1a4"),
    @NamedQuery(name = "FcadAddrJobMst.findByMemo2a4", query = "SELECT f FROM FcadAddrJobMst f WHERE f.memo2a4 = :memo2a4"),
    @NamedQuery(name = "FcadAddrJobMst.findByMemo3a4", query = "SELECT f FROM FcadAddrJobMst f WHERE f.memo3a4 = :memo3a4"),
    @NamedQuery(name = "FcadAddrJobMst.findByMemo4a4", query = "SELECT f FROM FcadAddrJobMst f WHERE f.memo4a4 = :memo4a4"),
    @NamedQuery(name = "FcadAddrJobMst.findByRegarda4", query = "SELECT f FROM FcadAddrJobMst f WHERE f.regarda4 = :regarda4"),
    @NamedQuery(name = "FcadAddrJobMst.findByCreatedt", query = "SELECT f FROM FcadAddrJobMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FcadAddrJobMst.findByModifyby", query = "SELECT f FROM FcadAddrJobMst f WHERE f.modifyby = :modifyby"),
    @NamedQuery(name = "FcadAddrJobMst.findByModifydt", query = "SELECT f FROM FcadAddrJobMst f WHERE f.modifydt = :modifydt"),
    @NamedQuery(name = "FcadAddrJobMst.findByWorkingflag", query = "SELECT f FROM FcadAddrJobMst f WHERE f.workingflag = :workingflag"),
    @NamedQuery(name = "FcadAddrJobMst.findBySystemUserid", query = "SELECT f FROM FcadAddrJobMst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FcadAddrJobMst.findByExportFlag", query = "SELECT f FROM FcadAddrJobMst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FcadAddrJobMst.findByImportFlag", query = "SELECT f FROM FcadAddrJobMst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FcadAddrJobMst.findByUpdatedt", query = "SELECT f FROM FcadAddrJobMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FcadAddrJobMst.findByStartDate", query = "SELECT f FROM FcadAddrJobMst f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FcadAddrJobMst.findByEndDate", query = "SELECT f FROM FcadAddrJobMst f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FcadAddrJobMst.findByJobStatus", query = "SELECT f FROM FcadAddrJobMst f WHERE f.jobStatus = :jobStatus"),
    @NamedQuery(name = "FcadAddrJobMst.findBySubject", query = "SELECT f FROM FcadAddrJobMst f WHERE f.subject = :subject")})
public class FcadAddrJobMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "JOB_UNIQUE_SRNO")
    private Integer jobUniqueSrno;
    @Size(max = 50)
    @Column(name = "JOBDESC")
    private String jobdesc;
    @Column(name = "REFDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date refdate;
    @Size(max = 25)
    @Column(name = "REFCODE")
    private String refcode;
    @Size(max = 255)
    @Column(name = "REFTITLE")
    private String reftitle;
    @Column(name = "REUSEJOBID")
    private Integer reusejobid;
    @Size(max = 20)
    @Column(name = "PASSWD")
    private String passwd;
    @Size(max = 100)
    @Column(name = "MEMO1A5")
    private String memo1a5;
    @Size(max = 100)
    @Column(name = "MEMO2A5")
    private String memo2a5;
    @Size(max = 100)
    @Column(name = "MEMO3A5")
    private String memo3a5;
    @Size(max = 100)
    @Column(name = "MEMO4A5")
    private String memo4a5;
    @Size(max = 100)
    @Column(name = "REGARDA5")
    private String regarda5;
    @Lob
    @Column(name = "A4")
    private String a4;
    @Size(max = 100)
    @Column(name = "MEMO1A4")
    private String memo1a4;
    @Size(max = 100)
    @Column(name = "MEMO2A4")
    private String memo2a4;
    @Size(max = 100)
    @Column(name = "MEMO3A4")
    private String memo3a4;
    @Size(max = 100)
    @Column(name = "MEMO4A4")
    private String memo4a4;
    @Size(max = 100)
    @Column(name = "REGARDA4")
    private String regarda4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "MODIFYBY")
    private Integer modifyby;
    @Column(name = "MODIFYDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifydt;
    @Size(max = 1)
    @Column(name = "WORKINGFLAG")
    private String workingflag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Lob
    @Column(name = "A5")
    private String a5;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "JOB_STATUS")
    private String jobStatus;
    @Size(max = 2000)
    @Column(name = "SUBJECT")
    private String subject;
    @OneToMany(mappedBy = "jobUniqueSrno")
    private Collection<FcadAddrJobStageTitle> fcadAddrJobStageTitleCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;

    public FcadAddrJobMst() {
    }

    public FcadAddrJobMst(Integer jobUniqueSrno) {
        this.jobUniqueSrno = jobUniqueSrno;
    }

    public FcadAddrJobMst(Integer jobUniqueSrno, Date createdt, Date startDate, String jobStatus) {
        this.jobUniqueSrno = jobUniqueSrno;
        this.createdt = createdt;
        this.startDate = startDate;
        this.jobStatus = jobStatus;
    }

    public Integer getJobUniqueSrno() {
        return jobUniqueSrno;
    }

    public void setJobUniqueSrno(Integer jobUniqueSrno) {
        this.jobUniqueSrno = jobUniqueSrno;
    }

    public String getJobdesc() {
        return jobdesc;
    }

    public void setJobdesc(String jobdesc) {
        this.jobdesc = jobdesc;
    }

    public Date getRefdate() {
        return refdate;
    }

    public void setRefdate(Date refdate) {
        this.refdate = refdate;
    }

    public String getRefcode() {
        return refcode;
    }

    public void setRefcode(String refcode) {
        this.refcode = refcode;
    }

    public String getReftitle() {
        return reftitle;
    }

    public void setReftitle(String reftitle) {
        this.reftitle = reftitle;
    }

    public Integer getReusejobid() {
        return reusejobid;
    }

    public void setReusejobid(Integer reusejobid) {
        this.reusejobid = reusejobid;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getMemo1a5() {
        return memo1a5;
    }

    public void setMemo1a5(String memo1a5) {
        this.memo1a5 = memo1a5;
    }

    public String getMemo2a5() {
        return memo2a5;
    }

    public void setMemo2a5(String memo2a5) {
        this.memo2a5 = memo2a5;
    }

    public String getMemo3a5() {
        return memo3a5;
    }

    public void setMemo3a5(String memo3a5) {
        this.memo3a5 = memo3a5;
    }

    public String getMemo4a5() {
        return memo4a5;
    }

    public void setMemo4a5(String memo4a5) {
        this.memo4a5 = memo4a5;
    }

    public String getRegarda5() {
        return regarda5;
    }

    public void setRegarda5(String regarda5) {
        this.regarda5 = regarda5;
    }

    public String getA4() {
        return a4;
    }

    public void setA4(String a4) {
        this.a4 = a4;
    }

    public String getMemo1a4() {
        return memo1a4;
    }

    public void setMemo1a4(String memo1a4) {
        this.memo1a4 = memo1a4;
    }

    public String getMemo2a4() {
        return memo2a4;
    }

    public void setMemo2a4(String memo2a4) {
        this.memo2a4 = memo2a4;
    }

    public String getMemo3a4() {
        return memo3a4;
    }

    public void setMemo3a4(String memo3a4) {
        this.memo3a4 = memo3a4;
    }

    public String getMemo4a4() {
        return memo4a4;
    }

    public void setMemo4a4(String memo4a4) {
        this.memo4a4 = memo4a4;
    }

    public String getRegarda4() {
        return regarda4;
    }

    public void setRegarda4(String regarda4) {
        this.regarda4 = regarda4;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Integer getModifyby() {
        return modifyby;
    }

    public void setModifyby(Integer modifyby) {
        this.modifyby = modifyby;
    }

    public Date getModifydt() {
        return modifydt;
    }

    public void setModifydt(Date modifydt) {
        this.modifydt = modifydt;
    }

    public String getWorkingflag() {
        return workingflag;
    }

    public void setWorkingflag(String workingflag) {
        this.workingflag = workingflag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getA5() {
        return a5;
    }

    public void setA5(String a5) {
        this.a5 = a5;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @XmlTransient
    public Collection<FcadAddrJobStageTitle> getFcadAddrJobStageTitleCollection() {
        return fcadAddrJobStageTitleCollection;
    }

    public void setFcadAddrJobStageTitleCollection(Collection<FcadAddrJobStageTitle> fcadAddrJobStageTitleCollection) {
        this.fcadAddrJobStageTitleCollection = fcadAddrJobStageTitleCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (jobUniqueSrno != null ? jobUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FcadAddrJobMst)) {
            return false;
        }
        FcadAddrJobMst other = (FcadAddrJobMst) object;
        if ((this.jobUniqueSrno == null && other.jobUniqueSrno != null) || (this.jobUniqueSrno != null && !this.jobUniqueSrno.equals(other.jobUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FcadAddrJobMst[ jobUniqueSrno=" + jobUniqueSrno + " ]";
    }
    
}
