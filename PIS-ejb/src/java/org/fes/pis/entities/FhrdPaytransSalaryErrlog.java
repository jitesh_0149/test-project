/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_PAYTRANS_SALARY_ERRLOG")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdPaytransSalaryErrlog.findAll", query = "SELECT f FROM FhrdPaytransSalaryErrlog f"),
    @NamedQuery(name = "FhrdPaytransSalaryErrlog.findBySalarySrgKey", query = "SELECT f FROM FhrdPaytransSalaryErrlog f WHERE f.fhrdPaytransSalaryErrlogPK.salarySrgKey = :salarySrgKey"),
    @NamedQuery(name = "FhrdPaytransSalaryErrlog.findBySelUserId", query = "SELECT f FROM FhrdPaytransSalaryErrlog f WHERE f.fhrdPaytransSalaryErrlogPK.selUserId = :selUserId"),
    @NamedQuery(name = "FhrdPaytransSalaryErrlog.findBySelErrorMessage", query = "SELECT f FROM FhrdPaytransSalaryErrlog f WHERE f.selErrorMessage = :selErrorMessage"),
    @NamedQuery(name = "FhrdPaytransSalaryErrlog.findBySelPossibleReason", query = "SELECT f FROM FhrdPaytransSalaryErrlog f WHERE f.selPossibleReason = :selPossibleReason"),
    @NamedQuery(name = "FhrdPaytransSalaryErrlog.findBySelOrgSalaryFromDate", query = "SELECT f FROM FhrdPaytransSalaryErrlog f WHERE f.selOrgSalaryFromDate = :selOrgSalaryFromDate"),
    @NamedQuery(name = "FhrdPaytransSalaryErrlog.findBySelOrgSalaryToDate", query = "SELECT f FROM FhrdPaytransSalaryErrlog f WHERE f.selOrgSalaryToDate = :selOrgSalaryToDate"),
    @NamedQuery(name = "FhrdPaytransSalaryErrlog.findBySelOrgAttendanceFromDate", query = "SELECT f FROM FhrdPaytransSalaryErrlog f WHERE f.selOrgAttendanceFromDate = :selOrgAttendanceFromDate"),
    @NamedQuery(name = "FhrdPaytransSalaryErrlog.findBySelOrgAttendanceToDate", query = "SELECT f FROM FhrdPaytransSalaryErrlog f WHERE f.selOrgAttendanceToDate = :selOrgAttendanceToDate"),
    @NamedQuery(name = "FhrdPaytransSalaryErrlog.findBySelUserSelection", query = "SELECT f FROM FhrdPaytransSalaryErrlog f WHERE f.selUserSelection = :selUserSelection"),
    @NamedQuery(name = "FhrdPaytransSalaryErrlog.findBySelUserOuList", query = "SELECT f FROM FhrdPaytransSalaryErrlog f WHERE f.selUserOuList = :selUserOuList"),
    @NamedQuery(name = "FhrdPaytransSalaryErrlog.findBySelCreateDate", query = "SELECT f FROM FhrdPaytransSalaryErrlog f WHERE f.selCreateDate = :selCreateDate")})
public class FhrdPaytransSalaryErrlog implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FhrdPaytransSalaryErrlogPK fhrdPaytransSalaryErrlogPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "SEL_ERROR_MESSAGE")
    private String selErrorMessage;
    @Size(max = 2000)
    @Column(name = "SEL_POSSIBLE_REASON")
    private String selPossibleReason;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SEL_ORG_SALARY_FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date selOrgSalaryFromDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SEL_ORG_SALARY_TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date selOrgSalaryToDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SEL_ORG_ATTENDANCE_FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date selOrgAttendanceFromDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SEL_ORG_ATTENDANCE_TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date selOrgAttendanceToDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "SEL_USER_SELECTION")
    private String selUserSelection;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "SEL_USER_OU_LIST")
    private String selUserOuList;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SEL_CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date selCreateDate;

    public FhrdPaytransSalaryErrlog() {
    }

    public FhrdPaytransSalaryErrlog(FhrdPaytransSalaryErrlogPK fhrdPaytransSalaryErrlogPK) {
        this.fhrdPaytransSalaryErrlogPK = fhrdPaytransSalaryErrlogPK;
    }

    public FhrdPaytransSalaryErrlog(FhrdPaytransSalaryErrlogPK fhrdPaytransSalaryErrlogPK, String selErrorMessage, Date selOrgSalaryFromDate, Date selOrgSalaryToDate, Date selOrgAttendanceFromDate, Date selOrgAttendanceToDate, String selUserSelection, String selUserOuList, Date selCreateDate) {
        this.fhrdPaytransSalaryErrlogPK = fhrdPaytransSalaryErrlogPK;
        this.selErrorMessage = selErrorMessage;
        this.selOrgSalaryFromDate = selOrgSalaryFromDate;
        this.selOrgSalaryToDate = selOrgSalaryToDate;
        this.selOrgAttendanceFromDate = selOrgAttendanceFromDate;
        this.selOrgAttendanceToDate = selOrgAttendanceToDate;
        this.selUserSelection = selUserSelection;
        this.selUserOuList = selUserOuList;
        this.selCreateDate = selCreateDate;
    }

    public FhrdPaytransSalaryErrlog(BigDecimal salarySrgKey, int selUserId) {
        this.fhrdPaytransSalaryErrlogPK = new FhrdPaytransSalaryErrlogPK(salarySrgKey, selUserId);
    }

    public FhrdPaytransSalaryErrlogPK getFhrdPaytransSalaryErrlogPK() {
        return fhrdPaytransSalaryErrlogPK;
    }

    public void setFhrdPaytransSalaryErrlogPK(FhrdPaytransSalaryErrlogPK fhrdPaytransSalaryErrlogPK) {
        this.fhrdPaytransSalaryErrlogPK = fhrdPaytransSalaryErrlogPK;
    }

    public String getSelErrorMessage() {
        return selErrorMessage;
    }

    public void setSelErrorMessage(String selErrorMessage) {
        this.selErrorMessage = selErrorMessage;
    }

    public String getSelPossibleReason() {
        return selPossibleReason;
    }

    public void setSelPossibleReason(String selPossibleReason) {
        this.selPossibleReason = selPossibleReason;
    }

    public Date getSelOrgSalaryFromDate() {
        return selOrgSalaryFromDate;
    }

    public void setSelOrgSalaryFromDate(Date selOrgSalaryFromDate) {
        this.selOrgSalaryFromDate = selOrgSalaryFromDate;
    }

    public Date getSelOrgSalaryToDate() {
        return selOrgSalaryToDate;
    }

    public void setSelOrgSalaryToDate(Date selOrgSalaryToDate) {
        this.selOrgSalaryToDate = selOrgSalaryToDate;
    }

    public Date getSelOrgAttendanceFromDate() {
        return selOrgAttendanceFromDate;
    }

    public void setSelOrgAttendanceFromDate(Date selOrgAttendanceFromDate) {
        this.selOrgAttendanceFromDate = selOrgAttendanceFromDate;
    }

    public Date getSelOrgAttendanceToDate() {
        return selOrgAttendanceToDate;
    }

    public void setSelOrgAttendanceToDate(Date selOrgAttendanceToDate) {
        this.selOrgAttendanceToDate = selOrgAttendanceToDate;
    }

    public String getSelUserSelection() {
        return selUserSelection;
    }

    public void setSelUserSelection(String selUserSelection) {
        this.selUserSelection = selUserSelection;
    }

    public String getSelUserOuList() {
        return selUserOuList;
    }

    public void setSelUserOuList(String selUserOuList) {
        this.selUserOuList = selUserOuList;
    }

    public Date getSelCreateDate() {
        return selCreateDate;
    }

    public void setSelCreateDate(Date selCreateDate) {
        this.selCreateDate = selCreateDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fhrdPaytransSalaryErrlogPK != null ? fhrdPaytransSalaryErrlogPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdPaytransSalaryErrlog)) {
            return false;
        }
        FhrdPaytransSalaryErrlog other = (FhrdPaytransSalaryErrlog) object;
        if ((this.fhrdPaytransSalaryErrlogPK == null && other.fhrdPaytransSalaryErrlogPK != null) || (this.fhrdPaytransSalaryErrlogPK != null && !this.fhrdPaytransSalaryErrlogPK.equals(other.fhrdPaytransSalaryErrlogPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdPaytransSalaryErrlog[ fhrdPaytransSalaryErrlogPK=" + fhrdPaytransSalaryErrlogPK + " ]";
    }
    
}
