/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_BULK_ATT_TEMP")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasBulkAttTemp.findAll", query = "SELECT f FROM FtasBulkAttTemp f"),
    @NamedQuery(name = "FtasBulkAttTemp.findByUserId", query = "SELECT f FROM FtasBulkAttTemp f WHERE f.ftasBulkAttTempPK.userId = :userId"),
    @NamedQuery(name = "FtasBulkAttTemp.findByEmpno", query = "SELECT f FROM FtasBulkAttTemp f WHERE f.empno = :empno"),
    @NamedQuery(name = "FtasBulkAttTemp.findByUserName", query = "SELECT f FROM FtasBulkAttTemp f WHERE f.userName = :userName"),
    @NamedQuery(name = "FtasBulkAttTemp.findByAttddt", query = "SELECT f FROM FtasBulkAttTemp f WHERE f.ftasBulkAttTempPK.attddt = :attddt"),
    @NamedQuery(name = "FtasBulkAttTemp.findByAttdtime", query = "SELECT f FROM FtasBulkAttTemp f WHERE f.attdtime = :attdtime"),
    @NamedQuery(name = "FtasBulkAttTemp.findByFromHalf", query = "SELECT f FROM FtasBulkAttTemp f WHERE f.fromHalf = :fromHalf"),
    @NamedQuery(name = "FtasBulkAttTemp.findByToHalf", query = "SELECT f FROM FtasBulkAttTemp f WHERE f.toHalf = :toHalf"),
    @NamedQuery(name = "FtasBulkAttTemp.findByFromTime", query = "SELECT f FROM FtasBulkAttTemp f WHERE f.fromTime = :fromTime"),
    @NamedQuery(name = "FtasBulkAttTemp.findByToTime", query = "SELECT f FROM FtasBulkAttTemp f WHERE f.toTime = :toTime"),
    @NamedQuery(name = "FtasBulkAttTemp.findByOumUnitSrno", query = "SELECT f FROM FtasBulkAttTemp f WHERE f.oumUnitSrno = :oumUnitSrno"),
    @NamedQuery(name = "FtasBulkAttTemp.findByTranYear", query = "SELECT f FROM FtasBulkAttTemp f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FtasBulkAttTemp.findByBatSrno", query = "SELECT f FROM FtasBulkAttTemp f WHERE f.ftasBulkAttTempPK.batSrno = :batSrno"),
    @NamedQuery(name = "FtasBulkAttTemp.findByFailureDescription", query = "SELECT f FROM FtasBulkAttTemp f WHERE f.failureDescription = :failureDescription"),
    @NamedQuery(name = "FtasBulkAttTemp.findByCreateby", query = "SELECT f FROM FtasBulkAttTemp f WHERE f.createby = :createby"),
    @NamedQuery(name = "FtasBulkAttTemp.findByCreatedt", query = "SELECT f FROM FtasBulkAttTemp f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasBulkAttTemp.findByUpdateby", query = "SELECT f FROM FtasBulkAttTemp f WHERE f.updateby = :updateby"),
    @NamedQuery(name = "FtasBulkAttTemp.findByUpdatedt", query = "SELECT f FROM FtasBulkAttTemp f WHERE f.updatedt = :updatedt")})
public class FtasBulkAttTemp implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FtasBulkAttTempPK ftasBulkAttTempPK;
    @Column(name = "EMPNO")
    private Integer empno;
    @Size(max = 100)
    @Column(name = "USER_NAME")
    private String userName;
    @Size(max = 8)
    @Column(name = "ATTDTIME")
    private String attdtime;
    @Size(max = 5)
    @Column(name = "FROM_HALF")
    private String fromHalf;
    @Size(max = 5)
    @Column(name = "TO_HALF")
    private String toHalf;
    @Size(max = 8)
    @Column(name = "FROM_TIME")
    private String fromTime;
    @Size(max = 8)
    @Column(name = "TO_TIME")
    private String toTime;
    @Column(name = "OUM_UNIT_SRNO")
    private Integer oumUnitSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Size(max = 2000)
    @Column(name = "FAILURE_DESCRIPTION")
    private String failureDescription;
    @Column(name = "CREATEBY")
    private Integer createby;
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEBY")
    private Integer updateby;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FhrdEmpmst fhrdEmpmst;

    public FtasBulkAttTemp() {
    }

    public FtasBulkAttTemp(FtasBulkAttTempPK ftasBulkAttTempPK) {
        this.ftasBulkAttTempPK = ftasBulkAttTempPK;
    }

    public FtasBulkAttTemp(FtasBulkAttTempPK ftasBulkAttTempPK, short tranYear) {
        this.ftasBulkAttTempPK = ftasBulkAttTempPK;
        this.tranYear = tranYear;
    }

    public FtasBulkAttTemp(int userId, Date attddt, BigDecimal batSrno) {
        this.ftasBulkAttTempPK = new FtasBulkAttTempPK(userId, attddt, batSrno);
    }

    public FtasBulkAttTempPK getFtasBulkAttTempPK() {
        return ftasBulkAttTempPK;
    }

    public void setFtasBulkAttTempPK(FtasBulkAttTempPK ftasBulkAttTempPK) {
        this.ftasBulkAttTempPK = ftasBulkAttTempPK;
    }

    public Integer getEmpno() {
        return empno;
    }

    public void setEmpno(Integer empno) {
        this.empno = empno;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAttdtime() {
        return attdtime;
    }

    public void setAttdtime(String attdtime) {
        this.attdtime = attdtime;
    }

    public String getFromHalf() {
        return fromHalf;
    }

    public void setFromHalf(String fromHalf) {
        this.fromHalf = fromHalf;
    }

    public String getToHalf() {
        return toHalf;
    }

    public void setToHalf(String toHalf) {
        this.toHalf = toHalf;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public Integer getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(Integer oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public String getFailureDescription() {
        return failureDescription;
    }

    public void setFailureDescription(String failureDescription) {
        this.failureDescription = failureDescription;
    }

    public Integer getCreateby() {
        return createby;
    }

    public void setCreateby(Integer createby) {
        this.createby = createby;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Integer getUpdateby() {
        return updateby;
    }

    public void setUpdateby(Integer updateby) {
        this.updateby = updateby;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public FhrdEmpmst getFhrdEmpmst() {
        return fhrdEmpmst;
    }

    public void setFhrdEmpmst(FhrdEmpmst fhrdEmpmst) {
        this.fhrdEmpmst = fhrdEmpmst;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ftasBulkAttTempPK != null ? ftasBulkAttTempPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasBulkAttTemp)) {
            return false;
        }
        FtasBulkAttTemp other = (FtasBulkAttTemp) object;
        if ((this.ftasBulkAttTempPK == null && other.ftasBulkAttTempPK != null) || (this.ftasBulkAttTempPK != null && !this.ftasBulkAttTempPK.equals(other.ftasBulkAttTempPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasBulkAttTemp[ ftasBulkAttTempPK=" + ftasBulkAttTempPK + " ]";
    }
    
}
