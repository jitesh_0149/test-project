/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_PORTFOLIO_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdPortfolioMst.findAll", query = "SELECT f FROM FhrdPortfolioMst f"),
    @NamedQuery(name = "FhrdPortfolioMst.findByPmUniqueSrno", query = "SELECT f FROM FhrdPortfolioMst f WHERE f.pmUniqueSrno = :pmUniqueSrno"),
    @NamedQuery(name = "FhrdPortfolioMst.findByPmName", query = "SELECT f FROM FhrdPortfolioMst f WHERE f.pmName = :pmName"),
    @NamedQuery(name = "FhrdPortfolioMst.findByPmDesc", query = "SELECT f FROM FhrdPortfolioMst f WHERE f.pmDesc = :pmDesc"),
    @NamedQuery(name = "FhrdPortfolioMst.findByStartDate", query = "SELECT f FROM FhrdPortfolioMst f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdPortfolioMst.findByEndDate", query = "SELECT f FROM FhrdPortfolioMst f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FhrdPortfolioMst.findByCreatedt", query = "SELECT f FROM FhrdPortfolioMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdPortfolioMst.findByUpdatedt", query = "SELECT f FROM FhrdPortfolioMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdPortfolioMst.findBySystemUserid", query = "SELECT f FROM FhrdPortfolioMst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdPortfolioMst.findByExportFlag", query = "SELECT f FROM FhrdPortfolioMst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdPortfolioMst.findByImportFlag", query = "SELECT f FROM FhrdPortfolioMst f WHERE f.importFlag = :importFlag")})
public class FhrdPortfolioMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PM_UNIQUE_SRNO")
    private Integer pmUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "PM_NAME")
    private String pmName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "PM_DESC")
    private String pmDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @OneToMany(mappedBy = "pmUniqueSrno")
    private Collection<FhrdEmpmst> fhrdEmpmstCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FhrdPortfolioMst() {
    }

    public FhrdPortfolioMst(Integer pmUniqueSrno) {
        this.pmUniqueSrno = pmUniqueSrno;
    }

    public FhrdPortfolioMst(Integer pmUniqueSrno, String pmName, String pmDesc, Date startDate, Date createdt) {
        this.pmUniqueSrno = pmUniqueSrno;
        this.pmName = pmName;
        this.pmDesc = pmDesc;
        this.startDate = startDate;
        this.createdt = createdt;
    }

    public Integer getPmUniqueSrno() {
        return pmUniqueSrno;
    }

    public void setPmUniqueSrno(Integer pmUniqueSrno) {
        this.pmUniqueSrno = pmUniqueSrno;
    }

    public String getPmName() {
        return pmName;
    }

    public void setPmName(String pmName) {
        this.pmName = pmName;
    }

    public String getPmDesc() {
        return pmDesc;
    }

    public void setPmDesc(String pmDesc) {
        this.pmDesc = pmDesc;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    @XmlTransient
    public Collection<FhrdEmpmst> getFhrdEmpmstCollection() {
        return fhrdEmpmstCollection;
    }

    public void setFhrdEmpmstCollection(Collection<FhrdEmpmst> fhrdEmpmstCollection) {
        this.fhrdEmpmstCollection = fhrdEmpmstCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pmUniqueSrno != null ? pmUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdPortfolioMst)) {
            return false;
        }
        FhrdPortfolioMst other = (FhrdPortfolioMst) object;
        if ((this.pmUniqueSrno == null && other.pmUniqueSrno != null) || (this.pmUniqueSrno != null && !this.pmUniqueSrno.equals(other.pmUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdPortfolioMst[ pmUniqueSrno=" + pmUniqueSrno + " ]";
    }
    
}
