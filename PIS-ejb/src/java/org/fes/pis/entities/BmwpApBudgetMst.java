/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "BMWP_AP_BUDGET_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmwpApBudgetMst.findAll", query = "SELECT b FROM BmwpApBudgetMst b"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmUniqueSrno", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmUniqueSrno = :apbmUniqueSrno"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmSrno", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmSrno = :apbmSrno"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmBudgetSrno", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmBudgetSrno = :apbmBudgetSrno"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmBudgetShortName", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmBudgetShortName = :apbmBudgetShortName"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmBudgetDesc", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmBudgetDesc = :apbmBudgetDesc"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmParentBudgetSrno", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmParentBudgetSrno = :apbmParentBudgetSrno"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmBudgetSrnoLevel", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmBudgetSrnoLevel = :apbmBudgetSrnoLevel"),
    @NamedQuery(name = "BmwpApBudgetMst.findByCreatedt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.createdt = :createdt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByUpdatedt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.updatedt = :updatedt"),
    @NamedQuery(name = "BmwpApBudgetMst.findBySystemUserid", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.systemUserid = :systemUserid"),
    @NamedQuery(name = "BmwpApBudgetMst.findByExportFlag", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.exportFlag = :exportFlag"),
    @NamedQuery(name = "BmwpApBudgetMst.findByImportFlag", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.importFlag = :importFlag"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmStartDate", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmStartDate = :apbmStartDate"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmEndDate", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmEndDate = :apbmEndDate"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmCarryFwdAdvAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmCarryFwdAdvAmt = :apbmCarryFwdAdvAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmReceivedFundAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmReceivedFundAmt = :apbmReceivedFundAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmReceivedAdvanceAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmReceivedAdvanceAmt = :apbmReceivedAdvanceAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmSurrenderFundAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmSurrenderFundAmt = :apbmSurrenderFundAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmWorkplanAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmWorkplanAmt = :apbmWorkplanAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmWpSurrenderAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmWpSurrenderAmt = :apbmWpSurrenderAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmReAllotWpAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmReAllotWpAmt = :apbmReAllotWpAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmReviseWpAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmReviseWpAmt = :apbmReviseWpAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmCommitedAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmCommitedAmt = :apbmCommitedAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmPipelinePayAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmPipelinePayAmt = :apbmPipelinePayAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmAdvanceCommitAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmAdvanceCommitAmt = :apbmAdvanceCommitAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmAdvancePayAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmAdvancePayAmt = :apbmAdvancePayAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmPipeAdvadjCommitAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmPipeAdvadjCommitAmt = :apbmPipeAdvadjCommitAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmPipeAdvadjPayAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmPipeAdvadjPayAmt = :apbmPipeAdvadjPayAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmReceiptPayAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmReceiptPayAmt = :apbmReceiptPayAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmReceiptCommitAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmReceiptCommitAmt = :apbmReceiptCommitAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmExpenseAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmExpenseAmt = :apbmExpenseAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmUsableBalanceAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmUsableBalanceAmt = :apbmUsableBalanceAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmBalanceAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmBalanceAmt = :apbmBalanceAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmPipeCommitedAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmPipeCommitedAmt = :apbmPipeCommitedAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmEndDatePossible", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmEndDatePossible = :apbmEndDatePossible"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmLedgerCodeTypeFlag", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmLedgerCodeTypeFlag = :apbmLedgerCodeTypeFlag"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmDrAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmDrAmt = :apbmDrAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmCrAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmCrAmt = :apbmCrAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmChildOfferFlag", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmChildOfferFlag = :apbmChildOfferFlag"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmVirtualBudgetFlag", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmVirtualBudgetFlag = :apbmVirtualBudgetFlag"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmLdrUsableBalanceAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmLdrUsableBalanceAmt = :apbmLdrUsableBalanceAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmLdrBalanceAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmLdrBalanceAmt = :apbmLdrBalanceAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmLedgerCodeSubTypeFlag", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmLedgerCodeSubTypeFlag = :apbmLedgerCodeSubTypeFlag"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmReCreateFlag", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmReCreateFlag = :apbmReCreateFlag"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmBlockByWpAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmBlockByWpAmt = :apbmBlockByWpAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmApplicableForTeams", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmApplicableForTeams = :apbmApplicableForTeams"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmTransactionDate", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmTransactionDate = :apbmTransactionDate"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmAdvanceWithWpFlag", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmAdvanceWithWpFlag = :apbmAdvanceWithWpFlag"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmProCrExpAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmProCrExpAmt = :apbmProCrExpAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmCommContriAmt", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmCommContriAmt = :apbmCommContriAmt"),
    @NamedQuery(name = "BmwpApBudgetMst.findByApbmPmFundMonitorFlag", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.apbmPmFundMonitorFlag = :apbmPmFundMonitorFlag"),
    @NamedQuery(name = "BmwpApBudgetMst.findByPmFundType", query = "SELECT b FROM BmwpApBudgetMst b WHERE b.pmFundType = :pmFundType")})
public class BmwpApBudgetMst implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_UNIQUE_SRNO")
    private BigDecimal apbmUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_SRNO")
    private int apbmSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_BUDGET_SRNO")
    private BigInteger apbmBudgetSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "APBM_BUDGET_SHORT_NAME")
    private String apbmBudgetShortName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "APBM_BUDGET_DESC")
    private String apbmBudgetDesc;
    @Column(name = "APBM_PARENT_BUDGET_SRNO")
    private BigInteger apbmParentBudgetSrno;
    @Column(name = "APBM_BUDGET_SRNO_LEVEL")
    private Integer apbmBudgetSrnoLevel;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date apbmStartDate;
    @Column(name = "APBM_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date apbmEndDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_CARRY_FWD_ADV_AMT")
    private BigDecimal apbmCarryFwdAdvAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_RECEIVED_FUND_AMT")
    private BigDecimal apbmReceivedFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_RECEIVED_ADVANCE_AMT")
    private BigDecimal apbmReceivedAdvanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_SURRENDER_FUND_AMT")
    private BigDecimal apbmSurrenderFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_WORKPLAN_AMT")
    private BigDecimal apbmWorkplanAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_WP_SURRENDER_AMT")
    private BigDecimal apbmWpSurrenderAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_RE_ALLOT_WP_AMT")
    private BigDecimal apbmReAllotWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_REVISE_WP_AMT")
    private BigDecimal apbmReviseWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_COMMITED_AMT")
    private BigDecimal apbmCommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_PIPELINE_PAY_AMT")
    private BigDecimal apbmPipelinePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_ADVANCE_COMMIT_AMT")
    private BigDecimal apbmAdvanceCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_ADVANCE_PAY_AMT")
    private BigDecimal apbmAdvancePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_PIPE_ADVADJ_COMMIT_AMT")
    private BigDecimal apbmPipeAdvadjCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_PIPE_ADVADJ_PAY_AMT")
    private BigDecimal apbmPipeAdvadjPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_RECEIPT_PAY_AMT")
    private BigDecimal apbmReceiptPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_RECEIPT_COMMIT_AMT")
    private BigDecimal apbmReceiptCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_EXPENSE_AMT")
    private BigDecimal apbmExpenseAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_USABLE_BALANCE_AMT")
    private BigDecimal apbmUsableBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_BALANCE_AMT")
    private BigDecimal apbmBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_PIPE_COMMITED_AMT")
    private BigDecimal apbmPipeCommitedAmt;
    @Column(name = "APBM_END_DATE_POSSIBLE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date apbmEndDatePossible;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "APBM_LEDGER_CODE_TYPE_FLAG")
    private String apbmLedgerCodeTypeFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_DR_AMT")
    private BigDecimal apbmDrAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_CR_AMT")
    private BigDecimal apbmCrAmt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "APBM_CHILD_OFFER_FLAG")
    private String apbmChildOfferFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "APBM_VIRTUAL_BUDGET_FLAG")
    private String apbmVirtualBudgetFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_LDR_USABLE_BALANCE_AMT")
    private BigDecimal apbmLdrUsableBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_LDR_BALANCE_AMT")
    private BigDecimal apbmLdrBalanceAmt;
    @Size(max = 1)
    @Column(name = "APBM_LEDGER_CODE_SUB_TYPE_FLAG")
    private String apbmLedgerCodeSubTypeFlag;
    @Size(max = 1)
    @Column(name = "APBM_RE_CREATE_FLAG")
    private String apbmReCreateFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_BLOCK_BY_WP_AMT")
    private BigDecimal apbmBlockByWpAmt;
    @Size(max = 20)
    @Column(name = "APBM_APPLICABLE_FOR_TEAMS")
    private String apbmApplicableForTeams;
    @Column(name = "APBM_TRANSACTION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date apbmTransactionDate;
    @Size(max = 1)
    @Column(name = "APBM_ADVANCE_WITH_WP_FLAG")
    private String apbmAdvanceWithWpFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_PRO_CR_EXP_AMT")
    private BigDecimal apbmProCrExpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "APBM_COMM_CONTRI_AMT")
    private BigDecimal apbmCommContriAmt;
    @Size(max = 1)
    @Column(name = "APBM_PM_FUND_MONITOR_FLAG")
    private String apbmPmFundMonitorFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PM_FUND_TYPE")
    private String pmFundType;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "apablApbmUniqueSrno")
    private Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection;
    @OneToMany(mappedBy = "apabdApbmUniqueSrno")
    private Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "APBM_PM_UNIQUE_SRNO", referencedColumnName = "PM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpProjectMst apbmPmUniqueSrno;
    @JoinColumn(name = "APBM_PTM_UNIQUE_SRNO", referencedColumnName = "PTM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpPartyTypeMst apbmPtmUniqueSrno;
    @JoinColumn(name = "APBM_FAM_UNIQUE_SRNO", referencedColumnName = "FAM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpFundAgencyMst apbmFamUniqueSrno;
    @JoinColumn(name = "APBM_BM_UNIQUE_SRNO", referencedColumnName = "BM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpBudgetMst apbmBmUniqueSrno;
    @OneToMany(mappedBy = "apbmParentUniqueSrno")
    private Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection;
    @JoinColumn(name = "APBM_PARENT_UNIQUE_SRNO", referencedColumnName = "APBM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpApBudgetMst apbmParentUniqueSrno;
    @JoinColumn(name = "APBM_ATM_UNIQUE_SRNO", referencedColumnName = "ATM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpAgencyTypeMst apbmAtmUniqueSrno;
    @JoinColumn(name = "APBM_APL_UNIQUE_SRNO", referencedColumnName = "APL_UNIQUE_SRNO")
    @ManyToOne(optional = false)
    private BmwpAgencyProjectLink apbmAplUniqueSrno;
    @OneToMany(mappedBy = "wpApbmUniqueSrno")
    private Collection<BmwpWorkplan> bmwpWorkplanCollection;

    public BmwpApBudgetMst() {
    }

    public BmwpApBudgetMst(BigDecimal apbmUniqueSrno) {
        this.apbmUniqueSrno = apbmUniqueSrno;
    }

    public BmwpApBudgetMst(BigDecimal apbmUniqueSrno, int apbmSrno, BigInteger apbmBudgetSrno, String apbmBudgetShortName, String apbmBudgetDesc, Date createdt, Date apbmStartDate, BigDecimal apbmCarryFwdAdvAmt, BigDecimal apbmReceivedFundAmt, BigDecimal apbmReceivedAdvanceAmt, BigDecimal apbmSurrenderFundAmt, BigDecimal apbmWorkplanAmt, BigDecimal apbmWpSurrenderAmt, BigDecimal apbmReAllotWpAmt, BigDecimal apbmReviseWpAmt, BigDecimal apbmCommitedAmt, BigDecimal apbmPipelinePayAmt, BigDecimal apbmAdvanceCommitAmt, BigDecimal apbmAdvancePayAmt, BigDecimal apbmPipeAdvadjCommitAmt, BigDecimal apbmPipeAdvadjPayAmt, BigDecimal apbmReceiptPayAmt, BigDecimal apbmReceiptCommitAmt, BigDecimal apbmExpenseAmt, BigDecimal apbmUsableBalanceAmt, BigDecimal apbmBalanceAmt, BigDecimal apbmPipeCommitedAmt, String apbmLedgerCodeTypeFlag, BigDecimal apbmDrAmt, BigDecimal apbmCrAmt, String apbmChildOfferFlag, String apbmVirtualBudgetFlag, BigDecimal apbmLdrUsableBalanceAmt, BigDecimal apbmLdrBalanceAmt, BigDecimal apbmBlockByWpAmt, BigDecimal apbmProCrExpAmt, BigDecimal apbmCommContriAmt, String pmFundType) {
        this.apbmUniqueSrno = apbmUniqueSrno;
        this.apbmSrno = apbmSrno;
        this.apbmBudgetSrno = apbmBudgetSrno;
        this.apbmBudgetShortName = apbmBudgetShortName;
        this.apbmBudgetDesc = apbmBudgetDesc;
        this.createdt = createdt;
        this.apbmStartDate = apbmStartDate;
        this.apbmCarryFwdAdvAmt = apbmCarryFwdAdvAmt;
        this.apbmReceivedFundAmt = apbmReceivedFundAmt;
        this.apbmReceivedAdvanceAmt = apbmReceivedAdvanceAmt;
        this.apbmSurrenderFundAmt = apbmSurrenderFundAmt;
        this.apbmWorkplanAmt = apbmWorkplanAmt;
        this.apbmWpSurrenderAmt = apbmWpSurrenderAmt;
        this.apbmReAllotWpAmt = apbmReAllotWpAmt;
        this.apbmReviseWpAmt = apbmReviseWpAmt;
        this.apbmCommitedAmt = apbmCommitedAmt;
        this.apbmPipelinePayAmt = apbmPipelinePayAmt;
        this.apbmAdvanceCommitAmt = apbmAdvanceCommitAmt;
        this.apbmAdvancePayAmt = apbmAdvancePayAmt;
        this.apbmPipeAdvadjCommitAmt = apbmPipeAdvadjCommitAmt;
        this.apbmPipeAdvadjPayAmt = apbmPipeAdvadjPayAmt;
        this.apbmReceiptPayAmt = apbmReceiptPayAmt;
        this.apbmReceiptCommitAmt = apbmReceiptCommitAmt;
        this.apbmExpenseAmt = apbmExpenseAmt;
        this.apbmUsableBalanceAmt = apbmUsableBalanceAmt;
        this.apbmBalanceAmt = apbmBalanceAmt;
        this.apbmPipeCommitedAmt = apbmPipeCommitedAmt;
        this.apbmLedgerCodeTypeFlag = apbmLedgerCodeTypeFlag;
        this.apbmDrAmt = apbmDrAmt;
        this.apbmCrAmt = apbmCrAmt;
        this.apbmChildOfferFlag = apbmChildOfferFlag;
        this.apbmVirtualBudgetFlag = apbmVirtualBudgetFlag;
        this.apbmLdrUsableBalanceAmt = apbmLdrUsableBalanceAmt;
        this.apbmLdrBalanceAmt = apbmLdrBalanceAmt;
        this.apbmBlockByWpAmt = apbmBlockByWpAmt;
        this.apbmProCrExpAmt = apbmProCrExpAmt;
        this.apbmCommContriAmt = apbmCommContriAmt;
        this.pmFundType = pmFundType;
    }

    public BigDecimal getApbmUniqueSrno() {
        return apbmUniqueSrno;
    }

    public void setApbmUniqueSrno(BigDecimal apbmUniqueSrno) {
        this.apbmUniqueSrno = apbmUniqueSrno;
    }

    public int getApbmSrno() {
        return apbmSrno;
    }

    public void setApbmSrno(int apbmSrno) {
        this.apbmSrno = apbmSrno;
    }

    public BigInteger getApbmBudgetSrno() {
        return apbmBudgetSrno;
    }

    public void setApbmBudgetSrno(BigInteger apbmBudgetSrno) {
        this.apbmBudgetSrno = apbmBudgetSrno;
    }

    public String getApbmBudgetShortName() {
        return apbmBudgetShortName;
    }

    public void setApbmBudgetShortName(String apbmBudgetShortName) {
        this.apbmBudgetShortName = apbmBudgetShortName;
    }

    public String getApbmBudgetDesc() {
        return apbmBudgetDesc;
    }

    public void setApbmBudgetDesc(String apbmBudgetDesc) {
        this.apbmBudgetDesc = apbmBudgetDesc;
    }

    public BigInteger getApbmParentBudgetSrno() {
        return apbmParentBudgetSrno;
    }

    public void setApbmParentBudgetSrno(BigInteger apbmParentBudgetSrno) {
        this.apbmParentBudgetSrno = apbmParentBudgetSrno;
    }

    public Integer getApbmBudgetSrnoLevel() {
        return apbmBudgetSrnoLevel;
    }

    public void setApbmBudgetSrnoLevel(Integer apbmBudgetSrnoLevel) {
        this.apbmBudgetSrnoLevel = apbmBudgetSrnoLevel;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Date getApbmStartDate() {
        return apbmStartDate;
    }

    public void setApbmStartDate(Date apbmStartDate) {
        this.apbmStartDate = apbmStartDate;
    }

    public Date getApbmEndDate() {
        return apbmEndDate;
    }

    public void setApbmEndDate(Date apbmEndDate) {
        this.apbmEndDate = apbmEndDate;
    }

    public BigDecimal getApbmCarryFwdAdvAmt() {
        return apbmCarryFwdAdvAmt;
    }

    public void setApbmCarryFwdAdvAmt(BigDecimal apbmCarryFwdAdvAmt) {
        this.apbmCarryFwdAdvAmt = apbmCarryFwdAdvAmt;
    }

    public BigDecimal getApbmReceivedFundAmt() {
        return apbmReceivedFundAmt;
    }

    public void setApbmReceivedFundAmt(BigDecimal apbmReceivedFundAmt) {
        this.apbmReceivedFundAmt = apbmReceivedFundAmt;
    }

    public BigDecimal getApbmReceivedAdvanceAmt() {
        return apbmReceivedAdvanceAmt;
    }

    public void setApbmReceivedAdvanceAmt(BigDecimal apbmReceivedAdvanceAmt) {
        this.apbmReceivedAdvanceAmt = apbmReceivedAdvanceAmt;
    }

    public BigDecimal getApbmSurrenderFundAmt() {
        return apbmSurrenderFundAmt;
    }

    public void setApbmSurrenderFundAmt(BigDecimal apbmSurrenderFundAmt) {
        this.apbmSurrenderFundAmt = apbmSurrenderFundAmt;
    }

    public BigDecimal getApbmWorkplanAmt() {
        return apbmWorkplanAmt;
    }

    public void setApbmWorkplanAmt(BigDecimal apbmWorkplanAmt) {
        this.apbmWorkplanAmt = apbmWorkplanAmt;
    }

    public BigDecimal getApbmWpSurrenderAmt() {
        return apbmWpSurrenderAmt;
    }

    public void setApbmWpSurrenderAmt(BigDecimal apbmWpSurrenderAmt) {
        this.apbmWpSurrenderAmt = apbmWpSurrenderAmt;
    }

    public BigDecimal getApbmReAllotWpAmt() {
        return apbmReAllotWpAmt;
    }

    public void setApbmReAllotWpAmt(BigDecimal apbmReAllotWpAmt) {
        this.apbmReAllotWpAmt = apbmReAllotWpAmt;
    }

    public BigDecimal getApbmReviseWpAmt() {
        return apbmReviseWpAmt;
    }

    public void setApbmReviseWpAmt(BigDecimal apbmReviseWpAmt) {
        this.apbmReviseWpAmt = apbmReviseWpAmt;
    }

    public BigDecimal getApbmCommitedAmt() {
        return apbmCommitedAmt;
    }

    public void setApbmCommitedAmt(BigDecimal apbmCommitedAmt) {
        this.apbmCommitedAmt = apbmCommitedAmt;
    }

    public BigDecimal getApbmPipelinePayAmt() {
        return apbmPipelinePayAmt;
    }

    public void setApbmPipelinePayAmt(BigDecimal apbmPipelinePayAmt) {
        this.apbmPipelinePayAmt = apbmPipelinePayAmt;
    }

    public BigDecimal getApbmAdvanceCommitAmt() {
        return apbmAdvanceCommitAmt;
    }

    public void setApbmAdvanceCommitAmt(BigDecimal apbmAdvanceCommitAmt) {
        this.apbmAdvanceCommitAmt = apbmAdvanceCommitAmt;
    }

    public BigDecimal getApbmAdvancePayAmt() {
        return apbmAdvancePayAmt;
    }

    public void setApbmAdvancePayAmt(BigDecimal apbmAdvancePayAmt) {
        this.apbmAdvancePayAmt = apbmAdvancePayAmt;
    }

    public BigDecimal getApbmPipeAdvadjCommitAmt() {
        return apbmPipeAdvadjCommitAmt;
    }

    public void setApbmPipeAdvadjCommitAmt(BigDecimal apbmPipeAdvadjCommitAmt) {
        this.apbmPipeAdvadjCommitAmt = apbmPipeAdvadjCommitAmt;
    }

    public BigDecimal getApbmPipeAdvadjPayAmt() {
        return apbmPipeAdvadjPayAmt;
    }

    public void setApbmPipeAdvadjPayAmt(BigDecimal apbmPipeAdvadjPayAmt) {
        this.apbmPipeAdvadjPayAmt = apbmPipeAdvadjPayAmt;
    }

    public BigDecimal getApbmReceiptPayAmt() {
        return apbmReceiptPayAmt;
    }

    public void setApbmReceiptPayAmt(BigDecimal apbmReceiptPayAmt) {
        this.apbmReceiptPayAmt = apbmReceiptPayAmt;
    }

    public BigDecimal getApbmReceiptCommitAmt() {
        return apbmReceiptCommitAmt;
    }

    public void setApbmReceiptCommitAmt(BigDecimal apbmReceiptCommitAmt) {
        this.apbmReceiptCommitAmt = apbmReceiptCommitAmt;
    }

    public BigDecimal getApbmExpenseAmt() {
        return apbmExpenseAmt;
    }

    public void setApbmExpenseAmt(BigDecimal apbmExpenseAmt) {
        this.apbmExpenseAmt = apbmExpenseAmt;
    }

    public BigDecimal getApbmUsableBalanceAmt() {
        return apbmUsableBalanceAmt;
    }

    public void setApbmUsableBalanceAmt(BigDecimal apbmUsableBalanceAmt) {
        this.apbmUsableBalanceAmt = apbmUsableBalanceAmt;
    }

    public BigDecimal getApbmBalanceAmt() {
        return apbmBalanceAmt;
    }

    public void setApbmBalanceAmt(BigDecimal apbmBalanceAmt) {
        this.apbmBalanceAmt = apbmBalanceAmt;
    }

    public BigDecimal getApbmPipeCommitedAmt() {
        return apbmPipeCommitedAmt;
    }

    public void setApbmPipeCommitedAmt(BigDecimal apbmPipeCommitedAmt) {
        this.apbmPipeCommitedAmt = apbmPipeCommitedAmt;
    }

    public Date getApbmEndDatePossible() {
        return apbmEndDatePossible;
    }

    public void setApbmEndDatePossible(Date apbmEndDatePossible) {
        this.apbmEndDatePossible = apbmEndDatePossible;
    }

    public String getApbmLedgerCodeTypeFlag() {
        return apbmLedgerCodeTypeFlag;
    }

    public void setApbmLedgerCodeTypeFlag(String apbmLedgerCodeTypeFlag) {
        this.apbmLedgerCodeTypeFlag = apbmLedgerCodeTypeFlag;
    }

    public BigDecimal getApbmDrAmt() {
        return apbmDrAmt;
    }

    public void setApbmDrAmt(BigDecimal apbmDrAmt) {
        this.apbmDrAmt = apbmDrAmt;
    }

    public BigDecimal getApbmCrAmt() {
        return apbmCrAmt;
    }

    public void setApbmCrAmt(BigDecimal apbmCrAmt) {
        this.apbmCrAmt = apbmCrAmt;
    }

    public String getApbmChildOfferFlag() {
        return apbmChildOfferFlag;
    }

    public void setApbmChildOfferFlag(String apbmChildOfferFlag) {
        this.apbmChildOfferFlag = apbmChildOfferFlag;
    }

    public String getApbmVirtualBudgetFlag() {
        return apbmVirtualBudgetFlag;
    }

    public void setApbmVirtualBudgetFlag(String apbmVirtualBudgetFlag) {
        this.apbmVirtualBudgetFlag = apbmVirtualBudgetFlag;
    }

    public BigDecimal getApbmLdrUsableBalanceAmt() {
        return apbmLdrUsableBalanceAmt;
    }

    public void setApbmLdrUsableBalanceAmt(BigDecimal apbmLdrUsableBalanceAmt) {
        this.apbmLdrUsableBalanceAmt = apbmLdrUsableBalanceAmt;
    }

    public BigDecimal getApbmLdrBalanceAmt() {
        return apbmLdrBalanceAmt;
    }

    public void setApbmLdrBalanceAmt(BigDecimal apbmLdrBalanceAmt) {
        this.apbmLdrBalanceAmt = apbmLdrBalanceAmt;
    }

    public String getApbmLedgerCodeSubTypeFlag() {
        return apbmLedgerCodeSubTypeFlag;
    }

    public void setApbmLedgerCodeSubTypeFlag(String apbmLedgerCodeSubTypeFlag) {
        this.apbmLedgerCodeSubTypeFlag = apbmLedgerCodeSubTypeFlag;
    }

    public String getApbmReCreateFlag() {
        return apbmReCreateFlag;
    }

    public void setApbmReCreateFlag(String apbmReCreateFlag) {
        this.apbmReCreateFlag = apbmReCreateFlag;
    }

    public BigDecimal getApbmBlockByWpAmt() {
        return apbmBlockByWpAmt;
    }

    public void setApbmBlockByWpAmt(BigDecimal apbmBlockByWpAmt) {
        this.apbmBlockByWpAmt = apbmBlockByWpAmt;
    }

    public String getApbmApplicableForTeams() {
        return apbmApplicableForTeams;
    }

    public void setApbmApplicableForTeams(String apbmApplicableForTeams) {
        this.apbmApplicableForTeams = apbmApplicableForTeams;
    }

    public Date getApbmTransactionDate() {
        return apbmTransactionDate;
    }

    public void setApbmTransactionDate(Date apbmTransactionDate) {
        this.apbmTransactionDate = apbmTransactionDate;
    }

    public String getApbmAdvanceWithWpFlag() {
        return apbmAdvanceWithWpFlag;
    }

    public void setApbmAdvanceWithWpFlag(String apbmAdvanceWithWpFlag) {
        this.apbmAdvanceWithWpFlag = apbmAdvanceWithWpFlag;
    }

    public BigDecimal getApbmProCrExpAmt() {
        return apbmProCrExpAmt;
    }

    public void setApbmProCrExpAmt(BigDecimal apbmProCrExpAmt) {
        this.apbmProCrExpAmt = apbmProCrExpAmt;
    }

    public BigDecimal getApbmCommContriAmt() {
        return apbmCommContriAmt;
    }

    public void setApbmCommContriAmt(BigDecimal apbmCommContriAmt) {
        this.apbmCommContriAmt = apbmCommContriAmt;
    }

    public String getApbmPmFundMonitorFlag() {
        return apbmPmFundMonitorFlag;
    }

    public void setApbmPmFundMonitorFlag(String apbmPmFundMonitorFlag) {
        this.apbmPmFundMonitorFlag = apbmPmFundMonitorFlag;
    }

    public String getPmFundType() {
        return pmFundType;
    }

    public void setPmFundType(String pmFundType) {
        this.pmFundType = pmFundType;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetLink> getBmwpApActivityBudgetLinkCollection() {
        return bmwpApActivityBudgetLinkCollection;
    }

    public void setBmwpApActivityBudgetLinkCollection(Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection) {
        this.bmwpApActivityBudgetLinkCollection = bmwpApActivityBudgetLinkCollection;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetDtl> getBmwpApActivityBudgetDtlCollection() {
        return bmwpApActivityBudgetDtlCollection;
    }

    public void setBmwpApActivityBudgetDtlCollection(Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection) {
        this.bmwpApActivityBudgetDtlCollection = bmwpApActivityBudgetDtlCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public BmwpProjectMst getApbmPmUniqueSrno() {
        return apbmPmUniqueSrno;
    }

    public void setApbmPmUniqueSrno(BmwpProjectMst apbmPmUniqueSrno) {
        this.apbmPmUniqueSrno = apbmPmUniqueSrno;
    }

    public BmwpPartyTypeMst getApbmPtmUniqueSrno() {
        return apbmPtmUniqueSrno;
    }

    public void setApbmPtmUniqueSrno(BmwpPartyTypeMst apbmPtmUniqueSrno) {
        this.apbmPtmUniqueSrno = apbmPtmUniqueSrno;
    }

    public BmwpFundAgencyMst getApbmFamUniqueSrno() {
        return apbmFamUniqueSrno;
    }

    public void setApbmFamUniqueSrno(BmwpFundAgencyMst apbmFamUniqueSrno) {
        this.apbmFamUniqueSrno = apbmFamUniqueSrno;
    }

    public BmwpBudgetMst getApbmBmUniqueSrno() {
        return apbmBmUniqueSrno;
    }

    public void setApbmBmUniqueSrno(BmwpBudgetMst apbmBmUniqueSrno) {
        this.apbmBmUniqueSrno = apbmBmUniqueSrno;
    }

    @XmlTransient
    public Collection<BmwpApBudgetMst> getBmwpApBudgetMstCollection() {
        return bmwpApBudgetMstCollection;
    }

    public void setBmwpApBudgetMstCollection(Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection) {
        this.bmwpApBudgetMstCollection = bmwpApBudgetMstCollection;
    }

    public BmwpApBudgetMst getApbmParentUniqueSrno() {
        return apbmParentUniqueSrno;
    }

    public void setApbmParentUniqueSrno(BmwpApBudgetMst apbmParentUniqueSrno) {
        this.apbmParentUniqueSrno = apbmParentUniqueSrno;
    }

    public BmwpAgencyTypeMst getApbmAtmUniqueSrno() {
        return apbmAtmUniqueSrno;
    }

    public void setApbmAtmUniqueSrno(BmwpAgencyTypeMst apbmAtmUniqueSrno) {
        this.apbmAtmUniqueSrno = apbmAtmUniqueSrno;
    }

    public BmwpAgencyProjectLink getApbmAplUniqueSrno() {
        return apbmAplUniqueSrno;
    }

    public void setApbmAplUniqueSrno(BmwpAgencyProjectLink apbmAplUniqueSrno) {
        this.apbmAplUniqueSrno = apbmAplUniqueSrno;
    }

    @XmlTransient
    public Collection<BmwpWorkplan> getBmwpWorkplanCollection() {
        return bmwpWorkplanCollection;
    }

    public void setBmwpWorkplanCollection(Collection<BmwpWorkplan> bmwpWorkplanCollection) {
        this.bmwpWorkplanCollection = bmwpWorkplanCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (apbmUniqueSrno != null ? apbmUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmwpApBudgetMst)) {
            return false;
        }
        BmwpApBudgetMst other = (BmwpApBudgetMst) object;
        if ((this.apbmUniqueSrno == null && other.apbmUniqueSrno != null) || (this.apbmUniqueSrno != null && !this.apbmUniqueSrno.equals(other.apbmUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.BmwpApBudgetMst[ apbmUniqueSrno=" + apbmUniqueSrno + " ]";
    }
    
}
