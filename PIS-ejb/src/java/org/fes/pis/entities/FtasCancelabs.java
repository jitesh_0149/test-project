/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_CANCELABS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasCancelabs.findAll", query = "SELECT f FROM FtasCancelabs f"),
    @NamedQuery(name = "FtasCancelabs.findByEmpno", query = "SELECT f FROM FtasCancelabs f WHERE f.empno = :empno"),
    @NamedQuery(name = "FtasCancelabs.findBySrno", query = "SELECT f FROM FtasCancelabs f WHERE f.srno = :srno"),
    @NamedQuery(name = "FtasCancelabs.findByCancelSrno", query = "SELECT f FROM FtasCancelabs f WHERE f.cancelSrno = :cancelSrno"),
    @NamedQuery(name = "FtasCancelabs.findByReason", query = "SELECT f FROM FtasCancelabs f WHERE f.reason = :reason"),
    @NamedQuery(name = "FtasCancelabs.findByAprvDate", query = "SELECT f FROM FtasCancelabs f WHERE f.aprvDate = :aprvDate"),
    @NamedQuery(name = "FtasCancelabs.findByAprvcomm", query = "SELECT f FROM FtasCancelabs f WHERE f.aprvcomm = :aprvcomm"),
    @NamedQuery(name = "FtasCancelabs.findByUpdatedt", query = "SELECT f FROM FtasCancelabs f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasCancelabs.findByOumUnitSrno", query = "SELECT f FROM FtasCancelabs f WHERE f.oumUnitSrno = :oumUnitSrno"),
    @NamedQuery(name = "FtasCancelabs.findByCreatedt", query = "SELECT f FROM FtasCancelabs f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasCancelabs.findByExportFlag", query = "SELECT f FROM FtasCancelabs f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FtasCancelabs.findByImportFlag", query = "SELECT f FROM FtasCancelabs f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FtasCancelabs.findBySystemUserid", query = "SELECT f FROM FtasCancelabs f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FtasCancelabs.findBySubSrno", query = "SELECT f FROM FtasCancelabs f WHERE f.subSrno = :subSrno"),
    @NamedQuery(name = "FtasCancelabs.findByCancelSubSrno", query = "SELECT f FROM FtasCancelabs f WHERE f.cancelSubSrno = :cancelSubSrno"),
    @NamedQuery(name = "FtasCancelabs.findByAbsOumUnitSrno", query = "SELECT f FROM FtasCancelabs f WHERE f.absOumUnitSrno = :absOumUnitSrno"),
    @NamedQuery(name = "FtasCancelabs.findByCabsSrgKey", query = "SELECT f FROM FtasCancelabs f WHERE f.cabsSrgKey = :cabsSrgKey"),
    @NamedQuery(name = "FtasCancelabs.findByEmplbSrgKey", query = "SELECT f FROM FtasCancelabs f WHERE f.emplbSrgKey = :emplbSrgKey"),
    @NamedQuery(name = "FtasCancelabs.findByTranYear", query = "SELECT f FROM FtasCancelabs f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FtasCancelabs.findByArcomment", query = "SELECT f FROM FtasCancelabs f WHERE f.arcomment = :arcomment"),
    @NamedQuery(name = "FtasCancelabs.findByFromDate", query = "SELECT f FROM FtasCancelabs f WHERE f.fromDate = :fromDate"),
    @NamedQuery(name = "FtasCancelabs.findByToDate", query = "SELECT f FROM FtasCancelabs f WHERE f.toDate = :toDate"),
    @NamedQuery(name = "FtasCancelabs.findByFromhalf", query = "SELECT f FROM FtasCancelabs f WHERE f.fromhalf = :fromhalf"),
    @NamedQuery(name = "FtasCancelabs.findByTohalf", query = "SELECT f FROM FtasCancelabs f WHERE f.tohalf = :tohalf"),
    @NamedQuery(name = "FtasCancelabs.findByPartialCancelFlag", query = "SELECT f FROM FtasCancelabs f WHERE f.partialCancelFlag = :partialCancelFlag")})
public class FtasCancelabs implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "EMPNO")
    private Integer empno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SRNO")
    private int srno;
    @Column(name = "CANCEL_SRNO")
    private Integer cancelSrno;
    @Size(max = 256)
    @Column(name = "REASON")
    private String reason;
    @Column(name = "APRV_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date aprvDate;
    @Size(max = 1)
    @Column(name = "APRVCOMM")
    private String aprvcomm;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OUM_UNIT_SRNO")
    private int oumUnitSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SUB_SRNO")
    private int subSrno;
    @Column(name = "CANCEL_SUB_SRNO")
    private Short cancelSubSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ABS_OUM_UNIT_SRNO")
    private int absOumUnitSrno;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CABS_SRG_KEY")
    private BigDecimal cabsSrgKey;
    @Column(name = "EMPLB_SRG_KEY")
    private BigDecimal emplbSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Size(max = 500)
    @Column(name = "ARCOMMENT")
    private String arcomment;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fromDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date toDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "FROMHALF")
    private String fromhalf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "TOHALF")
    private String tohalf;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PARTIAL_CANCEL_FLAG")
    private String partialCancelFlag;
    @OneToMany(mappedBy = "cabsSrgKey")
    private Collection<FtasAbsentees> ftasAbsenteesCollection;
    @JoinColumn(name = "ABS_SRG_KEY", referencedColumnName = "ABS_SRG_KEY")
    @ManyToOne
    private FtasAbsentees absSrgKey;
    @JoinColumn(name = "APRVBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst aprvby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "ACTNBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst actnby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst userId;

    public FtasCancelabs() {
    }

    public FtasCancelabs(BigDecimal cabsSrgKey) {
        this.cabsSrgKey = cabsSrgKey;
    }

    public FtasCancelabs(BigDecimal cabsSrgKey, int srno, int oumUnitSrno, Date createdt, int subSrno, int absOumUnitSrno, short tranYear, Date fromDate, Date toDate, String fromhalf, String tohalf, String partialCancelFlag) {
        this.cabsSrgKey = cabsSrgKey;
        this.srno = srno;
        this.oumUnitSrno = oumUnitSrno;
        this.createdt = createdt;
        this.subSrno = subSrno;
        this.absOumUnitSrno = absOumUnitSrno;
        this.tranYear = tranYear;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.fromhalf = fromhalf;
        this.tohalf = tohalf;
        this.partialCancelFlag = partialCancelFlag;
    }

    public Integer getEmpno() {
        return empno;
    }

    public void setEmpno(Integer empno) {
        this.empno = empno;
    }

    public int getSrno() {
        return srno;
    }

    public void setSrno(int srno) {
        this.srno = srno;
    }

    public Integer getCancelSrno() {
        return cancelSrno;
    }

    public void setCancelSrno(Integer cancelSrno) {
        this.cancelSrno = cancelSrno;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getAprvDate() {
        return aprvDate;
    }

    public void setAprvDate(Date aprvDate) {
        this.aprvDate = aprvDate;
    }

    public String getAprvcomm() {
        return aprvcomm;
    }

    public void setAprvcomm(String aprvcomm) {
        this.aprvcomm = aprvcomm;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public int getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(int oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public int getSubSrno() {
        return subSrno;
    }

    public void setSubSrno(int subSrno) {
        this.subSrno = subSrno;
    }

    public Short getCancelSubSrno() {
        return cancelSubSrno;
    }

    public void setCancelSubSrno(Short cancelSubSrno) {
        this.cancelSubSrno = cancelSubSrno;
    }

    public int getAbsOumUnitSrno() {
        return absOumUnitSrno;
    }

    public void setAbsOumUnitSrno(int absOumUnitSrno) {
        this.absOumUnitSrno = absOumUnitSrno;
    }

    public BigDecimal getCabsSrgKey() {
        return cabsSrgKey;
    }

    public void setCabsSrgKey(BigDecimal cabsSrgKey) {
        this.cabsSrgKey = cabsSrgKey;
    }

    public BigDecimal getEmplbSrgKey() {
        return emplbSrgKey;
    }

    public void setEmplbSrgKey(BigDecimal emplbSrgKey) {
        this.emplbSrgKey = emplbSrgKey;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public String getArcomment() {
        return arcomment;
    }

    public void setArcomment(String arcomment) {
        this.arcomment = arcomment;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public String getFromhalf() {
        return fromhalf;
    }

    public void setFromhalf(String fromhalf) {
        this.fromhalf = fromhalf;
    }

    public String getTohalf() {
        return tohalf;
    }

    public void setTohalf(String tohalf) {
        this.tohalf = tohalf;
    }

    public String getPartialCancelFlag() {
        return partialCancelFlag;
    }

    public void setPartialCancelFlag(String partialCancelFlag) {
        this.partialCancelFlag = partialCancelFlag;
    }

    @XmlTransient
    public Collection<FtasAbsentees> getFtasAbsenteesCollection() {
        return ftasAbsenteesCollection;
    }

    public void setFtasAbsenteesCollection(Collection<FtasAbsentees> ftasAbsenteesCollection) {
        this.ftasAbsenteesCollection = ftasAbsenteesCollection;
    }

    public FtasAbsentees getAbsSrgKey() {
        return absSrgKey;
    }

    public void setAbsSrgKey(FtasAbsentees absSrgKey) {
        this.absSrgKey = absSrgKey;
    }

    public FhrdEmpmst getAprvby() {
        return aprvby;
    }

    public void setAprvby(FhrdEmpmst aprvby) {
        this.aprvby = aprvby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getActnby() {
        return actnby;
    }

    public void setActnby(FhrdEmpmst actnby) {
        this.actnby = actnby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getUserId() {
        return userId;
    }

    public void setUserId(FhrdEmpmst userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cabsSrgKey != null ? cabsSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasCancelabs)) {
            return false;
        }
        FtasCancelabs other = (FtasCancelabs) object;
        if ((this.cabsSrgKey == null && other.cabsSrgKey != null) || (this.cabsSrgKey != null && !this.cabsSrgKey.equals(other.cabsSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasCancelabs[ cabsSrgKey=" + cabsSrgKey + " ]";
    }
    
}
