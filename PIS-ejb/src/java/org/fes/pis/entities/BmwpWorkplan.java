/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "BMWP_WORKPLAN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmwpWorkplan.findAll", query = "SELECT b FROM BmwpWorkplan b"),
    @NamedQuery(name = "BmwpWorkplan.findByWpUniqueSrno", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpUniqueSrno = :wpUniqueSrno"),
    @NamedQuery(name = "BmwpWorkplan.findByWpFinyear", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpFinyear = :wpFinyear"),
    @NamedQuery(name = "BmwpWorkplan.findByWpSrno", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpSrno = :wpSrno"),
    @NamedQuery(name = "BmwpWorkplan.findByWpPercentOfAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpPercentOfAmt = :wpPercentOfAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearActiInUnit", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearActiInUnit = :wpYearActiInUnit"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearWorkplanAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearWorkplanAmt = :wpYearWorkplanAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1WorkplanAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1WorkplanAmt = :wpQtr1WorkplanAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2WorkplanAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2WorkplanAmt = :wpQtr2WorkplanAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3WorkplanAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3WorkplanAmt = :wpQtr3WorkplanAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4WorkplanAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4WorkplanAmt = :wpQtr4WorkplanAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearReceivedAdvanceAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearReceivedAdvanceAmt = :wpYearReceivedAdvanceAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1ReceivedAdvanceAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1ReceivedAdvanceAmt = :wpQtr1ReceivedAdvanceAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2ReceivedAdvanceAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2ReceivedAdvanceAmt = :wpQtr2ReceivedAdvanceAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3ReceivedAdvanceAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3ReceivedAdvanceAmt = :wpQtr3ReceivedAdvanceAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4ReceivedAdvanceAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4ReceivedAdvanceAmt = :wpQtr4ReceivedAdvanceAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearReAllotWpAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearReAllotWpAmt = :wpYearReAllotWpAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1ReAllotWpAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1ReAllotWpAmt = :wpQtr1ReAllotWpAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2ReAllotWpAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2ReAllotWpAmt = :wpQtr2ReAllotWpAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3ReAllotWpAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3ReAllotWpAmt = :wpQtr3ReAllotWpAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4ReAllotWpAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4ReAllotWpAmt = :wpQtr4ReAllotWpAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearWpSurrenderAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearWpSurrenderAmt = :wpYearWpSurrenderAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1WpSurrenderAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1WpSurrenderAmt = :wpQtr1WpSurrenderAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2WpSurrenderAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2WpSurrenderAmt = :wpQtr2WpSurrenderAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3WpSurrenderAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3WpSurrenderAmt = :wpQtr3WpSurrenderAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4WpSurrenderAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4WpSurrenderAmt = :wpQtr4WpSurrenderAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearReviseWpAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearReviseWpAmt = :wpYearReviseWpAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1ReviseWpAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1ReviseWpAmt = :wpQtr1ReviseWpAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2ReviseWpAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2ReviseWpAmt = :wpQtr2ReviseWpAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3ReviseWpAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3ReviseWpAmt = :wpQtr3ReviseWpAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4ReviseWpAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4ReviseWpAmt = :wpQtr4ReviseWpAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearCommitedAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearCommitedAmt = :wpYearCommitedAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1CommitedAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1CommitedAmt = :wpQtr1CommitedAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2CommitedAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2CommitedAmt = :wpQtr2CommitedAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3CommitedAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3CommitedAmt = :wpQtr3CommitedAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4CommitedAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4CommitedAmt = :wpQtr4CommitedAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearPipelinePayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearPipelinePayAmt = :wpYearPipelinePayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1PipelinePayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1PipelinePayAmt = :wpQtr1PipelinePayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2PipelinePayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2PipelinePayAmt = :wpQtr2PipelinePayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3PipelinePayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3PipelinePayAmt = :wpQtr3PipelinePayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4PipelinePayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4PipelinePayAmt = :wpQtr4PipelinePayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearAdvanceCommitAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearAdvanceCommitAmt = :wpYearAdvanceCommitAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1AdvanceCommitAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1AdvanceCommitAmt = :wpQtr1AdvanceCommitAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2AdvanceCommitAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2AdvanceCommitAmt = :wpQtr2AdvanceCommitAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3AdvanceCommitAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3AdvanceCommitAmt = :wpQtr3AdvanceCommitAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4AdvanceCommitAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4AdvanceCommitAmt = :wpQtr4AdvanceCommitAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearAdvancePayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearAdvancePayAmt = :wpYearAdvancePayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1AdvancePayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1AdvancePayAmt = :wpQtr1AdvancePayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2AdvancePayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2AdvancePayAmt = :wpQtr2AdvancePayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3AdvancePayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3AdvancePayAmt = :wpQtr3AdvancePayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4AdvancePayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4AdvancePayAmt = :wpQtr4AdvancePayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYrPipeAdvadjCommitAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYrPipeAdvadjCommitAmt = :wpYrPipeAdvadjCommitAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQ1PipeAdvadjCommitAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQ1PipeAdvadjCommitAmt = :wpQ1PipeAdvadjCommitAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQ2PipeAdvadjCommitAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQ2PipeAdvadjCommitAmt = :wpQ2PipeAdvadjCommitAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQ3PipeAdvadjCommitAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQ3PipeAdvadjCommitAmt = :wpQ3PipeAdvadjCommitAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQ4PipeAdvadjCommitAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQ4PipeAdvadjCommitAmt = :wpQ4PipeAdvadjCommitAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearPipeAdvadjPayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearPipeAdvadjPayAmt = :wpYearPipeAdvadjPayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1PipeAdvadjPayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1PipeAdvadjPayAmt = :wpQtr1PipeAdvadjPayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2PipeAdvadjPayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2PipeAdvadjPayAmt = :wpQtr2PipeAdvadjPayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3PipeAdvadjPayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3PipeAdvadjPayAmt = :wpQtr3PipeAdvadjPayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4PipeAdvadjPayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4PipeAdvadjPayAmt = :wpQtr4PipeAdvadjPayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearReceiptPayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearReceiptPayAmt = :wpYearReceiptPayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1ReceiptPayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1ReceiptPayAmt = :wpQtr1ReceiptPayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2ReceiptPayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2ReceiptPayAmt = :wpQtr2ReceiptPayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3ReceiptPayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3ReceiptPayAmt = :wpQtr3ReceiptPayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4ReceiptPayAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4ReceiptPayAmt = :wpQtr4ReceiptPayAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearReceiptCommitAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearReceiptCommitAmt = :wpYearReceiptCommitAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1ReceiptCommitAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1ReceiptCommitAmt = :wpQtr1ReceiptCommitAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2ReceiptCommitAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2ReceiptCommitAmt = :wpQtr2ReceiptCommitAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3ReceiptCommitAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3ReceiptCommitAmt = :wpQtr3ReceiptCommitAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4ReceiptCommitAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4ReceiptCommitAmt = :wpQtr4ReceiptCommitAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearExpenseAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearExpenseAmt = :wpYearExpenseAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1ExpenseAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1ExpenseAmt = :wpQtr1ExpenseAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2ExpenseAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2ExpenseAmt = :wpQtr2ExpenseAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3ExpenseAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3ExpenseAmt = :wpQtr3ExpenseAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4ExpenseAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4ExpenseAmt = :wpQtr4ExpenseAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearUsableBalanceAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearUsableBalanceAmt = :wpYearUsableBalanceAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1UsableBalanceAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1UsableBalanceAmt = :wpQtr1UsableBalanceAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2UsableBalanceAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2UsableBalanceAmt = :wpQtr2UsableBalanceAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3UsableBalanceAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3UsableBalanceAmt = :wpQtr3UsableBalanceAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4UsableBalanceAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4UsableBalanceAmt = :wpQtr4UsableBalanceAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearBalanceAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearBalanceAmt = :wpYearBalanceAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1BalanceAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1BalanceAmt = :wpQtr1BalanceAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2BalanceAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2BalanceAmt = :wpQtr2BalanceAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3BalanceAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3BalanceAmt = :wpQtr3BalanceAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4BalanceAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4BalanceAmt = :wpQtr4BalanceAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearCarryFwdAdvAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearCarryFwdAdvAmt = :wpYearCarryFwdAdvAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1CarryFwdAdvAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1CarryFwdAdvAmt = :wpQtr1CarryFwdAdvAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2CarryFwdAdvAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2CarryFwdAdvAmt = :wpQtr2CarryFwdAdvAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3CarryFwdAdvAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3CarryFwdAdvAmt = :wpQtr3CarryFwdAdvAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4CarryFwdAdvAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4CarryFwdAdvAmt = :wpQtr4CarryFwdAdvAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpVillagecd", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpVillagecd = :wpVillagecd"),
    @NamedQuery(name = "BmwpWorkplan.findByWpSurveyNo", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpSurveyNo = :wpSurveyNo"),
    @NamedQuery(name = "BmwpWorkplan.findByCreatedt", query = "SELECT b FROM BmwpWorkplan b WHERE b.createdt = :createdt"),
    @NamedQuery(name = "BmwpWorkplan.findByUpdatedt", query = "SELECT b FROM BmwpWorkplan b WHERE b.updatedt = :updatedt"),
    @NamedQuery(name = "BmwpWorkplan.findBySystemUserid", query = "SELECT b FROM BmwpWorkplan b WHERE b.systemUserid = :systemUserid"),
    @NamedQuery(name = "BmwpWorkplan.findByExportFlag", query = "SELECT b FROM BmwpWorkplan b WHERE b.exportFlag = :exportFlag"),
    @NamedQuery(name = "BmwpWorkplan.findByImportFlag", query = "SELECT b FROM BmwpWorkplan b WHERE b.importFlag = :importFlag"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearPipeCommitedAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearPipeCommitedAmt = :wpYearPipeCommitedAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1PipeCommitedAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1PipeCommitedAmt = :wpQtr1PipeCommitedAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2PipeCommitedAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2PipeCommitedAmt = :wpQtr2PipeCommitedAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3PipeCommitedAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3PipeCommitedAmt = :wpQtr3PipeCommitedAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4PipeCommitedAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4PipeCommitedAmt = :wpQtr4PipeCommitedAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpTransactionAppliId", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpTransactionAppliId = :wpTransactionAppliId"),
    @NamedQuery(name = "BmwpWorkplan.findByWpPercentOfUnit", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpPercentOfUnit = :wpPercentOfUnit"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearSurrenderAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearSurrenderAmt = :wpYearSurrenderAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1SurrenderAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1SurrenderAmt = :wpQtr1SurrenderAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2SurrenderAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2SurrenderAmt = :wpQtr2SurrenderAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3SurrenderAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3SurrenderAmt = :wpQtr3SurrenderAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4SurrenderAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4SurrenderAmt = :wpQtr4SurrenderAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearActiUnitExecuted", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearActiUnitExecuted = :wpYearActiUnitExecuted"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearActiUnitSurrender", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearActiUnitSurrender = :wpYearActiUnitSurrender"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearActiUnitPipeline", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearActiUnitPipeline = :wpYearActiUnitPipeline"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearActiUnitBalance", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearActiUnitBalance = :wpYearActiUnitBalance"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1ActiInUnit", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1ActiInUnit = :wpQtr1ActiInUnit"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2ActiInUnit", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2ActiInUnit = :wpQtr2ActiInUnit"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3ActiInUnit", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3ActiInUnit = :wpQtr3ActiInUnit"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4ActiInUnit", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4ActiInUnit = :wpQtr4ActiInUnit"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1ActiUnitExecuted", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1ActiUnitExecuted = :wpQtr1ActiUnitExecuted"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2ActiUnitExecuted", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2ActiUnitExecuted = :wpQtr2ActiUnitExecuted"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3ActiUnitExecuted", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3ActiUnitExecuted = :wpQtr3ActiUnitExecuted"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4ActiUnitExecuted", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4ActiUnitExecuted = :wpQtr4ActiUnitExecuted"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1ActiUnitSurrender", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1ActiUnitSurrender = :wpQtr1ActiUnitSurrender"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2ActiUnitSurrender", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2ActiUnitSurrender = :wpQtr2ActiUnitSurrender"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3ActiUnitSurrender", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3ActiUnitSurrender = :wpQtr3ActiUnitSurrender"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4ActiUnitSurrender", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4ActiUnitSurrender = :wpQtr4ActiUnitSurrender"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1ActiUnitPipeline", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1ActiUnitPipeline = :wpQtr1ActiUnitPipeline"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2ActiUnitPipeline", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2ActiUnitPipeline = :wpQtr2ActiUnitPipeline"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3ActiUnitPipeline", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3ActiUnitPipeline = :wpQtr3ActiUnitPipeline"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4ActiUnitPipeline", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4ActiUnitPipeline = :wpQtr4ActiUnitPipeline"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1ActiUnitBalance", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1ActiUnitBalance = :wpQtr1ActiUnitBalance"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2ActiUnitBalance", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2ActiUnitBalance = :wpQtr2ActiUnitBalance"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3ActiUnitBalance", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3ActiUnitBalance = :wpQtr3ActiUnitBalance"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4ActiUnitBalance", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4ActiUnitBalance = :wpQtr4ActiUnitBalance"),
    @NamedQuery(name = "BmwpWorkplan.findByWpFinalizedFlag", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpFinalizedFlag = :wpFinalizedFlag"),
    @NamedQuery(name = "BmwpWorkplan.findByWpTransactionDate", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpTransactionDate = :wpTransactionDate"),
    @NamedQuery(name = "BmwpWorkplan.findByWpClosedQtrNo", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpClosedQtrNo = :wpClosedQtrNo"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearProCrExpAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearProCrExpAmt = :wpYearProCrExpAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1ProCrExpAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1ProCrExpAmt = :wpQtr1ProCrExpAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2ProCrExpAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2ProCrExpAmt = :wpQtr2ProCrExpAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3ProCrExpAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3ProCrExpAmt = :wpQtr3ProCrExpAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4ProCrExpAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4ProCrExpAmt = :wpQtr4ProCrExpAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1ReceivedActiUnit", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1ReceivedActiUnit = :wpQtr1ReceivedActiUnit"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2ReceivedActiUnit", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2ReceivedActiUnit = :wpQtr2ReceivedActiUnit"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3ReceivedActiUnit", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3ReceivedActiUnit = :wpQtr3ReceivedActiUnit"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4ReceivedActiUnit", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4ReceivedActiUnit = :wpQtr4ReceivedActiUnit"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1CarryFwdActiUnit", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1CarryFwdActiUnit = :wpQtr1CarryFwdActiUnit"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2CarryFwdActiUnit", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2CarryFwdActiUnit = :wpQtr2CarryFwdActiUnit"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3CarryFwdActiUnit", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3CarryFwdActiUnit = :wpQtr3CarryFwdActiUnit"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4CarryFwdActiUnit", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4CarryFwdActiUnit = :wpQtr4CarryFwdActiUnit"),
    @NamedQuery(name = "BmwpWorkplan.findByWpYearCommContriAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpYearCommContriAmt = :wpYearCommContriAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr1CommContriAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr1CommContriAmt = :wpQtr1CommContriAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr2CommContriAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr2CommContriAmt = :wpQtr2CommContriAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr3CommContriAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr3CommContriAmt = :wpQtr3CommContriAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpQtr4CommContriAmt", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpQtr4CommContriAmt = :wpQtr4CommContriAmt"),
    @NamedQuery(name = "BmwpWorkplan.findByWpPmFundMonitorFlag", query = "SELECT b FROM BmwpWorkplan b WHERE b.wpPmFundMonitorFlag = :wpPmFundMonitorFlag"),
    @NamedQuery(name = "BmwpWorkplan.findByPmFundType", query = "SELECT b FROM BmwpWorkplan b WHERE b.pmFundType = :pmFundType")})
public class BmwpWorkplan implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_UNIQUE_SRNO")
    private BigDecimal wpUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_FINYEAR")
    private int wpFinyear;
    @Column(name = "WP_SRNO")
    private Integer wpSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_PERCENT_OF_AMT")
    private BigDecimal wpPercentOfAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_ACTI_IN_UNIT")
    private BigDecimal wpYearActiInUnit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_WORKPLAN_AMT")
    private BigDecimal wpYearWorkplanAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_WORKPLAN_AMT")
    private BigDecimal wpQtr1WorkplanAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_WORKPLAN_AMT")
    private BigDecimal wpQtr2WorkplanAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_WORKPLAN_AMT")
    private BigDecimal wpQtr3WorkplanAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_WORKPLAN_AMT")
    private BigDecimal wpQtr4WorkplanAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_RECEIVED_ADVANCE_AMT")
    private BigDecimal wpYearReceivedAdvanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_RECEIVED_ADVANCE_AMT")
    private BigDecimal wpQtr1ReceivedAdvanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_RECEIVED_ADVANCE_AMT")
    private BigDecimal wpQtr2ReceivedAdvanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_RECEIVED_ADVANCE_AMT")
    private BigDecimal wpQtr3ReceivedAdvanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_RECEIVED_ADVANCE_AMT")
    private BigDecimal wpQtr4ReceivedAdvanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_RE_ALLOT_WP_AMT")
    private BigDecimal wpYearReAllotWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_RE_ALLOT_WP_AMT")
    private BigDecimal wpQtr1ReAllotWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_RE_ALLOT_WP_AMT")
    private BigDecimal wpQtr2ReAllotWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_RE_ALLOT_WP_AMT")
    private BigDecimal wpQtr3ReAllotWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_RE_ALLOT_WP_AMT")
    private BigDecimal wpQtr4ReAllotWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_WP_SURRENDER_AMT")
    private BigDecimal wpYearWpSurrenderAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_WP_SURRENDER_AMT")
    private BigDecimal wpQtr1WpSurrenderAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_WP_SURRENDER_AMT")
    private BigDecimal wpQtr2WpSurrenderAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_WP_SURRENDER_AMT")
    private BigDecimal wpQtr3WpSurrenderAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_WP_SURRENDER_AMT")
    private BigDecimal wpQtr4WpSurrenderAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_REVISE_WP_AMT")
    private BigDecimal wpYearReviseWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_REVISE_WP_AMT")
    private BigDecimal wpQtr1ReviseWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_REVISE_WP_AMT")
    private BigDecimal wpQtr2ReviseWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_REVISE_WP_AMT")
    private BigDecimal wpQtr3ReviseWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_REVISE_WP_AMT")
    private BigDecimal wpQtr4ReviseWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_COMMITED_AMT")
    private BigDecimal wpYearCommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_COMMITED_AMT")
    private BigDecimal wpQtr1CommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_COMMITED_AMT")
    private BigDecimal wpQtr2CommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_COMMITED_AMT")
    private BigDecimal wpQtr3CommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_COMMITED_AMT")
    private BigDecimal wpQtr4CommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_PIPELINE_PAY_AMT")
    private BigDecimal wpYearPipelinePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_PIPELINE_PAY_AMT")
    private BigDecimal wpQtr1PipelinePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_PIPELINE_PAY_AMT")
    private BigDecimal wpQtr2PipelinePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_PIPELINE_PAY_AMT")
    private BigDecimal wpQtr3PipelinePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_PIPELINE_PAY_AMT")
    private BigDecimal wpQtr4PipelinePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_ADVANCE_COMMIT_AMT")
    private BigDecimal wpYearAdvanceCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_ADVANCE_COMMIT_AMT")
    private BigDecimal wpQtr1AdvanceCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_ADVANCE_COMMIT_AMT")
    private BigDecimal wpQtr2AdvanceCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_ADVANCE_COMMIT_AMT")
    private BigDecimal wpQtr3AdvanceCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_ADVANCE_COMMIT_AMT")
    private BigDecimal wpQtr4AdvanceCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_ADVANCE_PAY_AMT")
    private BigDecimal wpYearAdvancePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_ADVANCE_PAY_AMT")
    private BigDecimal wpQtr1AdvancePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_ADVANCE_PAY_AMT")
    private BigDecimal wpQtr2AdvancePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_ADVANCE_PAY_AMT")
    private BigDecimal wpQtr3AdvancePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_ADVANCE_PAY_AMT")
    private BigDecimal wpQtr4AdvancePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YR_PIPE_ADVADJ_COMMIT_AMT")
    private BigDecimal wpYrPipeAdvadjCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_Q1_PIPE_ADVADJ_COMMIT_AMT")
    private BigDecimal wpQ1PipeAdvadjCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_Q2_PIPE_ADVADJ_COMMIT_AMT")
    private BigDecimal wpQ2PipeAdvadjCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_Q3_PIPE_ADVADJ_COMMIT_AMT")
    private BigDecimal wpQ3PipeAdvadjCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_Q4_PIPE_ADVADJ_COMMIT_AMT")
    private BigDecimal wpQ4PipeAdvadjCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_PIPE_ADVADJ_PAY_AMT")
    private BigDecimal wpYearPipeAdvadjPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_PIPE_ADVADJ_PAY_AMT")
    private BigDecimal wpQtr1PipeAdvadjPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_PIPE_ADVADJ_PAY_AMT")
    private BigDecimal wpQtr2PipeAdvadjPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_PIPE_ADVADJ_PAY_AMT")
    private BigDecimal wpQtr3PipeAdvadjPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_PIPE_ADVADJ_PAY_AMT")
    private BigDecimal wpQtr4PipeAdvadjPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_RECEIPT_PAY_AMT")
    private BigDecimal wpYearReceiptPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_RECEIPT_PAY_AMT")
    private BigDecimal wpQtr1ReceiptPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_RECEIPT_PAY_AMT")
    private BigDecimal wpQtr2ReceiptPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_RECEIPT_PAY_AMT")
    private BigDecimal wpQtr3ReceiptPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_RECEIPT_PAY_AMT")
    private BigDecimal wpQtr4ReceiptPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_RECEIPT_COMMIT_AMT")
    private BigDecimal wpYearReceiptCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_RECEIPT_COMMIT_AMT")
    private BigDecimal wpQtr1ReceiptCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_RECEIPT_COMMIT_AMT")
    private BigDecimal wpQtr2ReceiptCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_RECEIPT_COMMIT_AMT")
    private BigDecimal wpQtr3ReceiptCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_RECEIPT_COMMIT_AMT")
    private BigDecimal wpQtr4ReceiptCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_EXPENSE_AMT")
    private BigDecimal wpYearExpenseAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_EXPENSE_AMT")
    private BigDecimal wpQtr1ExpenseAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_EXPENSE_AMT")
    private BigDecimal wpQtr2ExpenseAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_EXPENSE_AMT")
    private BigDecimal wpQtr3ExpenseAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_EXPENSE_AMT")
    private BigDecimal wpQtr4ExpenseAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_USABLE_BALANCE_AMT")
    private BigDecimal wpYearUsableBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_USABLE_BALANCE_AMT")
    private BigDecimal wpQtr1UsableBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_USABLE_BALANCE_AMT")
    private BigDecimal wpQtr2UsableBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_USABLE_BALANCE_AMT")
    private BigDecimal wpQtr3UsableBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_USABLE_BALANCE_AMT")
    private BigDecimal wpQtr4UsableBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_BALANCE_AMT")
    private BigDecimal wpYearBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_BALANCE_AMT")
    private BigDecimal wpQtr1BalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_BALANCE_AMT")
    private BigDecimal wpQtr2BalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_BALANCE_AMT")
    private BigDecimal wpQtr3BalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_BALANCE_AMT")
    private BigDecimal wpQtr4BalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_CARRY_FWD_ADV_AMT")
    private BigDecimal wpYearCarryFwdAdvAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_CARRY_FWD_ADV_AMT")
    private BigDecimal wpQtr1CarryFwdAdvAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_CARRY_FWD_ADV_AMT")
    private BigDecimal wpQtr2CarryFwdAdvAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_CARRY_FWD_ADV_AMT")
    private BigDecimal wpQtr3CarryFwdAdvAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_CARRY_FWD_ADV_AMT")
    private BigDecimal wpQtr4CarryFwdAdvAmt;
    @Column(name = "WP_VILLAGECD")
    private Integer wpVillagecd;
    @Size(max = 150)
    @Column(name = "WP_SURVEY_NO")
    private String wpSurveyNo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_PIPE_COMMITED_AMT")
    private BigDecimal wpYearPipeCommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_PIPE_COMMITED_AMT")
    private BigDecimal wpQtr1PipeCommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_PIPE_COMMITED_AMT")
    private BigDecimal wpQtr2PipeCommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_PIPE_COMMITED_AMT")
    private BigDecimal wpQtr3PipeCommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_PIPE_COMMITED_AMT")
    private BigDecimal wpQtr4PipeCommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_TRANSACTION_APPLI_ID")
    private short wpTransactionAppliId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_PERCENT_OF_UNIT")
    private BigDecimal wpPercentOfUnit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_SURRENDER_AMT")
    private BigDecimal wpYearSurrenderAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_SURRENDER_AMT")
    private BigDecimal wpQtr1SurrenderAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_SURRENDER_AMT")
    private BigDecimal wpQtr2SurrenderAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_SURRENDER_AMT")
    private BigDecimal wpQtr3SurrenderAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_SURRENDER_AMT")
    private BigDecimal wpQtr4SurrenderAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_ACTI_UNIT_EXECUTED")
    private BigDecimal wpYearActiUnitExecuted;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_ACTI_UNIT_SURRENDER")
    private BigDecimal wpYearActiUnitSurrender;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_ACTI_UNIT_PIPELINE")
    private BigDecimal wpYearActiUnitPipeline;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_ACTI_UNIT_BALANCE")
    private BigDecimal wpYearActiUnitBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_ACTI_IN_UNIT")
    private BigDecimal wpQtr1ActiInUnit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_ACTI_IN_UNIT")
    private BigDecimal wpQtr2ActiInUnit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_ACTI_IN_UNIT")
    private BigDecimal wpQtr3ActiInUnit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_ACTI_IN_UNIT")
    private BigDecimal wpQtr4ActiInUnit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_ACTI_UNIT_EXECUTED")
    private BigDecimal wpQtr1ActiUnitExecuted;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_ACTI_UNIT_EXECUTED")
    private BigDecimal wpQtr2ActiUnitExecuted;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_ACTI_UNIT_EXECUTED")
    private BigDecimal wpQtr3ActiUnitExecuted;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_ACTI_UNIT_EXECUTED")
    private BigDecimal wpQtr4ActiUnitExecuted;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_ACTI_UNIT_SURRENDER")
    private BigDecimal wpQtr1ActiUnitSurrender;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_ACTI_UNIT_SURRENDER")
    private BigDecimal wpQtr2ActiUnitSurrender;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_ACTI_UNIT_SURRENDER")
    private BigDecimal wpQtr3ActiUnitSurrender;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_ACTI_UNIT_SURRENDER")
    private BigDecimal wpQtr4ActiUnitSurrender;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_ACTI_UNIT_PIPELINE")
    private BigDecimal wpQtr1ActiUnitPipeline;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_ACTI_UNIT_PIPELINE")
    private BigDecimal wpQtr2ActiUnitPipeline;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_ACTI_UNIT_PIPELINE")
    private BigDecimal wpQtr3ActiUnitPipeline;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_ACTI_UNIT_PIPELINE")
    private BigDecimal wpQtr4ActiUnitPipeline;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_ACTI_UNIT_BALANCE")
    private BigDecimal wpQtr1ActiUnitBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_ACTI_UNIT_BALANCE")
    private BigDecimal wpQtr2ActiUnitBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_ACTI_UNIT_BALANCE")
    private BigDecimal wpQtr3ActiUnitBalance;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_ACTI_UNIT_BALANCE")
    private BigDecimal wpQtr4ActiUnitBalance;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "WP_FINALIZED_FLAG")
    private String wpFinalizedFlag;
    @Column(name = "WP_TRANSACTION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date wpTransactionDate;
    @Column(name = "WP_CLOSED_QTR_NO")
    private Short wpClosedQtrNo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_PRO_CR_EXP_AMT")
    private BigDecimal wpYearProCrExpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_PRO_CR_EXP_AMT")
    private BigDecimal wpQtr1ProCrExpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_PRO_CR_EXP_AMT")
    private BigDecimal wpQtr2ProCrExpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_PRO_CR_EXP_AMT")
    private BigDecimal wpQtr3ProCrExpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_PRO_CR_EXP_AMT")
    private BigDecimal wpQtr4ProCrExpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_RECEIVED_ACTI_UNIT")
    private BigDecimal wpQtr1ReceivedActiUnit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_RECEIVED_ACTI_UNIT")
    private BigDecimal wpQtr2ReceivedActiUnit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_RECEIVED_ACTI_UNIT")
    private BigDecimal wpQtr3ReceivedActiUnit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_RECEIVED_ACTI_UNIT")
    private BigDecimal wpQtr4ReceivedActiUnit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_CARRY_FWD_ACTI_UNIT")
    private BigDecimal wpQtr1CarryFwdActiUnit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_CARRY_FWD_ACTI_UNIT")
    private BigDecimal wpQtr2CarryFwdActiUnit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_CARRY_FWD_ACTI_UNIT")
    private BigDecimal wpQtr3CarryFwdActiUnit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_CARRY_FWD_ACTI_UNIT")
    private BigDecimal wpQtr4CarryFwdActiUnit;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_YEAR_COMM_CONTRI_AMT")
    private BigDecimal wpYearCommContriAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR1_COMM_CONTRI_AMT")
    private BigDecimal wpQtr1CommContriAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR2_COMM_CONTRI_AMT")
    private BigDecimal wpQtr2CommContriAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR3_COMM_CONTRI_AMT")
    private BigDecimal wpQtr3CommContriAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "WP_QTR4_COMM_CONTRI_AMT")
    private BigDecimal wpQtr4CommContriAmt;
    @Size(max = 1)
    @Column(name = "WP_PM_FUND_MONITOR_FLAG")
    private String wpPmFundMonitorFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PM_FUND_TYPE")
    private String pmFundType;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "WP_ABD_UNIQUE_SRNO", referencedColumnName = "ABD_UNIQUE_SRNO")
    @ManyToOne(optional = false)
    private BmwpActivityBudgetDtl wpAbdUniqueSrno;
    @JoinColumn(name = "WP_AM_UNIQUE_SRNO", referencedColumnName = "AM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpActivityMst wpAmUniqueSrno;
    @JoinColumn(name = "WP_APL_UNIQUE_SRNO", referencedColumnName = "APL_UNIQUE_SRNO")
    @ManyToOne
    private BmwpAgencyProjectLink wpAplUniqueSrno;
    @JoinColumn(name = "WP_ATM_UNIQUE_SRNO", referencedColumnName = "ATM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpAgencyTypeMst wpAtmUniqueSrno;
    @JoinColumn(name = "WP_APABD_UNIQUE_SRNO", referencedColumnName = "APABD_UNIQUE_SRNO")
    @ManyToOne(optional = false)
    private BmwpApActivityBudgetDtl wpApabdUniqueSrno;
    @JoinColumn(name = "WP_APAM_UNIQUE_SRNO", referencedColumnName = "APAM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpApActivityMst wpApamUniqueSrno;
    @JoinColumn(name = "WP_APBM_UNIQUE_SRNO", referencedColumnName = "APBM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpApBudgetMst wpApbmUniqueSrno;
    @JoinColumn(name = "WP_BM_UNIQUE_SRNO", referencedColumnName = "BM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpBudgetMst wpBmUniqueSrno;
    @JoinColumn(name = "WP_FAM_UNIQUE_SRNO", referencedColumnName = "FAM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpFundAgencyMst wpFamUniqueSrno;
    @JoinColumn(name = "WP_PTD_UNIQUE_SRNO", referencedColumnName = "PTD_UNIQUE_SRNO")
    @ManyToOne(optional = false)
    private BmwpPartyTypeDtl wpPtdUniqueSrno;
    @JoinColumn(name = "WP_PM_UNIQUE_SRNO", referencedColumnName = "PM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpProjectMst wpPmUniqueSrno;
    @JoinColumn(name = "WP_ACTIVITY_UM_UNIQUE_SRNO", referencedColumnName = "UM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpUnitMst wpActivityUmUniqueSrno;
    @OneToOne(mappedBy = "refWpUniqueSrno")
    private BmwpWorkplan bmwpWorkplan;
    @JoinColumn(name = "REF_WP_UNIQUE_SRNO", referencedColumnName = "WP_UNIQUE_SRNO")
    @OneToOne
    private BmwpWorkplan refWpUniqueSrno;

    public BmwpWorkplan() {
    }

    public BmwpWorkplan(BigDecimal wpUniqueSrno) {
        this.wpUniqueSrno = wpUniqueSrno;
    }

    public BmwpWorkplan(BigDecimal wpUniqueSrno, int wpFinyear, BigDecimal wpPercentOfAmt, BigDecimal wpYearActiInUnit, BigDecimal wpYearWorkplanAmt, BigDecimal wpQtr1WorkplanAmt, BigDecimal wpQtr2WorkplanAmt, BigDecimal wpQtr3WorkplanAmt, BigDecimal wpQtr4WorkplanAmt, BigDecimal wpYearReceivedAdvanceAmt, BigDecimal wpQtr1ReceivedAdvanceAmt, BigDecimal wpQtr2ReceivedAdvanceAmt, BigDecimal wpQtr3ReceivedAdvanceAmt, BigDecimal wpQtr4ReceivedAdvanceAmt, BigDecimal wpYearReAllotWpAmt, BigDecimal wpQtr1ReAllotWpAmt, BigDecimal wpQtr2ReAllotWpAmt, BigDecimal wpQtr3ReAllotWpAmt, BigDecimal wpQtr4ReAllotWpAmt, BigDecimal wpYearWpSurrenderAmt, BigDecimal wpQtr1WpSurrenderAmt, BigDecimal wpQtr2WpSurrenderAmt, BigDecimal wpQtr3WpSurrenderAmt, BigDecimal wpQtr4WpSurrenderAmt, BigDecimal wpYearReviseWpAmt, BigDecimal wpQtr1ReviseWpAmt, BigDecimal wpQtr2ReviseWpAmt, BigDecimal wpQtr3ReviseWpAmt, BigDecimal wpQtr4ReviseWpAmt, BigDecimal wpYearCommitedAmt, BigDecimal wpQtr1CommitedAmt, BigDecimal wpQtr2CommitedAmt, BigDecimal wpQtr3CommitedAmt, BigDecimal wpQtr4CommitedAmt, BigDecimal wpYearPipelinePayAmt, BigDecimal wpQtr1PipelinePayAmt, BigDecimal wpQtr2PipelinePayAmt, BigDecimal wpQtr3PipelinePayAmt, BigDecimal wpQtr4PipelinePayAmt, BigDecimal wpYearAdvanceCommitAmt, BigDecimal wpQtr1AdvanceCommitAmt, BigDecimal wpQtr2AdvanceCommitAmt, BigDecimal wpQtr3AdvanceCommitAmt, BigDecimal wpQtr4AdvanceCommitAmt, BigDecimal wpYearAdvancePayAmt, BigDecimal wpQtr1AdvancePayAmt, BigDecimal wpQtr2AdvancePayAmt, BigDecimal wpQtr3AdvancePayAmt, BigDecimal wpQtr4AdvancePayAmt, BigDecimal wpYrPipeAdvadjCommitAmt, BigDecimal wpQ1PipeAdvadjCommitAmt, BigDecimal wpQ2PipeAdvadjCommitAmt, BigDecimal wpQ3PipeAdvadjCommitAmt, BigDecimal wpQ4PipeAdvadjCommitAmt, BigDecimal wpYearPipeAdvadjPayAmt, BigDecimal wpQtr1PipeAdvadjPayAmt, BigDecimal wpQtr2PipeAdvadjPayAmt, BigDecimal wpQtr3PipeAdvadjPayAmt, BigDecimal wpQtr4PipeAdvadjPayAmt, BigDecimal wpYearReceiptPayAmt, BigDecimal wpQtr1ReceiptPayAmt, BigDecimal wpQtr2ReceiptPayAmt, BigDecimal wpQtr3ReceiptPayAmt, BigDecimal wpQtr4ReceiptPayAmt, BigDecimal wpYearReceiptCommitAmt, BigDecimal wpQtr1ReceiptCommitAmt, BigDecimal wpQtr2ReceiptCommitAmt, BigDecimal wpQtr3ReceiptCommitAmt, BigDecimal wpQtr4ReceiptCommitAmt, BigDecimal wpYearExpenseAmt, BigDecimal wpQtr1ExpenseAmt, BigDecimal wpQtr2ExpenseAmt, BigDecimal wpQtr3ExpenseAmt, BigDecimal wpQtr4ExpenseAmt, BigDecimal wpYearUsableBalanceAmt, BigDecimal wpQtr1UsableBalanceAmt, BigDecimal wpQtr2UsableBalanceAmt, BigDecimal wpQtr3UsableBalanceAmt, BigDecimal wpQtr4UsableBalanceAmt, BigDecimal wpYearBalanceAmt, BigDecimal wpQtr1BalanceAmt, BigDecimal wpQtr2BalanceAmt, BigDecimal wpQtr3BalanceAmt, BigDecimal wpQtr4BalanceAmt, BigDecimal wpYearCarryFwdAdvAmt, BigDecimal wpQtr1CarryFwdAdvAmt, BigDecimal wpQtr2CarryFwdAdvAmt, BigDecimal wpQtr3CarryFwdAdvAmt, BigDecimal wpQtr4CarryFwdAdvAmt, Date createdt, BigDecimal wpYearPipeCommitedAmt, BigDecimal wpQtr1PipeCommitedAmt, BigDecimal wpQtr2PipeCommitedAmt, BigDecimal wpQtr3PipeCommitedAmt, BigDecimal wpQtr4PipeCommitedAmt, short wpTransactionAppliId, BigDecimal wpPercentOfUnit, BigDecimal wpYearSurrenderAmt, BigDecimal wpQtr1SurrenderAmt, BigDecimal wpQtr2SurrenderAmt, BigDecimal wpQtr3SurrenderAmt, BigDecimal wpQtr4SurrenderAmt, BigDecimal wpYearActiUnitExecuted, BigDecimal wpYearActiUnitSurrender, BigDecimal wpYearActiUnitPipeline, BigDecimal wpYearActiUnitBalance, BigDecimal wpQtr1ActiInUnit, BigDecimal wpQtr2ActiInUnit, BigDecimal wpQtr3ActiInUnit, BigDecimal wpQtr4ActiInUnit, BigDecimal wpQtr1ActiUnitExecuted, BigDecimal wpQtr2ActiUnitExecuted, BigDecimal wpQtr3ActiUnitExecuted, BigDecimal wpQtr4ActiUnitExecuted, BigDecimal wpQtr1ActiUnitSurrender, BigDecimal wpQtr2ActiUnitSurrender, BigDecimal wpQtr3ActiUnitSurrender, BigDecimal wpQtr4ActiUnitSurrender, BigDecimal wpQtr1ActiUnitPipeline, BigDecimal wpQtr2ActiUnitPipeline, BigDecimal wpQtr3ActiUnitPipeline, BigDecimal wpQtr4ActiUnitPipeline, BigDecimal wpQtr1ActiUnitBalance, BigDecimal wpQtr2ActiUnitBalance, BigDecimal wpQtr3ActiUnitBalance, BigDecimal wpQtr4ActiUnitBalance, String wpFinalizedFlag, BigDecimal wpYearProCrExpAmt, BigDecimal wpQtr1ProCrExpAmt, BigDecimal wpQtr2ProCrExpAmt, BigDecimal wpQtr3ProCrExpAmt, BigDecimal wpQtr4ProCrExpAmt, BigDecimal wpQtr1ReceivedActiUnit, BigDecimal wpQtr2ReceivedActiUnit, BigDecimal wpQtr3ReceivedActiUnit, BigDecimal wpQtr4ReceivedActiUnit, BigDecimal wpQtr1CarryFwdActiUnit, BigDecimal wpQtr2CarryFwdActiUnit, BigDecimal wpQtr3CarryFwdActiUnit, BigDecimal wpQtr4CarryFwdActiUnit, BigDecimal wpYearCommContriAmt, BigDecimal wpQtr1CommContriAmt, BigDecimal wpQtr2CommContriAmt, BigDecimal wpQtr3CommContriAmt, BigDecimal wpQtr4CommContriAmt, String pmFundType) {
        this.wpUniqueSrno = wpUniqueSrno;
        this.wpFinyear = wpFinyear;
        this.wpPercentOfAmt = wpPercentOfAmt;
        this.wpYearActiInUnit = wpYearActiInUnit;
        this.wpYearWorkplanAmt = wpYearWorkplanAmt;
        this.wpQtr1WorkplanAmt = wpQtr1WorkplanAmt;
        this.wpQtr2WorkplanAmt = wpQtr2WorkplanAmt;
        this.wpQtr3WorkplanAmt = wpQtr3WorkplanAmt;
        this.wpQtr4WorkplanAmt = wpQtr4WorkplanAmt;
        this.wpYearReceivedAdvanceAmt = wpYearReceivedAdvanceAmt;
        this.wpQtr1ReceivedAdvanceAmt = wpQtr1ReceivedAdvanceAmt;
        this.wpQtr2ReceivedAdvanceAmt = wpQtr2ReceivedAdvanceAmt;
        this.wpQtr3ReceivedAdvanceAmt = wpQtr3ReceivedAdvanceAmt;
        this.wpQtr4ReceivedAdvanceAmt = wpQtr4ReceivedAdvanceAmt;
        this.wpYearReAllotWpAmt = wpYearReAllotWpAmt;
        this.wpQtr1ReAllotWpAmt = wpQtr1ReAllotWpAmt;
        this.wpQtr2ReAllotWpAmt = wpQtr2ReAllotWpAmt;
        this.wpQtr3ReAllotWpAmt = wpQtr3ReAllotWpAmt;
        this.wpQtr4ReAllotWpAmt = wpQtr4ReAllotWpAmt;
        this.wpYearWpSurrenderAmt = wpYearWpSurrenderAmt;
        this.wpQtr1WpSurrenderAmt = wpQtr1WpSurrenderAmt;
        this.wpQtr2WpSurrenderAmt = wpQtr2WpSurrenderAmt;
        this.wpQtr3WpSurrenderAmt = wpQtr3WpSurrenderAmt;
        this.wpQtr4WpSurrenderAmt = wpQtr4WpSurrenderAmt;
        this.wpYearReviseWpAmt = wpYearReviseWpAmt;
        this.wpQtr1ReviseWpAmt = wpQtr1ReviseWpAmt;
        this.wpQtr2ReviseWpAmt = wpQtr2ReviseWpAmt;
        this.wpQtr3ReviseWpAmt = wpQtr3ReviseWpAmt;
        this.wpQtr4ReviseWpAmt = wpQtr4ReviseWpAmt;
        this.wpYearCommitedAmt = wpYearCommitedAmt;
        this.wpQtr1CommitedAmt = wpQtr1CommitedAmt;
        this.wpQtr2CommitedAmt = wpQtr2CommitedAmt;
        this.wpQtr3CommitedAmt = wpQtr3CommitedAmt;
        this.wpQtr4CommitedAmt = wpQtr4CommitedAmt;
        this.wpYearPipelinePayAmt = wpYearPipelinePayAmt;
        this.wpQtr1PipelinePayAmt = wpQtr1PipelinePayAmt;
        this.wpQtr2PipelinePayAmt = wpQtr2PipelinePayAmt;
        this.wpQtr3PipelinePayAmt = wpQtr3PipelinePayAmt;
        this.wpQtr4PipelinePayAmt = wpQtr4PipelinePayAmt;
        this.wpYearAdvanceCommitAmt = wpYearAdvanceCommitAmt;
        this.wpQtr1AdvanceCommitAmt = wpQtr1AdvanceCommitAmt;
        this.wpQtr2AdvanceCommitAmt = wpQtr2AdvanceCommitAmt;
        this.wpQtr3AdvanceCommitAmt = wpQtr3AdvanceCommitAmt;
        this.wpQtr4AdvanceCommitAmt = wpQtr4AdvanceCommitAmt;
        this.wpYearAdvancePayAmt = wpYearAdvancePayAmt;
        this.wpQtr1AdvancePayAmt = wpQtr1AdvancePayAmt;
        this.wpQtr2AdvancePayAmt = wpQtr2AdvancePayAmt;
        this.wpQtr3AdvancePayAmt = wpQtr3AdvancePayAmt;
        this.wpQtr4AdvancePayAmt = wpQtr4AdvancePayAmt;
        this.wpYrPipeAdvadjCommitAmt = wpYrPipeAdvadjCommitAmt;
        this.wpQ1PipeAdvadjCommitAmt = wpQ1PipeAdvadjCommitAmt;
        this.wpQ2PipeAdvadjCommitAmt = wpQ2PipeAdvadjCommitAmt;
        this.wpQ3PipeAdvadjCommitAmt = wpQ3PipeAdvadjCommitAmt;
        this.wpQ4PipeAdvadjCommitAmt = wpQ4PipeAdvadjCommitAmt;
        this.wpYearPipeAdvadjPayAmt = wpYearPipeAdvadjPayAmt;
        this.wpQtr1PipeAdvadjPayAmt = wpQtr1PipeAdvadjPayAmt;
        this.wpQtr2PipeAdvadjPayAmt = wpQtr2PipeAdvadjPayAmt;
        this.wpQtr3PipeAdvadjPayAmt = wpQtr3PipeAdvadjPayAmt;
        this.wpQtr4PipeAdvadjPayAmt = wpQtr4PipeAdvadjPayAmt;
        this.wpYearReceiptPayAmt = wpYearReceiptPayAmt;
        this.wpQtr1ReceiptPayAmt = wpQtr1ReceiptPayAmt;
        this.wpQtr2ReceiptPayAmt = wpQtr2ReceiptPayAmt;
        this.wpQtr3ReceiptPayAmt = wpQtr3ReceiptPayAmt;
        this.wpQtr4ReceiptPayAmt = wpQtr4ReceiptPayAmt;
        this.wpYearReceiptCommitAmt = wpYearReceiptCommitAmt;
        this.wpQtr1ReceiptCommitAmt = wpQtr1ReceiptCommitAmt;
        this.wpQtr2ReceiptCommitAmt = wpQtr2ReceiptCommitAmt;
        this.wpQtr3ReceiptCommitAmt = wpQtr3ReceiptCommitAmt;
        this.wpQtr4ReceiptCommitAmt = wpQtr4ReceiptCommitAmt;
        this.wpYearExpenseAmt = wpYearExpenseAmt;
        this.wpQtr1ExpenseAmt = wpQtr1ExpenseAmt;
        this.wpQtr2ExpenseAmt = wpQtr2ExpenseAmt;
        this.wpQtr3ExpenseAmt = wpQtr3ExpenseAmt;
        this.wpQtr4ExpenseAmt = wpQtr4ExpenseAmt;
        this.wpYearUsableBalanceAmt = wpYearUsableBalanceAmt;
        this.wpQtr1UsableBalanceAmt = wpQtr1UsableBalanceAmt;
        this.wpQtr2UsableBalanceAmt = wpQtr2UsableBalanceAmt;
        this.wpQtr3UsableBalanceAmt = wpQtr3UsableBalanceAmt;
        this.wpQtr4UsableBalanceAmt = wpQtr4UsableBalanceAmt;
        this.wpYearBalanceAmt = wpYearBalanceAmt;
        this.wpQtr1BalanceAmt = wpQtr1BalanceAmt;
        this.wpQtr2BalanceAmt = wpQtr2BalanceAmt;
        this.wpQtr3BalanceAmt = wpQtr3BalanceAmt;
        this.wpQtr4BalanceAmt = wpQtr4BalanceAmt;
        this.wpYearCarryFwdAdvAmt = wpYearCarryFwdAdvAmt;
        this.wpQtr1CarryFwdAdvAmt = wpQtr1CarryFwdAdvAmt;
        this.wpQtr2CarryFwdAdvAmt = wpQtr2CarryFwdAdvAmt;
        this.wpQtr3CarryFwdAdvAmt = wpQtr3CarryFwdAdvAmt;
        this.wpQtr4CarryFwdAdvAmt = wpQtr4CarryFwdAdvAmt;
        this.createdt = createdt;
        this.wpYearPipeCommitedAmt = wpYearPipeCommitedAmt;
        this.wpQtr1PipeCommitedAmt = wpQtr1PipeCommitedAmt;
        this.wpQtr2PipeCommitedAmt = wpQtr2PipeCommitedAmt;
        this.wpQtr3PipeCommitedAmt = wpQtr3PipeCommitedAmt;
        this.wpQtr4PipeCommitedAmt = wpQtr4PipeCommitedAmt;
        this.wpTransactionAppliId = wpTransactionAppliId;
        this.wpPercentOfUnit = wpPercentOfUnit;
        this.wpYearSurrenderAmt = wpYearSurrenderAmt;
        this.wpQtr1SurrenderAmt = wpQtr1SurrenderAmt;
        this.wpQtr2SurrenderAmt = wpQtr2SurrenderAmt;
        this.wpQtr3SurrenderAmt = wpQtr3SurrenderAmt;
        this.wpQtr4SurrenderAmt = wpQtr4SurrenderAmt;
        this.wpYearActiUnitExecuted = wpYearActiUnitExecuted;
        this.wpYearActiUnitSurrender = wpYearActiUnitSurrender;
        this.wpYearActiUnitPipeline = wpYearActiUnitPipeline;
        this.wpYearActiUnitBalance = wpYearActiUnitBalance;
        this.wpQtr1ActiInUnit = wpQtr1ActiInUnit;
        this.wpQtr2ActiInUnit = wpQtr2ActiInUnit;
        this.wpQtr3ActiInUnit = wpQtr3ActiInUnit;
        this.wpQtr4ActiInUnit = wpQtr4ActiInUnit;
        this.wpQtr1ActiUnitExecuted = wpQtr1ActiUnitExecuted;
        this.wpQtr2ActiUnitExecuted = wpQtr2ActiUnitExecuted;
        this.wpQtr3ActiUnitExecuted = wpQtr3ActiUnitExecuted;
        this.wpQtr4ActiUnitExecuted = wpQtr4ActiUnitExecuted;
        this.wpQtr1ActiUnitSurrender = wpQtr1ActiUnitSurrender;
        this.wpQtr2ActiUnitSurrender = wpQtr2ActiUnitSurrender;
        this.wpQtr3ActiUnitSurrender = wpQtr3ActiUnitSurrender;
        this.wpQtr4ActiUnitSurrender = wpQtr4ActiUnitSurrender;
        this.wpQtr1ActiUnitPipeline = wpQtr1ActiUnitPipeline;
        this.wpQtr2ActiUnitPipeline = wpQtr2ActiUnitPipeline;
        this.wpQtr3ActiUnitPipeline = wpQtr3ActiUnitPipeline;
        this.wpQtr4ActiUnitPipeline = wpQtr4ActiUnitPipeline;
        this.wpQtr1ActiUnitBalance = wpQtr1ActiUnitBalance;
        this.wpQtr2ActiUnitBalance = wpQtr2ActiUnitBalance;
        this.wpQtr3ActiUnitBalance = wpQtr3ActiUnitBalance;
        this.wpQtr4ActiUnitBalance = wpQtr4ActiUnitBalance;
        this.wpFinalizedFlag = wpFinalizedFlag;
        this.wpYearProCrExpAmt = wpYearProCrExpAmt;
        this.wpQtr1ProCrExpAmt = wpQtr1ProCrExpAmt;
        this.wpQtr2ProCrExpAmt = wpQtr2ProCrExpAmt;
        this.wpQtr3ProCrExpAmt = wpQtr3ProCrExpAmt;
        this.wpQtr4ProCrExpAmt = wpQtr4ProCrExpAmt;
        this.wpQtr1ReceivedActiUnit = wpQtr1ReceivedActiUnit;
        this.wpQtr2ReceivedActiUnit = wpQtr2ReceivedActiUnit;
        this.wpQtr3ReceivedActiUnit = wpQtr3ReceivedActiUnit;
        this.wpQtr4ReceivedActiUnit = wpQtr4ReceivedActiUnit;
        this.wpQtr1CarryFwdActiUnit = wpQtr1CarryFwdActiUnit;
        this.wpQtr2CarryFwdActiUnit = wpQtr2CarryFwdActiUnit;
        this.wpQtr3CarryFwdActiUnit = wpQtr3CarryFwdActiUnit;
        this.wpQtr4CarryFwdActiUnit = wpQtr4CarryFwdActiUnit;
        this.wpYearCommContriAmt = wpYearCommContriAmt;
        this.wpQtr1CommContriAmt = wpQtr1CommContriAmt;
        this.wpQtr2CommContriAmt = wpQtr2CommContriAmt;
        this.wpQtr3CommContriAmt = wpQtr3CommContriAmt;
        this.wpQtr4CommContriAmt = wpQtr4CommContriAmt;
        this.pmFundType = pmFundType;
    }

    public BigDecimal getWpUniqueSrno() {
        return wpUniqueSrno;
    }

    public void setWpUniqueSrno(BigDecimal wpUniqueSrno) {
        this.wpUniqueSrno = wpUniqueSrno;
    }

    public int getWpFinyear() {
        return wpFinyear;
    }

    public void setWpFinyear(int wpFinyear) {
        this.wpFinyear = wpFinyear;
    }

    public Integer getWpSrno() {
        return wpSrno;
    }

    public void setWpSrno(Integer wpSrno) {
        this.wpSrno = wpSrno;
    }

    public BigDecimal getWpPercentOfAmt() {
        return wpPercentOfAmt;
    }

    public void setWpPercentOfAmt(BigDecimal wpPercentOfAmt) {
        this.wpPercentOfAmt = wpPercentOfAmt;
    }

    public BigDecimal getWpYearActiInUnit() {
        return wpYearActiInUnit;
    }

    public void setWpYearActiInUnit(BigDecimal wpYearActiInUnit) {
        this.wpYearActiInUnit = wpYearActiInUnit;
    }

    public BigDecimal getWpYearWorkplanAmt() {
        return wpYearWorkplanAmt;
    }

    public void setWpYearWorkplanAmt(BigDecimal wpYearWorkplanAmt) {
        this.wpYearWorkplanAmt = wpYearWorkplanAmt;
    }

    public BigDecimal getWpQtr1WorkplanAmt() {
        return wpQtr1WorkplanAmt;
    }

    public void setWpQtr1WorkplanAmt(BigDecimal wpQtr1WorkplanAmt) {
        this.wpQtr1WorkplanAmt = wpQtr1WorkplanAmt;
    }

    public BigDecimal getWpQtr2WorkplanAmt() {
        return wpQtr2WorkplanAmt;
    }

    public void setWpQtr2WorkplanAmt(BigDecimal wpQtr2WorkplanAmt) {
        this.wpQtr2WorkplanAmt = wpQtr2WorkplanAmt;
    }

    public BigDecimal getWpQtr3WorkplanAmt() {
        return wpQtr3WorkplanAmt;
    }

    public void setWpQtr3WorkplanAmt(BigDecimal wpQtr3WorkplanAmt) {
        this.wpQtr3WorkplanAmt = wpQtr3WorkplanAmt;
    }

    public BigDecimal getWpQtr4WorkplanAmt() {
        return wpQtr4WorkplanAmt;
    }

    public void setWpQtr4WorkplanAmt(BigDecimal wpQtr4WorkplanAmt) {
        this.wpQtr4WorkplanAmt = wpQtr4WorkplanAmt;
    }

    public BigDecimal getWpYearReceivedAdvanceAmt() {
        return wpYearReceivedAdvanceAmt;
    }

    public void setWpYearReceivedAdvanceAmt(BigDecimal wpYearReceivedAdvanceAmt) {
        this.wpYearReceivedAdvanceAmt = wpYearReceivedAdvanceAmt;
    }

    public BigDecimal getWpQtr1ReceivedAdvanceAmt() {
        return wpQtr1ReceivedAdvanceAmt;
    }

    public void setWpQtr1ReceivedAdvanceAmt(BigDecimal wpQtr1ReceivedAdvanceAmt) {
        this.wpQtr1ReceivedAdvanceAmt = wpQtr1ReceivedAdvanceAmt;
    }

    public BigDecimal getWpQtr2ReceivedAdvanceAmt() {
        return wpQtr2ReceivedAdvanceAmt;
    }

    public void setWpQtr2ReceivedAdvanceAmt(BigDecimal wpQtr2ReceivedAdvanceAmt) {
        this.wpQtr2ReceivedAdvanceAmt = wpQtr2ReceivedAdvanceAmt;
    }

    public BigDecimal getWpQtr3ReceivedAdvanceAmt() {
        return wpQtr3ReceivedAdvanceAmt;
    }

    public void setWpQtr3ReceivedAdvanceAmt(BigDecimal wpQtr3ReceivedAdvanceAmt) {
        this.wpQtr3ReceivedAdvanceAmt = wpQtr3ReceivedAdvanceAmt;
    }

    public BigDecimal getWpQtr4ReceivedAdvanceAmt() {
        return wpQtr4ReceivedAdvanceAmt;
    }

    public void setWpQtr4ReceivedAdvanceAmt(BigDecimal wpQtr4ReceivedAdvanceAmt) {
        this.wpQtr4ReceivedAdvanceAmt = wpQtr4ReceivedAdvanceAmt;
    }

    public BigDecimal getWpYearReAllotWpAmt() {
        return wpYearReAllotWpAmt;
    }

    public void setWpYearReAllotWpAmt(BigDecimal wpYearReAllotWpAmt) {
        this.wpYearReAllotWpAmt = wpYearReAllotWpAmt;
    }

    public BigDecimal getWpQtr1ReAllotWpAmt() {
        return wpQtr1ReAllotWpAmt;
    }

    public void setWpQtr1ReAllotWpAmt(BigDecimal wpQtr1ReAllotWpAmt) {
        this.wpQtr1ReAllotWpAmt = wpQtr1ReAllotWpAmt;
    }

    public BigDecimal getWpQtr2ReAllotWpAmt() {
        return wpQtr2ReAllotWpAmt;
    }

    public void setWpQtr2ReAllotWpAmt(BigDecimal wpQtr2ReAllotWpAmt) {
        this.wpQtr2ReAllotWpAmt = wpQtr2ReAllotWpAmt;
    }

    public BigDecimal getWpQtr3ReAllotWpAmt() {
        return wpQtr3ReAllotWpAmt;
    }

    public void setWpQtr3ReAllotWpAmt(BigDecimal wpQtr3ReAllotWpAmt) {
        this.wpQtr3ReAllotWpAmt = wpQtr3ReAllotWpAmt;
    }

    public BigDecimal getWpQtr4ReAllotWpAmt() {
        return wpQtr4ReAllotWpAmt;
    }

    public void setWpQtr4ReAllotWpAmt(BigDecimal wpQtr4ReAllotWpAmt) {
        this.wpQtr4ReAllotWpAmt = wpQtr4ReAllotWpAmt;
    }

    public BigDecimal getWpYearWpSurrenderAmt() {
        return wpYearWpSurrenderAmt;
    }

    public void setWpYearWpSurrenderAmt(BigDecimal wpYearWpSurrenderAmt) {
        this.wpYearWpSurrenderAmt = wpYearWpSurrenderAmt;
    }

    public BigDecimal getWpQtr1WpSurrenderAmt() {
        return wpQtr1WpSurrenderAmt;
    }

    public void setWpQtr1WpSurrenderAmt(BigDecimal wpQtr1WpSurrenderAmt) {
        this.wpQtr1WpSurrenderAmt = wpQtr1WpSurrenderAmt;
    }

    public BigDecimal getWpQtr2WpSurrenderAmt() {
        return wpQtr2WpSurrenderAmt;
    }

    public void setWpQtr2WpSurrenderAmt(BigDecimal wpQtr2WpSurrenderAmt) {
        this.wpQtr2WpSurrenderAmt = wpQtr2WpSurrenderAmt;
    }

    public BigDecimal getWpQtr3WpSurrenderAmt() {
        return wpQtr3WpSurrenderAmt;
    }

    public void setWpQtr3WpSurrenderAmt(BigDecimal wpQtr3WpSurrenderAmt) {
        this.wpQtr3WpSurrenderAmt = wpQtr3WpSurrenderAmt;
    }

    public BigDecimal getWpQtr4WpSurrenderAmt() {
        return wpQtr4WpSurrenderAmt;
    }

    public void setWpQtr4WpSurrenderAmt(BigDecimal wpQtr4WpSurrenderAmt) {
        this.wpQtr4WpSurrenderAmt = wpQtr4WpSurrenderAmt;
    }

    public BigDecimal getWpYearReviseWpAmt() {
        return wpYearReviseWpAmt;
    }

    public void setWpYearReviseWpAmt(BigDecimal wpYearReviseWpAmt) {
        this.wpYearReviseWpAmt = wpYearReviseWpAmt;
    }

    public BigDecimal getWpQtr1ReviseWpAmt() {
        return wpQtr1ReviseWpAmt;
    }

    public void setWpQtr1ReviseWpAmt(BigDecimal wpQtr1ReviseWpAmt) {
        this.wpQtr1ReviseWpAmt = wpQtr1ReviseWpAmt;
    }

    public BigDecimal getWpQtr2ReviseWpAmt() {
        return wpQtr2ReviseWpAmt;
    }

    public void setWpQtr2ReviseWpAmt(BigDecimal wpQtr2ReviseWpAmt) {
        this.wpQtr2ReviseWpAmt = wpQtr2ReviseWpAmt;
    }

    public BigDecimal getWpQtr3ReviseWpAmt() {
        return wpQtr3ReviseWpAmt;
    }

    public void setWpQtr3ReviseWpAmt(BigDecimal wpQtr3ReviseWpAmt) {
        this.wpQtr3ReviseWpAmt = wpQtr3ReviseWpAmt;
    }

    public BigDecimal getWpQtr4ReviseWpAmt() {
        return wpQtr4ReviseWpAmt;
    }

    public void setWpQtr4ReviseWpAmt(BigDecimal wpQtr4ReviseWpAmt) {
        this.wpQtr4ReviseWpAmt = wpQtr4ReviseWpAmt;
    }

    public BigDecimal getWpYearCommitedAmt() {
        return wpYearCommitedAmt;
    }

    public void setWpYearCommitedAmt(BigDecimal wpYearCommitedAmt) {
        this.wpYearCommitedAmt = wpYearCommitedAmt;
    }

    public BigDecimal getWpQtr1CommitedAmt() {
        return wpQtr1CommitedAmt;
    }

    public void setWpQtr1CommitedAmt(BigDecimal wpQtr1CommitedAmt) {
        this.wpQtr1CommitedAmt = wpQtr1CommitedAmt;
    }

    public BigDecimal getWpQtr2CommitedAmt() {
        return wpQtr2CommitedAmt;
    }

    public void setWpQtr2CommitedAmt(BigDecimal wpQtr2CommitedAmt) {
        this.wpQtr2CommitedAmt = wpQtr2CommitedAmt;
    }

    public BigDecimal getWpQtr3CommitedAmt() {
        return wpQtr3CommitedAmt;
    }

    public void setWpQtr3CommitedAmt(BigDecimal wpQtr3CommitedAmt) {
        this.wpQtr3CommitedAmt = wpQtr3CommitedAmt;
    }

    public BigDecimal getWpQtr4CommitedAmt() {
        return wpQtr4CommitedAmt;
    }

    public void setWpQtr4CommitedAmt(BigDecimal wpQtr4CommitedAmt) {
        this.wpQtr4CommitedAmt = wpQtr4CommitedAmt;
    }

    public BigDecimal getWpYearPipelinePayAmt() {
        return wpYearPipelinePayAmt;
    }

    public void setWpYearPipelinePayAmt(BigDecimal wpYearPipelinePayAmt) {
        this.wpYearPipelinePayAmt = wpYearPipelinePayAmt;
    }

    public BigDecimal getWpQtr1PipelinePayAmt() {
        return wpQtr1PipelinePayAmt;
    }

    public void setWpQtr1PipelinePayAmt(BigDecimal wpQtr1PipelinePayAmt) {
        this.wpQtr1PipelinePayAmt = wpQtr1PipelinePayAmt;
    }

    public BigDecimal getWpQtr2PipelinePayAmt() {
        return wpQtr2PipelinePayAmt;
    }

    public void setWpQtr2PipelinePayAmt(BigDecimal wpQtr2PipelinePayAmt) {
        this.wpQtr2PipelinePayAmt = wpQtr2PipelinePayAmt;
    }

    public BigDecimal getWpQtr3PipelinePayAmt() {
        return wpQtr3PipelinePayAmt;
    }

    public void setWpQtr3PipelinePayAmt(BigDecimal wpQtr3PipelinePayAmt) {
        this.wpQtr3PipelinePayAmt = wpQtr3PipelinePayAmt;
    }

    public BigDecimal getWpQtr4PipelinePayAmt() {
        return wpQtr4PipelinePayAmt;
    }

    public void setWpQtr4PipelinePayAmt(BigDecimal wpQtr4PipelinePayAmt) {
        this.wpQtr4PipelinePayAmt = wpQtr4PipelinePayAmt;
    }

    public BigDecimal getWpYearAdvanceCommitAmt() {
        return wpYearAdvanceCommitAmt;
    }

    public void setWpYearAdvanceCommitAmt(BigDecimal wpYearAdvanceCommitAmt) {
        this.wpYearAdvanceCommitAmt = wpYearAdvanceCommitAmt;
    }

    public BigDecimal getWpQtr1AdvanceCommitAmt() {
        return wpQtr1AdvanceCommitAmt;
    }

    public void setWpQtr1AdvanceCommitAmt(BigDecimal wpQtr1AdvanceCommitAmt) {
        this.wpQtr1AdvanceCommitAmt = wpQtr1AdvanceCommitAmt;
    }

    public BigDecimal getWpQtr2AdvanceCommitAmt() {
        return wpQtr2AdvanceCommitAmt;
    }

    public void setWpQtr2AdvanceCommitAmt(BigDecimal wpQtr2AdvanceCommitAmt) {
        this.wpQtr2AdvanceCommitAmt = wpQtr2AdvanceCommitAmt;
    }

    public BigDecimal getWpQtr3AdvanceCommitAmt() {
        return wpQtr3AdvanceCommitAmt;
    }

    public void setWpQtr3AdvanceCommitAmt(BigDecimal wpQtr3AdvanceCommitAmt) {
        this.wpQtr3AdvanceCommitAmt = wpQtr3AdvanceCommitAmt;
    }

    public BigDecimal getWpQtr4AdvanceCommitAmt() {
        return wpQtr4AdvanceCommitAmt;
    }

    public void setWpQtr4AdvanceCommitAmt(BigDecimal wpQtr4AdvanceCommitAmt) {
        this.wpQtr4AdvanceCommitAmt = wpQtr4AdvanceCommitAmt;
    }

    public BigDecimal getWpYearAdvancePayAmt() {
        return wpYearAdvancePayAmt;
    }

    public void setWpYearAdvancePayAmt(BigDecimal wpYearAdvancePayAmt) {
        this.wpYearAdvancePayAmt = wpYearAdvancePayAmt;
    }

    public BigDecimal getWpQtr1AdvancePayAmt() {
        return wpQtr1AdvancePayAmt;
    }

    public void setWpQtr1AdvancePayAmt(BigDecimal wpQtr1AdvancePayAmt) {
        this.wpQtr1AdvancePayAmt = wpQtr1AdvancePayAmt;
    }

    public BigDecimal getWpQtr2AdvancePayAmt() {
        return wpQtr2AdvancePayAmt;
    }

    public void setWpQtr2AdvancePayAmt(BigDecimal wpQtr2AdvancePayAmt) {
        this.wpQtr2AdvancePayAmt = wpQtr2AdvancePayAmt;
    }

    public BigDecimal getWpQtr3AdvancePayAmt() {
        return wpQtr3AdvancePayAmt;
    }

    public void setWpQtr3AdvancePayAmt(BigDecimal wpQtr3AdvancePayAmt) {
        this.wpQtr3AdvancePayAmt = wpQtr3AdvancePayAmt;
    }

    public BigDecimal getWpQtr4AdvancePayAmt() {
        return wpQtr4AdvancePayAmt;
    }

    public void setWpQtr4AdvancePayAmt(BigDecimal wpQtr4AdvancePayAmt) {
        this.wpQtr4AdvancePayAmt = wpQtr4AdvancePayAmt;
    }

    public BigDecimal getWpYrPipeAdvadjCommitAmt() {
        return wpYrPipeAdvadjCommitAmt;
    }

    public void setWpYrPipeAdvadjCommitAmt(BigDecimal wpYrPipeAdvadjCommitAmt) {
        this.wpYrPipeAdvadjCommitAmt = wpYrPipeAdvadjCommitAmt;
    }

    public BigDecimal getWpQ1PipeAdvadjCommitAmt() {
        return wpQ1PipeAdvadjCommitAmt;
    }

    public void setWpQ1PipeAdvadjCommitAmt(BigDecimal wpQ1PipeAdvadjCommitAmt) {
        this.wpQ1PipeAdvadjCommitAmt = wpQ1PipeAdvadjCommitAmt;
    }

    public BigDecimal getWpQ2PipeAdvadjCommitAmt() {
        return wpQ2PipeAdvadjCommitAmt;
    }

    public void setWpQ2PipeAdvadjCommitAmt(BigDecimal wpQ2PipeAdvadjCommitAmt) {
        this.wpQ2PipeAdvadjCommitAmt = wpQ2PipeAdvadjCommitAmt;
    }

    public BigDecimal getWpQ3PipeAdvadjCommitAmt() {
        return wpQ3PipeAdvadjCommitAmt;
    }

    public void setWpQ3PipeAdvadjCommitAmt(BigDecimal wpQ3PipeAdvadjCommitAmt) {
        this.wpQ3PipeAdvadjCommitAmt = wpQ3PipeAdvadjCommitAmt;
    }

    public BigDecimal getWpQ4PipeAdvadjCommitAmt() {
        return wpQ4PipeAdvadjCommitAmt;
    }

    public void setWpQ4PipeAdvadjCommitAmt(BigDecimal wpQ4PipeAdvadjCommitAmt) {
        this.wpQ4PipeAdvadjCommitAmt = wpQ4PipeAdvadjCommitAmt;
    }

    public BigDecimal getWpYearPipeAdvadjPayAmt() {
        return wpYearPipeAdvadjPayAmt;
    }

    public void setWpYearPipeAdvadjPayAmt(BigDecimal wpYearPipeAdvadjPayAmt) {
        this.wpYearPipeAdvadjPayAmt = wpYearPipeAdvadjPayAmt;
    }

    public BigDecimal getWpQtr1PipeAdvadjPayAmt() {
        return wpQtr1PipeAdvadjPayAmt;
    }

    public void setWpQtr1PipeAdvadjPayAmt(BigDecimal wpQtr1PipeAdvadjPayAmt) {
        this.wpQtr1PipeAdvadjPayAmt = wpQtr1PipeAdvadjPayAmt;
    }

    public BigDecimal getWpQtr2PipeAdvadjPayAmt() {
        return wpQtr2PipeAdvadjPayAmt;
    }

    public void setWpQtr2PipeAdvadjPayAmt(BigDecimal wpQtr2PipeAdvadjPayAmt) {
        this.wpQtr2PipeAdvadjPayAmt = wpQtr2PipeAdvadjPayAmt;
    }

    public BigDecimal getWpQtr3PipeAdvadjPayAmt() {
        return wpQtr3PipeAdvadjPayAmt;
    }

    public void setWpQtr3PipeAdvadjPayAmt(BigDecimal wpQtr3PipeAdvadjPayAmt) {
        this.wpQtr3PipeAdvadjPayAmt = wpQtr3PipeAdvadjPayAmt;
    }

    public BigDecimal getWpQtr4PipeAdvadjPayAmt() {
        return wpQtr4PipeAdvadjPayAmt;
    }

    public void setWpQtr4PipeAdvadjPayAmt(BigDecimal wpQtr4PipeAdvadjPayAmt) {
        this.wpQtr4PipeAdvadjPayAmt = wpQtr4PipeAdvadjPayAmt;
    }

    public BigDecimal getWpYearReceiptPayAmt() {
        return wpYearReceiptPayAmt;
    }

    public void setWpYearReceiptPayAmt(BigDecimal wpYearReceiptPayAmt) {
        this.wpYearReceiptPayAmt = wpYearReceiptPayAmt;
    }

    public BigDecimal getWpQtr1ReceiptPayAmt() {
        return wpQtr1ReceiptPayAmt;
    }

    public void setWpQtr1ReceiptPayAmt(BigDecimal wpQtr1ReceiptPayAmt) {
        this.wpQtr1ReceiptPayAmt = wpQtr1ReceiptPayAmt;
    }

    public BigDecimal getWpQtr2ReceiptPayAmt() {
        return wpQtr2ReceiptPayAmt;
    }

    public void setWpQtr2ReceiptPayAmt(BigDecimal wpQtr2ReceiptPayAmt) {
        this.wpQtr2ReceiptPayAmt = wpQtr2ReceiptPayAmt;
    }

    public BigDecimal getWpQtr3ReceiptPayAmt() {
        return wpQtr3ReceiptPayAmt;
    }

    public void setWpQtr3ReceiptPayAmt(BigDecimal wpQtr3ReceiptPayAmt) {
        this.wpQtr3ReceiptPayAmt = wpQtr3ReceiptPayAmt;
    }

    public BigDecimal getWpQtr4ReceiptPayAmt() {
        return wpQtr4ReceiptPayAmt;
    }

    public void setWpQtr4ReceiptPayAmt(BigDecimal wpQtr4ReceiptPayAmt) {
        this.wpQtr4ReceiptPayAmt = wpQtr4ReceiptPayAmt;
    }

    public BigDecimal getWpYearReceiptCommitAmt() {
        return wpYearReceiptCommitAmt;
    }

    public void setWpYearReceiptCommitAmt(BigDecimal wpYearReceiptCommitAmt) {
        this.wpYearReceiptCommitAmt = wpYearReceiptCommitAmt;
    }

    public BigDecimal getWpQtr1ReceiptCommitAmt() {
        return wpQtr1ReceiptCommitAmt;
    }

    public void setWpQtr1ReceiptCommitAmt(BigDecimal wpQtr1ReceiptCommitAmt) {
        this.wpQtr1ReceiptCommitAmt = wpQtr1ReceiptCommitAmt;
    }

    public BigDecimal getWpQtr2ReceiptCommitAmt() {
        return wpQtr2ReceiptCommitAmt;
    }

    public void setWpQtr2ReceiptCommitAmt(BigDecimal wpQtr2ReceiptCommitAmt) {
        this.wpQtr2ReceiptCommitAmt = wpQtr2ReceiptCommitAmt;
    }

    public BigDecimal getWpQtr3ReceiptCommitAmt() {
        return wpQtr3ReceiptCommitAmt;
    }

    public void setWpQtr3ReceiptCommitAmt(BigDecimal wpQtr3ReceiptCommitAmt) {
        this.wpQtr3ReceiptCommitAmt = wpQtr3ReceiptCommitAmt;
    }

    public BigDecimal getWpQtr4ReceiptCommitAmt() {
        return wpQtr4ReceiptCommitAmt;
    }

    public void setWpQtr4ReceiptCommitAmt(BigDecimal wpQtr4ReceiptCommitAmt) {
        this.wpQtr4ReceiptCommitAmt = wpQtr4ReceiptCommitAmt;
    }

    public BigDecimal getWpYearExpenseAmt() {
        return wpYearExpenseAmt;
    }

    public void setWpYearExpenseAmt(BigDecimal wpYearExpenseAmt) {
        this.wpYearExpenseAmt = wpYearExpenseAmt;
    }

    public BigDecimal getWpQtr1ExpenseAmt() {
        return wpQtr1ExpenseAmt;
    }

    public void setWpQtr1ExpenseAmt(BigDecimal wpQtr1ExpenseAmt) {
        this.wpQtr1ExpenseAmt = wpQtr1ExpenseAmt;
    }

    public BigDecimal getWpQtr2ExpenseAmt() {
        return wpQtr2ExpenseAmt;
    }

    public void setWpQtr2ExpenseAmt(BigDecimal wpQtr2ExpenseAmt) {
        this.wpQtr2ExpenseAmt = wpQtr2ExpenseAmt;
    }

    public BigDecimal getWpQtr3ExpenseAmt() {
        return wpQtr3ExpenseAmt;
    }

    public void setWpQtr3ExpenseAmt(BigDecimal wpQtr3ExpenseAmt) {
        this.wpQtr3ExpenseAmt = wpQtr3ExpenseAmt;
    }

    public BigDecimal getWpQtr4ExpenseAmt() {
        return wpQtr4ExpenseAmt;
    }

    public void setWpQtr4ExpenseAmt(BigDecimal wpQtr4ExpenseAmt) {
        this.wpQtr4ExpenseAmt = wpQtr4ExpenseAmt;
    }

    public BigDecimal getWpYearUsableBalanceAmt() {
        return wpYearUsableBalanceAmt;
    }

    public void setWpYearUsableBalanceAmt(BigDecimal wpYearUsableBalanceAmt) {
        this.wpYearUsableBalanceAmt = wpYearUsableBalanceAmt;
    }

    public BigDecimal getWpQtr1UsableBalanceAmt() {
        return wpQtr1UsableBalanceAmt;
    }

    public void setWpQtr1UsableBalanceAmt(BigDecimal wpQtr1UsableBalanceAmt) {
        this.wpQtr1UsableBalanceAmt = wpQtr1UsableBalanceAmt;
    }

    public BigDecimal getWpQtr2UsableBalanceAmt() {
        return wpQtr2UsableBalanceAmt;
    }

    public void setWpQtr2UsableBalanceAmt(BigDecimal wpQtr2UsableBalanceAmt) {
        this.wpQtr2UsableBalanceAmt = wpQtr2UsableBalanceAmt;
    }

    public BigDecimal getWpQtr3UsableBalanceAmt() {
        return wpQtr3UsableBalanceAmt;
    }

    public void setWpQtr3UsableBalanceAmt(BigDecimal wpQtr3UsableBalanceAmt) {
        this.wpQtr3UsableBalanceAmt = wpQtr3UsableBalanceAmt;
    }

    public BigDecimal getWpQtr4UsableBalanceAmt() {
        return wpQtr4UsableBalanceAmt;
    }

    public void setWpQtr4UsableBalanceAmt(BigDecimal wpQtr4UsableBalanceAmt) {
        this.wpQtr4UsableBalanceAmt = wpQtr4UsableBalanceAmt;
    }

    public BigDecimal getWpYearBalanceAmt() {
        return wpYearBalanceAmt;
    }

    public void setWpYearBalanceAmt(BigDecimal wpYearBalanceAmt) {
        this.wpYearBalanceAmt = wpYearBalanceAmt;
    }

    public BigDecimal getWpQtr1BalanceAmt() {
        return wpQtr1BalanceAmt;
    }

    public void setWpQtr1BalanceAmt(BigDecimal wpQtr1BalanceAmt) {
        this.wpQtr1BalanceAmt = wpQtr1BalanceAmt;
    }

    public BigDecimal getWpQtr2BalanceAmt() {
        return wpQtr2BalanceAmt;
    }

    public void setWpQtr2BalanceAmt(BigDecimal wpQtr2BalanceAmt) {
        this.wpQtr2BalanceAmt = wpQtr2BalanceAmt;
    }

    public BigDecimal getWpQtr3BalanceAmt() {
        return wpQtr3BalanceAmt;
    }

    public void setWpQtr3BalanceAmt(BigDecimal wpQtr3BalanceAmt) {
        this.wpQtr3BalanceAmt = wpQtr3BalanceAmt;
    }

    public BigDecimal getWpQtr4BalanceAmt() {
        return wpQtr4BalanceAmt;
    }

    public void setWpQtr4BalanceAmt(BigDecimal wpQtr4BalanceAmt) {
        this.wpQtr4BalanceAmt = wpQtr4BalanceAmt;
    }

    public BigDecimal getWpYearCarryFwdAdvAmt() {
        return wpYearCarryFwdAdvAmt;
    }

    public void setWpYearCarryFwdAdvAmt(BigDecimal wpYearCarryFwdAdvAmt) {
        this.wpYearCarryFwdAdvAmt = wpYearCarryFwdAdvAmt;
    }

    public BigDecimal getWpQtr1CarryFwdAdvAmt() {
        return wpQtr1CarryFwdAdvAmt;
    }

    public void setWpQtr1CarryFwdAdvAmt(BigDecimal wpQtr1CarryFwdAdvAmt) {
        this.wpQtr1CarryFwdAdvAmt = wpQtr1CarryFwdAdvAmt;
    }

    public BigDecimal getWpQtr2CarryFwdAdvAmt() {
        return wpQtr2CarryFwdAdvAmt;
    }

    public void setWpQtr2CarryFwdAdvAmt(BigDecimal wpQtr2CarryFwdAdvAmt) {
        this.wpQtr2CarryFwdAdvAmt = wpQtr2CarryFwdAdvAmt;
    }

    public BigDecimal getWpQtr3CarryFwdAdvAmt() {
        return wpQtr3CarryFwdAdvAmt;
    }

    public void setWpQtr3CarryFwdAdvAmt(BigDecimal wpQtr3CarryFwdAdvAmt) {
        this.wpQtr3CarryFwdAdvAmt = wpQtr3CarryFwdAdvAmt;
    }

    public BigDecimal getWpQtr4CarryFwdAdvAmt() {
        return wpQtr4CarryFwdAdvAmt;
    }

    public void setWpQtr4CarryFwdAdvAmt(BigDecimal wpQtr4CarryFwdAdvAmt) {
        this.wpQtr4CarryFwdAdvAmt = wpQtr4CarryFwdAdvAmt;
    }

    public Integer getWpVillagecd() {
        return wpVillagecd;
    }

    public void setWpVillagecd(Integer wpVillagecd) {
        this.wpVillagecd = wpVillagecd;
    }

    public String getWpSurveyNo() {
        return wpSurveyNo;
    }

    public void setWpSurveyNo(String wpSurveyNo) {
        this.wpSurveyNo = wpSurveyNo;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public BigDecimal getWpYearPipeCommitedAmt() {
        return wpYearPipeCommitedAmt;
    }

    public void setWpYearPipeCommitedAmt(BigDecimal wpYearPipeCommitedAmt) {
        this.wpYearPipeCommitedAmt = wpYearPipeCommitedAmt;
    }

    public BigDecimal getWpQtr1PipeCommitedAmt() {
        return wpQtr1PipeCommitedAmt;
    }

    public void setWpQtr1PipeCommitedAmt(BigDecimal wpQtr1PipeCommitedAmt) {
        this.wpQtr1PipeCommitedAmt = wpQtr1PipeCommitedAmt;
    }

    public BigDecimal getWpQtr2PipeCommitedAmt() {
        return wpQtr2PipeCommitedAmt;
    }

    public void setWpQtr2PipeCommitedAmt(BigDecimal wpQtr2PipeCommitedAmt) {
        this.wpQtr2PipeCommitedAmt = wpQtr2PipeCommitedAmt;
    }

    public BigDecimal getWpQtr3PipeCommitedAmt() {
        return wpQtr3PipeCommitedAmt;
    }

    public void setWpQtr3PipeCommitedAmt(BigDecimal wpQtr3PipeCommitedAmt) {
        this.wpQtr3PipeCommitedAmt = wpQtr3PipeCommitedAmt;
    }

    public BigDecimal getWpQtr4PipeCommitedAmt() {
        return wpQtr4PipeCommitedAmt;
    }

    public void setWpQtr4PipeCommitedAmt(BigDecimal wpQtr4PipeCommitedAmt) {
        this.wpQtr4PipeCommitedAmt = wpQtr4PipeCommitedAmt;
    }

    public short getWpTransactionAppliId() {
        return wpTransactionAppliId;
    }

    public void setWpTransactionAppliId(short wpTransactionAppliId) {
        this.wpTransactionAppliId = wpTransactionAppliId;
    }

    public BigDecimal getWpPercentOfUnit() {
        return wpPercentOfUnit;
    }

    public void setWpPercentOfUnit(BigDecimal wpPercentOfUnit) {
        this.wpPercentOfUnit = wpPercentOfUnit;
    }

    public BigDecimal getWpYearSurrenderAmt() {
        return wpYearSurrenderAmt;
    }

    public void setWpYearSurrenderAmt(BigDecimal wpYearSurrenderAmt) {
        this.wpYearSurrenderAmt = wpYearSurrenderAmt;
    }

    public BigDecimal getWpQtr1SurrenderAmt() {
        return wpQtr1SurrenderAmt;
    }

    public void setWpQtr1SurrenderAmt(BigDecimal wpQtr1SurrenderAmt) {
        this.wpQtr1SurrenderAmt = wpQtr1SurrenderAmt;
    }

    public BigDecimal getWpQtr2SurrenderAmt() {
        return wpQtr2SurrenderAmt;
    }

    public void setWpQtr2SurrenderAmt(BigDecimal wpQtr2SurrenderAmt) {
        this.wpQtr2SurrenderAmt = wpQtr2SurrenderAmt;
    }

    public BigDecimal getWpQtr3SurrenderAmt() {
        return wpQtr3SurrenderAmt;
    }

    public void setWpQtr3SurrenderAmt(BigDecimal wpQtr3SurrenderAmt) {
        this.wpQtr3SurrenderAmt = wpQtr3SurrenderAmt;
    }

    public BigDecimal getWpQtr4SurrenderAmt() {
        return wpQtr4SurrenderAmt;
    }

    public void setWpQtr4SurrenderAmt(BigDecimal wpQtr4SurrenderAmt) {
        this.wpQtr4SurrenderAmt = wpQtr4SurrenderAmt;
    }

    public BigDecimal getWpYearActiUnitExecuted() {
        return wpYearActiUnitExecuted;
    }

    public void setWpYearActiUnitExecuted(BigDecimal wpYearActiUnitExecuted) {
        this.wpYearActiUnitExecuted = wpYearActiUnitExecuted;
    }

    public BigDecimal getWpYearActiUnitSurrender() {
        return wpYearActiUnitSurrender;
    }

    public void setWpYearActiUnitSurrender(BigDecimal wpYearActiUnitSurrender) {
        this.wpYearActiUnitSurrender = wpYearActiUnitSurrender;
    }

    public BigDecimal getWpYearActiUnitPipeline() {
        return wpYearActiUnitPipeline;
    }

    public void setWpYearActiUnitPipeline(BigDecimal wpYearActiUnitPipeline) {
        this.wpYearActiUnitPipeline = wpYearActiUnitPipeline;
    }

    public BigDecimal getWpYearActiUnitBalance() {
        return wpYearActiUnitBalance;
    }

    public void setWpYearActiUnitBalance(BigDecimal wpYearActiUnitBalance) {
        this.wpYearActiUnitBalance = wpYearActiUnitBalance;
    }

    public BigDecimal getWpQtr1ActiInUnit() {
        return wpQtr1ActiInUnit;
    }

    public void setWpQtr1ActiInUnit(BigDecimal wpQtr1ActiInUnit) {
        this.wpQtr1ActiInUnit = wpQtr1ActiInUnit;
    }

    public BigDecimal getWpQtr2ActiInUnit() {
        return wpQtr2ActiInUnit;
    }

    public void setWpQtr2ActiInUnit(BigDecimal wpQtr2ActiInUnit) {
        this.wpQtr2ActiInUnit = wpQtr2ActiInUnit;
    }

    public BigDecimal getWpQtr3ActiInUnit() {
        return wpQtr3ActiInUnit;
    }

    public void setWpQtr3ActiInUnit(BigDecimal wpQtr3ActiInUnit) {
        this.wpQtr3ActiInUnit = wpQtr3ActiInUnit;
    }

    public BigDecimal getWpQtr4ActiInUnit() {
        return wpQtr4ActiInUnit;
    }

    public void setWpQtr4ActiInUnit(BigDecimal wpQtr4ActiInUnit) {
        this.wpQtr4ActiInUnit = wpQtr4ActiInUnit;
    }

    public BigDecimal getWpQtr1ActiUnitExecuted() {
        return wpQtr1ActiUnitExecuted;
    }

    public void setWpQtr1ActiUnitExecuted(BigDecimal wpQtr1ActiUnitExecuted) {
        this.wpQtr1ActiUnitExecuted = wpQtr1ActiUnitExecuted;
    }

    public BigDecimal getWpQtr2ActiUnitExecuted() {
        return wpQtr2ActiUnitExecuted;
    }

    public void setWpQtr2ActiUnitExecuted(BigDecimal wpQtr2ActiUnitExecuted) {
        this.wpQtr2ActiUnitExecuted = wpQtr2ActiUnitExecuted;
    }

    public BigDecimal getWpQtr3ActiUnitExecuted() {
        return wpQtr3ActiUnitExecuted;
    }

    public void setWpQtr3ActiUnitExecuted(BigDecimal wpQtr3ActiUnitExecuted) {
        this.wpQtr3ActiUnitExecuted = wpQtr3ActiUnitExecuted;
    }

    public BigDecimal getWpQtr4ActiUnitExecuted() {
        return wpQtr4ActiUnitExecuted;
    }

    public void setWpQtr4ActiUnitExecuted(BigDecimal wpQtr4ActiUnitExecuted) {
        this.wpQtr4ActiUnitExecuted = wpQtr4ActiUnitExecuted;
    }

    public BigDecimal getWpQtr1ActiUnitSurrender() {
        return wpQtr1ActiUnitSurrender;
    }

    public void setWpQtr1ActiUnitSurrender(BigDecimal wpQtr1ActiUnitSurrender) {
        this.wpQtr1ActiUnitSurrender = wpQtr1ActiUnitSurrender;
    }

    public BigDecimal getWpQtr2ActiUnitSurrender() {
        return wpQtr2ActiUnitSurrender;
    }

    public void setWpQtr2ActiUnitSurrender(BigDecimal wpQtr2ActiUnitSurrender) {
        this.wpQtr2ActiUnitSurrender = wpQtr2ActiUnitSurrender;
    }

    public BigDecimal getWpQtr3ActiUnitSurrender() {
        return wpQtr3ActiUnitSurrender;
    }

    public void setWpQtr3ActiUnitSurrender(BigDecimal wpQtr3ActiUnitSurrender) {
        this.wpQtr3ActiUnitSurrender = wpQtr3ActiUnitSurrender;
    }

    public BigDecimal getWpQtr4ActiUnitSurrender() {
        return wpQtr4ActiUnitSurrender;
    }

    public void setWpQtr4ActiUnitSurrender(BigDecimal wpQtr4ActiUnitSurrender) {
        this.wpQtr4ActiUnitSurrender = wpQtr4ActiUnitSurrender;
    }

    public BigDecimal getWpQtr1ActiUnitPipeline() {
        return wpQtr1ActiUnitPipeline;
    }

    public void setWpQtr1ActiUnitPipeline(BigDecimal wpQtr1ActiUnitPipeline) {
        this.wpQtr1ActiUnitPipeline = wpQtr1ActiUnitPipeline;
    }

    public BigDecimal getWpQtr2ActiUnitPipeline() {
        return wpQtr2ActiUnitPipeline;
    }

    public void setWpQtr2ActiUnitPipeline(BigDecimal wpQtr2ActiUnitPipeline) {
        this.wpQtr2ActiUnitPipeline = wpQtr2ActiUnitPipeline;
    }

    public BigDecimal getWpQtr3ActiUnitPipeline() {
        return wpQtr3ActiUnitPipeline;
    }

    public void setWpQtr3ActiUnitPipeline(BigDecimal wpQtr3ActiUnitPipeline) {
        this.wpQtr3ActiUnitPipeline = wpQtr3ActiUnitPipeline;
    }

    public BigDecimal getWpQtr4ActiUnitPipeline() {
        return wpQtr4ActiUnitPipeline;
    }

    public void setWpQtr4ActiUnitPipeline(BigDecimal wpQtr4ActiUnitPipeline) {
        this.wpQtr4ActiUnitPipeline = wpQtr4ActiUnitPipeline;
    }

    public BigDecimal getWpQtr1ActiUnitBalance() {
        return wpQtr1ActiUnitBalance;
    }

    public void setWpQtr1ActiUnitBalance(BigDecimal wpQtr1ActiUnitBalance) {
        this.wpQtr1ActiUnitBalance = wpQtr1ActiUnitBalance;
    }

    public BigDecimal getWpQtr2ActiUnitBalance() {
        return wpQtr2ActiUnitBalance;
    }

    public void setWpQtr2ActiUnitBalance(BigDecimal wpQtr2ActiUnitBalance) {
        this.wpQtr2ActiUnitBalance = wpQtr2ActiUnitBalance;
    }

    public BigDecimal getWpQtr3ActiUnitBalance() {
        return wpQtr3ActiUnitBalance;
    }

    public void setWpQtr3ActiUnitBalance(BigDecimal wpQtr3ActiUnitBalance) {
        this.wpQtr3ActiUnitBalance = wpQtr3ActiUnitBalance;
    }

    public BigDecimal getWpQtr4ActiUnitBalance() {
        return wpQtr4ActiUnitBalance;
    }

    public void setWpQtr4ActiUnitBalance(BigDecimal wpQtr4ActiUnitBalance) {
        this.wpQtr4ActiUnitBalance = wpQtr4ActiUnitBalance;
    }

    public String getWpFinalizedFlag() {
        return wpFinalizedFlag;
    }

    public void setWpFinalizedFlag(String wpFinalizedFlag) {
        this.wpFinalizedFlag = wpFinalizedFlag;
    }

    public Date getWpTransactionDate() {
        return wpTransactionDate;
    }

    public void setWpTransactionDate(Date wpTransactionDate) {
        this.wpTransactionDate = wpTransactionDate;
    }

    public Short getWpClosedQtrNo() {
        return wpClosedQtrNo;
    }

    public void setWpClosedQtrNo(Short wpClosedQtrNo) {
        this.wpClosedQtrNo = wpClosedQtrNo;
    }

    public BigDecimal getWpYearProCrExpAmt() {
        return wpYearProCrExpAmt;
    }

    public void setWpYearProCrExpAmt(BigDecimal wpYearProCrExpAmt) {
        this.wpYearProCrExpAmt = wpYearProCrExpAmt;
    }

    public BigDecimal getWpQtr1ProCrExpAmt() {
        return wpQtr1ProCrExpAmt;
    }

    public void setWpQtr1ProCrExpAmt(BigDecimal wpQtr1ProCrExpAmt) {
        this.wpQtr1ProCrExpAmt = wpQtr1ProCrExpAmt;
    }

    public BigDecimal getWpQtr2ProCrExpAmt() {
        return wpQtr2ProCrExpAmt;
    }

    public void setWpQtr2ProCrExpAmt(BigDecimal wpQtr2ProCrExpAmt) {
        this.wpQtr2ProCrExpAmt = wpQtr2ProCrExpAmt;
    }

    public BigDecimal getWpQtr3ProCrExpAmt() {
        return wpQtr3ProCrExpAmt;
    }

    public void setWpQtr3ProCrExpAmt(BigDecimal wpQtr3ProCrExpAmt) {
        this.wpQtr3ProCrExpAmt = wpQtr3ProCrExpAmt;
    }

    public BigDecimal getWpQtr4ProCrExpAmt() {
        return wpQtr4ProCrExpAmt;
    }

    public void setWpQtr4ProCrExpAmt(BigDecimal wpQtr4ProCrExpAmt) {
        this.wpQtr4ProCrExpAmt = wpQtr4ProCrExpAmt;
    }

    public BigDecimal getWpQtr1ReceivedActiUnit() {
        return wpQtr1ReceivedActiUnit;
    }

    public void setWpQtr1ReceivedActiUnit(BigDecimal wpQtr1ReceivedActiUnit) {
        this.wpQtr1ReceivedActiUnit = wpQtr1ReceivedActiUnit;
    }

    public BigDecimal getWpQtr2ReceivedActiUnit() {
        return wpQtr2ReceivedActiUnit;
    }

    public void setWpQtr2ReceivedActiUnit(BigDecimal wpQtr2ReceivedActiUnit) {
        this.wpQtr2ReceivedActiUnit = wpQtr2ReceivedActiUnit;
    }

    public BigDecimal getWpQtr3ReceivedActiUnit() {
        return wpQtr3ReceivedActiUnit;
    }

    public void setWpQtr3ReceivedActiUnit(BigDecimal wpQtr3ReceivedActiUnit) {
        this.wpQtr3ReceivedActiUnit = wpQtr3ReceivedActiUnit;
    }

    public BigDecimal getWpQtr4ReceivedActiUnit() {
        return wpQtr4ReceivedActiUnit;
    }

    public void setWpQtr4ReceivedActiUnit(BigDecimal wpQtr4ReceivedActiUnit) {
        this.wpQtr4ReceivedActiUnit = wpQtr4ReceivedActiUnit;
    }

    public BigDecimal getWpQtr1CarryFwdActiUnit() {
        return wpQtr1CarryFwdActiUnit;
    }

    public void setWpQtr1CarryFwdActiUnit(BigDecimal wpQtr1CarryFwdActiUnit) {
        this.wpQtr1CarryFwdActiUnit = wpQtr1CarryFwdActiUnit;
    }

    public BigDecimal getWpQtr2CarryFwdActiUnit() {
        return wpQtr2CarryFwdActiUnit;
    }

    public void setWpQtr2CarryFwdActiUnit(BigDecimal wpQtr2CarryFwdActiUnit) {
        this.wpQtr2CarryFwdActiUnit = wpQtr2CarryFwdActiUnit;
    }

    public BigDecimal getWpQtr3CarryFwdActiUnit() {
        return wpQtr3CarryFwdActiUnit;
    }

    public void setWpQtr3CarryFwdActiUnit(BigDecimal wpQtr3CarryFwdActiUnit) {
        this.wpQtr3CarryFwdActiUnit = wpQtr3CarryFwdActiUnit;
    }

    public BigDecimal getWpQtr4CarryFwdActiUnit() {
        return wpQtr4CarryFwdActiUnit;
    }

    public void setWpQtr4CarryFwdActiUnit(BigDecimal wpQtr4CarryFwdActiUnit) {
        this.wpQtr4CarryFwdActiUnit = wpQtr4CarryFwdActiUnit;
    }

    public BigDecimal getWpYearCommContriAmt() {
        return wpYearCommContriAmt;
    }

    public void setWpYearCommContriAmt(BigDecimal wpYearCommContriAmt) {
        this.wpYearCommContriAmt = wpYearCommContriAmt;
    }

    public BigDecimal getWpQtr1CommContriAmt() {
        return wpQtr1CommContriAmt;
    }

    public void setWpQtr1CommContriAmt(BigDecimal wpQtr1CommContriAmt) {
        this.wpQtr1CommContriAmt = wpQtr1CommContriAmt;
    }

    public BigDecimal getWpQtr2CommContriAmt() {
        return wpQtr2CommContriAmt;
    }

    public void setWpQtr2CommContriAmt(BigDecimal wpQtr2CommContriAmt) {
        this.wpQtr2CommContriAmt = wpQtr2CommContriAmt;
    }

    public BigDecimal getWpQtr3CommContriAmt() {
        return wpQtr3CommContriAmt;
    }

    public void setWpQtr3CommContriAmt(BigDecimal wpQtr3CommContriAmt) {
        this.wpQtr3CommContriAmt = wpQtr3CommContriAmt;
    }

    public BigDecimal getWpQtr4CommContriAmt() {
        return wpQtr4CommContriAmt;
    }

    public void setWpQtr4CommContriAmt(BigDecimal wpQtr4CommContriAmt) {
        this.wpQtr4CommContriAmt = wpQtr4CommContriAmt;
    }

    public String getWpPmFundMonitorFlag() {
        return wpPmFundMonitorFlag;
    }

    public void setWpPmFundMonitorFlag(String wpPmFundMonitorFlag) {
        this.wpPmFundMonitorFlag = wpPmFundMonitorFlag;
    }

    public String getPmFundType() {
        return pmFundType;
    }

    public void setPmFundType(String pmFundType) {
        this.pmFundType = pmFundType;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public BmwpActivityBudgetDtl getWpAbdUniqueSrno() {
        return wpAbdUniqueSrno;
    }

    public void setWpAbdUniqueSrno(BmwpActivityBudgetDtl wpAbdUniqueSrno) {
        this.wpAbdUniqueSrno = wpAbdUniqueSrno;
    }

    public BmwpActivityMst getWpAmUniqueSrno() {
        return wpAmUniqueSrno;
    }

    public void setWpAmUniqueSrno(BmwpActivityMst wpAmUniqueSrno) {
        this.wpAmUniqueSrno = wpAmUniqueSrno;
    }

    public BmwpAgencyProjectLink getWpAplUniqueSrno() {
        return wpAplUniqueSrno;
    }

    public void setWpAplUniqueSrno(BmwpAgencyProjectLink wpAplUniqueSrno) {
        this.wpAplUniqueSrno = wpAplUniqueSrno;
    }

    public BmwpAgencyTypeMst getWpAtmUniqueSrno() {
        return wpAtmUniqueSrno;
    }

    public void setWpAtmUniqueSrno(BmwpAgencyTypeMst wpAtmUniqueSrno) {
        this.wpAtmUniqueSrno = wpAtmUniqueSrno;
    }

    public BmwpApActivityBudgetDtl getWpApabdUniqueSrno() {
        return wpApabdUniqueSrno;
    }

    public void setWpApabdUniqueSrno(BmwpApActivityBudgetDtl wpApabdUniqueSrno) {
        this.wpApabdUniqueSrno = wpApabdUniqueSrno;
    }

    public BmwpApActivityMst getWpApamUniqueSrno() {
        return wpApamUniqueSrno;
    }

    public void setWpApamUniqueSrno(BmwpApActivityMst wpApamUniqueSrno) {
        this.wpApamUniqueSrno = wpApamUniqueSrno;
    }

    public BmwpApBudgetMst getWpApbmUniqueSrno() {
        return wpApbmUniqueSrno;
    }

    public void setWpApbmUniqueSrno(BmwpApBudgetMst wpApbmUniqueSrno) {
        this.wpApbmUniqueSrno = wpApbmUniqueSrno;
    }

    public BmwpBudgetMst getWpBmUniqueSrno() {
        return wpBmUniqueSrno;
    }

    public void setWpBmUniqueSrno(BmwpBudgetMst wpBmUniqueSrno) {
        this.wpBmUniqueSrno = wpBmUniqueSrno;
    }

    public BmwpFundAgencyMst getWpFamUniqueSrno() {
        return wpFamUniqueSrno;
    }

    public void setWpFamUniqueSrno(BmwpFundAgencyMst wpFamUniqueSrno) {
        this.wpFamUniqueSrno = wpFamUniqueSrno;
    }

    public BmwpPartyTypeDtl getWpPtdUniqueSrno() {
        return wpPtdUniqueSrno;
    }

    public void setWpPtdUniqueSrno(BmwpPartyTypeDtl wpPtdUniqueSrno) {
        this.wpPtdUniqueSrno = wpPtdUniqueSrno;
    }

    public BmwpProjectMst getWpPmUniqueSrno() {
        return wpPmUniqueSrno;
    }

    public void setWpPmUniqueSrno(BmwpProjectMst wpPmUniqueSrno) {
        this.wpPmUniqueSrno = wpPmUniqueSrno;
    }

    public BmwpUnitMst getWpActivityUmUniqueSrno() {
        return wpActivityUmUniqueSrno;
    }

    public void setWpActivityUmUniqueSrno(BmwpUnitMst wpActivityUmUniqueSrno) {
        this.wpActivityUmUniqueSrno = wpActivityUmUniqueSrno;
    }

    public BmwpWorkplan getBmwpWorkplan() {
        return bmwpWorkplan;
    }

    public void setBmwpWorkplan(BmwpWorkplan bmwpWorkplan) {
        this.bmwpWorkplan = bmwpWorkplan;
    }

    public BmwpWorkplan getRefWpUniqueSrno() {
        return refWpUniqueSrno;
    }

    public void setRefWpUniqueSrno(BmwpWorkplan refWpUniqueSrno) {
        this.refWpUniqueSrno = refWpUniqueSrno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (wpUniqueSrno != null ? wpUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmwpWorkplan)) {
            return false;
        }
        BmwpWorkplan other = (BmwpWorkplan) object;
        if ((this.wpUniqueSrno == null && other.wpUniqueSrno != null) || (this.wpUniqueSrno != null && !this.wpUniqueSrno.equals(other.wpUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.BmwpWorkplan[ wpUniqueSrno=" + wpUniqueSrno + " ]";
    }
    
}
