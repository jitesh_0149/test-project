/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_TADA_CASHALLOW_RULE_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasTadaCashallowRuleMst.findAll", query = "SELECT f FROM FtasTadaCashallowRuleMst f"),
    @NamedQuery(name = "FtasTadaCashallowRuleMst.findByCarmSrgKey", query = "SELECT f FROM FtasTadaCashallowRuleMst f WHERE f.carmSrgKey = :carmSrgKey"),
    @NamedQuery(name = "FtasTadaCashallowRuleMst.findByFromDay", query = "SELECT f FROM FtasTadaCashallowRuleMst f WHERE f.fromDay = :fromDay"),
    @NamedQuery(name = "FtasTadaCashallowRuleMst.findByToDay", query = "SELECT f FROM FtasTadaCashallowRuleMst f WHERE f.toDay = :toDay"),
    @NamedQuery(name = "FtasTadaCashallowRuleMst.findByCashAllowance", query = "SELECT f FROM FtasTadaCashallowRuleMst f WHERE f.cashAllowance = :cashAllowance"),
    @NamedQuery(name = "FtasTadaCashallowRuleMst.findByCashAllowRate", query = "SELECT f FROM FtasTadaCashallowRuleMst f WHERE f.cashAllowRate = :cashAllowRate"),
    @NamedQuery(name = "FtasTadaCashallowRuleMst.findByStartDate", query = "SELECT f FROM FtasTadaCashallowRuleMst f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FtasTadaCashallowRuleMst.findByEndDate", query = "SELECT f FROM FtasTadaCashallowRuleMst f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FtasTadaCashallowRuleMst.findByCreatedt", query = "SELECT f FROM FtasTadaCashallowRuleMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasTadaCashallowRuleMst.findByUpdatedt", query = "SELECT f FROM FtasTadaCashallowRuleMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasTadaCashallowRuleMst.findBySystemUserid", query = "SELECT f FROM FtasTadaCashallowRuleMst f WHERE f.systemUserid = :systemUserid")})
public class FtasTadaCashallowRuleMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CARM_SRG_KEY")
    private Long carmSrgKey;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "FROM_DAY")
    private BigDecimal fromDay;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TO_DAY")
    private BigDecimal toDay;
    @Column(name = "CASH_ALLOWANCE")
    private BigDecimal cashAllowance;
    @Column(name = "CASH_ALLOW_RATE")
    private BigDecimal cashAllowRate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "GCL_SRG_KEY", referencedColumnName = "GCL_SRG_KEY")
    @ManyToOne(optional = false)
    private FtasTadaGradeCityLink gclSrgKey;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "DSGM_SRG_KEY", referencedColumnName = "DSGM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdDesigmst dsgmSrgKey;

    public FtasTadaCashallowRuleMst() {
    }

    public FtasTadaCashallowRuleMst(Long carmSrgKey) {
        this.carmSrgKey = carmSrgKey;
    }

    public FtasTadaCashallowRuleMst(Long carmSrgKey, BigDecimal fromDay, BigDecimal toDay, Date startDate, Date createdt) {
        this.carmSrgKey = carmSrgKey;
        this.fromDay = fromDay;
        this.toDay = toDay;
        this.startDate = startDate;
        this.createdt = createdt;
    }

    public Long getCarmSrgKey() {
        return carmSrgKey;
    }

    public void setCarmSrgKey(Long carmSrgKey) {
        this.carmSrgKey = carmSrgKey;
    }

    public BigDecimal getFromDay() {
        return fromDay;
    }

    public void setFromDay(BigDecimal fromDay) {
        this.fromDay = fromDay;
    }

    public BigDecimal getToDay() {
        return toDay;
    }

    public void setToDay(BigDecimal toDay) {
        this.toDay = toDay;
    }

    public BigDecimal getCashAllowance() {
        return cashAllowance;
    }

    public void setCashAllowance(BigDecimal cashAllowance) {
        this.cashAllowance = cashAllowance;
    }

    public BigDecimal getCashAllowRate() {
        return cashAllowRate;
    }

    public void setCashAllowRate(BigDecimal cashAllowRate) {
        this.cashAllowRate = cashAllowRate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasTadaGradeCityLink getGclSrgKey() {
        return gclSrgKey;
    }

    public void setGclSrgKey(FtasTadaGradeCityLink gclSrgKey) {
        this.gclSrgKey = gclSrgKey;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdDesigmst getDsgmSrgKey() {
        return dsgmSrgKey;
    }

    public void setDsgmSrgKey(FhrdDesigmst dsgmSrgKey) {
        this.dsgmSrgKey = dsgmSrgKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (carmSrgKey != null ? carmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasTadaCashallowRuleMst)) {
            return false;
        }
        FtasTadaCashallowRuleMst other = (FtasTadaCashallowRuleMst) object;
        if ((this.carmSrgKey == null && other.carmSrgKey != null) || (this.carmSrgKey != null && !this.carmSrgKey.equals(other.carmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasTadaCashallowRuleMst[ carmSrgKey=" + carmSrgKey + " ]";
    }
    
}
