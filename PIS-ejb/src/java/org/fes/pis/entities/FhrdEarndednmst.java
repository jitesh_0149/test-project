/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_EARNDEDNMST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdEarndednmst.findAll", query = "SELECT f FROM FhrdEarndednmst f"),
    @NamedQuery(name = "FhrdEarndednmst.findByEarndedncd", query = "SELECT f FROM FhrdEarndednmst f WHERE f.earndedncd = :earndedncd"),
    @NamedQuery(name = "FhrdEarndednmst.findByEarndednName", query = "SELECT f FROM FhrdEarndednmst f WHERE f.earndednName = :earndednName"),
    @NamedQuery(name = "FhrdEarndednmst.findByEarndednFlag", query = "SELECT f FROM FhrdEarndednmst f WHERE f.earndednFlag = :earndednFlag"),
    @NamedQuery(name = "FhrdEarndednmst.findByEarndednType", query = "SELECT f FROM FhrdEarndednmst f WHERE f.earndednType = :earndednType"),
    @NamedQuery(name = "FhrdEarndednmst.findByBasecodeFlag", query = "SELECT f FROM FhrdEarndednmst f WHERE f.basecodeFlag = :basecodeFlag"),
    @NamedQuery(name = "FhrdEarndednmst.findByLevelNo", query = "SELECT f FROM FhrdEarndednmst f WHERE f.levelNo = :levelNo"),
    @NamedQuery(name = "FhrdEarndednmst.findByPreferenceNo", query = "SELECT f FROM FhrdEarndednmst f WHERE f.preferenceNo = :preferenceNo"),
    @NamedQuery(name = "FhrdEarndednmst.findByPrintOrder", query = "SELECT f FROM FhrdEarndednmst f WHERE f.printOrder = :printOrder"),
    @NamedQuery(name = "FhrdEarndednmst.findByFirstEntryFlag", query = "SELECT f FROM FhrdEarndednmst f WHERE f.firstEntryFlag = :firstEntryFlag"),
    @NamedQuery(name = "FhrdEarndednmst.findByPrintInPaySlip", query = "SELECT f FROM FhrdEarndednmst f WHERE f.printInPaySlip = :printInPaySlip"),
    @NamedQuery(name = "FhrdEarndednmst.findByPrintIf0Flag", query = "SELECT f FROM FhrdEarndednmst f WHERE f.printIf0Flag = :printIf0Flag"),
    @NamedQuery(name = "FhrdEarndednmst.findByPayableOnValue", query = "SELECT f FROM FhrdEarndednmst f WHERE f.payableOnValue = :payableOnValue"),
    @NamedQuery(name = "FhrdEarndednmst.findByCreatedt", query = "SELECT f FROM FhrdEarndednmst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdEarndednmst.findByUpdatedt", query = "SELECT f FROM FhrdEarndednmst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdEarndednmst.findBySystemUserid", query = "SELECT f FROM FhrdEarndednmst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdEarndednmst.findByExportFlag", query = "SELECT f FROM FhrdEarndednmst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdEarndednmst.findByImportFlag", query = "SELECT f FROM FhrdEarndednmst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdEarndednmst.findByEdmSrgKey", query = "SELECT f FROM FhrdEarndednmst f WHERE f.edmSrgKey = :edmSrgKey"),
    @NamedQuery(name = "FhrdEarndednmst.findByContractTypeFlag", query = "SELECT f FROM FhrdEarndednmst f WHERE f.contractTypeFlag = :contractTypeFlag"),
    @NamedQuery(name = "FhrdEarndednmst.findByStartDate", query = "SELECT f FROM FhrdEarndednmst f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FhrdEarndednmst.findByEndDate", query = "SELECT f FROM FhrdEarndednmst f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FhrdEarndednmst.findByEdmAltSrno", query = "SELECT f FROM FhrdEarndednmst f WHERE f.edmAltSrno = :edmAltSrno"),
    @NamedQuery(name = "FhrdEarndednmst.findByEdmAltSubsrno", query = "SELECT f FROM FhrdEarndednmst f WHERE f.edmAltSubsrno = :edmAltSubsrno"),
    @NamedQuery(name = "FhrdEarndednmst.findByTaxableFlag", query = "SELECT f FROM FhrdEarndednmst f WHERE f.taxableFlag = :taxableFlag"),
    @NamedQuery(name = "FhrdEarndednmst.findByStatutoryFlag", query = "SELECT f FROM FhrdEarndednmst f WHERE f.statutoryFlag = :statutoryFlag")})
public class FhrdEarndednmst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "EARNDEDNCD")
    private String earndedncd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "EARNDEDN_NAME")
    private String earndednName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EARNDEDN_FLAG")
    private String earndednFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EARNDEDN_TYPE")
    private String earndednType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "BASECODE_FLAG")
    private String basecodeFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "LEVEL_NO")
    private short levelNo;
    @Column(name = "PREFERENCE_NO")
    private Short preferenceNo;
    @Column(name = "PRINT_ORDER")
    private Short printOrder;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "FIRST_ENTRY_FLAG")
    private String firstEntryFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PRINT_IN_PAY_SLIP")
    private String printInPaySlip;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PRINT_IF_0_FLAG")
    private String printIf0Flag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "PAYABLE_ON_VALUE")
    private String payableOnValue;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "EDM_SRG_KEY")
    private BigDecimal edmSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "CONTRACT_TYPE_FLAG")
    private String contractTypeFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Column(name = "EDM_ALT_SRNO")
    private Integer edmAltSrno;
    @Column(name = "EDM_ALT_SUBSRNO")
    private Integer edmAltSubsrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "TAXABLE_FLAG")
    private String taxableFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "STATUTORY_FLAG")
    private String statutoryFlag;
    @OneToMany(mappedBy = "ptedsEdmSrgKey")
    private Collection<FhrdPaytransearndednSummary> fhrdPaytransearndednSummaryCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fhrdEarndednmst")
    private Collection<FhrdPaytransearndedn> fhrdPaytransearndednCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "edmSrgKey")
    private Collection<FhrdEarndedn> fhrdEarndednCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "edmSrgKey")
    private Collection<FhrdEmpearndedn> fhrdEmpearndednCollection;

    public FhrdEarndednmst() {
    }

    public FhrdEarndednmst(BigDecimal edmSrgKey) {
        this.edmSrgKey = edmSrgKey;
    }

    public FhrdEarndednmst(BigDecimal edmSrgKey, String earndedncd, String earndednName, String earndednFlag, String earndednType, String basecodeFlag, short levelNo, String firstEntryFlag, String printInPaySlip, String printIf0Flag, String payableOnValue, Date createdt, String contractTypeFlag, Date startDate, String taxableFlag, String statutoryFlag) {
        this.edmSrgKey = edmSrgKey;
        this.earndedncd = earndedncd;
        this.earndednName = earndednName;
        this.earndednFlag = earndednFlag;
        this.earndednType = earndednType;
        this.basecodeFlag = basecodeFlag;
        this.levelNo = levelNo;
        this.firstEntryFlag = firstEntryFlag;
        this.printInPaySlip = printInPaySlip;
        this.printIf0Flag = printIf0Flag;
        this.payableOnValue = payableOnValue;
        this.createdt = createdt;
        this.contractTypeFlag = contractTypeFlag;
        this.startDate = startDate;
        this.taxableFlag = taxableFlag;
        this.statutoryFlag = statutoryFlag;
    }

    public String getEarndedncd() {
        return earndedncd;
    }

    public void setEarndedncd(String earndedncd) {
        this.earndedncd = earndedncd;
    }

    public String getEarndednName() {
        return earndednName;
    }

    public void setEarndednName(String earndednName) {
        this.earndednName = earndednName;
    }

    public String getEarndednFlag() {
        return earndednFlag;
    }

    public void setEarndednFlag(String earndednFlag) {
        this.earndednFlag = earndednFlag;
    }

    public String getEarndednType() {
        return earndednType;
    }

    public void setEarndednType(String earndednType) {
        this.earndednType = earndednType;
    }

    public String getBasecodeFlag() {
        return basecodeFlag;
    }

    public void setBasecodeFlag(String basecodeFlag) {
        this.basecodeFlag = basecodeFlag;
    }

    public short getLevelNo() {
        return levelNo;
    }

    public void setLevelNo(short levelNo) {
        this.levelNo = levelNo;
    }

    public Short getPreferenceNo() {
        return preferenceNo;
    }

    public void setPreferenceNo(Short preferenceNo) {
        this.preferenceNo = preferenceNo;
    }

    public Short getPrintOrder() {
        return printOrder;
    }

    public void setPrintOrder(Short printOrder) {
        this.printOrder = printOrder;
    }

    public String getFirstEntryFlag() {
        return firstEntryFlag;
    }

    public void setFirstEntryFlag(String firstEntryFlag) {
        this.firstEntryFlag = firstEntryFlag;
    }

    public String getPrintInPaySlip() {
        return printInPaySlip;
    }

    public void setPrintInPaySlip(String printInPaySlip) {
        this.printInPaySlip = printInPaySlip;
    }

    public String getPrintIf0Flag() {
        return printIf0Flag;
    }

    public void setPrintIf0Flag(String printIf0Flag) {
        this.printIf0Flag = printIf0Flag;
    }

    public String getPayableOnValue() {
        return payableOnValue;
    }

    public void setPayableOnValue(String payableOnValue) {
        this.payableOnValue = payableOnValue;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public BigDecimal getEdmSrgKey() {
        return edmSrgKey;
    }

    public void setEdmSrgKey(BigDecimal edmSrgKey) {
        this.edmSrgKey = edmSrgKey;
    }

    public String getContractTypeFlag() {
        return contractTypeFlag;
    }

    public void setContractTypeFlag(String contractTypeFlag) {
        this.contractTypeFlag = contractTypeFlag;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getEdmAltSrno() {
        return edmAltSrno;
    }

    public void setEdmAltSrno(Integer edmAltSrno) {
        this.edmAltSrno = edmAltSrno;
    }

    public Integer getEdmAltSubsrno() {
        return edmAltSubsrno;
    }

    public void setEdmAltSubsrno(Integer edmAltSubsrno) {
        this.edmAltSubsrno = edmAltSubsrno;
    }

    public String getTaxableFlag() {
        return taxableFlag;
    }

    public void setTaxableFlag(String taxableFlag) {
        this.taxableFlag = taxableFlag;
    }

    public String getStatutoryFlag() {
        return statutoryFlag;
    }

    public void setStatutoryFlag(String statutoryFlag) {
        this.statutoryFlag = statutoryFlag;
    }

    @XmlTransient
    public Collection<FhrdPaytransearndednSummary> getFhrdPaytransearndednSummaryCollection() {
        return fhrdPaytransearndednSummaryCollection;
    }

    public void setFhrdPaytransearndednSummaryCollection(Collection<FhrdPaytransearndednSummary> fhrdPaytransearndednSummaryCollection) {
        this.fhrdPaytransearndednSummaryCollection = fhrdPaytransearndednSummaryCollection;
    }

    @XmlTransient
    public Collection<FhrdPaytransearndedn> getFhrdPaytransearndednCollection() {
        return fhrdPaytransearndednCollection;
    }

    public void setFhrdPaytransearndednCollection(Collection<FhrdPaytransearndedn> fhrdPaytransearndednCollection) {
        this.fhrdPaytransearndednCollection = fhrdPaytransearndednCollection;
    }

    @XmlTransient
    public Collection<FhrdEarndedn> getFhrdEarndednCollection() {
        return fhrdEarndednCollection;
    }

    public void setFhrdEarndednCollection(Collection<FhrdEarndedn> fhrdEarndednCollection) {
        this.fhrdEarndednCollection = fhrdEarndednCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @XmlTransient
    public Collection<FhrdEmpearndedn> getFhrdEmpearndednCollection() {
        return fhrdEmpearndednCollection;
    }

    public void setFhrdEmpearndednCollection(Collection<FhrdEmpearndedn> fhrdEmpearndednCollection) {
        this.fhrdEmpearndednCollection = fhrdEmpearndednCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (edmSrgKey != null ? edmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdEarndednmst)) {
            return false;
        }
        FhrdEarndednmst other = (FhrdEarndednmst) object;
        if ((this.edmSrgKey == null && other.edmSrgKey != null) || (this.edmSrgKey != null && !this.edmSrgKey.equals(other.edmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdEarndednmst[ edmSrgKey=" + edmSrgKey + " ]";
    }
    
}
