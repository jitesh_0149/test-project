/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "BMWP_PARTY_TYPE_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmwpPartyTypeDtl.findAll", query = "SELECT b FROM BmwpPartyTypeDtl b"),
    @NamedQuery(name = "BmwpPartyTypeDtl.findByPtdUniqueSrno", query = "SELECT b FROM BmwpPartyTypeDtl b WHERE b.ptdUniqueSrno = :ptdUniqueSrno"),
    @NamedQuery(name = "BmwpPartyTypeDtl.findByPtdSrno", query = "SELECT b FROM BmwpPartyTypeDtl b WHERE b.ptdSrno = :ptdSrno"),
    @NamedQuery(name = "BmwpPartyTypeDtl.findByPtdPartysrno", query = "SELECT b FROM BmwpPartyTypeDtl b WHERE b.ptdPartysrno = :ptdPartysrno"),
    @NamedQuery(name = "BmwpPartyTypeDtl.findByPtdName", query = "SELECT b FROM BmwpPartyTypeDtl b WHERE b.ptdName = :ptdName"),
    @NamedQuery(name = "BmwpPartyTypeDtl.findByPtdFinPartyFlag", query = "SELECT b FROM BmwpPartyTypeDtl b WHERE b.ptdFinPartyFlag = :ptdFinPartyFlag"),
    @NamedQuery(name = "BmwpPartyTypeDtl.findByPtdWorkplanPartyFlag", query = "SELECT b FROM BmwpPartyTypeDtl b WHERE b.ptdWorkplanPartyFlag = :ptdWorkplanPartyFlag"),
    @NamedQuery(name = "BmwpPartyTypeDtl.findByPtdVillagecd", query = "SELECT b FROM BmwpPartyTypeDtl b WHERE b.ptdVillagecd = :ptdVillagecd"),
    @NamedQuery(name = "BmwpPartyTypeDtl.findByPtdSurveyNo", query = "SELECT b FROM BmwpPartyTypeDtl b WHERE b.ptdSurveyNo = :ptdSurveyNo"),
    @NamedQuery(name = "BmwpPartyTypeDtl.findByCreatedt", query = "SELECT b FROM BmwpPartyTypeDtl b WHERE b.createdt = :createdt"),
    @NamedQuery(name = "BmwpPartyTypeDtl.findByUpdatedt", query = "SELECT b FROM BmwpPartyTypeDtl b WHERE b.updatedt = :updatedt"),
    @NamedQuery(name = "BmwpPartyTypeDtl.findBySystemUserid", query = "SELECT b FROM BmwpPartyTypeDtl b WHERE b.systemUserid = :systemUserid"),
    @NamedQuery(name = "BmwpPartyTypeDtl.findByExportFlag", query = "SELECT b FROM BmwpPartyTypeDtl b WHERE b.exportFlag = :exportFlag"),
    @NamedQuery(name = "BmwpPartyTypeDtl.findByImportFlag", query = "SELECT b FROM BmwpPartyTypeDtl b WHERE b.importFlag = :importFlag"),
    @NamedQuery(name = "BmwpPartyTypeDtl.findByPtdPanNo", query = "SELECT b FROM BmwpPartyTypeDtl b WHERE b.ptdPanNo = :ptdPanNo"),
    @NamedQuery(name = "BmwpPartyTypeDtl.findByPmFundType", query = "SELECT b FROM BmwpPartyTypeDtl b WHERE b.pmFundType = :pmFundType")})
public class BmwpPartyTypeDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PTD_UNIQUE_SRNO")
    private BigDecimal ptdUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PTD_SRNO")
    private int ptdSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PTD_PARTYSRNO")
    private BigDecimal ptdPartysrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "PTD_NAME")
    private String ptdName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PTD_FIN_PARTY_FLAG")
    private String ptdFinPartyFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PTD_WORKPLAN_PARTY_FLAG")
    private String ptdWorkplanPartyFlag;
    @Column(name = "PTD_VILLAGECD")
    private Integer ptdVillagecd;
    @Size(max = 150)
    @Column(name = "PTD_SURVEY_NO")
    private String ptdSurveyNo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Size(max = 30)
    @Column(name = "PTD_PAN_NO")
    private String ptdPanNo;
    @Size(max = 1)
    @Column(name = "PM_FUND_TYPE")
    private String pmFundType;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "PTD_PTM_UNIQUE_SRNO", referencedColumnName = "PTM_UNIQUE_SRNO")
    @ManyToOne(optional = false)
    private BmwpPartyTypeMst ptdPtmUniqueSrno;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "wpPtdUniqueSrno")
    private Collection<BmwpWorkplan> bmwpWorkplanCollection;

    public BmwpPartyTypeDtl() {
    }

    public BmwpPartyTypeDtl(BigDecimal ptdUniqueSrno) {
        this.ptdUniqueSrno = ptdUniqueSrno;
    }

    public BmwpPartyTypeDtl(BigDecimal ptdUniqueSrno, int ptdSrno, BigDecimal ptdPartysrno, String ptdName, String ptdFinPartyFlag, String ptdWorkplanPartyFlag, Date createdt) {
        this.ptdUniqueSrno = ptdUniqueSrno;
        this.ptdSrno = ptdSrno;
        this.ptdPartysrno = ptdPartysrno;
        this.ptdName = ptdName;
        this.ptdFinPartyFlag = ptdFinPartyFlag;
        this.ptdWorkplanPartyFlag = ptdWorkplanPartyFlag;
        this.createdt = createdt;
    }

    public BigDecimal getPtdUniqueSrno() {
        return ptdUniqueSrno;
    }

    public void setPtdUniqueSrno(BigDecimal ptdUniqueSrno) {
        this.ptdUniqueSrno = ptdUniqueSrno;
    }

    public int getPtdSrno() {
        return ptdSrno;
    }

    public void setPtdSrno(int ptdSrno) {
        this.ptdSrno = ptdSrno;
    }

    public BigDecimal getPtdPartysrno() {
        return ptdPartysrno;
    }

    public void setPtdPartysrno(BigDecimal ptdPartysrno) {
        this.ptdPartysrno = ptdPartysrno;
    }

    public String getPtdName() {
        return ptdName;
    }

    public void setPtdName(String ptdName) {
        this.ptdName = ptdName;
    }

    public String getPtdFinPartyFlag() {
        return ptdFinPartyFlag;
    }

    public void setPtdFinPartyFlag(String ptdFinPartyFlag) {
        this.ptdFinPartyFlag = ptdFinPartyFlag;
    }

    public String getPtdWorkplanPartyFlag() {
        return ptdWorkplanPartyFlag;
    }

    public void setPtdWorkplanPartyFlag(String ptdWorkplanPartyFlag) {
        this.ptdWorkplanPartyFlag = ptdWorkplanPartyFlag;
    }

    public Integer getPtdVillagecd() {
        return ptdVillagecd;
    }

    public void setPtdVillagecd(Integer ptdVillagecd) {
        this.ptdVillagecd = ptdVillagecd;
    }

    public String getPtdSurveyNo() {
        return ptdSurveyNo;
    }

    public void setPtdSurveyNo(String ptdSurveyNo) {
        this.ptdSurveyNo = ptdSurveyNo;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public String getPtdPanNo() {
        return ptdPanNo;
    }

    public void setPtdPanNo(String ptdPanNo) {
        this.ptdPanNo = ptdPanNo;
    }

    public String getPmFundType() {
        return pmFundType;
    }

    public void setPmFundType(String pmFundType) {
        this.pmFundType = pmFundType;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public BmwpPartyTypeMst getPtdPtmUniqueSrno() {
        return ptdPtmUniqueSrno;
    }

    public void setPtdPtmUniqueSrno(BmwpPartyTypeMst ptdPtmUniqueSrno) {
        this.ptdPtmUniqueSrno = ptdPtmUniqueSrno;
    }

    @XmlTransient
    public Collection<BmwpWorkplan> getBmwpWorkplanCollection() {
        return bmwpWorkplanCollection;
    }

    public void setBmwpWorkplanCollection(Collection<BmwpWorkplan> bmwpWorkplanCollection) {
        this.bmwpWorkplanCollection = bmwpWorkplanCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ptdUniqueSrno != null ? ptdUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmwpPartyTypeDtl)) {
            return false;
        }
        BmwpPartyTypeDtl other = (BmwpPartyTypeDtl) object;
        if ((this.ptdUniqueSrno == null && other.ptdUniqueSrno != null) || (this.ptdUniqueSrno != null && !this.ptdUniqueSrno.equals(other.ptdUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.BmwpPartyTypeDtl[ ptdUniqueSrno=" + ptdUniqueSrno + " ]";
    }
    
}
