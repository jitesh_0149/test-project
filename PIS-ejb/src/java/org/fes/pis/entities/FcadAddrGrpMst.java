/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FCAD_ADDR_GRP_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FcadAddrGrpMst.findAll", query = "SELECT f FROM FcadAddrGrpMst f"),
    @NamedQuery(name = "FcadAddrGrpMst.findByAgmUniqueSrno", query = "SELECT f FROM FcadAddrGrpMst f WHERE f.agmUniqueSrno = :agmUniqueSrno"),
    @NamedQuery(name = "FcadAddrGrpMst.findByAgmGrpCd", query = "SELECT f FROM FcadAddrGrpMst f WHERE f.agmGrpCd = :agmGrpCd"),
    @NamedQuery(name = "FcadAddrGrpMst.findByAgmGrpDesc", query = "SELECT f FROM FcadAddrGrpMst f WHERE f.agmGrpDesc = :agmGrpDesc"),
    @NamedQuery(name = "FcadAddrGrpMst.findByAgmSelectionFlag", query = "SELECT f FROM FcadAddrGrpMst f WHERE f.agmSelectionFlag = :agmSelectionFlag"),
    @NamedQuery(name = "FcadAddrGrpMst.findByAgmOrderno", query = "SELECT f FROM FcadAddrGrpMst f WHERE f.agmOrderno = :agmOrderno"),
    @NamedQuery(name = "FcadAddrGrpMst.findByAgmPurpose", query = "SELECT f FROM FcadAddrGrpMst f WHERE f.agmPurpose = :agmPurpose"),
    @NamedQuery(name = "FcadAddrGrpMst.findByAgmVisibilityFlag", query = "SELECT f FROM FcadAddrGrpMst f WHERE f.agmVisibilityFlag = :agmVisibilityFlag"),
    @NamedQuery(name = "FcadAddrGrpMst.findByAgmUserManFlag", query = "SELECT f FROM FcadAddrGrpMst f WHERE f.agmUserManFlag = :agmUserManFlag"),
    @NamedQuery(name = "FcadAddrGrpMst.findByCreatedt", query = "SELECT f FROM FcadAddrGrpMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FcadAddrGrpMst.findByUpdatedt", query = "SELECT f FROM FcadAddrGrpMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FcadAddrGrpMst.findBySystemUserid", query = "SELECT f FROM FcadAddrGrpMst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FcadAddrGrpMst.findByExportFlag", query = "SELECT f FROM FcadAddrGrpMst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FcadAddrGrpMst.findByImportFlag", query = "SELECT f FROM FcadAddrGrpMst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FcadAddrGrpMst.findByStartDate", query = "SELECT f FROM FcadAddrGrpMst f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FcadAddrGrpMst.findByEndDate", query = "SELECT f FROM FcadAddrGrpMst f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FcadAddrGrpMst.findByAgmDisplayInCadFlag", query = "SELECT f FROM FcadAddrGrpMst f WHERE f.agmDisplayInCadFlag = :agmDisplayInCadFlag"),
    @NamedQuery(name = "FcadAddrGrpMst.findByAgmVisibilityName", query = "SELECT f FROM FcadAddrGrpMst f WHERE f.agmVisibilityName = :agmVisibilityName")})
public class FcadAddrGrpMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "AGM_UNIQUE_SRNO")
    private Long agmUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "AGM_GRP_CD")
    private String agmGrpCd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "AGM_GRP_DESC")
    private String agmGrpDesc;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AGM_SELECTION_FLAG")
    private String agmSelectionFlag;
    @Column(name = "AGM_ORDERNO")
    private Short agmOrderno;
    @Size(max = 100)
    @Column(name = "AGM_PURPOSE")
    private String agmPurpose;
    @Size(max = 1)
    @Column(name = "AGM_VISIBILITY_FLAG")
    private String agmVisibilityFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AGM_USER_MAN_FLAG")
    private String agmUserManFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "AGM_DISPLAY_IN_CAD_FLAG")
    private String agmDisplayInCadFlag;
    @Size(max = 200)
    @Column(name = "AGM_VISIBILITY_NAME")
    private String agmVisibilityName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "agdAgmUniqueSrno")
    private Collection<FcadAddrGrpDtl> fcadAddrGrpDtlCollection;
    @JoinColumn(name = "CREATEBY_OU", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst createbyOu;
    @JoinColumn(name = "AGM_VISIBILITY_TO_OU", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst agmVisibilityToOu;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;

    public FcadAddrGrpMst() {
    }

    public FcadAddrGrpMst(Long agmUniqueSrno) {
        this.agmUniqueSrno = agmUniqueSrno;
    }

    public FcadAddrGrpMst(Long agmUniqueSrno, String agmGrpCd, String agmGrpDesc, String agmSelectionFlag, String agmUserManFlag, Date createdt, Date startDate, String agmDisplayInCadFlag) {
        this.agmUniqueSrno = agmUniqueSrno;
        this.agmGrpCd = agmGrpCd;
        this.agmGrpDesc = agmGrpDesc;
        this.agmSelectionFlag = agmSelectionFlag;
        this.agmUserManFlag = agmUserManFlag;
        this.createdt = createdt;
        this.startDate = startDate;
        this.agmDisplayInCadFlag = agmDisplayInCadFlag;
    }

    public Long getAgmUniqueSrno() {
        return agmUniqueSrno;
    }

    public void setAgmUniqueSrno(Long agmUniqueSrno) {
        this.agmUniqueSrno = agmUniqueSrno;
    }

    public String getAgmGrpCd() {
        return agmGrpCd;
    }

    public void setAgmGrpCd(String agmGrpCd) {
        this.agmGrpCd = agmGrpCd;
    }

    public String getAgmGrpDesc() {
        return agmGrpDesc;
    }

    public void setAgmGrpDesc(String agmGrpDesc) {
        this.agmGrpDesc = agmGrpDesc;
    }

    public String getAgmSelectionFlag() {
        return agmSelectionFlag;
    }

    public void setAgmSelectionFlag(String agmSelectionFlag) {
        this.agmSelectionFlag = agmSelectionFlag;
    }

    public Short getAgmOrderno() {
        return agmOrderno;
    }

    public void setAgmOrderno(Short agmOrderno) {
        this.agmOrderno = agmOrderno;
    }

    public String getAgmPurpose() {
        return agmPurpose;
    }

    public void setAgmPurpose(String agmPurpose) {
        this.agmPurpose = agmPurpose;
    }

    public String getAgmVisibilityFlag() {
        return agmVisibilityFlag;
    }

    public void setAgmVisibilityFlag(String agmVisibilityFlag) {
        this.agmVisibilityFlag = agmVisibilityFlag;
    }

    public String getAgmUserManFlag() {
        return agmUserManFlag;
    }

    public void setAgmUserManFlag(String agmUserManFlag) {
        this.agmUserManFlag = agmUserManFlag;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getAgmDisplayInCadFlag() {
        return agmDisplayInCadFlag;
    }

    public void setAgmDisplayInCadFlag(String agmDisplayInCadFlag) {
        this.agmDisplayInCadFlag = agmDisplayInCadFlag;
    }

    public String getAgmVisibilityName() {
        return agmVisibilityName;
    }

    public void setAgmVisibilityName(String agmVisibilityName) {
        this.agmVisibilityName = agmVisibilityName;
    }

    @XmlTransient
    public Collection<FcadAddrGrpDtl> getFcadAddrGrpDtlCollection() {
        return fcadAddrGrpDtlCollection;
    }

    public void setFcadAddrGrpDtlCollection(Collection<FcadAddrGrpDtl> fcadAddrGrpDtlCollection) {
        this.fcadAddrGrpDtlCollection = fcadAddrGrpDtlCollection;
    }

    public SystOrgUnitMst getCreatebyOu() {
        return createbyOu;
    }

    public void setCreatebyOu(SystOrgUnitMst createbyOu) {
        this.createbyOu = createbyOu;
    }

    public SystOrgUnitMst getAgmVisibilityToOu() {
        return agmVisibilityToOu;
    }

    public void setAgmVisibilityToOu(SystOrgUnitMst agmVisibilityToOu) {
        this.agmVisibilityToOu = agmVisibilityToOu;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (agmUniqueSrno != null ? agmUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FcadAddrGrpMst)) {
            return false;
        }
        FcadAddrGrpMst other = (FcadAddrGrpMst) object;
        if ((this.agmUniqueSrno == null && other.agmUniqueSrno != null) || (this.agmUniqueSrno != null && !this.agmUniqueSrno.equals(other.agmUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FcadAddrGrpMst[ agmUniqueSrno=" + agmUniqueSrno + " ]";
    }
    
}
