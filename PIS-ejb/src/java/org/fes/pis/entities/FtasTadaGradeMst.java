/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_TADA_GRADE_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasTadaGradeMst.findAll", query = "SELECT f FROM FtasTadaGradeMst f"),
    @NamedQuery(name = "FtasTadaGradeMst.findByGmSrgKey", query = "SELECT f FROM FtasTadaGradeMst f WHERE f.gmSrgKey = :gmSrgKey"),
    @NamedQuery(name = "FtasTadaGradeMst.findByGmDesc", query = "SELECT f FROM FtasTadaGradeMst f WHERE f.gmDesc = :gmDesc"),
    @NamedQuery(name = "FtasTadaGradeMst.findByStartDate", query = "SELECT f FROM FtasTadaGradeMst f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FtasTadaGradeMst.findByEndDate", query = "SELECT f FROM FtasTadaGradeMst f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FtasTadaGradeMst.findByCreatedt", query = "SELECT f FROM FtasTadaGradeMst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasTadaGradeMst.findByUpdatedt", query = "SELECT f FROM FtasTadaGradeMst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasTadaGradeMst.findBySystemUserid", query = "SELECT f FROM FtasTadaGradeMst f WHERE f.systemUserid = :systemUserid")})
public class FtasTadaGradeMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "GM_SRG_KEY")
    private Long gmSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "GM_DESC")
    private String gmDesc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gmSrgKey")
    private Collection<FtasTadaGradeCityLink> ftasTadaGradeCityLinkCollection;

    public FtasTadaGradeMst() {
    }

    public FtasTadaGradeMst(Long gmSrgKey) {
        this.gmSrgKey = gmSrgKey;
    }

    public FtasTadaGradeMst(Long gmSrgKey, String gmDesc, Date startDate, Date createdt) {
        this.gmSrgKey = gmSrgKey;
        this.gmDesc = gmDesc;
        this.startDate = startDate;
        this.createdt = createdt;
    }

    public Long getGmSrgKey() {
        return gmSrgKey;
    }

    public void setGmSrgKey(Long gmSrgKey) {
        this.gmSrgKey = gmSrgKey;
    }

    public String getGmDesc() {
        return gmDesc;
    }

    public void setGmDesc(String gmDesc) {
        this.gmDesc = gmDesc;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @XmlTransient
    public Collection<FtasTadaGradeCityLink> getFtasTadaGradeCityLinkCollection() {
        return ftasTadaGradeCityLinkCollection;
    }

    public void setFtasTadaGradeCityLinkCollection(Collection<FtasTadaGradeCityLink> ftasTadaGradeCityLinkCollection) {
        this.ftasTadaGradeCityLinkCollection = ftasTadaGradeCityLinkCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gmSrgKey != null ? gmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasTadaGradeMst)) {
            return false;
        }
        FtasTadaGradeMst other = (FtasTadaGradeMst) object;
        if ((this.gmSrgKey == null && other.gmSrgKey != null) || (this.gmSrgKey != null && !this.gmSrgKey.equals(other.gmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasTadaGradeMst[ gmSrgKey=" + gmSrgKey + " ]";
    }
    
}
