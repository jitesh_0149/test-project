/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_TADA_CONV_EXP_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasTadaConvExpDtl.findAll", query = "SELECT f FROM FtasTadaConvExpDtl f"),
    @NamedQuery(name = "FtasTadaConvExpDtl.findByTbmSrgKey", query = "SELECT f FROM FtasTadaConvExpDtl f WHERE f.ftasTadaConvExpDtlPK.tbmSrgKey = :tbmSrgKey"),
    @NamedQuery(name = "FtasTadaConvExpDtl.findBySrno", query = "SELECT f FROM FtasTadaConvExpDtl f WHERE f.ftasTadaConvExpDtlPK.srno = :srno"),
    @NamedQuery(name = "FtasTadaConvExpDtl.findByUserId", query = "SELECT f FROM FtasTadaConvExpDtl f WHERE f.userId = :userId"),
    @NamedQuery(name = "FtasTadaConvExpDtl.findByConvDt", query = "SELECT f FROM FtasTadaConvExpDtl f WHERE f.convDt = :convDt"),
    @NamedQuery(name = "FtasTadaConvExpDtl.findByConvFrom", query = "SELECT f FROM FtasTadaConvExpDtl f WHERE f.convFrom = :convFrom"),
    @NamedQuery(name = "FtasTadaConvExpDtl.findByConvTo", query = "SELECT f FROM FtasTadaConvExpDtl f WHERE f.convTo = :convTo"),
    @NamedQuery(name = "FtasTadaConvExpDtl.findByConvAmt", query = "SELECT f FROM FtasTadaConvExpDtl f WHERE f.convAmt = :convAmt"),
    @NamedQuery(name = "FtasTadaConvExpDtl.findByConvKm", query = "SELECT f FROM FtasTadaConvExpDtl f WHERE f.convKm = :convKm"),
    @NamedQuery(name = "FtasTadaConvExpDtl.findByRemarks", query = "SELECT f FROM FtasTadaConvExpDtl f WHERE f.remarks = :remarks"),
    @NamedQuery(name = "FtasTadaConvExpDtl.findByCreatedt", query = "SELECT f FROM FtasTadaConvExpDtl f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasTadaConvExpDtl.findByUpdatedt", query = "SELECT f FROM FtasTadaConvExpDtl f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasTadaConvExpDtl.findBySystemUserid", query = "SELECT f FROM FtasTadaConvExpDtl f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FtasTadaConvExpDtl.findByPurpose", query = "SELECT f FROM FtasTadaConvExpDtl f WHERE f.purpose = :purpose")})
public class FtasTadaConvExpDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FtasTadaConvExpDtlPK ftasTadaConvExpDtlPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_ID")
    private int userId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONV_DT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date convDt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CONV_FROM")
    private String convFrom;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "CONV_TO")
    private String convTo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "CONV_AMT")
    private BigDecimal convAmt;
    @Column(name = "CONV_KM")
    private BigDecimal convKm;
    @Size(max = 100)
    @Column(name = "REMARKS")
    private String remarks;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 2000)
    @Column(name = "PURPOSE")
    private String purpose;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CM_SRG_KEY", referencedColumnName = "CM_SRG_KEY")
    @ManyToOne(optional = false)
    private FtasTadaConvMst cmSrgKey;
    @JoinColumn(name = "TBM_SRG_KEY", referencedColumnName = "TBM_SRG_KEY", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private FtasTadaBillMst ftasTadaBillMst;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;

    public FtasTadaConvExpDtl() {
    }

    public FtasTadaConvExpDtl(FtasTadaConvExpDtlPK ftasTadaConvExpDtlPK) {
        this.ftasTadaConvExpDtlPK = ftasTadaConvExpDtlPK;
    }

    public FtasTadaConvExpDtl(FtasTadaConvExpDtlPK ftasTadaConvExpDtlPK, int userId, Date convDt, String convFrom, String convTo, BigDecimal convAmt, Date createdt) {
        this.ftasTadaConvExpDtlPK = ftasTadaConvExpDtlPK;
        this.userId = userId;
        this.convDt = convDt;
        this.convFrom = convFrom;
        this.convTo = convTo;
        this.convAmt = convAmt;
        this.createdt = createdt;
    }

    public FtasTadaConvExpDtl(BigDecimal tbmSrgKey, int srno) {
        this.ftasTadaConvExpDtlPK = new FtasTadaConvExpDtlPK(tbmSrgKey, srno);
    }

    public FtasTadaConvExpDtlPK getFtasTadaConvExpDtlPK() {
        return ftasTadaConvExpDtlPK;
    }

    public void setFtasTadaConvExpDtlPK(FtasTadaConvExpDtlPK ftasTadaConvExpDtlPK) {
        this.ftasTadaConvExpDtlPK = ftasTadaConvExpDtlPK;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getConvDt() {
        return convDt;
    }

    public void setConvDt(Date convDt) {
        this.convDt = convDt;
    }

    public String getConvFrom() {
        return convFrom;
    }

    public void setConvFrom(String convFrom) {
        this.convFrom = convFrom;
    }

    public String getConvTo() {
        return convTo;
    }

    public void setConvTo(String convTo) {
        this.convTo = convTo;
    }

    public BigDecimal getConvAmt() {
        return convAmt;
    }

    public void setConvAmt(BigDecimal convAmt) {
        this.convAmt = convAmt;
    }

    public BigDecimal getConvKm() {
        return convKm;
    }

    public void setConvKm(BigDecimal convKm) {
        this.convKm = convKm;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasTadaConvMst getCmSrgKey() {
        return cmSrgKey;
    }

    public void setCmSrgKey(FtasTadaConvMst cmSrgKey) {
        this.cmSrgKey = cmSrgKey;
    }

    public FtasTadaBillMst getFtasTadaBillMst() {
        return ftasTadaBillMst;
    }

    public void setFtasTadaBillMst(FtasTadaBillMst ftasTadaBillMst) {
        this.ftasTadaBillMst = ftasTadaBillMst;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ftasTadaConvExpDtlPK != null ? ftasTadaConvExpDtlPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasTadaConvExpDtl)) {
            return false;
        }
        FtasTadaConvExpDtl other = (FtasTadaConvExpDtl) object;
        if ((this.ftasTadaConvExpDtlPK == null && other.ftasTadaConvExpDtlPK != null) || (this.ftasTadaConvExpDtlPK != null && !this.ftasTadaConvExpDtlPK.equals(other.ftasTadaConvExpDtlPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasTadaConvExpDtl[ ftasTadaConvExpDtlPK=" + ftasTadaConvExpDtlPK + " ]";
    }
    
}
