/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_ED_REP_DTL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdEdRepDtl.findAll", query = "SELECT f FROM FhrdEdRepDtl f"),
    @NamedQuery(name = "FhrdEdRepDtl.findByErdSrgKey", query = "SELECT f FROM FhrdEdRepDtl f WHERE f.erdSrgKey = :erdSrgKey"),
    @NamedQuery(name = "FhrdEdRepDtl.findByErdEdmSrgKey1", query = "SELECT f FROM FhrdEdRepDtl f WHERE f.erdEdmSrgKey1 = :erdEdmSrgKey1"),
    @NamedQuery(name = "FhrdEdRepDtl.findByErdEdmSrgKey2", query = "SELECT f FROM FhrdEdRepDtl f WHERE f.erdEdmSrgKey2 = :erdEdmSrgKey2"),
    @NamedQuery(name = "FhrdEdRepDtl.findByErdEdmSrgKey3", query = "SELECT f FROM FhrdEdRepDtl f WHERE f.erdEdmSrgKey3 = :erdEdmSrgKey3"),
    @NamedQuery(name = "FhrdEdRepDtl.findByErdEdmSrgKey4", query = "SELECT f FROM FhrdEdRepDtl f WHERE f.erdEdmSrgKey4 = :erdEdmSrgKey4"),
    @NamedQuery(name = "FhrdEdRepDtl.findByErdEdmSrgKey5", query = "SELECT f FROM FhrdEdRepDtl f WHERE f.erdEdmSrgKey5 = :erdEdmSrgKey5"),
    @NamedQuery(name = "FhrdEdRepDtl.findByErdEdmSrgKey6", query = "SELECT f FROM FhrdEdRepDtl f WHERE f.erdEdmSrgKey6 = :erdEdmSrgKey6"),
    @NamedQuery(name = "FhrdEdRepDtl.findByErdEdmSrgKey7", query = "SELECT f FROM FhrdEdRepDtl f WHERE f.erdEdmSrgKey7 = :erdEdmSrgKey7"),
    @NamedQuery(name = "FhrdEdRepDtl.findByErdEdmSrgKey8", query = "SELECT f FROM FhrdEdRepDtl f WHERE f.erdEdmSrgKey8 = :erdEdmSrgKey8"),
    @NamedQuery(name = "FhrdEdRepDtl.findByErdEdmSrgKey9", query = "SELECT f FROM FhrdEdRepDtl f WHERE f.erdEdmSrgKey9 = :erdEdmSrgKey9"),
    @NamedQuery(name = "FhrdEdRepDtl.findByErdEdmSrgKey10", query = "SELECT f FROM FhrdEdRepDtl f WHERE f.erdEdmSrgKey10 = :erdEdmSrgKey10"),
    @NamedQuery(name = "FhrdEdRepDtl.findByErdEdmOrder", query = "SELECT f FROM FhrdEdRepDtl f WHERE f.erdEdmOrder = :erdEdmOrder"),
    @NamedQuery(name = "FhrdEdRepDtl.findByCreatedt", query = "SELECT f FROM FhrdEdRepDtl f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdEdRepDtl.findByUpdatedt", query = "SELECT f FROM FhrdEdRepDtl f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdEdRepDtl.findByExportFlag", query = "SELECT f FROM FhrdEdRepDtl f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdEdRepDtl.findByImportFlag", query = "SELECT f FROM FhrdEdRepDtl f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdEdRepDtl.findBySystemUserid", query = "SELECT f FROM FhrdEdRepDtl f WHERE f.systemUserid = :systemUserid")})
public class FhrdEdRepDtl implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ERD_SRG_KEY")
    private BigDecimal erdSrgKey;
    @Column(name = "ERD_EDM_SRG_KEY_1")
    private BigDecimal erdEdmSrgKey1;
    @Column(name = "ERD_EDM_SRG_KEY_2")
    private BigDecimal erdEdmSrgKey2;
    @Column(name = "ERD_EDM_SRG_KEY_3")
    private BigDecimal erdEdmSrgKey3;
    @Column(name = "ERD_EDM_SRG_KEY_4")
    private BigDecimal erdEdmSrgKey4;
    @Column(name = "ERD_EDM_SRG_KEY_5")
    private BigDecimal erdEdmSrgKey5;
    @Column(name = "ERD_EDM_SRG_KEY_6")
    private BigDecimal erdEdmSrgKey6;
    @Column(name = "ERD_EDM_SRG_KEY_7")
    private BigDecimal erdEdmSrgKey7;
    @Column(name = "ERD_EDM_SRG_KEY_8")
    private BigDecimal erdEdmSrgKey8;
    @Column(name = "ERD_EDM_SRG_KEY_9")
    private BigDecimal erdEdmSrgKey9;
    @Column(name = "ERD_EDM_SRG_KEY_10")
    private BigDecimal erdEdmSrgKey10;
    @Column(name = "ERD_EDM_ORDER")
    private Integer erdEdmOrder;
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "ERM_SRG_KEY", referencedColumnName = "ERM_SRG_KEY")
    @ManyToOne
    private FhrdEdRepMst ermSrgKey;

    public FhrdEdRepDtl() {
    }

    public FhrdEdRepDtl(BigDecimal erdSrgKey) {
        this.erdSrgKey = erdSrgKey;
    }

    public BigDecimal getErdSrgKey() {
        return erdSrgKey;
    }

    public void setErdSrgKey(BigDecimal erdSrgKey) {
        this.erdSrgKey = erdSrgKey;
    }

    public BigDecimal getErdEdmSrgKey1() {
        return erdEdmSrgKey1;
    }

    public void setErdEdmSrgKey1(BigDecimal erdEdmSrgKey1) {
        this.erdEdmSrgKey1 = erdEdmSrgKey1;
    }

    public BigDecimal getErdEdmSrgKey2() {
        return erdEdmSrgKey2;
    }

    public void setErdEdmSrgKey2(BigDecimal erdEdmSrgKey2) {
        this.erdEdmSrgKey2 = erdEdmSrgKey2;
    }

    public BigDecimal getErdEdmSrgKey3() {
        return erdEdmSrgKey3;
    }

    public void setErdEdmSrgKey3(BigDecimal erdEdmSrgKey3) {
        this.erdEdmSrgKey3 = erdEdmSrgKey3;
    }

    public BigDecimal getErdEdmSrgKey4() {
        return erdEdmSrgKey4;
    }

    public void setErdEdmSrgKey4(BigDecimal erdEdmSrgKey4) {
        this.erdEdmSrgKey4 = erdEdmSrgKey4;
    }

    public BigDecimal getErdEdmSrgKey5() {
        return erdEdmSrgKey5;
    }

    public void setErdEdmSrgKey5(BigDecimal erdEdmSrgKey5) {
        this.erdEdmSrgKey5 = erdEdmSrgKey5;
    }

    public BigDecimal getErdEdmSrgKey6() {
        return erdEdmSrgKey6;
    }

    public void setErdEdmSrgKey6(BigDecimal erdEdmSrgKey6) {
        this.erdEdmSrgKey6 = erdEdmSrgKey6;
    }

    public BigDecimal getErdEdmSrgKey7() {
        return erdEdmSrgKey7;
    }

    public void setErdEdmSrgKey7(BigDecimal erdEdmSrgKey7) {
        this.erdEdmSrgKey7 = erdEdmSrgKey7;
    }

    public BigDecimal getErdEdmSrgKey8() {
        return erdEdmSrgKey8;
    }

    public void setErdEdmSrgKey8(BigDecimal erdEdmSrgKey8) {
        this.erdEdmSrgKey8 = erdEdmSrgKey8;
    }

    public BigDecimal getErdEdmSrgKey9() {
        return erdEdmSrgKey9;
    }

    public void setErdEdmSrgKey9(BigDecimal erdEdmSrgKey9) {
        this.erdEdmSrgKey9 = erdEdmSrgKey9;
    }

    public BigDecimal getErdEdmSrgKey10() {
        return erdEdmSrgKey10;
    }

    public void setErdEdmSrgKey10(BigDecimal erdEdmSrgKey10) {
        this.erdEdmSrgKey10 = erdEdmSrgKey10;
    }

    public Integer getErdEdmOrder() {
        return erdEdmOrder;
    }

    public void setErdEdmOrder(Integer erdEdmOrder) {
        this.erdEdmOrder = erdEdmOrder;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEdRepMst getErmSrgKey() {
        return ermSrgKey;
    }

    public void setErmSrgKey(FhrdEdRepMst ermSrgKey) {
        this.ermSrgKey = ermSrgKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (erdSrgKey != null ? erdSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdEdRepDtl)) {
            return false;
        }
        FhrdEdRepDtl other = (FhrdEdRepDtl) object;
        if ((this.erdSrgKey == null && other.erdSrgKey != null) || (this.erdSrgKey != null && !this.erdSrgKey.equals(other.erdSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdEdRepDtl[ erdSrgKey=" + erdSrgKey + " ]";
    }
    
}
