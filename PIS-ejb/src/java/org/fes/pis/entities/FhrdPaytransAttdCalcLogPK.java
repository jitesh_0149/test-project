/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author anu
 */
@Embeddable
public class FhrdPaytransAttdCalcLogPK implements Serializable {
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATTD_SRG_KEY")
    private BigDecimal attdSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ATTD_USER_ID")
    private int attdUserId;

    public FhrdPaytransAttdCalcLogPK() {
    }

    public FhrdPaytransAttdCalcLogPK(BigDecimal attdSrgKey, int attdUserId) {
        this.attdSrgKey = attdSrgKey;
        this.attdUserId = attdUserId;
    }

    public BigDecimal getAttdSrgKey() {
        return attdSrgKey;
    }

    public void setAttdSrgKey(BigDecimal attdSrgKey) {
        this.attdSrgKey = attdSrgKey;
    }

    public int getAttdUserId() {
        return attdUserId;
    }

    public void setAttdUserId(int attdUserId) {
        this.attdUserId = attdUserId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (attdSrgKey != null ? attdSrgKey.hashCode() : 0);
        hash += (int) attdUserId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdPaytransAttdCalcLogPK)) {
            return false;
        }
        FhrdPaytransAttdCalcLogPK other = (FhrdPaytransAttdCalcLogPK) object;
        if ((this.attdSrgKey == null && other.attdSrgKey != null) || (this.attdSrgKey != null && !this.attdSrgKey.equals(other.attdSrgKey))) {
            return false;
        }
        if (this.attdUserId != other.attdUserId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdPaytransAttdCalcLogPK[ attdSrgKey=" + attdSrgKey + ", attdUserId=" + attdUserId + " ]";
    }
    
}
