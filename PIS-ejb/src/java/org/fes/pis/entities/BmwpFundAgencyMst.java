/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "BMWP_FUND_AGENCY_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BmwpFundAgencyMst.findAll", query = "SELECT b FROM BmwpFundAgencyMst b"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamUniqueSrno", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famUniqueSrno = :famUniqueSrno"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamSrno", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famSrno = :famSrno"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamAgencyShortName", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famAgencyShortName = :famAgencyShortName"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamAgencyName", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famAgencyName = :famAgencyName"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamMouFundAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famMouFundAmt = :famMouFundAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamReceivedFundAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famReceivedFundAmt = :famReceivedFundAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamToReceivedFundAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famToReceivedFundAmt = :famToReceivedFundAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamSurrenderFundAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famSurrenderFundAmt = :famSurrenderFundAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamTotalFundAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famTotalFundAmt = :famTotalFundAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamWorkplanAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famWorkplanAmt = :famWorkplanAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamWpSurrenderAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famWpSurrenderAmt = :famWpSurrenderAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamReAllotWpAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famReAllotWpAmt = :famReAllotWpAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamOfferWpAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famOfferWpAmt = :famOfferWpAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamReviseWpAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famReviseWpAmt = :famReviseWpAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamCommitedAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famCommitedAmt = :famCommitedAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamPipelinePayAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famPipelinePayAmt = :famPipelinePayAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamAdvanceCommitAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famAdvanceCommitAmt = :famAdvanceCommitAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamAdvancePayAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famAdvancePayAmt = :famAdvancePayAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamPipeAdvadjCommitAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famPipeAdvadjCommitAmt = :famPipeAdvadjCommitAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamPipeAdvadjPayAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famPipeAdvadjPayAmt = :famPipeAdvadjPayAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamReceiptPayAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famReceiptPayAmt = :famReceiptPayAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamReceiptCommitAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famReceiptCommitAmt = :famReceiptCommitAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamExpenseAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famExpenseAmt = :famExpenseAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamUsableBalanceAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famUsableBalanceAmt = :famUsableBalanceAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamBalanceAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famBalanceAmt = :famBalanceAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByCreatedt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.createdt = :createdt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByUpdatedt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.updatedt = :updatedt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findBySystemUserid", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.systemUserid = :systemUserid"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByExportFlag", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.exportFlag = :exportFlag"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByImportFlag", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.importFlag = :importFlag"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamPipeCommitedAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famPipeCommitedAmt = :famPipeCommitedAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamStartDate", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famStartDate = :famStartDate"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamEndDate", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famEndDate = :famEndDate"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamEndDatePossible", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famEndDatePossible = :famEndDatePossible"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamTransactionDate", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famTransactionDate = :famTransactionDate"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamProCrExpAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famProCrExpAmt = :famProCrExpAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamReceivedAdvanceAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famReceivedAdvanceAmt = :famReceivedAdvanceAmt"),
    @NamedQuery(name = "BmwpFundAgencyMst.findByFamCarryFwdAdvAmt", query = "SELECT b FROM BmwpFundAgencyMst b WHERE b.famCarryFwdAdvAmt = :famCarryFwdAdvAmt")})
public class BmwpFundAgencyMst implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_UNIQUE_SRNO")
    private BigDecimal famUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_SRNO")
    private int famSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "FAM_AGENCY_SHORT_NAME")
    private String famAgencyShortName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "FAM_AGENCY_NAME")
    private String famAgencyName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_MOU_FUND_AMT")
    private BigDecimal famMouFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_RECEIVED_FUND_AMT")
    private BigDecimal famReceivedFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_TO_RECEIVED_FUND_AMT")
    private BigDecimal famToReceivedFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_SURRENDER_FUND_AMT")
    private BigDecimal famSurrenderFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_TOTAL_FUND_AMT")
    private BigDecimal famTotalFundAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_WORKPLAN_AMT")
    private BigDecimal famWorkplanAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_WP_SURRENDER_AMT")
    private BigDecimal famWpSurrenderAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_RE_ALLOT_WP_AMT")
    private BigDecimal famReAllotWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_OFFER_WP_AMT")
    private BigDecimal famOfferWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_REVISE_WP_AMT")
    private BigDecimal famReviseWpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_COMMITED_AMT")
    private BigDecimal famCommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_PIPELINE_PAY_AMT")
    private BigDecimal famPipelinePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_ADVANCE_COMMIT_AMT")
    private BigDecimal famAdvanceCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_ADVANCE_PAY_AMT")
    private BigDecimal famAdvancePayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_PIPE_ADVADJ_COMMIT_AMT")
    private BigDecimal famPipeAdvadjCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_PIPE_ADVADJ_PAY_AMT")
    private BigDecimal famPipeAdvadjPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_RECEIPT_PAY_AMT")
    private BigDecimal famReceiptPayAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_RECEIPT_COMMIT_AMT")
    private BigDecimal famReceiptCommitAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_EXPENSE_AMT")
    private BigDecimal famExpenseAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_USABLE_BALANCE_AMT")
    private BigDecimal famUsableBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_BALANCE_AMT")
    private BigDecimal famBalanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_PIPE_COMMITED_AMT")
    private BigDecimal famPipeCommitedAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date famStartDate;
    @Column(name = "FAM_END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date famEndDate;
    @Column(name = "FAM_END_DATE_POSSIBLE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date famEndDatePossible;
    @Column(name = "FAM_TRANSACTION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date famTransactionDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_PRO_CR_EXP_AMT")
    private BigDecimal famProCrExpAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_RECEIVED_ADVANCE_AMT")
    private BigDecimal famReceivedAdvanceAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FAM_CARRY_FWD_ADV_AMT")
    private BigDecimal famCarryFwdAdvAmt;
    @OneToMany(mappedBy = "apablFamUniqueSrno")
    private Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "aplFamUniqueSrno")
    private Collection<BmwpAgencyProjectLink> bmwpAgencyProjectLinkCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "FAM_ADDRNO", referencedColumnName = "AM_UNIQUE_SRNO")
    @OneToOne(optional = false)
    private FcadAddrMst famAddrno;
    @JoinColumn(name = "FAM_ASM_UNIQUE_SRNO", referencedColumnName = "ASM_UNIQUE_SRNO")
    @ManyToOne
    private BmwpAgenSecMst famAsmUniqueSrno;
    @JoinColumn(name = "FAM_ATM_UNIQUE_SRNO", referencedColumnName = "ATM_UNIQUE_SRNO")
    @ManyToOne(optional = false)
    private BmwpAgencyTypeMst famAtmUniqueSrno;
    @OneToMany(mappedBy = "apabdFamUniqueSrno")
    private Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection;
    @OneToMany(mappedBy = "apamFamUniqueSrno")
    private Collection<BmwpApActivityMst> bmwpApActivityMstCollection;
    @OneToMany(mappedBy = "apbmFamUniqueSrno")
    private Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection;
    @OneToMany(mappedBy = "wpFamUniqueSrno")
    private Collection<BmwpWorkplan> bmwpWorkplanCollection;

    public BmwpFundAgencyMst() {
    }

    public BmwpFundAgencyMst(BigDecimal famUniqueSrno) {
        this.famUniqueSrno = famUniqueSrno;
    }

    public BmwpFundAgencyMst(BigDecimal famUniqueSrno, int famSrno, String famAgencyShortName, String famAgencyName, BigDecimal famMouFundAmt, BigDecimal famReceivedFundAmt, BigDecimal famToReceivedFundAmt, BigDecimal famSurrenderFundAmt, BigDecimal famTotalFundAmt, BigDecimal famWorkplanAmt, BigDecimal famWpSurrenderAmt, BigDecimal famReAllotWpAmt, BigDecimal famOfferWpAmt, BigDecimal famReviseWpAmt, BigDecimal famCommitedAmt, BigDecimal famPipelinePayAmt, BigDecimal famAdvanceCommitAmt, BigDecimal famAdvancePayAmt, BigDecimal famPipeAdvadjCommitAmt, BigDecimal famPipeAdvadjPayAmt, BigDecimal famReceiptPayAmt, BigDecimal famReceiptCommitAmt, BigDecimal famExpenseAmt, BigDecimal famUsableBalanceAmt, BigDecimal famBalanceAmt, Date createdt, BigDecimal famPipeCommitedAmt, Date famStartDate, BigDecimal famProCrExpAmt, BigDecimal famReceivedAdvanceAmt, BigDecimal famCarryFwdAdvAmt) {
        this.famUniqueSrno = famUniqueSrno;
        this.famSrno = famSrno;
        this.famAgencyShortName = famAgencyShortName;
        this.famAgencyName = famAgencyName;
        this.famMouFundAmt = famMouFundAmt;
        this.famReceivedFundAmt = famReceivedFundAmt;
        this.famToReceivedFundAmt = famToReceivedFundAmt;
        this.famSurrenderFundAmt = famSurrenderFundAmt;
        this.famTotalFundAmt = famTotalFundAmt;
        this.famWorkplanAmt = famWorkplanAmt;
        this.famWpSurrenderAmt = famWpSurrenderAmt;
        this.famReAllotWpAmt = famReAllotWpAmt;
        this.famOfferWpAmt = famOfferWpAmt;
        this.famReviseWpAmt = famReviseWpAmt;
        this.famCommitedAmt = famCommitedAmt;
        this.famPipelinePayAmt = famPipelinePayAmt;
        this.famAdvanceCommitAmt = famAdvanceCommitAmt;
        this.famAdvancePayAmt = famAdvancePayAmt;
        this.famPipeAdvadjCommitAmt = famPipeAdvadjCommitAmt;
        this.famPipeAdvadjPayAmt = famPipeAdvadjPayAmt;
        this.famReceiptPayAmt = famReceiptPayAmt;
        this.famReceiptCommitAmt = famReceiptCommitAmt;
        this.famExpenseAmt = famExpenseAmt;
        this.famUsableBalanceAmt = famUsableBalanceAmt;
        this.famBalanceAmt = famBalanceAmt;
        this.createdt = createdt;
        this.famPipeCommitedAmt = famPipeCommitedAmt;
        this.famStartDate = famStartDate;
        this.famProCrExpAmt = famProCrExpAmt;
        this.famReceivedAdvanceAmt = famReceivedAdvanceAmt;
        this.famCarryFwdAdvAmt = famCarryFwdAdvAmt;
    }

    public BigDecimal getFamUniqueSrno() {
        return famUniqueSrno;
    }

    public void setFamUniqueSrno(BigDecimal famUniqueSrno) {
        this.famUniqueSrno = famUniqueSrno;
    }

    public int getFamSrno() {
        return famSrno;
    }

    public void setFamSrno(int famSrno) {
        this.famSrno = famSrno;
    }

    public String getFamAgencyShortName() {
        return famAgencyShortName;
    }

    public void setFamAgencyShortName(String famAgencyShortName) {
        this.famAgencyShortName = famAgencyShortName;
    }

    public String getFamAgencyName() {
        return famAgencyName;
    }

    public void setFamAgencyName(String famAgencyName) {
        this.famAgencyName = famAgencyName;
    }

    public BigDecimal getFamMouFundAmt() {
        return famMouFundAmt;
    }

    public void setFamMouFundAmt(BigDecimal famMouFundAmt) {
        this.famMouFundAmt = famMouFundAmt;
    }

    public BigDecimal getFamReceivedFundAmt() {
        return famReceivedFundAmt;
    }

    public void setFamReceivedFundAmt(BigDecimal famReceivedFundAmt) {
        this.famReceivedFundAmt = famReceivedFundAmt;
    }

    public BigDecimal getFamToReceivedFundAmt() {
        return famToReceivedFundAmt;
    }

    public void setFamToReceivedFundAmt(BigDecimal famToReceivedFundAmt) {
        this.famToReceivedFundAmt = famToReceivedFundAmt;
    }

    public BigDecimal getFamSurrenderFundAmt() {
        return famSurrenderFundAmt;
    }

    public void setFamSurrenderFundAmt(BigDecimal famSurrenderFundAmt) {
        this.famSurrenderFundAmt = famSurrenderFundAmt;
    }

    public BigDecimal getFamTotalFundAmt() {
        return famTotalFundAmt;
    }

    public void setFamTotalFundAmt(BigDecimal famTotalFundAmt) {
        this.famTotalFundAmt = famTotalFundAmt;
    }

    public BigDecimal getFamWorkplanAmt() {
        return famWorkplanAmt;
    }

    public void setFamWorkplanAmt(BigDecimal famWorkplanAmt) {
        this.famWorkplanAmt = famWorkplanAmt;
    }

    public BigDecimal getFamWpSurrenderAmt() {
        return famWpSurrenderAmt;
    }

    public void setFamWpSurrenderAmt(BigDecimal famWpSurrenderAmt) {
        this.famWpSurrenderAmt = famWpSurrenderAmt;
    }

    public BigDecimal getFamReAllotWpAmt() {
        return famReAllotWpAmt;
    }

    public void setFamReAllotWpAmt(BigDecimal famReAllotWpAmt) {
        this.famReAllotWpAmt = famReAllotWpAmt;
    }

    public BigDecimal getFamOfferWpAmt() {
        return famOfferWpAmt;
    }

    public void setFamOfferWpAmt(BigDecimal famOfferWpAmt) {
        this.famOfferWpAmt = famOfferWpAmt;
    }

    public BigDecimal getFamReviseWpAmt() {
        return famReviseWpAmt;
    }

    public void setFamReviseWpAmt(BigDecimal famReviseWpAmt) {
        this.famReviseWpAmt = famReviseWpAmt;
    }

    public BigDecimal getFamCommitedAmt() {
        return famCommitedAmt;
    }

    public void setFamCommitedAmt(BigDecimal famCommitedAmt) {
        this.famCommitedAmt = famCommitedAmt;
    }

    public BigDecimal getFamPipelinePayAmt() {
        return famPipelinePayAmt;
    }

    public void setFamPipelinePayAmt(BigDecimal famPipelinePayAmt) {
        this.famPipelinePayAmt = famPipelinePayAmt;
    }

    public BigDecimal getFamAdvanceCommitAmt() {
        return famAdvanceCommitAmt;
    }

    public void setFamAdvanceCommitAmt(BigDecimal famAdvanceCommitAmt) {
        this.famAdvanceCommitAmt = famAdvanceCommitAmt;
    }

    public BigDecimal getFamAdvancePayAmt() {
        return famAdvancePayAmt;
    }

    public void setFamAdvancePayAmt(BigDecimal famAdvancePayAmt) {
        this.famAdvancePayAmt = famAdvancePayAmt;
    }

    public BigDecimal getFamPipeAdvadjCommitAmt() {
        return famPipeAdvadjCommitAmt;
    }

    public void setFamPipeAdvadjCommitAmt(BigDecimal famPipeAdvadjCommitAmt) {
        this.famPipeAdvadjCommitAmt = famPipeAdvadjCommitAmt;
    }

    public BigDecimal getFamPipeAdvadjPayAmt() {
        return famPipeAdvadjPayAmt;
    }

    public void setFamPipeAdvadjPayAmt(BigDecimal famPipeAdvadjPayAmt) {
        this.famPipeAdvadjPayAmt = famPipeAdvadjPayAmt;
    }

    public BigDecimal getFamReceiptPayAmt() {
        return famReceiptPayAmt;
    }

    public void setFamReceiptPayAmt(BigDecimal famReceiptPayAmt) {
        this.famReceiptPayAmt = famReceiptPayAmt;
    }

    public BigDecimal getFamReceiptCommitAmt() {
        return famReceiptCommitAmt;
    }

    public void setFamReceiptCommitAmt(BigDecimal famReceiptCommitAmt) {
        this.famReceiptCommitAmt = famReceiptCommitAmt;
    }

    public BigDecimal getFamExpenseAmt() {
        return famExpenseAmt;
    }

    public void setFamExpenseAmt(BigDecimal famExpenseAmt) {
        this.famExpenseAmt = famExpenseAmt;
    }

    public BigDecimal getFamUsableBalanceAmt() {
        return famUsableBalanceAmt;
    }

    public void setFamUsableBalanceAmt(BigDecimal famUsableBalanceAmt) {
        this.famUsableBalanceAmt = famUsableBalanceAmt;
    }

    public BigDecimal getFamBalanceAmt() {
        return famBalanceAmt;
    }

    public void setFamBalanceAmt(BigDecimal famBalanceAmt) {
        this.famBalanceAmt = famBalanceAmt;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public BigDecimal getFamPipeCommitedAmt() {
        return famPipeCommitedAmt;
    }

    public void setFamPipeCommitedAmt(BigDecimal famPipeCommitedAmt) {
        this.famPipeCommitedAmt = famPipeCommitedAmt;
    }

    public Date getFamStartDate() {
        return famStartDate;
    }

    public void setFamStartDate(Date famStartDate) {
        this.famStartDate = famStartDate;
    }

    public Date getFamEndDate() {
        return famEndDate;
    }

    public void setFamEndDate(Date famEndDate) {
        this.famEndDate = famEndDate;
    }

    public Date getFamEndDatePossible() {
        return famEndDatePossible;
    }

    public void setFamEndDatePossible(Date famEndDatePossible) {
        this.famEndDatePossible = famEndDatePossible;
    }

    public Date getFamTransactionDate() {
        return famTransactionDate;
    }

    public void setFamTransactionDate(Date famTransactionDate) {
        this.famTransactionDate = famTransactionDate;
    }

    public BigDecimal getFamProCrExpAmt() {
        return famProCrExpAmt;
    }

    public void setFamProCrExpAmt(BigDecimal famProCrExpAmt) {
        this.famProCrExpAmt = famProCrExpAmt;
    }

    public BigDecimal getFamReceivedAdvanceAmt() {
        return famReceivedAdvanceAmt;
    }

    public void setFamReceivedAdvanceAmt(BigDecimal famReceivedAdvanceAmt) {
        this.famReceivedAdvanceAmt = famReceivedAdvanceAmt;
    }

    public BigDecimal getFamCarryFwdAdvAmt() {
        return famCarryFwdAdvAmt;
    }

    public void setFamCarryFwdAdvAmt(BigDecimal famCarryFwdAdvAmt) {
        this.famCarryFwdAdvAmt = famCarryFwdAdvAmt;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetLink> getBmwpApActivityBudgetLinkCollection() {
        return bmwpApActivityBudgetLinkCollection;
    }

    public void setBmwpApActivityBudgetLinkCollection(Collection<BmwpApActivityBudgetLink> bmwpApActivityBudgetLinkCollection) {
        this.bmwpApActivityBudgetLinkCollection = bmwpApActivityBudgetLinkCollection;
    }

    @XmlTransient
    public Collection<BmwpAgencyProjectLink> getBmwpAgencyProjectLinkCollection() {
        return bmwpAgencyProjectLinkCollection;
    }

    public void setBmwpAgencyProjectLinkCollection(Collection<BmwpAgencyProjectLink> bmwpAgencyProjectLinkCollection) {
        this.bmwpAgencyProjectLinkCollection = bmwpAgencyProjectLinkCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FcadAddrMst getFamAddrno() {
        return famAddrno;
    }

    public void setFamAddrno(FcadAddrMst famAddrno) {
        this.famAddrno = famAddrno;
    }

    public BmwpAgenSecMst getFamAsmUniqueSrno() {
        return famAsmUniqueSrno;
    }

    public void setFamAsmUniqueSrno(BmwpAgenSecMst famAsmUniqueSrno) {
        this.famAsmUniqueSrno = famAsmUniqueSrno;
    }

    public BmwpAgencyTypeMst getFamAtmUniqueSrno() {
        return famAtmUniqueSrno;
    }

    public void setFamAtmUniqueSrno(BmwpAgencyTypeMst famAtmUniqueSrno) {
        this.famAtmUniqueSrno = famAtmUniqueSrno;
    }

    @XmlTransient
    public Collection<BmwpApActivityBudgetDtl> getBmwpApActivityBudgetDtlCollection() {
        return bmwpApActivityBudgetDtlCollection;
    }

    public void setBmwpApActivityBudgetDtlCollection(Collection<BmwpApActivityBudgetDtl> bmwpApActivityBudgetDtlCollection) {
        this.bmwpApActivityBudgetDtlCollection = bmwpApActivityBudgetDtlCollection;
    }

    @XmlTransient
    public Collection<BmwpApActivityMst> getBmwpApActivityMstCollection() {
        return bmwpApActivityMstCollection;
    }

    public void setBmwpApActivityMstCollection(Collection<BmwpApActivityMst> bmwpApActivityMstCollection) {
        this.bmwpApActivityMstCollection = bmwpApActivityMstCollection;
    }

    @XmlTransient
    public Collection<BmwpApBudgetMst> getBmwpApBudgetMstCollection() {
        return bmwpApBudgetMstCollection;
    }

    public void setBmwpApBudgetMstCollection(Collection<BmwpApBudgetMst> bmwpApBudgetMstCollection) {
        this.bmwpApBudgetMstCollection = bmwpApBudgetMstCollection;
    }

    @XmlTransient
    public Collection<BmwpWorkplan> getBmwpWorkplanCollection() {
        return bmwpWorkplanCollection;
    }

    public void setBmwpWorkplanCollection(Collection<BmwpWorkplan> bmwpWorkplanCollection) {
        this.bmwpWorkplanCollection = bmwpWorkplanCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (famUniqueSrno != null ? famUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmwpFundAgencyMst)) {
            return false;
        }
        BmwpFundAgencyMst other = (BmwpFundAgencyMst) object;
        if ((this.famUniqueSrno == null && other.famUniqueSrno != null) || (this.famUniqueSrno != null && !this.famUniqueSrno.equals(other.famUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.BmwpFundAgencyMst[ famUniqueSrno=" + famUniqueSrno + " ]";
    }
    
}
