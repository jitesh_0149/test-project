/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_PAYTRANS_MONVAR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdPaytransMonvar.findAll", query = "SELECT f FROM FhrdPaytransMonvar f"),
    @NamedQuery(name = "FhrdPaytransMonvar.findByPtmvSrgKey", query = "SELECT f FROM FhrdPaytransMonvar f WHERE f.ptmvSrgKey = :ptmvSrgKey"),
    @NamedQuery(name = "FhrdPaytransMonvar.findByOrgSalaryFromDate", query = "SELECT f FROM FhrdPaytransMonvar f WHERE f.orgSalaryFromDate = :orgSalaryFromDate"),
    @NamedQuery(name = "FhrdPaytransMonvar.findByOrgSalaryToDate", query = "SELECT f FROM FhrdPaytransMonvar f WHERE f.orgSalaryToDate = :orgSalaryToDate"),
    @NamedQuery(name = "FhrdPaytransMonvar.findByEarndedncd", query = "SELECT f FROM FhrdPaytransMonvar f WHERE f.earndedncd = :earndedncd"),
    @NamedQuery(name = "FhrdPaytransMonvar.findByEarndednFlag", query = "SELECT f FROM FhrdPaytransMonvar f WHERE f.earndednFlag = :earndednFlag"),
    @NamedQuery(name = "FhrdPaytransMonvar.findByPrintInPaySlip", query = "SELECT f FROM FhrdPaytransMonvar f WHERE f.printInPaySlip = :printInPaySlip"),
    @NamedQuery(name = "FhrdPaytransMonvar.findByPrintIf0Flag", query = "SELECT f FROM FhrdPaytransMonvar f WHERE f.printIf0Flag = :printIf0Flag"),
    @NamedQuery(name = "FhrdPaytransMonvar.findByVariableAmt", query = "SELECT f FROM FhrdPaytransMonvar f WHERE f.variableAmt = :variableAmt"),
    @NamedQuery(name = "FhrdPaytransMonvar.findByTranYear", query = "SELECT f FROM FhrdPaytransMonvar f WHERE f.tranYear = :tranYear"),
    @NamedQuery(name = "FhrdPaytransMonvar.findByCreatedt", query = "SELECT f FROM FhrdPaytransMonvar f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdPaytransMonvar.findByUpdatedt", query = "SELECT f FROM FhrdPaytransMonvar f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdPaytransMonvar.findByOumUnitSrno", query = "SELECT f FROM FhrdPaytransMonvar f WHERE f.oumUnitSrno = :oumUnitSrno"),
    @NamedQuery(name = "FhrdPaytransMonvar.findBySystemUserid", query = "SELECT f FROM FhrdPaytransMonvar f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdPaytransMonvar.findByExportFlag", query = "SELECT f FROM FhrdPaytransMonvar f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdPaytransMonvar.findByImportFlag", query = "SELECT f FROM FhrdPaytransMonvar f WHERE f.importFlag = :importFlag")})
public class FhrdPaytransMonvar implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PTMV_SRG_KEY")
    private BigDecimal ptmvSrgKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_SALARY_FROM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgSalaryFromDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ORG_SALARY_TO_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orgSalaryToDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "EARNDEDNCD")
    private String earndedncd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "EARNDEDN_FLAG")
    private String earndednFlag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PRINT_IN_PAY_SLIP")
    private String printInPaySlip;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "PRINT_IF_0_FLAG")
    private String printIf0Flag;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VARIABLE_AMT")
    private BigInteger variableAmt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TRAN_YEAR")
    private short tranYear;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OUM_UNIT_SRNO")
    private int oumUnitSrno;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst userId;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "EED_SRG_KEY", referencedColumnName = "EED_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdEmpearndedn eedSrgKey;

    public FhrdPaytransMonvar() {
    }

    public FhrdPaytransMonvar(BigDecimal ptmvSrgKey) {
        this.ptmvSrgKey = ptmvSrgKey;
    }

    public FhrdPaytransMonvar(BigDecimal ptmvSrgKey, Date orgSalaryFromDate, Date orgSalaryToDate, String earndedncd, String earndednFlag, String printInPaySlip, String printIf0Flag, BigInteger variableAmt, short tranYear, Date createdt, int oumUnitSrno) {
        this.ptmvSrgKey = ptmvSrgKey;
        this.orgSalaryFromDate = orgSalaryFromDate;
        this.orgSalaryToDate = orgSalaryToDate;
        this.earndedncd = earndedncd;
        this.earndednFlag = earndednFlag;
        this.printInPaySlip = printInPaySlip;
        this.printIf0Flag = printIf0Flag;
        this.variableAmt = variableAmt;
        this.tranYear = tranYear;
        this.createdt = createdt;
        this.oumUnitSrno = oumUnitSrno;
    }

    public BigDecimal getPtmvSrgKey() {
        return ptmvSrgKey;
    }

    public void setPtmvSrgKey(BigDecimal ptmvSrgKey) {
        this.ptmvSrgKey = ptmvSrgKey;
    }

    public Date getOrgSalaryFromDate() {
        return orgSalaryFromDate;
    }

    public void setOrgSalaryFromDate(Date orgSalaryFromDate) {
        this.orgSalaryFromDate = orgSalaryFromDate;
    }

    public Date getOrgSalaryToDate() {
        return orgSalaryToDate;
    }

    public void setOrgSalaryToDate(Date orgSalaryToDate) {
        this.orgSalaryToDate = orgSalaryToDate;
    }

    public String getEarndedncd() {
        return earndedncd;
    }

    public void setEarndedncd(String earndedncd) {
        this.earndedncd = earndedncd;
    }

    public String getEarndednFlag() {
        return earndednFlag;
    }

    public void setEarndednFlag(String earndednFlag) {
        this.earndednFlag = earndednFlag;
    }

    public String getPrintInPaySlip() {
        return printInPaySlip;
    }

    public void setPrintInPaySlip(String printInPaySlip) {
        this.printInPaySlip = printInPaySlip;
    }

    public String getPrintIf0Flag() {
        return printIf0Flag;
    }

    public void setPrintIf0Flag(String printIf0Flag) {
        this.printIf0Flag = printIf0Flag;
    }

    public BigInteger getVariableAmt() {
        return variableAmt;
    }

    public void setVariableAmt(BigInteger variableAmt) {
        this.variableAmt = variableAmt;
    }

    public short getTranYear() {
        return tranYear;
    }

    public void setTranYear(short tranYear) {
        this.tranYear = tranYear;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public int getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(int oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getUserId() {
        return userId;
    }

    public void setUserId(FhrdEmpmst userId) {
        this.userId = userId;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpearndedn getEedSrgKey() {
        return eedSrgKey;
    }

    public void setEedSrgKey(FhrdEmpearndedn eedSrgKey) {
        this.eedSrgKey = eedSrgKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ptmvSrgKey != null ? ptmvSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdPaytransMonvar)) {
            return false;
        }
        FhrdPaytransMonvar other = (FhrdPaytransMonvar) object;
        if ((this.ptmvSrgKey == null && other.ptmvSrgKey != null) || (this.ptmvSrgKey != null && !this.ptmvSrgKey.equals(other.ptmvSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdPaytransMonvar[ ptmvSrgKey=" + ptmvSrgKey + " ]";
    }
    
}
