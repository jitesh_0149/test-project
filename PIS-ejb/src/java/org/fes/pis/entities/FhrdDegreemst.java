/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FHRD_DEGREEMST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FhrdDegreemst.findAll", query = "SELECT f FROM FhrdDegreemst f"),
    @NamedQuery(name = "FhrdDegreemst.findByDegreecd", query = "SELECT f FROM FhrdDegreemst f WHERE f.degreecd = :degreecd"),
    @NamedQuery(name = "FhrdDegreemst.findByDegreeName", query = "SELECT f FROM FhrdDegreemst f WHERE f.degreeName = :degreeName"),
    @NamedQuery(name = "FhrdDegreemst.findByCreatedt", query = "SELECT f FROM FhrdDegreemst f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FhrdDegreemst.findByUpdatedt", query = "SELECT f FROM FhrdDegreemst f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FhrdDegreemst.findByDegreeSrno", query = "SELECT f FROM FhrdDegreemst f WHERE f.degreeSrno = :degreeSrno"),
    @NamedQuery(name = "FhrdDegreemst.findByExportFlag", query = "SELECT f FROM FhrdDegreemst f WHERE f.exportFlag = :exportFlag"),
    @NamedQuery(name = "FhrdDegreemst.findByImportFlag", query = "SELECT f FROM FhrdDegreemst f WHERE f.importFlag = :importFlag"),
    @NamedQuery(name = "FhrdDegreemst.findBySystemUserid", query = "SELECT f FROM FhrdDegreemst f WHERE f.systemUserid = :systemUserid"),
    @NamedQuery(name = "FhrdDegreemst.findByDgrmSrgKey", query = "SELECT f FROM FhrdDegreemst f WHERE f.dgrmSrgKey = :dgrmSrgKey")})
public class FhrdDegreemst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "DEGREECD")
    private String degreecd;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "DEGREE_NAME")
    private String degreeName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DEGREE_SRNO")
    private int degreeSrno;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DGRM_SRG_KEY")
    private BigDecimal dgrmSrgKey;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dgrmSrgKey")
    private Collection<FhrdQualification> fhrdQualificationCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fhrdDegreemst")
    private Collection<FhrdDegreedtl> fhrdDegreedtlCollection;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "DLM_SRG_KEY", referencedColumnName = "DLM_SRG_KEY")
    @ManyToOne(optional = false)
    private FhrdDegreeLevelMst dlmSrgKey;

    public FhrdDegreemst() {
    }

    public FhrdDegreemst(BigDecimal dgrmSrgKey) {
        this.dgrmSrgKey = dgrmSrgKey;
    }

    public FhrdDegreemst(BigDecimal dgrmSrgKey, String degreecd, String degreeName, Date createdt, int degreeSrno) {
        this.dgrmSrgKey = dgrmSrgKey;
        this.degreecd = degreecd;
        this.degreeName = degreeName;
        this.createdt = createdt;
        this.degreeSrno = degreeSrno;
    }

    public String getDegreecd() {
        return degreecd;
    }

    public void setDegreecd(String degreecd) {
        this.degreecd = degreecd;
    }

    public String getDegreeName() {
        return degreeName;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public int getDegreeSrno() {
        return degreeSrno;
    }

    public void setDegreeSrno(int degreeSrno) {
        this.degreeSrno = degreeSrno;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public BigDecimal getDgrmSrgKey() {
        return dgrmSrgKey;
    }

    public void setDgrmSrgKey(BigDecimal dgrmSrgKey) {
        this.dgrmSrgKey = dgrmSrgKey;
    }

    @XmlTransient
    public Collection<FhrdQualification> getFhrdQualificationCollection() {
        return fhrdQualificationCollection;
    }

    public void setFhrdQualificationCollection(Collection<FhrdQualification> fhrdQualificationCollection) {
        this.fhrdQualificationCollection = fhrdQualificationCollection;
    }

    @XmlTransient
    public Collection<FhrdDegreedtl> getFhrdDegreedtlCollection() {
        return fhrdDegreedtlCollection;
    }

    public void setFhrdDegreedtlCollection(Collection<FhrdDegreedtl> fhrdDegreedtlCollection) {
        this.fhrdDegreedtlCollection = fhrdDegreedtlCollection;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdDegreeLevelMst getDlmSrgKey() {
        return dlmSrgKey;
    }

    public void setDlmSrgKey(FhrdDegreeLevelMst dlmSrgKey) {
        this.dlmSrgKey = dlmSrgKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dgrmSrgKey != null ? dgrmSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FhrdDegreemst)) {
            return false;
        }
        FhrdDegreemst other = (FhrdDegreemst) object;
        if ((this.dgrmSrgKey == null && other.dgrmSrgKey != null) || (this.dgrmSrgKey != null && !this.dgrmSrgKey.equals(other.dgrmSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FhrdDegreemst[ dgrmSrgKey=" + dgrmSrgKey + " ]";
    }
    
}
