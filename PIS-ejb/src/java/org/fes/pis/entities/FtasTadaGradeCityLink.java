/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "FTAS_TADA_GRADE_CITY_LINK")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FtasTadaGradeCityLink.findAll", query = "SELECT f FROM FtasTadaGradeCityLink f"),
    @NamedQuery(name = "FtasTadaGradeCityLink.findByGclSrgKey", query = "SELECT f FROM FtasTadaGradeCityLink f WHERE f.gclSrgKey = :gclSrgKey"),
    @NamedQuery(name = "FtasTadaGradeCityLink.findByCity", query = "SELECT f FROM FtasTadaGradeCityLink f WHERE f.city = :city"),
    @NamedQuery(name = "FtasTadaGradeCityLink.findByStartDate", query = "SELECT f FROM FtasTadaGradeCityLink f WHERE f.startDate = :startDate"),
    @NamedQuery(name = "FtasTadaGradeCityLink.findByEndDate", query = "SELECT f FROM FtasTadaGradeCityLink f WHERE f.endDate = :endDate"),
    @NamedQuery(name = "FtasTadaGradeCityLink.findByCreatedt", query = "SELECT f FROM FtasTadaGradeCityLink f WHERE f.createdt = :createdt"),
    @NamedQuery(name = "FtasTadaGradeCityLink.findByUpdatedt", query = "SELECT f FROM FtasTadaGradeCityLink f WHERE f.updatedt = :updatedt"),
    @NamedQuery(name = "FtasTadaGradeCityLink.findBySystemUserid", query = "SELECT f FROM FtasTadaGradeCityLink f WHERE f.systemUserid = :systemUserid")})
public class FtasTadaGradeCityLink implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "GCL_SRG_KEY")
    private Long gclSrgKey;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "CITY")
    private String city;
    @Basic(optional = false)
    @NotNull
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @JoinColumn(name = "OUM_UNIT_SRNO", referencedColumnName = "OUM_UNIT_SRNO")
    @ManyToOne(optional = false)
    private SystOrgUnitMst oumUnitSrno;
    @JoinColumn(name = "GM_SRG_KEY", referencedColumnName = "GM_SRG_KEY")
    @ManyToOne(optional = false)
    private FtasTadaGradeMst gmSrgKey;
    @JoinColumn(name = "CREATEBY", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private FhrdEmpmst createby;
    @JoinColumn(name = "UPDATEBY", referencedColumnName = "USER_ID")
    @ManyToOne
    private FhrdEmpmst updateby;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gclSrgKey")
    private Collection<FtasTadaCashallowRuleMst> ftasTadaCashallowRuleMstCollection;

    public FtasTadaGradeCityLink() {
    }

    public FtasTadaGradeCityLink(Long gclSrgKey) {
        this.gclSrgKey = gclSrgKey;
    }

    public FtasTadaGradeCityLink(Long gclSrgKey, String city, Date startDate, Date createdt) {
        this.gclSrgKey = gclSrgKey;
        this.city = city;
        this.startDate = startDate;
        this.createdt = createdt;
    }

    public Long getGclSrgKey() {
        return gclSrgKey;
    }

    public void setGclSrgKey(Long gclSrgKey) {
        this.gclSrgKey = gclSrgKey;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public SystOrgUnitMst getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(SystOrgUnitMst oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public FtasTadaGradeMst getGmSrgKey() {
        return gmSrgKey;
    }

    public void setGmSrgKey(FtasTadaGradeMst gmSrgKey) {
        this.gmSrgKey = gmSrgKey;
    }

    public FhrdEmpmst getCreateby() {
        return createby;
    }

    public void setCreateby(FhrdEmpmst createby) {
        this.createby = createby;
    }

    public FhrdEmpmst getUpdateby() {
        return updateby;
    }

    public void setUpdateby(FhrdEmpmst updateby) {
        this.updateby = updateby;
    }

    @XmlTransient
    public Collection<FtasTadaCashallowRuleMst> getFtasTadaCashallowRuleMstCollection() {
        return ftasTadaCashallowRuleMstCollection;
    }

    public void setFtasTadaCashallowRuleMstCollection(Collection<FtasTadaCashallowRuleMst> ftasTadaCashallowRuleMstCollection) {
        this.ftasTadaCashallowRuleMstCollection = ftasTadaCashallowRuleMstCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gclSrgKey != null ? gclSrgKey.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FtasTadaGradeCityLink)) {
            return false;
        }
        FtasTadaGradeCityLink other = (FtasTadaGradeCityLink) object;
        if ((this.gclSrgKey == null && other.gclSrgKey != null) || (this.gclSrgKey != null && !this.gclSrgKey.equals(other.gclSrgKey))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.FtasTadaGradeCityLink[ gclSrgKey=" + gclSrgKey + " ]";
    }
    
}
