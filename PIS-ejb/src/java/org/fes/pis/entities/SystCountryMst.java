/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anu
 */
@Entity
@Table(name = "SYST_COUNTRY_MST")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SystCountryMst.findAll", query = "SELECT s FROM SystCountryMst s"),
    @NamedQuery(name = "SystCountryMst.findByOumUnitSrno", query = "SELECT s FROM SystCountryMst s WHERE s.oumUnitSrno = :oumUnitSrno"),
    @NamedQuery(name = "SystCountryMst.findByCtryUniqueSrno", query = "SELECT s FROM SystCountryMst s WHERE s.ctryUniqueSrno = :ctryUniqueSrno"),
    @NamedQuery(name = "SystCountryMst.findByCtryName", query = "SELECT s FROM SystCountryMst s WHERE s.ctryName = :ctryName"),
    @NamedQuery(name = "SystCountryMst.findByCtryCode", query = "SELECT s FROM SystCountryMst s WHERE s.ctryCode = :ctryCode"),
    @NamedQuery(name = "SystCountryMst.findByWorkingflag", query = "SELECT s FROM SystCountryMst s WHERE s.workingflag = :workingflag"),
    @NamedQuery(name = "SystCountryMst.findByMisFlag", query = "SELECT s FROM SystCountryMst s WHERE s.misFlag = :misFlag"),
    @NamedQuery(name = "SystCountryMst.findByDeleteFlag", query = "SELECT s FROM SystCountryMst s WHERE s.deleteFlag = :deleteFlag"),
    @NamedQuery(name = "SystCountryMst.findByCreateby", query = "SELECT s FROM SystCountryMst s WHERE s.createby = :createby"),
    @NamedQuery(name = "SystCountryMst.findByCreatedt", query = "SELECT s FROM SystCountryMst s WHERE s.createdt = :createdt"),
    @NamedQuery(name = "SystCountryMst.findByUpdateby", query = "SELECT s FROM SystCountryMst s WHERE s.updateby = :updateby"),
    @NamedQuery(name = "SystCountryMst.findByUpdatedt", query = "SELECT s FROM SystCountryMst s WHERE s.updatedt = :updatedt"),
    @NamedQuery(name = "SystCountryMst.findBySystemUserid", query = "SELECT s FROM SystCountryMst s WHERE s.systemUserid = :systemUserid"),
    @NamedQuery(name = "SystCountryMst.findByExportFlag", query = "SELECT s FROM SystCountryMst s WHERE s.exportFlag = :exportFlag"),
    @NamedQuery(name = "SystCountryMst.findByImportFlag", query = "SELECT s FROM SystCountryMst s WHERE s.importFlag = :importFlag"),
    @NamedQuery(name = "SystCountryMst.findByPortalUrl", query = "SELECT s FROM SystCountryMst s WHERE s.portalUrl = :portalUrl")})
public class SystCountryMst implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "OUM_UNIT_SRNO")
    private int oumUnitSrno;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CTRY_UNIQUE_SRNO")
    private Integer ctryUniqueSrno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CTRY_NAME")
    private String ctryName;
    @Size(max = 50)
    @Column(name = "CTRY_CODE")
    private String ctryCode;
    @Size(max = 1)
    @Column(name = "WORKINGFLAG")
    private String workingflag;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "MIS_FLAG")
    private String misFlag;
    @Size(max = 1)
    @Column(name = "DELETE_FLAG")
    private String deleteFlag;
    @Column(name = "CREATEBY")
    private Integer createby;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CREATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdt;
    @Column(name = "UPDATEBY")
    private Integer updateby;
    @Column(name = "UPDATEDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedt;
    @Column(name = "SYSTEM_USERID")
    private Integer systemUserid;
    @Size(max = 1)
    @Column(name = "EXPORT_FLAG")
    private String exportFlag;
    @Size(max = 1)
    @Column(name = "IMPORT_FLAG")
    private String importFlag;
    @Size(max = 200)
    @Column(name = "PORTAL_URL")
    private String portalUrl;

    public SystCountryMst() {
    }

    public SystCountryMst(Integer ctryUniqueSrno) {
        this.ctryUniqueSrno = ctryUniqueSrno;
    }

    public SystCountryMst(Integer ctryUniqueSrno, int oumUnitSrno, String ctryName, String misFlag, Date createdt) {
        this.ctryUniqueSrno = ctryUniqueSrno;
        this.oumUnitSrno = oumUnitSrno;
        this.ctryName = ctryName;
        this.misFlag = misFlag;
        this.createdt = createdt;
    }

    public int getOumUnitSrno() {
        return oumUnitSrno;
    }

    public void setOumUnitSrno(int oumUnitSrno) {
        this.oumUnitSrno = oumUnitSrno;
    }

    public Integer getCtryUniqueSrno() {
        return ctryUniqueSrno;
    }

    public void setCtryUniqueSrno(Integer ctryUniqueSrno) {
        this.ctryUniqueSrno = ctryUniqueSrno;
    }

    public String getCtryName() {
        return ctryName;
    }

    public void setCtryName(String ctryName) {
        this.ctryName = ctryName;
    }

    public String getCtryCode() {
        return ctryCode;
    }

    public void setCtryCode(String ctryCode) {
        this.ctryCode = ctryCode;
    }

    public String getWorkingflag() {
        return workingflag;
    }

    public void setWorkingflag(String workingflag) {
        this.workingflag = workingflag;
    }

    public String getMisFlag() {
        return misFlag;
    }

    public void setMisFlag(String misFlag) {
        this.misFlag = misFlag;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Integer getCreateby() {
        return createby;
    }

    public void setCreateby(Integer createby) {
        this.createby = createby;
    }

    public Date getCreatedt() {
        return createdt;
    }

    public void setCreatedt(Date createdt) {
        this.createdt = createdt;
    }

    public Integer getUpdateby() {
        return updateby;
    }

    public void setUpdateby(Integer updateby) {
        this.updateby = updateby;
    }

    public Date getUpdatedt() {
        return updatedt;
    }

    public void setUpdatedt(Date updatedt) {
        this.updatedt = updatedt;
    }

    public Integer getSystemUserid() {
        return systemUserid;
    }

    public void setSystemUserid(Integer systemUserid) {
        this.systemUserid = systemUserid;
    }

    public String getExportFlag() {
        return exportFlag;
    }

    public void setExportFlag(String exportFlag) {
        this.exportFlag = exportFlag;
    }

    public String getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(String importFlag) {
        this.importFlag = importFlag;
    }

    public String getPortalUrl() {
        return portalUrl;
    }

    public void setPortalUrl(String portalUrl) {
        this.portalUrl = portalUrl;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ctryUniqueSrno != null ? ctryUniqueSrno.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SystCountryMst)) {
            return false;
        }
        SystCountryMst other = (SystCountryMst) object;
        if ((this.ctryUniqueSrno == null && other.ctryUniqueSrno != null) || (this.ctryUniqueSrno != null && !this.ctryUniqueSrno.equals(other.ctryUniqueSrno))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.fes.pis.entities.SystCountryMst[ ctryUniqueSrno=" + ctryUniqueSrno + " ]";
    }
    
}
