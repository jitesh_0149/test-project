/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdEmpEventDtl;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdEmpEventDtlFacade extends AbstractFacade<FhrdEmpEventDtl> implements FhrdEmpEventDtlFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdEmpEventDtlFacade() {
        super(FhrdEmpEventDtl.class);
    }

    @Override
    public List<FhrdEmpEventDtl> findEventEmpwise(Integer p_userid, String p_event_name) {
        String v_query = "SELECT * FROM FHRD_EMP_EVENT_DTL "
                + " WHERE USER_ID= " + p_userid
                + " AND EM_SRG_KEY =FHRD_PACK.GET_EVENT_SRG_KEY(" + p_userid + ",'" + p_event_name + "')";
        return em.createNativeQuery(v_query, FhrdEmpEventDtl.class).getResultList();
    }

    @Override
    public List<FhrdEmpEventDtl> findAllEmpwise(Integer p_userid) {
        String v_query = "SELECT * FROM FHRD_EMP_EVENT_DTL"
                + " WHERE USER_ID=" + p_userid
                + " ORDER BY EVENT_DATE";
        return em.createNativeQuery(v_query, FhrdEmpEventDtl.class).getResultList();
    }
}
