/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.entities.FhrdEarndedn;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdEarndednFacade extends AbstractFacade<FhrdEarndedn> implements FhrdEarndednFacadeLocal {

    
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdEarndednFacade() {
        super(FhrdEarndedn.class);
    }

//    @Override
//    public List<FhrdEarndedn> findEdRuleContractwise(Integer p_oum_unit_srno, BigDecimal p_cm_srg_key) {
//        String v_query = " select * from fhrd_earndedn"
//                + " where rmgm_srg_key in (select rmgm_srg_key from fhrd_rule_group_relation"
//                + " where rgc_srg_key IN (SELECT RGC_SRG_KEY FROM FHRD_RULE_GROUP_CATEGORY WHERE  CAT_DESC= 'ACCOUNT' AND OUM_UNIT_SRNO IN (OU_PACK.GET_OUM_UNIT_SRNO('FHRD_RULE_GROUP_CATEGORY'," + p_oum_unit_srno + ")))"
//                + " and ragm_srg_key in (select ragm_srg_key from fhrd_contract_group_relation"
//                + " where cm_srg_key=" + p_cm_srg_key + "))";
//        System.out.println("query " + v_query);
//        return em.createNativeQuery(
//                v_query, FhrdEarndedn.class).getResultList();
//    }
    @Override
    public List<FhrdEarndedn> findAll(Integer p_oum_unit_srno, Date p_start_date, Date p_end_date) {
        String v_start_date = DateTimeUtility.ChangeDateFormat(p_start_date, null);
        String v_end_date = DateTimeUtility.ChangeDateFormat(p_end_date, null);
        String v_query = "SELECT * FROM FHRD_EARNDEDN "
                + " WHERE OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_EARNDEDN'," + p_oum_unit_srno + ") ";

        if (p_end_date != null) {
            v_query += " AND START_DATE<= '" + v_end_date + "'";
        }
        if (p_start_date != null) {
            v_query += " AND (END_DATE>= '" + v_start_date + "'  OR END_DATE IS NULL) ";
        }
        v_query += " ORDER BY ED_SRG_KEY";
        return em.createNativeQuery(v_query, FhrdEarndedn.class).getResultList();
    }

    @Override
    public List<FhrdEarndedn> findEDRuleContractwise(String p_user_id, List<BigDecimal> p_lst_cm_srg_key, Date p_start_date, Date p_end_date) {
        String v_query = " SELECT RGR.RMGM_SRG_KEY FROM "
                + " FHRD_RULE_GROUP_RELATION RGR,FHRD_CONTRACT_GROUP_RELATION CGR "
                + " WHERE "
                + " RGR.RAGM_SRG_KEY=CGR.RAGM_SRG_KEY "
                + " AND CGR.CM_SRG_KEY IN (" + p_lst_cm_srg_key.toString().replace("[", "").replace("]", "") + ") ";
        List<BigDecimal> lst_rmgm_srg_key = em.createNativeQuery(v_query).getResultList();
        v_query = " SELECT * FROM FHRD_EARNDEDN "
                + " WHERE  ED_SRG_KEY IN (SELECT * FROM TABLE(FTAS_PACK.GET_ED_RULE_RMGM_WISE( "
                + " " + lst_rmgm_srg_key.toString().replace("[", "'").replace("]", "'") + ", "
                + " '" + DateTimeUtility.ChangeDateFormat(p_start_date, null) + "', "
                + " '" + DateTimeUtility.ChangeDateFormat(p_end_date, null) + "', "
                + p_user_id + " ))) ORDER BY EARNDEDNCD";
//        System.out.println("ed query "+v_query);
        return em.createNativeQuery(v_query, FhrdEarndedn.class).getResultList();
    }
}
