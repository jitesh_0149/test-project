/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FcadAddrMst;

/**
 *
 * @author anu
 */
@Local
public interface FcadAddrMstFacadeLocal {

    void create(FcadAddrMst fcadAddrMst);

    void edit(FcadAddrMst fcadAddrMst);

    void remove(FcadAddrMst fcadAddrMst);

    FcadAddrMst find(Object id);

    List<FcadAddrMst> findAll();

    List<FcadAddrMst> findRange(int[] range);

    int count();

    public List<FcadAddrMst> findByAddrNo(BigDecimal p_addrno);

    public BigDecimal getMaxSrno_fhrdAddrMst(Integer p_oum_unit_srno);
}
