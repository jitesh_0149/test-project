/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdDegreeStreamMst;

/**
 *
 * @author anu
 */
@Local
public interface FhrdDegreeStreamMstFacadeLocal {

    void create(FhrdDegreeStreamMst fhrdDegreeStreamMst);

    void edit(FhrdDegreeStreamMst fhrdDegreeStreamMst);

    void remove(FhrdDegreeStreamMst fhrdDegreeStreamMst);

    FhrdDegreeStreamMst find(Object id);

    List<FhrdDegreeStreamMst> findAll();

    List<FhrdDegreeStreamMst> findRange(int[] range);

    int count();

    public List<FhrdDegreeStreamMst> findAll(Integer p_oum_unit_srno);
}
