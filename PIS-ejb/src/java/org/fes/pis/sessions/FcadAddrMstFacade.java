/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FcadAddrMst;

/**
 *
 * @author anu
 */
@Stateless
public class FcadAddrMstFacade extends AbstractFacade<FcadAddrMst> implements FcadAddrMstFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FcadAddrMstFacade() {
        super(FcadAddrMst.class);
    }

    @Override
    public List<FcadAddrMst> findByAddrNo(BigDecimal p_addrno) {
        return em.createNativeQuery(
                " SELECT * FROM FCAD_ADDR_MST"
                + " WHERE AM_UNIQUE_SRNO=" + p_addrno, FcadAddrMst.class).getResultList();

    }

    @Override
    public BigDecimal getMaxSrno_fhrdAddrMst(Integer p_oum_unit_srno) {
        BigDecimal v_maxsrno =
                (BigDecimal) em.createNativeQuery(
                " SELECT NVL(MAX(addrno),0)+1  FROM FCAD_ADDR_MST "
                + "  WHERE oum_unit_srno = " + p_oum_unit_srno).getSingleResult();
        return v_maxsrno;
    }
}
