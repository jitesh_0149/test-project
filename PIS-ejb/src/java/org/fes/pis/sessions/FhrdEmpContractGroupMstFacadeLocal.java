/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdEmpContractGroupMst;

/**
 *
 * @author anu
 */
@Local
public interface FhrdEmpContractGroupMstFacadeLocal {

    void create(FhrdEmpContractGroupMst fhrdEmpContractGroupMst);

    void edit(FhrdEmpContractGroupMst fhrdEmpContractGroupMst);

    void remove(FhrdEmpContractGroupMst fhrdEmpContractGroupMst);

    FhrdEmpContractGroupMst find(Object id);

    List<FhrdEmpContractGroupMst> findAll();

    List<FhrdEmpContractGroupMst> findRange(int[] range);

    int count();
    
}
