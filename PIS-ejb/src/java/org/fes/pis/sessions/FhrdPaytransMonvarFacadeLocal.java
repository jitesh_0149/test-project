/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdPaytransMonvar;

/**
 *
 * @author anu
 */
@Local
public interface FhrdPaytransMonvarFacadeLocal {

    void create(FhrdPaytransMonvar fhrdPaytransMonvar);

    void edit(FhrdPaytransMonvar fhrdPaytransMonvar);

    void remove(FhrdPaytransMonvar fhrdPaytransMonvar);

    FhrdPaytransMonvar find(Object id);

    List<FhrdPaytransMonvar> findAll();

    List<FhrdPaytransMonvar> findRange(int[] range);

    int count();
    
}
