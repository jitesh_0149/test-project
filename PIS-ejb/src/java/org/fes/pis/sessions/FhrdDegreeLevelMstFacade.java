/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdDegreeLevelMst;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdDegreeLevelMstFacade extends AbstractFacade<FhrdDegreeLevelMst> implements FhrdDegreeLevelMstFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdDegreeLevelMstFacade() {
        super(FhrdDegreeLevelMst.class);
    }

    @Override
    public List<FhrdDegreeLevelMst> findAll(Integer p_oum_unit_srno, boolean findByDate) {
        String v_query = "SELECT * FROM FHRD_DEGREE_LEVEL_MST "
                + " WHERE OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_DEGREE_LEVEL_MST'," + p_oum_unit_srno + ")";
        if (findByDate) {
            v_query += " AND (END_DATE>= TO_DATE(TO_CHAR(SYSDATE,'DD-MON-YYYY'),'DD-MON-YYYY')  OR END_DATE IS NULL) "
                    + " AND START_DATE<=TO_DATE(TO_CHAR(SYSDATE,'DD-MON-YYYY'),'DD-MON-YYYY')";
        }
        v_query += " ORDER BY DLM_DESCRIPTION";
        return em.createNativeQuery(v_query, FhrdDegreeLevelMst.class).getResultList();
    }
}
