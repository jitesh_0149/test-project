/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdCasteCategoryMst;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdCasteCategorymstFacade extends AbstractFacade<FhrdCasteCategoryMst> implements FhrdCasteCategorymstFacadeLocal {
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdCasteCategorymstFacade() {
        super(FhrdCasteCategoryMst.class);
    }
    
    @Override   
    public List<FhrdCasteCategoryMst> findAll(Integer p_oum_unit_srno) {
        return em.createNativeQuery(
                " SELECT * FROM FHRD_CASTE_CATEGORY_MST "
                + " WHERE OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FTAS_LEAVEMST'," + p_oum_unit_srno + ")", FhrdCasteCategoryMst.class).getResultList();
    }
}
