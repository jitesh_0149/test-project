/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdEmpContractGroupMst;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdEmpContractGroupMstFacade extends AbstractFacade<FhrdEmpContractGroupMst> implements FhrdEmpContractGroupMstFacadeLocal {
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdEmpContractGroupMstFacade() {
        super(FhrdEmpContractGroupMst.class);
    }
    
}
