/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FtasBulkAttTemp;

/**
 *
 * @author anu
 */
@Stateless
public class FtasBulkAttTempFacade extends AbstractFacade<FtasBulkAttTemp> implements FtasBulkAttTempFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FtasBulkAttTempFacade() {
        super(FtasBulkAttTemp.class);
    }

    @Override
    public List<FtasBulkAttTemp> findAllUnsavedRecord(BigDecimal p_bat_srno) {
        return em.createNativeQuery("SELECT * FROM FTAS_BULK_ATT_TEMP"
                + " WHERE BAT_SRNO=" + p_bat_srno, FtasBulkAttTemp.class).getResultList();
    }
}
