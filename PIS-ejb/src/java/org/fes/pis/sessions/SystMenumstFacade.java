/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.SystMenumst;

/**
 *
 * @author anu
 */
@Stateless
public class SystMenumstFacade extends AbstractFacade<SystMenumst> implements SystMenumstFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SystMenumstFacade() {
        super(SystMenumst.class);
    }
}
