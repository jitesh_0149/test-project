/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FtasTadaGradeMst;

/**
 *
 * @author mayuri
 */
@Stateless
public class FtasTadaGradeMstFacade extends AbstractFacade<FtasTadaGradeMst> {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FtasTadaGradeMstFacade() {
        super(FtasTadaGradeMst.class);
    }

    public List<FtasTadaGradeMst> findAll(Integer p_oum_unit_srno, boolean p_active_only) {
        String v_query = "SELECT * FROM FTAS_TADA_GRADE_MST"
                + " WHERE OUM_UNIT_SRNO = OU_PACK.GET_OUM_UNIT_SRNO('FTAS_TADA_GRADE_MST'," + p_oum_unit_srno + ")";
        if (p_active_only) {
            v_query += " AND END_DATE IS NULL ";
        }
        v_query += " ORDER BY GM_DESC";
        return em.createNativeQuery(v_query, FtasTadaGradeMst.class).getResultList();
    }
}
