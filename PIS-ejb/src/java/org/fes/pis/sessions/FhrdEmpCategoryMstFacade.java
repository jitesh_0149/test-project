/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdEmpCategoryMst;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdEmpCategoryMstFacade extends AbstractFacade<FhrdEmpCategoryMst> implements FhrdEmpCategoryMstFacadeLocal {
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdEmpCategoryMstFacade() {
        super(FhrdEmpCategoryMst.class);
    }
    
}
