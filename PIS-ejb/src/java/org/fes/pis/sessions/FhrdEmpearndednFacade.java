/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.custom_entities.PaytransMonthlyVariableBean;
import org.fes.pis.entities.FhrdEmpearndedn;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdEmpearndednFacade extends AbstractFacade<FhrdEmpearndedn> implements FhrdEmpearndednFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;
    

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdEmpearndednFacade() {
        super(FhrdEmpearndedn.class);
    }

    @Override
    public List<FhrdEmpearndedn> getEDRuleEmpwise(Integer p_userid, String p_contract_list, Integer p_oum_unit_srno, String p_opening_date, String p_closing_date) {
//        String v_query = "SELECT EED.*"
//                + " FROM FHRD_EMPEARNDEDN EED,"
//                + "     FHRD_EARNDEDN ED"
//                + " WHERE EED.USER_ID      =" + p_userid
//                + "  AND ED.ED_SRG_KEY     =EED.ED_SRG_KEY"
//                + "  AND EED.RMGM_SRG_KEY IN"
//                + "     (SELECT RMGM_SRG_KEY"
//                + "     FROM FHRD_RULE_GROUP_RELATION"
//                + "     WHERE RGC_SRG_KEY  =FHRD_PACK.GET_ACCOUNT_RGC_SRG_KEY(" + p_oum_unit_srno + ")"
//                + "      AND RAGM_SRG_KEY IN"
//                + "          (SELECT RAGM_SRG_KEY"
//                + "          FROM FHRD_CONTRACT_GROUP_RELATION"
//                + "          WHERE CM_SRG_KEY IN (" + p_contract_list + ")"
//                + "          )"
//                + "     )"
//                + "  AND (EED.START_DATE>='" + p_opening_date + "' AND EED.START_DATE <='" + p_closing_date + "')"
//                + "  AND (EED.END_DATE  >='" + p_opening_date + "' AND EED.END_DATE   <='" + p_closing_date + "')"
//                + " ORDER BY ED.EARNDEDNCD";
        String v_query = "SELECT * "
                + "FROM   FHRD_EMPEARNDEDN "
                + "WHERE  USER_ID= "+p_userid
                + "AND    CONT_START_DATE BETWEEN '" + p_opening_date + "' AND '" + p_closing_date + "'";
//        System.out.println("que " + v_query);
        return em.createNativeQuery(v_query, FhrdEmpearndedn.class).getResultList();
    }

//    @Override
//    public List<FhrdEmpearndedn> findAllMonthlyVariable(Integer p_oum_unit_srno, Date p_start_date, Date p_end_date, Integer p_user_id, boolean isPending) {
//        String v_query = "SELECT *"
//                + " FROM FHRD_EMPEARNDEDN EED"
//                + " WHERE PAYABLE_ON_VALUE='MV' "
//                + " AND EED.USER_ID =   " + p_user_id
//                + " AND ((TO_DATE('" + DateTimeUtility.ChangeDateFormat(p_start_date, null) + "','DD-MON-YYYY') BETWEEN OPENING_DATE AND CLOSING_DATE)"
//                + " OR (TO_DATE('" + DateTimeUtility.ChangeDateFormat(p_end_date, null) + "','DD-MON-YYYY') BETWEEN OPENING_DATE AND CLOSING_DATE))";
//        if (isPending) {
//            v_query += " AND EED.EED_SRG_KEY NOT IN"
//                    + "     ( SELECT EED_SRG_KEY FROM FHRD_PAYTRANS_MONVAR WHERE USER_ID=EED.USER_ID"
//                    + "     )";
//        }
////        System.out.println("MV Query " + v_query);
//        return em.createNativeQuery(v_query, FhrdEmpearndedn.class).getResultList();
//    }
//    
    @Override
    public List<PaytransMonthlyVariableBean> findAllMonthlyVariable(Integer p_oum_unit_srno, Date p_start_date, Date p_end_date, Integer p_user_id, String p_status) {
        String v_query = "SELECT EED.USER_ID,MV.PTMV_SRG_KEY,"
                + " EED.EED_SRG_KEY,EED.EARNDEDNCD,EM.EARNDEDN_NAME,MV.VARIALBLE_AMT"
                + " FROM FHRD_EARNDEDNMST EM,FHRD_EMPEARNDEDN EED"
                + " LEFT OUTER JOIN FHRD_PAYTRANS_MONVAR MV"
                + " ON (EED.USER_ID = MV.USER_ID AND EED.EED_SRG_KEY=MV.EED_SRG_KEY)"
                + " WHERE "
                + "     EM.EDM_SRG_KEY=EED.EDM_SRG_KEY"
                + " AND  EED.PAYABLE_ON_VALUE='MV' "
                + " AND  EED.USER_ID =     736"
                + " AND  ((TO_DATE('1-APR-2014','DD-MON-YYYY') BETWEEN EED.OPENING_DATE AND EED.CLOSING_DATE)"
                + " OR   (TO_DATE('30-APR-2014','DD-MON-YYYY') BETWEEN EED.OPENING_DATE AND EED.CLOSING_DATE))";
        if (p_status.equals("S")) {
            v_query += " AND MV.PTMV_SRG_KEY IS NOT NULL";
        }
        if (p_status.equals("P")) {
            v_query += " AND MV.PTMV_SRG_KEY IS NULL";
        }
//        System.out.println("MV Query " + v_query);
        return em.createNativeQuery(v_query, PaytransMonthlyVariableBean.class).getResultList();
    }
}
