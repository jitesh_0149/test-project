/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdReligionMst;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdReligionMstFacade extends AbstractFacade<FhrdReligionMst> implements FhrdReligionMstFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdReligionMstFacade() {
        super(FhrdReligionMst.class);
    }

    @Override
    public List<FhrdReligionMst> findAll(Integer p_oum_unit_srno) {
        return em.createNativeQuery("SELECT * FROM FHRD_RELIGION_MST WHERE OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_RELIGION_MST'," + p_oum_unit_srno + ") ORDER BY RG_DESCRIPTION", FhrdReligionMst.class).getResultList();
    }
}
