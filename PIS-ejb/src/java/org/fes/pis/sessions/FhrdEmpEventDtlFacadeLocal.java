/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdEmpEventDtl;

/**
 *
 * @author anu
 */
@Local
public interface FhrdEmpEventDtlFacadeLocal {

    void create(FhrdEmpEventDtl fhrdEmpEventDtl);

    void edit(FhrdEmpEventDtl fhrdEmpEventDtl);

    void remove(FhrdEmpEventDtl fhrdEmpEventDtl);

    FhrdEmpEventDtl find(Object id);

    List<FhrdEmpEventDtl> findAll();

    List<FhrdEmpEventDtl> findRange(int[] range);

    int count();

    public List<FhrdEmpEventDtl> findEventEmpwise(Integer p_userid, String p_event_name);

    public List<FhrdEmpEventDtl> findAllEmpwise(Integer p_userid);
}
