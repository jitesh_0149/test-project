/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdPaytransAttdCalcLog;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdPaytransAttdCalcLogFacade extends AbstractFacade<FhrdPaytransAttdCalcLog> implements FhrdPaytransAttdCalcLogFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdPaytransAttdCalcLogFacade() {
        super(FhrdPaytransAttdCalcLog.class);
    }

    @Override
    public List<FhrdPaytransAttdCalcLog> findAllByAttdSrgKey(BigDecimal p_attd_srg_key) {
        String v_query = "SELECT * FROM FHRD_PAYTRANS_ATTD_CALC_LOG WHERE ATTD_SRG_KEY = " + p_attd_srg_key;
        return em.createNativeQuery(v_query, FhrdPaytransAttdCalcLog.class).getResultList();
    }

    @Override
    public BigDecimal getAttdSrgKey() {
        BigDecimal v_maxsrno = (BigDecimal) em.createNativeQuery(" select FHRD_SEQ_PT_ATTD_CALC_LOG_14.nextval from dual").getSingleResult();
        return v_maxsrno;
    }
}
