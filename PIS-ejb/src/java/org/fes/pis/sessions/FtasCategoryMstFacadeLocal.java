/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FtasCategoryMst;

/**
 *
 * @author anu
 */
@Local
public interface FtasCategoryMstFacadeLocal {

    void create(FtasCategoryMst ftasCategoryMst);

    void edit(FtasCategoryMst ftasCategoryMst);

    void remove(FtasCategoryMst ftasCategoryMst);

    FtasCategoryMst find(Object id);

    List<FtasCategoryMst> findAll();

    List<FtasCategoryMst> findRange(int[] range);

    int count();

    public List<FtasCategoryMst> find_category(String p_category_type);
    
    public FtasCategoryMst find_category(String p_cat, String p_cal_val_desc);
    
}
