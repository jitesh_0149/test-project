/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FtasTadaCashallowRuleMst;

/**
 *
 * @author mayuri
 */
@Local
public interface FtasTadaCashallowRuleMstFacadeLocal {

    void create(FtasTadaCashallowRuleMst ftasTadaCashallowRuleMst);

    void edit(FtasTadaCashallowRuleMst ftasTadaCashallowRuleMst);

    void remove(FtasTadaCashallowRuleMst ftasTadaCashallowRuleMst);

    FtasTadaCashallowRuleMst find(Object id);

    List<FtasTadaCashallowRuleMst> findAll();

    List<FtasTadaCashallowRuleMst> findRange(int[] range);

    int count();
    
    public List<FtasTadaCashallowRuleMst> findAll(Integer p_oum_unit_srno);
}
