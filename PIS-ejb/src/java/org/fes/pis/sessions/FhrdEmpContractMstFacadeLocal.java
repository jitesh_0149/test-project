/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.custom_entities.Date_Duration;
import org.fes.pis.entities.FhrdEmpContractMst;

/**
 *
 * @author anu
 */
@Local
public interface FhrdEmpContractMstFacadeLocal {

    void create(FhrdEmpContractMst fhrdEmpContractMst);

    void edit(FhrdEmpContractMst fhrdEmpContractMst);

    void remove(FhrdEmpContractMst fhrdEmpContractMst);

    FhrdEmpContractMst find(Object id);

    List<FhrdEmpContractMst> findAll();

    List<FhrdEmpContractMst> findRange(int[] range);

    int count();

    List<FhrdEmpContractMst> findAllContractEmpwise(Integer p_userid, String p_contract_type);

    public List<FhrdEmpContractMst> findAllContractEmpwiseDatewise(Integer p_userid, String p_strat_date, String p_end_date, String p_sudden_enddate);

    public List<FhrdEmpContractMst> findLatestContractEmpwise(Integer p_userid, String p_contract_category);

    public List<FhrdEmpContractMst> findAllByMainContract(Integer p_user_id, String p_start_date, String p_end_date);

    public List<FhrdEmpContractMst> findMaxContractPeriod(Integer p_user_id, String p_cm_srg_key);

    public List<FhrdEmpContractMst> findAllToRelieveTerminate(Integer p_user_id, String p_end_date);

    public Date_Duration findMinMaxContractDate(Integer p_user_id);

    public FhrdEmpContractMst findForRelTermNoticePeriod(Integer p_user_id, Date p_date);
}
