/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdEmpleavebal;
import org.fes.pis.entities.FhrdEmpleaverule;

/**
 *
 * @author anu
 */
@Local
public interface FhrdEmpleaveruleFacadeLocal {

    void create(FhrdEmpleaverule fhrdEmpleaverule);

    void edit(FhrdEmpleaverule fhrdEmpleaverule);

    void remove(FhrdEmpleaverule fhrdEmpleaverule);

    FhrdEmpleaverule find(Object id);

    List<FhrdEmpleaverule> findAll();

    List<FhrdEmpleaverule> findRange(int[] range);

    int count();

    public FhrdEmpleaverule getMinMaxTaken_holidayEmbedded(Integer p_rulesrno);

    public List<FhrdEmpleavebal> getLeaveRuleEmpwise(Integer p_userid, Integer p_oum_unit_snro,String p_contract_list, String p_opening_date, String p_closing_date);
}
