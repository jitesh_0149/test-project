/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdPortfolioMst;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdPortfolioMstFacade extends AbstractFacade<FhrdPortfolioMst> {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdPortfolioMstFacade() {
        super(FhrdPortfolioMst.class);
    }

    public List<FhrdPortfolioMst> findAll(Integer p_oum_unit_srno, boolean findByDate) {
        String v_query = "SELECT * FROM FHRD_PORTFOLIO_MST "
                + " WHERE OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_PORTFOLIO_MST'," + p_oum_unit_srno + ")";
        if (findByDate) {
            v_query += " AND (END_DATE>= TO_DATE(TO_CHAR(SYSDATE,'DD-MON-YYYY'),'DD-MON-YYYY')  OR END_DATE IS NULL) "
                    + " AND START_DATE<=TO_DATE(TO_CHAR(SYSDATE,'DD-MON-YYYY'),'DD-MON-YYYY')";
        }
        v_query += " ORDER BY PM_NAME";
        return em.createNativeQuery(v_query, FhrdPortfolioMst.class).getResultList();
    }
}
