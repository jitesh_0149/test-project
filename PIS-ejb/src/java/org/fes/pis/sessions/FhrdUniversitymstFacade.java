/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdUniversitymst;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdUniversitymstFacade extends AbstractFacade<FhrdUniversitymst> implements FhrdUniversitymstFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdUniversitymstFacade() {
        super(FhrdUniversitymst.class);
    }

    @Override
    public List<FhrdUniversitymst> findAll(Integer p_oum_unit_srno) {
        return em.createNativeQuery(
                " SELECT * FROM FHRD_UNIVERSITYMST "
                + " WHERE OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_UNIVERSITYMST'," + p_oum_unit_srno + ")"
                + " ORDER BY UNIVERSITYCD", FhrdUniversitymst.class).getResultList();
    }

    @Override
    public boolean isUniversityExist(Integer p_oum_unit_srno, String p_univercity_code, String p_unitversity_name, String p_city, String p_state) {
        String v_query = "SELECT COUNT(*) FROM FHRD_UNIVERSITYMST "
                + " WHERE "
                + " (UNIVERSITYCD='" + p_univercity_code + "' AND OUM_UNIT_SRNO IN ( OU_PACK.GET_OUM_UNIT_SRNO('FHRD_UNIVERSITYMST'," + p_oum_unit_srno + "))  AND CITY='" + p_city + "' AND STATE='" + p_state + "') "
                + " OR ( UNIVERSITY_NAME='" + p_unitversity_name.replaceAll("'", "''") + "' AND OUM_UNIT_SRNO IN ( OU_PACK.GET_OUM_UNIT_SRNO('FHRD_UNIVERSITYMST'," + p_oum_unit_srno + "))  AND CITY='" + p_city + "' AND STATE='" + p_state + "') ";
        BigDecimal v_total_records = (BigDecimal) em.createNativeQuery(v_query).getSingleResult();
        return !v_total_records.equals(BigDecimal.ZERO);


    }

    @Override
    public String findUniversity(Integer p_oum_unit_srno, String p_univercity_code) {
        String v_query = "SELECT * FROM FHRD_UNIVERSITYMST "
                + " WHERE "
                + " (OUM_UNIT_SRNO IN ( OU_PACK.GET_OUM_UNIT_SRNO('FHRD_UNIVERSITYMST'," + p_oum_unit_srno + ")) AND UNIVERSITYCD='" + p_univercity_code + "') ";
        List<FhrdUniversitymst> lst_temp = em.createNativeQuery(v_query, FhrdUniversitymst.class).getResultList();
        String v_returning_value = null;
        if (!lst_temp.isEmpty()) {
            v_returning_value = lst_temp.get(0).getUmSrgKey().toString();
        }
        return v_returning_value;


    }
}
