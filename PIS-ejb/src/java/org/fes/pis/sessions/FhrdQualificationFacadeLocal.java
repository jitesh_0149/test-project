/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdQualification;

/**
 *
 * @author anu
 */
@Local
public interface FhrdQualificationFacadeLocal {

    void create(FhrdQualification fhrdQualification);

    void edit(FhrdQualification fhrdQualification);

    void remove(FhrdQualification fhrdQualification);

    FhrdQualification find(Object id);

    List<FhrdQualification> findAll();

    List<FhrdQualification> findRange(int[] range);

    int count();

    public List<FhrdQualification> findQualificationUseridwise(Integer p_userid);

    public List<String> findAllColleges(Integer p_oum_unit_srno);
}
