/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.SystCountryMst;

/**
 *
 * @author fes
 */
@Stateless
public class SystCountryMstFacade extends AbstractFacade<SystCountryMst> implements SystCountryMstFacadeLocal {
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SystCountryMstFacade() {
        super(SystCountryMst.class);
    }
    
    @Override
    public List<SystCountryMst> findAll(Integer p_oum_unit_srno, String p_country_name) {
        String v_query = "SELECT * FROM SYST_COUNTRY_MST "
                + " WHERE OUM_UNIT_SRNO=OU_PACK.GET_ORG_SRNO(" + p_oum_unit_srno + ")";
        if (p_country_name != null) {
            v_query += " AND CTRY_NAME = '" + p_country_name + "'";
        }
        v_query += " ORDER BY CTRY_NAME";
        return em.createNativeQuery(v_query, SystCountryMst.class).getResultList();
    }
}
