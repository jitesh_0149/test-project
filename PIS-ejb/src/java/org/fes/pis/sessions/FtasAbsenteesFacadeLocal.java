/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FtasAbsentees;

/**
 *
 * @author anu
 */
@Local
public interface FtasAbsenteesFacadeLocal {

    void create(FtasAbsentees ftasAbsentees);

    void edit(FtasAbsentees ftasAbsentees);

    void remove(FtasAbsentees ftasAbsentees);

    FtasAbsentees find(Object id);

    List<FtasAbsentees> findAll();

    List<FtasAbsentees> findRange(int[] range);

    int count();

    public List<FtasAbsentees> find_Approval_List(Integer p_userid, Integer p_oum, Integer p_srno);

    public List<String> getAppliedApplication(Integer p_userid, Integer p_org_unit_srno);

    public Integer getMaxSrno_ftasAbsentees(Integer p_userid, Integer p_user_oum);

    public Integer getMaxCancelSrnoFtasAbsentees(Integer p_userid, Integer p_oum_unit_srno);

    public List<FtasAbsentees> findAll_ApprovalList(Integer p_userid);

    public List<FtasAbsentees> findAll_ApprovedApplList(Integer p_userid, Integer p_oum_unit_srno, boolean p_partial_allowed);

    public BigDecimal getLeaveRatio(BigDecimal p_emplb_srg_key, BigDecimal p_parent_emplb_srg_key);

    public BigDecimal checkForWhileOnTourTraining(Integer p_user_id, Date p_from_date, Date p_to_date);

    public List<FtasAbsentees> findAll(Integer p_oum_unit_srno, Integer p_user_id, boolean p_native_level, boolean p_org_level, Date p_from_date, Date p_to_date, String p_lm_srg_key, boolean p_org_access_rights, String p_application_flag, boolean p_active_only, String p_order_by);

    public List<FtasAbsentees> findAllForTABill(Integer p_user_id);
}
