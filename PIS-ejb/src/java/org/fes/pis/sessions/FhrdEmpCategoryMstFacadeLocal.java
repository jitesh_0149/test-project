/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdEmpCategoryMst;

/**
 *
 * @author anu
 */
@Local
public interface FhrdEmpCategoryMstFacadeLocal {

    void create(FhrdEmpCategoryMst fhrdEmpCategoryMst);

    void edit(FhrdEmpCategoryMst fhrdEmpCategoryMst);

    void remove(FhrdEmpCategoryMst fhrdEmpCategoryMst);

    FhrdEmpCategoryMst find(Object id);

    List<FhrdEmpCategoryMst> findAll();

    List<FhrdEmpCategoryMst> findRange(int[] range);

    int count();
    
}
