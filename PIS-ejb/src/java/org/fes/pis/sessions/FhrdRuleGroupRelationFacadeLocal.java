/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdRuleGroupRelation;

/**
 *
 * @author anu
 */
@Local
public interface FhrdRuleGroupRelationFacadeLocal {

    void create(FhrdRuleGroupRelation fhrdRuleGroupRelation);

    void edit(FhrdRuleGroupRelation fhrdRuleGroupRelation);

    void remove(FhrdRuleGroupRelation fhrdRuleGroupRelation);

    FhrdRuleGroupRelation find(Object id);

    List<FhrdRuleGroupRelation> findAll();

    List<FhrdRuleGroupRelation> findRange(int[] range);

    int count();
    
}
