/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.entities.FtasDaily;

/**
 *
 * @author anu
 */
@Stateless
public class FtasDailyFacade extends AbstractFacade<FtasDaily> implements FtasDailyFacadeLocal {

    
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FtasDailyFacade() {
        super(FtasDaily.class);
    }

    @Override
    public List<FtasDaily> findAll(Integer p_user_id, Date p_from_date, Date p_to_date) {
        String v_query = "SELECT * "
                + " FROM   FTAS_DAILY"
                + " WHERE  USER_ID=" + p_user_id;
        if (p_from_date != null && p_to_date != null) {
            v_query += " AND ATTDDT BETWEEN '" + DateTimeUtility.ChangeDateFormat(p_from_date, null) + "'"
                    + "    AND '" + DateTimeUtility.ChangeDateFormat(p_to_date, null) + "'";
        }
        v_query += " ORDER BY ATTDDT";
        return em.createNativeQuery(v_query, FtasDaily.class).getResultList();
    }

    @Override
    public Integer checkTodaysAtt(Integer p_userid, Date p_date) {
        return new Integer(em.createNativeQuery(
                "SELECT FTAS_PACK.GET_TODAYS_ATTENDANCE_HALF( "
                + " " + p_userid + ", "
                + "'" + DateTimeUtility.ChangeDateFormat(p_date, null) + "') FROM DUAL").getSingleResult().toString());
    }

    @Override
    public FtasDaily find(Integer p_userid, Date p_date) {
        String v_date = DateTimeUtility.ChangeDateFormat(p_date, null);
        return (FtasDaily) em.createNativeQuery(
                "SELECT  * FROM FTAS_DAILY "
                + " WHERE USER_ID=" + p_userid + ""
                + " AND ATTDDT = '" + v_date + "'", FtasDaily.class).getSingleResult();
    }
}
