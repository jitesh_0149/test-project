/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdReligionMst;

/**
 *
 * @author anu
 */
@Local
public interface FhrdReligionMstFacadeLocal {

    void create(FhrdReligionMst fhrdReligionMst);

    void edit(FhrdReligionMst fhrdReligionMst);

    void remove(FhrdReligionMst fhrdReligionMst);

    FhrdReligionMst find(Object id);

    List<FhrdReligionMst> findAll();

    List<FhrdReligionMst> findRange(int[] range);

    int count();

    public List<FhrdReligionMst> findAll(Integer p_oum_unit_srno);
}
