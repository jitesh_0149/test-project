/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.entities.FhrdRuleGroupCategory;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdRuleGroupCategoryFacade extends AbstractFacade<FhrdRuleGroupCategory> implements FhrdRuleGroupCategoryFacadeLocal {

    
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdRuleGroupCategoryFacade() {
        super(FhrdRuleGroupCategory.class);
    }

    @Override
    public List<FhrdRuleGroupCategory> findAll(Integer p_oum_unit_srno, Date p_start_date, Date p_end_date) {
        String v_start_date = DateTimeUtility.ChangeDateFormat(p_start_date, null);
        String v_end_date = DateTimeUtility.ChangeDateFormat(p_end_date, null);
        String v_query = "SELECT * FROM FHRD_RULE_GROUP_CATEGORY"
                + " WHERE OUM_UNIT_SRNO IN (OU_PACK.GET_OUM_UNIT_SRNO('FHRD_RULE_GROUP_CATEGORY'," + p_oum_unit_srno + "))";
        if (p_end_date != null) {
            v_query += " AND START_DATE<= '" + v_end_date + "'";
        }
        if (p_start_date != null) {
            v_query += " AND (END_DATE>= '" + v_start_date + "'  OR END_DATE IS NULL) ";
        }
        v_query += " ORDER BY CAT_DESC";        
        return em.createNativeQuery(v_query, FhrdRuleGroupCategory.class).getResultList();
    }
}
