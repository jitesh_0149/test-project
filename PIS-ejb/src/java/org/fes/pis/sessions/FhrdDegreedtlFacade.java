/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdDegreedtl;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdDegreedtlFacade extends AbstractFacade<FhrdDegreedtl> implements FhrdDegreedtlFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdDegreedtlFacade() {
        super(FhrdDegreedtl.class);
    }

    @Override
    public List<FhrdDegreedtl> findSubjectList(BigDecimal p_dgrm_srg_key, Integer p_native_oum) {
        return em.createNativeQuery(
                " select * from fhrd_degreedtl"
                + " where dgrm_srg_key=" + p_dgrm_srg_key + ""
                + " and OUM_UNIT_SRNO=" + p_native_oum, FhrdDegreedtl.class).getResultList();
    }

    @Override
    public boolean isSubjectValid(Integer p_oum_unit_srno, String p_dgrm_unique_srno, String p_subject_code, String p_subject_name) {

        BigDecimal v_total_count = (BigDecimal) em.createNativeQuery(" SELECT COUNT(*) FROM FHRD_DEGREEDTL "
                + " WHERE"
                //+ " (OUM_UNIT_SRNO=OU_PACK.GET_OUM_UNIT_SRNO('FHRD_DEGREEDTL'," + p_oum_unit_srno + ") "
                //+ " AND SUBJECT_CODE='" + p_subject_code + "'"
                //+ " AND DGRM_SRG_KEY=" + p_dgrm_unique_srno + ") "
                //+ " OR "
                + " (OUM_UNIT_SRNO=OU_PACK.GET_OUM_UNIT_SRNO('FHRD_DEGREEMST'," + p_oum_unit_srno + ") "
                + " AND SUBJECT_NAME='" + p_subject_name + "')  AND DGRM_SRG_KEY=" + p_dgrm_unique_srno).getSingleResult();
        return v_total_count.equals(BigDecimal.ZERO);
    }

    @Override
    public List<FhrdDegreedtl> findAll(Integer p_oum_unit_srno, BigDecimal p_dgrm_unique_srno, String p_subject_name) {
        String v_query = " SELECT * FROM FHRD_DEGREEDTL "
                + " WHERE OUM_UNIT_SRNO=OU_PACK.GET_OUM_UNIT_SRNO('FHRD_DEGREEDTL'," + p_oum_unit_srno + ") ";
        if (p_subject_name != null) {
            v_query += " AND SUBJECT_NAME='" + p_subject_name + "'";
        }
        if (p_dgrm_unique_srno != null) {
            v_query += "AND DGRM_SRG_KEY=" + p_dgrm_unique_srno;
        }
        return em.createNativeQuery(v_query, FhrdDegreedtl.class).getResultList();

    }
}
