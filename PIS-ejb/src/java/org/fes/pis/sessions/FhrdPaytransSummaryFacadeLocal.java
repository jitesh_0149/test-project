/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdPaytransSummary;

/**
 *
 * @author anu
 */
@Local
public interface FhrdPaytransSummaryFacadeLocal {

    void create(FhrdPaytransSummary fhrdPaytransSummary);

    void edit(FhrdPaytransSummary fhrdPaytransSummary);

    void remove(FhrdPaytransSummary fhrdPaytransSummary);

    FhrdPaytransSummary find(Object id);

    List<FhrdPaytransSummary> findAll();

    List<FhrdPaytransSummary> findRange(int[] range);

    int count();

    public List<FhrdPaytransSummary> findToDisburse(String p_user_id_list);
}
