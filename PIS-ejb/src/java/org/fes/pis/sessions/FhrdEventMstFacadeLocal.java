/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdEventMst;

/**
 *
 * @author anu
 */
@Local
public interface FhrdEventMstFacadeLocal {

    void create(FhrdEventMst fhrdEventMst);

    void edit(FhrdEventMst fhrdEventMst);

    void remove(FhrdEventMst fhrdEventMst);

    FhrdEventMst find(Object id);

    List<FhrdEventMst> findAll();

    List<FhrdEventMst> findRange(int[] range);

    int count();

    public List<FhrdEventMst> findAll_ByOrder();
    
    public List<FhrdEventMst> find_Detail_List(BigDecimal p_em_srg_key);
    
    public FhrdEventMst findByEventName(String p_event_name);
    
    public List<FhrdEventMst> findAll(Integer p_org_unit_srno);
    
}
