/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.BmwpBudgetMst;

/**
 *
 * @author anu
 */
@Local
public interface BmwpBudgetMstFacadeLocal {

    void create(BmwpBudgetMst bmwpBudgetMst);

    void edit(BmwpBudgetMst bmwpBudgetMst);

    void remove(BmwpBudgetMst bmwpBudgetMst);

    BmwpBudgetMst find(Object id);

    List<BmwpBudgetMst> findAll();

    List<BmwpBudgetMst> findRange(int[] range);

    int count();
    
}
