/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.entities.FhrdContractMst;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdContractMstFacade extends AbstractFacade<FhrdContractMst> implements FhrdContractMstFacadeLocal {

    
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdContractMstFacade() {
        super(FhrdContractMst.class);
    }

    @Override
    public List<FhrdContractMst> findContractListOuwise(Integer p_oum_unit_srno, String p_contract_type) {
        return em.createNativeQuery(
                " SELECT * FROM FHRD_CONTRACT_MST"
                + " where OUM_UNIT_SRNO=OU_PACK.GET_OUM_UNIT_SRNO('FHRD_CONTRACT_MST'," + p_oum_unit_srno + ")"
                + " and contract_category='" + p_contract_type + "'", FhrdContractMst.class).getResultList();
    }

    @Override
    public List<FhrdContractMst> findContractListOuwise(Integer p_oum_unit_srno, String p_contract_type, Date p_start_date, Date p_end_date) {
        String v_start_date = DateTimeUtility.ChangeDateFormat(p_start_date, null);
        String v_end_date = DateTimeUtility.ChangeDateFormat(p_end_date, null);
        String v_query = " SELECT * FROM FHRD_CONTRACT_MST"
                + " where OUM_UNIT_SRNO=OU_PACK.GET_OUM_UNIT_SRNO('FHRD_CONTRACT_MST'," + p_oum_unit_srno + ")"
                + " and contract_category='" + p_contract_type + "' ";
        if (p_end_date != null) {
            v_query += " AND START_DATE<= '" + v_start_date + "'";
        }
        if (p_start_date != null) {
            v_query += " AND (END_DATE>= '" + v_end_date + "'  OR END_DATE IS NULL) ";
        }
        v_query += " ORDER BY CONTRACT_DESC";
//        System.out.println("query "+v_query);
        return em.createNativeQuery(v_query, FhrdContractMst.class).getResultList();
    }

    @Override
    public List<FhrdContractMst> findAll(Integer p_oum_unit_srno) {
        return em.createNativeQuery("SELECT * FROM FHRD_CONTRACT_MST "
                + " WHERE OUM_UNIT_SRNO = OU_PACK.GET_OUM_UNIT_SRNO('FHRD_CONTRACT_MST'," + p_oum_unit_srno + ")"
                + " ORDER BY CONTRACT_DESC", FhrdContractMst.class).getResultList();
    }

    @Override
    public FhrdContractMst find_contract_desc(Integer p_sys_user, Integer p_contract_Srno) {
        return (FhrdContractMst) em.createNativeQuery(
                " select Contract_Desc from"
                + " Fhrd_Contract_Mst where"
                + " contract_Srno=" + p_contract_Srno + " and oum_unit_srno=" + p_sys_user, FhrdContractMst.class).getSingleResult();
    }

    @Override
    public List<FhrdContractMst> findAllAlternateContracts(Integer p_alt_srno, Integer p_oum_unit_srno) {
        return em.createNativeQuery("SELECT * "
                + " FROM FHRD_CONTRACT_MST WHERE cm_alt_srno=" + p_alt_srno
                + " AND OUM_UNIT_SRNO IN (OU_PACK.GET_OUM_UNIT_SRNO('FHRD_CONTRACT_MST'," + p_oum_unit_srno + "))"
                + " ORDER BY CONTRACT_DESC", FhrdContractMst.class).getResultList();
    }

    @Override
    public BigDecimal findMaxAltSrno() {
        return (BigDecimal) em.createNativeQuery("SELECT MAX(CM_ALT_SRNO) FROM FHRD_CONTRACT_MST").getSingleResult();
    }

    @Override
    public boolean isContractDescExists(Integer p_oum_unit_srno, String p_contract_desc, Date p_start_date, Date p_end_date) {
        String v_start_date = DateTimeUtility.ChangeDateFormat(p_start_date, null);
        String v_end_date = DateTimeUtility.ChangeDateFormat(p_end_date, null);
        String v_query = "SELECT COUNT(*)  FROM FHRD_CONTRACT_MST "
                + " WHERE CONTRACT_DESC= '" + p_contract_desc + "'"
                + " AND OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_CONTRACT_MST'," + p_oum_unit_srno + ")"
                + " AND ((END_DATE>='" + v_start_date + "' OR END_DATE IS NULL) ";
        if (p_end_date != null) {
            v_query += " AND START_DATE<='" + v_end_date + "') ";
        } else {
            v_query += ")";
        }
        BigDecimal v_total_count = (BigDecimal) em.createNativeQuery(v_query).getSingleResult();
        return !v_total_count.equals(BigDecimal.ZERO);
    }

    @Override
    public List<FhrdContractMst> findSupplementContractListOuwise(Integer p_oum_unit_srno, String p_contract_Altlist) {
//        System.out.println("alt "+p_contract_Altlist);
        String v_query = " SELECT * FROM FHRD_CONTRACT_MST"
                + " where OUM_UNIT_SRNO=OU_PACK.GET_OUM_UNIT_SRNO('FHRD_CONTRACT_MST'," + p_oum_unit_srno + ")"
                + " and contract_category='S'";
        if (!p_contract_Altlist.equals("")) {
            v_query += " and CM_ALT_SRNO NOT IN (" + p_contract_Altlist + ")";
        }
        v_query += " ORDER BY CONTRACT_DESC,END_DATE";
//        System.out.println("query: "+v_query);
        return em.createNativeQuery(v_query, FhrdContractMst.class).getResultList();
    }

}
