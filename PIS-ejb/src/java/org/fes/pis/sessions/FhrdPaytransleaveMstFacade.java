/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdPaytransleaveMst;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdPaytransleaveMstFacade extends AbstractFacade<FhrdPaytransleaveMst> implements FhrdPaytransleaveMstFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdPaytransleaveMstFacade() {
        super(FhrdPaytransleaveMst.class);
    }

    @Override
    public String findLastAttendancePeriod(Integer p_user_id) {
        return (String) em.createNativeQuery("SELECT TO_CHAR(MAX(ORG_ATTENDANCE_FROM_DATE),'DD-MON-YYYY')||' TO '||TO_CHAR(MAX(ORG_ATTENDANCE_TO_DATE),'DD-MON-YYYY') "
                + " FROM   FHRD_PAYTRANSLEAVE_MST "
                + " WHERE  USER_ID=" + p_user_id).getSingleResult();
    }
}
