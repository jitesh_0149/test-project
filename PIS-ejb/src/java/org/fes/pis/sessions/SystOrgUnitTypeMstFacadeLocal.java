/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.SystOrgUnitTypeMst;

/**
 *
 * @author anu
 */
@Local
public interface SystOrgUnitTypeMstFacadeLocal {

    void create(SystOrgUnitTypeMst systOrgUnitTypeMst);

    void edit(SystOrgUnitTypeMst systOrgUnitTypeMst);

    void remove(SystOrgUnitTypeMst systOrgUnitTypeMst);

    SystOrgUnitTypeMst find(Object id);

    List<SystOrgUnitTypeMst> findAll();

    List<SystOrgUnitTypeMst> findRange(int[] range);

    int count();
    
}
