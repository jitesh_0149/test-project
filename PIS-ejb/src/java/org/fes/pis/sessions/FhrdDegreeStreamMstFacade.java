/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdDegreeStreamMst;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdDegreeStreamMstFacade extends AbstractFacade<FhrdDegreeStreamMst> implements FhrdDegreeStreamMstFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdDegreeStreamMstFacade() {
        super(FhrdDegreeStreamMst.class);
    }

    @Override
    public List<FhrdDegreeStreamMst> findAll(Integer p_oum_unit_srno) {
        return em.createNativeQuery("SELECT * FROM FHRD_DEGREE_STREAM_MST "
                + " WHERE OUM_UNIT_SRNO IN (OU_PACK.GET_OUM_UNIT_SRNO('FHRD_DEGREE_STREAM_MST'," + p_oum_unit_srno + "))"
                + " ORDER BY DSM_DESCRIPTION", FhrdDegreeStreamMst.class).getResultList();

    }
}
