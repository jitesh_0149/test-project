/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdExperience;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdExperienceFacade extends AbstractFacade<FhrdExperience> implements FhrdExperienceFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdExperienceFacade() {
        super(FhrdExperience.class);
    }

    @Override
    public List<FhrdExperience> findExperienceUseridwise(Integer p_userid) {
        return em.createNativeQuery("SELECT * FROM FHRD_EXPERIENCE "
                + " WHERE USER_ID=" + p_userid
                + " ORDER BY FROMDATE", FhrdExperience.class).getResultList();
    }

    @Override
    public List<String> findAllCompanies(Integer p_oum_unit_srno) {
        return (List<String>) em.createNativeQuery("SELECT DISTINCT COMPANYNAME  FROM FHRD_EXPERIENCE WHERE OUM_UNIT_SRNO IN (select * from table(ou_pack.get_sub_ou(ou_pack.get_org_srno("+p_oum_unit_srno+"),'d','y'))) ORDER BY COMPANYNAME").getResultList();
    }

    @Override
    public List<String> findAllDesignations(Integer p_oum_unit_srno) {
        return (List<String>) em.createNativeQuery("SELECT DISTINCT DESIGNATION  FROM FHRD_EXPERIENCE WHERE OUM_UNIT_SRNO IN  (select * from table(ou_pack.get_sub_ou(ou_pack.get_org_srno("+p_oum_unit_srno+"),'d','y'))) ORDER BY DESIGNATION").getResultList();
    }
}
