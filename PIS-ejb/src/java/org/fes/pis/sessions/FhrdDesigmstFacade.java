/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdDesigmst;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdDesigmstFacade extends AbstractFacade<FhrdDesigmst> implements FhrdDesigmstFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdDesigmstFacade() {
        super(FhrdDesigmst.class);
    }

    @Override
    public Integer getMaxSrno_fhrdDesigMst() {
        Integer v_maxsrno =
                (Integer) em.createNativeQuery(
                " select NVL(max(desig_SRNO),0) SRNO"
                + " from FHRD_DESIGMSt  order by createdt").getSingleResult();
        return v_maxsrno;
    }

    @Override
    public List<FhrdDesigmst> findAllDesignation(Integer p_oum_unit_srno) {
        return em.createNativeQuery("SELECT * FROM FHRD_DESIGMST "
                + " WHERE OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_DESIGMST'," + p_oum_unit_srno + ") "
                + " and DESIG_SRNO not in (0) "
                + " AND (END_DATE IS NULL OR END_DATE> SYSDATE) order by DESIG_DESC", FhrdDesigmst.class).getResultList();
    }

    @Override
    public List<FhrdDesigmst> findAll(Integer p_oum_unit_srno) {
        return em.createNativeQuery("SELECT * FROM FHRD_DESIGMST "
                + " WHERE OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_DESIGMST'," + p_oum_unit_srno + ")"
                + " ORDER BY DESIG_DESC", FhrdDesigmst.class).getResultList();
    }

    @Override
    public Date findMaxTransactionDate(BigDecimal p_dsgm_srg_key) {
        return (Date) em.createNativeQuery("SELECT MAX(END_DATE) FROM FHRD_EMP_CONTRACT_MST"
                + " WHERE USER_ID IN (SELECT USER_ID FROM FHRD_EMPMST WHERE DSGM_SRG_KEY=" + p_dsgm_srg_key + ") ").getSingleResult();
    }

    @Override
    public List<FhrdDesigmst> findAll(String p_dsgm_srg_key, Integer p_oum_unit_srno) {
        return em.createNativeQuery("SELECT * FROM FHRD_DESIGMST"
                + " WHERE DSGM_SRG_KEY NOT IN (" + p_dsgm_srg_key + ")"
                + " AND OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_DESIGMST'," + p_oum_unit_srno + ")"
                + " ORDER BY DESIG_DESC", FhrdDesigmst.class).getResultList();

    }
}
