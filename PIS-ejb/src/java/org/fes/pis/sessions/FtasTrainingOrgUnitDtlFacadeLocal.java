/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FtasTrainingOrgUnitDtl;

/**
 *
 * @author anu
 */
@Local
public interface FtasTrainingOrgUnitDtlFacadeLocal {

    void create(FtasTrainingOrgUnitDtl ftasTrainingOrgUnitDtl);

    void edit(FtasTrainingOrgUnitDtl ftasTrainingOrgUnitDtl);

    void remove(FtasTrainingOrgUnitDtl ftasTrainingOrgUnitDtl);

    FtasTrainingOrgUnitDtl find(Object id);

    List<FtasTrainingOrgUnitDtl> findAll();

    List<FtasTrainingOrgUnitDtl> findRange(int[] range);

    int count();
    
    public FtasTrainingOrgUnitDtl findSelectedTrainingOrgDtl(BigDecimal p_trann_srg_key, Integer p_oum_unit_srno);
    
}
