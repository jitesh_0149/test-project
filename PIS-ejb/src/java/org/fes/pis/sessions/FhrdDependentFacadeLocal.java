/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdDependent;

/**
 *
 * @author anu
 */
@Local
public interface FhrdDependentFacadeLocal {

    void create(FhrdDependent fhrdDependent);

    void edit(FhrdDependent fhrdDependent);

    void remove(FhrdDependent fhrdDependent);

    FhrdDependent find(Object id);

    List<FhrdDependent> findAll();

    List<FhrdDependent> findRange(int[] range);

    int count();

    public List<FhrdDependent> findUseridwise(Integer p_userid);

    public List<String> findAllOccupation(Integer p_oum_unit_srno);
}
