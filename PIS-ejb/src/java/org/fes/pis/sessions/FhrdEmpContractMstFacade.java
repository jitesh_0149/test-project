/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.custom_entities.Date_Duration;
import org.fes.pis.entities.FhrdEmpContractMst;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdEmpContractMstFacade extends AbstractFacade<FhrdEmpContractMst> implements FhrdEmpContractMstFacadeLocal {

    DateTimeUtility DateTimeUtility=new DateTimeUtility();
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdEmpContractMstFacade() {
        super(FhrdEmpContractMst.class);
    }

    @Override
    public List<FhrdEmpContractMst> findAllContractEmpwise(Integer p_userid, String p_contract_type) {
        String v_query = "select a.* from fhrd_emp_contract_mst a, fhrd_contract_mst b "
                + " where a.user_id=" + p_userid
                //     + " and (a.end_date is null or a.end_date >= sysdate)"
                + " and a.cm_srg_key=b.cm_srg_key"
                + " and b.contract_category in (" + p_contract_type + ")";
        return em.createNativeQuery(v_query, FhrdEmpContractMst.class).getResultList();
    }

    @Override
    public List<FhrdEmpContractMst> findLatestContractEmpwise(Integer p_userid, String p_contract_category) {
        String v_query = " SELECT A.*"
                + " FROM FHRD_EMP_CONTRACT_MST A,"
                + "  FHRD_CONTRACT_MST B"
                + " WHERE A.USER_ID         =" + p_userid
                + " AND A.CM_SRG_KEY        = B.CM_SRG_KEY"
                + " AND B.CONTRACT_CATEGORY ='" + p_contract_category + "'"
                + " AND A.END_DATE         IN"
                + "  (SELECT MAX(END_DATE) FROM FHRD_EMP_CONTRACT_MST WHERE USER_ID=" + p_userid + "  )";
        return em.createNativeQuery(v_query, FhrdEmpContractMst.class).getResultList();

    }

    @Override
    public List<FhrdEmpContractMst> findAllContractEmpwiseDatewise(Integer p_userid, String p_start_date, String p_end_date, String p_sudden_enddate) {
//        String v_start_date = DateTimeUtility.ChangeDateFormat(p_strat_date, null);
//        String v_end_date = DateTimeUtility.ChangeDateFormat(p_end_date, null);
//        String v_sudden_enddate = DateTimeUtility.ChangeDateFormat(p_sudden_enddate, null);
        String v_query = " SELECT *"
                + " FROM FHRD_EMP_CONTRACT_MST"
                + " WHERE USER_ID    =" + p_userid
                + "  AND START_DATE >= '" + p_start_date + "'"
                + "  AND END_DATE   <='" + p_end_date + "'"
                + "  AND END_DATE > '" + p_sudden_enddate + "'";
        return em.createNativeQuery(v_query, FhrdEmpContractMst.class).getResultList();

    }

    @Override
    public List<FhrdEmpContractMst> findAllByMainContract(Integer p_user_id, String p_start_date, String p_end_date) {
        String v_query = "SELECT  * "
                + " FROM    FHRD_EMP_CONTRACT_MST"
                + " WHERE   USER_ID=" + p_user_id
                + " AND     (START_DATE>='" + p_start_date + "' AND START_DATE<='" + p_end_date + "')"
                + " AND     (END_DATE>='" + p_start_date + "' AND END_DATE<='" + p_end_date + "')";
        return em.createNativeQuery(v_query, FhrdEmpContractMst.class).getResultList();
    }

    @Override
    public List<FhrdEmpContractMst> findMaxContractPeriod(Integer p_user_id, String p_cm_srg_key) {
        String v_query = "SELECT *"
                + " FROM FHRD_EMP_CONTRACT_MST"
                + " WHERE CM_SRG_KEY = " + p_cm_srg_key
                + "  AND USER_ID     = " + p_user_id
                + "  AND END_DATE   IN"
                + "     (SELECT MAX(END_DATE)"
                + "     FROM FHRD_EMP_CONTRACT_MST"
                + "     WHERE USER_ID   =" + p_user_id
                + "      AND CM_SRG_KEY = " + p_cm_srg_key
                + "     )";
        return em.createNativeQuery(v_query, FhrdEmpContractMst.class).getResultList();
    }

    @Override
    public FhrdEmpContractMst findForRelTermNoticePeriod(Integer p_user_id, Date p_date) {
        String v_query = " SELECT * "
                + " FROM   FHRD_EMP_CONTRACT_MST "
                + " WHERE  (USER_ID,START_DATE) "
                + " IN ( "
                + " SELECT USER_ID,MAX(START_DATE) "
                + " FROM   FHRD_EMP_CONTRACT_MST "
                + " WHERE  USER_ID=" + p_user_id
                + " AND    CONTRACT_CATEGORY='M' "
                + " AND    '" + DateTimeUtility.ChangeDateFormat(p_date, null) + "' BETWEEN START_DATE AND END_DATE "
                + " GROUP BY USER_ID) "
                + " AND   CONTRACT_CATEGORY='M' ";
        return (FhrdEmpContractMst) em.createNativeQuery(v_query, FhrdEmpContractMst.class).getSingleResult();
    }

    @Override
    public List<FhrdEmpContractMst> findAllToRelieveTerminate(Integer p_user_id, String p_end_date) {
        String v_query = "SELECT *"
                + " FROM FHRD_EMP_CONTRACT_MST"
                + " WHERE  USER_ID     = " + p_user_id
                + "  AND END_DATE >  '" + p_end_date + "'";

        return em.createNativeQuery(v_query, FhrdEmpContractMst.class).getResultList();
    }

    @Override
    public Date_Duration findMinMaxContractDate(Integer p_user_id) {
        String v_min_max = em.createNativeQuery("SELECT TO_CHAR(MIN(START_DATE),'DD-MON-YYYY')||'###'||TO_CHAR(MAX(END_DATE),'DD-MON-YYYY') MIN_MAX_DATE "
                + " FROM FHRD_EMP_CONTRACT_MST "
                + " WHERE USER_ID=" + p_user_id).getSingleResult().toString();
        Date_Duration date_Duration = new Date_Duration();
        if (!v_min_max.equals("###")) {
            date_Duration.setStart_date(DateTimeUtility.stringToDate(v_min_max.split("###")[0], null));
            date_Duration.setEnd_date(DateTimeUtility.stringToDate(v_min_max.split("###")[1], null));
        }
        return date_Duration;
    }
}
