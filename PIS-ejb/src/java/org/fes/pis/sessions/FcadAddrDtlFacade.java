/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FcadAddrDtl;

/**
 *
 * @author anu
 */
@Stateless
public class FcadAddrDtlFacade extends AbstractFacade<FcadAddrDtl> implements FcadAddrDtlFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FcadAddrDtlFacade() {
        super(FcadAddrDtl.class);
    }

    @Override
    public FcadAddrDtl findActive(BigDecimal p_am_unique_srno) {
        return (FcadAddrDtl) em.createNativeQuery(
                " SELECT * FROM FCAD_ADDR_DTL"
                + " WHERE AD_AM_UNIQUE_SRNO = " + p_am_unique_srno
                + " AND AD_END_DATE IS NULL", FcadAddrDtl.class).getSingleResult();
    }
}
