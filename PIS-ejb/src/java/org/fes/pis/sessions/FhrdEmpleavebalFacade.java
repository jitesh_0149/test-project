/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.entities.FhrdEmpleavebal;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdEmpleavebalFacade extends AbstractFacade<FhrdEmpleavebal> implements FhrdEmpleavebalFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdEmpleavebalFacade() {
        super(FhrdEmpleavebal.class);
    }

    @Override
    public BigDecimal getEmpParentLeaveBal(Integer p_userid, String p_leavecd, Integer p_rulesrno) {
        return (BigDecimal) em.createNativeQuery(
                " select current_bal/Ftas_Attdcalc_TEMP_1.Get_Leave_Ratio(" + p_userid.toString() + ", opening_date, closing_date, '" + p_leavecd + "', LeaveCd ) balance "
                + " from fhrd_empleavebal where rulesrno=" + p_rulesrno).getSingleResult();
    }

    @Override
    public String getEmpLeaveBal(Integer p_userid, String p_leavecd) {
        return (String) em.createNativeQuery(
                " select CURRENT_BAL||','||APPLI_PIPE_BAL||','||CANC_PIPE_BAL"
                + " from FHRD_EMPLEAVEBAL where user_id= " + p_userid
                + " and leavecd='" + p_leavecd + "'"
                + " and (TO_CHAR(opening_date,'yyyy')=TO_CHAR(sysdate,'yyyy')"
                + " or TO_CHAR(closing_date,'yyyy')=TO_CHAR(sysdate,'yyyy') ) ").getSingleResult();
    }

    @Override
    public List<FhrdEmpleavebal> findAllFromLeave(Integer p_oum_unit_srno, Integer p_user_id, boolean p_nullClosingBalance, String p_considerAsAttendanceFlag, boolean p_timeSensitive, String p_leaveFlag, String p_virtualLeave, String p_parenteable, String p_ttrParenteable, String p_applicationRequiredFlag, String p_exceptEmplbSrgKey, boolean p_halfApplicable, Date p_clubStartDate, String p_encashment, BigDecimal p_lrSrgKey, Date p_from_date, Date p_to_date, String p_user_managed) {
        String v_query = " SELECT A.* FROM FHRD_EMPLEAVEBAL A,fhrd_EMPLEAVERULE B,FTAS_LEAVEMST C "
                + " WHERE  A.ELR_SRG_KEY   =B.ELR_SRG_KEY "
                + " AND B.LM_SRG_KEY=C.LM_SRG_KEY "
                + " AND A.user_id = " + p_user_id;
        if (p_nullClosingBalance) {
            v_query += " AND  A.CLOSING_BAL IS NULL ";
        }
        if (p_considerAsAttendanceFlag != null) {
            v_query += " AND C.CONSIDER_AS_ATTENDANCE_FLAG='" + p_considerAsAttendanceFlag + "' ";
        }
//        if (p_timeSensitive) {
//            //if (p_clubStartDate == null) {
//           
//            v_query += " AND A.OPENING_DATE<=(SYSDATE) ";
//            //} 
//            //else {
//            //    v_query += " AND (A.OPENING_DATE<='" + DateTimeUtility.ChangeDateFormat(p_clubStartDate, null) + "'"
//            //            + " AND A.CLOSING_DATE>='" + DateTimeUtility.ChangeDateFormat(p_clubStartDate, null) + "') ";
//            //}
//        }
        if (p_from_date != null && p_to_date != null) {
            v_query += " AND ("
                    + "'" + DateTimeUtility.ChangeDateFormat(p_from_date, null) + "' BETWEEN A.OPENING_DATE AND A.CLOSING_DATE"
                    + " OR "
                    + "'" + DateTimeUtility.ChangeDateFormat(p_to_date, null) + "' BETWEEN A.OPENING_DATE AND A.CLOSING_DATE)";
        }
        if (p_lrSrgKey != null) {
            v_query += " AND A.LR_SRG_KEY=" + p_lrSrgKey;
        }
        if (p_leaveFlag != null) {
            v_query += " AND C.LEAVE_FLAG='" + p_leaveFlag + "'";
        }
        if (p_exceptEmplbSrgKey != null) {
            v_query += " AND A.EMPLB_SRG_KEY NOT IN (" + p_exceptEmplbSrgKey + ")";
        }
        if (p_user_managed != null) {
            v_query += " AND B.USER_ENTRY_FLAG='" + p_user_managed + "'";
        }
        if (p_halfApplicable) {
            v_query += " AND B.HALFDAY_APPLICABLE='Y'";
        }
        if (p_encashment != null && p_encashment.equals("Y")) {
            v_query += " AND B.ENCASHMENT='" + p_encashment + "'";
        }
        if (p_virtualLeave != null) {
            v_query += " AND C.VIRTUAL_LEAVE='" + p_virtualLeave + "'";
        }
        if (p_parenteable != null) {
            v_query += " AND C.PARENTABLE ='" + p_parenteable + "' ";
        }
        if (p_ttrParenteable != null) {
            v_query += " AND C.T_TR_PARENT ='" + p_ttrParenteable + "' ";
        }
        if (p_applicationRequiredFlag != null) {
            v_query += " AND C.APPLICATION_REQUIRED='" + p_applicationRequiredFlag + "'";
        }
        v_query += " AND A.OPENING_DATE <= (SELECT "
                + " TO_DATE('31-DEC-'||MAX(HOLI_YEAR),'DD-MON-YYYY') FROM FTAS_HOLIDAY_MST"
                + " WHERE OUM_UNIT_SRNO = OU_PACK.GET_OUM_UNIT_SRNO('FTAS_HOLIDAY_MST'," + p_oum_unit_srno + ")"
                + " AND TOTAL_AWAITED_APPROVAL=0)"
                + " ORDER BY A.LEAVECD,A.OPENING_DATE";



//        SELECT A.* FROM FHRD_EMPLEAVEBAL A,fhrd_EMPLEAVERULE B,FTAS_LEAVEMST C  
//        WHERE  A.ELR_SRG_KEY   =B.ELR_SRG_KEY  AND B.LM_SRG_KEY=C.LM_SRG_KEY  
//         AND A.user_id = 324 AND  A.CLOSING_BAL IS NULL  
//         AND C.CONSIDER_AS_ATTENDANCE_FLAG='N'  
//         AND (A.OPENING_DATE<=(SYSDATE) 
//         AND A.CLOSING_DATE>=(SYSDATE))  
//        AND C.LEAVE_FLAG='Y' AND C.APPLICATION_REQUIRED='Y'

        return em.createNativeQuery(v_query, FhrdEmpleavebal.class).getResultList();
    }

    @Override
    public BigDecimal checkEmpLeaveClosingBal(Integer p_userid) {
        return (BigDecimal) em.createNativeQuery(
                " SELECT COUNT(*) FROM FHRD_EMPLEAVEBAL"
                + " WHERE CLOSING_BAL IS NULL"
                + " AND USER_ID= " + p_userid).getSingleResult();
    }

    @Override
    public boolean isUserTransactionPending(Integer p_user_id) {
        BigDecimal v_count = (BigDecimal) em.createNativeQuery("SELECT COUNT(*) FROM FHRD_EMPLEAVEBAL "
                + " WHERE USER_ID=" + p_user_id
                + " AND   CLOSING_BAL IS NULL").getSingleResult();
        return v_count.equals(BigDecimal.ZERO) ? false : true;

    }

    @Override
    public List<FhrdEmpleavebal> findAllForEncashmentApplication(Integer p_user_id) {
        String v_query = " SELECT *"
                + " FROM  (SELECT * "
                + " FROM FHRD_EMPLEAVEBAL "
                + " WHERE USER_ID=" + p_user_id
                + " AND   ELR_SRG_KEY IN (SELECT ELR_SRG_KEY FROM FHRD_EMPLEAVERULE"
                + " WHERE USER_ID=" + p_user_id
                + " AND   ENCASHMENT='Y'"
                + " AND   ENCASH_FREQ_PERIOD_UNIT<>'R')"
                + " AND   CLOSING_BAL IS NULL"
                + " ORDER BY OPENING_DATE)"
                + " WHERE ROWNUM=1";
        return em.createNativeQuery(v_query, FhrdEmpleavebal.class).getResultList();

    }
}
