/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.BmwpApBudgetMst;

/**
 *
 * @author anu
 */
@Local
public interface BmwpApBudgetMstFacadeLocal {

    void create(BmwpApBudgetMst bmwpApBudgetMst);

    void edit(BmwpApBudgetMst bmwpApBudgetMst);

    void remove(BmwpApBudgetMst bmwpApBudgetMst);

    BmwpApBudgetMst find(Object id);

    List<BmwpApBudgetMst> findAll();

    List<BmwpApBudgetMst> findRange(int[] range);

    int count();
    
    public List<BmwpApBudgetMst> findAllAPwise(Integer p_apl_unique_srno);
}
