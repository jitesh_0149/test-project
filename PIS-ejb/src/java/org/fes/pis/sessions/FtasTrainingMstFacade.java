/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FtasTrainingMst;
import org.fes.pis.entities.FtasTrainingOrgUnitDtl;

/**
 *
 * @author anu
 */
@Stateless
public class FtasTrainingMstFacade extends AbstractFacade<FtasTrainingMst> implements FtasTrainingMstFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FtasTrainingMstFacade() {
        super(FtasTrainingMst.class);
    }

    @Override
    public List<FtasTrainingMst> getTrainingList(Integer p_user_id, BigDecimal p_lm_srg_key, Integer p_oum_unit_srno) {
        String v_query = "select * from ftas_training_MST a where "
                + " FROM_DATE >= ADD_MONTHS(SYSDATE,-2)"
                + " AND A.TRANN_SRG_KEY IN"
                + " (SELECT TRANN_SRG_KEY FROM FTAS_TRAINING_ORG_UNIT_DTL"
                + " WHERE SYST_ORG_UNIT_SRNO IN"
                + " (select * from table(ou_pack.get_sub_ou(" + p_oum_unit_srno + ",'u','y')))) "
                + " AND FROM_DATE >= (SELECT OPENING_DATE "
                + " FROM FHRD_EMPLEAVEBAL WHERE USER_ID = " + p_user_id + " AND LM_SRG_KEY=" + p_lm_srg_key + ")  "
                + " AND FROM_DATE >= ("
                + "     SELECT MAX(TO_DATE)"
                + "     FROM FTAS_LOCK_DTL WHERE USER_ID = " + p_user_id + ")     "
                + " order by 1";
//        System.out.println("query "+v_query);
        return em.createNativeQuery(v_query, FtasTrainingMst.class).getResultList();
    }
}
