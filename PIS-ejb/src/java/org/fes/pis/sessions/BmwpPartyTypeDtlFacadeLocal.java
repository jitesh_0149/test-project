/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.BmwpPartyTypeDtl;

/**
 *
 * @author anu
 */
@Local
public interface BmwpPartyTypeDtlFacadeLocal {

    void create(BmwpPartyTypeDtl bmwpPartyTypeDtl);

    void edit(BmwpPartyTypeDtl bmwpPartyTypeDtl);

    void remove(BmwpPartyTypeDtl bmwpPartyTypeDtl);

    BmwpPartyTypeDtl find(Object id);

    List<BmwpPartyTypeDtl> findAll();

    List<BmwpPartyTypeDtl> findRange(int[] range);

    int count();
    
}
