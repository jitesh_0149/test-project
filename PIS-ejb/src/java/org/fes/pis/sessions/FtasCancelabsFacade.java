/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FtasCancelabs;

/**
 *
 * @author anu
 */
@Stateless
public class FtasCancelabsFacade extends AbstractFacade<FtasCancelabs> implements FtasCancelabsFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FtasCancelabsFacade() {
        super(FtasCancelabs.class);
    }

    @Override
    public List<FtasCancelabs> findAll_ApprovalList(Integer p_userid) {
        String v_query = " SELECT * FROM FTAS_CANCELABS "
                + " WHERE ACTNBY = " + p_userid
                + " AND APRVBY IS NULL ";
//        System.out.println("query: "+v_query);
        return em.createNativeQuery(v_query, FtasCancelabs.class).getResultList();
    }
}
