/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.entities.FhrdLeaverule;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdLeaveruleFacade extends AbstractFacade<FhrdLeaverule> implements FhrdLeaveruleFacadeLocal {

    
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdLeaveruleFacade() {
        super(FhrdLeaverule.class);
    }

    @Override
    public List<FhrdLeaverule> findParentRules(Integer p_oum_unit_srno, Date p_start_date, String p_rmgm_srg_key) {
        String v_date = DateTimeUtility.ChangeDateFormat(p_start_date, null);
        String v_query = " SELECT *"
                + " FROM FHRD_LEAVERULE"
                + " WHERE OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_LEAVERULE'," + p_oum_unit_srno + ")"
                + " AND RMGM_SRG_KEY     =" + p_rmgm_srg_key
                + " AND LR_SRG_KEY NOT  IN"
                + "  (SELECT PARENT_LR_SRG_KEY"
                + "  FROM FHRD_LEAVERULE"
                + "  WHERE OUM_UNIT_SRNO   IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_LEAVERULE'," + p_oum_unit_srno + ")"
                + "  AND PARENT_LR_SRG_KEY IS NOT NULL"
                + "  )"
                + " AND START_DATE <= '" + v_date + "'"
                + " AND (END_DATE  >= sysdate"
                + " OR END_DATE    IS NULL)"
                + " ORDER BY GROUP_LEAVE_DESC";
//        System.out.println("query " + v_query);
        return em.createNativeQuery(
                v_query, FhrdLeaverule.class).getResultList();
    }

    @Override
    public List<FhrdLeaverule> findAll(Integer p_oum_unit_srno) {
        return em.createNativeQuery("SELECT * FROM FHRD_LEAVERULE "
                + " WHERE OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_LEAVERULE'," + p_oum_unit_srno + ") "
                + " ORDER BY GROUP_LEAVE_DESC", FhrdLeaverule.class).getResultList();
    }

    @Override
    public List<FhrdLeaverule> findLeaveRuleContractwise(String p_user_id, List<BigDecimal> p_lst_cm_srg_key, Date p_start_date, Date p_end_date, String p_gender_flag) {
        String v_query = " SELECT RGR.RMGM_SRG_KEY FROM "
                + " FHRD_RULE_GROUP_RELATION RGR,FHRD_CONTRACT_GROUP_RELATION CGR "
                + " WHERE "
                + " RGR.RAGM_SRG_KEY=CGR.RAGM_SRG_KEY "
                + " AND CGR.CM_SRG_KEY IN (" + p_lst_cm_srg_key.toString().replace("[", "").replace("]", "") + ") ";
        List<BigDecimal> lst_rmgm_srg_key = em.createNativeQuery(v_query).getResultList();
        v_query = " SELECT * FROM FHRD_LEAVERULE "
                + " WHERE  LR_SRG_KEY IN (SELECT * FROM TABLE(FTAS_PACK.GET_LEAVE_RULE_RMGM_WISE( "
                + " " + lst_rmgm_srg_key.toString().replace("[", "'").replace("]", "'") + ", "
                + " '" + DateTimeUtility.ChangeDateFormat(p_start_date, null) + "', "
                + " '" + DateTimeUtility.ChangeDateFormat(p_end_date, null) + "', "
                + p_user_id + " ))) ";
//        System.out.println("query "+v_query);
        return em.createNativeQuery(v_query, FhrdLeaverule.class).getResultList();
    }
}
