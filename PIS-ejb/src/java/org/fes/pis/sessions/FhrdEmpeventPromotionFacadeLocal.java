/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdEmpeventPromotion;

/**
 *
 * @author anu
 */
@Local
public interface FhrdEmpeventPromotionFacadeLocal {

    void create(FhrdEmpeventPromotion fhrdEmpeventPromotion);

    void edit(FhrdEmpeventPromotion fhrdEmpeventPromotion);

    void remove(FhrdEmpeventPromotion fhrdEmpeventPromotion);

    FhrdEmpeventPromotion find(Object id);

    List<FhrdEmpeventPromotion> findAll();

    List<FhrdEmpeventPromotion> findRange(int[] range);

    int count();
    
}
