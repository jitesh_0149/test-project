/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdDegreeLevelMst;

/**
 *
 * @author anu
 */
@Local
public interface FhrdDegreeLevelMstFacadeLocal {

    void create(FhrdDegreeLevelMst fhrdDegreeLevelMst);

    void edit(FhrdDegreeLevelMst fhrdDegreeLevelMst);

    void remove(FhrdDegreeLevelMst fhrdDegreeLevelMst);

    FhrdDegreeLevelMst find(Object id);

    List<FhrdDegreeLevelMst> findAll();

    List<FhrdDegreeLevelMst> findRange(int[] range);

    int count();

    public List<FhrdDegreeLevelMst> findAll(Integer p_oum_unit_srno, boolean findByDate);
}
