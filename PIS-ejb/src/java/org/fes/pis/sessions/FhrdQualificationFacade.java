/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdQualification;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdQualificationFacade extends AbstractFacade<FhrdQualification> implements FhrdQualificationFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdQualificationFacade() {
        super(FhrdQualification.class);
    }

    @Override
    public List<FhrdQualification> findQualificationUseridwise(Integer p_userid) {
        return em.createNativeQuery("SELECT * FROM FHRD_QUALIFICATION "
                + " WHERE USER_ID=" + p_userid
                + " ORDER BY TO_DATE('1-'||NVL(PASS_MONTH,'JAN')||PASS_YEAR) DESC", FhrdQualification.class).getResultList();
    }

    @Override
    public List<String> findAllColleges(Integer p_oum_unit_srno) {
        return (List<String>) em.createNativeQuery("SELECT DISTINCT COLLEGE_NAME  FROM FHRD_QUALIFICATION WHERE OUM_UNIT_SRNO IN (select * from table(ou_pack.get_sub_ou(ou_pack.get_org_srno(" + p_oum_unit_srno + "),'d','y')))"
                + " AND COLLEGE_NAME IS NOT NULL  ORDER BY COLLEGE_NAME").getResultList();
    }
}
