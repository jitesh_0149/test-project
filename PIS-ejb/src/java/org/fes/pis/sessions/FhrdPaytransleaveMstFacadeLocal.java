/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdPaytransleaveMst;

/**
 *
 * @author anu
 */
@Local
public interface FhrdPaytransleaveMstFacadeLocal {

    void create(FhrdPaytransleaveMst fhrdPaytransleaveMst);

    void edit(FhrdPaytransleaveMst fhrdPaytransleaveMst);

    void remove(FhrdPaytransleaveMst fhrdPaytransleaveMst);

    FhrdPaytransleaveMst find(Object id);

    List<FhrdPaytransleaveMst> findAll();

    List<FhrdPaytransleaveMst> findRange(int[] range);

    int count();

    public String findLastAttendancePeriod(Integer p_user_id);
}
