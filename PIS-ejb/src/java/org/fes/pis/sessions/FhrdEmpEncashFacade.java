/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdEmpEncash;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdEmpEncashFacade extends AbstractFacade<FhrdEmpEncash> implements FhrdEmpEncashFacadeLocal {
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdEmpEncashFacade() {
        super(FhrdEmpEncash.class);
    }
    
    @Override
    public List<FhrdEmpEncash> findAllForApproval(String p_appr_auth, Integer p_oum_unit_srno) {
        String v_query = "SELECT * FROM FHRD_EMP_ENCASH"
                + " WHERE ACTNBY                  =" + p_appr_auth
                + " AND APPLICATION_FLAG          = 'U'"
                + " AND APRVBY                   IS NULL"
                + " AND ATTD_CALC_TIME_ENTRY_FLAG = 'N'";
        return em.createNativeQuery(v_query, FhrdEmpEncash.class).getResultList();
    }
}
