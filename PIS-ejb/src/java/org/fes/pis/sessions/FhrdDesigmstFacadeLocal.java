/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdDesigmst;

/**
 *
 * @author anu
 */
@Local
public interface FhrdDesigmstFacadeLocal {

    void create(FhrdDesigmst fhrdDesigmst);

    void edit(FhrdDesigmst fhrdDesigmst);

    void remove(FhrdDesigmst fhrdDesigmst);

    FhrdDesigmst find(Object id);

    List<FhrdDesigmst> findAll();

    List<FhrdDesigmst> findRange(int[] range);

    int count();

    public java.lang.Integer getMaxSrno_fhrdDesigMst();
    
    public List<FhrdDesigmst> findAllDesignation(Integer p_oum_unit_srno);
    
    public List<FhrdDesigmst> findAll(Integer p_oum_unit_srno);
    
    public Date findMaxTransactionDate(BigDecimal p_dsgm_srg_key);
    
    public List<FhrdDesigmst> findAll(String p_dsgm_srg_key, Integer p_oum_unit_srno);
    
}
