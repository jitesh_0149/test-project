/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdPaytransSummary;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdPaytransSummaryFacade extends AbstractFacade<FhrdPaytransSummary> implements FhrdPaytransSummaryFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdPaytransSummaryFacade() {
        super(FhrdPaytransSummary.class);
    }

    @Override
    public List<FhrdPaytransSummary> findToDisburse(String p_user_id_list) {
        String v_query = "SELECT * FROM FHRD_PAYTRANS_SUMMARY"
                + "     WHERE DISBURSE_FLAG='N'"
                + "     AND USER_ID IN (" + p_user_id_list + ")";
        return em.createNativeQuery(v_query, FhrdPaytransSummary.class).getResultList();
    }
}
