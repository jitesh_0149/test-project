/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdEarndedn;

/**
 *
 * @author anu
 */
@Local
public interface FhrdEarndednFacadeLocal {

    void create(FhrdEarndedn fhrdEarndedn);

    void edit(FhrdEarndedn fhrdEarndedn);

    void remove(FhrdEarndedn fhrdEarndedn);

    FhrdEarndedn find(Object id);

    List<FhrdEarndedn> findAll();

    List<FhrdEarndedn> findRange(int[] range);

    int count();

    //public List<FhrdEarndedn> findEdRuleContractwise(Integer p_oum_unit_srno, BigDecimal p_cm_srg_key);

    public List<FhrdEarndedn> findAll(Integer p_oum_unit_srno, Date p_start_date, Date p_end_date);

    public List<FhrdEarndedn> findEDRuleContractwise(String p_user_id, List<BigDecimal> p_lst_cm_srg_key, Date p_start_date, Date p_end_date);


}
