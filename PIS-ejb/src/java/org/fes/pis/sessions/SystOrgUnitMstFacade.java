/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.SystOrgUnitMst;

/**
 *
 * @author anu
 */
@Stateless
public class SystOrgUnitMstFacade extends AbstractFacade<SystOrgUnitMst> implements SystOrgUnitMstFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SystOrgUnitMstFacade() {
        super(SystOrgUnitMst.class);
    }

    @Override
    public List<SystOrgUnitMst> findAllForUser(Integer p_user_id, Integer p_oum_unit_srno) {
        return em.createNativeQuery(
                " SELECT * "
                + " FROM   SYST_ORG_UNIT_MST "
                + " WHERE  OUM_UNIT_SRNO IN ( "
                + " SELECT OUM_UNIT_SRNO "
                + " FROM   SYST_ORG_ACCESS_RIGHTS  "
                + " WHERE  USER_ID=" + p_user_id
                //                + " AND    ACCESS_FLAG <> 'S' "
                + " AND    (APPLI_NAME ='FHRD'))"
                + " ORDER BY OUM_NAME", SystOrgUnitMst.class).getResultList();
    }

    @Override
    public String findOuName(Integer p_oum_unit_srno) {
        return (String) em.createNativeQuery("SELECT OUM_NAME FROM SYST_ORG_UNIT_MST WHERE OUM_UNIT_SRNO=" + p_oum_unit_srno).getSingleResult();

    }

    @Override
    public SystOrgUnitMst getOrganisation(Integer p_oum_unit_srno) {
        return (SystOrgUnitMst) em.createNativeQuery("select * from syst_org_unit_mst where oum_unit_srno in (select ou_pack.get_org_srno(" + p_oum_unit_srno + ") from dual)", SystOrgUnitMst.class).getSingleResult();
    }

    @Override
    public List<SystOrgUnitMst> findPostingOuForNewEmployee(Integer p_oum_unit_srno) {
        String v_query = "SELECT * "
                + " FROM   SYST_ORG_UNIT_MST  "
                + " WHERE  OUM_UNIT_SRNO IN (  "
                + " SELECT OUM_UNIT_SRNO "
                + " FROM  TABLE(OU_PACK.ORG_HIERARCHY(OU_PACK.GET_ORG_SRNO(" + p_oum_unit_srno + ") ))  "
                + " WHERE ISLEAF=1) "
                + " AND   OUM_OPER_END_DATE IS NULL "
                + " ORDER BY OUM_NAME";
        
        return em.createNativeQuery(v_query, SystOrgUnitMst.class).getResultList();
    }

    @Override
    public List<SystOrgUnitMst> findNativeOuForMonVar(Integer p_oum_unit_srno) {
        String v_query = "SELECT * "
                + " FROM   SYST_ORG_UNIT_MST"
                + " WHERE  OUM_UNIT_SRNO IN ("
                + "  SELECT OU_PACK.GET_NATIVE_OU(OU)"
                + " FROM("
                + " SELECT DISTINCT POSTING_OUM_UNIT_SRNO OU"
                + " FROM   FHRD_EMPMST"
                + " WHERE  NOT (ORG_RELIEVE_DATE IS NOT NULL OR TERMINATION_DATE IS NOT NULL)"
                + " AND    POSTING_OUM_UNIT_SRNO IN (SELECT OUM_UNIT_SRNO FROM TABLE(OU_PACK.ORG_HIERARCHY(" + p_oum_unit_srno + ")))))"
                + " ORDER BY OUM_UNIT_SRNO";
        return em.createNativeQuery(v_query, SystOrgUnitMst.class).getResultList();

    }

    @Override
    public List<SystOrgUnitMst> findAllSubOuDownLevel(Integer p_working_ou) {
        String v_query = "SELECT * FROM SYST_ORG_UNIT_MST WHERE OUM_UNIT_SRNO IN ("
                + "SELECT  * FROM TABLE(OU_PACK.GET_SUB_OU_DOWN_LEVEL(OU_PACK.GET_NATIVE_OU(" + p_working_ou + "),'Y',2,'Y')))";
        return em.createNativeQuery(v_query, SystOrgUnitMst.class).getResultList();
    }

    @Override
    public List<SystOrgUnitMst> findAll(Integer p_oum_unit_srno, boolean subOu, boolean subOuDownLevel, boolean nativeSubOu, boolean nativeSubOuDownLevel, boolean orgSubOu, Integer p_access_span_user_id, String p_access_span_flag) {
        String v_query = "SELECT *"
                + " FROM SYST_ORG_UNIT_MST";
        if (subOu) {
            v_query += " WHERE OUM_UNIT_SRNO IN (SELECT * FROM TABLE(OU_PACK.GET_SUB_OU(" + p_oum_unit_srno + ",'D','Y')))";
        } else if (subOuDownLevel) {
            v_query += " WHERE OUM_UNIT_SRNO IN (SELECT * FROM TABLE(OU_PACK.GET_SUB_DOWN_LEVEL(" + p_oum_unit_srno + ",'Y',2,'Y')))";
        } else if (nativeSubOu) {
            v_query += " WHERE OUM_UNIT_SRNO IN (SELECT * FROM TABLE(OU_PACK.GET_SUB_OU(OU_PACK.GET_NATIVE_OU(" + p_oum_unit_srno + "),'D','Y')))";
        } else if (nativeSubOuDownLevel) {
            v_query += " WHERE OUM_UNIT_SRNO IN (SELECT * FROM TABLE(OU_PACK.GET_SUB_OU_DOWN_LEVEL(OU_PACK.GET_NATIVE_OU(" + p_oum_unit_srno + "),'Y',2,'Y')))";
        } else if (orgSubOu) {
            v_query += " WHERE OUM_UNIT_SRNO IN (SELECT * FROM TABLE(OU_PACK.GET_SUB_OU(OU_PACK.GET_ORG_SRNO(" + p_oum_unit_srno + "),'D','Y')))";
        } else {
            v_query += " WHERE OUM_UNIT_SRNO IN (SELECT * FROM TABLE(OU_PACK.GET_SUB_OU(OU_PACK.GET_ORG_SRNO(" + p_oum_unit_srno + "),'D','Y')))";
        }
        if (p_access_span_user_id != null) {
            v_query += "   AND (OUM_UNIT_SRNO IN  (SELECT OUM_UNIT_SRNO"
                    + "     FROM SYST_ORG_ACCESS_RIGHTS"
                    + "     WHERE APPLI_NAME = 'FHRD'"
                    + "      AND USER_ID     = " + p_access_span_user_id
                    + "      AND ACCESS_SPAN = '" + p_access_span_flag + "'"
                    + "     ) OR  OUM_UNIT_SRNO=" + p_oum_unit_srno + ")";
        }
        v_query += " ORDER BY OUM_NAME";

        return em.createNativeQuery(v_query, SystOrgUnitMst.class).getResultList();
    }
}