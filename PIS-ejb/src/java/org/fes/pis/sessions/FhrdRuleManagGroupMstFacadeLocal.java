/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdRuleManagGroupMst;

/**
 *
 * @author anu
 */
@Local
public interface FhrdRuleManagGroupMstFacadeLocal {

    void create(FhrdRuleManagGroupMst fhrdRuleManagGroupMst);

    void edit(FhrdRuleManagGroupMst fhrdRuleManagGroupMst);

    void remove(FhrdRuleManagGroupMst fhrdRuleManagGroupMst);

    FhrdRuleManagGroupMst find(Object id);

    List<FhrdRuleManagGroupMst> findAll();

    List<FhrdRuleManagGroupMst> findRange(int[] range);

    int count();

    public List<FhrdRuleManagGroupMst> findAll(Integer p_oum_unit_srno, BigDecimal p_v_srg_key, String p_category_desc, String p_contract_type, Date p_start_date, Date p_end_date, boolean p_for_new_applicable);

    public FhrdRuleManagGroupMst findRmgmSrgkey(Integer p_group_srno, String p_group_desc);

    public List<FhrdRuleManagGroupMst> findAll(Integer p_oum_unit_srno);

    public List<FhrdRuleManagGroupMst> findAlternateManageableGroup(Integer p_rmgm_alt_srno, Integer p_oum_unit_srno);

    public boolean isManageGroupDescExists(Integer p_oum_unit_srno, String p_manage_group_desc, Date p_start_date, Date p_end_date);
}
