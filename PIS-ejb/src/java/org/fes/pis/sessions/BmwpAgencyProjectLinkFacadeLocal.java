/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.BmwpAgencyProjectLink;

/**
 *
 * @author anu
 */
@Local
public interface BmwpAgencyProjectLinkFacadeLocal {

    void create(BmwpAgencyProjectLink bmwpAgencyProjectLink);

    void edit(BmwpAgencyProjectLink bmwpAgencyProjectLink);

    void remove(BmwpAgencyProjectLink bmwpAgencyProjectLink);

    BmwpAgencyProjectLink find(Object id);

    List<BmwpAgencyProjectLink> findAll();

    List<BmwpAgencyProjectLink> findRange(int[] range);

    int count();
    
}
