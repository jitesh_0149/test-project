/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FtasAbsenteesDtl;

/**
 *
 * @author anu
 */
@Local
public interface FtasAbsenteesDtlFacadeLocal {

    void create(FtasAbsenteesDtl ftasAbsenteesDtl);

    void edit(FtasAbsenteesDtl ftasAbsenteesDtl);

    void remove(FtasAbsenteesDtl ftasAbsenteesDtl);

    FtasAbsenteesDtl find(Object id);

    List<FtasAbsenteesDtl> findAll();

    List<FtasAbsenteesDtl> findRange(int[] range);

    int count();
    
    public List<FtasAbsenteesDtl> find_ApprovalListDtl(BigDecimal p_absSrgKey);
    
}
