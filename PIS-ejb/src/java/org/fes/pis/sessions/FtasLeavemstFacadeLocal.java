/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FtasLeavemst;

/**
 *
 * @author anu
 */
@Local
public interface FtasLeavemstFacadeLocal {

    void create(FtasLeavemst ftasLeavemst);

    void edit(FtasLeavemst ftasLeavemst);

    void remove(FtasLeavemst ftasLeavemst);

    FtasLeavemst find(Object id);

    List<FtasLeavemst> findAll();

    List<FtasLeavemst> findRange(int[] range);

    int count();

    public List<FtasLeavemst> findAll(Integer p_oum_unit_srno);

    public FtasLeavemst findLeaveCode_OrgWise(String p_leave_code, Integer p_org_om);

    public List<FtasLeavemst> findAllforManageGroup(String p_rmgm_srg_key);

    public List<BigDecimal> findAvailablePrintOrders(Integer p_oum_unit_srno);

    public boolean isLeaveCodeExist(Integer p_oum_unit_srno, String p_leave_code, Date p_start_date, Date p_end_date);

    public List<FtasLeavemst> findAllAlternatives(Integer p_oum_unit_srno, BigDecimal p_lm_srg_key);

    public List<FtasLeavemst> findAllActualAttd(Integer p_oum_unit_srno);

    public BigDecimal findTraining(Integer p_oum_unit_srno);

    public List<FtasLeavemst> findAllLeaves(Integer p_working_ou);

    public FtasLeavemst findByLeaveCd(Integer p_oum_unit_srno, String p_leavecd, Date p_date);
    
}
