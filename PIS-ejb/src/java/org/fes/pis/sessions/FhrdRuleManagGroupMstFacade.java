/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.entities.FhrdRuleManagGroupMst;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdRuleManagGroupMstFacade extends AbstractFacade<FhrdRuleManagGroupMst> implements FhrdRuleManagGroupMstFacadeLocal {

    
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdRuleManagGroupMstFacade() {
        super(FhrdRuleManagGroupMst.class);
    }

    @Override
    public FhrdRuleManagGroupMst findRmgmSrgkey(Integer p_group_srno, String p_group_desc) {
        return (FhrdRuleManagGroupMst) em.createNativeQuery(
                " select * from fhrd_rule_manag_group_mst"
                + " where MANAG_GROUP_SRNO=" + p_group_srno
                + " and MANAG_GROUP_DESC='" + p_group_desc + "'"
                + " order by RMGM_SRG_KEY", FhrdRuleManagGroupMst.class).getSingleResult();
    }

    @Override
    public List<FhrdRuleManagGroupMst> findAll(Integer p_oum_unit_srno) {
        return em.createNativeQuery("SELECT * FROM FHRD_RULE_MANAG_GROUP_MST "
                + " WHERE OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_DESIGMST'," + p_oum_unit_srno + ") "
                + " ORDER BY  MANAG_GROUP_DESC", FhrdRuleManagGroupMst.class).getResultList();
    }

    @Override
    public List<FhrdRuleManagGroupMst> findAll(Integer p_oum_unit_srno, BigDecimal p_v_srg_key, String p_category_desc, String p_contract_type, Date p_start_date, Date p_end_date, boolean p_for_new_applicable) {
        String v_start_date = DateTimeUtility.ChangeDateFormat(p_start_date, null);
        String v_end_date = DateTimeUtility.ChangeDateFormat(p_end_date, null);
        String v_query = "SELECT * FROM FHRD_RULE_MANAG_GROUP_MST "
                + " WHERE OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_RULE_MANAG_GROUP_MST'," + p_oum_unit_srno + ") ";
        if (p_contract_type != null) {
            v_query += " AND contract_type_flag='" + p_contract_type + "' ";
        }
        if (p_v_srg_key != null) {
            v_query += " AND RGC_SRG_KEY= " + p_v_srg_key;

        } else if (p_category_desc != null) {
            v_query += " AND RGC_SRG_KEY=( "
                    + " SELECT RGC_SRG_KEY FROM FHRD_RULE_GROUP_CATEGORY "
                    + " WHERE CAT_DESC='" + p_category_desc + "' "
                    + " AND OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_RULE_GROUP_CATEGORY'," + p_oum_unit_srno + "))";
        }
        if (p_for_new_applicable) {
            if (p_start_date != null) {
                v_query += " AND START_DATE<= '" + v_start_date + "'";
            }
            if (p_end_date != null) {
                v_query += " AND (END_DATE>= '" + v_end_date + "'  OR END_DATE IS NULL) ";
            } else {
                v_query += " AND END_DATE IS NULL";
            }
        } else {
            if (p_end_date != null) {
                v_query += " AND START_DATE<= '" + v_end_date + "'";
            }
            if (p_start_date != null) {
                v_query += " AND (END_DATE>= '" + v_start_date + "'  OR END_DATE IS NULL) ";
            }
        }
        v_query += " ORDER BY MANAG_GROUP_DESC ";
        return em.createNativeQuery(v_query, FhrdRuleManagGroupMst.class).getResultList();

    }

    @Override
    public List<FhrdRuleManagGroupMst> findAlternateManageableGroup(Integer p_rmgm_alt_srno, Integer p_oum_unit_srno) {
        return em.createNativeQuery("SELECT * FROM FHRD_RULE_MANAG_GROUP_MST "
                + " WHERE RMGM_ALT_SRNO = " + p_rmgm_alt_srno + ""
                + " AND OUM_UNIT_SRNO IN (OU_PACK.GET_OUM_UNIT_SRNO('FHRD_RULE_MANAG_GROUP_MST'," + p_oum_unit_srno + "))"
                + " ORDER BY  MANAG_GROUP_DESC", FhrdRuleManagGroupMst.class).getResultList();
    }

    @Override
    public boolean isManageGroupDescExists(Integer p_oum_unit_srno, String p_manage_group_desc, Date p_start_date, Date p_end_date) {
        String v_start_date = DateTimeUtility.ChangeDateFormat(p_start_date, null);
        String v_end_date = DateTimeUtility.ChangeDateFormat(p_end_date, null);
        String v_query = "SELECT COUNT(*)  FROM FHRD_RULE_MANAG_GROUP_MST "
                + " WHERE MANAG_GROUP_DESC= '" + p_manage_group_desc + "'"
                + " AND OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_RULE_MANAG_GROUP_MST'," + p_oum_unit_srno + ")"
                + " AND ((END_DATE>='" + v_start_date + "' OR END_DATE IS NULL) ";
        if (p_end_date != null) {
            v_query += " AND START_DATE<='" + v_end_date + "') ";
        } else {
            v_query += ")";
        }
        BigDecimal v_total_count = (BigDecimal) em.createNativeQuery(v_query).getSingleResult();
        return !v_total_count.equals(BigDecimal.ZERO);
    }
}
