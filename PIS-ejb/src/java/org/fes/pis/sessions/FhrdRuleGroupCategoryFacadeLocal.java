/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdRuleGroupCategory;

/**
 *
 * @author anu
 */
@Local
public interface FhrdRuleGroupCategoryFacadeLocal {

    void create(FhrdRuleGroupCategory fhrdRuleGroupCategory);

    void edit(FhrdRuleGroupCategory fhrdRuleGroupCategory);

    void remove(FhrdRuleGroupCategory fhrdRuleGroupCategory);

    FhrdRuleGroupCategory find(Object id);

    List<FhrdRuleGroupCategory> findAll();

    List<FhrdRuleGroupCategory> findRange(int[] range);

    int count();

    public List<FhrdRuleGroupCategory> findAll(Integer p_oum_unit_srno, Date p_start_date, Date p_end_date);
}
