/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdCasteCategoryMst;

/**
 *
 * @author anu
 */
@Local
public interface FhrdCasteCategorymstFacadeLocal {

    void create(FhrdCasteCategoryMst FhrdCasteCategoryMst);

    void edit(FhrdCasteCategoryMst FhrdCasteCategoryMst);

    void remove(FhrdCasteCategoryMst FhrdCasteCategoryMst);

    FhrdCasteCategoryMst find(Object id);

    List<FhrdCasteCategoryMst> findAll();

    List<FhrdCasteCategoryMst> findRange(int[] range);

    int count();
    
    public List<FhrdCasteCategoryMst> findAll(Integer p_oum_unit_srno);
}
