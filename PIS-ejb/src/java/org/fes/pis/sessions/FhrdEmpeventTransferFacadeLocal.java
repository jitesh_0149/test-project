/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdEmpeventTransfer;

/**
 *
 * @author anu
 */
@Local
public interface FhrdEmpeventTransferFacadeLocal {

    void create(FhrdEmpeventTransfer fhrdEmpeventTransfer);

    void edit(FhrdEmpeventTransfer fhrdEmpeventTransfer);

    void remove(FhrdEmpeventTransfer fhrdEmpeventTransfer);

    FhrdEmpeventTransfer find(Object id);

    List<FhrdEmpeventTransfer> findAll();

    List<FhrdEmpeventTransfer> findRange(int[] range);

    int count();
    
}
