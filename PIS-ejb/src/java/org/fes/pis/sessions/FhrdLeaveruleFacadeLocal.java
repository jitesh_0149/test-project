/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdLeaverule;

/**
 *
 * @author anu
 */
@Local
public interface FhrdLeaveruleFacadeLocal {

    void create(FhrdLeaverule fhrdLeaverule);

    void edit(FhrdLeaverule fhrdLeaverule);

    void remove(FhrdLeaverule fhrdLeaverule);

    FhrdLeaverule find(Object id);

    List<FhrdLeaverule> findAll();

    List<FhrdLeaverule> findRange(int[] range);

    int count();

    public List<FhrdLeaverule> findLeaveRuleContractwise(String p_user_id, List<BigDecimal> p_lst_cm_srg_key, Date p_start_date, Date p_end_date, String p_gender_flag);

    public List<FhrdLeaverule> findParentRules(Integer p_oum_unit_srno, Date p_start_date, String p_rmgm_srg_key);

    public List<FhrdLeaverule> findAll(Integer p_oum_unit_srno);
}
