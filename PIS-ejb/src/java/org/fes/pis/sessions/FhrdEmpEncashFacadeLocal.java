/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdEmpEncash;

/**
 *
 * @author anu
 */
@Local
public interface FhrdEmpEncashFacadeLocal {

    void create(FhrdEmpEncash fhrdEmpEncash);

    void edit(FhrdEmpEncash fhrdEmpEncash);

    void remove(FhrdEmpEncash fhrdEmpEncash);

    FhrdEmpEncash find(Object id);

    List<FhrdEmpEncash> findAll();

    List<FhrdEmpEncash> findRange(int[] range);

    int count();
    
    public List<FhrdEmpEncash> findAllForApproval(String p_appr_auth, Integer p_oum_unit_srno);
}
