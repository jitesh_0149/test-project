/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdEmpeventRelieve;

/**
 *
 * @author anu
 */
@Local
public interface FhrdEmpeventRelieveFacadeLocal {

    void create(FhrdEmpeventRelieve fhrdEmpeventRelieve);

    void edit(FhrdEmpeventRelieve fhrdEmpeventRelieve);

    void remove(FhrdEmpeventRelieve fhrdEmpeventRelieve);

    FhrdEmpeventRelieve find(Object id);

    List<FhrdEmpeventRelieve> findAll();

    List<FhrdEmpeventRelieve> findRange(int[] range);

    int count();
    
}
