/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdContractGroupRelation;

/**
 *
 * @author anu
 */
@Local
public interface FhrdContractGroupRelationFacadeLocal {

    void create(FhrdContractGroupRelation fhrdContractGroupRelation);

    void edit(FhrdContractGroupRelation fhrdContractGroupRelation);

    void remove(FhrdContractGroupRelation fhrdContractGroupRelation);

    FhrdContractGroupRelation find(Object id);

    List<FhrdContractGroupRelation> findAll();

    List<FhrdContractGroupRelation> findRange(int[] range);

    int count();
    
}
