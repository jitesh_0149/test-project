/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.custom_entities.AbstractFacade;
import org.fes.pis.entities.FhrdPaytransSalaryErrlog;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdPaytransSalaryErrlogFacade extends AbstractFacade<FhrdPaytransSalaryErrlog> implements FhrdPaytransSalaryErrlogFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdPaytransSalaryErrlogFacade() {
        super(FhrdPaytransSalaryErrlog.class);
    }

    @Override
    public List<FhrdPaytransSalaryErrlog> findAllBySalarySrgKey(BigDecimal p_salary_srg_key) {
        String v_query = "SELECT * FROM FHRD_PAYTRANS_SALARY_ERRLOG WHERE SALARY_SRG_KEY=" + p_salary_srg_key;
        return em.createNativeQuery(v_query, FhrdPaytransSalaryErrlog.class).getResultList();
    }
    
    @Override
    public BigDecimal getSalarySrgKey() {
        BigDecimal v_maxsrno = (BigDecimal) em.createNativeQuery(" select FHRD_SEQ_SALARY_SRG_KEY.nextval from dual").getSingleResult();
//        System.out.println("max "+v_maxsrno);
        return v_maxsrno;
    }
}
