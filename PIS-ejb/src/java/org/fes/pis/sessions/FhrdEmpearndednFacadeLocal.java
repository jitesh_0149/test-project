/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.custom_entities.PaytransMonthlyVariableBean;
import org.fes.pis.entities.FhrdEmpearndedn;

/**
 *
 * @author anu
 */
@Local
public interface FhrdEmpearndednFacadeLocal {

    void create(FhrdEmpearndedn fhrdEmpearndedn);

    void edit(FhrdEmpearndedn fhrdEmpearndedn);

    void remove(FhrdEmpearndedn fhrdEmpearndedn);

    FhrdEmpearndedn find(Object id);

    List<FhrdEmpearndedn> findAll();

    List<FhrdEmpearndedn> findRange(int[] range);

    int count();

    public List<FhrdEmpearndedn> getEDRuleEmpwise(Integer p_userid, String p_contract_list, Integer p_oum_unit_srno, String p_opening_date, String p_closing_date);

    public List<PaytransMonthlyVariableBean> findAllMonthlyVariable(Integer p_oum_unit_srno, Date p_start_date, Date p_end_date, Integer p_user_id, String p_status);
}
