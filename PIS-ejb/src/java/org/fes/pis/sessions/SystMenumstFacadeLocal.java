/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.SystMenumst;

/**
 *
 * @author anu
 */
@Local
public interface SystMenumstFacadeLocal {

    void create(SystMenumst systMenumst);

    void edit(SystMenumst systMenumst);

    void remove(SystMenumst systMenumst);

    SystMenumst find(Object id);

    List<SystMenumst> findAll();

    List<SystMenumst> findRange(int[] range);

    int count();
}
