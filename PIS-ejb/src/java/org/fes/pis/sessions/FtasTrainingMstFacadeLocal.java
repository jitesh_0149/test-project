/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FtasTrainingMst;

/**
 *
 * @author anu
 */
@Local
public interface FtasTrainingMstFacadeLocal {

    void create(FtasTrainingMst ftasTrainingMst);

    void edit(FtasTrainingMst ftasTrainingMst);

    void remove(FtasTrainingMst ftasTrainingMst);

    FtasTrainingMst find(Object id);

    List<FtasTrainingMst> findAll();

    List<FtasTrainingMst> findRange(int[] range);

    int count();
    
    public List<FtasTrainingMst> getTrainingList(Integer p_user_id, BigDecimal p_lm_srg_key, Integer p_oum_unit_srno);
    
}
