/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FtasHolidayDtl;

/**
 *
 * @author anu
 */
@Stateless
public class FtasHolidayDtlFacade extends AbstractFacade<FtasHolidayDtl> implements FtasHolidayDtlFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FtasHolidayDtlFacade() {
        super(FtasHolidayDtl.class);
    }

    @Override
    public List<FtasHolidayDtl> findHolidayYearWise(Integer p_year, Integer p_native_ou) {
        return em.createNativeQuery("select * from"
                + " ftas_holiday_dtl"
                + " where holi_year= " + p_year + ""
                + " and oum_unit_srno=" + p_native_ou + ""
                + " order by holi_date", FtasHolidayDtl.class).getResultList();
    }

    @Override
    public List<String> findWeekendDays(Integer p_year) {
        return em.createNativeQuery(
                " SELECT DISTINCT"
                + " CASE "
                + "  WHEN SUBSTR(HOLI_DESC, 1 ,INSTR(HOLI_DESC, ',', 1, 1)-1) IS NOT NULL"
                + "  THEN SUBSTR(HOLI_DESC, 1 ,INSTR(HOLI_DESC, ',', 1, 1)-1)"
                + "  ELSE holi_desc"
                + " END HOLI_DESC"
                + " FROM ftas_holiday_dtl"
                + " WHERE weekend_flag='Y'"
                + " AND HOLI_YEAR     =" + p_year).getResultList();
    }

    @Override
    public List<FtasHolidayDtl> findAll(Integer p_oum_unit_srno) {
        return em.createNativeQuery("select * "
                + " FROM FTAS_HOLIDAY_DTL A"
                + " WHERE A.HOLI_DATE BETWEEN to_date('1-NOV-'||to_char(to_number(to_char(sysdate,'yyyy'))-1))"
                + " and LAST_DAY(TO_DATE('1-FEB-'||TO_CHAR(TO_NUMBER(TO_CHAR(sysdate,'yyyy'))+1))) "
                + " AND OUM_UNIT_SRNO in (select * from table(ou_pack.get_sub_ou(ou_pack.get_native_ou(" + p_oum_unit_srno + "),'b','y')))"
                + " AND A.ACTION_FLAG='A'"
                + " ORDER BY A.HOLI_DATE", FtasHolidayDtl.class).getResultList();
    }

    @Override
    public List<String> getYearListForHolidayProcess(Integer p_oum_unit_srno) {
        return em.createNativeQuery(" select to_char(column_value) "
                + " from table(ftas_pack.Get_YearListForHolidayProcess(ou_pack.get_native_ou(" + p_oum_unit_srno + "))) ").getResultList();
    }

    @Override
    public List<String> findAllHoliDesc(Integer p_year) {
        return em.createNativeQuery("SELECT DISTINCT(HOLI_DESC) FROM FTAS_HOLIDAY_DTL"
                + " WHERE HOLI_YEAR=" + p_year).getResultList();
    }
}