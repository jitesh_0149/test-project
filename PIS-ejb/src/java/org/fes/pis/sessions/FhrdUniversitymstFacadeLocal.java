/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdUniversitymst;

/**
 *
 * @author anu
 */
@Local
public interface FhrdUniversitymstFacadeLocal {

    void create(FhrdUniversitymst fhrdUniversitymst);

    void edit(FhrdUniversitymst fhrdUniversitymst);

    void remove(FhrdUniversitymst fhrdUniversitymst);

    FhrdUniversitymst find(Object id);

    List<FhrdUniversitymst> findAll();

    List<FhrdUniversitymst> findRange(int[] range);

    int count();

    public List<FhrdUniversitymst> findAll(Integer p_oum_unit_srno);

    public boolean isUniversityExist(Integer p_oum_unit_srno, String p_univercity_code, String p_unitversity_name, String p_city, String p_state);

    public String findUniversity(Integer p_oum_unit_srno, String p_univercity_code);
}
