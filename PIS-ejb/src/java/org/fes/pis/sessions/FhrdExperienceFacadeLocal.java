/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdExperience;

/**
 *
 * @author anu
 */
@Local
public interface FhrdExperienceFacadeLocal {

    void create(FhrdExperience fhrdExperience);

    void edit(FhrdExperience fhrdExperience);

    void remove(FhrdExperience fhrdExperience);

    FhrdExperience find(Object id);

    List<FhrdExperience> findAll();

    List<FhrdExperience> findRange(int[] range);

    int count();

    public List<FhrdExperience> findExperienceUseridwise(Integer p_userid);

    public List<String> findAllCompanies(Integer p_oum_unit_srno);

    public List<String> findAllDesignations(Integer p_oum_unit_srno);
}
