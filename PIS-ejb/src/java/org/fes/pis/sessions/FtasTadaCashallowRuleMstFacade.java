/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FtasTadaCashallowRuleMst;


/**
 *
 * @author mayuri
 */
@Stateless
public class FtasTadaCashallowRuleMstFacade extends AbstractFacade<FtasTadaCashallowRuleMst> implements FtasTadaCashallowRuleMstFacadeLocal {
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FtasTadaCashallowRuleMstFacade() {
        super(FtasTadaCashallowRuleMst.class);
    }
    @Override
    public List<FtasTadaCashallowRuleMst> findAll(Integer p_oum_unit_srno){
    String v_query="SELECT * FROM FTAS_TADA_CASHALLOW_RULE_MST"
                + " WHERE OUM_UNIT_SRNO = OU_PACK.GET_OUM_UNIT_SRNO('FTAS_TADA_CASHALLOW_RULE_MST',"+p_oum_unit_srno+")"
                + " ORDER BY CARM_SRG_KEY";
     return em.createNativeQuery(v_query,FtasTadaCashallowRuleMst.class).getResultList();
    }
   
    
}
