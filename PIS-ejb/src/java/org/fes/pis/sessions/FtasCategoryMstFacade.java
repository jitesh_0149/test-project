/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FtasCategoryMst;

/**
 *
 * @author anu
 */
@Stateless
public class FtasCategoryMstFacade extends AbstractFacade<FtasCategoryMst> implements FtasCategoryMstFacadeLocal {
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FtasCategoryMstFacade() {
        super(FtasCategoryMst.class);
    }
    
    @Override
    public List<FtasCategoryMst> find_category(String p_category_type) {
        return em.createNativeQuery("select * from" +
                " ftas_category_mst where cat_type='" + p_category_type + "'" +
                " and alive_flag='Y' order by 3", FtasCategoryMst.class).getResultList();

    }
    
    @Override
    public FtasCategoryMst find_category(String p_cat, String p_cal_val_desc) {
        return (FtasCategoryMst) em.createNativeQuery("select * from "
                            + " ftas_category_mst where cat_type='" + p_cat + "' "
                            + " and cat_value_desc=trim('" + p_cal_val_desc + "')", FtasCategoryMst.class).getSingleResult();

    }
    
}
