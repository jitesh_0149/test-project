/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.BmwpAgencyProjectLink;

/**
 *
 * @author anu
 */
@Stateless
public class BmwpAgencyProjectLinkFacade extends AbstractFacade<BmwpAgencyProjectLink> implements BmwpAgencyProjectLinkFacadeLocal {
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BmwpAgencyProjectLinkFacade() {
        super(BmwpAgencyProjectLink.class);
    }
    
}
