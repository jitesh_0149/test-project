/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FcadAddrDtl;

/**
 *
 * @author anu
 */
@Local
public interface FcadAddrDtlFacadeLocal {

    void create(FcadAddrDtl fcadAddrDtl);

    void edit(FcadAddrDtl fcadAddrDtl);

    void remove(FcadAddrDtl fcadAddrDtl);

    FcadAddrDtl find(Object id);

    List<FcadAddrDtl> findAll();

    List<FcadAddrDtl> findRange(int[] range);

    int count();

    public FcadAddrDtl findActive(BigDecimal p_am_unique_srno);
}
