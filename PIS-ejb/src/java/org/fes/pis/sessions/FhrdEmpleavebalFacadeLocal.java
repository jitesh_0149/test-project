/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdEmpleavebal;

/**
 *
 * @author anu
 */
@Local
public interface FhrdEmpleavebalFacadeLocal {

    void create(FhrdEmpleavebal fhrdEmpleavebal);

    void edit(FhrdEmpleavebal fhrdEmpleavebal);

    void remove(FhrdEmpleavebal fhrdEmpleavebal);

    FhrdEmpleavebal find(Object id);

    List<FhrdEmpleavebal> findAll();

    List<FhrdEmpleavebal> findRange(int[] range);

    int count();

    public BigDecimal getEmpParentLeaveBal(Integer p_userid, String p_leavecd, Integer p_rulesrno);

    public String getEmpLeaveBal(Integer p_userid, String p_leavecd);

    public List<FhrdEmpleavebal> findAllFromLeave(Integer p_oum_unit_srno, Integer p_user_id, boolean p_nullClosingBalance, String p_considerAsAttendanceFlag, boolean p_timeSensitive, String p_leaveFlag, String p_virtualLeave, String p_parenteable, String p_ttrParenteable, String p_applicationRequiredFlag, String p_exceptEmplbSrgKey, boolean p_halfApplicable, Date p_clubStartDate, String p_encashment, BigDecimal p_lrSrgKey, Date p_from_date, Date p_to_date, String p_user_managed);

    public BigDecimal checkEmpLeaveClosingBal(Integer p_userid);

    public boolean isUserTransactionPending(Integer p_user_id);

    public List<FhrdEmpleavebal> findAllForEncashmentApplication(Integer p_user_id);
}
