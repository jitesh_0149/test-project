/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.entities.FhrdRuleApplGroupMst;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdRuleApplGroupMstFacade extends AbstractFacade<FhrdRuleApplGroupMst> implements FhrdRuleApplGroupMstFacadeLocal {

    
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdRuleApplGroupMstFacade() {
        super(FhrdRuleApplGroupMst.class);
    }

    @Override
    public List<FhrdRuleApplGroupMst> find_applicable_group(String p_contract_type) {
        return em.createNativeQuery(
                " select * from FHRD_RULE_APPL_GROUP_MST "
                + " where END_DATE>sysdate"
                + " or END_DATE is null "
                + " AND contract_type_flag='" + p_contract_type + "'"
                + " order by RAGM_SRG_KEY", FhrdRuleApplGroupMst.class).getResultList();
    }

    @Override
    public List<FhrdRuleApplGroupMst> findAll(Integer p_oum_unit_srno) {
        return em.createNativeQuery("SELECT * FROM FHRD_RULE_APPL_GROUP_MST "
                + " WHERE OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_DESIGMST'," + p_oum_unit_srno + ") "
                + " ORDER BY APPL_GROUP_DESC", FhrdRuleApplGroupMst.class).getResultList();
    }

    @Override
    public boolean isApplGroupDescExists(Integer p_oum_unit_srno, String p_appl_group_desc, Date p_start_date, Date p_end_date) {
        String v_start_date = DateTimeUtility.ChangeDateFormat(p_start_date, null);
        String v_end_date = DateTimeUtility.ChangeDateFormat(p_end_date, null);
        String v_query = "SELECT COUNT(*)  FROM FHRD_RULE_APPL_GROUP_MST "
                + " WHERE APPL_GROUP_DESC= '" + p_appl_group_desc + "'"
                + " AND OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_RULE_APPL_GROUP_MST'," + p_oum_unit_srno + ")"
                + " AND ((END_DATE>='" + v_start_date + "' OR END_DATE IS NULL) ";
        if (p_end_date != null) {
            v_query += " AND START_DATE<='" + v_end_date + "') ";
        } else {
            v_query += ")";
        }
        BigDecimal v_total_count = (BigDecimal) em.createNativeQuery(v_query).getSingleResult();
        return !v_total_count.equals(BigDecimal.ZERO);
    }
}
