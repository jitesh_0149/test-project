/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdDegreemst;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdDegreemstFacade extends AbstractFacade<FhrdDegreemst> implements FhrdDegreemstFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdDegreemstFacade() {
        super(FhrdDegreemst.class);
    }

    @Override
    public List<FhrdDegreemst> findAll(Integer p_oum_unit_srno) {
        return em.createNativeQuery("SELECT * FROM FHRD_DEGREEMST "
                + " WHERE OUM_UNIT_SRNO IN (OU_PACK.GET_OUM_UNIT_SRNO('FHRD_DEGREEMST'," + p_oum_unit_srno + "))"
                + " ORDER BY DEGREECD", FhrdDegreemst.class).getResultList();



    }

    @Override
    public boolean isDegreeValid(Integer p_oum_unit_srno, String p_degree_cd, String p_degree_name) {
        BigDecimal v_total_count = (BigDecimal) em.createNativeQuery("SELECT COUNT(*) FROM FHRD_DEGREEMST "
                + " WHERE (OUM_UNIT_SRNO=OU_PACK.GET_OUM_UNIT_SRNO('FHRD_DEGREEMST'," + p_oum_unit_srno + ") "
                + " AND DEGREECD='" + p_degree_cd + "') "
                + " OR (OUM_UNIT_SRNO=OU_PACK.GET_OUM_UNIT_SRNO('FHRD_DEGREEMST'," + p_oum_unit_srno + ") "
                + " AND DEGREE_NAME='" + p_degree_name + "') ").getSingleResult();
        return v_total_count.equals(BigDecimal.ZERO);
    }
}
