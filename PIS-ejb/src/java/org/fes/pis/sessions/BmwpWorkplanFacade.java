/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.BmwpAgencyProjectLink;
import org.fes.pis.entities.BmwpWorkplan;

/**
 *
 * @author anu
 */
@Stateless
public class BmwpWorkplanFacade extends AbstractFacade<BmwpWorkplan> implements BmwpWorkplanFacadeLocal {
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BmwpWorkplanFacade() {
        super(BmwpWorkplan.class);
    }
    
    @Override
    public List<BmwpWorkplan> findAll(Integer p_oum_unit_srno, Integer p_finyear, String p_fundType, BigDecimal p_apl_unique_srno, String workplan_status, BigDecimal p_exc_wp_unique_srno) {
        String v_query =
                " SELECT WP.* FROM "
                + " BMWP_WORKPLAN WP,BMWP_AGENCY_PROJECT_LINK APL,BMWP_PROJECT_MST PM "
                + " WHERE "
                + " WP.WP_APL_UNIQUE_SRNO=APL.APL_UNIQUE_SRNO "
                + " AND APL.APL_PM_UNIQUE_SRNO=PM.PM_UNIQUE_SRNO ";
        if (p_oum_unit_srno != null) {
            v_query += " AND WP.OUM_UNIT_SRNO=" + p_oum_unit_srno;
        }
        if (p_apl_unique_srno != null) {
            v_query += " AND APL.APL_UNIQUE_SRNO=" + p_apl_unique_srno;
        }
        if (p_finyear != null) {
            v_query += " AND WP.WP_FINYEAR=" + p_finyear;
        }
        if (workplan_status != null) {
            v_query += " AND WP.WP_FINALIZED_FLAG='" + workplan_status + "'";
        }
        if (p_fundType != null) {
            v_query += " AND PM.PM_FUND_TYPE='" + p_fundType + "'";
        }
        if (p_exc_wp_unique_srno != null) {
            v_query += " AND WP.WP_UNIQUE_SRNO <>" + p_exc_wp_unique_srno;
        }
//        String v_query = "select w from "
//                + " BmwpWorkplan as w "
//                + " where w.oumUnitSrno.oumUnitSrno=" + p_oum_unit_srno
//                + " and w.wpFinyear=" + p_finyear;
//        if (p_agency_project != null) {
//            v_query += " and w.wpApabdUniqueSrno.apabdApablUniqueSrno.apablApbmUniqueSrno.apbmAplUniqueSrno.aplUniqueSrno=" + p_agency_project;
//        }
//        if (p_fundType != null) {
//            v_query += " and w.wpApabdUniqueSrno.apabdApablUniqueSrno.apablApbmUniqueSrno.apbmAplUniqueSrno.aplPmUniqueSrno.pmFundType='" + p_fundType + "'";
//        }
//        if (p_workplanStatus != null) {
//            v_query += " and w.wpFinalizedFlag='" + p_workplanStatus + "'";
//        }
//        v_query += " order by w.wpApabdUniqueSrno.apabdApablUniqueSrno.apablApbmUniqueSrno.apbmAplUniqueSrno.aplFamUniqueSrno.famAgencyName"
//                + ",w.wpApabdUniqueSrno.apabdApablUniqueSrno.apablApbmUniqueSrno.apbmAplUniqueSrno.aplPmUniqueSrno.pmProjName "
//                + ",w.wpApabdUniqueSrno.apabdApablUniqueSrno.apablApamUniqueSrno.apamActivityDesc "
//                + ",w.wpApabdUniqueSrno.apabdApablUniqueSrno.apablApbmUniqueSrno.apbmBudgetDesc";
//
        
       // System.out.println("V_query"+v_query);
        return em.createNativeQuery(v_query, BmwpWorkplan.class).getResultList();
    }
    
    @Override
    public List<BmwpAgencyProjectLink> findAgencyProjectLinkNativeQuery(Integer p_oum_unit_srno, Integer p_finyear, String p_fund_type, boolean finalizedworkplan, boolean fromSubOu, BigDecimal exceptAplSrno, BigDecimal p_ptd_unique_srno) {
        String v_query = " SELECT APL.* "
                + " FROM BMWP_AGENCY_PROJECT_LINK APL,BMWP_PROJECT_MST PM ,BMWP_FUND_AGENCY_MST FAM "
                + " WHERE APL.APL_UNIQUE_SRNO IN ( "
                + " SELECT DISTINCT WP_APL_UNIQUE_SRNO "
                + " FROM BMWP_WORKPLAN "
                + " WHERE WP_FINYEAR=" + p_finyear;
        if (finalizedworkplan) {
            v_query += " AND WP_Finalized_Flag='Y' ";
        }
        if (p_ptd_unique_srno != null) {
            v_query += " and wp_ptd_unique_srno=" + p_ptd_unique_srno;
        }
        if (fromSubOu) {
            v_query += " AND OUM_UNIT_SRNO IN  (select * from table(ou_pack.get_sub_ou(" + p_oum_unit_srno + ",'D','Y')))";
        } else {
            v_query += " AND OUM_UNIT_SRNO =" + p_oum_unit_srno;
        }
        v_query += " )";
        v_query += " AND APL.APL_FAM_UNIQUE_SRNO=FAM.FAM_UNIQUE_SRNO "
                + " AND APL.APL_PM_UNIQUE_SRNO=PM.PM_UNIQUE_SRNO ";
        if (exceptAplSrno != null) {
            v_query += " AND APL.APL_UNIQUE_SRNO<>" + exceptAplSrno;
        }
        if (p_fund_type != null) {
            v_query += " AND PM.PM_FUND_TYPE='" + p_fund_type + "'";
        }

        v_query += " ORDER BY FAM.FAM_AGENCY_SHORT_NAME,PM.PM_PROJ_SHORT_NAME ";
        //System.out.println("V_query"+v_query);
        return em.createNativeQuery(v_query, BmwpAgencyProjectLink.class).getResultList();
    }
}
