/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FtasAbsenteesDtl;

/**
 *
 * @author anu
 */
@Stateless
public class FtasAbsenteesDtlFacade extends AbstractFacade<FtasAbsenteesDtl> implements FtasAbsenteesDtlFacadeLocal {
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FtasAbsenteesDtlFacade() {
        super(FtasAbsenteesDtl.class);
    }
    
    @Override
    public List<FtasAbsenteesDtl> find_ApprovalListDtl(BigDecimal p_absSrgKey) {
        return em.createNativeQuery(
                " SELECT * FROM FTAS_ABSENTEES_DTL "
                + " WHERE ABS_SRG_KEY = "+p_absSrgKey,FtasAbsenteesDtl.class).getResultList();
    }
}
