/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FtasTadaConvMst;

/**
 *
 * @author mayuri
 */
@Stateless
public class FtasTadaConvMstFacade extends AbstractFacade<FtasTadaConvMst> {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FtasTadaConvMstFacade() {
        super(FtasTadaConvMst.class);
    }

    public List<FtasTadaConvMst> findAll(Integer p_oum_unit_srno, boolean p_activeOnly) {
        String v_query = "SELECT * FROM FTAS_TADA_CONV_MST"
                + " WHERE OUM_UNIT_SRNO = OU_PACK.GET_OUM_UNIT_SRNO('FTAS_TADA_CONV_MST'," + p_oum_unit_srno + ")";
        if (p_activeOnly) {
            v_query += " AND END_DATE IS NULL";
        }

        v_query += " ORDER BY CM_DESC";
        return em.createNativeQuery(v_query, FtasTadaConvMst.class).getResultList();
    }
}
