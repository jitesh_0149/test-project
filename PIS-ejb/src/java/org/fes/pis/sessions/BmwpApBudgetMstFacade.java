/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.BmwpApBudgetMst;

/**
 *
 * @author anu
 */
@Stateless
public class BmwpApBudgetMstFacade extends AbstractFacade<BmwpApBudgetMst> implements BmwpApBudgetMstFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BmwpApBudgetMstFacade() {
        super(BmwpApBudgetMst.class);
    }

    @Override
    public List<BmwpApBudgetMst> findAllAPwise(Integer p_apl_unique_srno) {
        String v_query = " SELECT * FROM BMWP_AP_BUDGET_MST"
                + " WHERE APBM_APL_UNIQUE_SRNO = " + p_apl_unique_srno
                + " ORDER BY APBM_UNIQUE_SRNO";

        return em.createNativeQuery(v_query, BmwpApBudgetMst.class).getResultList();

    }
}
