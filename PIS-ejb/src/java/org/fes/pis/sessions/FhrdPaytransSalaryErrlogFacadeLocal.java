/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdPaytransSalaryErrlog;

/**
 *
 * @author anu
 */
@Local
public interface FhrdPaytransSalaryErrlogFacadeLocal {

    void create(FhrdPaytransSalaryErrlog fhrdPaytransSalaryErrlog);

    void edit(FhrdPaytransSalaryErrlog fhrdPaytransSalaryErrlog);

    void remove(FhrdPaytransSalaryErrlog fhrdPaytransSalaryErrlog);

    FhrdPaytransSalaryErrlog find(Object id);

    List<FhrdPaytransSalaryErrlog> findAll();

    List<FhrdPaytransSalaryErrlog> findRange(int[] range);

    int count();
    
    public List<FhrdPaytransSalaryErrlog> findAllBySalarySrgKey(BigDecimal p_salary_srg_key);
    
    public BigDecimal getSalarySrgKey();
}
