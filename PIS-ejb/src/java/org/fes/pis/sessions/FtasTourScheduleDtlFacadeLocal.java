/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FtasTourScheduleDtl;

/**
 *
 * @author anu
 */
@Local
public interface FtasTourScheduleDtlFacadeLocal {

    void create(FtasTourScheduleDtl ftasTourScheduleDtl);

    void edit(FtasTourScheduleDtl ftasTourScheduleDtl);

    void remove(FtasTourScheduleDtl ftasTourScheduleDtl);

    FtasTourScheduleDtl find(Object id);

    List<FtasTourScheduleDtl> findAll();

    List<FtasTourScheduleDtl> findRange(int[] range);

    int count();

    public List<FtasTourScheduleDtl> findAll(Integer p_user_id, Date p_from_date, Date p_to_date, boolean p_active_only);
}
