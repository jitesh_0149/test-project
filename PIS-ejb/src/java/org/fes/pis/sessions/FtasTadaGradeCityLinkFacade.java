/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FtasTadaGradeCityLink;

/**
 *
 * @author mayuri
 */
@Stateless
public class FtasTadaGradeCityLinkFacade extends AbstractFacade<FtasTadaGradeCityLink> {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FtasTadaGradeCityLinkFacade() {
        super(FtasTadaGradeCityLink.class);
    }

    public List<FtasTadaGradeCityLink> findAll(Integer p_oum_unit_srno) {
        String v_query = "SELECT * FROM FTAS_TADA_GRADE_CITY_LINK"
                + " WHERE OUM_UNIT_SRNO = OU_PACK.GET_OUM_UNIT_SRNO('FTAS_TADA_GRADE_CITY_LINK'," + p_oum_unit_srno + ")"
                + " ORDER BY CITY";
        return em.createNativeQuery(v_query, FtasTadaGradeCityLink.class).getResultList();
    }
}
