/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.entities.FhrdEmpmst;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdEmpmstFacade extends AbstractFacade<FhrdEmpmst> implements FhrdEmpmstFacadeLocal {

    
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdEmpmstFacade() {
        super(FhrdEmpmst.class);
    }

    @Override
    public List<FhrdEmpmst> findEmpListForBulkAtt(Integer p_native_ou, Date p_start_date, Date p_end_date) {
        String v_start_date = DateTimeUtility.ChangeDateFormat(p_start_date, null);
        String v_end_date = DateTimeUtility.ChangeDateFormat(p_end_date, null);
        String v_query = " SELECT *"
                + " FROM FHRD_EMPMST"
                + " WHERE USER_ID IN"
                + "  ( SELECT DISTINCT USER_ID"
                + "  FROM FHRD_EMP_CONTRACT_MST"
                + "  WHERE (('" + v_start_date + "' BETWEEN START_DATE AND END_DATE)"
                + "  OR ('" + v_end_date + "' BETWEEN START_DATE AND END_DATE)"
                + "  ))"
                + " AND POSTING_OUM_UNIT_SRNO IN"
                + "  (SELECT * FROM TABLE(ou_pack.get_sub_ou(" + p_native_ou + ",'d','y'))"
                + "  ) ORDER BY USER_ID";
        return em.createNativeQuery(
                v_query, FhrdEmpmst.class).getResultList();
    }

    @Override
    public List<FhrdEmpmst> findAll_useridwise() {
        return em.createQuery("select object(o) from FhrdEmpmst as o where o.userId not in (0) order by o.userId").getResultList();
    }

//    @Override
//    public List<FhrdEmpmst> findByAccessSpan(Integer p_user_id, Integer p_oum_unit_srno) {
//        String v_query = "SELECT * FROM FHRD_EMPMST "
//                + " WHERE    POSTING_OUM_UNIT_SRNO=" + p_oum_unit_srno
//        
//                + " AND      USER_TYPE_SRNO<>2  "
//                + " AND NOT  (ORG_RELIEVE_DATE IS NOT NULL"
//                + " OR       TERMINATION_DATE IS NOT NULL) "
//                + " ORDER BY USER_NAME";
//        return em.createNativeQuery(v_query, FhrdEmpmst.class).getResultList();
//    }
    @Override
    public List<FhrdEmpmst> findAll(Integer p_oum_unit_srno, boolean byPostingOu, boolean byNativeOu, boolean activeOnly, Date activeDate, Integer p_access_span_userid, String p_verification_status, boolean balanceActiveOnly, boolean allowWorkingOuAccess, boolean p_order_by_empno, Integer p_under_ou) {
        String v_query = " SELECT * FROM FHRD_EMPMST "
                + " WHERE USER_TYPE_SRNO = 1 ";
        if (byPostingOu) {
            v_query += " AND  POSTING_OUM_UNIT_SRNO =" + p_oum_unit_srno;
        } else if (byNativeOu) {
            v_query += " AND  POSTING_OUM_UNIT_SRNO IN (OU_PACK.GET_NATIVE_OU(" + p_oum_unit_srno + ")";
        } else {
            v_query += " AND  OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_EMPMST'," + p_oum_unit_srno + ")";
        }

        if (p_under_ou != null) {
            v_query += " AND POSTING_OUM_UNIT_SRNO IN (SELECT * FROM TABLE(OU_PACK.GET_SUB_OU(" + p_under_ou + ",'D','Y')))";
        }
        if (balanceActiveOnly) {
            v_query += " AND USER_ID IN (SELECT USER_ID FROM FHRD_EMPLEAVEBAL WHERE CLOSING_BAL IS NULL)";
        }

        if (activeOnly) {
            if (activeDate == null) {
                v_query += " AND NOT (ORG_RELIEVE_DATE IS NOT NULL OR TERMINATION_DATE IS NOT NULL)";
            } else {
                v_query += " AND NOT ((ORG_RELIEVE_DATE IS NOT NULL AND ORG_RELIEVE_DATE<='" + DateTimeUtility.ChangeDateFormat(activeDate, null) + "'"
                        + " OR (TERMINATION_DATE IS NOT NULL AND  TERMINATION_DATE<='" + DateTimeUtility.ChangeDateFormat(activeDate, null) + "')))";
            }
        }
        //else{
        //    v_query+="   AND  (ORG_RELIEVE_DATE IS NOT NULL"
        //            + "        OR TERMINATION_DATE      IS NOT NULL) "
        //            + "  AND USER_ID IN (SELECT DISTINCT USER_ID FROM FHRD_EMP_CONTRACT_MST)";
        //}
        if (p_access_span_userid != null) {
            v_query += " AND  (POSTING_OUM_UNIT_SRNO IN (SELECT OUM_UNIT_SRNO FROM SYST_ORG_ACCESS_RIGHTS WHERE ACCESS_SPAN='O' AND APPLI_NAME='FHRD' AND USER_ID=" + p_access_span_userid + ")";
            if (allowWorkingOuAccess) {
                v_query += " OR POSTING_OUM_UNIT_SRNO=" + p_oum_unit_srno + ")";
            } else {
                v_query += " OR USER_ID=" + p_access_span_userid + ")";
            }
        }
        if (p_verification_status != null) {
            v_query += " AND VERIFICATION='" + p_verification_status + "'";
        }
        if (p_order_by_empno) {
            v_query += " ORDER BY EMPNO";
        } else {
            v_query += " ORDER BY USER_NAME";
        }

        return em.createNativeQuery(v_query, FhrdEmpmst.class).getResultList();
    }

    @Override
    public List<FhrdEmpmst> findAllEmployeeForContract(Integer p_oum_unit_srno, Integer p_access_span_user_id) {
        String v_query = "SELECT * FROM FHRD_EMPMST   "
                + " WHERE OUM_UNIT_SRNO IN ou_pack.get_org_srno(" + p_oum_unit_srno + ")"
                + " and user_type_srno  IN (select user_type_srno from syst_user_type_mst"
                + " where oum_unit_srno IN ou_pack.get_oum_unit_srno('SYST_USER_TYPE_MST'," + p_oum_unit_srno + ")"
                + " and user_type_desc in (" + new PIS_GlobalData().getUserTypeForContract() + "))";
        if (p_access_span_user_id != null) {
            v_query += " AND (POSTING_OUM_UNIT_SRNO IN(SELECT OUM_UNIT_SRNO "
                    + " FROM   SYST_ORG_ACCESS_RIGHTS"
                    + " WHERE  ACCESS_SPAN='O' "
                    + " AND    USER_ID=" + p_access_span_user_id + ")"
                    + " OR USER_ID=" + p_access_span_user_id + ")"
                    + " AND  USER_ID IN (SELECT USER_ID FROM FHRD_EMP_CONTRACT_MST)";
        }
//        v_query += " order by USER_NAME";
        return em.createNativeQuery(v_query, FhrdEmpmst.class).getResultList();
    }

    @Override
    public List<FhrdEmpmst> findAllForMonVar(Integer p_oum_unit_srno, String p_month_start_date, String p_month_end_date, String p_status) {
        String v_query = " SELECT *"
                + " FROM FHRD_EMPMST"
                + " WHERE POSTING_OUM_UNIT_SRNO IN"
                + "     (SELECT * FROM TABLE(OU_PACK.GET_SUB_OU(" + p_oum_unit_srno + ",'D','Y'))"
                + "     )"
                + " AND USER_ID IN"
                + "     ( SELECT DISTINCT USER_ID"
                + "     FROM FHRD_EMPEARNDEDN EED"
                + "     WHERE PAYABLE_ON_VALUE='MV'"
                + "      AND TO_DATE('" + p_month_end_date + "','DD-MON-YYYY') BETWEEN OPENING_DATE AND CLOSING_DATE";
//        if (p_status.equals("S")) {
//            v_query += "      AND EED.EED_SRG_KEY IN"
//                    + "          ( SELECT EED_SRG_KEY FROM FHRD_PAYTRANS_MONVAR WHERE USER_ID=EED.USER_ID"
//                    + "          )";
//        }
//        if (p_status.equals("P")) {
//            v_query += "      AND EED.EED_SRG_KEY NOT IN"
//                    + "          ( SELECT EED_SRG_KEY FROM FHRD_PAYTRANS_MONVAR WHERE USER_ID=EED.USER_ID"
//                    + "          )";
//        }

        v_query += "     )"
                + " ORDER BY NVL(USER_FIRST_NAME,USER_NAME) ";
        return em.createNativeQuery(v_query, FhrdEmpmst.class).getResultList();
    }

    @Override
    public List<FhrdEmpmst> findAllForAttendanceCalc(Integer p_oum_unit_srno, String p_start_date, String p_end_date) {

//        String v_query = "SELECT *"
//                + " FROM FHRD_EMPMST"
//                + " WHERE USER_ID IN"
//                + "     ( SELECT  USER_ID FROM FTAS_LOCK_DTL"
//                + "     WHERE   ((FROM_DATE BETWEEN '" + p_start_date + "' AND '" + p_end_date + "')"
//                + "     OR        (TO_DATE BETWEEN '" + p_start_date + "' AND '" + p_end_date + "'))"
//                + "     AND       (SELF_STATUS_FLAG='A' AND ADMIN_STATUS_FLAG IS NULL "
//                + "     AND AUDIT_STATUS_FLAG IS NULL)"
//                + "     )"
//                + " AND POSTING_OUM_UNIT_SRNO IN"
//                + "     (SELECT * FROM TABLE(OU_PACK.GET_SUB_OU(" + p_oum_unit_srno + ",'D','Y'))"
//                + "     )"
//                + " AND USER_ID NOT IN"
//                + "     (SELECT USER_ID"
//                + "     FROM  FHRD_PAYTRANSLEAVE_MST"
//                + "     WHERE ORG_ATTENDANCE_FROM_DATE='" + p_start_date + "'"
//                + "     AND   ORG_ATTENDANCE_TO_DATE  = '" + p_end_date + "'"
//                + "     )"
//                + "ORDER BY USER_ID";
        String v_query = "SELECT * "
                + " FROM FHRD_EMPMST"
                + " WHERE USER_ID IN("
                + " SELECT USER_ID"
                + " FROM FHRD_EMPMST "
                + "     WHERE USER_ID IN     "
                + "               (SELECT  USER_ID FROM FTAS_LOCK_DTL     "
                + "               WHERE   ('" + p_start_date + "' BETWEEN FROM_DATE AND TO_DATE)     "
                + "               AND       (SELF_STATUS_FLAG='A' AND ADMIN_STATUS_FLAG IS NULL      "
                + "               AND AUDIT_STATUS_FLAG IS NULL)) "
                + "     AND POSTING_OUM_UNIT_SRNO IN     (SELECT * FROM TABLE(OU_PACK.GET_SUB_OU(" + p_oum_unit_srno + ",'D','Y'))) "
                + "     AND USER_ID NOT IN       (SELECT USER_ID     "
                + "                              FROM  FHRD_PAYTRANSLEAVE_MST     "
                + ""
                + "                             WHERE ORG_ATTENDANCE_FROM_DATE='" + p_start_date + "'    "
                + "                              AND   ORG_ATTENDANCE_TO_DATE  = '" + p_end_date + "')"
                + "     AND USER_ID NOT IN ("
                + "               SELECT USER_ID"
                + "               FROM FHRD_EMPMST "
                + "               WHERE"
                /*
                 * + " USER_ID IN " + " (SELECT USER_ID FROM FTAS_LOCK_DTL " + "
                 * WHERE ((FROM_DATE BETWEEN ADD_MONTHS(TO_DATE('" +
                 * p_start_date + "','DD-MON-YYYY'),-1) AND
                 * ADD_MONTHS(TO_DATE('" + p_end_date + "','DD-MON-YYYY'),-1)) "
                 * + " OR (TO_DATE BETWEEN ADD_MONTHS(TO_DATE('" + p_start_date
                 * + "','DD-MON-YYYY'),-1) AND ADD_MONTHS(TO_DATE('" +
                 * p_end_date + "','DD-MON-YYYY'),-1))) " + " AND
                 * (SELF_STATUS_FLAG='A' AND ADMIN_STATUS_FLAG IS NULL " + " AND
                 * AUDIT_STATUS_FLAG IS NULL)) " + " AND "
                 */
                + "  POSTING_OUM_UNIT_SRNO IN     (SELECT * FROM TABLE(OU_PACK.GET_SUB_OU(" + p_oum_unit_srno + ",'D','Y'))) "
                + "               AND USER_ID NOT IN       (SELECT USER_ID     "
                + "                                        FROM  FHRD_PAYTRANSLEAVE_MST     "
                + "                                        WHERE ORG_ATTENDANCE_FROM_DATE=ADD_MONTHS(TO_DATE('" + p_start_date + "','DD-MON-YYYY'),-1)   "
                + "                                        AND   ORG_ATTENDANCE_TO_DATE  = ADD_MONTHS(TO_DATE('" + p_end_date + "','DD-MON-YYYY'),-1))))"
                + " ORDER BY USER_ID";

        return em.createNativeQuery(v_query, FhrdEmpmst.class).getResultList();
    }

    @Override
    public List<FhrdEmpmst> findAllForSalaryCalc(Integer p_oum_unit_srno, String p_start_date, String p_end_date) {
        String v_query = "SELECT *"
                + " FROM FHRD_EMPMST"
                + " WHERE POSTING_OUM_UNIT_SRNO IN"
                + "     (SELECT * FROM TABLE(OU_PACK.GET_SUB_OU(" + p_oum_unit_srno + ",'D','Y'))"
                + "     )"
                + " AND USER_ID  IN"
                + "     (SELECT USER_ID"
                + "     FROM FHRD_PAYTRANSLEAVE_MST"
                + "     WHERE ORG_ATTENDANCE_FROM_DATE BETWEEN '" + p_start_date + "' AND '" + p_end_date + "'"
                + "     )      "
                + " AND USER_ID NOT IN"
                + "     (SELECT USER_ID"
                + "     FROM FHRD_PAYTRANS_SUMMARY"
                + "     WHERE ORG_ATTENDANCE_FROM_DATE BETWEEN '" + p_start_date + "' AND '" + p_end_date + "'"
                + "     ) ";
//                System.out.println("query "+v_query);
        return em.createNativeQuery(v_query, FhrdEmpmst.class).getResultList();
    }

    @Override
    public List<FhrdEmpmst> findReportingUser(Integer p_oum_unit_srno, Integer p_user_id) {
        String v_query = " select * from fhrd_empmst where user_id in ( "
                + " select distinct user_id from syst_org_access_rights "
                + " where APPR_AUDIT_FLAG = 'A' "
                + " and appli_name = 'FHRD'"
                + " and OUM_UNIT_SRNO = " + p_oum_unit_srno
                + " ) AND USER_TYPE_SRNO<>2 "
                + " AND USER_ID <> " + p_user_id
                + " AND NOT (ORG_RELIEVE_DATE IS NOT NULL OR TERMINATION_DATE IS NOT NULL)";
        return em.createNativeQuery(v_query, FhrdEmpmst.class).getResultList();
    }

    @Override
    public boolean hasAccessSpanO(Integer p_user_id, Integer p_oum_unit_srno) {
        String v_query = "SELECT ACCESS_SPAN FROM SYST_ORG_ACCESS_RIGHTS"
                + " WHERE  OUM_UNIT_SRNO = " + p_oum_unit_srno
                + " AND USER_ID = " + p_user_id
                + " AND APPLI_NAME = 'FHRD'";
        List<String> v_access_span = em.createNativeQuery(v_query).getResultList();
        return v_access_span.isEmpty() ? false : (v_access_span.get(0).equals("O"));
    }

    @Override
    public List<FhrdEmpmst> findAllRelievedUsers(Integer p_oum_unit_srno) {
        return em.createNativeQuery("SELECT * FROM FHRD_EMPMST"
                + " WHERE USER_ID IN (SELECT USER_ID FROM FHRD_EMP_CONTRACT_MST"
                + " WHERE OUM_UNIT_SRNO = OU_PACK.GET_OUM_UNIT_SRNO('FHRD_EMP_CONTRACT_MST'," + p_oum_unit_srno + "))"
                + " AND   ORG_RELIEVE_DATE IS NOT NULL "
                + " ORDER BY USER_NAME", FhrdEmpmst.class).getResultList();
    }

    @Override
    public List<FhrdEmpmst> findAllFromAttendanceCalc(Integer p_oum_unit_srno, String p_user_status) {
        String v_query = " SELECT * "
                + " FROM   FHRD_EMPMST "
                + " WHERE  USER_ID IN (SELECT DISTINCT USER_ID "
                + " FROM   FHRD_PAYTRANSLEAVE_DTL "
                + " WHERE  ORG_ATTENDANCE_TO_DATE>( "
                + " SELECT MAX(TO_DATE)  "
                + " FROM   FTAS_LOCK_MST "
                + " WHERE  OUM_UNIT_SRNO=OU_PACK.GET_OUM_UNIT_SRNO('FTAS_LOCK_MST'," + p_oum_unit_srno + ") "
                + " AND    LOCK_STATUS='Y')) "
                + " AND    OUM_UNIT_SRNO = OU_PACK.GET_OUM_UNIT_SRNO('FHRD_EMPMST'," + p_oum_unit_srno + ")";
        if (p_user_status.equals("C")) {
            v_query += " AND (ORG_RELIEVE_DATE IS NULL AND TERMINATION_DATE IS NULL) ";
        } else if (p_user_status.equals("R")) {
            v_query += " AND (ORG_RELIEVE_DATE IS NOT NULL OR TERMINATION_DATE IS NOT NULL) ";
        }
        v_query += " ORDER BY EMPNO";
        return em.createNativeQuery(v_query, FhrdEmpmst.class).getResultList();

    }
}
