/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdEmpleavebal;
import org.fes.pis.entities.FhrdEmpleaverule;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdEmpleaveruleFacade extends AbstractFacade<FhrdEmpleaverule> implements FhrdEmpleaveruleFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdEmpleaveruleFacade() {
        super(FhrdEmpleaverule.class);
    }

    @Override
    public FhrdEmpleaverule getMinMaxTaken_holidayEmbedded(Integer p_rulesrno) {
        return (FhrdEmpleaverule) em.createNativeQuery(
                " select * from FHRD_EMPLEAVERULE where rulesrno=" + p_rulesrno, FhrdEmpleaverule.class).getSingleResult();
    }

//    @Override
//    public List<FhrdEmpleavebal> getLeaveRuleEmpwise(Integer p_userid, Integer p_oum_unit_snro) {
//        String v_query="SELECT * FROM FHRD_EMPLEAVEBAL "
//                + " WHERE ELR_SRG_KEY IN (SELECT A.ELR_SRG_KEY "
//                + " FROM FHRD_EMPLEAVERULE A,"
//                + " FTAS_LEAVEMST B"
//                + " WHERE A.user_id      ="+p_userid
//                + " AND (A.CLOSING_DATE IS NULL"
//                + " OR A.CLOSING_DATE   >= SYSDATE)"
//                + " AND (A.END_DATE     IS NULL"
//                + " OR A.END_DATE       >=SYSDATE)"
//                + " AND A.LM_SRG_KEY     =B.LM_SRG_KEY"
//                + " AND B.LEAVE_FLAG     ='Y'"
//                + " AND A.OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_EMPLEAVERULE',"+p_oum_unit_snro+"))";
////        System.out.println("query "+v_query);
//        return em.createNativeQuery(v_query, FhrdEmpleavebal.class).getResultList();
//    }
    @Override
    public List<FhrdEmpleavebal> getLeaveRuleEmpwise(Integer p_userid, Integer p_oum_unit_snro, String p_contract_list, String p_opening_date, String p_closing_date) {
        String v_query = "SELECT * FROM FHRD_EMPLEAVEBAL "
                + " WHERE ELR_SRG_KEY IN ("
                + " SELECT ELR.ELR_SRG_KEY"
                + " FROM FHRD_EMPLEAVERULE ELR,"
                + "     FHRD_LEAVERULE LR"
                + " WHERE ELR.USER_ID      =" + p_userid
                + " AND LR.LR_SRG_KEY     =ELR.LR_SRG_KEY"
                + " AND ELR.RMGM_SRG_KEY IN"
                + "     (SELECT RMGM_SRG_KEY"
                + "     FROM FHRD_RULE_GROUP_RELATION"
                + "     WHERE RGC_SRG_KEY  =FHRD_PACK.GET_ADMIN_RGC_SRG_KEY(" + p_oum_unit_snro + ")";

        if (p_contract_list != null) {
            v_query += "      AND RAGM_SRG_KEY IN"
                    + "          (SELECT RAGM_SRG_KEY"
                    + "          FROM FHRD_CONTRACT_GROUP_RELATION"
                    + "          WHERE CM_SRG_KEY IN (" + p_contract_list + ")"
                    + "          )";
        }
        v_query += "     )     ) "
                + " AND (START_DATE>='" + p_opening_date + "' AND START_DATE <='" + p_closing_date + "')"
                + " AND (END_DATE  >='" + p_opening_date + "' AND END_DATE   <='" + p_closing_date + "')"
                + " ORDER BY LEAVECD,OPENING_DATE";
//        System.out.println("query " + v_query);
        return em.createNativeQuery(v_query, FhrdEmpleavebal.class).getResultList();
    }
}
