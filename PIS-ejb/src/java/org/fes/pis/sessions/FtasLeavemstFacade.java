/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.entities.FtasLeavemst;

/**
 *
 * @author anu
 */
@Stateless
public class FtasLeavemstFacade extends AbstractFacade<FtasLeavemst> implements FtasLeavemstFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;
    

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FtasLeavemstFacade() {
        super(FtasLeavemst.class);
    }

    @Override
    public List<FtasLeavemst> findAll(Integer p_oum_unit_srno) {
        return em.createNativeQuery(
                " SELECT * FROM FTAS_LEAVEMST "
                + " WHERE OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FTAS_LEAVEMST'," + p_oum_unit_srno + ")", FtasLeavemst.class).getResultList();
    }

    @Override
    public FtasLeavemst findLeaveCode_OrgWise(String p_leave_code, Integer p_org_om) {
        return (FtasLeavemst) em.createNativeQuery(
                " select * from ftas_leavemst where leavecd='" + p_leave_code + "'"
                + " and oum_unit_srno in (select ou_pack.get_org_srno(" + p_org_om + ") from dual)", FtasLeavemst.class).getSingleResult();
    }

    @Override
    public List<FtasLeavemst> findAllforManageGroup(String p_rmgm_srg_key) {
        String v_query = "SELECT * "
                + " FROM FTAS_LEAVEMST "
                + " WHERE LM_ALT_SRNO NOT IN "
                + "     (SELECT LM_ALT_SRNO FROM FHRD_LEAVERULE WHERE RMGM_SRG_KEY = " + p_rmgm_srg_key
                + "     ) "
                + " AND (MULTIPLE_RULE_FLAG='Y' OR "
                + "     (MULTIPLE_RULE_FLAG='N' AND LM_ALT_SRNO NOT IN "
                + "     (SELECT DISTINCT LM_ALT_SRNO "
                + "     FROM FHRD_EMPLEAVERULE "
                + "     WHERE USER_ID IN "
                + "          (SELECT DISTINCT USER_ID "
                + "          FROM FHRD_EMPLEAVERULE "
                + "          WHERE RMGM_SRG_KEY IN "
                + "               (SELECT DISTINCT RMGM_SRG_KEY "
                + "               FROM FHRD_RULE_MANAG_GROUP_MST "
                + "               WHERE RMGM_ALT_SRNO = "
                + "                    (SELECT RMGM_ALT_SRNO FROM FHRD_RULE_MANAG_GROUP_MST WHERE RMGM_SRG_KEY= " + p_rmgm_srg_key
                + "                    ) "
                + "               ) "
                + "          ) "
                + "     ))) "
                + " AND (LEAVE_FLAG='Y' OR CONSIDER_AS_ATTENDANCE_FLAG IN ('T','R')) "
                + " ORDER BY LEAVECD ";
//        System.out.println("leave query "+v_query);
        return em.createNativeQuery(v_query, FtasLeavemst.class).getResultList();
    }

    @Override
    public List<BigDecimal> findAvailablePrintOrders(Integer p_oum_unit_srno) {
        return em.createNativeQuery(
                " SELECT LEVEL PRINT_ORDER "
                + " FROM DUAL "
                + " CONNECT BY LEVEL < 100 "
                + " MINUS "
                + " SELECT PRINT_ORDER FROM FTAS_LEAVEMST "
                + " WHERE OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FTAS_LEAVEMST'," + p_oum_unit_srno + ") AND PRINT_ORDER IS NOT NULL ").getResultList();

    }

    @Override
    public boolean isLeaveCodeExist(Integer p_oum_unit_srno, String p_leave_code, Date p_start_date, Date p_end_date) {
        String v_start_date = DateTimeUtility.ChangeDateFormat(p_start_date, null);
        String v_end_date = DateTimeUtility.ChangeDateFormat(p_end_date, null);
        String v_query = "SELECT COUNT(*)  FROM FTAS_LEAVEMST "
                + " WHERE LEAVECD= '" + p_leave_code + "'"
                + " AND OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FTAS_LEAVEMST'," + p_oum_unit_srno + ")"
                + " AND ((END_DATE>='" + v_start_date + "' OR END_DATE IS NULL) ";
        if (p_end_date != null) {
            v_query += " AND START_DATE<='" + v_end_date + "') ";
        } else {
            v_query += ")";
        }
        BigDecimal v_total_count = (BigDecimal) em.createNativeQuery(v_query).getSingleResult();
        return !v_total_count.equals(BigDecimal.ZERO);


    }

    @Override
    public List<FtasLeavemst> findAllAlternatives(Integer p_oum_unit_srno, BigDecimal p_lm_srg_key) {
        String v_query = " SELECT *"
                + " FROM FTAS_LEAVEMST"
                + " WHERE OUM_UNIT_SRNO IN (OU_PACK.GET_OUM_UNIT_SRNO('FTAS_LEAVEMST'," + p_oum_unit_srno + "))"
                + " AND LM_ALT_SRNO = "
                + " (SELECT LM_ALT_SRNO FROM FTAS_LEAVEMST WHERE LM_SRG_KEY=" + p_lm_srg_key
                + " ) ";
        return em.createNativeQuery(v_query, FtasLeavemst.class).getResultList();
    }

    @Override
    public List<FtasLeavemst> findAllActualAttd(Integer p_oum_unit_srno) {
        String v_query = " SELECT * "
                + " FROM FTAS_LEAVEMST"
                + " WHERE (ACTUAL_ATTENDANCE_FLAG    ='Y'"
                + " OR CONSIDER_AS_ACTUAL_ATTD_FLAG='Y')"
                + " AND OUM_UNIT_SRNO IN (OU_PACK.GET_OUM_UNIT_SRNO('FTAS_LEAVEMST'," + p_oum_unit_srno + "))";
        return em.createNativeQuery(v_query, FtasLeavemst.class).getResultList();
    }

    @Override
    public BigDecimal findTraining(Integer p_oum_unit_srno) {
        return (BigDecimal) em.createNativeQuery("SELECT LM_SRG_KEY "
                + "  FROM FTAS_LEAVEMST"
                + "  WHERE CONSIDER_AS_ATTENDANCE_FLAG = 'R'"
                + "  AND OUM_UNIT_SRNO               IN"
                + "  (SELECT * FROM TABLE(ou_pack.get_sub_ou(ou_pack.get_org_srno(" + p_oum_unit_srno + "),'d','y'))"
                + "  )").getSingleResult();
    }

    @Override
    public List<FtasLeavemst> findAllLeaves(Integer p_working_ou) {
        String v_query = "SELECT *"
                + " FROM FTAS_LEAVEMST"
                + " WHERE OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FTAS_LEAVEMST'," + p_working_ou + ")"
                + " AND (LEAVE_FLAG      ='Y'"
                + " OR CONSIDER_AS_ATTENDANCE_FLAG IN ('T','R'))"
                + " ORDER BY LEAVECD";
        return em.createNativeQuery(v_query, FtasLeavemst.class).getResultList();
    }

    @Override
    public FtasLeavemst findByLeaveCd(Integer p_oum_unit_srno, String p_leavecd, Date p_date) {
        String v_date = DateTimeUtility.ChangeDateFormat(p_date, null);
        List<FtasLeavemst> lst_temp = em.createNativeQuery("SELECT * "
                + " FROM   FTAS_LEAVEMST "
                + " WHERE  LEAVECD='" + p_leavecd + "' "
                + " AND    OUM_UNIT_SRNO=OU_PACK.GET_OUM_UNIT_SRNO('FTAS_LEAVEMST'," + p_oum_unit_srno + ") "
                + " AND    '" + v_date + "' BETWEEN START_DATE AND NVL(END_DATE,'" + v_date + "') ", FtasLeavemst.class).getResultList();
        return lst_temp.isEmpty() ? null : lst_temp.get(0);


    }
}
