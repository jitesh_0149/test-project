/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FtasTrainingOrgUnitDtl;

/**
 *
 * @author anu
 */
@Stateless
public class FtasTrainingOrgUnitDtlFacade extends AbstractFacade<FtasTrainingOrgUnitDtl> implements FtasTrainingOrgUnitDtlFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FtasTrainingOrgUnitDtlFacade() {
        super(FtasTrainingOrgUnitDtl.class);
    }

    @Override
    public FtasTrainingOrgUnitDtl findSelectedTrainingOrgDtl(BigDecimal p_trann_srg_key, Integer p_oum_unit_srno) {
        String v_query = "SELECT *"
                + " FROM FTAS_TRAINING_ORG_UNIT_DTL"
                + " WHERE TRANN_SRG_KEY     = " + p_trann_srg_key
                + " AND SYST_ORG_UNIT_SRNO = " + p_oum_unit_srno;

        return (FtasTrainingOrgUnitDtl) em.createNativeQuery(v_query, FtasTrainingOrgUnitDtl.class).getSingleResult();
    }
}
