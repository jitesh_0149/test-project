/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdRuleGroupRelation;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdRuleGroupRelationFacade extends AbstractFacade<FhrdRuleGroupRelation> implements FhrdRuleGroupRelationFacadeLocal {
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdRuleGroupRelationFacade() {
        super(FhrdRuleGroupRelation.class);
    }
    
}
