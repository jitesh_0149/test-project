/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.BmwpAgencyProjectLink;
import org.fes.pis.entities.BmwpWorkplan;

/**
 *
 * @author anu
 */
@Local
public interface BmwpWorkplanFacadeLocal {

    void create(BmwpWorkplan bmwpWorkplan);

    void edit(BmwpWorkplan bmwpWorkplan);

    void remove(BmwpWorkplan bmwpWorkplan);

    BmwpWorkplan find(Object id);

    List<BmwpWorkplan> findAll();

    List<BmwpWorkplan> findRange(int[] range);

    int count();
    
    public List<BmwpWorkplan> findAll(Integer p_oum_unit_srno, Integer p_finyear, String p_fundType, BigDecimal p_apl_unique_srno, String workplan_status, BigDecimal p_exc_wp_unique_srno);
    
    public List<BmwpAgencyProjectLink> findAgencyProjectLinkNativeQuery(Integer p_oum_unit_srno, Integer p_finyear, String p_fund_type, boolean finalizedworkplan, boolean fromSubOu, BigDecimal exceptAplSrno, BigDecimal p_ptd_unique_srno);
    
}
