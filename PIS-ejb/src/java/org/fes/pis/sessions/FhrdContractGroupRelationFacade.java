/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdContractGroupRelation;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdContractGroupRelationFacade extends AbstractFacade<FhrdContractGroupRelation> implements FhrdContractGroupRelationFacadeLocal {
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdContractGroupRelationFacade() {
        super(FhrdContractGroupRelation.class);
    }
    
}
