/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FtasTrainingScheduleDtl;

/**
 *
 * @author anu
 */
@Local
public interface FtasTrainingScheduleDtlFacadeLocal {

    void create(FtasTrainingScheduleDtl ftasTrainingScheduleDtl);

    void edit(FtasTrainingScheduleDtl ftasTrainingScheduleDtl);

    void remove(FtasTrainingScheduleDtl ftasTrainingScheduleDtl);

    FtasTrainingScheduleDtl find(Object id);

    List<FtasTrainingScheduleDtl> findAll();

    List<FtasTrainingScheduleDtl> findRange(int[] range);

    int count();

    public List<FtasTrainingScheduleDtl> findAll(Integer p_user_id, Date p_from_date, Date p_to_date, boolean p_active_only);
}
