/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdEmpmst;

/**
 *
 * @author anu
 */
@Local
public interface FhrdEmpmstFacadeLocal {

    void create(FhrdEmpmst fhrdEmpmst);

    void edit(FhrdEmpmst fhrdEmpmst);

    void remove(FhrdEmpmst fhrdEmpmst);

    FhrdEmpmst find(Object id);

    List<FhrdEmpmst> findAll();

    List<FhrdEmpmst> findRange(int[] range);

    int count();

    public List<FhrdEmpmst> findEmpListForBulkAtt(Integer p_native_ou, Date p_start_date, Date p_end_date);

    public List<FhrdEmpmst> findAll_useridwise();

    public List<FhrdEmpmst> findAll(Integer p_oum_unit_srno, boolean byPostingOu, boolean byNativeOu, boolean activeOnly, Date activeDate, Integer p_access_span_userid, String p_verification_status, boolean balanceActiveOnly, boolean allowWorkingOuAccess, boolean p_order_by_empno, Integer p_under_ou);

    public List<FhrdEmpmst> findAllEmployeeForContract(Integer p_oum_unit_srno, Integer p_access_span_user_id);

    public List<FhrdEmpmst> findAllForMonVar(Integer p_oum_unit_srno, String p_month_start_date, String p_month_end_date, String p_status);

    public List<FhrdEmpmst> findAllForAttendanceCalc(Integer p_oum_unit_srno, String p_start_dtae, String p_end_date);

    public List<FhrdEmpmst> findAllForSalaryCalc(Integer p_oum_unit_srno, String p_start_date, String p_end_date);

    public List<FhrdEmpmst> findReportingUser(Integer p_oum_unit_srno, Integer p_user_id);

    public boolean hasAccessSpanO(Integer p_user_id, Integer p_oum_unit_srno);

    public List<FhrdEmpmst> findAllRelievedUsers(Integer p_oum_unit_srno);

    public List<FhrdEmpmst> findAllFromAttendanceCalc(Integer p_oum_unit_srno, String p_user_status);
}
