/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdEventMst;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdEventMstFacade extends AbstractFacade<FhrdEventMst> implements FhrdEventMstFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdEventMstFacade() {
        super(FhrdEventMst.class);
    }

    @Override
    public List<FhrdEventMst> findAll_ByOrder() {
        return em.createQuery("select object(o) from FhrdEventMst as o order by o.emSrgKey").getResultList();
    }

    @Override
    public List<FhrdEventMst> find_Detail_List(BigDecimal p_em_srg_key) {
        return em.createNativeQuery(
                " SELECT * FROM FHRD_EVENT_MST"
                + " where EM_SRG_KEY=" + p_em_srg_key, FhrdEventMst.class).getResultList();
    }

    @Override
    public FhrdEventMst findByEventName(String p_event_name) {
        String p_str = "SELECT * FROM"
                + " FHRD_EVENT_MST WHERE event_name like '" + p_event_name + "'";
//        System.out.println(""+p_str);
        return (FhrdEventMst) em.createNativeQuery(p_str, FhrdEventMst.class).getSingleResult();
    }

    @Override
    public List<FhrdEventMst> findAll(Integer p_org_unit_srno) {
        return em.createNativeQuery("SELECT * FROM FHRD_EVENT_MST "
                + " WHERE OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_EVENT_MST'," + p_org_unit_srno + ") "
                + " ORDER BY EM_SRG_KEY", FhrdEventMst.class).getResultList();
    }
}
