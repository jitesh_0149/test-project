/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdDegreemst;

/**
 *
 * @author anu
 */
@Local
public interface FhrdDegreemstFacadeLocal {

    void create(FhrdDegreemst fhrdDegreemst);

    void edit(FhrdDegreemst fhrdDegreemst);

    void remove(FhrdDegreemst fhrdDegreemst);

    FhrdDegreemst find(Object id);

    List<FhrdDegreemst> findAll();

    List<FhrdDegreemst> findRange(int[] range);

    int count();

    public List<FhrdDegreemst> findAll(Integer p_oum_unit_srno);

    public boolean isDegreeValid(Integer p_oum_unit_srno, String p_degree_cd, String p_degree_name);
}
