/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdEarndednConfig;

/**
 *
 * @author anu
 */
@Local
public interface FhrdEarndednConfigFacadeLocal {

    void create(FhrdEarndednConfig fhrdEarndednConfig);

    void edit(FhrdEarndednConfig fhrdEarndednConfig);

    void remove(FhrdEarndednConfig fhrdEarndednConfig);

    FhrdEarndednConfig find(Object id);

    List<FhrdEarndednConfig> findAll();

    List<FhrdEarndednConfig> findRange(int[] range);

    int count();
    
}
