/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdDegreedtl;

/**
 *
 * @author anu
 */
@Local
public interface FhrdDegreedtlFacadeLocal {

    void create(FhrdDegreedtl fhrdDegreedtl);

    void edit(FhrdDegreedtl fhrdDegreedtl);

    void remove(FhrdDegreedtl fhrdDegreedtl);

    FhrdDegreedtl find(Object id);

    List<FhrdDegreedtl> findAll();

    List<FhrdDegreedtl> findRange(int[] range);

    int count();

    public List<FhrdDegreedtl> findSubjectList(BigDecimal p_dgrm_srg_key, Integer p_native_oum);

    public boolean isSubjectValid(Integer p_oum_unit_srno, String p_dgrm_unique_srno, String p_subject_code, String p_subject_name);

    public List<FhrdDegreedtl> findAll(Integer p_oum_unit_srno, BigDecimal p_dgrm_unique_srno, String p_subject_name);
}
