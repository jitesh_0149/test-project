/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdEarndednmst;

/**
 *
 * @author anu
 */
@Local
public interface FhrdEarndednmstFacadeLocal {

    void create(FhrdEarndednmst fhrdEarndednmst);

    void edit(FhrdEarndednmst fhrdEarndednmst);

    void remove(FhrdEarndednmst fhrdEarndednmst);

    FhrdEarndednmst find(Object id);

    List<FhrdEarndednmst> findAll();

    List<FhrdEarndednmst> findRange(int[] range);

    int count();

    public List<FhrdEarndednmst> find_Detail_List(BigDecimal p_edm_srg_key);

    public List<FhrdEarndednmst> find_Basecode(String p_start_date, String p_end_date, String p_basecd_flag, Integer p_oum_unit_srno);

    public List<FhrdEarndednmst> findAllforManageGroup(String p_rmgm_srg_key, Date p_manage_start_date, Date p_manage_end_date);

    public BigDecimal findAllAlternateEDCode(Integer p_alt_srno);

    public BigDecimal findMaxAlternateEDCode();

    public List<FhrdEarndednmst> findAll(Integer p_oum_unit_srno);

    public boolean isEdCodeDescExists(Integer p_oum_unit_srno, Date p_start_date, Date p_end_date, String p_ed_code, String p_ed_name);

    public List<FhrdEarndednmst> findAllAlternatives(Integer p_oum_unit_srno, BigDecimal p_edm_srg_key);

    public List<FhrdEarndednmst> findAllMonthlyVariableCodes(Integer p_oum_unit_srno);

    public List<FhrdEarndednmst> findAllMonthlyVariableCodes(Integer p_oum_unit_srno, String p_date);
}
