/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdContractMst;

/**
 *
 * @author anu
 */
@Local
public interface FhrdContractMstFacadeLocal {

    void create(FhrdContractMst fhrdContractMst);

    void edit(FhrdContractMst fhrdContractMst);

    void remove(FhrdContractMst fhrdContractMst);

    FhrdContractMst find(Object id);

    List<FhrdContractMst> findAll();

    List<FhrdContractMst> findRange(int[] range);

    int count();

    public List<FhrdContractMst> findAll(Integer p_oum_unit_srno);

    public FhrdContractMst find_contract_desc(Integer p_sys_user, Integer p_contract_Srno);

    public List<FhrdContractMst> findAllAlternateContracts(Integer p_alt_srno, Integer p_oum_unit_srno);

    public BigDecimal findMaxAltSrno();

    public List<FhrdContractMst> findContractListOuwise(Integer p_oum_unit_srno, String p_contract_type);
    
    public List<FhrdContractMst> findContractListOuwise(Integer p_oum_unit_srno, String p_contract_type, Date p_start_date, Date p_end_date);

    public List<FhrdContractMst> findSupplementContractListOuwise(Integer p_oum_unit_srno, String p_contract_Altlist);

    public boolean isContractDescExists(Integer p_oum_unit_srno, String p_contract_desc, Date p_start_date, Date p_end_date);
}
