/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.entities.FtasTourScheduleDtl;

/**
 *
 * @author anu
 */
@Stateless
public class FtasTourScheduleDtlFacade extends AbstractFacade<FtasTourScheduleDtl> implements FtasTourScheduleDtlFacadeLocal {

    
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FtasTourScheduleDtlFacade() {
        super(FtasTourScheduleDtl.class);
    }

    @Override
    public List<FtasTourScheduleDtl> findAll(Integer p_user_id, Date p_from_date, Date p_to_date, boolean p_active_only) {
        String v_query = "SELECT T.* "
                + " FROM   FTAS_TOUR_SCHEDULE_DTL T,FTAS_ABSENTEES A "
                + " WHERE   A.ABS_SRG_KEY=T.ABS_SRG_KEY "
                + " AND     ((T.FROM_DATETIME BETWEEN '" + DateTimeUtility.ChangeDateFormat(p_from_date, null) + "' AND '" + DateTimeUtility.ChangeDateFormat(p_to_date, null) + "') "
                + " OR      (T.TO_DATETIME   BETWEEN '" + DateTimeUtility.ChangeDateFormat(p_from_date, null) + "' AND '" + DateTimeUtility.ChangeDateFormat(p_to_date, null) + "')) "
                + " AND     A.USER_ID=" + p_user_id;
        if (p_active_only) {
            v_query += "  AND (A.APRVCOMM IS NULL OR A.APRVCOMM <> 'N' "
                    + " AND ( "
                    + " A.CABS_SRG_KEY IS NULL OR  "
                    + " (A.CABS_SRG_KEY IS NOT NULL AND (A.CABS_APRVCOMM IS NULL OR A.CABS_APRVCOMM = 'N')) "
                    + " ))  ";
        }
        v_query += " ORDER BY T.FROM_DATETIME";
        return em.createNativeQuery(v_query, FtasTourScheduleDtl.class).getResultList();
    }
}
