/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdPaytransAttdCalcLog;

/**
 *
 * @author anu
 */
@Local
public interface FhrdPaytransAttdCalcLogFacadeLocal {

    void create(FhrdPaytransAttdCalcLog fhrdPaytransAttdCalcLog);

    void edit(FhrdPaytransAttdCalcLog fhrdPaytransAttdCalcLog);

    void remove(FhrdPaytransAttdCalcLog fhrdPaytransAttdCalcLog);

    FhrdPaytransAttdCalcLog find(Object id);

    List<FhrdPaytransAttdCalcLog> findAll();

    List<FhrdPaytransAttdCalcLog> findRange(int[] range);

    int count();

    public List<FhrdPaytransAttdCalcLog> findAllByAttdSrgKey(BigDecimal p_attd_srg_key);
    
    public BigDecimal getAttdSrgKey();
}
