/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FtasLockMst;

/**
 *
 * @author anu
 */
@Local
public interface FtasLockMstFacadeLocal {

    void create(FtasLockMst ftasLockMst);

    void edit(FtasLockMst ftasLockMst);

    void remove(FtasLockMst ftasLockMst);

    FtasLockMst find(Object id);

    List<FtasLockMst> findAll();

    List<FtasLockMst> findRange(int[] range);

    int count();
    
}
