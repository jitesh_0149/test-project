/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FtasLockDtl;

/**
 *
 * @author anu
 */
@Local
public interface FtasLockDtlFacadeLocal {

    void create(FtasLockDtl ftasLockDtl);

    void edit(FtasLockDtl ftasLockDtl);

    void remove(FtasLockDtl ftasLockDtl);

    FtasLockDtl find(Object id);

    List<FtasLockDtl> findAll();

    List<FtasLockDtl> findRange(int[] range);

    int count();
    
    public Date findMinLockDate(String p_oum_unit_srno);
}
