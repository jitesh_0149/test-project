/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FtasCancelabs;

/**
 *
 * @author anu
 */
@Local
public interface FtasCancelabsFacadeLocal {

    void create(FtasCancelabs ftasCancelabs);

    void edit(FtasCancelabs ftasCancelabs);

    void remove(FtasCancelabs ftasCancelabs);

    FtasCancelabs find(Object id);

    List<FtasCancelabs> findAll();

    List<FtasCancelabs> findRange(int[] range);

    int count();
    
    public List<FtasCancelabs> findAll_ApprovalList(Integer p_userid);
    
}
