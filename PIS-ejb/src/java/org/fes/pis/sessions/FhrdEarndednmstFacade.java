/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.entities.FhrdEarndednmst;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdEarndednmstFacade extends AbstractFacade<FhrdEarndednmst> implements FhrdEarndednmstFacadeLocal {

    
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdEarndednmstFacade() {
        super(FhrdEarndednmst.class);
    }

    @Override
    public List<FhrdEarndednmst> find_Detail_List(BigDecimal p_edm_srg_key) {
        return em.createNativeQuery("select * from fhrd_earndednmst "
                + " where EDM_SRG_KEY=" + p_edm_srg_key, FhrdEarndednmst.class).getResultList();
    }

    @Override
    public List<FhrdEarndednmst> find_Basecode(String p_start_date, String p_end_date, String p_basecd_flag, Integer p_oum_unit_srno) {
        String v_query = "SELECT * FROM  FHRD_EARNDEDNMST"
                + " WHERE basecode_flag = '" + p_basecd_flag + "'"
                + " and   oum_unit_srno in (select * from table(ou_pack.get_sub_ou(ou_pack.get_org_srno(17),'b','y')))"
                + " AND ((TO_DATE(TO_CHAR(START_DATE,'DD-MON-YYYY')) BETWEEN '" + p_start_date + "' AND '";
        if (p_end_date == null) {
            v_query += "')";
        } else {
            v_query += p_end_date + "')";
        }
        v_query += " OR (TO_DATE(TO_CHAR(END_DATE,'DD-MON-YYYY')) BETWEEN TO_DATE(TO_CHAR('" + p_start_date + "','DD-MON-YYYY')) AND '";
        if (p_end_date == null) {
            v_query += "')";
        } else {
            v_query += p_end_date + "')";
        }
        v_query += " OR ('" + p_start_date + "' BETWEEN TO_DATE(TO_CHAR(START_DATE,'DD-MON-YYYY')) AND TO_DATE(TO_CHAR(END_DATE,'DD-MON-YYYY')))"
                + " OR ('";
        if (p_end_date == null) {
            v_query += "'";
        } else {
            v_query += p_end_date + "'";
        }
        v_query += "BETWEEN TO_DATE(TO_CHAR(START_DATE,'DD-MON-YYYY')) AND TO_DATE(TO_CHAR(END_DATE,'DD-MON-YYYY')))"
                + " OR (END_DATE IS NULL "
                + " AND ('" + p_start_date + "' >= TO_DATE(TO_CHAR(START_DATE,'DD-MON-YYYY'))"
                + " OR '";
        if (p_end_date == null) {
            v_query += "";
        } else {
            v_query += p_end_date;
        }
        v_query += "' >= TO_DATE(TO_CHAR(START_DATE,'DD-MON-YYYY'))"
                + " )"
                + " )"
                + " )";
        return em.createNativeQuery(v_query, FhrdEarndednmst.class).getResultList();
    }

    @Override
    public List<FhrdEarndednmst> findAllforManageGroup(String p_rmgm_srg_key, Date p_manage_start_date, Date p_manage_end_date) {
        String v_query = "SELECT * FROM FHRD_EARNDEDNMST "
                + " WHERE EDM_alt_srno NOT IN (SELECT EDM_ALT_SRNO "
                + " FROM FHRD_EARNDEDN WHERE (RMGM_SRG_KEY = " + p_rmgm_srg_key + " AND END_DATE IS NULL))";
        //+ " AND EDM_SRG_KEY NOT IN (SELECT EDM_SRG_KEY "
        //+ " FROM FHRD_EARNDEDN WHERE RMGM_SRG_KEY=" + p_rmgm_srg_key + ") ";
        if (p_manage_end_date != null) {
            v_query += " AND START_DATE<= '" + DateTimeUtility.ChangeDateFormat(p_manage_end_date, null) + "'";
        }
//        v_query += " AND (END_DATE>= '" + DateTimeUtility.ChangeDateFormat(p_manage_start_date, null) + "'  OR END_DATE IS NULL) "
        v_query += " ORDER BY EARNDEDNCD";
//        System.out.println("query "+v_query);
        return em.createNativeQuery(v_query, FhrdEarndednmst.class).getResultList();
    }

    @Override
    public BigDecimal findAllAlternateEDCode(Integer p_alt_srno) {
        return (BigDecimal) em.createNativeQuery("SELECT max(EDM_ALT_SUBSRNO)"
                + " FROM FHRD_EARNDEDNMST WHERE EDM_ALT_SRNO=" + p_alt_srno).getSingleResult();
    }

    @Override
    public BigDecimal findMaxAlternateEDCode() {
        return (BigDecimal) em.createNativeQuery("SELECT nvl(max(EDM_ALT_SRNO),0)"
                + " FROM FHRD_EARNDEDNMST ").getSingleResult();
    }

    @Override
    public List<FhrdEarndednmst> findAll(Integer p_oum_unit_srno) {
        return em.createNativeQuery(
                " SELECT * FROM FHRD_EARNDEDNMST "
                + " WHERE OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_EARNDEDNMST'," + p_oum_unit_srno + ") "
                + "ORDER BY EARNDEDNCD", FhrdEarndednmst.class).getResultList();
    }

    @Override
    public boolean isEdCodeDescExists(Integer p_oum_unit_srno, Date p_start_date, Date p_end_date, String p_ed_code, String p_ed_name) {
        String v_start_date = DateTimeUtility.ChangeDateFormat(p_start_date, null);
        String v_end_date = DateTimeUtility.ChangeDateFormat(p_end_date, null);
        String v_query = "SELECT COUNT(*)  FROM FHRD_EARNDEDNMST "
                + " WHERE (EARNDEDNCD= '" + p_ed_code + "' OR EARNDEDN_NAME='" + p_ed_name + "')"
                + " AND OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_EARNDEDNMST'," + p_oum_unit_srno + ")"
                + " AND ((END_DATE>='" + v_start_date + "' OR END_DATE IS NULL) ";
        if (p_end_date != null) {
            v_query += " AND START_DATE<='" + v_end_date + "') ";
        } else {
            v_query += ")";
        }
        BigDecimal v_total_count = (BigDecimal) em.createNativeQuery(v_query).getSingleResult();
        return !v_total_count.equals(BigDecimal.ZERO);
    }

    @Override
    public List<FhrdEarndednmst> findAllAlternatives(Integer p_oum_unit_srno, BigDecimal p_edm_srg_key) {
        String v_query = " SELECT * FROM FHRD_EARNDEDNMST "
                + " WHERE OUM_UNIT_SRNO IN (OU_PACK.GET_OUM_UNIT_SRNO('FHRD_EARNDEDNMST'," + p_oum_unit_srno + ")) "
                + " AND EDM_ALT_SRNO=(SELECT EDM_ALT_SRNO FROM FHRD_EARNDEDNMST WHERE EDM_SRG_KEY=" + p_edm_srg_key + ") ";
        return em.createNativeQuery(v_query, FhrdEarndednmst.class).getResultList();
    }

    @Override
    public List<FhrdEarndednmst> findAllMonthlyVariableCodes(Integer p_oum_unit_srno) {
        String v_query = "SELECT *"
                + " FROM FHRD_EARNDEDNMST"
                + " WHERE PAYABLE_ON_VALUE = 'MV' "
                + " AND OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('FHRD_EARNDEDNMST'," + p_oum_unit_srno + ")"
                + " ORDER BY EARNDEDNCD";
//        System.out.println("query "+v_query);
        return em.createNativeQuery(v_query, FhrdEarndednmst.class).getResultList();
    }

    @Override
    public List<FhrdEarndednmst> findAllMonthlyVariableCodes(Integer p_oum_unit_srno, String p_date) {
        String v_string = " SELECT * "
                + " FROM   FHRD_EARNDEDNMST"
                + " WHERE EDM_SRG_KEY IN"
                + " (SELECT DISTINCT EDM_SRG_KEY FROM FHRD_EMPEARNDEDN"
                + " WHERE  USER_ID IN ("
                + "          SELECT USER_ID FROM FHRD_EMPMST "
                + "          WHERE  POSTING_OUM_UNIT_SRNO"
                + "          IN (SELECT * FROM TABLE(OU_PACK.GET_SUB_OU(" + p_oum_unit_srno + ",'D','Y'))))"
                + " AND    '" + p_date + "' BETWEEN OPENING_DATE AND CLOSING_DATE"
                + " AND    PAYABLE_ON_VALUE='MV')"
                + " ORDER BY EARNDEDNCD";
//        System.out.println("query "+v_string);
        return em.createNativeQuery(v_string, FhrdEarndednmst.class).getResultList();
    }
}
