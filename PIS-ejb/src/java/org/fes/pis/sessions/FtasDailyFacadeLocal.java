/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FtasDaily;

/**
 *
 * @author anu
 */
@Local
public interface FtasDailyFacadeLocal {

    void create(FtasDaily ftasDaily);

    void edit(FtasDaily ftasDaily);

    void remove(FtasDaily ftasDaily);

    FtasDaily find(Object id);

    List<FtasDaily> findAll();

    List<FtasDaily> findRange(int[] range);

    int count();

    public Integer checkTodaysAtt(Integer p_userid, Date p_date);

    public FtasDaily find(Integer p_userid, Date p_date);

    public List<FtasDaily> findAll(Integer p_user_id, Date p_from_date, Date p_to_date);
}
