/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.SystUserTypeMst;

/**
 *
 * @author anu
 */
@Local
public interface SystUserTypeMstFacadeLocal {

    void create(SystUserTypeMst systUserTypeMst);

    void edit(SystUserTypeMst systUserTypeMst);

    void remove(SystUserTypeMst systUserTypeMst);

    SystUserTypeMst find(Object id);

    List<SystUserTypeMst> findAll();

    List<SystUserTypeMst> findRange(int[] range);

    int count();

    public List<SystUserTypeMst> findAll(Integer p_oum_unit_srno, boolean exceptSystemUser);
}
