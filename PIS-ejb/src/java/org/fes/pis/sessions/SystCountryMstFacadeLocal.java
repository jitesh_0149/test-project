/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.SystCountryMst;

/**
 *
 * @author fes
 */
@Local
public interface SystCountryMstFacadeLocal {

    void create(SystCountryMst systCountryMst);

    void edit(SystCountryMst systCountryMst);

    void remove(SystCountryMst systCountryMst);

    SystCountryMst find(Object id);

    List<SystCountryMst> findAll();

    List<SystCountryMst> findRange(int[] range);

    int count();

    public List<SystCountryMst> findAll(Integer p_oum_unit_srno, String p_country_name);
}
