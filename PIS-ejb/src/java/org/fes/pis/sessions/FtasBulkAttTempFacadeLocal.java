/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FtasBulkAttTemp;

/**
 *
 * @author anu
 */
@Local
public interface FtasBulkAttTempFacadeLocal {

    void create(FtasBulkAttTemp ftasBulkAttTemp);

    void edit(FtasBulkAttTemp ftasBulkAttTemp);

    void remove(FtasBulkAttTemp ftasBulkAttTemp);

    FtasBulkAttTemp find(Object id);

    List<FtasBulkAttTemp> findAll();

    List<FtasBulkAttTemp> findRange(int[] range);

    int count();
    
    public List<FtasBulkAttTemp> findAllUnsavedRecord(BigDecimal p_bat_srno);
    
}
