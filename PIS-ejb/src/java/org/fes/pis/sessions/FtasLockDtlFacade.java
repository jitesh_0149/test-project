/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FtasLockDtl;

/**
 *
 * @author anu
 */
@Stateless
public class FtasLockDtlFacade extends AbstractFacade<FtasLockDtl> implements FtasLockDtlFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FtasLockDtlFacade() {
        super(FtasLockDtl.class);
    }

    @Override
    public Date findMinLockDate(String p_oum_unit_srno) {
        List<Date> v_return_date = null;
        String v_query;
//        String v_query = "SELECT NVL(MIN_DATE,NULL) VDATE FROM "
//                + " (SELECT USER_ID,MAX(TO_DATE) MIN_DATE"
//                + " FROM   FTAS_LOCK_DTL"
//                + " WHERE  USER_ID IN (SELECT USER_ID FROM FHRD_EMPMST WHERE POSTING_OUM_UNIT_SRNO =" + p_oum_unit_srno + ")"
//                + " GROUP  BY USER_ID"
//                + " ORDER BY 2)"
//                + " WHERE ROWNUM=1";
////        System.out.println("1---> " + v_query);
//        //System.out.println("dhf "+em.createNativeQuery(v_query).getSingleResult());
//        v_return_date = em.createNativeQuery(v_query).getResultList();
//        if (v_return_date.isEmpty()) {
//            v_query = "";
        v_query = "SELECT MAX(TO_DATE) "
                + "     FROM   FTAS_LOCK_MST"
                + "     WHERE  LOCK_STATUS = 'Y'"
                + "     AND    OUM_UNIT_SRNO = Ou_Pack.GET_OUM_UNIT_SRNO('FTAS_LOCK_MST'," + p_oum_unit_srno + ")";
        v_return_date = em.createNativeQuery(v_query).getResultList();
//        }
        return v_return_date.get(0);

    }
}
