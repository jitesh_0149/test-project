/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.SystUserTypeMst;

/**
 *
 * @author anu
 */
@Stateless
public class SystUserTypeMstFacade extends AbstractFacade<SystUserTypeMst> implements SystUserTypeMstFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SystUserTypeMstFacade() {
        super(SystUserTypeMst.class);
    }

    @Override
    public List<SystUserTypeMst> findAll(Integer p_oum_unit_srno, boolean exceptSystemUser) {
        String v_query = " select * from syst_user_type_mst"
                + " where oum_unit_srno= OU_PACK.GET_ORG_SRNO(" + p_oum_unit_srno + ")";
        if (exceptSystemUser) {
            v_query += "  AND USER_TYPE_DESC <> 'SYSTEM'";
        }
        v_query += " ORDER BY USER_TYPE_DESC";

        return em.createNativeQuery(v_query, SystUserTypeMst.class).getResultList();
    }
}
