/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FtasHolidayMst;

/**
 *
 * @author anu
 */
@Local
public interface FtasHolidayMstFacadeLocal {

    void create(FtasHolidayMst ftasHolidayMst);

    void edit(FtasHolidayMst ftasHolidayMst);

    void remove(FtasHolidayMst ftasHolidayMst);

    FtasHolidayMst find(Object id);

    List<FtasHolidayMst> findAll();

    List<FtasHolidayMst> findRange(int[] range);

    int count();
    
}
