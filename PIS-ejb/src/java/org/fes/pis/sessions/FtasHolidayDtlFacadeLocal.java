/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FtasHolidayDtl;

/**
 *
 * @author anu
 */
@Local
public interface FtasHolidayDtlFacadeLocal {

    void create(FtasHolidayDtl ftasHolidayDtl);

    void edit(FtasHolidayDtl ftasHolidayDtl);

    void remove(FtasHolidayDtl ftasHolidayDtl);

    FtasHolidayDtl find(Object id);

    List<FtasHolidayDtl> findAll();

    List<FtasHolidayDtl> findRange(int[] range);

    int count();

    public List<FtasHolidayDtl> findHolidayYearWise(Integer p_year, Integer p_native_ou);

//    public List<String> findWeekendDays(Integer p_year);
    public List<String> findWeekendDays(Integer p_year);

    public List<FtasHolidayDtl> findAll(Integer p_oum_unit_srno);

    public List<String> getYearListForHolidayProcess(Integer p_oum_unit_srno);

    public List<String> findAllHoliDesc(Integer p_year);
}