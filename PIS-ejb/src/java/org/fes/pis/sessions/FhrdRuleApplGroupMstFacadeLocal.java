/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.FhrdRuleApplGroupMst;

/**
 *
 * @author anu
 */
@Local
public interface FhrdRuleApplGroupMstFacadeLocal {

    void create(FhrdRuleApplGroupMst fhrdRuleApplGroupMst);

    void edit(FhrdRuleApplGroupMst fhrdRuleApplGroupMst);

    void remove(FhrdRuleApplGroupMst fhrdRuleApplGroupMst);

    FhrdRuleApplGroupMst find(Object id);

    List<FhrdRuleApplGroupMst> findAll();

    List<FhrdRuleApplGroupMst> findRange(int[] range);

    int count();

    public List<FhrdRuleApplGroupMst> find_applicable_group(String p_contract_type);

    public List<FhrdRuleApplGroupMst> findAll(Integer p_oum_unit_srno);

    public boolean isApplGroupDescExists(Integer p_oum_unit_srno, String p_appl_group_desc, Date p_start_date, Date p_end_date);
}
