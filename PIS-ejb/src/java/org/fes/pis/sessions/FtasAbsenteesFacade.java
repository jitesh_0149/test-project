/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.entities.FtasAbsentees;

/**
 *
 * @author anu
 */
@Stateless
public class FtasAbsenteesFacade extends AbstractFacade<FtasAbsentees> implements FtasAbsenteesFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FtasAbsenteesFacade() {
        super(FtasAbsentees.class);
    }

    @Override
    public List<FtasAbsentees> find_Approval_List(Integer p_userid, Integer p_oum, Integer p_srno) {
        return em.createNativeQuery(
                "select * from Ftas_Absentees "
                + " WHERE user_id = " + p_userid
                + " and oum_Unit_Srno= " + p_oum
                + " and srno= " + p_srno, FtasAbsentees.class).getResultList();
    }

    @Override
    public List<String> getAppliedApplication(Integer p_userid, Integer p_org_unit_srno) {
        return em.createNativeQuery(
                "SELECT TO_CHAR(d.from_date,'DD-Mon-YYYY')||':'||decode(d.FROMHALF,0,'FullDay',1,'Half-1',2,'Half-2')||':'|| TO_CHAR(e.TO_DATE,'DD-Mon-YYYY')||':'||decode(e.TOHALF,0,'FullDay',1,'Half-1',2,'Half-2')||':'||nvl2(d.APRVBY,'Approved','Pipeline') appldate"
                + " FROM FTAS_ABSENTEES d , FTAS_ABSENTEES e"
                + " WHERE d.user_id= " + p_userid + " AND d.from_date IN"
                + " ( Select min(A.from_date) from FTAS_ABSENTEES A"
                + " Where A.user_id= " + p_userid + " AND (A.user_id, A.SRNO)"
                + " NOT IN (Select B.user_id, B.SRNO from FTAS_CANCELABS B"
                + " Where B.user_id= " + p_userid + " and (B.APRVCOMM ='Y' or B.APRVCOMM is NULL) )"
                + " and A.CANCEL_SRNO is null group by A.OUM_UNIT_SRNO,A.user_id,A.SRNO )"
                + " AND  e.TO_DATE IN ( Select max(A.to_date) from FTAS_ABSENTEES A"
                + " Where A.user_id=  " + p_userid + " AND (A.user_id, A.SRNO)"
                + " NOT IN (Select B.user_id, B.SRNO from FTAS_CANCELABS B"
                + " Where B.user_id= " + p_userid + " and (B.APRVCOMM ='Y' or B.APRVCOMM is NULL) )"
                + " And A.CANCEL_SRNO is null group by A.OUM_UNIT_SRNO,A.user_id,A.SRNO )"
                + " AND D.OUM_UNIT_SRNO = e.OUM_UNIT_SRNO AND D.user_id= E.user_id AND"
                + " D.SRNO= E.SRNO and d.from_date >= add_months(sysdate,-12)"
                + " and D.OUM_UNIT_SRNO in (select * from table(ou_pack.get_sub_ou(" + p_org_unit_srno + ",'b','y')))"
                + " ORDER BY d.from_date DESC").getResultList();
    }

    @Override
    public Integer getMaxSrno_ftasAbsentees(Integer p_userid, Integer p_user_oum) {
        return (Integer) em.createNativeQuery(
                " select nvl(max(srno),0) srno"
                + " from FTAS_ABSENTEES"
                + " where user_id=" + p_userid + ""
                + " and OUM_UNIT_SRNO=" + p_user_oum + ""
                + " order by createdt").getSingleResult();
    }

    @Override
    public Integer getMaxCancelSrnoFtasAbsentees(Integer p_userid, Integer p_oum_unit_srno) {
        return (Integer) em.createNativeQuery(
                " select nvl(max(CANCEL_SRNO),0) CANCEL_SRNO"
                + " from FES_PROJECT.FTAS_ABSENTEES"
                + " where user_id=" + p_userid + ""
                + " and OUM_UNIT_SRNO=" + p_oum_unit_srno
                + " order by createdt").getSingleResult();
    }

    @Override
    public List<FtasAbsentees> findAll_ApprovalList(Integer p_userid) {
        String v_query = " SELECT * "
                + " FROM   FTAS_ABSENTEES "
                + " WHERE  ACTNBY = " + p_userid
                + " AND    (APRVCOMM IS NULL OR (APRVCOMM='Y' AND CABS_SRG_KEY IS NOT NULL AND CABS_APRVCOMM IS NULL)) ";
        return em.createNativeQuery(v_query, FtasAbsentees.class).getResultList();
    }

    @Override
    public List<FtasAbsentees> findAll_ApprovedApplList(Integer p_userid, Integer p_oum_unit_srno, boolean p_partial_allowed) {
        String v_query = " SELECT * FROM FTAS_ABSENTEES  "
                + " WHERE USER_ID= " + p_userid
                + " AND OUM_UNIT_SRNO= " + p_oum_unit_srno
                + " AND APRVCOMM='Y'"
                + " AND CABS_SRG_KEY IS NULL";
        if (p_partial_allowed) {
            v_query += "  AND TO_DATE > (SELECT FTAS_PACK.GET_LAST_LOCKED_DATE(" + p_userid + "," + p_oum_unit_srno + " ) FROM DUAL)";
        } else {

            v_query += " AND FROM_DATE > (SELECT FTAS_PACK.GET_LAST_LOCKED_DATE(" + p_userid + "," + p_oum_unit_srno + " ) FROM DUAL)";
        }

        return em.createNativeQuery(v_query, FtasAbsentees.class).getResultList();
    }

    @Override
    public BigDecimal getLeaveRatio(BigDecimal p_emplb_srg_key, BigDecimal p_parent_emplb_srg_key) {
        return (BigDecimal) em.createNativeQuery("SELECT  FTAS_ATTD_CALC.GET_LEAVE_RATIO(" + p_emplb_srg_key + "," + p_parent_emplb_srg_key + ") FROM DUAL").getSingleResult();
    }

    @Override
    public BigDecimal checkForWhileOnTourTraining(Integer p_user_id, Date p_from_date, Date p_to_date) {
        String v_from_date = DateTimeUtility.ChangeDateFormat(p_from_date, null);
        String v_to_date = DateTimeUtility.ChangeDateFormat(p_to_date, null);

        String v_query = "SELECT COUNT(*) "
                + " FROM   FTAS_ABSENTEES "
                + " WHERE  USER_ID=" + p_user_id + ""
                + " AND    APPLICATION_FLAG IN ('T','R') "
                + " AND    (FROM_DATE<='" + v_from_date + "' AND TO_DATE>='" + v_to_date + "') "
                + " AND (APRVCOMM = 'Y' "
                + " AND ( "
                + " CABS_SRG_KEY IS NULL OR "
                + " (CABS_APRVCOMM = 'Y') "
                + " ) "
                + " )";

        BigDecimal v_total_count = (BigDecimal) em.createNativeQuery(v_query).getSingleResult();
        return v_total_count;
    }

    @Override
    public List<FtasAbsentees> findAll(Integer p_oum_unit_srno, Integer p_user_id, boolean p_native_level, boolean p_org_level, Date p_from_date, Date p_to_date, String p_lm_srg_key, boolean p_org_access_rights, String p_application_flag, boolean p_active_only, String p_order_by) {
        String v_query = "SELECT * FROM FTAS_ABSENTEES ";
        String v_from_date, v_to_date;
        if (p_org_level) {
            v_query += " WHERE OUM_UNIT_SRNO IN (SELECT * FROM TABLE(OU_PACK.GET_SUB_OU(OU_PACK.GET_ORG_SRNO(" + p_oum_unit_srno + "),'D','Y')))";
        } else if (p_native_level) {
            v_query += " WHERE (OUM_UNIT_SRNO IN (SELECT * FROM TABLE(OU_PACK.GET_SUB_OU_DOWN_LEVEL(OU_PACK.GET_NATIVE_OU(" + p_oum_unit_srno + "),'Y',2,'Y')))";
            if (p_org_access_rights) {
                v_query += " OR APRVBY = " + p_user_id;
            }
            v_query += ")";
        } else {
            v_query += " WHERE OUM_UNIT_SRNO=OU_PACK.GET_OUM_UNIT_SRNO('FTAS_ABSENTEES'," + p_oum_unit_srno + ")";
        }
        if (p_user_id != null && p_org_access_rights == false) {
            v_query += " AND USER_ID=" + p_user_id;
        }
        if (p_from_date != null && p_to_date != null) {
            v_from_date = DateTimeUtility.ChangeDateFormat(p_from_date, null);
            v_to_date = DateTimeUtility.ChangeDateFormat(p_to_date, null);
            v_query += " AND ((FROM_DATE BETWEEN TO_DATE('" + v_from_date + "','DD-MON-YYYY')"
                    + " AND TO_DATE('" + v_to_date + "','DD-MON-YYYY'))"
                    + " OR (TO_DATE BETWEEN TO_DATE('" + v_from_date + "','DD-MON-YYYY')"
                    + " AND TO_DATE('" + v_to_date + "','DD-MON-YYYY')))";
        }
        if (p_application_flag != null) {
            v_query += " AND APPLICATION_FLAG='" + p_application_flag + "' ";
        }

        if (p_active_only) {
            v_query += "  AND (APRVCOMM IS NULL OR APRVCOMM <> 'N' "
                    + " AND ( "
                    + " CABS_SRG_KEY IS NULL OR  "
                    + " (CABS_SRG_KEY IS NOT NULL AND (CABS_APRVCOMM IS NULL OR CABS_APRVCOMM = 'N')) "
                    + " ))  ";
        }

        if (p_lm_srg_key != null) {
            v_query += " AND ABS_SRG_KEY IN"
                    + "     ( SELECT ABS_SRG_KEY FROM FTAS_ABSENTEES_DTL WHERE "
                    + "     (LM_SRG_KEY = " + p_lm_srg_key + " OR ACTUAL_LM_SRG_KEY= " + p_lm_srg_key
                    + "     OR PARENT_LM_SRG_KEY= " + p_lm_srg_key + " OR CHILD_LM_SRG_KEY= " + p_lm_srg_key
                    + "     ))";
        }
        if (p_order_by == null) {
            v_query += " ORDER BY FROM_DATE";
        } else {
            v_query += " ORDER BY " + p_order_by;
        }
        return em.createNativeQuery(v_query, FtasAbsentees.class).getResultList();
    }

    @Override
    public List<FtasAbsentees> findAllForTABill(Integer p_user_id) {
        String v_query = "SELECT * FROM FTAS_ABSENTEES "
                + " WHERE USER_ID=" + p_user_id
                + " AND APPLICATION_FLAG IN ('T','R') "
                + " AND (APRVCOMM = 'Y' "
                + " AND ( "
                + " CABS_SRG_KEY IS NULL OR  "
                + " (CABS_SRG_KEY IS NOT NULL AND CABS_APRVCOMM = 'N')) "
                + " )  "
                + " ORDER BY FROM_DATE";
        return em.createNativeQuery(v_query, FtasAbsentees.class).getResultList();
    }
}
