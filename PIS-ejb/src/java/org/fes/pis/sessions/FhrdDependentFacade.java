/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdDependent;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdDependentFacade extends AbstractFacade<FhrdDependent> implements FhrdDependentFacadeLocal {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdDependentFacade() {
        super(FhrdDependent.class);
    }

    @Override
    public List<FhrdDependent> findUseridwise(Integer p_userid) {
        return em.createNativeQuery("SELECT * FROM FHRD_DEPENDENT WHERE USER_ID=" + p_userid, FhrdDependent.class).getResultList();
    }

    @Override
    public List<String> findAllOccupation(Integer p_oum_unit_srno) {
        return (List<String>) em.createNativeQuery("SELECT DISTINCT OCCUPATION  FROM FHRD_DEPENDENT WHERE OUM_UNIT_SRNO IN (select * from table(ou_pack.get_sub_ou(ou_pack.get_org_srno(" + p_oum_unit_srno + "),'d','y'))) ORDER BY OCCUPATION").getResultList();
    }
}
