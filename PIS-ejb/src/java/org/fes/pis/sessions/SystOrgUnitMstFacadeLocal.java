/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import java.util.List;
import javax.ejb.Local;
import org.fes.pis.entities.SystOrgUnitMst;

/**
 *
 * @author anu
 */
@Local
public interface SystOrgUnitMstFacadeLocal {

    void create(SystOrgUnitMst systOrgUnitMst);

    void edit(SystOrgUnitMst systOrgUnitMst);

    void remove(SystOrgUnitMst systOrgUnitMst);

    SystOrgUnitMst find(Object id);

    List<SystOrgUnitMst> findAll();

    List<SystOrgUnitMst> findRange(int[] range);

    int count();

    public List<SystOrgUnitMst> findAllForUser(Integer p_user_id, Integer p_oum_unit_srno);

    public String findOuName(Integer p_oum_unit_srno);

    public SystOrgUnitMst getOrganisation(Integer p_oum_unit_srno);

    public List<SystOrgUnitMst> findPostingOuForNewEmployee(Integer p_oum_unit_srno);

    public List<SystOrgUnitMst> findNativeOuForMonVar(Integer p_oum_unit_srno);

    public List<SystOrgUnitMst> findAllSubOuDownLevel(Integer p_working_ou);

    public List<SystOrgUnitMst> findAll(Integer p_oum_unit_srno, boolean subOu, boolean subOuDownLevel, boolean nativeSubOu, boolean nativeSubOuDownLevel, boolean orgSubOu, Integer p_access_span_user_id, String p_access_span_flag);
}
