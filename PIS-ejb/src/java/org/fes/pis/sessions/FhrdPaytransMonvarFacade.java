/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.sessions;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.fes.pis.entities.FhrdPaytransMonvar;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdPaytransMonvarFacade extends AbstractFacade<FhrdPaytransMonvar> implements FhrdPaytransMonvarFacadeLocal {
    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdPaytransMonvarFacade() {
        super(FhrdPaytransMonvar.class);
    }
    
}
