/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.db.fes_project.pack;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.Database_Output;
import org.fes.pis.custom_entities.System_Properties;

/**
 *
 * @author anu
 */
public class Fhrd_Paycalc implements Serializable{

    String systemName = new System_Properties().getSystemName();

    

    public Database_Output Calculate_Salary_Proc(String calling_class,
            String calling_method,
            BigDecimal p_salary_srg_key,
            String p_user_list,
            Integer p_oum_unit_srno,
            Date p_salary_from_date,
            Date p_salary_to_date,
            Date p_att_from_date,
            Date p_att_to_date,
            String p_att_from_effect_time,
            String p_att_to_effect_time,
            String p_process_by,
            String p_calc_force_flag) {
        Database_Output ent_Database_Output = new Database_Output(false);
        Connection cn = null;
        CallableStatement CALL_PROCESS = null;
        System.out.println("");
        System.out.println("1--> " + p_user_list);
        System.out.println("2--> " + p_oum_unit_srno);
        System.out.println("3--> " + p_salary_from_date);
        System.out.println("4--> " + p_salary_to_date);
        System.out.println("5--> " + p_att_from_date);
        System.out.println("6--> " + p_att_to_date);
        System.out.println("7--> " + p_att_from_effect_time);
        System.out.println("8--> " + p_att_to_effect_time);
        System.out.println("9--> " + p_process_by);        
        System.out.println("10--> " + p_calc_force_flag);
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            cn.setAutoCommit(false);
            cn.setSavepoint();
            CALL_PROCESS = cn.prepareCall("{call FHRD_PAY_CALC.CalculatePay(?,?,?,?,?,?,?,?,?,?,?)}");
            CALL_PROCESS.setBigDecimal(1, p_salary_srg_key);
            CALL_PROCESS.setString(2, p_user_list);
            CALL_PROCESS.setInt(3, p_oum_unit_srno);
            CALL_PROCESS.setDate(4, new java.sql.Date(p_salary_from_date.getTime()));
            CALL_PROCESS.setDate(5, new java.sql.Date(p_salary_to_date.getTime()));
            CALL_PROCESS.setDate(6, new java.sql.Date(p_att_from_date.getTime()));
            CALL_PROCESS.setDate(7, new java.sql.Date(p_att_to_date.getTime()));
            CALL_PROCESS.setString(8, p_att_from_effect_time);
            CALL_PROCESS.setString(9, p_att_to_effect_time);
            CALL_PROCESS.setString(10, p_process_by);
            CALL_PROCESS.setString(11, p_calc_force_flag);

            try {
                CALL_PROCESS.executeUpdate();
                cn.commit();
                ent_Database_Output.setExecuted_successfully(true);
            } catch (SQLException ex) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Calculate_Salary_Proc", calling_class,
                        calling_method, null, ex);
            }
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Calculate_Salary_Proc", calling_class,
                    calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Calculate_Salary_Proc", calling_class,
                    calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        CALL_PROCESS.close();
                    }
                } catch (Exception ex) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Calculate_Salary_Proc", calling_class,
                            calling_method, null, ex);
                }
            }
        }
        return ent_Database_Output;


    }

    // <editor-fold defaultstate="collapsed" desc="getJdbcFES_PROJECT_Oracle()">
    private DataSource getJdbcFES_PROJECT_Oracle() throws NamingException {
        Context c = new InitialContext();
        return (DataSource) c.lookup("java:comp/env/jdbcFES_PROJECT_Oracle");
    }// </editor-fold>
}
