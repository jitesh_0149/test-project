package org.fes.pis.db.fes_project.pack;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.fes.pis.custom_entities.Database_Output;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;

public class Fcad_Pack implements Serializable{

    String systemName = new System_Properties().getSystemName();

    
    // <editor-fold defaultstate="collapsed" desc="Insert_And_Get_AddressNo">

    public Database_Output Insert_And_Get_AddressNo(
            String p_calling_class,
            String p_calling_method,
            String p_org_name,
            String p_first_name,
            String p_middle_name,
            String p_last_name,
            String p_addr1,
            String p_addr2,
            String p_addr3,
            String p_city,
            String p_state,
            String p_pincode,
            String p_country,
            String p_email,
            String p_website,
            Integer p_processby,
            Integer p_oum_unit_srno) {
        Connection cn = null;
        CallableStatement Call_Insert_And_Get_AddressNo = null;
        Database_Output ent_Database_Output = new Database_Output(false);
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            Call_Insert_And_Get_AddressNo =
                    cn.prepareCall("{?= call fcad_pack.Insert_And_Get_AddressNo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            Call_Insert_And_Get_AddressNo.registerOutParameter(1, java.sql.Types.NUMERIC);
            Call_Insert_And_Get_AddressNo.setString(2, p_org_name);
            Call_Insert_And_Get_AddressNo.setString(3, p_first_name);
            Call_Insert_And_Get_AddressNo.setString(4, p_middle_name);
            Call_Insert_And_Get_AddressNo.setString(5, p_last_name);
            Call_Insert_And_Get_AddressNo.setString(6, p_addr1);
            Call_Insert_And_Get_AddressNo.setString(7, p_addr2);
            Call_Insert_And_Get_AddressNo.setString(8, p_addr3);
            Call_Insert_And_Get_AddressNo.setString(9, p_city);
            Call_Insert_And_Get_AddressNo.setString(10, p_state);
            Call_Insert_And_Get_AddressNo.setString(11,p_pincode);
            Call_Insert_And_Get_AddressNo.setString(12, p_country);
            Call_Insert_And_Get_AddressNo.setString(13, p_email);
            Call_Insert_And_Get_AddressNo.setString(14, p_website);
            Call_Insert_And_Get_AddressNo.setInt(15, p_processby);
            Call_Insert_And_Get_AddressNo.setInt(16, p_oum_unit_srno);
            Call_Insert_And_Get_AddressNo.executeUpdate();
            ent_Database_Output.setLong1(Call_Insert_And_Get_AddressNo.getLong(1));
            ent_Database_Output.setExecuted_successfully(true);
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, Fcad_Pack.class.getName(), "Insert_And_Get_AddressNo", p_calling_class, p_calling_method, "", ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, Fcad_Pack.class.getName(), "Insert_And_Get_AddressNo", p_calling_class, p_calling_method, "", ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        Call_Insert_And_Get_AddressNo.close();
                    }

                } catch (Exception e) {
                }
            }
        }
        return ent_Database_Output;
    }// </editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="Default Constructor">

    public Fcad_Pack() {
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="getJdbcFES_PROJECT_Oracle()">

    private DataSource getJdbcFES_PROJECT_Oracle() throws NamingException {
        Context c = new InitialContext();
        return (DataSource) c.lookup("java:comp/env/jdbcFES_PROJECT_Oracle");
    }// </editor-fold>
}
