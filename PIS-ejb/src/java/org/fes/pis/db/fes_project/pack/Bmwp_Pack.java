package org.fes.pis.db.fes_project.pack;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.fes.pis.custom_entities.Database_Output;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;

public class Bmwp_Pack implements Serializable{

    String systemName = new System_Properties().getSystemName();

    
    // <editor-fold defaultstate="collapsed" desc="Ins_Updt_project_mst">

    public Database_Output Ins_Updt_project_mst(
            String calling_class,
            String calling_method,
            Integer oum_unit_srno,
            Integer fin_year,
            String pm_proj_short_name,
            String pm_proj_name,
            String pm_fund_type,
            BigDecimal pm_unique_srno,
            BigDecimal pm_parent_unique_srno,
            Date pm_start_date,
            Date pm_end_date,
            String database_transaction,
            Integer processby,
            String pm_remarks) {
        Database_Output ent_Database_Output = new Database_Output(false);
        Connection cn = null;
        CallableStatement v_proc = null;
        String v_message = null;
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            cn.setAutoCommit(false);
            cn.setSavepoint();
            v_proc = cn.prepareCall("{call BMWP_PACK.Ins_Updt_Project_Mst(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            v_proc.setInt(1, oum_unit_srno);
            v_proc.setInt(2, fin_year);
            v_proc.setString(3, pm_proj_short_name);
            v_proc.setString(4, pm_proj_name);
            v_proc.setString(5, pm_fund_type);
            v_proc.setBigDecimal(6, pm_unique_srno);
            v_proc.setBigDecimal(7, pm_parent_unique_srno);
            v_proc.registerOutParameter(7, java.sql.Types.NUMERIC);
            v_proc.setDate(8, new java.sql.Date(pm_start_date.getTime()));
            if (pm_end_date == null) {
                v_proc.setDate(9, null);
            } else {
                v_proc.setDate(9, new java.sql.Date(pm_end_date.getTime()));
            }
            v_proc.setString(10, database_transaction);
            v_proc.setInt(11, processby);
            v_proc.setString(12, pm_remarks);
            v_proc.setString(13, null);
            v_proc.registerOutParameter(13, java.sql.Types.VARCHAR);
            v_proc.executeUpdate();
            v_message = v_proc.getString(13);
            ent_Database_Output.setString1(v_message);
            ent_Database_Output.setBigdecimal1(v_proc.getBigDecimal(7));
            if (v_message == null) {
                cn.commit();
            } else {
                cn.rollback();
            }
            ent_Database_Output.setExecuted_successfully(true);
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Ins_Updt_Project_Mst", calling_class,
                    calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Ins_Updt_Project_Mst", calling_class,
                    calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        v_proc.close();
                    }
                } catch (Exception ex) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Ins_Updt_Project_Mst", calling_class,
                            calling_method, null, ex);
                }
            }
        }
        return ent_Database_Output;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Ins_Updt_Activity_Mst">

    public Database_Output Ins_Updt_Activity_Mst(
            String calling_class,
            String calling_method,
            Integer oum_unit_srno,
            Integer fin_year,
            String activity_short_name,
            String activity_desc,
            Long am_um_unique_srno,
            BigDecimal am_unique_srno,
            BigDecimal am_parent_unique_srno,
            BigInteger am_activity_srno,
            Date start_date,
            Date end_date,
            String transcation_type,
            Integer proccessby) {
        boolean executed_successfully = false;
        Database_Output ent_Database_Output = new Database_Output();
        Connection cn = null;
        CallableStatement v_proc = null;
        String v_message = null;
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            cn.setAutoCommit(false);
            cn.setSavepoint();
            v_proc =
                    cn.prepareCall("{call BMWP_PACK.Ins_Updt_Activity_Mst(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            v_proc.setInt(1, oum_unit_srno);
            v_proc.setInt(2, fin_year);
            v_proc.setString(3, activity_short_name);
            v_proc.setString(4, activity_desc);
            v_proc.setLong(5, am_um_unique_srno);
            v_proc.setBigDecimal(6, am_unique_srno);
            v_proc.registerOutParameter(6, java.sql.Types.NUMERIC);
            v_proc.setBigDecimal(7, am_parent_unique_srno);
            v_proc.setBigDecimal(8, new BigDecimal(am_activity_srno.toString()));
            v_proc.setDate(9, new java.sql.Date(start_date.getTime()));
            if (end_date != null) {
                v_proc.setDate(10, new java.sql.Date(end_date.getTime()));
            } else {
                v_proc.setDate(10, null);
            }
            v_proc.setString(11, transcation_type);
            v_proc.setInt(12, proccessby);
            v_proc.setString(13, null);
            v_proc.registerOutParameter(13, java.sql.Types.VARCHAR);
            v_proc.executeUpdate();
            v_message = v_proc.getString(13);
            ent_Database_Output.setString1(v_message);
            ent_Database_Output.setBigdecimal1(v_proc.getBigDecimal(6));
            if (v_message == null) {
                cn.commit();
            } else {
                cn.rollback();
            }
            executed_successfully = true;
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Ins_Updt_Activity_Mst", calling_class,
                    calling_method, null, ex);
        } catch (Exception e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Ins_Updt_Activity_Mst", calling_class,
                    calling_method, null, e);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        v_proc.close();
                    }
                } catch (Exception e) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Ins_Updt_Activity_Mst", calling_class,
                            calling_method, null, e);
                }
            }
        }
        ent_Database_Output.setExecuted_successfully(executed_successfully);
        return ent_Database_Output;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Ins_Updt_Budget_Mst">

    public Database_Output Ins_Updt_Budget_Mst(
            String calling_class,
            String calling_method,
            Integer oum_unit_srno,
            Integer fin_year,
            String budget_short_name,
            String budget_desc,
            BigDecimal bm_unique_srno,
            BigDecimal bm_parent_unique_srno,
            BigInteger bm_budget_srno,
            Date start_date,
            Date end_date,
            String bm_ledger_code_type_flag,
            String bm_virtual_budget_flag,
            String transcation_type,
            Integer proccessby) {
        boolean executed_successfully = false;
        Database_Output ent_Database_Output = new Database_Output();
        Connection cn = null;
        CallableStatement v_proc = null;
        String v_message = null;
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            cn.setAutoCommit(false);
            cn.setSavepoint();
            v_proc =
                    cn.prepareCall("{call BMWP_PACK.Ins_Updt_Budget_Mst(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            v_proc.setInt(1, oum_unit_srno);
            v_proc.setInt(2, fin_year);
            v_proc.setString(3, budget_short_name);
            v_proc.setString(4, budget_desc);
            v_proc.setBigDecimal(5, bm_unique_srno);
            v_proc.registerOutParameter(5, java.sql.Types.NUMERIC);
            v_proc.setBigDecimal(6, bm_parent_unique_srno);
            v_proc.setBigDecimal(7, new BigDecimal(bm_budget_srno.toString()));
            v_proc.setDate(8, new java.sql.Date(start_date.getTime()));
            if (end_date != null) {
                v_proc.setDate(9, new java.sql.Date(end_date.getTime()));
            } else {
                v_proc.setDate(9, null);
            }
            v_proc.setString(10, bm_ledger_code_type_flag);
            v_proc.setString(11, bm_virtual_budget_flag);
            v_proc.setString(12, transcation_type);
            v_proc.setInt(13, proccessby);
            v_proc.setString(14, null);
            v_proc.registerOutParameter(14, java.sql.Types.VARCHAR);
            v_proc.executeUpdate();
            v_message = v_proc.getString(14);
            ent_Database_Output.setString1(v_message);
            ent_Database_Output.setBigdecimal1(v_proc.getBigDecimal(5));
            if (v_message == null) {
                cn.commit();
            } else {
                cn.rollback();
            }
            executed_successfully = true;
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Ins_Updt_Budget_Mst", calling_class,
                    calling_method, null, ex);
        } catch (Exception e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Ins_Updt_Budget_Mst", calling_class,
                    calling_method, null, e);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        v_proc.close();
                    }
                } catch (Exception e) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Ins_Updt_Budget_Mst", calling_class,
                            calling_method, null, e);
                }
            }
        }
        ent_Database_Output.setExecuted_successfully(executed_successfully);
        return ent_Database_Output;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Ins_Updt_AP_Activity_Mst">

    public Database_Output Ins_Updt_AP_Activity_Mst(
            String calling_class,
            String calling_method,
            Integer oum_unit_srno,
            Integer fin_year,
            String activity_short_name,
            String activity_desc,
            BigDecimal apl_unique_srno,
            BigDecimal apbm_unique_srno,
            BigDecimal apbm_parent_unique_srno,
            BigInteger apbm_activity_srno,
            Date start_date,
            Date end_date,
            String transcation_type,
            Integer proccessby) {
        boolean executed_successfully = false;
        Database_Output ent_Database_Output = new Database_Output();
        Connection cn = null;
        CallableStatement v_proc = null;
        String v_message = null;
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            cn.setAutoCommit(false);
            cn.setSavepoint();
            v_proc =
                    cn.prepareCall("{call BMWP_PACK.Ins_Updt_AP_Activity_Mst(?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            v_proc.setInt(1, oum_unit_srno);
            v_proc.setInt(2, fin_year);
            v_proc.setString(3, activity_short_name);
            v_proc.setString(4, activity_desc);
            v_proc.setBigDecimal(5, apl_unique_srno);
            v_proc.setBigDecimal(6, apbm_unique_srno);
            v_proc.registerOutParameter(6, java.sql.Types.NUMERIC);
            v_proc.setBigDecimal(7, apbm_parent_unique_srno);
            v_proc.setBigDecimal(8, new BigDecimal(apbm_activity_srno.toString()));
            v_proc.setDate(9, new java.sql.Date(start_date.getTime()));
            if (end_date != null) {
                v_proc.setDate(10, new java.sql.Date(end_date.getTime()));
            } else {
                v_proc.setDate(10, null);
            }
            v_proc.setString(11, transcation_type);
            v_proc.setInt(12, proccessby);
            v_proc.setString(13, null);
            v_proc.registerOutParameter(13, java.sql.Types.VARCHAR);
            v_proc.executeUpdate();
            v_message = v_proc.getString(13);
            ent_Database_Output.setString1(v_message);
            ent_Database_Output.setBigdecimal1(v_proc.getBigDecimal(6));
            if (v_message == null) {
                cn.commit();
            } else {
                cn.rollback();
            }
            executed_successfully = true;
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Ins_Updt_AP_Activity_Mst", calling_class,
                    calling_method, null, ex);
        } catch (Exception e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Ins_Updt_AP_Activity_Mst", calling_class,
                    calling_method, null, e);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        v_proc.close();
                    }
                } catch (Exception e) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Ins_Updt_AP_Activity_Mst", calling_class,
                            calling_method, null, e);
                }
            }
        }
        ent_Database_Output.setExecuted_successfully(executed_successfully);
        return ent_Database_Output;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Ins_Updt_AP_Budget_Mst">

    public Database_Output Ins_Updt_AP_Budget_Mst(
            String calling_class,
            String calling_method,
            Integer oum_unit_srno,
            Integer fin_year,
            String budget_short_name,
            String budget_desc,
            BigDecimal apl_unique_srno,
            BigDecimal apbm_unique_srno,
            BigDecimal apbm_parent_unique_srno,
            BigInteger apbm_budget_srno,
            Date start_date,
            Date end_date,
            String apbm_ledger_code_type_flag,
            String apbm_virtual_budget_flag,
            String transcation_type,
            Integer proccessby) {
        boolean executed_successfully = false;
        Database_Output ent_Database_Output = new Database_Output();
        Connection cn = null;
        CallableStatement v_proc = null;
        String v_message = null;
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            cn.setAutoCommit(false);
            cn.setSavepoint();
            v_proc =
                    cn.prepareCall("{call BMWP_PACK.Ins_Updt_AP_Budget_Mst(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            v_proc.setInt(1, oum_unit_srno);
            v_proc.setInt(2, fin_year);
            v_proc.setString(3, budget_short_name);
            v_proc.setString(4, budget_desc);
            v_proc.setBigDecimal(5, apl_unique_srno);
            v_proc.setBigDecimal(6, apbm_unique_srno);
            v_proc.registerOutParameter(6, java.sql.Types.NUMERIC);
            v_proc.setBigDecimal(7, apbm_parent_unique_srno);
            v_proc.setBigDecimal(8, new BigDecimal(apbm_budget_srno.toString()));
            v_proc.setDate(9, new java.sql.Date(start_date.getTime()));
            if (end_date != null) {
                v_proc.setDate(10, new java.sql.Date(end_date.getTime()));
            } else {
                v_proc.setDate(10, null);
            }
            v_proc.setString(11, apbm_ledger_code_type_flag);
            v_proc.setString(12,apbm_virtual_budget_flag);
            v_proc.setString(13, transcation_type);
            v_proc.setInt(14, proccessby);
            v_proc.setString(15, null);
            v_proc.registerOutParameter(15, java.sql.Types.VARCHAR);
            v_proc.executeUpdate();
            v_message = v_proc.getString(15);
            ent_Database_Output.setString1(v_message);
            ent_Database_Output.setBigdecimal1(v_proc.getBigDecimal(6));
            if (v_message == null) {
                cn.commit();
            } else {
                cn.rollback();
            }
            executed_successfully = true;
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Ins_Updt_AP_Budget_Mst", calling_class,
                    calling_method, null, ex);
        } catch (Exception e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Ins_Updt_AP_Budget_Mst", calling_class,
                    calling_method, null, e);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        v_proc.close();
                    }
                } catch (Exception e) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Ins_Updt_AP_Budget_Mst", calling_class,
                            calling_method, null, e);
                }
            }
        }
        ent_Database_Output.setExecuted_successfully(executed_successfully);
        return ent_Database_Output;
    }
    // </editor-fold>
    //     <editor-fold defaultstate="collapsed" desc="Default Constructor">
//     <editor-fold defaultstate="collapsed" desc="default constructor">

    public Bmwp_Pack() {
    }
    // </editor-fold>  
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="getJdbcFES_PROJECT_Oracle()">

    private DataSource getJdbcFES_PROJECT_Oracle() throws NamingException {
        Context c = new InitialContext();
        return (DataSource) c.lookup("java:comp/env/jdbcFES_PROJECT_Oracle");
    }// </editor-fold>
}
