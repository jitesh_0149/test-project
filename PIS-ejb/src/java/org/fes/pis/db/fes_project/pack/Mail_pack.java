/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.db.fes_project.pack;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.Database_Output;
import org.fes.pis.custom_entities.System_Properties;

/**
 *
 * @author anu
 */
public class Mail_pack implements Serializable{

    String systemName = new System_Properties().getSystemName();

    

    //<editor-fold defaultstate="collapsed" desc="Check_Leave_TTR_ContOverlap">
    public Database_Output send_mail_as_html(
            String calling_class,
            String calling_method,
            String p_from,
            String p_to,
            String p_subject,
            String p_text_msg,
            String p_html_msg,
            String p_smtp_host,
            String p_smtp_port) {
        Database_Output ent_Database_Output = new Database_Output(false);
        Connection cn = null;
        CallableStatement v_proc = null;

        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            cn.setAutoCommit(false);
            cn.setSavepoint();
            v_proc = cn.prepareCall("{call MAIL_PACK.SEND_MAIL_AS_HTML(?,?,?,?,?,?,?)}");

            v_proc.setString(1, p_from);
            v_proc.setString(2, p_to);
            v_proc.setString(3, p_subject);
            v_proc.setString(4, p_text_msg);
            v_proc.setString(5, p_html_msg);
            v_proc.setString(6, p_smtp_host);
            v_proc.setString(7, p_smtp_port);
            v_proc.execute();
            ent_Database_Output.setExecuted_successfully(true);
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "SEND_MAIL_AS_HTML", calling_class,
                    calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "SEND_MAIL_AS_HTML", calling_class,
                    calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        v_proc.close();
                    }
                } catch (Exception ex) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "SEND_MAIL_AS_HTML", calling_class,
                            calling_method, null, ex);
                }
            }
        }
        return ent_Database_Output;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="getJdbcFES_PROJECT_Oracle()">
    private DataSource getJdbcFES_PROJECT_Oracle() throws NamingException {
        Context c = new InitialContext();
        return (DataSource) c.lookup("java:comp/env/jdbcFES_PROJECT_Oracle");
    }// </editor-fold>   
}