/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.db.fes_project.pack;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.Database_Output;
import org.fes.pis.custom_entities.System_Properties;

/**
 *
 * @author anu
 */
public class Ou_Pack implements Serializable{

    String systemName = new System_Properties().getSystemName();

    

    // <editor-fold defaultstate="collapsed" desc="Check_Sysuser">
    public Database_Output Check_Sysuser(
            String calling_class,
            String calling_method,
            Integer p_user_id,
            Integer p_oum_unit_srno,
            String ipAppli_name) {
        Database_Output ent_Database_Output = new Database_Output(false);
        Connection cn = null;
        CallableStatement v_proc = null;
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            v_proc = cn.prepareCall("{?= call BOOL_TO_VARCHAR(OU_PACK.CHECK_SYSUSER (?,?,?))}");
            v_proc.registerOutParameter(1, java.sql.Types.VARCHAR);
            v_proc.setInt(2, p_user_id);
            v_proc.setInt(3, p_oum_unit_srno);
            v_proc.setString(4, ipAppli_name);
            v_proc.execute();
            ent_Database_Output.setString1(v_proc.getString(1));
            ent_Database_Output.setExecuted_successfully(true);
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Check_Sysuser", calling_class,
                    calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Check_Sysuser", calling_class,
                    calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        v_proc.close();
                    }
                } catch (Exception ex) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Check_Sysuser", calling_class,
                            calling_method, null, ex);
                }
            }
        }
        return ent_Database_Output;
    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="check_privileges"> 

    public Database_Output check_privileges(
            String calling_class,
            String calling_method,
            String p_sys_id,
            Integer p_global_user) {
        Database_Output ent_Database_Output = new Database_Output(false);
        Connection cn = null;
        CallableStatement v_proc = null;
        String v_system_user = null;
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            cn.setAutoCommit(false);
            cn.setSavepoint();
            v_proc = cn.prepareCall("{ ?= call ou_pack.Check_and_GetSysUser (?,?)}");
            v_proc.registerOutParameter(1, java.sql.Types.VARCHAR);
            v_proc.setInt(2, p_global_user);
            v_proc.setString(3, p_sys_id);
            v_proc.execute();
            v_system_user = v_proc.getString(1);
            ent_Database_Output.setString1(v_system_user);
            ent_Database_Output.setExecuted_successfully(true);
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "check_privileges", calling_class,
                    calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "check_privileges", calling_class,
                    calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        v_proc.close();
                    }
                } catch (Exception ex) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "check_privileges", calling_class,
                            calling_method, null, ex);
                }
            }
        }
        return ent_Database_Output;
    }
    //</editor-fold>  

    // <editor-fold defaultstate="collapsed" desc="getJdbcFES_PROJECT_Oracle()">
    private DataSource getJdbcFES_PROJECT_Oracle() throws NamingException {
        Context c = new InitialContext();
        return (DataSource) c.lookup("java:comp/env/jdbcFES_PROJECT_Oracle");
    }// </editor-fold>   
    // <editor-fold defaultstate="collapsed" desc="GetSysTime">

    public Database_Output GetSysTime(
            String calling_class,
            String calling_method) {
        Database_Output ent_Database_Output = new Database_Output(false);
        Connection cn = null;
        CallableStatement v_proc = null;
        Date v_sysTime = null;
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            cn.setAutoCommit(false);
            cn.setSavepoint();
            v_proc = cn.prepareCall("{?= call sysdate()}");
            v_proc.registerOutParameter(1, java.sql.Types.TIME);
            v_proc.executeUpdate();
            v_sysTime = v_proc.getTime(1);
            ent_Database_Output.setDate1(v_sysTime);
            if (v_sysTime == null) {
                cn.commit();
            } else {
                cn.rollback();
            }
            ent_Database_Output.setExecuted_successfully(true);
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Get_NativeOu", calling_class,
                    calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Get_NativeOu", calling_class,
                    calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        v_proc.close();
                    }
                } catch (Exception ex) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Ins_Updt_Project_Mst", calling_class,
                            calling_method, null, ex);
                }
            }
        }
        return ent_Database_Output;
    }// </editor-fold>   
}
