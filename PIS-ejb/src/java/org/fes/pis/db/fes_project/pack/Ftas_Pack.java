/* 
 * To change this template, choose Tools | Templates 
 * and open the template in the editor. 
 */
package org.fes.pis.db.fes_project.pack;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.Database_Output;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.jsf.utilities.JSFMessages;

/**
 *
 * @author anu
 */
public class Ftas_Pack implements Serializable {

    String systemName = new System_Properties().getSystemName();

    

    //<editor-fold defaultstate="collapsed" desc="getNativeOu"> 
    public Database_Output getNativeOu(
            String calling_class,
            String calling_method,
            Integer p_oum_unit_srno) {
        Database_Output ent_Database_Output = new Database_Output(false);
        Connection cn = null;
        CallableStatement v_proc = null;
        Integer v_native_ou;
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            cn.setAutoCommit(false);
            cn.setSavepoint();
            v_proc = cn.prepareCall("{?= call Ou_Pack.GET_NATIVE_OU(?)}");
            v_proc.registerOutParameter(1, java.sql.Types.NUMERIC);
            v_proc.setInt(2, p_oum_unit_srno);
            v_proc.executeUpdate();
            v_native_ou = v_proc.getInt(1);
            ent_Database_Output.setInteger1(v_native_ou);
            if (v_native_ou == null) {
                cn.commit();
            } else {
                cn.rollback();
            }
            ent_Database_Output.setExecuted_successfully(true);
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "getNativeOu", calling_class,
                    calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "getNativeOu", calling_class,
                    calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        v_proc.close();
                    }
                } catch (Exception ex) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "getNativeOu", calling_class,
                            calling_method, null, ex);
                }
            }
        }
        return ent_Database_Output;
    }
    //</editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="SetAttendance"> 

    public Database_Output SetAttendance(
            String calling_class,
            String calling_method,
            Integer p_user_id,
            Date p_attdt,
            Date p_atttime,
            BigDecimal p_half1_lm,
            BigDecimal p_half2_lm,
            Date p_h1_atttime,
            Date p_h2_atttime,
            String p_backlog_flag,
            Integer p_createby,
            Integer p_sysuserid) {
        Database_Output database_Output = new Database_Output(false);
        Connection cn = null;
        CallableStatement v_proc = null;
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            cn.setAutoCommit(false);
            cn.setSavepoint();
            v_proc = cn.prepareCall("{call FTAS_PACK.SET_ATTENDANCE(?,?,?,?,?,?,?,?,?,?)}");
            v_proc.setInt(1, p_user_id);
            v_proc.setDate(2, p_attdt == null ? null : new java.sql.Date(p_attdt.getTime()));
            v_proc.setDate(3, p_atttime == null ? null : new java.sql.Date(p_atttime.getTime()));
            v_proc.setBigDecimal(4, p_half1_lm == null ? null : p_half1_lm);
            v_proc.setBigDecimal(5, p_half2_lm == null ? null : p_half2_lm);
            v_proc.setDate(6, p_h1_atttime == null ? null : new java.sql.Date(p_h1_atttime.getTime()));
            v_proc.setDate(7, p_h2_atttime == null ? null : new java.sql.Date(p_h2_atttime.getTime()));
            v_proc.setString(8, p_backlog_flag);
            v_proc.setInt(9, p_createby);
            if (p_sysuserid != null) {
                v_proc.setInt(10, p_sysuserid);
            } else {
                v_proc.setString(10, null);
            }
            v_proc.executeUpdate();
            cn.commit();
            database_Output.setExecuted_successfully(true);
        } catch (Exception e) {
            database_Output.setFacesMessage(JSFMessages.getErrorMessage(e));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "SetAttendance", calling_class,
                    calling_method, null, e);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        v_proc.close();
                    }
                } catch (Exception e) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "SetAttendance", calling_class,
                            calling_method, null, e);
                }
            }
        }
        return database_Output;
    }
    //</editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="bulk_att_insert_SQL"> 

    public Database_Output bulk_att_insert_SQL(
            String calling_class,
            String calling_method,
            String p_emp_list,
            String p_from_date,
            String p_to_date,
            String p_from_half,
            String p_to_half,
            String p_att_time,
            String p_system_userid,
            String p_create_by,
            Integer p_year) {
        Database_Output ent_Database_Output = new Database_Output(false);
        Connection cn = null;
        CallableStatement v_proc = null;
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            cn.setAutoCommit(false);
            cn.setSavepoint();
            v_proc = cn.prepareCall("{call FTAS_PACK.BULK_ATTENDANCE_INSERT(?,?,?,?,?,?,?,?,?)}");
            v_proc.setString(1, p_emp_list);
            v_proc.setString(2, p_from_date);
            v_proc.setString(3, p_to_date);
            v_proc.setString(4, p_from_half);
            v_proc.setString(5, p_to_half);
            v_proc.setString(6, p_att_time);
            v_proc.setString(7, p_create_by);
            v_proc.setString(8, p_system_userid);
            v_proc.setInt(9, p_year);
            v_proc.execute();
            cn.commit();
            ent_Database_Output.setExecuted_successfully(true);
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "bulk_att_insert_SQL", calling_class,
                    calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "bulk_att_insert_SQL", calling_class,
                    calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        v_proc.close();
                    }
                } catch (Exception ex) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "bulk_att_insert_SQL", calling_class,
                            calling_method, null, ex);
                }
            }
        }
        return ent_Database_Output;


    }
    //</editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="SetMissingAttendance"> 

    public Database_Output SetMissingAttendance(
            String calling_class,
            String calling_method,
            String p_attddt_list,
            Integer p_userid,
            Integer p_year,
            Integer p_createby,
            Integer p_oum_unit_srno,
            String p_backlog_flag) {
        Database_Output database_Output = new Database_Output(false);
        Connection cn = null;
        CallableStatement v_proc = null;
//        System.out.println("list " + p_attddt_list); 
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            v_proc = cn.prepareCall("{call FTAS_PACK.SET_MISSING_ATTENDANCE(?,?,?,?,?,?)}");
            v_proc.setString(1, p_attddt_list);
            v_proc.setInt(2, p_userid);
            v_proc.setInt(3, p_year);
            v_proc.setInt(4, p_createby);
            v_proc.setInt(5, p_oum_unit_srno);
            v_proc.setString(6, p_backlog_flag);
//            System.out.println("before execution"); 
            v_proc.execute();
            database_Output.setExecuted_successfully(true);
        } catch (Exception e) {
            database_Output.setFacesMessage(JSFMessages.getErrorMessage(e));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "SetAttendance", calling_class,
                    calling_method, null, e);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        v_proc.close();
                    }
                } catch (Exception e) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "SetAttendance", calling_class,
                            calling_method, null, e);
                }
            }
        }
        return database_Output;
    }
    //</editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="Holiday_Process"> 

    public Database_Output Holiday_Process(
            String calling_class,
            String calling_method,
            String p_oum,
            String p_year,
            String p_global_user,
            String p_holiday_desc,
            String p_holiday_type,
            Date p_holiday_date) {
        Database_Output ent_Database_Output = new Database_Output(false);
        Connection cn = null;
        CallableStatement v_proc = null;
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            cn.setAutoCommit(false);
            cn.setSavepoint();
            v_proc = cn.prepareCall("{call Ftas_Pack.Holiday_Process(?,?,?,?,?,?)}");
            v_proc.setString(1, p_oum);
            v_proc.setString(2, p_year);
            v_proc.setString(3, p_global_user);
            v_proc.setString(4, p_holiday_desc);
            v_proc.setString(5, p_holiday_type);
            if (p_holiday_date != null) {
                v_proc.setDate(6, new java.sql.Date(p_holiday_date.getTime()));
            } else {
                v_proc.setDate(6, null);
            }
            v_proc.executeUpdate();
            cn.commit();
            ent_Database_Output.setExecuted_successfully(true);
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Holiday_Process", calling_class,
                    calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Holiday_Process", calling_class,
                    calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        v_proc.close();
                    }
                } catch (Exception ex) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "SetAttendance", calling_class,
                            calling_method, null, ex);
                }
            }
        }
        return ent_Database_Output;
    }
    //</editor-fold> 
    // <editor-fold defaultstate="collapsed" desc="getJdbcFES_PROJECT_Oracle()"> 

    private DataSource getJdbcFES_PROJECT_Oracle() throws NamingException {
        Context c = new InitialContext();
        return (DataSource) c.lookup("java:comp/env/jdbcFES_PROJECT_Oracle");
    }// </editor-fold>  

    //<editor-fold defaultstate="collapsed" desc="Check_Leave_TTR_ContOverlap"> 
    public Database_Output Check_Leave_TTR_ContOverlap(
            String calling_class,
            String calling_method,
            String p_application_flag,
            String p_appli_type,
            BigDecimal p_emplb_srg_key,
            Integer p_user_id,
            Date p_from_date,
            Date p_to_date,
            String p_fromhalf,
            String p_tohalf) {
        Database_Output ent_Database_Output = new Database_Output(false);
        Connection cn = null;
        CallableStatement v_proc = null;
        String v_return;

        try {
//            System.out.println("----------------------------------------------"); 
//            System.out.println("p_user_id " + p_user_id); 
//            System.out.println("p_from_date " + p_from_date); 
//            System.out.println("p_to_date " + p_to_date); 
//            System.out.println("p_fromhalf " + p_fromhalf); 
//            System.out.println("p_tohalf " + p_tohalf); 
//            System.out.println("----------------------------------------------"); 
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            cn.setAutoCommit(false);
            cn.setSavepoint();
            v_proc = cn.prepareCall("{? = call FTAS_PACK.CHECK_LEAVE_TTR_CONT_OVERLAP(?,?,?,?,?,?,?,?)}");
            v_proc.registerOutParameter(1, java.sql.Types.VARCHAR);
            v_proc.setInt(2, p_user_id);
            v_proc.setString(3, p_application_flag);
            v_proc.setString(4, p_appli_type);
            v_proc.setBigDecimal(5, p_emplb_srg_key);
            v_proc.setDate(6, new java.sql.Date(p_from_date.getTime()));
            v_proc.setDate(7, new java.sql.Date(p_to_date.getTime()));
            v_proc.setString(8, p_fromhalf);
            v_proc.setString(9, p_tohalf);
            v_proc.execute();
            v_return = v_proc.getString(1);
            ent_Database_Output.setString1(v_return);
            cn.commit();
            ent_Database_Output.setExecuted_successfully(true);
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Check_Leave_TTR_ContOverlap", calling_class,
                    calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Check_Leave_TTR_ContOverlap", calling_class,
                    calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        v_proc.close();
                    }
                } catch (Exception ex) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Check_Leave_TTR_ContOverlap", calling_class,
                            calling_method, null, ex);
                }
            }
        }
        return ent_Database_Output;
    }
    //</editor-fold> 

    //<editor-fold defaultstate="collapsed" desc="VALIDATE_ABSENTEES_DTL"> 
    public Database_Output VALIDATE_ABSENTEES_DTL(
            String calling_class,
            String calling_method,
            BigDecimal p_emplb_srg_key,
            BigDecimal p_actual_Emplb_srg_key,
            BigDecimal p_child_Emplb_srg_key,
            BigDecimal p_parent_Emplb_srg_key,
            BigDecimal p_ttr_Emplb_srg_key,
            String p_application_flag,
            String p_appli_type,
            Date p_from_date,
            Date p_to_date,
            String p_from_half,
            String p_to_half,
            BigDecimal p_days,
            BigDecimal p_total_days,
            BigDecimal p_leave_bal_laps,
            String p_last_leavecd,
            Integer p_processby,
            Integer p_system_user_id,
            String p_validate_exhaust,
            String p_updateBalance) {
        Database_Output ent_Database_Output = new Database_Output(false);
        Connection cn = null;
        CallableStatement v_proc = null;
        String v_return;

        try {
//            System.out.println("----------------------------------------------"); 
//             
//            System.out.println("----------------------------------------------"); 
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            cn.setAutoCommit(false);
            cn.setSavepoint();
            v_proc = cn.prepareCall("{call FTAS_PACK.VALIDATE_ABSENTEES_DTL(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            //v_proc.registerOutParameter(1, java.sql.Types.VARCHAR); 
            v_proc.setBigDecimal(1, p_emplb_srg_key);
            v_proc.setBigDecimal(2, p_actual_Emplb_srg_key);
            v_proc.setBigDecimal(3, p_child_Emplb_srg_key);
            v_proc.setBigDecimal(4, p_parent_Emplb_srg_key);
            v_proc.setBigDecimal(5, p_ttr_Emplb_srg_key);
            v_proc.setString(6, p_application_flag);
            v_proc.setString(7, p_appli_type);
            v_proc.setDate(8, new java.sql.Date(p_from_date.getTime()));
            v_proc.setDate(9, new java.sql.Date(p_to_date.getTime()));
            v_proc.setString(10, p_from_half);
            v_proc.setString(11, p_to_half);
            v_proc.setBigDecimal(12, p_days);
            v_proc.setBigDecimal(13, p_total_days);
            v_proc.setBigDecimal(14, BigDecimal.ZERO);
            v_proc.setBigDecimal(15, p_leave_bal_laps);
            v_proc.setString(16, p_last_leavecd);
            v_proc.setInt(17, p_processby);
            v_proc.setString(18, null);
            v_proc.setString(19, p_validate_exhaust);
            v_proc.setString(20, p_updateBalance);
            v_proc.registerOutParameter(21, java.sql.Types.VARCHAR);
            v_proc.registerOutParameter(22, java.sql.Types.NUMERIC);
//            System.out.println("p_updateBalance "+p_updateBalance); 
            v_proc.execute();
            //v_return = v_proc.getString(1); 
            ent_Database_Output.setString1(v_proc.getString(21));
            cn.commit();
            ent_Database_Output.setExecuted_successfully(true);
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "VALIDATE_ABSENTEES_DTL", calling_class,
                    calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "VALIDATE_ABSENTEES_DTL", calling_class,
                    calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        v_proc.close();
                    }
                } catch (Exception ex) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "VALIDATE_ABSENTEES_DTL", calling_class,
                            calling_method, null, ex);
                }
            }
        }
        return ent_Database_Output;
    }
    //</editor-fold> 

    //<editor-fold defaultstate="collapsed" desc="bulk_att_insert_SQL"> 
    public Database_Output BULK_ATTENDANCE_INSERT_Procedure(
            String calling_class,
            String calling_method,
            BigDecimal p_srno,
            String p_emp_list,
            Date p_from_date,
            Date p_to_date,
            BigDecimal p_from_lm_srg_key,
            BigDecimal p_to_lm_srg_key,
            Integer p_createby) {
        Database_Output ent_Database_Output = new Database_Output(false);
        Connection cn = null;
        CallableStatement v_proc = null;
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            cn.setAutoCommit(false);
            cn.setSavepoint();
            v_proc = cn.prepareCall("{call FTAS_PACK.BULK_ATTENDANCE_INSERT(?,?,?,?,?,?,?)}");
            v_proc.setBigDecimal(1, p_srno);
            v_proc.setString(2, p_emp_list);
            v_proc.setDate(3, new java.sql.Date(p_from_date.getTime()));
            v_proc.setDate(4, new java.sql.Date(p_to_date.getTime()));
            v_proc.setBigDecimal(5, p_from_lm_srg_key);
            v_proc.setBigDecimal(6, p_to_lm_srg_key);
            v_proc.setInt(7, p_createby);
            v_proc.execute();
            cn.commit();
            ent_Database_Output.setExecuted_successfully(true);
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "BULK_ATTENDANCE_INSERT_Procedure", calling_class,
                    calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "BULK_ATTENDANCE_INSERT_Procedure", calling_class,
                    calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        v_proc.close();
                    }
                } catch (Exception ex) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "BULK_ATTENDANCE_INSERT_Procedure", calling_class,
                            calling_method, null, ex);
                }
            }
        }
        return ent_Database_Output;
    }
    //</editor-fold> 
}
