/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.db.fes_project.pack;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.Database_Output;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.jsf.utilities.JSFMessages;

/**
 *
 * @author anu
 */
public class Ftas_Attdcalc implements Serializable{

    String systemName = new System_Properties().getSystemName();

    

    //<editor-fold defaultstate="collapsed" desc="Calculate_Attendance_Proc">
    public Database_Output Calculate_Attendance_Proc(String calling_class,
            String calling_method,
            BigDecimal p_attd_srg_key,
            String p_user_id,
            Date p_att_from_date,
            String p_att_from_effect_time,
            Date p_att_to_date,
            String p_att_to_effect_time,
            Date p_salary_from_date,
            String p_salary_from_effect_time,
            Integer p_process_by,
            Integer p_oum_unit_srno,
            String p_calc_force_flag) {
        Database_Output ent_Database_Output = new Database_Output(false);
        Connection cn = null;
        CallableStatement CALL_PROCESS = null;
        //        System.out.println("0--> "+p_attd_srg_key);
        //        System.out.println("1--> " + p_user_id);
        //        System.out.println("2--> " + p_att_from_date);
        //        System.out.println("3--> " + p_att_from_effect_time);
        //        System.out.println("4--> " + p_att_to_date);
        //        System.out.println("5--> " + p_att_to_effect_time);
        //        System.out.println("6--> " + p_salary_from_date);
        //        System.out.println("7--> " + p_salary_from_effect_time);
        //        System.out.println("8--> " + p_process_by);
        //        System.out.println("9--> " + p_oum_unit_srno);
        //        System.out.println("10--> " + p_calc_force_flag);
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            cn.setAutoCommit(false);
            cn.setSavepoint();
            CALL_PROCESS = cn.prepareCall("{call FTAS_ATTD_CALC.CALCULATE_ATTENDANCE(?,?,?,?,?,?,?,?,?,?,?)}");
            CALL_PROCESS.setBigDecimal(1, p_attd_srg_key);
            CALL_PROCESS.setString(2, p_user_id);
            CALL_PROCESS.setInt(3, p_oum_unit_srno);
            CALL_PROCESS.setDate(4, new java.sql.Date(p_att_from_date.getTime()));
            CALL_PROCESS.setString(5, p_att_from_effect_time);
            CALL_PROCESS.setDate(6, new java.sql.Date(p_att_to_date.getTime()));
            CALL_PROCESS.setString(7, p_att_to_effect_time);
            CALL_PROCESS.setDate(8, new java.sql.Date(p_salary_from_date.getTime()));
            CALL_PROCESS.setString(9, p_salary_from_effect_time);
            CALL_PROCESS.setInt(10, p_process_by);
            CALL_PROCESS.setString(11, p_calc_force_flag);

            try {
                CALL_PROCESS.executeUpdate();
                cn.commit();
                ent_Database_Output.setExecuted_successfully(true);
            } catch (SQLException ex) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Calculate_Attendance_Proc", calling_class,
                        calling_method, null, ex);
            }
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Calculate_Attendance_Proc", calling_class,
                    calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Calculate_Attendance_Proc", calling_class,
                    calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        CALL_PROCESS.close();
                    }
                } catch (Exception ex) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "Calculate_Attendance_Proc", calling_class,
                            calling_method, null, ex);
                }
            }
        }
        return ent_Database_Output;


    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="REVERT_ATTENDANCE_CALCULATION">

    public Database_Output REVERT_ATTENDANCE_CALCULATION(String calling_class,
            String calling_method,
            Integer p_user_id,
            Date p_org_attd_from_date,
            Date p_org_attd_to_date,
            Integer p_process_by) {
        Database_Output database_Output = new Database_Output(false);
        Connection cn = null;
        CallableStatement v_proc = null;
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            cn.setAutoCommit(false);
            cn.setSavepoint();
            v_proc = cn.prepareCall("{call FTAS_ATTD_CALC.REVERT_ATTENDANCE_CALCULATION(?,?,?,?,?)}");
            v_proc.setInt(1, p_user_id);
            v_proc.setDate(2, new java.sql.Date(p_org_attd_from_date.getTime()));
            v_proc.setDate(3, new java.sql.Date(p_org_attd_to_date.getTime()));
            v_proc.setInt(4, p_process_by);
            v_proc.registerOutParameter(5, java.sql.Types.VARCHAR);

            try {
                v_proc.executeUpdate();
                database_Output.setString1(v_proc.getString(5));
                if (v_proc.getString(5).equals("Y")) {
                    database_Output.setString1(null);
                    cn.commit();
                } else {
                    cn.rollback();
                }
                database_Output.setExecuted_successfully(true);
            } catch (SQLException sQLException) {
                database_Output.setFacesMessage(JSFMessages.getErrorMessage(sQLException));
                cn.rollback();
            }

        } catch (Exception e) {
            database_Output.setFacesMessage(JSFMessages.getErrorMessage(e));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "REVERT_ATTENDANCE_CALCULATION", calling_class,
                    calling_method, null, e);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        v_proc.close();
                    }
                } catch (Exception e) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "REVERT_ATTENDANCE_CALCULATION", calling_class,
                            calling_method, null, e);
                }
            }
        }

        return database_Output;


    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="getJdbcFES_PROJECT_Oracle()">

    private DataSource getJdbcFES_PROJECT_Oracle() throws NamingException {
        Context c = new InitialContext();
        return (DataSource) c.lookup("java:comp/env/jdbcFES_PROJECT_Oracle");
    }// </editor-fold>
}
