package org.fes.pis.db.fes_project.pack;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.fes.pis.custom_entities.Database_Output;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;

public class Bmwp_Misc implements Serializable{

    String systemName = new System_Properties().getSystemName();

    
    // <editor-fold defaultstate="collapsed" desc="Recomm_Activity_Date">

    public Database_Output Recomm_Activity_Date(
            String p_calling_class,
            String p_calling_method,
            BigDecimal p_am_unique_srno,
            Integer p_fin_year,
            String p_operation,
            BigDecimal p_parent_unique_srno) {
        Connection cn = null;
        Database_Output ent_Database_Output = new Database_Output(false);
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            Statement stmt = cn.createStatement();
            try {
                ResultSet rs;
                String q_find_menu = new String();
                q_find_menu = "SELECT * FROM TABLE(BMWP_MISC.Recomm_Activity_Date(" + p_am_unique_srno + "," + p_fin_year + ",'" + p_operation + "'," + p_parent_unique_srno + "))";
                rs = stmt.executeQuery(q_find_menu);
                try {
                    if (rs.next()) {
                        ent_Database_Output.setDate1(rs.getDate("START_DATE"));
                        ent_Database_Output.setDate2(rs.getDate("END_DATE"));
                    }
                    ent_Database_Output.setExecuted_successfully(true);
                } finally {
                    rs.close();
                }
            } finally {
                stmt.close();
            }
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, Bmwp_Misc.class.getName(), "Recomm_Activity_Date()", p_calling_class, p_calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, Bmwp_Misc.class.getName(), "Recomm_Activity_Date()", p_calling_class, p_calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                    }

                } catch (Exception e) {
                }
            }
        }
        return ent_Database_Output;
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Recomm_Ap_Activity_Date">

    public Database_Output Recomm_Ap_Activity_Date(
            String p_calling_class,
            String p_calling_method,
            BigDecimal p_ap_am_unique_srno,
            Integer p_fin_year,
            String p_operation,
            BigDecimal p_am_parent_unique_srno,
            BigDecimal p_apl_unique_srno) {
        Connection cn = null;
        Database_Output ent_Database_Output = new Database_Output(false);
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            Statement stmt = cn.createStatement();
            try {
                ResultSet rs;
                String q_find_menu = new String();
                q_find_menu = "SELECT * FROM TABLE(BMWP_MISC.Recomm_Ap_Activity_Date(" + p_ap_am_unique_srno + "," + p_fin_year + ",'" + p_operation + "'," + p_am_parent_unique_srno + "," + p_apl_unique_srno + "))";
                rs = stmt.executeQuery(q_find_menu);
                try {
                    if (rs.next()) {
                        ent_Database_Output.setDate1(rs.getDate("START_DATE"));
                        ent_Database_Output.setDate2(rs.getDate("END_DATE"));
                    }
                    ent_Database_Output.setExecuted_successfully(true);
                } finally {
                    rs.close();
                }
            } finally {
                stmt.close();
            }
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, Bmwp_Misc.class.getName(), "Recomm_Ap_Activity_Date()", p_calling_class, p_calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, Bmwp_Misc.class.getName(), "Recomm_Ap_Activity_Date()", p_calling_class, p_calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                    }

                } catch (Exception e) {
                }
            }
        }
        return ent_Database_Output;
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Recomm_Ap_Budget_Date">

    public Database_Output Recomm_Ap_Budget_Date(
            String p_calling_class,
            String p_calling_method,
            BigDecimal p_ap_bm_unique_srno,
            Integer p_fin_year,
            String p_operation,
            BigDecimal p_bm_parent_unique_srno,
            BigDecimal p_apl_unique_srno) {
        Connection cn = null;
        Database_Output ent_Database_Output = new Database_Output(false);
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            Statement stmt = cn.createStatement();
            try {
                ResultSet rs;
                String q_find_menu = new String();
                q_find_menu = "SELECT * FROM TABLE(BMWP_MISC.Recomm_Ap_Budget_Date(" + p_ap_bm_unique_srno + "," + p_fin_year + ",'" + p_operation + "'," + p_bm_parent_unique_srno + "," + p_apl_unique_srno + "))";
                rs = stmt.executeQuery(q_find_menu);
                try {
                    if (rs.next()) {
                        ent_Database_Output.setDate1(rs.getDate("START_DATE"));
                        ent_Database_Output.setDate2(rs.getDate("END_DATE"));
                    }
                    ent_Database_Output.setExecuted_successfully(true);
                } finally {
                    rs.close();
                }
            } finally {
                stmt.close();
            }
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, Bmwp_Misc.class.getName(), "Recomm_Ap_Budget_Date()", p_calling_class, p_calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, Bmwp_Misc.class.getName(), "Recomm_Ap_Budget_Date()", p_calling_class, p_calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                    }

                } catch (Exception e) {
                }
            }
        }
        return ent_Database_Output;
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Recomm_Agency_Project_Lnk_Date">

    public Database_Output Recomm_Agency_Project_Lnk_Date(String p_calling_class, String p_calling_method,
            BigDecimal p_apl_unique_srno,
            Integer p_fin_year,
            String p_operation,
            BigDecimal p_fam_unique_srno,
            BigDecimal p_pm_unique_srno) {
        Connection cn = null;
        Database_Output ent_Database_Output = new Database_Output(false);
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            Statement stmt = cn.createStatement();
            try {
                ResultSet rs;
                String q_find_menu = new String();
                q_find_menu = "SELECT * FROM TABLE(BMWP_MISC.Recomm_Agency_Project_Lnk_Date(" + p_apl_unique_srno + "," + p_fin_year + ",'" + p_operation + "'," + p_fam_unique_srno + "," + p_pm_unique_srno + "))";
                rs = stmt.executeQuery(q_find_menu);
                try {
                    if (rs.next()) {
                        ent_Database_Output.setDate1(rs.getDate("START_DATE"));
                        ent_Database_Output.setDate2(rs.getDate("END_DATE"));
                    }
                    ent_Database_Output.setExecuted_successfully(true);
                } finally {
                    rs.close();
                }
            } finally {
                stmt.close();
            }
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, Bmwp_Misc.class.getName(), "Recomm_Agency_Project_Lnk_Date", p_calling_class, p_calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, Bmwp_Misc.class.getName(), "Recomm_Agency_Project_Lnk_Date", p_calling_class, p_calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                    }

                } catch (Exception e) {
                }
            }
        }
        return ent_Database_Output;
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Recomm_Agency_Type_Date">

    public Database_Output Recomm_Agency_Type_Date(String p_calling_class,
            String p_calling_method,
            BigDecimal p_atm_unique_srno,
            Integer p_fin_year,
            String p_operation) {
        Connection cn = null;
        Database_Output ent_Database_Output = new Database_Output(false);
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            Statement stmt = cn.createStatement();
            try {
                ResultSet rs;
                String q_find_menu = new String();
                q_find_menu = "SELECT * FROM TABLE(BMWP_MISC.RECOMM_AGENCY_TYPE_DATE(" + p_atm_unique_srno + "," + p_fin_year + ",'" + p_operation + "'))";
                rs = stmt.executeQuery(q_find_menu);
                try {
                    if (rs.next()) {
                        ent_Database_Output.setDate1(rs.getDate("START_DATE"));
                        ent_Database_Output.setDate2(rs.getDate("END_DATE"));
                    }
                    ent_Database_Output.setExecuted_successfully(true);
                } finally {
                    rs.close();
                }
            } finally {
                stmt.close();
            }
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, Bmwp_Misc.class.getName(), "Recomm_Agency_Type_Date", p_calling_class, p_calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, Bmwp_Misc.class.getName(), "Recomm_Agency_Type_Date", p_calling_class, p_calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                    }

                } catch (Exception e) {
                }
            }
        }
        return ent_Database_Output;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Recomm_Budget_Date">

    public Database_Output Recomm_Budget_Date(
            String p_calling_class,
            String p_calling_method,
            BigDecimal p_bm_unique_srno,
            Integer p_fin_year,
            String p_operation,
            BigDecimal p_bm_parent_unique_srno) {
        Connection cn = null;
        Database_Output ent_Database_Output = new Database_Output(false);
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            Statement stmt = cn.createStatement();
            try {
                ResultSet rs;
                String q_find_menu = new String();
                q_find_menu = "SELECT * FROM TABLE(BMWP_MISC.Recomm_Budget_Date(" + p_bm_unique_srno + "," + p_fin_year + ",'" + p_operation + "'," + p_bm_parent_unique_srno + "))";
                rs = stmt.executeQuery(q_find_menu);
                try {
                    if (rs.next()) {
                        ent_Database_Output.setDate1(rs.getDate("START_DATE"));
                        ent_Database_Output.setDate2(rs.getDate("END_DATE"));
                    }
                    ent_Database_Output.setExecuted_successfully(true);
                } finally {
                    rs.close();
                }
            } finally {
                stmt.close();
            }
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, Bmwp_Misc.class.getName(), "Recomm_Budget_Date", p_calling_class, p_calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, Bmwp_Misc.class.getName(), "Recomm_Budget_Date", p_calling_class, p_calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                    }

                } catch (Exception e) {
                }
            }
        }
        return ent_Database_Output;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Recomm_Fund_Agency_Date">

    public Database_Output Recomm_Fund_Agency_Date(
            String p_calling_class,
            String p_calling_method,
            BigDecimal p_fam_unique_srno,
            Integer p_fin_year,
            String p_operation,
            BigDecimal p_atm_unique_srno) {
        Connection cn = null;
        Database_Output ent_Database_Output = new Database_Output(false);
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            Statement stmt = cn.createStatement();
            try {
                ResultSet rs;
                String q_find_menu = new String();
                q_find_menu = "SELECT * FROM TABLE(BMWP_MISC.Recomm_Fund_Agency_Date(" + p_fam_unique_srno + "," + p_fin_year + ",'" + p_operation + "'," + p_atm_unique_srno + "))";
                rs = stmt.executeQuery(q_find_menu);
                try {
                    if (rs.next()) {
                        ent_Database_Output.setDate1(rs.getDate("START_DATE"));
                        ent_Database_Output.setDate2(rs.getDate("END_DATE"));
                    }
                    ent_Database_Output.setExecuted_successfully(true);
                } finally {
                    rs.close();
                }
            } finally {
                stmt.close();
            }
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, Bmwp_Misc.class.getName(), "Recomm_Fund_Agency_Date", p_calling_class, p_calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, Bmwp_Misc.class.getName(), "Recomm_Fund_Agency_Date", p_calling_class, p_calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                    }

                } catch (Exception e) {
                }
            }
        }
        return ent_Database_Output;
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Recomm_Project_Date">

    public Database_Output Recomm_Project_Date(
            String p_calling_class,
            String p_calling_method,
            BigDecimal p_pm_unique_srno,
            Integer p_fin_year,
            String p_operation) {
        Connection cn = null;
        Database_Output ent_Database_Output = new Database_Output(false);
        try {
            cn = getJdbcFES_PROJECT_Oracle().getConnection();
            Statement stmt = cn.createStatement();
            try {
                ResultSet rs;
                String q_find_menu = new String();
                q_find_menu = "SELECT * FROM TABLE(BMWP_MISC.Recomm_Project_Date(" + p_pm_unique_srno + "," + p_fin_year + ",'" + p_operation + "'))";
                rs = stmt.executeQuery(q_find_menu);
                try {
                    if (rs.next()) {
                        ent_Database_Output.setDate1(rs.getDate("START_DATE"));
                        ent_Database_Output.setDate2(rs.getDate("END_DATE"));
                    }
                    ent_Database_Output.setExecuted_successfully(true);
                } finally {
                    rs.close();
                }
            } finally {
                stmt.close();
            }
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, Bmwp_Misc.class.getName(), "Recomm_Project_Dat", p_calling_class, p_calling_method, null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, Bmwp_Misc.class.getName(), "Recomm_Project_Dat", p_calling_class, p_calling_method, null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                    }

                } catch (Exception e) {
                }
            }
        }
        return ent_Database_Output;
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Default Constructor">

    public Bmwp_Misc() {
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="getJdbcFES_PROJECT_Oracle()">
    private DataSource getJdbcFES_PROJECT_Oracle() throws NamingException {
        Context c = new InitialContext();
        return (DataSource) c.lookup("java:comp/env/jdbcFES_PROJECT_Oracle");
    }// </editor-fold>
}
