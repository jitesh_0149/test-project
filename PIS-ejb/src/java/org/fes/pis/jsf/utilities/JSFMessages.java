package org.fes.pis.jsf.utilities;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.PersistenceException;
import org.fes.pis.custom_sessions.ejb_utilitiesLocal;

public class JSFMessages {

    private static String schema;
    //<editor-fold defaultstate="collapsed" desc="getErrorMessage">

    public static FacesMessage getErrorMessage(Exception e) {
        String message = null;
        if (e instanceof EJBException) {
            Exception ejb = ((EJBException) e).getCausedByException();
            if (ejb != null) {
                message = ejb.getCause() == null ? ejb.getMessage() : (ejb.getCause().getCause() == null ? ejb.getCause().toString() : ejb.getCause().getCause().toString());
                message = "Reason: " + message == null ? null : removeSchemaName(message);
            }

        } else if (e instanceof PersistenceException) {
            Throwable t = getLastThrowable(e);
            if (t != null && (t instanceof SQLException)) {
                SQLException sqlException = (SQLException) t;
                message = "Reason: " + (removeSchemaName(sqlException.getMessage()));
            } else {
                message = "Reason: " + e.getClass();
            }
        } else if (e instanceof SQLException) {
            SQLException sqlException = (SQLException) e;
            message = "Reason: " + (removeSchemaName(sqlException.getMessage()));
        }

        if (message == null) {
            StackTraceElement se = e.getStackTrace()[0];
            message = "Error at " + se.getFileName() + ":" + se.getLineNumber() + " Reason: " + (e.getMessage() == null ? e.getClass().getName() : e.getMessage());
        }

        return new FacesMessage(FacesMessage.SEVERITY_ERROR, "Some unexpected error occrred", message);

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="getLastThrowable">

    private static Throwable getLastThrowable(Exception e) {
        Throwable t = null;
        for (t = e.getCause(); t != null && t.getCause() != null; t = t.getCause()) {
        }
        return t;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="removeSchemaName">

    private static String removeSchemaName(String message) {
        try {
            return message.replaceAll("\"" + getSchemaName() + "\".", "").replaceAll(getSchemaName() + ".", "");
        } catch (Exception e) {
            return message;
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="getSchemaName()">

    private static String getSchemaName() {
        if (schema == null) {
            try {
                schema = lookupejb_utilitiesLocal().getSchemaName();
            } catch (NamingException ex) {
                Logger.getLogger(JSFMessages.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return schema;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lookupejb_utilitiesLocal">

    private static ejb_utilitiesLocal lookupejb_utilitiesLocal() throws NamingException {
        Context c = new InitialContext();
        return (ejb_utilitiesLocal) c.lookup("java:global/PIS-app/PIS-ejb/ejb_utilities!org.fes.pis.custom_sessions.ejb_utilitiesLocal");
    }
    //</editor-fold>
}
