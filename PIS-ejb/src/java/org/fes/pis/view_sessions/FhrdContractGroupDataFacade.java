/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.view_sessions;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.fes.pis.view_entities.FhrdContractGroupData;

/**
 *
 * @author anu
 */
@Stateless
public class FhrdContractGroupDataFacade extends AbstractFacade<FhrdContractGroupData> {

    @PersistenceContext(unitName = "PIS-app-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FhrdContractGroupDataFacade() {
        super(FhrdContractGroupData.class);
    }

    public List<FhrdContractGroupData> findAllContractwise(BigDecimal p_cm_srg_key) {
//        System.out.println("Select * from Fhrd_Contract_Group_Data "
//                + " where cm_srg_key = " + p_cm_srg_key);
        return em.createNativeQuery("Select * from Fhrd_Contract_Group_Data "
                + " where cm_srg_key = " + p_cm_srg_key, FhrdContractGroupData.class).getResultList();
    }

    public List<FhrdContractGroupData> findAllContract(BigDecimal p_cm_srg_key) {
        Query query1 = em.createNamedQuery("FhrdContractGroupData.findByCmSrgKey");
        query1.setParameter("cmSrgKey", p_cm_srg_key);
        return query1.getResultList();
    }
}
