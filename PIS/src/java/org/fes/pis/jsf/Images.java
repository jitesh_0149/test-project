/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf;

import java.io.*;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.servlet.ServletContext;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author jitesh
 */
@ManagedBean
@ApplicationScoped
public class Images {

    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    ServletContext servletContext;

    public StreamedContent getImage() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the view. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        } else {
            // So, browser is requesting the image. Return a real StreamedContent with the image bytes.
            String id = context.getExternalContext().getRequestParameterMap().get("image_user_id");
            if (context.getExternalContext().getRequestParameterMap().get("showUploaded").equals("N")) {
                if (!id.equals("") ? (fhrdEmpmstFacade.find(new Integer(id)).getEmpImg() != null) : false) {
                    return new DefaultStreamedContent(new ByteArrayInputStream(fhrdEmpmstFacade.find(new Integer(id)).getEmpImg()));
                } else {
                    InputStream inputStream = new FileInputStream(servletContext.getRealPath("") + File.separator + "img_not_available.jpg");
                    return new DefaultStreamedContent(inputStream);
                }
            } else {
                InputStream inputStream = new FileInputStream(context.getExternalContext().getRequestParameterMap().get("upload_location"));
                return new DefaultStreamedContent(inputStream);
            }
        }


    }

    public Images() {
        servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
    }
}