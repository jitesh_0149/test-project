package org.fes.pis.jsf;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.ejb_utilitiesLocal;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.SystOrgUnitMst;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;
import org.fes.pis.sessions.SystOrgUnitMstFacadeLocal;

/**
 *
 * @author Krishna
 */
@ManagedBean(name = "GlobalSettings")
@SessionScoped
public class GlobalSettings implements Serializable {

    @EJB
    private SystOrgUnitMstFacadeLocal systOrgUnitMstFacade;
    @EJB
    private ejb_utilitiesLocal ejb_utilities;
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    Ou_Pack ou_Pack = new Ou_Pack();
    // <editor-fold defaultstate="collapsed" desc="SystemName">
    String SystemName = new System_Properties().getSystemName();
    // </editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="organisationName">
    SystOrgUnitMst organisation = null;
    String organisationName = null;
    String organisationShortName = null;

    public SystOrgUnitMst getOrganisation() {
        if (organisation == null) {
            organisation = systOrgUnitMstFacade.getOrganisation(getWorking_ou());
        }
        return organisation;
    }

    public void setOrganisation(SystOrgUnitMst organisation) {
        this.organisation = organisation;
    }

    public String getOrganisationShortName() {
        if (organisationShortName == null) {
            organisationShortName = getOrganisation().getOumShortName();
        }
        return organisationShortName;
    }

    public void setOrganisationShortName(String organisationShortName) {
        this.organisationShortName = organisationShortName;
    }

    public String getOrganisationName() {
        if (organisationName == null) {
            organisationName = getOrganisation().getOumName();
        }
        return organisationName;
    }

    public void setOrganisationName(String organisationName) {
        this.organisationName = organisationName;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ent_login_user">
    FhrdEmpmst ent_login_user;

    public FhrdEmpmst getEnt_login_user() {
        if (ent_login_user == null) {
            if (FacesContext.getCurrentInstance().getExternalContext().getRemoteUser() != null) {
                ent_login_user = fhrdEmpmstFacade.find(new Integer(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser().trim()));
            }
        }
        return ent_login_user;
    }

    public void setEnt_login_user(FhrdEmpmst ent_login_user) {
        this.ent_login_user = ent_login_user;
    }
    //</editor-fold>    

    // <editor-fold defaultstate="collapsed" desc="getUserDetail_ById">
    public FhrdEmpmst getUserDetail_ById(Integer p_user_id) {
        return fhrdEmpmstFacade.find(p_user_id);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Gives Id of the user who is logged in">

    private Integer getUserId() {
        return getEnt_login_user().getUserId();
    }
    // </editor-fold>            
    // <editor-fold defaultstate="collapsed" desc="Global working+name+posting+orgou">
    Integer user_id = null;
    Integer posting_ou = null;
    Integer working_ou = null;
    String user_name = null;
    SystOrgUnitMst ent_working_ou = null;
    SystOrgUnitMst ent_posting_ou = null;

    public SystOrgUnitMst getEnt_working_ou() {
        if (ent_working_ou == null) {
            ent_working_ou = getEnt_posting_ou();
        }
        return ent_working_ou;
    }

    public void setEnt_working_ou(SystOrgUnitMst ent_working_ou) {
        this.ent_working_ou = ent_working_ou;
    }

    public String getUser_name() {
        if (user_name == null) {
            user_name = getEnt_login_user().getUserName();
        }
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public Integer getWorking_ou() {
        working_ou = getEnt_working_ou().getOumUnitSrno();
        return working_ou;
    }

    public void setWorking_ou(Integer working_ou) {
        this.working_ou = working_ou;
    }

    public Integer getPosting_ou() {
        if (posting_ou == null) {
            posting_ou = getEnt_posting_ou().getOumUnitSrno();
        }
        return posting_ou;

    }

    public void setPosting_ou(Integer posting_ou) {
        this.posting_ou = posting_ou;
    }

    public Integer getUser_id() {
        user_id = getUserId();
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public SystOrgUnitMst getEnt_posting_ou() {
        if (ent_posting_ou == null) {
            ent_posting_ou = getEnt_login_user().getPostingOumUnitSrno();
        }
        return ent_posting_ou;
    }

    public void setEnt_posting_ou(SystOrgUnitMst ent_posting_ou) {
        this.ent_posting_ou = ent_posting_ou;
    }

    // </editor-fold>      
    // <editor-fold defaultstate="collapsed" desc="getUniqueSrno">
//    public BigDecimal getUniqueSrno(String p_table_name) {
//        return ejb_utilities.getUniqueSrNo(p_table_name);
//    }
    public BigDecimal getUniqueSrno(Integer p_transactionYear, String p_table_name) {
        return ejb_utilities.getUniqueSrNo(p_transactionYear, p_table_name);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Default Constructor & Other Bean">
// <editor-fold defaultstate="collapsed" desc="Default Constructor">
    public GlobalSettings() {
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="getGlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>  
    // <editor-fold defaultstate="collapsed" desc="getGlobalResources">

    protected GlobalResources getGlobalResources() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalResources) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalResources");
    }// </editor-fold>  
    // </editor-fold>   
}
