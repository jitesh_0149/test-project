/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.reports;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.SystOrgUnitMst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;
import org.fes.pis.sessions.SystOrgUnitMstFacadeLocal;
import org.joda.time.DateTime;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Utility_Reports")
@ViewScoped
public class PIS_Utility_Reports implements Serializable {

    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    @EJB
    private SystOrgUnitMstFacadeLocal systOrgUnitMstFacade;
    System_Properties system_Properties = new System_Properties();
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="Reports Parameters">
    //<editor-fold defaultstate="collapsed" desc="Common Parameters">
    String hdn_report_name;
    String hdn_report_type;
    String hdn_report_sub_type;
    String hdn_report_exp_name;
    String hdn_report_loc_type;

    public String getHdn_report_exp_name() {
        return hdn_report_exp_name;
    }

    public void setHdn_report_exp_name(String hdn_report_exp_name) {
        this.hdn_report_exp_name = hdn_report_exp_name;
    }

    public String getHdn_report_name() {
        return hdn_report_name;
    }

    public void setHdn_report_name(String hdn_report_name) {
        this.hdn_report_name = hdn_report_name;
    }

    public String getHdn_report_type() {
        return hdn_report_type;
    }

    public void setHdn_report_type(String hdn_report_type) {
        this.hdn_report_type = hdn_report_type;
    }

    public String getHdn_report_sub_type() {
        return hdn_report_sub_type;
    }

    public void setHdn_report_sub_type(String hdn_report_sub_type) {
        this.hdn_report_sub_type = hdn_report_sub_type;
    }

    public String getHdn_report_loc_type() {
        return hdn_report_loc_type;
    }

    public void setHdn_report_loc_type(String hdn_report_loc_type) {
        this.hdn_report_loc_type = hdn_report_loc_type;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="boolean parameters">
    boolean organisationLevel;
    boolean showOrgUnit;
    boolean showFromDate;
    boolean showToDate;
    boolean showNativeLevel;
    boolean showCalMonths;
    boolean showCalYear;
    boolean forEmpSalary;
    boolean calBound;
    boolean showEmpStatus;
    boolean showUsers;
    boolean showRelievedUsers;
    boolean showUserType;

    public boolean isCalBound() {
        return calBound;
    }

    public boolean isShowEmpStatus() {
        return showEmpStatus;
    }

    public void setShowEmpStatus(boolean showEmpStatus) {
        this.showEmpStatus = showEmpStatus;
    }

    public void setCalBound(boolean calBound) {
        this.calBound = calBound;
    }

    public boolean isShowToDate() {
        return showToDate;
    }

    public void setShowToDate(boolean showToDate) {
        this.showToDate = showToDate;
    }

    public boolean isForEmpSalary() {
        return forEmpSalary;
    }

    public void setForEmpSalary(boolean forEmpSalary) {
        this.forEmpSalary = forEmpSalary;
    }

    public boolean isShowCalYear() {
        return showCalYear;
    }

    public void setShowCalYear(boolean showCalYear) {
        this.showCalYear = showCalYear;
    }

    public boolean isShowCalMonths() {
        return showCalMonths;
    }

    public void setShowCalMonths(boolean showCalMonths) {
        this.showCalMonths = showCalMonths;
    }

    public boolean isShowNativeLevel() {
        return showNativeLevel;
    }

    public void setShowNativeLevel(boolean showNativeLevel) {
        this.showNativeLevel = showNativeLevel;
    }

    public boolean isShowOrgUnit() {
        return showOrgUnit;
    }

    public void setShowOrgUnit(boolean showOrgUnit) {
        this.showOrgUnit = showOrgUnit;
    }

    public boolean isShowFromDate() {
        return showFromDate;
    }

    public void setShowFromDate(boolean showFromDate) {
        this.showFromDate = showFromDate;
    }

    public boolean isOrganisationLevel() {
        return organisationLevel;
    }

    public void setOrganisationLevel(boolean organisationLevel) {
        this.organisationLevel = organisationLevel;
    }

    public boolean isShowUsers() {
        return showUsers;
    }

    public void setShowUsers(boolean showUsers) {
        this.showUsers = showUsers;
    }

    public boolean isShowRelievedUsers() {
        return showRelievedUsers;
    }

    public void setShowRelievedUsers(boolean showRelievedUsers) {
        this.showRelievedUsers = showRelievedUsers;
    }

    public boolean isShowUserType() {
        return showUserType;
    }

    public void setShowUserType(boolean showUserType) {
        this.showUserType = showUserType;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="gen_org_unit_srno">
    SystOrgUnitMst ent_org_unit;

    public SystOrgUnitMst getEnt_org_unit() {
        return ent_org_unit;
    }

    public void setEnt_org_unit(SystOrgUnitMst ent_org_unit) {
        this.ent_org_unit = ent_org_unit;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_org_unit">
    String ddl_org_unit;
    List<SelectItem> ddl_org_unit_options;

    public String getDdl_org_unit() {
        return ddl_org_unit;
    }

    public void setDdl_org_unit(String ddl_org_unit) {
        this.ddl_org_unit = ddl_org_unit;
    }

    public List<SelectItem> getDdl_org_unit_options() {
        return ddl_org_unit_options;
    }

    public void setDdl_org_unit_options(List<SelectItem> ddl_org_unit_options) {
        this.ddl_org_unit_options = ddl_org_unit_options;
    }

    public void setDdl_org_unit() {
        String v_old_selected = ddl_org_unit;
        if (showOrgUnit) {
            List<SystOrgUnitMst> lst_SystOrgUnitMsts;
            if (getPIS_GlobalSettings().getSysetemUser_Fhrd()) {

                lst_SystOrgUnitMsts = systOrgUnitMstFacade.findAll(globalData.getWorking_ou(), false, false, false, false, true, null, null);
            } else {
                lst_SystOrgUnitMsts = systOrgUnitMstFacade.findAll(globalData.getWorking_ou(), false, false, false, false, false, globalData.getUser_id(), "O");
            }
            ddl_org_unit_options = new ArrayList<SelectItem>();
            int n = lst_SystOrgUnitMsts.size();
            if (n > 0) {
                for (int i = 0; i < n; i++) {
                    ddl_org_unit_options.add(new SelectItem(lst_SystOrgUnitMsts.get(i).getOumUnitSrno(), lst_SystOrgUnitMsts.get(i).getOumName()));
                }
                ddl_org_unit = globalData.getWorking_ou().toString();
            } else {
                ddl_org_unit_options.add(new SelectItem(null, "--- Not Available ---"));
                ddl_org_unit = null;
            }
            if (v_old_selected != null) {
                ddl_org_unit = v_old_selected;
            }
            ent_org_unit = ddl_org_unit != null ? systOrgUnitMstFacade.find(Integer.valueOf(ddl_org_unit)) : new SystOrgUnitMst();

        } else {
            if (!organisationLevel) {
                ent_org_unit = globalData.getEnt_working_ou();
            } else {
                ent_org_unit = globalData.getOrganisation();
            }
        }
        setDdl_user();
    }

    public void ddl_org_unit_changed(ValueChangeEvent event) {
        ddl_org_unit = event.getNewValue().toString();
        ent_org_unit = systOrgUnitMstFacade.find(new Integer(ddl_org_unit));
        setDdl_user();
        //        setDdl_agency_type();
        //        setDdl_apl();
        //        setLst_ApActivityMsts();
        //        setLst_ActivityMsts();
        //        setLst_ApBudgetMsts();
        //        setLst_BudgetMsts();
        //        setDdl_party_type();
        //        setLst_PartyTypeDtls();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_native_level">
    String ddl_native_level;

    public String getDdl_native_level() {
        return ddl_native_level;
    }

    public void setDdl_native_level(String ddl_native_level) {
        this.ddl_native_level = ddl_native_level;
    }

    public void ddl_native_level_changed(ValueChangeEvent event) {
        ddl_native_level = event.getNewValue() == null ? null : event.getNewValue().toString();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="min max lock date">
    Date min_date;
    Date max_date;

    private void setMinMaxDate() {
        if (calBound) {
            min_date = system_Properties.getAttdSystemStartDate();
            max_date = new Date();
        } else {

            cal_from_date_max = null;
            cal_to_date_max = null;
            cal_from_date_min = null;
            cal_to_date_min = null;
        }
        setDdl_year();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_year">
    String ddl_year;
    List<SelectItem> ddl_year_options;

    public String getDdl_year() {
        return ddl_year;
    }

    public void setDdl_year(String ddl_year) {
        this.ddl_year = ddl_year;
    }

    public List<SelectItem> getDdl_year_options() {
        return ddl_year_options;
    }

    public void setDdl_year_options(List<SelectItem> ddl_year_options) {
        this.ddl_year_options = ddl_year_options;
    }

    private void setDdl_year() {
        Integer v_from_year = min_date.getYear() + 1900;
        Integer v_to_year = max_date.getYear() + 1900;
        ddl_year_options = new ArrayList<SelectItem>();
        while (v_from_year <= v_to_year) {
            ddl_year_options.add(new SelectItem(v_from_year));
            v_from_year++;
        }
        if (ddl_year_options.isEmpty()) {
            ddl_year = null;
        } else {
            ddl_year = String.valueOf((new Date()).getYear() + 1900);
        }
        setDdl_month();
    }

    public void ddl_year_changed(ValueChangeEvent event) {
        ddl_year = event.getNewValue() == null ? null : event.getNewValue().toString();
        setDdl_month();
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ddl_month">
    String ddl_month;
    List<SelectItem> ddl_month_options;

    public List<SelectItem> getDdl_month_options() {
        return ddl_month_options;
    }

    public void setDdl_month_options(List<SelectItem> ddl_month_options) {
        this.ddl_month_options = ddl_month_options;
    }

    public String getDdl_month() {
        return ddl_month;
    }

    public void setDdl_month(String ddl_month) {
        this.ddl_month = ddl_month;
    }

    public void ddl_month_changed(ValueChangeEvent event) {
        ddl_month = event.getNewValue() == null ? null : event.getNewValue().toString();
    }

    private void setDdl_month() {
        ddl_month_options = new ArrayList<SelectItem>();
        if (ddl_year == null) {
            ddl_month_options.add(new SelectItem(null, "--- Not Available ---"));
            ddl_month = null;
        } else {
            Date v_year_start_date = new Date(Integer.valueOf(ddl_year) - 1900, 0, 1);
            Date v_year_end_date = new Date(Integer.valueOf(ddl_year) - 1900, 11, 31);
            Date v_min_date = min_date;
            Date v_max_date = max_date;
            while (v_min_date.before(v_max_date) || v_min_date.equals(v_max_date)) {
                if (!(v_min_date.before(v_year_start_date))
                        || !(v_min_date.after(v_year_end_date))) {
                    ddl_month_options.add(new SelectItem(DateTimeUtility.ChangeDateFormat(v_min_date, "MM").toString(), DateTimeUtility.ChangeDateFormat(v_min_date, "MMM")));
                }
                v_min_date = new DateTime(v_min_date).plusMonths(1).toDate();
            }
            ddl_month = ddl_month_options.get(0).getValue() == null ? null : ddl_month_options.get(0).getValue().toString();
        }
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_from_date">
    Date cal_from_date;
    String cal_from_date_min;
    String cal_from_date_max;
    String cal_from_date_label;

    public Date getCal_from_date() {
        return cal_from_date;
    }

    public void setCal_from_date(Date cal_from_date) {
        this.cal_from_date = cal_from_date;
    }

    public String getCal_from_date_max() {
        return cal_from_date_max;
    }

    public void setCal_from_date_max(String cal_from_date_max) {
        this.cal_from_date_max = cal_from_date_max;
    }

    public String getCal_from_date_min() {
        return cal_from_date_min;
    }

    public void setCal_from_date_min(String cal_from_date_min) {
        this.cal_from_date_min = cal_from_date_min;
    }

    public String getCal_from_date_label() {
        return cal_from_date_label;
    }

    public void setCal_from_date_label(String cal_from_date_label) {
        this.cal_from_date_label = cal_from_date_label;
    }

    private void setCal_from_date() {
        if (calBound) {
            cal_from_date_min = DateTimeUtility.ChangeDateFormat(min_date, null);
            cal_from_date_max = DateTimeUtility.ChangeDateFormat(DateTimeUtility.endDateOfMonthFromDate(new Date()), null);
            Date v_today = DateTimeUtility.stringToDate(DateTimeUtility.ChangeDateFormat(new Date(), null), null);
            cal_from_date = DateTimeUtility.converToDateTime(v_today).minusMonths(1).toDate();
            cal_from_date.setDate(6);
            cal_to_date = v_today;
            cal_to_date.setDate(5);
        } else {
            cal_from_date_max = null;
            cal_to_date_max = null;
            cal_from_date_min = null;
            cal_to_date_min = null;
        }
        setCal_to_date();
    }

    public void cal_from_date_changed(SelectEvent event) {
        cal_from_date = (Date) event.getObject();
        setCal_to_date();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_to_date">
    Date cal_to_date;
    String cal_to_date_min;
    String cal_to_date_max;

    public Date getCal_to_date() {
        return cal_to_date;
    }

    public void setCal_to_date(Date cal_to_date) {
        this.cal_to_date = cal_to_date;
    }

    public String getCal_to_date_max() {
        return cal_to_date_max;
    }

    public void setCal_to_date_max(String cal_to_date_max) {
        this.cal_to_date_max = cal_to_date_max;
    }

    public String getCal_to_date_min() {
        return cal_to_date_min;
    }

    public void setCal_to_date_min(String cal_to_date_min) {
        this.cal_to_date_min = cal_to_date_min;
    }

    private void setCal_to_date() {
        if (calBound) {
            cal_to_date_min = DateTimeUtility.ChangeDateFormat(cal_from_date, null);
            if (cal_from_date.getDate() == 6) {
                cal_to_date = DateTimeUtility.findNextDateFromDays(cal_from_date, 30);
                cal_to_date.setDate(5);
            } else {
                if (cal_from_date.after(cal_to_date)) {
                    cal_to_date = cal_from_date;
                }
            }

            //Date v_45_days = DateTimeUtility.converToDateTime(cal_from_date).plusDays(45).toDate();
            //if (DateTimeUtility.stringToDate(cal_from_date_max, null).before(v_45_days)) {
            //cal_to_date_max = cal_from_date_max;
            //} else {
            //cal_to_date_max = DateTimeUtility.ChangeDateFormat(v_45_days, null);
            //}

        } else {

            cal_from_date_max = null;
            cal_to_date_max = null;
            cal_from_date_min = null;
            cal_to_date_min = null;
        }
    }

    public void cal_to_date_changed(SelectEvent event) {
        cal_to_date = (Date) event.getObject();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="rdb_emp_status">
    String rdb_emp_status;

    public String getRdb_emp_status() {
        return rdb_emp_status;
    }

    public void setRdb_emp_status(String rdb_emp_status) {
        this.rdb_emp_status = rdb_emp_status;
    }

    private void setRdb_emp_status() {
        rdb_emp_status = "A";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_user_type">
    String ddl_user_type;

    public String getDdl_user_type() {
        return ddl_user_type;
    }

    public void setDdl_user_type(String ddl_user_type) {
        this.ddl_user_type = ddl_user_type;
    }

    private void setDdl_user_type() {
        ddl_user_type = "A";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_user">
    String ddl_user;
    List<SelectItem> ddl_user_options;

    public String getDdl_user() {
        return ddl_user;
    }

    public void setDdl_user(String ddl_user) {
        this.ddl_user = ddl_user;
    }

    public List<SelectItem> getDdl_user_options() {
        return ddl_user_options;
    }

    public void setDdl_user_options(List<SelectItem> ddl_user_options) {
        this.ddl_user_options = ddl_user_options;
    }

    private void setDdl_user() {
        if (showUsers || showRelievedUsers) {
            showUsers = true;
            List<FhrdEmpmst> lst_Empmsts = null;
            if (showRelievedUsers) {
                lst_Empmsts = fhrdEmpmstFacade.findAllRelievedUsers(globalData.getWorking_ou());
            } else {
                lst_Empmsts = fhrdEmpmstFacade.findAll(globalData.getWorking_ou(), false, false, false, null, null, null, false, false, false, new Integer(ddl_org_unit));
            }
            ddl_user_options = new ArrayList<SelectItem>();
            for (FhrdEmpmst m : lst_Empmsts) {
                ddl_user_options.add(new SelectItem(m.getUserId(), m.getUserName() + " (" + m.getEmpno() + ")"));
            }

            if (showRelievedUsers) {
                ddl_user = ddl_user_options.get(0).getValue().toString();
            } else {
                ddl_user_options.add(0, new SelectItem(null, "--- All ----"));
                ddl_user = null;
            }
        } else {
            ddl_user = null;
        }
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Report's General Methods">
    //<editor-fold defaultstate="collapsed" desc="setReportData">

    public void setReportData(String report_type, String report_sub_type, String report_name, String report_export_name, String report_loc_type) {
        this.hdn_report_sub_type = report_sub_type;
        this.hdn_report_type = report_type;
        this.hdn_report_name = report_name;
        this.hdn_report_exp_name = report_export_name;
        this.hdn_report_loc_type = report_loc_type;
        setDdl_org_unit();
        setDdl_year();
        setDdl_user();
        ddl_month = "1";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="hideAllGeneralParameters">

    public void hideAllGeneralParameters() {
        cal_from_date_label = "From Date:";
        showToDate = false;
        showNativeLevel = false;
        showOrgUnit = false;
        showFromDate = false;
        showCalMonths = false;
        showCalYear = false;
        organisationLevel = false;
        forEmpSalary = false;
        showEmpStatus = false;
        calBound = true;
        showRelievedUsers = false;
        showUsers = false;
        showUserType = false;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="runGeneralReport()">

    public void runGeneralReport() {
        ServletOutputStream servletOutputStream = null;
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            servletOutputStream = response.getOutputStream();
            Map hm = new HashMap();


            hm.put("p_year", ddl_year);
            hm.put("p_ou", ent_org_unit.getOumUnitSrno());
            hm.put("p_ouname", ent_org_unit.getOumName());
            hm.put("p_orgname", globalData.getOrganisationName());
            hm.put("p_from_date", cal_from_date);
            hm.put("p_to_date", cal_to_date);
            //hm.put("p_from_userid", txt_from_user_id);
            //hm.put("p_to_userid", txt_to_user_id);
            hm.put("p_native_level", "Y");
            hm.put("p_emp_status", rdb_emp_status);
            hm.put("p_user_type_srno", Integer.valueOf(ddl_user_type));
            hm.put("p_user_id", ddl_user); //pass null as default value
            hm.put("p_user_name", ddl_user == null ? null : fhrdEmpmstFacade.find(new Integer(ddl_user)).getUserName());




            Date v_from_date;
            Date v_to_date;
//            if (forEmpAttd) {
//                if (showCalMonths && showCalYear) {
//                    Date v_from;
//                    Date v_to;
//                    if (ddl_year != null && ddl_month.isEmpty() == false) {
//                        int v_year = Integer.parseInt(ddl_year);
//                        Calendar c1 = Calendar.getInstance();
//                        c1.set(v_year, Integer.parseInt(ddl_month) - 1, 1);
//
//                        v_from = c1.getTime();
//                        c1.add(Calendar.MONTH, 1);
//                        c1.add(Calendar.DATE, -1);
//                        v_to = c1.getTime();
//                        Calendar c2 = Calendar.getInstance();
//                        c2.set(v_year, Integer.parseInt(ddl_month) - 1, 5);
//                        v_to_date = c2.getTime();
//                        c2.add(Calendar.MONTH, -1);
//                        c2.add(Calendar.DATE, 1);
//                        v_from_date = c2.getTime();
//                        
//                        hm.put("p_from_date", v_from_date);
//                        hm.put("p_to_date", v_to_date);
//                    }
//                }
//            }
            if (forEmpSalary) {
                if (showCalMonths && showCalYear) {
                    if (ddl_year != null && ddl_month.isEmpty() == false) {
                        int v_year = Integer.parseInt(ddl_year);
                        Calendar c1 = Calendar.getInstance();
                        c1.set(v_year, Integer.parseInt(ddl_month) - 1, 1);

                        v_from_date = c1.getTime();
                        c1.add(Calendar.MONTH, 1);
                        c1.add(Calendar.DATE, -1);
                        v_to_date = c1.getTime();
                        System.out.println("for Salary form " + v_from_date + " to " + v_to_date);
                        hm.put("p_from_date", v_from_date);
                        hm.put("p_to_date", v_to_date);
                    }
                }
            }

            //Print & Verify Paramter to debug
            boolean debugParameters = false;
            if (debugParameters) {
                System.out.println("p_ou : " + hm.get("p_ou"));
                System.out.println("p_ouname : " + hm.get("p_ouname"));
                System.out.println("p_from_date : " + hm.get("p_from_date"));
                System.out.println("p_to_date : " + hm.get("p_to_date"));
                System.out.println("p_from_user : " + hm.get("p_from_userid"));
                System.out.println("p_to_user : " + hm.get("p_to_userid"));
                System.out.println("p_native_level : " + hm.get("p_native_level"));
                System.out.println("p_user_id : " + hm.get("p_user_id"));
                System.out.println("" + hdn_report_name);
                System.out.println("" + hdn_report_exp_name);
            }

            if (hdn_report_type.equals("E")) {

                try {
                    hm.put("IS_IGNORE_PAGINATION", true);
                    Connection cn = getGlobalUtilities().getjdbcFES_PROJECT_Oracle().getConnection();
                    try {
                        InputStream reportStream = context.getExternalContext().getResourceAsStream("/reports/" + (hdn_report_loc_type.equals("C") ? "common/" : "excel/") + hdn_report_name.trim() + ".jasper");
                        JasperPrint jasperPrint = JasperFillManager.fillReport(reportStream, hm, cn);
                        response.setContentType("application/vnd.ms-excel");
                        SimpleDateFormat sdf = new SimpleDateFormat();
                        sdf.applyPattern("dd-MM-yyyy_HH-mm");
                        String v_date = (sdf.format(new Date())).toString();

                        if (hdn_report_sub_type == null) {
                            String filename = hdn_report_exp_name + "_" + v_date + ".xls";
                            response.setHeader("Content-Disposition", "inline;filename=" + filename);
                            JExcelApiExporter exporterXLS = new JExcelApiExporter();
                            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
                            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
                            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response.getOutputStream());
                            exporterXLS.exportReport();
                        } else if (hdn_report_sub_type.equals("EX")) {
                            String filename = hdn_report_exp_name + "_" + v_date + ".xlsx";
                            response.setHeader("Content-Disposition", "inline;filename=" + filename);
                            JRXlsxExporter exporterXLS = new JRXlsxExporter();
                            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
                            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
                            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response.getOutputStream());
                            exporterXLS.exportReport();
                        }
                        servletOutputStream.flush();
                        servletOutputStream.close();
                        context.responseComplete();
                    } catch (JRException e) {
                        LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "runGeneralReport() - reportName : " + hdn_report_name, null, e);
                    } catch (Exception e) {
                        LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "runGeneralReport() - reportName : " + hdn_report_name, null, e);
                    } finally {
                        if (!cn.isClosed()) {
                            cn.close();
                        }
                    }
                } catch (SQLException ex) {
                    LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "runGeneralReport() - reportName : " + hdn_report_name, null, ex);
                }
            } else {
                try {
                    hm.put("IS_IGNORE_PAGINATION", false);
                    Connection cn = getGlobalUtilities().getjdbcFES_PROJECT_Oracle().getConnection();
                    try {
                        InputStream reportStream = context.getExternalContext().getResourceAsStream("/reports/" + (hdn_report_loc_type.equals("C") ? "common/" : "pdf/") + hdn_report_name.trim() + ".jasper");
                        response.setContentType("application/pdf");
                        response.setHeader("Cache-Control", "public, max-age=1");
                        JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, hm, cn);
                        cn.close();
                        servletOutputStream.flush();
                        servletOutputStream.close();
                        context.responseComplete();
                    } catch (JRException e) {
                        LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "runGeneralReport() - reportName : " + hdn_report_name, null, e);
                    } catch (Exception e) {
                        LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "runGeneralReport() - reportName : " + hdn_report_name, null, e);
                    } finally {
                        if (!cn.isClosed()) {
                            cn.close();
                        }
                    }
                } catch (SQLException ex) {
                    LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "runGeneralReport() - reportName : " + hdn_report_name, null, ex);
                }

            }
        } catch (IOException ex) {
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "runGeneralReport() - reportName : " + hdn_report_name, null, ex);
        } finally {
            try {
                servletOutputStream.close();
            } catch (IOException ex) {
                LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "runGeneralReport() - reportName : " + hdn_report_name, null, ex);
            }
        }
    }
    //</editor-fold>
    //</editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="Report's Link Methods">
    //<editor-fold defaultstate="collapsed" desc="attd_reg_clicked">

    public void attd_reg_clicked() {
        hideAllGeneralParameters();
        showOrgUnit = true;
        showNativeLevel = true;
        showFromDate = true;
        showToDate = true;
        showCalYear = false;
//        forEmpAttd = true;
        setReportData("P", null, "attd_reg", "Monthly Attendace_Register", "C");
        RequestContext.getCurrentInstance().execute("setReportDialog('" + hdn_report_type + "');");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="salaryslip_clicked">

    public void salaryslip_clicked() {
        hideAllGeneralParameters();
        showOrgUnit = true;
        showCalMonths = true;
        showCalYear = false;
        forEmpSalary = true;
        setReportData("P", null, "salaryslip", "EmployeeSalarySlip", "C");
        RequestContext.getCurrentInstance().execute("setReportDialog('" + hdn_report_type + "');");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="leaveconssumary_clicked">

    public void leaveconssumary_clicked() {
        hideAllGeneralParameters();
        showOrgUnit = true;
        showNativeLevel = true;
        showFromDate = true;
        showToDate = true;
        showCalYear = false;
        setReportData("P", null, "leaveconssumary", "EmployeeLeaveSummary", "C");
        RequestContext.getCurrentInstance().execute("setReportDialog('" + hdn_report_type + "');");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="leave_balance_report_clicked()"> 

    public void leave_balance_report_clicked() {
        hideAllGeneralParameters();
        showOrgUnit = true;
        showUsers = true;
        showNativeLevel = true;
        showFromDate = true;
        showToDate = true;
        showCalYear = false;
        setReportData("P", null, "leave_balance_report", "EmployeeLeaveBalance", "C");
        RequestContext.getCurrentInstance().execute("setReportDialog('" + hdn_report_type + "');");
    }
    //</editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="holiday_clicked()">

    public void holiday_clicked() {
        hideAllGeneralParameters();
        showCalYear = true;
        organisationLevel = true;
        setReportData("P", null, "HOLIDAY_SUMMARY", "HOLIDAYSUMMARY", "C");
        RequestContext.getCurrentInstance().execute("setReportDialog('" + hdn_report_type + "');");
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="empnonpayable_encashsum_pdf_clicked()"> 
    public void empnonpayable_encashsum_pdf_clicked() {
        hideAllGeneralParameters();
        showOrgUnit = true;
//        showNativeLevel = true;
        showFromDate = true;
        showToDate = true;
//        showCalYear = false;
        setReportData("P", null, "empnonpayable_encashsum", "Employee Non-Payable Days and Leave Encash Summary", "C");
        RequestContext.getCurrentInstance().execute("setReportDialog('" + hdn_report_type + "');");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="empnonpayable_encashsum_xls_clicked()"> 

    public void empnonpayable_encashsum_xls_clicked() {
        hideAllGeneralParameters();
        showOrgUnit = true;
//        showNativeLevel = true;
        showFromDate = true;
        showToDate = true;
//        showCalYear = false;
        setReportData("E", null, "empnonpayable_encashsum", "Employee Non-Payable Days and Leave Encash Summary", "C");
        RequestContext.getCurrentInstance().execute("setReportDialog('" + hdn_report_type + "');");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employee_encash_detail_pdf_clicked()"> 

    public void employee_encash_detail_pdf_clicked() {
        hideAllGeneralParameters();
        showOrgUnit = true;
//        showNativeLevel = true;
        showFromDate = true;
        showToDate = true;
//        showCalYear = false;
        setReportData("P", null, "employee_encash_detail", "Employee Encash Detail", "C");
        RequestContext.getCurrentInstance().execute("setReportDialog('" + hdn_report_type + "');");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employee_encash_detail_xls_clicked()"> 

    public void employee_encash_detail_xls_clicked() {
        hideAllGeneralParameters();
        showOrgUnit = true;
//        showNativeLevel = true;
        showFromDate = true;
        showToDate = true;
//        showCalYear = false;
        setReportData("E", null, "employee_encash_detail", "Employee Encash Detail", "C");
        RequestContext.getCurrentInstance().execute("setReportDialog('" + hdn_report_type + "');");
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="employee_non_pay_detail_pdf_clicked()"> 
    public void employee_non_pay_detail_pdf_clicked() {
        hideAllGeneralParameters();
        showOrgUnit = true;
//        showNativeLevel = true;
        showFromDate = true;
        showToDate = true;
//        showCalYear = false;
        setReportData("P", null, "employee_non_pay_detail", "Employee Non-Payable Days Detail", "C");
        RequestContext.getCurrentInstance().execute("setReportDialog('" + hdn_report_type + "');");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employee_non_pay_detail_xls_clicked()"> 

    public void employee_non_pay_detail_xls_clicked() {
        hideAllGeneralParameters();
        showOrgUnit = true;
//        showNativeLevel = true;
        showFromDate = true;
        showToDate = true;
//        showCalYear = false;
        setReportData("E", null, "employee_non_pay_detail", "Employee  Non-Payable Days Detail", "C");
        RequestContext.getCurrentInstance().execute("setReportDialog('" + hdn_report_type + "');");
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="empnonpayable_encash_det_clicked()"> 
    public void empnonpayable_encash_det_clicked() {
        hideAllGeneralParameters();
        showOrgUnit = true;
        showFromDate = true;
        showToDate = true;
        showEmpStatus = true;
        setReportData("P", null, "empnonpayable_encash_det", "Employee Non-Payable Days and Leave Encash Summary", "C");
        RequestContext.getCurrentInstance().execute("setReportDialog('" + hdn_report_type + "');");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employee_list_pdf_clicked()"> 

    public void employee_list_pdf_clicked() {
        hideAllGeneralParameters();
        cal_from_date_label = "As on Date:";
        showOrgUnit = true;
        showFromDate = true;
        showEmpStatus = true;
        showUserType = true;
        setReportData("P", null, "employee_list", "Employee List", "C");
        RequestContext.getCurrentInstance().execute("setReportDialog('" + hdn_report_type + "');");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="employee_list_xls_clicked()"> 

    public void employee_list_xls_clicked() {
        hideAllGeneralParameters();
        cal_from_date_label = "As on Date:";
        showOrgUnit = true;
        showFromDate = true;
        showEmpStatus = true;
        showUserType = true;
        setReportData("E", null, "employee_list", "Employee List", "C");
        RequestContext.getCurrentInstance().execute("setReportDialog('" + hdn_report_type + "');");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="recentContractEnd_clicked"> 

    public void recentContractEnd_clicked() {
        hideAllGeneralParameters();
        showFromDate = true;
        showToDate = true;
        calBound = false;
        setReportData("P", null, "RecentContractEnd", "RecentContractEnd", "C");
        RequestContext.getCurrentInstance().execute("setReportDialog('" + hdn_report_type + "');");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="relieve_settlement_clicked"> 

    public void relieve_settlement_clicked() {
        hideAllGeneralParameters();
        showRelievedUsers = true;
        setReportData("P", null, "relieve_settlement", "relieve_settlement", "C");
        RequestContext.getCurrentInstance().execute("setReportDialog('" + hdn_report_type + "');");
    }
    //</editor-fold>

    public void locked_attd_leave_summary_clicked() {
        hideAllGeneralParameters();
        showFromDate = true;
        showToDate = true;
        showOrgUnit = true;
        showUsers = true;
        showEmpStatus = true;
        setReportData("E", null, "locked_attd_leave_summary", "locked_attd_leave_summary", "C");
        RequestContext.getCurrentInstance().execute("setReportDialog('" + hdn_report_type + "');");
    }

    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="init()"> 
    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        calBound = true;
        setRdb_emp_status();
        setDdl_user_type();
        setMinMaxDate();
        setCal_from_date();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Default constructor and other bean methods">

    /**
     * Creates a new instance of PIS_Utility_Reports
     */
    public PIS_Utility_Reports() {
    }

    //<editor-fold defaultstate="collapsed" desc="global settings">
    // <editor-fold defaultstate="collapsed" desc="getGlobalSettings">
    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="getFIS_GlobalSettings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="getGlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    //</editor-fold>  
    //</editor-fold>
}
