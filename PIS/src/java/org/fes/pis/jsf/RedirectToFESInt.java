package org.fes.pis.jsf;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.fes.pis.custom_entities.System_Properties;

@ManagedBean
@RequestScoped
public class RedirectToFESInt implements Serializable {

    System_Properties system_Properties = new System_Properties();
    String txt = new String();

    public String getTxt() {
        return txt;
    }

    public void setTxt(String txt) {
        this.txt = txt;
    }

    @PostConstruct
    public void init() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        try {
            String v_forward_uri = FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get("javax.servlet.forward.request_uri").toString();
            HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            if (!getGlobalResources().isRedirectToFESInt()) {
                response.sendRedirect("/" + system_Properties.getSystemName() + "/Login.htm");
            } else {
                response.sendRedirect("/fes-integrated/Login.htm?ref_url=" + v_forward_uri);
            }
        } catch (Exception ex) {
            Logger.getLogger(RedirectToFESInt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public RedirectToFESInt() {
    }
    // <editor-fold defaultstate="collapsed" desc="getGlobalResources">

    protected GlobalResources getGlobalResources() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalResources) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalResources");
    }// </editor-fold>
}
