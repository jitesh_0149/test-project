package org.fes.pis.jsf;

import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import org.fes.pis.custom_entities.System_Properties;

@ManagedBean(name = "Login")
@RequestScoped
public class Login implements Serializable {

    System_Properties system_Properties = new System_Properties();
    String txt = new String();

    public String getTxt() {
        return txt;
    }

    public void setTxt(String txt) {
        this.txt = txt;
    }

    @PostConstruct
    public void init() {
        FacesContext facesContext = FacesContext.getCurrentInstance();        
        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        if (!getGlobalResources().isRedirectToFESInt()) {
            String v_user_id = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
            if (v_user_id != null) {
                try {
                    response.sendRedirect("/" + system_Properties.getSystemUrl() + "/");
                } catch (Exception ex) {
                    Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            try {
                response.sendRedirect("/fes-integrated/Login.htm");
            } catch (IOException ex) {
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public Login() {
    }
    // <editor-fold defaultstate="collapsed" desc="getGlobalResources">

    protected GlobalResources getGlobalResources() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalResources) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalResources");
    }// </editor-fold>
}