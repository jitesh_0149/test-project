/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.converters;

import com.ibm.icu.text.NumberFormat;
import java.math.BigDecimal;
import java.util.Locale;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author jitesh
 */
@FacesConverter("singleFractionDigit")
public class singleFractionDigit implements Converter {

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent component, String submittedValue) {
//        if (submittedValue == null) {
//            return null;
//        } else {
//            if (submittedValue.trim().equals("")) {
//                return null;
//            }
//            try {
//                Format format = com.ibm.icu.text.NumberFormat.getNumberInstance(new Locale("en", "in"));
//                return format.format(new BigDecimal(submittedValue));
//
//            } catch (Exception exception) {
//                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid player"));
//            }
//        }
        return null;
//        return null;  
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Object value) {
        if (value == null) {
            return null;
        } else {
            if (value.toString().trim().equals("")) {
                return null;
            }
            try {
                Locale l = new Locale("en", "in");
                NumberFormat format = com.ibm.icu.text.NumberFormat.getNumberInstance(l);
                format.setMinimumFractionDigits(1);
                return format.format(new BigDecimal(value.toString()));

            } catch (Exception exception) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid Number"));
            }
        }
    }
}
