package org.fes.pis.jsf;

import java.io.Serializable;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.fes.pis.custom_entities.System_Properties;

@ManagedBean
@ApplicationScoped
public class ApplicationProperties implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="Application Version">
    String version = System_Properties.version;

    public String getVersion() {
        return version;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Defautl Constructor">
    public ApplicationProperties() {
    }
    //</editor-fold>
}
