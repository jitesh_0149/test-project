/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.pis.payroll;

//import java.io.Serializable;
//import java.math.BigDecimal;
//import java.math.BigInteger;
//import java.sql.Connection;
//import java.sql.ResultSet;
//import java.sql.Statement;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.logging.Level;
//import javax.annotation.PostConstruct;
//import javax.ejb.EJB;
//import javax.faces.application.FacesMessage;
//import javax.faces.bean.ManagedBean;
//import javax.faces.bean.ViewScoped;
//import javax.faces.context.FacesContext;
//import javax.faces.event.ActionEvent;
//import org.fes.lib.utilities.DateTimeUtility;
//import org.fes.lib.utilities.LogGenerator;
//import org.fes.pis.custom_entities.AttendanceLockDataList;
//import org.fes.pis.custom_entities.Emp_PF_List;
//import org.fes.pis.custom_entities.PIS_GlobalData;
//import org.fes.pis.custom_entities.System_Properties;
//import org.fes.pis.db.fes_project.pack.Ou_Pack;
//import org.fes.pis.entities.FhrdEmpmst;
//import org.fes.pis.entities.FhrdPfcontr;
//import org.fes.pis.jsf.GlobalSettings;
//import org.fes.pis.jsf.GlobalUtilities;
//import org.fes.pis.jsf.PIS_GlobalSettings;
//import org.fes.pis.sessions.FhrdPfcontrFacadeLocal;
/**
 *
 * @author jitesh
 */
//@ManagedBean(name = "PIS_PF_Entry")
//@ViewScoped
public class PIS_PF_Entry {
//implements Serializable{
//
//    @EJB
//    private FhrdPfcontrFacadeLocal fhrdPfcontrFacade;
//    //<editor-fold defaultstate="collapsed" desc="Declarations">
//
//    
//    System_Properties system_Properties = new System_Properties();
//    String systemName = system_Properties.getSystemName();
//    Ou_Pack ou_Pack = new Ou_Pack();
//    public static Integer v_user_type = 1;
//    //</editor-fold>  
//    //<editor-fold defaultstate="collapsed" desc="globalData">
//    PIS_GlobalData globalData;
//
//    public PIS_GlobalData getGlobalData() {
//        return globalData;
//    }
//
//    public void setGlobalData(PIS_GlobalData globalData) {
//        this.globalData = globalData;
//    }
//    //</editor-fold>
//    //<editor-fold defaultstate="collapsed" desc="tbl_emp_pf"> 
//    List<Emp_PF_List> tbl_emp_pf;
//    List<Emp_PF_List> tbl_emp_pf_filtered;
//
//    public List<Emp_PF_List> getTbl_emp_pf() {
//        return tbl_emp_pf;
//    }
//
//    public void setTbl_emp_pf(List<Emp_PF_List> tbl_emp_pf) {
//        this.tbl_emp_pf = tbl_emp_pf;
//    }
//
//    public void setTbl_emp_pf() {
//        tbl_emp_pf = getEmp_pf_list(2015, 1);
//
//    }
//
//    public List<Emp_PF_List> getTbl_emp_pf_filtered() {
//        return tbl_emp_pf_filtered;
//    }
//
//    public void setTbl_emp_pf_filtered(List<Emp_PF_List> tbl_emp_pf_filtered) {
//        this.tbl_emp_pf_filtered = tbl_emp_pf_filtered;
//    }
//    //</editor-fold>
//    //<editor-fold defaultstate="collapsed" desc="ent_emp_pf_amount"> 
//    Emp_PF_List ent_emp_pf_amount;
//    
//    public Emp_PF_List getEnt_emp_pf_amount() {
//        return ent_emp_pf_amount;
//    }
//    
//    public void setEnt_emp_pf_amount(Emp_PF_List ent_emp_pf_amount) {
//        this.ent_emp_pf_amount = ent_emp_pf_amount;
//    }
//    //</editor-fold>
//    //<editor-fold defaultstate="collapsed" desc="getEmp_pf_list"> 
//    public List<Emp_PF_List> getEmp_pf_list(Integer p_year, Integer p_month) {
//        List<Emp_PF_List> lst_emp = new ArrayList<Emp_PF_List>();
//        FacesContext context = FacesContext.getCurrentInstance();
//        String CMD_EMP_LIST = "SELECT B.PF_MONTH_NO, B.PF_YEAR, A.USER_ID, A.EMPNO, A.USER_NAME, B.PF_ACTNO,A.NTGCFPFNO,"
//                + "     B.PF_SALARY, B.EMP_CONTR_PF, B.EMP_CONTR_VPF, B.EMP_CONTR_FPF, B.EMPR_CONTR_PF, B.EMPR_CONTR_FPF"
//                + " FROM FHRD_EMPMST A"
//                + " LEFT OUTER JOIN FHRD_PFCONTR B"
//                + " ON (A.USER_ID        =B.USER_ID)"
//                + " WHERE A.USER_ID NOT IN"
//                + "     ( SELECT USER_ID FROM FHRD_PFCONTR WHERE PF_YEAR = " + p_year + " AND PF_MONTH_NO =" + p_month
//                + "     )"
//                + " AND A.USER_TYPE_SRNO                             =1"
//                + " AND NVL(A.TERMINATION_DATE ,A.ORG_RELIEVE_DATE) IS NULL"
//                + " ORDER BY A.USER_ID";
//        Connection cn = null;
//        try {
//            Statement st = null;
//            try {
//                ResultSet res = null;
//                try {
//                    cn = getGlobalUtilities().getjdbcFES_PROJECT_Oracle().getConnection();
//                    int i = 0;
//                    st = cn.createStatement();
//                    res = st.executeQuery(CMD_EMP_LIST);
//                    while (res.next()) {
//                        Emp_PF_List ent_Emp_PF_List = new Emp_PF_List();
//                        ent_Emp_PF_List.setRowid(i);
//                        ent_Emp_PF_List.setUserID(res.getInt("USER_ID"));
//                        ent_Emp_PF_List.setUserName(res.getString("USER_NAME"));
//                        ent_Emp_PF_List.setEmpno(res.getInt("EMPNO"));
//                        ent_Emp_PF_List.setMonth(res.getInt("PF_MONTH_NO"));
//                        ent_Emp_PF_List.setYear(res.getInt("PF_YEAR"));
//                        ent_Emp_PF_List.setPfActno(res.getBigDecimal("PF_ACTNO"));
//                        ent_Emp_PF_List.setPfSalary(res.getBigDecimal("PF_SALARY"));
//                        ent_Emp_PF_List.setPfEmpPf(res.getBigDecimal("EMP_CONTR_PF"));
//                        ent_Emp_PF_List.setPfEmpVpf(res.getBigDecimal("EMP_CONTR_VPF"));
//                        ent_Emp_PF_List.setPfEmpFpf(res.getBigDecimal("EMP_CONTR_FPF"));
//                        ent_Emp_PF_List.setPfEmprPf(res.getBigDecimal("EMPR_CONTR_PF"));
//                        ent_Emp_PF_List.setPfEmprFpf(res.getBigDecimal("EMPR_CONTR_FPF"));
//                        ent_Emp_PF_List.setNtgcfPFNo(res.getString("NTGCFPFNO"));
//
//
//                        lst_emp.add(i, ent_Emp_PF_List);
//                        i++;
//                    }
//                } catch (Exception e) {
//                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Can't Find Employee List", "Some unexpected error occurred during finding missing attendance"));
//                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "getEmp_pf_list", null, e);
//                } finally {
//                    res.close();
//                }
//            } catch (Exception e) {
//                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Can't Find Employee List", "Some unexpected error occurred during finding missing attendance"));
//                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "getEmp_pf_list", null, e);
//            } finally {
//                st.close();
//            }
//        } catch (Exception ex) {
//            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "Some unexpected error occurred during finding missing attendance"));
//            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "getEmp_pf_list", null, ex);
//        } finally {
//            if (cn != null) {
//                try {
//                    if (!cn.isClosed()) {
//                        cn.close();
//                    }
//                } catch (Exception e) {
//                }
//            }
//        }
//        return lst_emp;
//    }
//    //</editor-fold>
//    //<editor-fold defaultstate="collapsed" desc="dialog text fields">
//    //<editor-fold defaultstate="collapsed" desc="txt_pf">
//    String txt_pf;
//    
//    public String getTxt_pf() {
//        return txt_pf;
//    }
//    
//    public void setTxt_pf(String txt_pf) {
//        this.txt_pf = txt_pf;
//    }
//    //</editor-fold>
//    //<editor-fold defaultstate="collapsed" desc="txt_vpf">
//    String txt_vpf;
//    
//    public String getTxt_vpf() {
//        return txt_vpf;
//    }
//    
//    public void setTxt_vpf(String txt_vpf) {
//        this.txt_vpf = txt_vpf;
//    }
//    //</editor-fold>
//    //<editor-fold defaultstate="collapsed" desc="txt_fpf">
//    String txt_fpf;
//    
//    public String getTxt_fpf() {
//        return txt_fpf;
//    }
//    
//    public void setTxt_fpf(String txt_fpf) {
//        this.txt_fpf = txt_fpf;
//    }
//    //</editor-fold>
//    //<editor-fold defaultstate="collapsed" desc="txt_emplr_pf">
//    String txt_emplr_pf;
//    
//    public String getTxt_emplr_pf() {
//        return txt_emplr_pf;
//    }
//    
//    public void setTxt_emplr_pf(String txt_emplr_pf) {
//        this.txt_emplr_pf = txt_emplr_pf;
//    }
//    //</editor-fold>
//    //<editor-fold defaultstate="collapsed" desc="txt_emplr_fpf">
//    String txt_emplr_fpf;
//    
//    public String getTxt_emplr_fpf() {
//        return txt_emplr_fpf;
//    }
//    
//    public void setTxt_emplr_fpf(String txt_emplr_fpf) {
//        this.txt_emplr_fpf = txt_emplr_fpf;
//    }
//    //</editor-fold>
//    //</editor-fold>
//    
//    //<editor-fold defaultstate="collapsed" desc="btn_set_pf_amounts_clicked"> 
//    public void btn_set_pf_amounts_clicked(ActionEvent event){
//        System.out.println("sdf");
//        FhrdPfcontr entFhrdPfcontr=new FhrdPfcontr();
//        entFhrdPfcontr.setCreateby(globalData.getEnt_login_user());
//        entFhrdPfcontr.setCreatedt(new Date());
//        entFhrdPfcontr.setEmpContrFpf(new Long(txt_fpf));
//        entFhrdPfcontr.setEmpContrPf(new Long(txt_pf));
//        entFhrdPfcontr.setEmpno(ent_emp_pf_amount.getUserID());
//        entFhrdPfcontr.setEmprContrFpf(new Long(txt_emplr_fpf));
//        entFhrdPfcontr.setEmprContrPf(new Long(txt_emplr_pf));
//        entFhrdPfcontr.setOumUnitSrno(globalData.getEnt_working_ou());
//        entFhrdPfcontr.setPfActno(ent_emp_pf_amount.getPfActno().toString());
//        entFhrdPfcontr.setPfMonthNo(new Short(ent_emp_pf_amount.getMonth().toString()));
//        entFhrdPfcontr.setPfSalary(new BigDecimal(ent_emp_pf_amount.getPfSalary().toString()));
//        entFhrdPfcontr.setPfYear(ent_emp_pf_amount.getYear().toString());
//        entFhrdPfcontr.setPfcSrgKey(BigDecimal.ZERO);
//        entFhrdPfcontr.setUserId(new FhrdEmpmst(ent_emp_pf_amount.getUserID()));
//        
//        try{
//            fhrdPfcontrFacade.create(entFhrdPfcontr);
//            
//        }catch(Exception e){}
//    }
//    //</editor-fold>
//    
//    //<editor-fold defaultstate="collapsed" desc="init()"> 
//    @PostConstruct
//    public void init() {
//        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
//        setTbl_emp_pf();
//    }
//    //</editor-fold>
//    //<editor-fold defaultstate="collapsed" desc="default and global methods">
//    // <editor-fold defaultstate="collapsed" desc="Global Settings">
//
//    protected GlobalSettings getGlobalSettings() {
//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
//    }// </editor-fold>
//    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">
//
//    protected PIS_GlobalSettings getPIS_GlobalSettings() {
//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
//    }// </editor-fold>
//    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">
//
//    protected GlobalUtilities getGlobalUtilities() {
//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
//    }// </editor-fold>
//
//    /**
//     * Creates a new instance of PIS_PF_Entry
//     */
//    public PIS_PF_Entry() {
//    }
//    //</editor-fold>
}