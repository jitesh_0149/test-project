/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.pis.payroll;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.DisbursemenDatatList;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.customJpaController;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.FhrdPaytransSummary;
import org.fes.pis.jsf.GlobalResources;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;
import org.fes.pis.sessions.FhrdPaytransSummaryFacadeLocal;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Salary_Disbursement")
@ViewScoped
public class PIS_Salary_Disbursement implements Serializable {

    @EJB
    private customJpaController customJpaController;
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    @EJB
    private FhrdPaytransSummaryFacadeLocal fhrdPaytransSummaryFacade;
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="declarations">
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    Ou_Pack ou_Pack = new Ou_Pack();
    // </editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="default">

    /**
     * Creates a new instance of PIS_Salary_Disbursement
     */
    public PIS_Salary_Disbursement() {
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_status">
    String ddl_status;

    public String getDdl_status() {
        return ddl_status;
    }

    public void setDdl_status(String ddl_status) {
        this.ddl_status = ddl_status;
    }

    public void ddl_status_changed(ValueChangeEvent event) {
        ddl_status = event.getNewValue() == null ? null : event.getNewValue().toString();
        //        setTblEmpInfo();
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="tbl_emp_list"> 
    List<DisbursemenDatatList> tbl_disbursement_list;

    public List<DisbursemenDatatList> getTbl_disbursement_list() {
        return tbl_disbursement_list;
    }

    public void setTbl_disbursement_list(List<DisbursemenDatatList> tbl_disbursement_list) {
        this.tbl_disbursement_list = tbl_disbursement_list;
    }

    public void setTbl_emp_list() {
        tbl_disbursement_list = findAllDisburseData();
    }
    DisbursemenDatatList[] selectedLists;

    public DisbursemenDatatList[] getSelectedLists() {
        return selectedLists;
    }

    public void setSelectedLists(DisbursemenDatatList[] selectedLists) {
        this.selectedLists = selectedLists;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="refreshTable"> 
    public void refreshTable() {
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_approve_clicked"> 

    public void btn_confirm_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (selectedLists.length > 0) {
            try {
                int n = selectedLists.length;
                String v_user_id_list = "";
                for (int i = 0; i < n; i++) {
                    if (i == 0) {
                        v_user_id_list = selectedLists[i].getUser_id().toString();
                    } else {
                        v_user_id_list += "," + selectedLists[i].getUser_id().toString();
                    }

                }
                List<FhrdPaytransSummary> lsFhrdPaytransSummarys = fhrdPaytransSummaryFacade.findToDisburse(v_user_id_list);
                int m = lsFhrdPaytransSummarys.size();
                List<FhrdPaytransSummary> lstSummarys = new ArrayList<FhrdPaytransSummary>();
                for (int i = 0; i < m; i++) {
                    FhrdPaytransSummary entFhrdPaytransSummary = lsFhrdPaytransSummarys.get(i);
                    entFhrdPaytransSummary.setDisburseFlag("Y");
                    entFhrdPaytransSummary.setDisburseDate(new Date());
                    lstSummarys.add(entFhrdPaytransSummary);
                }
                boolean isTransactionDone = customJpaController.setDisbursedYes(lstSummarys);
                if (isTransactionDone) {
                    try {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Updated Successfully", "Selected user salary disbursed Successfully."));
                        setTbl_emp_list();
                        selectedLists = null;
                    } catch (Exception e) {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed reset data", "Selected user salary disbursed Successfully but failed reset data. Please reload the page"));
                        LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_accept_clicked", null, e);
                    }
                } else {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during process."));
                }
            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during Accepting Application."));
                LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_accept_clicked", null, e);
            }
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Select Application", "Please select any Application to Approve/Reject."));
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_reject_clicked">

    public void btn_cancel_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
//        if (selectedLists.length > 0) {
//            try {
//                boolean isTransactionDone = customJpaController.ApproveRejectAppls(selectedApplications, "N", globalData.getUser_id());
//                if (isTransactionDone) {
//                    try {
//                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Application Rejected", "Selected Application Rejected Successfully."));
//                        setTbl_approval_list();
//                        entFtasAbsentees_detail = null;
//                    } catch (Exception e) {
//                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed reset data", "Selected Application Rejected Successfully but failed reset data. Please reload the page"));
//                        LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_reject_clicked", null, e);
//                    }
//                } else {
//                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during Rejecting Application."));
//                }
//            } catch (Exception e) {
//                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during Rejecting Application."));
//                LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_reject_clicked", null, e);
//            }
//        } else {
//            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Select Application", "Please select any Application to Approve/Reject."));
//        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="findAllDisburseData"> 

    public List<DisbursemenDatatList> findAllDisburseData() {
        List<DisbursemenDatatList> lst_b_data = new ArrayList<DisbursemenDatatList>();
        Connection cn = null;
        String v_query = "SELECT B.USER_ID, "
                + " TO_CHAR(B.MIN_SALARY_DATE,'MON-YYYY') FROM_SALARY_DATE, "
                + " TO_CHAR(B.MAX_SALARY_DATE,'MON-YYYY') TO_SALARY_DATE, "
                + " TOTAL_PAY,TOTAL_EARNING, "
                + " SUM(PM.EMP_NON_PAYABLE_DAYS) EMP_NON_PAYABLE_DAYS "
                + " FROM   FHRD_PAYTRANSLEAVE_MST PM,( "
                + " SELECT USER_ID, "
                + " MIN(ORG_SALARY_FROM_DATE) MIN_SALARY_DATE, "
                + " MAX(ORG_SALARY_TO_DATE) MAX_SALARY_DATE, "
                + " MIN(ORG_ATTENDANCE_FROM_DATE) MIN_ATTD_DATE, "
                + " MAX(ORG_ATTENDANCE_TO_DATE)   MAX_ATTD_DATE, "
                + " SUM(TOTAL_EARNING) TOTAL_EARNING,SUM(TOTAL_PAY) TOTAL_PAY "
                + " FROM   FHRD_PAYTRANS_SUMMARY "
                + " WHERE  DISBURSE_FLAG='N' "
                + " GROUP BY USER_ID) B "
                + " WHERE  (PM.ORG_ATTENDANCE_FROM_DATE BETWEEN B.MIN_ATTD_DATE AND B.MAX_ATTD_DATE) "
                + " AND    (PM.ORG_ATTENDANCE_TO_DATE BETWEEN B.MIN_ATTD_DATE AND B.MAX_ATTD_DATE) "
                + " AND    PM.USER_ID=B.USER_ID "
                + " GROUP  BY B.USER_ID,B.MIN_SALARY_DATE,B.MAX_SALARY_DATE,B.TOTAL_PAY,B.TOTAL_EARNING"
                + " ORDER BY USER_ID";

//        System.out.println("query " + v_query);
        try {
            Statement st = null;
            try {
                ResultSet res = null;
                try {
                    cn = getGlobalUtilities().getjdbcFES_PROJECT_Oracle().getConnection();
                    int i = 0;
                    st = cn.createStatement();
                    ////                    System.out.println("query "+v_query);
                    res = st.executeQuery(v_query);
                    while (res.next()) {
                        DisbursemenDatatList ent_DatatList = new DisbursemenDatatList();
                        ent_DatatList.setUser_id(res.getInt("USER_ID"));
                        FhrdEmpmst entEmpmst = fhrdEmpmstFacade.find(ent_DatatList.getUser_id());
                        ent_DatatList.setUser_name(entEmpmst.getUserName());
                        ent_DatatList.setRowid(i);
                        ent_DatatList.setTotal_pay(res.getBigDecimal("TOTAL_PAY"));
                        ent_DatatList.setTotal_pay(res.getBigDecimal("TOTAL_EARNING"));
                        ent_DatatList.setFrom_month(res.getString("FROM_SALARY_DATE"));
                        ent_DatatList.setTo_month(res.getString("FROM_SALARY_DATE"));

                        lst_b_data.add(ent_DatatList);
                        i++;
                    }
                } catch (SQLException e) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "findAllDisburseData", null, e);
                } finally {
                    res.close();
                }
            } catch (SQLException sQLException) {
            } finally {
                st.close();
            }
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "findAllDisburseData", null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        cn = null;
                    }
                } catch (Exception e) {
                }
            }
        }
        return lst_b_data;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="init()"> 
    @PostConstruct
    public void init() {
        setTbl_emp_list();
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalResources">

    protected GlobalResources getGlobalResources() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalResources) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalResources");
    }// </editor-fold>
    // </editor-fold>    
}
