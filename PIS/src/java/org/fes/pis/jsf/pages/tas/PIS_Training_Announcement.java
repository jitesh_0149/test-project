package org.fes.pis.jsf.pages.tas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.validation.ConstraintViolationException;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.custom_entities.FtasTrainingScheduleDtlEntry;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.customJpaController;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.*;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;
import org.fes.pis.sessions.FtasCategoryMstFacadeLocal;
import org.fes.pis.sessions.FtasTrainingMstFacadeLocal;
import org.fes.pis.sessions.SystOrgUnitMstFacadeLocal;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DualListModel;

@ManagedBean(name = "PIS_Training_Announcement")
@ViewScoped
public class PIS_Training_Announcement implements Serializable {
    //<editor-fold defaultstate="collapsed" desc="globalData">

    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Declarations"> 

    
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    boolean isValidUser = false;
    Ou_Pack ou_Pack = new Ou_Pack();
    //</editor-fold> 
    // <editor-fold defaultstate="collapsed" desc="EJB Declaration">
    @EJB
    private FtasTrainingMstFacadeLocal ftasTrainingMstFacade;
    @EJB
    private customJpaController customJpaController;
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    @EJB
    private FtasCategoryMstFacadeLocal ftasCategoryMstFacade;
    @EJB
    private SystOrgUnitMstFacadeLocal systOrgUnitMstFacade;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="default methods">

    /**
     * Creates a new instance of PIS_Training_Announcement
     */
    public PIS_Training_Announcement() {
//        globalData.getWorking_ou() = globalData.getWorking_ou();
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Binded Attributes">
    // <editor-fold defaultstate="collapsed" desc="binded methods panel 1">
    String txt_subject;
    String txt_training_co_first_name;
    String txt_training_co_last_name;
    String txt_training_announcement_co_contact_no;

    // <editor-fold defaultstate="collapsed" desc="getter/setter">
    public String getTxt_subject() {
        return txt_subject;
    }

    public void setTxt_subject(String txt_subject) {
        this.txt_subject = txt_subject;
    }

    public String getTxt_training_co_first_name() {
        return txt_training_co_first_name;
    }

    public void setTxt_training_co_first_name(String txt_training_co_first_name) {
        this.txt_training_co_first_name = txt_training_co_first_name;
    }

    public String getTxt_training_co_last_name() {
        return txt_training_co_last_name;
    }

    public void setTxt_training_co_last_name(String txt_training_co_last_name) {
        this.txt_training_co_last_name = txt_training_co_last_name;
    }

    public String getTxt_training_announcement_co_contact_no() {
        return txt_training_announcement_co_contact_no;
    }

    public void setTxt_training_announcement_co_contact_no(String txt_training_announcement_co_contact_no) {
        this.txt_training_announcement_co_contact_no = txt_training_announcement_co_contact_no;
    }
    // </editor-fold>
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Institute Address">
    String txt_institute_name;
    String txt_institute_address;
    String txt_institute_city;
    String txt_institute_state;
    String txt_institute_pincode;
    String txt_institute_country;
    String txt_institute_contacts;

    // <editor-fold defaultstate="collapsed" desc="getter/setter">
    public String getTxt_institute_name() {
        return txt_institute_name;
    }

    public void setTxt_institute_name(String txt_institute_name) {
        this.txt_institute_name = txt_institute_name;
    }

    public String getTxt_institute_address() {
        return txt_institute_address;
    }

    public void setTxt_institute_address(String txt_institute_address) {
        this.txt_institute_address = txt_institute_address;
    }

    public String getTxt_institute_city() {
        return txt_institute_city;
    }

    public void setTxt_institute_city(String txt_institute_city) {
        this.txt_institute_city = txt_institute_city;
    }

    public String getTxt_institute_contacts() {
        return txt_institute_contacts;
    }

    public void setTxt_institute_contacts(String txt_institute_contacts) {
        this.txt_institute_contacts = txt_institute_contacts;
    }

    public String getTxt_institute_country() {
        return txt_institute_country;
    }

    public void setTxt_institute_country(String txt_institute_country) {
        this.txt_institute_country = txt_institute_country;
    }

    public String getTxt_institute_pincode() {
        return txt_institute_pincode;
    }

    public void setTxt_institute_pincode(String txt_institute_pincode) {
        this.txt_institute_pincode = txt_institute_pincode;
    }

    public String getTxt_institute_state() {
        return txt_institute_state;
    }

    public void setTxt_institute_state(String txt_institute_state) {
        this.txt_institute_state = txt_institute_state;
    }

    public String getTxt_venue_contacts() {
        return txt_venue_contacts;
    }

    public void setTxt_venue_contacts(String txt_venue_contacts) {
        this.txt_venue_contacts = txt_venue_contacts;
    }// </editor-fold>
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="panel 2 venue">
    String txt_venue_name;
    String txt_venue_address;
    String txt_venue_city;
    String txt_venue_contacts;
    String txt_venue_pincode;
    String txt_venue_state;
    String txt_venue_country;

    // <editor-fold defaultstate="collapsed" desc="getter/setter">
    public String getTxt_venue_address() {
        return txt_venue_address;
    }

    public void setTxt_venue_address(String txt_venue_address) {
        this.txt_venue_address = txt_venue_address;
    }

    public String getTxt_venue_city() {
        return txt_venue_city;
    }

    public void setTxt_venue_city(String txt_venue_city) {
        this.txt_venue_city = txt_venue_city;
    }

    public String getTxt_venue_contact_no() {
        return txt_venue_contacts;
    }

    public void setTxt_venue_contact_no(String txt_venue_contacts) {
        this.txt_venue_contacts = txt_venue_contacts;
    }

    public String getTxt_venue_country() {
        return txt_venue_country;
    }

    public void setTxt_venue_country(String txt_venue_country) {
        this.txt_venue_country = txt_venue_country;
    }

    public String getTxt_venue_name() {
        return txt_venue_name;
    }

    public void setTxt_venue_name(String txt_venue_name) {
        this.txt_venue_name = txt_venue_name;
    }

    public String getTxt_venue_pincode() {
        return txt_venue_pincode;
    }

    public void setTxt_venue_pincode(String txt_venue_pincode) {
        this.txt_venue_pincode = txt_venue_pincode;
    }

    public String getTxt_venue_state() {
        return txt_venue_state;
    }

    public void setTxt_venue_state(String txt_venue_state) {
        this.txt_venue_state = txt_venue_state;
    }// </editor-fold>
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="objective.... and fees">
    String txt_objective;
    String txt_topics;
    String txt_benefits;
    String txt_eligibility;
    String txt_tuition_fees = "0";
    String txt_tuition_lodging = "0";
    String txt_tuition_lodging_boarding = "0";
    String txt_other_fees_desc;
    String txt_other_fees_amount = "0";

    // <editor-fold defaultstate="collapsed" desc="getter/setter">
    public String getTxt_objective() {
        return txt_objective;
    }

    public void setTxt_objective(String txt_objective) {
        this.txt_objective = txt_objective;
    }

    public String getTxt_other_fees_amount() {
        return txt_other_fees_amount;
    }

    public void setTxt_other_fees_amount(String txt_other_fees_amount) {
        this.txt_other_fees_amount = txt_other_fees_amount;
    }

    public String getTxt_other_fees_desc() {
        return txt_other_fees_desc;
    }

    public void setTxt_other_fees_desc(String txt_other_fees_desc) {
        this.txt_other_fees_desc = txt_other_fees_desc;
    }

    public String getTxt_benefits() {
        return txt_benefits;
    }

    public void setTxt_benefits(String txt_benefits) {
        this.txt_benefits = txt_benefits;
    }

    public String getTxt_eligibility() {
        return txt_eligibility;
    }

    public void setTxt_eligibility(String txt_eligibility) {
        this.txt_eligibility = txt_eligibility;
    }

    public String getTxt_topics() {
        return txt_topics;
    }

    public void setTxt_topics(String txt_topics) {
        this.txt_topics = txt_topics;
    }

    public String getTxt_tuition_lodging_boarding() {
        return txt_tuition_lodging_boarding;
    }

    public void setTxt_tuition_lodging_boarding(String txt_tuition_lodging_boarding) {
        this.txt_tuition_lodging_boarding = txt_tuition_lodging_boarding;
    }

    public String getTxt_tuition_fees() {
        return txt_tuition_fees;
    }

    public void setTxt_tuition_fees(String txt_tuition_fees) {
        this.txt_tuition_fees = txt_tuition_fees;
    }

    public String getTxt_tuition_lodging() {
        return txt_tuition_lodging;
    }

    public void setTxt_tuition_lodging(String txt_tuition_lodging) {
        this.txt_tuition_lodging = txt_tuition_lodging;
    }
    // </editor-fold>
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="time and date values">
//    Date cal_from_date;
//    Date cal_to_date;
//    String txt_total_days;
    String cal_to_date_min;
    String cal_to_date_max;
    String cal_detail_from_date_min;
    String cal_detail_from_date_max;
    String cal_detail_to_date_min;
    String cal_detail_to_date_max;

    // <editor-fold defaultstate="collapsed" desc="getter/setter">
//    public String getTxt_total_days() {
//        return txt_total_days;
//    }
//
//    public void setTxt_total_days(String txt_total_days) {
//        this.txt_total_days = txt_total_days;
//    }
    public String getCal_to_date_max() {
        return cal_to_date_max;
    }

    public void setCal_to_date_max(String cal_to_date_max) {
        this.cal_to_date_max = cal_to_date_max;
    }

    public String getCal_to_date_min() {
        return cal_to_date_min;
    }

    public void setCal_to_date_min(String cal_to_date_min) {
        this.cal_to_date_min = cal_to_date_min;
    }

    public String getCal_detail_from_date_max() {
        return cal_detail_from_date_max;
    }

    public void setCal_detail_from_date_max(String cal_detail_from_date_max) {
        this.cal_detail_from_date_max = cal_detail_from_date_max;
    }

    public String getCal_detail_from_date_min() {
        return cal_detail_from_date_min;
    }

    public void setCal_detail_from_date_min(String cal_detail_from_date_min) {
        this.cal_detail_from_date_min = cal_detail_from_date_min;
    }

    public String getCal_detail_to_date_max() {
        return cal_detail_to_date_max;
    }

    public void setCal_detail_to_date_max(String cal_detail_to_date_max) {
        this.cal_detail_to_date_max = cal_detail_to_date_max;
    }

    public String getCal_detail_to_date_min() {
        return cal_detail_to_date_min;
    }

    public void setCal_detail_to_date_min(String cal_detail_to_date_min) {
        this.cal_detail_to_date_min = cal_detail_to_date_min;
    }
//    public Date getCal_from_date() {
//        return cal_from_date;
//    }
//
//    public void setCal_from_date(Date cal_from_date) {
//        this.cal_from_date = cal_from_date;
//    }
//
//    public Date getCal_to_date() {
//        return cal_to_date;
//    }
//
//    public void setCal_to_date(Date cal_to_date) {
//        this.cal_to_date = cal_to_date;
//    }
    // </editor-fold>

    public void cal_detail_date_valuechange(SelectEvent event) {
        if (event.getComponent().getId().equalsIgnoreCase("cal_detail_from_date")) {
            cal_detail_from_date_min = DateTimeUtility.ChangeDateFormat((Date) event.getObject(), "dd-MMM-yyyy hh:mm");
            cal_detail_to_date_min = DateTimeUtility.ChangeDateFormat((Date) event.getObject(), "dd-MMM-yyyy hh:mm");
        }
    }
    // </editor-fold> 
    // <editor-fold defaultstate="collapsed" desc="Place Attributes">
    List<FtasTrainingScheduleDtlEntry> tbl_training_schedule;
    String txt_training_desc;
    Date cal_detail_from_date;
    Date cal_detail_to_date;

    public Date getCal_detail_to_date() {
        return cal_detail_to_date;
    }

    public void setCal_detail_to_date(Date cal_detail_to_date) {
        this.cal_detail_to_date = cal_detail_to_date;
    }

    // <editor-fold defaultstate="collapsed" desc="getter/setter">
    public List<FtasTrainingScheduleDtlEntry> getTbl_training_schedule() {
        return tbl_training_schedule;
    }

    public void setTbl_training_schedule(List<FtasTrainingScheduleDtlEntry> tbl_training_schedule) {
        this.tbl_training_schedule = tbl_training_schedule;
    }

    public Date getCal_detail_from_date() {
        return cal_detail_from_date;
    }

    public void setCal_detail_from_date(Date cal_detail_from_date) {
        this.cal_detail_from_date = cal_detail_from_date;
    }

    public String getTxt_training_desc() {
        return txt_training_desc;
    }

    public void setTxt_training_desc(String txt_training_desc) {
        this.txt_training_desc = txt_training_desc;
    }
    // </editor-fold>
    // </editor-fold>
    // </editor-fold>
    // Dropdown Lists...
    // <editor-fold defaultstate="collapsed" desc="ddl_category">
    String ddl_category;
    List<SelectItem> ddl_category_options;

    public String getDdl_category() {
        return ddl_category;
    }

    public void setDdl_category(String ddl_category) {
        this.ddl_category = ddl_category;
    }

    public List<SelectItem> getDdl_category_options() {
        return ddl_category_options;
    }

    public void setDdl_category_options(List<SelectItem> ddl_category_options) {
        this.ddl_category_options = ddl_category_options;
    }

    public void setDdl_category_options() {
        ddl_category_options = new ArrayList<SelectItem>();
        List<FtasCategoryMst> lst_cat_option = ftasCategoryMstFacade.find_category("ANCT");
        if (lst_cat_option.isEmpty()) {
            ddl_category_options.add(new SelectItem(null, "None"));
        } else {
            ddl_category_options.add(new SelectItem(null, "---Select One---"));
            Integer n = lst_cat_option.size();
            for (int i = 0; i < n; i++) {
                ddl_category_options.add(new SelectItem(lst_cat_option.get(i).getCatmSrgKey(), lst_cat_option.get(i).getCatValueDesc()));
            }
        }
        ddl_category = ddl_category_options.get(0).toString();
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ddl_announce_type">
    String ddl_announce_type;
    List<SelectItem> ddl_announce_type_options;

    public String getDdl_announce_type() {
        return ddl_announce_type;
    }

    public void setDdl_announce_type(String ddl_announce_type) {
        this.ddl_announce_type = ddl_announce_type;
    }

    public List<SelectItem> getDdl_announce_type_options() {
        return ddl_announce_type_options;
    }

    public void setDdl_announce_type_options(List<SelectItem> ddl_announce_type_options) {
        this.ddl_announce_type_options = ddl_announce_type_options;
    }

    public void setDdl_announce_type() {
        ddl_announce_type_options = new ArrayList<SelectItem>();
        List<FtasCategoryMst> lst_cat_option = ftasCategoryMstFacade.find_category("ANNO");
        if (lst_cat_option.isEmpty()) {
            ddl_announce_type_options.add(new SelectItem(null, "None"));
        } else {
            ddl_announce_type_options.add(new SelectItem(null, "---Select One---"));
            Integer n = lst_cat_option.size();
            for (int i = 0; i < n; i++) {
                ddl_announce_type_options.add(new SelectItem(lst_cat_option.get(i).getCatmSrgKey(), lst_cat_option.get(i).getCatValueDesc()));
            }
        }
        ddl_announce_type = ddl_announce_type_options.get(0).toString();

    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ddl_training_salutation">
    String ddl_training_salutation;
    List<SelectItem> ddl_training_salutation_options;

    public String getDdl_training_salutation() {
        return ddl_training_salutation;
    }

    public void setDdl_training_salutation(String ddl_training_salutation) {
        this.ddl_training_salutation = ddl_training_salutation;
    }

    public List<SelectItem> getDdl_training_salutation_options() {
        return ddl_training_salutation_options;
    }

    public void setDdl_training_salutation_options(List<SelectItem> ddl_training_salutation_options) {
        this.ddl_training_salutation_options = ddl_training_salutation_options;
    }

    public void setDdl_training_salutation_options() {
        ddl_training_salutation_options = new ArrayList<SelectItem>();
        List<FtasCategoryMst> lst_cat_option = ftasCategoryMstFacade.find_category("SLTN");
        if (lst_cat_option.isEmpty()) {
            ddl_training_salutation_options.add(new SelectItem("None"));
        } else {
            Integer n = lst_cat_option.size();
            for (int i = 0; i < n; i++) {
                ddl_training_salutation_options.add(new SelectItem(lst_cat_option.get(i).getCatmSrgKey(), lst_cat_option.get(i).getCatValueDesc()));
            }
        }
        ddl_training_salutation = ddl_training_salutation_options.get(0).getValue().toString();
    }
    // </editor-fold>   
    // <editor-fold defaultstate="collapsed" desc="org picklist">
    DualListModel<String> picklst_org = new DualListModel<String>();

    public DualListModel<String> getPicklst_org() {
        return picklst_org;
    }

    public void setPicklst_org(DualListModel<String> picklst_org) {
        this.picklst_org = picklst_org;
    }

    public void setPicklst_org() {
        if (picklst_org.getSource().isEmpty()) {
            try {
                List<SystOrgUnitMst> lst_OrgUnitMsts = systOrgUnitMstFacade.findAll(globalData.getWorking_ou(), false, false, false, false, true, null, null);;
                Integer n = lst_OrgUnitMsts.size();
                for (int i = 0; i < n; i++) {
                    orglist_Source.add(lst_OrgUnitMsts.get(i).getOumUnitSrno() + " - " + lst_OrgUnitMsts.get(i).getOumName());
                }
            } catch (Exception e) {
            }
            org_list = new DualListModel<String>(orglist_Source, orglist_Target);
            picklst_org = org_list;
        }
    }
    List<String> orglist_Source = new ArrayList<String>();
    List<String> orglist_Target = new ArrayList<String>();
    DualListModel<String> org_list;

    public DualListModel<String> getOrg_list() {
        return org_list;
    }

    public void setOrg_list(DualListModel<String> org_list) {
        this.org_list = org_list;
    }// </editor-fold>

    // Functions...
    // <editor-fold defaultstate="collapsed" desc="setDays">
    public int setDays(Date p_fromDate, Date p_toDate) {
        Date d1 = p_fromDate;
        java.util.Calendar temp_cal = java.util.Calendar.getInstance();
        temp_cal.set(d1.getYear(), d1.getMonth(), d1.getDate());
        d1 = temp_cal.getTime();
        Date d2 = p_toDate;
        temp_cal.set(d2.getYear(), d2.getMonth(), d2.getDate());
        d2 = temp_cal.getTime();
        double v_diff_limit;
        float temp_newdiff;
        v_diff_limit = d2.getTime() - d1.getTime();
        temp_newdiff = (int) (v_diff_limit / 86400000);
        temp_newdiff += 1;
        return (int) temp_newdiff;
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="txt announce by values">
    // <editor-fold defaultstate="collapsed" desc="binding attrinutes">
    String txt_announce_by;
    String hdn_valid_user = new String();
    String lbl_username;
    String lbl_empnm_err;

    // <editor-fold defaultstate="collapsed" desc="getter/setter">
    public String getHdn_valid_user() {
        return hdn_valid_user;
    }

    public void setHdn_valid_user(String hdn_valid_user) {
        this.hdn_valid_user = hdn_valid_user;
    }

    public String getLbl_empnm_err() {
        return lbl_empnm_err;
    }

    public void setLbl_empnm_err(String lbl_empnm_err) {
        this.lbl_empnm_err = lbl_empnm_err;
    }

    public String getLbl_username() {
        return lbl_username;
    }

    public void setLbl_username(String lbl_username) {
        this.lbl_username = lbl_username;
    }

    public String getTxt_announce_by() {
        return txt_announce_by;
    }

    public void setTxt_announce_by(String txt_announce_by) {
        this.txt_announce_by = txt_announce_by;
    }// </editor-fold>
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="txt_announce_by_valuechenge event">

    public void txt_announce_by_processValueChange(ValueChangeEvent event) {
        lbl_username = "";
        lbl_empnm_err = "";
        isValidUser = false;
        if (event.getNewValue() != null) {
            if (!event.getNewValue().toString().trim().equals("")) {
//                FhrdEmpmst entEmpmst1 = getEmployeeName(Integer.valueOf(event.getNewValue().toString()));
                FhrdEmpmst entEmpmst1 = fhrdEmpmstFacade.find(Integer.valueOf(event.getNewValue().toString()));
                if (entEmpmst1 != null) {
                    lbl_username = entEmpmst1.getUserName();
                    isValidUser = true;
                } else {
                    lbl_username = "";
                    lbl_empnm_err = "User Does Not Exist";
                }
            }
        }
    }// </editor-fold>
    // </editor-fold>    
    // Btn Action Events...
    //<editor-fold defaultstate="collapsed" desc="footer attributes">
    String f_from_date;
    String f_to_date;
    String f_total_days;

    public String getF_from_date() {
        return f_from_date;
    }

    public void setF_from_date(String f_from_date) {
        this.f_from_date = f_from_date;
    }

    public String getF_to_date() {
        return f_to_date;
    }

    public void setF_to_date(String f_to_date) {
        this.f_to_date = f_to_date;
    }

    public String getF_total_days() {
        return f_total_days;
    }

    public void setF_total_days(String f_total_days) {
        this.f_total_days = f_total_days;

    }
    //</editor-fold>
    //// <editor-fold defaultstate="collapsed" desc="btn add to list">

    public void btn_add_to_list_action(ActionEvent event) {
        FtasTrainingScheduleDtlEntry ent_TourTraining1 = new FtasTrainingScheduleDtlEntry();
        FtasTrainingScheduleDtl entDtl = new FtasTrainingScheduleDtl();
        Date v_from;
        Date v_to;
        if (tbl_training_schedule == null) {
            tbl_training_schedule = new ArrayList<FtasTrainingScheduleDtlEntry>();
            f_from_date = DateTimeUtility.ChangeDateFormat(cal_detail_from_date, "dd-MMM-yyyy hh:mm");

        } else {
            if (tbl_training_schedule.size() > 0) {
                tbl_training_schedule.get(tbl_training_schedule.size() - 1).setLastRow(false);
            }
        }

        entDtl.setTrsdSrgKey(new BigDecimal(tbl_training_schedule.size()));
        entDtl.setDescription(txt_training_desc.toUpperCase());
        entDtl.setFromDatetime(cal_detail_from_date);
        entDtl.setToDatetime(cal_detail_to_date);
        f_to_date = DateTimeUtility.ChangeDateFormat(cal_detail_to_date, "dd-MMM-yyyy hh:mm");
        v_to = cal_detail_to_date;
        ent_TourTraining1.setEntFtasTrainingScheduleDtl(entDtl);
        ent_TourTraining1.setLastRow(true);
        tbl_training_schedule.add(ent_TourTraining1);
        v_from = tbl_training_schedule.get(0).getEntFtasTrainingScheduleDtl().getFromDatetime();
        Integer v_total_days = setDays(v_from, v_to);
        f_total_days = String.valueOf(v_total_days);
        cal_detail_from_date = null;
        cal_detail_to_date = null;
        txt_training_desc = "";
    }

    // </editor-fold>
    FtasTrainingScheduleDtlEntry ent_TourTraining;

    public FtasTrainingScheduleDtlEntry getEnt_TourTraining() {
        return ent_TourTraining;
    }

    public void setEnt_TourTraining(FtasTrainingScheduleDtlEntry ent_TourTraining) {
        tbl_training_schedule.remove(ent_TourTraining);
        this.ent_TourTraining = ent_TourTraining;
    }
    
    public void btn_schedule_delete_action(ActionEvent event){
       
//        setTbl_training_schedule(tbl_training_schedule); 
        
    }
    
    // <editor-fold defaultstate="collapsed" desc="btn_save_action">
    public void btn_save_action(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        FhrdEmpmst ent_Empmst = new FhrdEmpmst(globalData.getUser_id());
        SystOrgUnitMst ent_OrgUnitMst = new SystOrgUnitMst(globalData.getWorking_ou());
        FtasTrainingMst entFtasTrainingMst = new FtasTrainingMst();
        Integer v_cal_year = DateTimeUtility.stringToDate(f_from_date, "dd-MMM-yyyy hh:mm").getYear() + 1900;
        BigDecimal v_trann_srgkey = getGlobalSettings().getUniqueSrno(v_cal_year, "FTAS_TRAINING_MST");
        try {
            entFtasTrainingMst.setTrannSrgKey(v_trann_srgkey);
            FhrdEmpmst ent_em_ann = new FhrdEmpmst(Integer.valueOf(txt_announce_by));
            entFtasTrainingMst.setAnnounceBy(ent_em_ann);
            entFtasTrainingMst.setAnnouncementFlag('Y');
            entFtasTrainingMst.setAnnouncementType(new FtasCategoryMst(new BigDecimal(ddl_announce_type)));
            entFtasTrainingMst.setCategoryType(new FtasCategoryMst(new BigDecimal(ddl_category)));
            entFtasTrainingMst.setSubject(txt_subject.toUpperCase());
            entFtasTrainingMst.setCoOrdinatorSalut(new FtasCategoryMst(new BigDecimal(ddl_training_salutation)));
            entFtasTrainingMst.setCoOrdinatorFName(txt_training_co_first_name.toUpperCase());
            entFtasTrainingMst.setCoOrdinatorLName(txt_training_co_last_name.toUpperCase());
            entFtasTrainingMst.setCoOrdinatorContact(txt_training_announcement_co_contact_no);
            entFtasTrainingMst.setFromDate(DateTimeUtility.stringToDate(f_from_date, "dd-MMM-yyyy hh:mm"));
            entFtasTrainingMst.setToDate(DateTimeUtility.stringToDate(f_to_date, "dd-MMM-yyyy hh:mm"));
            entFtasTrainingMst.setTotalDays(new BigDecimal(f_total_days));

            entFtasTrainingMst.setInstituteAddress(txt_institute_address.toUpperCase());
            entFtasTrainingMst.setInstituteCity(txt_institute_city.toUpperCase());
            entFtasTrainingMst.setInstituteState(txt_institute_state.toUpperCase());
            entFtasTrainingMst.setInstituteCountry(txt_institute_country.toUpperCase());
            entFtasTrainingMst.setInstituteContact(txt_institute_contacts.toUpperCase());
            entFtasTrainingMst.setInstituteName(txt_institute_name.toUpperCase());
            entFtasTrainingMst.setInstitutePincode(txt_institute_pincode);
            entFtasTrainingMst.setTuitionFees(Long.valueOf(txt_tuition_fees));
            entFtasTrainingMst.setTuitionLodgingFees(Long.valueOf(txt_tuition_lodging));
            entFtasTrainingMst.setTuitionLodgingBoardingFees(Long.valueOf(txt_tuition_lodging_boarding));
            if (!txt_other_fees_desc.isEmpty()) {
                entFtasTrainingMst.setOtherFeesDesc(txt_other_fees_desc);
                entFtasTrainingMst.setOtherFeesAmount(Long.valueOf(txt_other_fees_amount));
            }

            entFtasTrainingMst.setVenueAddress(txt_venue_address.toUpperCase());
            entFtasTrainingMst.setVenueCity(txt_venue_city.toUpperCase());
            entFtasTrainingMst.setVenueContact(txt_venue_contacts.toUpperCase());
            entFtasTrainingMst.setVenueCountry(txt_venue_country.toUpperCase());
            entFtasTrainingMst.setVenuePincode(txt_venue_pincode);
            entFtasTrainingMst.setVenueState(txt_venue_state.toUpperCase());

            entFtasTrainingMst.setBenefits(txt_benefits.toUpperCase());
            entFtasTrainingMst.setEligibility(txt_eligibility.toUpperCase());
            entFtasTrainingMst.setObjective(txt_objective.toUpperCase());
            entFtasTrainingMst.setTopicsIncluded(txt_topics.toUpperCase());
            entFtasTrainingMst.setCreateby(ent_Empmst);
            entFtasTrainingMst.setCreatedt(new Date());
            entFtasTrainingMst.setOumUnitSrno(globalData.getWorking_ou());
            entFtasTrainingMst.setTranYear(Short.valueOf(v_cal_year.toString()));
            
            // <editor-fold defaultstate="collapsed" desc="FtasTrainingOrgDtl">
            DualListModel<String> lst_org = picklst_org;
            int v_orgtarget_length = lst_org.getTarget().size();
            List<FtasTrainingOrgUnitDtl> lst_FtasTrainingOrgDtls = new ArrayList<FtasTrainingOrgUnitDtl>();
            for (int i = 0; i < v_orgtarget_length; i++) {
                FtasTrainingOrgUnitDtl ent_ftasOrgDtl = new FtasTrainingOrgUnitDtl();
                BigDecimal v_troud_srgkey = getGlobalSettings().getUniqueSrno(v_cal_year, "FTAS_TRAINING_ORG_UNIT_DTL");
                String[] split_org_list = lst_org.getTarget().get(i).toString().split("-");
                String v_org_srno = split_org_list[0].trim();
                int org_srno_int = Integer.valueOf(v_org_srno);
                ent_ftasOrgDtl.setTroudSrgKey(v_troud_srgkey);
                ent_ftasOrgDtl.setTrannSrgKey(entFtasTrainingMst);
                ent_ftasOrgDtl.setSubSrno(i + 1);
                ent_ftasOrgDtl.setCreateby(ent_Empmst);
                ent_ftasOrgDtl.setOumUnitSrno(ent_OrgUnitMst);
                ent_ftasOrgDtl.setCreatedt(new Date());
                ent_ftasOrgDtl.setSystOrgUnitSrno(org_srno_int);
                ent_ftasOrgDtl.setTranYear(Short.valueOf(v_cal_year.toString()));
                lst_FtasTrainingOrgDtls.add(ent_ftasOrgDtl);
            }// </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="FtasScheduleDtl">
            List<FtasTrainingScheduleDtl> lst_FtasScheduleDtls = new ArrayList<FtasTrainingScheduleDtl>();
            List<FtasTrainingScheduleDtlEntry> lst_FtasScheduleDtl_temp = tbl_training_schedule;
            for (int i = 0; i < lst_FtasScheduleDtl_temp.size(); i++) {
                FtasTrainingScheduleDtl entFtasScheduleDtl = new FtasTrainingScheduleDtl();
                BigDecimal v_trsd_srgkey = getGlobalSettings().getUniqueSrno(v_cal_year, "FTAS_TRAINING_SCHEDULE_DTL");
                entFtasScheduleDtl.setTrsdSrgKey(v_trsd_srgkey);
//                entFtasScheduleDtl.setTrannSrgKey(entFtasTrainingMst);
                entFtasScheduleDtl.setSubSrno(i + 1);
                entFtasScheduleDtl.setCreateby(ent_Empmst);
                entFtasScheduleDtl.setCreatedt(new Date());
                entFtasScheduleDtl.setDescription(lst_FtasScheduleDtl_temp.get(i).getEntFtasTrainingScheduleDtl().getDescription());
                entFtasScheduleDtl.setFromDatetime(lst_FtasScheduleDtl_temp.get(i).getEntFtasTrainingScheduleDtl().getFromDatetime());
                entFtasScheduleDtl.setToDatetime(lst_FtasScheduleDtl_temp.get(i).getEntFtasTrainingScheduleDtl().getToDatetime());
                entFtasScheduleDtl.setOumUnitSrno(ent_OrgUnitMst);
                entFtasScheduleDtl.setTranYear(Short.valueOf(v_cal_year.toString()));
                lst_FtasScheduleDtls.add(entFtasScheduleDtl);
            }// </editor-fold>
            entFtasTrainingMst.setFtasTrainingOrgUnitDtlCollection(lst_FtasTrainingOrgDtls);
//            entFtasTrainingMst.setFtasTrainingScheduleDtlCollection(lst_FtasScheduleDtls);
            try {
                ftasTrainingMstFacade.create(entFtasTrainingMst);
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully."));
                resetForm();
            } catch (EJBException e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "eRecord not saved.Error in Create..."));
            } catch (ConstraintViolationException e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "cRecord not saved.Error in Create..."));
            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "sRecord not saved.Error in Create..."));
            }
        } catch (Exception ex) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Record not saved.Check All the Entries..."));
        }
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="btn_reset_action">

    public void btn_reset_action(ActionEvent event) {
        resetForm();
    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetForm()"> 

    public void resetForm() {
        cal_detail_from_date = null;
        cal_detail_to_date = null;
        txt_subject = null;
        txt_announce_by = null;
        txt_training_co_first_name = null;
        txt_training_co_last_name = null;
        txt_training_announcement_co_contact_no = null;
        txt_announce_by = null;
        txt_venue_name = null;
        txt_tuition_fees = "0";
        txt_tuition_lodging = "0";
        txt_tuition_lodging_boarding = "0";
        txt_other_fees_amount = "0";
        txt_institute_name = null;
        txt_institute_address = null;
        txt_institute_city = null;
        txt_institute_pincode = null;
        txt_institute_state = null;
        txt_institute_country = null;
        txt_institute_contacts = null;
        txt_venue_name = null;
        txt_venue_city = null;
        txt_venue_pincode = null;
        txt_venue_state = null;
        txt_venue_country = null;
        txt_venue_contacts = null;
        txt_objective = null;
        txt_topics = null;
        txt_benefits = null;
        txt_eligibility = null;
        txt_other_fees_desc = null;
        txt_training_desc = null;
        txt_announce_by = null;
        lbl_username = null;
        lbl_empnm_err = null;
        tbl_training_schedule = null;
        ddl_announce_type = null;
        ddl_category = null;
        ddl_training_salutation = "mr";
        setPicklst_org();
        orglist_Target.clear();
    }
    //</editor-fold>
    //get Global Settings...
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setDdl_announce_type();
        setDdl_category_options();
        setDdl_training_salutation_options();
        setPicklst_org();
    }
    //</editor-fold>
}
