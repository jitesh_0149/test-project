package org.fes.pis.jsf.pages.pis.masters;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.fes.pis.custom_entities.PIS_GlobalData;

/**
 *
 * @author anu
 */
@ManagedBean
@ViewScoped
public class admin_conf implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;
    
    public PIS_GlobalData getGlobalData() {
        return globalData;
    }
    
    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    String simple_text = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("jitesh");

    public String getSimple_text() {
        
        
        return simple_text;
    }

    public void setSimple_text(String simple_text) {
        this.simple_text = simple_text;
    }
   

    public admin_conf() {
        
    }
}
