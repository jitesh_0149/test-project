/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.view.tas;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.AttendanceLockDataList;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.FtasAbsentees;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FtasAbsenteesFacadeLocal;
import org.fes.pis.sessions.FtasLockDtlFacadeLocal;
import org.joda.time.DateTime;
import org.primefaces.context.RequestContext;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Pending_Attendance")
@ViewScoped
public class PIS_Pending_Attendace implements Serializable {

    @EJB
    private FtasAbsenteesFacadeLocal ftasAbsenteesFacade;
    @EJB
    private FtasLockDtlFacadeLocal ftasLockDtlFacade;

    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    
    boolean isValidUser = false;
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ddl_month">
    String ddl_month;
    List<SelectItem> ddl_month_options;

    public List<SelectItem> getDdl_month_options() {
        return ddl_month_options;
    }

    public void setDdl_month_options(List<SelectItem> ddl_month_options) {
        this.ddl_month_options = ddl_month_options;
    }

    public String getDdl_month() {
        return ddl_month;
    }

    public void setDdl_month(String ddl_month) {
        this.ddl_month = ddl_month;
    }

    public void ddl_month_changed(ValueChangeEvent event) {
        ddl_month = event.getNewValue() == null ? null : event.getNewValue().toString();
        setTbl_emp_list();

    }

    private void setDdl_month() {
        ddl_month_options = new ArrayList<SelectItem>();
        List<SelectItem> lst = new ArrayList<SelectItem>();
        if (ddl_year == null) {
            ddl_month_options.add(new SelectItem(null, "--- Not Available ---"));
            ddl_month = null;
        } else {
            Date v_year_start_date = new Date(Integer.valueOf(ddl_year) - 1900, 0, 1);
            Date v_year_end_date = new Date(Integer.valueOf(ddl_year) - 1900, 11, 31);
            Date v_min_lock_date = min_lock_date;
            Date v_max_lock_date = max_lock_date;
            while (v_min_lock_date.before(v_max_lock_date) || v_min_lock_date.equals(v_max_lock_date)) {
                if (!(v_min_lock_date.before(v_year_start_date))
                        && !(v_min_lock_date.after(v_year_end_date))) {
                    lst.add(new SelectItem(DateTimeUtility.ChangeDateFormat(v_min_lock_date, "MM").toString(), DateTimeUtility.ChangeDateFormat(v_min_lock_date, "MMM")));
                }
                v_min_lock_date = new DateTime(v_min_lock_date).plusMonths(1).toDate();
            }
            int n = lst.size();
            for (int j = n - 1; j >= 0; --j) {
                ddl_month_options.add(lst.get(j));
            }
            ddl_month = ddl_month_options.get(0).getValue().toString();
            setTbl_emp_list();


        }
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="min max lock date">
    Date min_lock_date;
    Date max_lock_date;

    private void setMinMaxDate() {
        Date v_date = ftasLockDtlFacade.findMinLockDate(globalData.getWorking_ou().toString());
        min_lock_date = new DateTime(v_date).plusMonths(1).toDate();
        min_lock_date = new Date(min_lock_date.getYear(), min_lock_date.getMonth(), 1);
        max_lock_date = new Date(new Date().getYear(), new Date().getMonth(), 1);
        setDdl_year();
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ddl year"> 
    String ddl_year;
    List<SelectItem> ddl_year_options;

    public String getDdl_year() {
        return ddl_year;
    }

    public void setDdl_year(String ddl_year) {
        this.ddl_year = ddl_year;
    }

    public List<SelectItem> getDdl_year_options() {
        return ddl_year_options;
    }

    public void setDdl_year_options(List<SelectItem> ddl_year_options) {
        this.ddl_year_options = ddl_year_options;
    }

    public void ddl_year_changed(ValueChangeEvent event) {
        ddl_year = event.getNewValue() == null ? null : event.getNewValue().toString();
        setDdl_month();
    }

    private void setDdl_year() {
        Integer from_year = min_lock_date.getYear() + 1900;
        Integer to_year = max_lock_date.getYear() + 1900;
        ddl_year_options = new ArrayList<SelectItem>();
        while (to_year >= from_year) {
            ddl_year_options.add(new SelectItem(to_year));
            to_year--;
        }
        if (ddl_year_options.isEmpty()) {
            ddl_year = null;
        } else {
            ddl_year = String.valueOf((new Date()).getYear() + 1900);
        }
        setDdl_month();
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_emp_list"> 
    List<AttendanceLockDataList> tbl_emp_list;
    List<AttendanceLockDataList> tbl_emp_list_filtered;

    public List<AttendanceLockDataList> getTbl_emp_list() {
        return tbl_emp_list;
    }

    public List<AttendanceLockDataList> getTbl_emp_list_filtered() {
        return tbl_emp_list_filtered;
    }

    public void setTbl_emp_list_filtered(List<AttendanceLockDataList> tbl_emp_list_filtered) {
        this.tbl_emp_list_filtered = tbl_emp_list_filtered;
    }

    public void setTbl_emp_list(List<AttendanceLockDataList> tbl_emp_list) {
        this.tbl_emp_list = tbl_emp_list;
    }

    public void setTbl_emp_list() {
//        System.out.println(" in set table");
        if (ddl_month != null && ddl_year.isEmpty() == false) {
//            System.out.println("in if" + ddl_month + " " + ddl_year);
            int v_year = Integer.parseInt(ddl_year);
            Calendar c2 = Calendar.getInstance();
            c2.set(v_year, Integer.parseInt(ddl_month) - 1, 5);
            String v_to_date = DateTimeUtility.ChangeDateFormat(c2.getTime(), null);
            Boolean sysetemUser_Fhrd = getPIS_GlobalSettings().getSysetemUser_Fhrd();
//            System.out.println("syst " + sysetemUser_Fhrd);
            if (sysetemUser_Fhrd) {
//                System.out.println("globalData.getOrganisation().getOumUnitSrno() " + globalData.getOrganisation().getOumUnitSrno());
                tbl_emp_list = getLst_att_lock(globalData.getOrganisation().getOumUnitSrno(), v_to_date, "P");
            } else {
                tbl_emp_list = getLst_att_lock(globalData.getWorking_ou(), v_to_date, "P");
            }
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="getLst_att_lock"> 

    public List<AttendanceLockDataList> getLst_att_lock(Integer p_oum_unit_srno, String p_to_date, String p_status) {
        List<AttendanceLockDataList> lst_emp = new ArrayList<AttendanceLockDataList>();
        FacesContext context = FacesContext.getCurrentInstance();
        String CMD_EMP_LIST = "SELECT * FROM TABLE(FTAS_PACK.GET_USER_FOR_LOCK(" + p_oum_unit_srno + ",'" + p_to_date + "','" + p_status + "'))";
//        System.out.println("CMD " + CMD_EMP_LIST);
        Connection cn = null;
        try {
            Statement st = null;
            try {
                ResultSet res = null;
                try {
                    cn = getGlobalUtilities().getjdbcFES_PROJECT_Oracle().getConnection();
                    int i = 0;
                    st = cn.createStatement();
                    res = st.executeQuery(CMD_EMP_LIST);
                    while (res.next()) {
                        AttendanceLockDataList ent_Atten1 = new AttendanceLockDataList();
                        ent_Atten1.setRowid(i);
                        ent_Atten1.setUser_id(res.getInt("USER_ID"));
                        ent_Atten1.setUser_name(res.getString("USER_NAME"));
                        ent_Atten1.setFrom_date(res.getDate("FROM_DATE"));
                        ent_Atten1.setTo_date(res.getDate("TO_DATE"));
                        ent_Atten1.setLeaves(res.getBigDecimal("LEAVES"));
                        ent_Atten1.setPending_days(res.getBigDecimal("PENDING_DAYS"));
                        ent_Atten1.setOum_unit_srno(res.getInt("OUM_UNIT_SRNO"));
                        ent_Atten1.setOum_name(res.getString("OU_NAME"));
                        ent_Atten1.setOther_reason(res.getString("OTHER_REASON"));
                        lst_emp.add(i, ent_Atten1);
                        i++;
                    }
                } catch (Exception e) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Can't Find Attendance List", "Some unexpected error occurred during finding missing attendance"));
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "getLst_att_lock", null, e);
                } finally {
                    res.close();
                }
            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Can't Find Attendance List", "Some unexpected error occurred during finding missing attendance"));
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "getLst_att_lock", null, e);
            } finally {
                st.close();
            }
        } catch (Exception ex) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Can't Find Attendance List", "Some unexpected error occurred during finding missing attendance"));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "getLst_missing_att()", null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                    }
                } catch (Exception e) {
                }
            }
        }
        return lst_emp;
    }

    public List<FtasAbsentees> getTbl_leave_detail() {
        return tbl_leave_detail;
    }

    public void setTbl_leave_detail(List<FtasAbsentees> tbl_leave_detail) {
        this.tbl_leave_detail = tbl_leave_detail;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="View Detail Methods">
    //<editor-fold defaultstate="collapsed" desc="Leave List Settings">
    List<FtasAbsentees> tbl_leave_detail;
    AttendanceLockDataList ent_view_detail;

    public AttendanceLockDataList getEnt_view_detail() {
        return ent_view_detail;
    }

    public void setEnt_view_detail(AttendanceLockDataList ent_view_detail) {
        //tbl_leave_detail = ftasAbsenteesFacade.findAll(globalData.getWorking_ou(), ent_view_detail.getUser_id(), false, false, ent_view_detail.getFrom_date(), ent_view_detail.getTo_date());
        tbl_leave_detail = ftasAbsenteesFacade.findAll(ent_view_detail.getOum_unit_srno(), ent_view_detail.getUser_id(), false, false, ent_view_detail.getFrom_date(), ent_view_detail.getTo_date(), null, false, null, false, null);
        RequestContext.getCurrentInstance().execute("pw_dlg_view_detail.show();");
        this.ent_view_detail = ent_view_detail;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="viewLeaveDetail">

    public void viewLeaveDetail(FtasAbsentees selectedLeave) {
        this.selectedLeave = selectedLeave;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="selectedLeave">
    FtasAbsentees selectedLeave;

    public FtasAbsentees getSelectedLeave() {
        return selectedLeave;
    }

    public void setSelectedLeave(FtasAbsentees selectedLeave) {
        this.selectedLeave = selectedLeave;
    }
    //</editor-fold>
    //</editor-fold>   
    //<editor-fold defaultstate="collapsed" desc="btn_view_detail_click"> 

    public void btn_view_detail_click(ActionEvent event) {
    }
    //</editor-fold>       
    //<editor-fold defaultstate="collapsed" desc="init()"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();

        setMinMaxDate();
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings and default methods">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>

    /**
     * Creates a new instance of PIS_Attendance_Lock
     */
    public PIS_Pending_Attendace() {
    }
    // </editor-fold> 
}