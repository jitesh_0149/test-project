/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.pis.emp_related;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.fes.pis.custom_entities.PIS_GlobalData;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Emp_EDrule")
@ViewScoped
public class PIS_Emp_EDrule implements Serializable {
    //<editor-fold defaultstate="collapsed" desc="globalData">

    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="default">

    /**
     * Creates a new instance of PIS_Emp_EDrule
     */
    public PIS_Emp_EDrule() {
    }
    //</editor-fold>

}
