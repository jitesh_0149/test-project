/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.pis.emp_info;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.customJpaController;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.*;
import org.fes.pis.jsf.GlobalResources;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.*;
import org.primefaces.context.RequestContext;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Employee_Contract")
@ViewScoped
public class PIS_Employee_Contract implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="EJB Declarations">
    @EJB
    private FhrdEmpContractMstFacadeLocal fhrdEmpContractMstFacade;
    @EJB
    private FhrdEmpearndednFacadeLocal fhrdEmpearndednFacade;
    @EJB
    private customJpaController customJpaController;
    @EJB
    private FhrdEmpleaveruleFacadeLocal fhrdEmpleaveruleFacade;
    @EJB
    private FhrdEarndednFacadeLocal fhrdEarndednFacade;
    @EJB
    private FhrdLeaveruleFacadeLocal fhrdLeaveruleFacade;
    @EJB
    private FhrdContractMstFacadeLocal fhrdContractMstFacade;
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    @EJB
    private FhrdEmpleavebalFacadeLocal fhrdEmpleavebalFacade;
    @EJB
    private FhrdDesigmstFacadeLocal fhrdDesigmstFacade;
    Ou_Pack ou_Pack = new Ou_Pack();

    
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="default">
    /**
     * Creates a new instance of PIS_Employee_Contract
     */
    public PIS_Employee_Contract() {
    }
    //</editor-fold>
    ///////////////Employee Table
    //<editor-fold defaultstate="collapsed" desc="tbl_emp_info"> 
    List<FhrdEmpmst> tbl_emp_info;

    public List<FhrdEmpmst> getTbl_emp_info() {
        return tbl_emp_info;
    }

    public void setTbl_emp_info(List<FhrdEmpmst> tbl_emp_info) {
        this.tbl_emp_info = tbl_emp_info;
    }

    public void setTblEmpInfo() {
        tbl_emp_info = fhrdEmpmstFacade.findAllEmployeeForContract(globalData.getWorking_ou(), getPIS_GlobalSettings().getSysetemUser_Fhrd() ? null : globalData.getUser_id());
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_emp_info_filtered"> 
    List<FhrdEmpmst> tbl_emp_info_filtered;

    public List<FhrdEmpmst> getTbl_emp_info_filtered() {
        return tbl_emp_info_filtered;
    }

    public void setTbl_emp_info_filtered(List<FhrdEmpmst> tbl_emp_info_filtered) {
        this.tbl_emp_info_filtered = tbl_emp_info_filtered;
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="ent_empmst_detail"> 
    FhrdEmpmst ent_empmst_detail;

    public FhrdEmpmst getEnt_empmst_detail() {
        return ent_empmst_detail;
    }

    public void setEnt_empmst_detail(FhrdEmpmst ent_empmst_detail) {
        tbl_view_supp_contract = fhrdEmpContractMstFacade.findAllContractEmpwise(ent_empmst_detail.getUserId(), "'M'");
        this.ent_empmst_detail = ent_empmst_detail;
    }
    //</editor-fold>  
    //<editor-fold defaultstate="collapsed" desc="suddenEmployeeContract"> 

    public void suddenEmployeeContract(FhrdEmpContractMst fhrdEmpContractMst) {
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ent_suddenEmployeeContract"> 
    FhrdEmpContractMst ent_suddenEmployeeContract;

    public FhrdEmpContractMst getEnt_suddenEmployeeContract() {
        return ent_suddenEmployeeContract;
    }

    public void setEnt_suddenEmployeeContract(FhrdEmpContractMst ent_suddenEmployeeContract) {
//        cal_sudden_contract_end_date = ent_suddenEmployeeContract.getStartDate();
        cal_sudden_contract_end_date_min = DateTimeUtility.ChangeDateFormat(ent_suddenEmployeeContract.getStartDate(), null);
        cal_sudden_contract_end_date_max = DateTimeUtility.ChangeDateFormat(ent_suddenEmployeeContract.getEndDate(), null);

        cal_transaction_end_date_min = cal_sudden_contract_end_date_min;
        cal_transaction_end_date_max = cal_sudden_contract_end_date_max;

        RequestContext.getCurrentInstance().execute("pw_dlg_sudden_contract.show()");
        this.ent_suddenEmployeeContract = ent_suddenEmployeeContract;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_sudden_contract_end_date"> 
    Date cal_sudden_contract_end_date;
    String cal_sudden_contract_end_date_min;
    String cal_sudden_contract_end_date_max;

    public Date getCal_sudden_contract_end_date() {
        return cal_sudden_contract_end_date;
    }

    public void setCal_sudden_contract_end_date(Date cal_sudden_contract_end_date) {
        this.cal_sudden_contract_end_date = cal_sudden_contract_end_date;
    }

    public String getCal_sudden_contract_end_date_max() {
        return cal_sudden_contract_end_date_max;
    }

    public void setCal_sudden_contract_end_date_max(String cal_sudden_contract_end_date_max) {
        this.cal_sudden_contract_end_date_max = cal_sudden_contract_end_date_max;
    }

    public String getCal_sudden_contract_end_date_min() {
        return cal_sudden_contract_end_date_min;
    }

    public void setCal_sudden_contract_end_date_min(String cal_sudden_contract_end_date_min) {
        this.cal_sudden_contract_end_date_min = cal_sudden_contract_end_date_min;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_transaction_end_date"> 
    Date cal_transaction_end_date;
    String cal_transaction_end_date_min;
    String cal_transaction_end_date_max;

    public Date getCal_transaction_end_date() {
        return cal_transaction_end_date;
    }

    public void setCal_transaction_end_date(Date cal_transaction_end_date) {
        this.cal_transaction_end_date = cal_transaction_end_date;
    }

    public String getCal_transaction_end_date_max() {
        return cal_transaction_end_date_max;
    }

    public void setCal_transaction_end_date_max(String cal_transaction_end_date_max) {
        this.cal_transaction_end_date_max = cal_transaction_end_date_max;
    }

    public String getCal_transaction_end_date_min() {
        return cal_transaction_end_date_min;
    }

    public void setCal_transaction_end_date_min(String cal_transaction_end_date_min) {
        this.cal_transaction_end_date_min = cal_transaction_end_date_min;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="btn_set_sudden_contract_end"> 
    public void btn_set_sudden_contract_end(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if (ent_suddenEmployeeContract.getContractCategory().equals("M")) {
                List<FhrdEmpContractMst> lstContractMsts = fhrdEmpContractMstFacade.findAllContractEmpwiseDatewise(ent_suddenEmployeeContract.getUserId().getUserId(), DateTimeUtility.ChangeDateFormat(ent_suddenEmployeeContract.getStartDate(), null), DateTimeUtility.ChangeDateFormat(ent_suddenEmployeeContract.getEndDate(), null), DateTimeUtility.ChangeDateFormat(cal_sudden_contract_end_date, null));
                int n = lstContractMsts.size();
                for (int i = 0; i < n; i++) {
                    lstContractMsts.get(i).setEndDate(cal_sudden_contract_end_date);
                    lstContractMsts.get(i).setTranEndDate(cal_transaction_end_date);
                    lstContractMsts.get(i).setContSuddenEndFlag("Y");
                    lstContractMsts.get(i).setUpdateby(globalData.getEnt_login_user());
                    lstContractMsts.get(i).setUpdatedt(new Date());
                }
//            System.out.println("before trans");
                boolean isSuccess = customJpaController.setSuddenContractEnd(lstContractMsts, globalData.getUser_id());
//            System.out.println("isSuccess " + isSuccess);
                if (isSuccess) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Contract end Successfully"));
                    cal_sudden_contract_end_date = null;
                    tbl_view_supp_contract = fhrdEmpContractMstFacade.findAllContractEmpwise(ent_suddenEmployeeContract.getUserId().getUserId(), "'M'");

                } else {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occurred while saving the records"));
                }
            } else {
                List<FhrdEmpContractMst> lstContractMsts = new ArrayList<FhrdEmpContractMst>();
                lstContractMsts.add(fhrdEmpContractMstFacade.find(ent_suddenEmployeeContract.getEcmSrgKey()));
                int n = lstContractMsts.size();
                for (int i = 0; i < n; i++) {
                    lstContractMsts.get(i).setEndDate(cal_sudden_contract_end_date);
                    lstContractMsts.get(i).setTranEndDate(cal_transaction_end_date);
                    lstContractMsts.get(i).setContSuddenEndFlag("Y");
                    lstContractMsts.get(i).setUpdateby(globalData.getEnt_login_user());
                    lstContractMsts.get(i).setUpdatedt(new Date());
                }
//            System.out.println("before trans");
                boolean isSuccess = customJpaController.setSuddenContractEnd(lstContractMsts, globalData.getUser_id());
//            System.out.println("isSuccess " + isSuccess);
                if (isSuccess) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Contract end Successfully"));
                    cal_sudden_contract_end_date = null;
                    tbl_view_supp_contract = fhrdEmpContractMstFacade.findAllContractEmpwise(ent_suddenEmployeeContract.getUserId().getUserId(), "'M'");

                } else {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occurred while saving the records"));
                }
            }
        } catch (Exception e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_set_sudden_contract_end", null, null);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occurred while saving the records"));
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ent_set_emp_contract"> 

    public void setEmployeeContract(FhrdEmpmst fhrdEmpmst) {
        ent_empmst_setContract = fhrdEmpmst;
        //BigDecimal v_count_closing = fhrdEmpleavebalFacade.checkEmpLeaveClosingBal(ent_empmst_setContract.getUserId());
        BigDecimal v_count_closing = BigDecimal.ZERO;
        if (v_count_closing.intValue() != 0) {
            RequestContext.getCurrentInstance().execute("SetAndShowDialog('GeneralDialog','Invalid Operation','You can not add new Contract as settlement of previous contract is pending');");
        } else {
            cal_tran_start_date = null;
            List<FhrdEmpContractMst> entContractMst = fhrdEmpContractMstFacade.findLatestContractEmpwise(fhrdEmpmst.getUserId(), "M");
//        cal_contract_start_date_min = DateTimeUtility.ChangeDateFormat(entContractMst.getEndDate(), null);
            if (!entContractMst.isEmpty()) {
                cal_contract_start_date = DateTimeUtility.findNextDateFromDays(entContractMst.get(0).getEndDate(), 1);
//            cal_contract_end_date = DateTimeUtility.findNextDateFromDays(entContractMst.get(0).getEndDate(), 1);
                cal_contract_start_date_min = DateTimeUtility.ChangeDateFormat(DateTimeUtility.findNextDateFromDays(entContractMst.get(0).getEndDate(), 1), null);
                cal_contract_start_date_max = DateTimeUtility.ChangeDateFormat(DateTimeUtility.findNextDateFromDays(entContractMst.get(0).getEndDate(), 1), null);
                cal_tran_start_date = DateTimeUtility.findNextDateFromDays(entContractMst.get(0).getTranEndDate(), 1);
                cal_contract_end_date_min = DateTimeUtility.ChangeDateFormat(DateTimeUtility.findNextDateFromDays(entContractMst.get(0).getEndDate(), 1), null);

            } else {
                cal_contract_start_date = null;
                cal_contract_end_date = null;
                cal_contract_start_date_min = null;
                cal_contract_start_date_max = null;
                cal_contract_end_date_min = null;
            }
            setDdl_current_desig_options();
            txt_user_name = fhrdEmpmst.getUserName();
            resetContractDateChange();
            RequestContext.getCurrentInstance().execute("pw_dlg_set_new_contract.show()");
            RequestContext.getCurrentInstance().execute("$('.desig-yes').hide()");
        }
    }
    //</editor-fold>  
    /////////////// Contract Detail View
    //<editor-fold defaultstate="collapsed" desc="tbl_view_supp_contract"> 
    List<FhrdEmpContractMst> tbl_view_supp_contract;

    public List<FhrdEmpContractMst> getTbl_view_supp_contract() {
        return tbl_view_supp_contract;
    }

    public void setTbl_view_supp_contract(List<FhrdEmpContractMst> tbl_view_supp_contract) {
        this.tbl_view_supp_contract = tbl_view_supp_contract;
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="ent_view_contract_detail"> 
    FhrdEmpContractMst ent_view_contract_detail;

//16-12-2013
    public FhrdEmpContractMst getEnt_view_contract_detail() {
        return ent_view_contract_detail;
    }

    public void setEnt_view_contract_detail(FhrdEmpContractMst ent_view_contract_detail) {
        this.ent_view_contract_detail = ent_view_contract_detail;
        setEnt_view_contract_detail();
    }

    public void setEnt_view_contract_detail() {
        String v_opeining_date = DateTimeUtility.ChangeDateFormat(ent_view_contract_detail.getStartDate(), null);
        String v_closing_date = DateTimeUtility.ChangeDateFormat(ent_view_contract_detail.getEndDate(), null);
        tbl_view_contract = fhrdEmpContractMstFacade.findAllByMainContract(ent_view_contract_detail.getUserId().getUserId(), v_opeining_date, v_closing_date);
        String v_contract_list = null;
        int n = tbl_view_contract.size();

        String v_tran_start_date = null;
        String v_tran_end_date = null;
        for (int i = 0; i < n; i++) {
            if (tbl_view_contract.get(i).getCmSrgKey().getContractCategory().equals("M")) {
                v_tran_start_date = DateTimeUtility.ChangeDateFormat(tbl_view_contract.get(i).getTranStartDate(), null);
                v_tran_end_date = DateTimeUtility.ChangeDateFormat(tbl_view_contract.get(i).getTranEndDate(), null);
            }
            if (i == 0) {
                v_contract_list = tbl_view_contract.get(i).getCmSrgKey().getCmSrgKey().toString();
            } else {
                v_contract_list += "," + tbl_view_contract.get(i).getCmSrgKey().getCmSrgKey().toString();
            }
        }

        tbl_view_leavebal = fhrdEmpleaveruleFacade.getLeaveRuleEmpwise(ent_empmst_detail.getUserId(), ent_empmst_detail.getPostingOumUnitSrno().getOumUnitSrno(), v_contract_list, v_tran_start_date, v_tran_end_date);
        tbl_view_edrule = fhrdEmpearndednFacade.getEDRuleEmpwise(ent_empmst_detail.getUserId(), v_contract_list, ent_empmst_detail.getPostingOumUnitSrno().getOumUnitSrno(), v_tran_start_date, v_tran_end_date);
        setToAddSuddenSupplContract();
        RequestContext.getCurrentInstance().execute("pw_dlg_view_contract_detail.show()");
    }
    //</editor-fold>
    ///
    ///////////////tbl_view_contract
    //<editor-fold defaultstate="collapsed" desc="View Contract">
    //<editor-fold defaultstate="collapsed" desc="lnk_add_sudden_suppl_contract_clicked">

    public void lnk_add_sudden_suppl_contract_clicked(ActionEvent event) {
        setToAddSuddenSupplContract();
        RequestContext.getCurrentInstance().execute("pw_dlg_add_sudden_suppl_contract.show()");
    }

    public void setToAddSuddenSupplContract() {
        String v_contractAltlist = null;
        ddl_add_supplement_contract_options = new ArrayList<SelectItem>();
        List<FhrdEmpContractMst> lstContractMsts = tbl_view_contract;
        for (FhrdEmpContractMst c : lstContractMsts) {
            if (c.getCmSrgKey().getContractCategory().equals("M")) {
                if (v_contractAltlist == null) {
                    v_contractAltlist = c.getCmAltSrno().toString();
                } else {
                    v_contractAltlist += "," + c.getCmAltSrno().toString();
                }
                cal_add_suppl_start_date = c.getStartDate();
                cal_add_suppl_end_date = c.getEndDate();
                cal_add_suppl_start_date_min = DateTimeUtility.ChangeDateFormat(c.getStartDate(), null);
                cal_add_suppl_end_date_min = DateTimeUtility.ChangeDateFormat(c.getStartDate(), null);
                cal_add_suppl_start_date_max = DateTimeUtility.ChangeDateFormat(c.getEndDate(), null);
                cal_add_suppl_end_date_max = DateTimeUtility.ChangeDateFormat(c.getEndDate(), null);
            }
        }
        List<FhrdContractMst> lstFhrdContractMsts = fhrdContractMstFacade.findSupplementContractListOuwise(globalData.getWorking_ou(), v_contractAltlist);
        if (lstFhrdContractMsts.isEmpty()) {
            SelectItem s = new SelectItem(null, "--- Not Available ---");
            ddl_add_supplement_contract_options.add(s);
        } else {
            SelectItem s = new SelectItem(null, "--- Select ---");
            ddl_add_supplement_contract_options.add(s);
            Integer n = lstFhrdContractMsts.size();
            for (int i = 0; i < n; i++) {
                s = new SelectItem(lstFhrdContractMsts.get(i).getCmSrgKey(), lstFhrdContractMsts.get(i).getContractDesc());
                ddl_add_supplement_contract_options.add(s);
            }
        }
        ddl_add_supplement_contract = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_view_contract_detail">
    List<FhrdEmpContractMst> tbl_view_contract;

    public List<FhrdEmpContractMst> getTbl_view_contract() {
        return tbl_view_contract;
    }

    public void setTbl_view_contract(List<FhrdEmpContractMst> tbl_view_contract) {
        this.tbl_view_contract = tbl_view_contract;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_sudden_contract_end_date">
    Date cal_sudden_suppl_end_date;
    String cal_sudden_suppl_end_date_min;
    String cal_sudden_suppl_end_date_max;

    public Date getCal_sudden_suppl_end_date() {
        return cal_sudden_suppl_end_date;
    }

    public void setCal_sudden_suppl_end_date(Date cal_sudden_suppl_end_date) {
        this.cal_sudden_suppl_end_date = cal_sudden_suppl_end_date;
    }

    public String getCal_sudden_suppl_end_date_min() {
        return cal_sudden_suppl_end_date_min;
    }

    public void setCal_sudden_suppl_end_date_min(String cal_sudden_suppl_end_date_min) {
        this.cal_sudden_suppl_end_date_min = cal_sudden_suppl_end_date_min;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_view_leavebal">
    List<FhrdEmpleavebal> tbl_view_leavebal;

    public List<FhrdEmpleavebal> getTbl_view_leavebal() {
        return tbl_view_leavebal;
    }

    public void setTbl_view_leavebal(List<FhrdEmpleavebal> tbl_view_leavebal) {
        this.tbl_view_leavebal = tbl_view_leavebal;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ent_edit_FhrdLeaveEmpleavebal">
    FhrdEmpleavebal ent_edit_FhrdLeaveEmpleavebal;

    public FhrdEmpleavebal getEnt_edit_FhrdLeaveEmpleavebal() {
        return ent_edit_FhrdLeaveEmpleavebal;
    }

    public void setEnt_edit_FhrdLeaveEmpleavebal(FhrdEmpleavebal ent_edit_FhrdLeaveEmpleavebal) {
        this.ent_edit_FhrdLeaveEmpleavebal = ent_edit_FhrdLeaveEmpleavebal;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_edit_leaveRule_clicked">

    public void btn_edit_leaveRule_clicked(FhrdEmpleavebal fhrdEmpleavebal) {
        ent_edit_FhrdLeaveEmpleavebal = fhrdEmpleavebal;
        RequestContext.getCurrentInstance().execute("pw_dlg_setNewEmpLeave.show()");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_edit_leavebal_clicked">

    public void btn_edit_leavebal_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            BigDecimal v_net_balance = (ent_edit_FhrdLeaveEmpleavebal.getOpeningBal().add(ent_edit_FhrdLeaveEmpleavebal.getEarnedBal())).add(ent_edit_FhrdLeaveEmpleavebal.getAddNewLeavebal());
            FhrdEmpleavebal entEmpleavebal = ent_edit_FhrdLeaveEmpleavebal;
            entEmpleavebal.setNetLeaveBalance(v_net_balance);
            entEmpleavebal.setUpdateby(globalData.getEnt_login_user());
            entEmpleavebal.setUpdatedt(new Date());
            fhrdEmpleavebalFacade.edit(entEmpleavebal);
            RequestContext.getCurrentInstance().execute("pw_dlg_setNewEmpLeave.hide()");
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "New leave balance set Successfully"));
        } catch (Exception e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_edit_leavebal_clicked", null, null);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Warning", "Record not saved successfully.Please check all the entries."));
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_view_edrule">
    List<FhrdEmpearndedn> tbl_view_edrule;

    public List<FhrdEmpearndedn> getTbl_view_edrule() {
        return tbl_view_edrule;
    }

    public void setTbl_view_edrule(List<FhrdEmpearndedn> tbl_view_edrule) {
        this.tbl_view_edrule = tbl_view_edrule;
    }
    //<editor-fold defaultstate="collapsed" desc="filter_first_entry_flag">
    public List<SelectItem> filter_first_entry_flag;

    public List<SelectItem> getFilter_first_entry_flag() {
        return filter_first_entry_flag;
    }

    public void setFilter_first_entry_flag(List<SelectItem> filter_first_entry_flag) {
        this.filter_first_entry_flag = filter_first_entry_flag;
    }

    private void setFilter_first_entry_flag() {
        filter_first_entry_flag = new ArrayList<SelectItem>();
        filter_first_entry_flag.add(new SelectItem("", "--- All ---"));
        filter_first_entry_flag.add(new SelectItem("A", "Automatic"));
        filter_first_entry_flag.add(new SelectItem("M", "Manual"));
    }
    //</editor-fold>

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_view_ed_detail_action">
    public void btn_view_ed_detail_action(FhrdEmpearndedn ent) {
        ent_FhrdEmpEarndedn_detail = ent;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ent_FhrdEmpEarndedn_detail">
    FhrdEmpearndedn ent_FhrdEmpEarndedn_detail;

    public FhrdEmpearndedn getEnt_FhrdEmpEarndedn_detail() {
        return ent_FhrdEmpEarndedn_detail;
    }

    public void setEnt_FhrdEmpEarndedn_detail(FhrdEmpearndedn ent_FhrdEmpEarndedn_detail) {
        this.ent_FhrdEmpEarndedn_detail = ent_FhrdEmpEarndedn_detail;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ent_FhrdEmpEarndedn_set">
    FhrdEmpearndedn ent_FhrdEmpEarndedn_set;

    public FhrdEmpearndedn getEnt_FhrdEmpEarndedn_set() {
        return ent_FhrdEmpEarndedn_set;
    }

    public void setEnt_FhrdEmpEarndedn_set(FhrdEmpearndedn ent_FhrdEmpEarndedn_set) {
        //        txt_manual_entry_value = ent_FhrdEmpEarndedn_set.getEedSrgKey().toString();
        RequestContext.getCurrentInstance().execute("pw_dlg_set_manual_entry.show()");
        this.ent_FhrdEmpEarndedn_set = ent_FhrdEmpEarndedn_set;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_manual_entry_value">
    String txt_manual_entry_value;

    public String getTxt_manual_entry_value() {
        return txt_manual_entry_value;
    }

    public void setTxt_manual_entry_value(String txt_manual_entry_value) {
        this.txt_manual_entry_value = txt_manual_entry_value;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_dlg_manual_entry_clicked">

    public void btn_dlg_manual_entry_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            FhrdEmpearndedn entEmpearndedn = ent_FhrdEmpEarndedn_set;
            entEmpearndedn.setFormulaAmtValue(txt_manual_entry_value);
            entEmpearndedn.setUpdateby(globalData.getEnt_login_user());
            entEmpearndedn.setUpdatedt(new Date());
            fhrdEmpearndednFacade.edit(entEmpearndedn);
            RequestContext.getCurrentInstance().execute("pw_dlg_set_manual_entry.hide()");
            txt_manual_entry_value = null;
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Value set Successfully"));
        } catch (Exception e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_dlg_manual_entry_clicked", null, null);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred while saving the record"));
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_supplement_contract">
    String ddl_add_supplement_contract;
    List<SelectItem> ddl_add_supplement_contract_options;

    public String getDdl_add_supplement_contract() {
        return ddl_add_supplement_contract;
    }

    public void setDdl_add_supplement_contract(String ddl_add_supplement_contract) {
        this.ddl_add_supplement_contract = ddl_add_supplement_contract;
    }

    public List<SelectItem> getDdl_add_supplement_contract_options() {
        return ddl_add_supplement_contract_options;
    }

    public void setDdl_add_supplement_contract_options(List<SelectItem> ddl_add_supplement_contract_options) {
        this.ddl_add_supplement_contract_options = ddl_add_supplement_contract_options;
    }

    public void ddl_add_supplement_contract_changed(ValueChangeEvent event) {
        ddl_add_supplement_contract = event.getNewValue() == null ? null : event.getNewValue().toString();
        List<FhrdEmpContractMst> lstEmpContractMst = fhrdEmpContractMstFacade.findMaxContractPeriod(tbl_view_contract.get(0).getUserId().getUserId(), ddl_add_supplement_contract);
        if (lstEmpContractMst.isEmpty()) {
            for (int i = 0; i < tbl_view_contract.size(); i++) {
                if (ddl_add_supplement_contract != null) {
                    if (tbl_view_contract.get(i).getCmSrgKey().getContractCategory().equals("M")) {
                        cal_add_suppl_start_date = tbl_view_contract.get(i).getStartDate();
                        cal_add_suppl_start_date_min = DateTimeUtility.ChangeDateFormat(tbl_view_contract.get(i).getStartDate(), null);
                        cal_add_suppl_end_date_min = DateTimeUtility.ChangeDateFormat(tbl_view_contract.get(i).getStartDate(), null);
                        break;
                    }
                }
            }
        } else {
            cal_add_suppl_start_date = DateTimeUtility.nextDate(lstEmpContractMst.get(0).getEndDate());
            cal_add_suppl_start_date_min = DateTimeUtility.ChangeDateFormat(DateTimeUtility.nextDate(lstEmpContractMst.get(0).getEndDate()), null);
            cal_add_suppl_end_date_min = DateTimeUtility.ChangeDateFormat(DateTimeUtility.nextDate(lstEmpContractMst.get(0).getEndDate()), null);

        }
        for (int i = 0; i < tbl_view_contract.size(); i++) {
            if (tbl_view_contract.get(i).getCmSrgKey().getContractCategory().equals("M")) {
                cal_add_suppl_end_date = tbl_view_contract.get(i).getEndDate();
                cal_add_suppl_start_date_max = DateTimeUtility.ChangeDateFormat(tbl_view_contract.get(i).getEndDate(), null);
                cal_add_suppl_end_date_max = DateTimeUtility.ChangeDateFormat(tbl_view_contract.get(i).getEndDate(), null);
            }
        }
        if (cal_add_suppl_start_date.equals(cal_add_suppl_end_date) || DateTimeUtility.prevDate(cal_add_suppl_start_date).equals(cal_add_suppl_end_date)) {
            cal_add_suppl_start_date = null;
            cal_add_suppl_end_date = null;
        }
    }

    public void addSupplementaryContract(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            FhrdEmpContractMst entSupplContractMst = new FhrdEmpContractMst();
            FhrdContractMst esuppl_Mst = new FhrdContractMst(new BigDecimal(ddl_add_supplement_contract));
            entSupplContractMst.setOumUnitSrno(globalData.getWorking_ou());
            entSupplContractMst.setCmSrgKey(esuppl_Mst);
            entSupplContractMst.setStartDate(cal_add_suppl_start_date);
            entSupplContractMst.setEndDate(cal_add_suppl_end_date);
            entSupplContractMst.setTranStartDate(DateTimeUtility.findBiggestDate(ent_view_contract_detail.getTranStartDate(), cal_add_suppl_start_date));
            entSupplContractMst.setTranEndDate(DateTimeUtility.findSmallestDate(ent_view_contract_detail.getEndDate(), cal_add_suppl_end_date));
            entSupplContractMst.setCreateby(globalData.getEnt_login_user());
            entSupplContractMst.setCreatedt(new Date());
            entSupplContractMst.setEcmSrgKey(new BigDecimal("-2"));
            entSupplContractMst.setEmpcmSrgKey(new FhrdEmpCategoryMst(new BigDecimal("1")));
            entSupplContractMst.setRefOumUnitSrno(globalData.getWorking_ou());
            entSupplContractMst.setUserId(ent_view_contract_detail.getUserId());
            entSupplContractMst.setDsgmSrgKey(ent_view_contract_detail.getDsgmSrgKey());
            entSupplContractMst.setNoticePeriod(ent_view_contract_detail.getNoticePeriod());
            entSupplContractMst.setContSuddenEndFlag("N");
            entSupplContractMst.setContractCategory("S");


            try {
                fhrdEmpContractMstFacade.create(entSupplContractMst);
                setEnt_view_contract_detail();
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Contract successfully assigned to Employee " + ent_view_contract_detail.getUserId().getUserName()));
                RequestContext.getCurrentInstance().execute("pw_dlg_add_sudden_suppl_contract.hide()");

            } catch (EJBException e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred."));
            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred."));
            }
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred."));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_assign_clicked", null, null);
        }
    }
    FhrdContractMst ent_AddSupplementContractDetail;

    public FhrdContractMst getEnt_AddSupplementContractDetail() {
        return ent_AddSupplementContractDetail;
    }

    public void setEnt_AddSupplementContractDetail(FhrdContractMst ent_AddSupplementContractDetail) {
        //        List<FhrdContractMst> lst_List = tbl_supplement_contracts;
        //        lst_List.remove(ent_SupplementContractDetail);
        //        setDdl_supplement_contract();
        //        setTbl_leave_edrule();
        this.ent_AddSupplementContractDetail = ent_AddSupplementContractDetail;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="add supplement contract calendars">
    Date cal_add_suppl_start_date;
    Date cal_add_suppl_end_date;
    String cal_add_suppl_start_date_min;
    String cal_add_suppl_start_date_max;
    String cal_add_suppl_end_date_min;
    String cal_add_suppl_end_date_max;

    public Date getCal_add_suppl_end_date() {
        return cal_add_suppl_end_date;
    }

    public void setCal_add_suppl_end_date(Date cal_add_suppl_end_date) {
        this.cal_add_suppl_end_date = cal_add_suppl_end_date;
    }

    public String getCal_add_suppl_end_date_max() {
        return cal_add_suppl_end_date_max;
    }

    public void setCal_add_suppl_end_date_max(String cal_add_suppl_end_date_max) {
        this.cal_add_suppl_end_date_max = cal_add_suppl_end_date_max;
    }

    public String getCal_add_suppl_end_date_min() {
        return cal_add_suppl_end_date_min;
    }

    public void setCal_add_suppl_end_date_min(String cal_add_suppl_end_date_min) {
        this.cal_add_suppl_end_date_min = cal_add_suppl_end_date_min;
    }

    public Date getCal_add_suppl_start_date() {
        return cal_add_suppl_start_date;
    }

    public void setCal_add_suppl_start_date(Date cal_add_suppl_start_date) {
        this.cal_add_suppl_start_date = cal_add_suppl_start_date;
    }

    public String getCal_add_suppl_start_date_max() {
        return cal_add_suppl_start_date_max;
    }

    public void setCal_add_suppl_start_date_max(String cal_add_suppl_start_date_max) {
        this.cal_add_suppl_start_date_max = cal_add_suppl_start_date_max;
    }

    public String getCal_add_suppl_start_date_min() {
        return cal_add_suppl_start_date_min;
    }

    public void setCal_add_suppl_start_date_min(String cal_add_suppl_start_date_min) {
        this.cal_add_suppl_start_date_min = cal_add_suppl_start_date_min;
    }

    public void cal_add_suppl_start_date_changed(ValueChangeEvent event) {
        cal_add_suppl_start_date = event.getNewValue() == null ? null : (Date) event.getNewValue();
        if (cal_add_suppl_start_date != null) {
            cal_add_suppl_end_date_min = DateTimeUtility.ChangeDateFormat(cal_add_suppl_start_date, null);
        }
    }
    //</editor-fold>
    //</editor-fold>
    ////////////////Set New Contract
    //<editor-fold defaultstate="collapsed" desc="New Contract">
    //<editor-fold defaultstate="collapsed" desc="Contract Start End Date">
    Date cal_tran_start_date;
    Date cal_tran_end_date;
    Date cal_contract_start_date;
    Date cal_contract_end_date;
    String cal_contract_end_date_min;
    String cal_contract_start_date_min;
    String cal_contract_start_date_max;

    public Date getCal_tran_end_date() {
        return cal_tran_end_date;
    }

    public void setCal_tran_end_date(Date cal_tran_end_date) {
        this.cal_tran_end_date = cal_tran_end_date;
    }

    public Date getCal_tran_start_date() {
        return cal_tran_start_date;
    }

    public void setCal_tran_start_date(Date cal_tran_start_date) {
        this.cal_tran_start_date = cal_tran_start_date;
    }

    public String getCal_contract_start_date_max() {
        return cal_contract_start_date_max;
    }

    public void setCal_contract_start_date_max(String cal_contract_start_date_max) {
        this.cal_contract_start_date_max = cal_contract_start_date_max;
    }

    public String getCal_contract_start_date_min() {
        return cal_contract_start_date_min;
    }

    public void setCal_contract_start_date_min(String cal_contract_start_date_min) {
        this.cal_contract_start_date_min = cal_contract_start_date_min;
    }

    public String getCal_contract_end_date_min() {
        return cal_contract_end_date_min;
    }

    public void setCal_contract_end_date_min(String cal_contract_end_date_min) {
        this.cal_contract_end_date_min = cal_contract_end_date_min;
    }

    public Date getCal_contract_end_date() {
        return cal_contract_end_date;
    }

    public void setCal_contract_end_date(Date cal_contract_end_date) {
        this.cal_contract_end_date = cal_contract_end_date;
    }

    public Date getCal_contract_start_date() {
        return cal_contract_start_date;
    }

    public void setCal_contract_start_date(Date cal_contract_start_date) {
        this.cal_contract_start_date = cal_contract_start_date;
    }

    public void cal_contract_start_date_changed(ValueChangeEvent event) {
        cal_contract_start_date = event.getNewValue() == null ? null : (Date) event.getNewValue();
        cal_contract_end_date_min = DateTimeUtility.ChangeDateFormat(cal_contract_start_date, null);
        cal_contract_end_date = null;
        resetContractDateChange();
    }

    public void cal_contract_end_date_changed(ValueChangeEvent event) {
        cal_contract_end_date = event.getNewValue() == null ? null : (Date) event.getNewValue();
        resetContractDateChange();
    }

    public void resetContractDateChange() {
        setDdl_contract();
        setDdl_supplement_contract();
        tbl_leaverule = null;
        tbl_edrule = null;
        tbl_supplement_contracts = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="chk_desig_flag">
    boolean chk_desig_flag;

    public boolean isChk_desig_flag() {
        return chk_desig_flag;
    }

    public void setChk_desig_flag(boolean chk_desig_flag) {
        this.chk_desig_flag = chk_desig_flag;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_user_name">
    String txt_user_name;

    public String getTxt_user_name() {
        return txt_user_name;
    }

    public void setTxt_user_name(String txt_user_name) {
        this.txt_user_name = txt_user_name;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_current_desig">
    String ddl_current_desig;
    List<SelectItem> ddl_current_desig_options;

    public String getDdl_current_desig() {
        return ddl_current_desig;
    }

    public void setDdl_current_desig(String ddl_current_desig) {
        this.ddl_current_desig = ddl_current_desig;
    }

    public List<SelectItem> getDdl_current_desig_options() {
        return ddl_current_desig_options;
    }

    public void setDdl_current_desig_options(List<SelectItem> ddl_current_desig_options) {
        this.ddl_current_desig_options = ddl_current_desig_options;
    }

    public void setDdl_current_desig_options() {
        ddl_current_desig_options = new ArrayList<SelectItem>();
        SelectItem s = new SelectItem(ent_empmst_setContract.getDsgmSrgKey().getDsgmSrgKey().toString(), ent_empmst_setContract.getDsgmSrgKey().getDesigDesc());
        ddl_current_desig_options.add(s);
        ddl_current_desig = ent_empmst_setContract.getDsgmSrgKey().getDsgmSrgKey().toString();
        setDdl_new_desig_options();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_new_desig">
    String ddl_new_desig;
    List<SelectItem> ddl_new_desig_options;

    public String getDdl_new_desig() {
        return ddl_new_desig;
    }

    public void setDdl_new_desig(String ddl_new_desig) {
        this.ddl_new_desig = ddl_new_desig;
    }

    public List<SelectItem> getDdl_new_desig_options() {
        return ddl_new_desig_options;
    }

    public void setDdl_new_desig_options(List<SelectItem> ddl_new_desig_options) {
        this.ddl_new_desig_options = ddl_new_desig_options;
    }

    public void setDdl_new_desig_options() {
        ddl_new_desig_options = new ArrayList<SelectItem>();
        List<FhrdDesigmst> lstDesigmsts;
        if (ddl_current_desig != null) {
            lstDesigmsts = fhrdDesigmstFacade.findAll(ddl_current_desig, globalData.getWorking_ou());
        } else {
            lstDesigmsts = fhrdDesigmstFacade.findAll(globalData.getWorking_ou());
        }
        if (lstDesigmsts.isEmpty()) {
            SelectItem s = new SelectItem(null, "--- Not Available ---");
            ddl_new_desig_options.add(s);
        } else {
            SelectItem s = new SelectItem(null, "--- Select ---");
            ddl_new_desig_options.add(s);
            Integer n = lstDesigmsts.size();
            for (int i = 0; i < n; i++) {
                s = new SelectItem(lstDesigmsts.get(i).getDsgmSrgKey().toString(), lstDesigmsts.get(i).getDesigDesc());
                ddl_new_desig_options.add(s);
            }
        }
        ddl_new_desig = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_notice_period">
    String txt_notice_period;

    public String getTxt_notice_period() {
        return txt_notice_period;
    }

    public void setTxt_notice_period(String txt_notice_period) {
        this.txt_notice_period = txt_notice_period;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_contract">
    List<SelectItem> ddl_contract_option;
    String ddl_contract;

    public String getDdl_contract() {
        return ddl_contract;
    }

    public void setDdl_contract(String ddl_contract) {
        this.ddl_contract = ddl_contract;
    }

    public List<SelectItem> getDdl_contract_option() {
        return ddl_contract_option;
    }

    public void setDdl_contract_option(List<SelectItem> ddl_contract_option) {
        this.ddl_contract_option = ddl_contract_option;
    }

    public void setDdl_contract() {
        ddl_contract_option = new ArrayList<SelectItem>();
        List<FhrdContractMst> lstContractMsts = fhrdContractMstFacade.findContractListOuwise(globalData.getWorking_ou(), "M", cal_contract_start_date, cal_contract_end_date);
        if (lstContractMsts.isEmpty()) {
            SelectItem s = new SelectItem(null, "--- Not Available ---");
            ddl_contract_option.add(s);
        } else {
            SelectItem s = new SelectItem(null, "--- Select ---");
            ddl_contract_option.add(s);
            Integer n = lstContractMsts.size();
            for (int i = 0; i < n; i++) {
                s = new SelectItem(lstContractMsts.get(i).getCmSrgKey(), lstContractMsts.get(i).getContractDesc());
                ddl_contract_option.add(s);
            }
        }
        ddl_contract = null;
    }
    //<editor-fold defaultstate="collapsed" desc="ddl_contract_change">

    public void ddl_contract_changed(ValueChangeEvent event) {
        if (event.getNewValue() != null) {
            ddl_contract = event.getNewValue().toString();
            setTbl_leave_edrule();
            setDdl_supplement_contract();
            cal_suppl_start_date_min = DateTimeUtility.ChangeDateFormat(cal_contract_start_date, null);
            cal_suppl_start_date_max = DateTimeUtility.ChangeDateFormat(cal_contract_end_date, null);
            cal_suppl_end_date_min = DateTimeUtility.ChangeDateFormat(cal_contract_start_date, null);
            cal_suppl_end_date_max = DateTimeUtility.ChangeDateFormat(cal_contract_end_date, null);
            cal_suppl_start_date = cal_contract_start_date;
            cal_suppl_end_date = cal_contract_end_date;
        } else {
            tbl_leaverule = null;
            tbl_edrule = null;
            ddl_supplement_contract = null;
        }
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calendar supplementary start date and date">
    Date cal_suppl_start_date;
    Date cal_suppl_end_date;
    String cal_suppl_start_date_min;
    String cal_suppl_start_date_max;
    String cal_suppl_end_date_min;
    String cal_suppl_end_date_max;

    public Date getCal_suppl_end_date() {
        return cal_suppl_end_date;
    }

    public void setCal_suppl_end_date(Date cal_suppl_end_date) {
        this.cal_suppl_end_date = cal_suppl_end_date;
    }

    public Date getCal_suppl_start_date() {
        return cal_suppl_start_date;
    }

    public void setCal_suppl_start_date(Date cal_suppl_start_date) {
        this.cal_suppl_start_date = cal_suppl_start_date;
    }

    public String getCal_suppl_end_date_max() {
        return cal_suppl_end_date_max;
    }

    public void setCal_suppl_end_date_max(String cal_suppl_end_date_max) {
        this.cal_suppl_end_date_max = cal_suppl_end_date_max;
    }

    public String getCal_suppl_end_date_min() {
        return cal_suppl_end_date_min;
    }

    public void setCal_suppl_end_date_min(String cal_suppl_end_date_min) {
        this.cal_suppl_end_date_min = cal_suppl_end_date_min;
    }

    public String getCal_suppl_start_date_max() {
        return cal_suppl_start_date_max;
    }

    public void setCal_suppl_start_date_max(String cal_suppl_start_date_max) {
        this.cal_suppl_start_date_max = cal_suppl_start_date_max;
    }

    public String getCal_suppl_start_date_min() {
        return cal_suppl_start_date_min;
    }

    public void setCal_suppl_start_date_min(String cal_suppl_start_date_min) {
        this.cal_suppl_start_date_min = cal_suppl_start_date_min;
    }

    public void cal_suppl_start_date_changed(ValueChangeEvent event) {
        cal_suppl_start_date = event.getNewValue() == null ? null : (Date) event.getNewValue();
        if (cal_suppl_start_date != null) {
            cal_suppl_end_date_min = DateTimeUtility.ChangeDateFormat(cal_suppl_start_date, null);
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_supplement_contract">
    String ddl_supplement_contract;
    List<SelectItem> ddl_supplement_contract_options;

    public String getDdl_supplement_contract() {
        return ddl_supplement_contract;
    }

    public void setDdl_supplement_contract(String ddl_supplement_contract) {
        this.ddl_supplement_contract = ddl_supplement_contract;
    }

    public List<SelectItem> getDdl_supplement_contract_options() {
        return ddl_supplement_contract_options;
    }

    public void setDdl_supplement_contract_options(List<SelectItem> ddl_supplement_contract_options) {
        this.ddl_supplement_contract_options = ddl_supplement_contract_options;
    }

    public void setDdl_supplement_contract() {
        ddl_supplement_contract_options = new ArrayList<SelectItem>();
        String v_contractAltlist = "";
        if (ddl_contract != null) {
            FhrdContractMst entContractMst = fhrdContractMstFacade.find(new BigDecimal(ddl_contract));
            v_contractAltlist = entContractMst.getCmAltSrno().toString();
            if (tbl_supplement_contracts != null) {
                for (FhrdContractMst c : tbl_supplement_contracts) {
                    v_contractAltlist += "," + c.getCmAltSrno().toString();
                }
            }
        }
        List<FhrdContractMst> lstContractMsts = fhrdContractMstFacade.findSupplementContractListOuwise(globalData.getWorking_ou(), v_contractAltlist);
        if (lstContractMsts.isEmpty()) {
            SelectItem s = new SelectItem(null, "--- Not Available ---");
            ddl_supplement_contract_options.add(s);
        } else {
            SelectItem s = new SelectItem(null, "--- Select ---");
            ddl_supplement_contract_options.add(s);
            Integer n = lstContractMsts.size();
            for (int i = 0; i < n; i++) {
                s = new SelectItem(lstContractMsts.get(i).getCmSrgKey(), lstContractMsts.get(i).getContractDesc());
                ddl_supplement_contract_options.add(s);
            }
        }
        ddl_supplement_contract = null;
    }

    public void addNewSupplementaryContract(ActionEvent event) {
        //ddl_supplement_contract == null ? null : new BigDecimal(ddl_supplement_contract)
        if (ddl_supplement_contract != null) {
            if (tbl_supplement_contracts == null) {
                tbl_supplement_contracts = new ArrayList<FhrdContractMst>();
            }
            FhrdContractMst ent_addContractMst = fhrdContractMstFacade.find(new BigDecimal(ddl_supplement_contract));
            ent_addContractMst.setStartDate(cal_suppl_start_date);
            ent_addContractMst.setEndDate(cal_suppl_end_date);
            tbl_supplement_contracts.add(ent_addContractMst);

            String v_contract_list = ddl_contract;
            for (FhrdContractMst c : tbl_supplement_contracts) {
                v_contract_list += "," + c.getCmSrgKey().toString();
            }
            setTbl_leave_edrule();
            RequestContext.getCurrentInstance().execute("pw_dlg_supplementary_contract.hide()");
            setDdl_supplement_contract();
            cal_suppl_start_date_min = DateTimeUtility.ChangeDateFormat(cal_contract_start_date, null);
            cal_suppl_start_date_max = DateTimeUtility.ChangeDateFormat(cal_contract_end_date, null);
            cal_suppl_end_date_min = DateTimeUtility.ChangeDateFormat(cal_contract_start_date, null);
            cal_suppl_end_date_max = DateTimeUtility.ChangeDateFormat(cal_contract_end_date, null);
            cal_suppl_start_date = cal_contract_start_date;
            cal_suppl_end_date = cal_contract_end_date;
        }

    }
    FhrdContractMst ent_SupplementContractDetail;

    public FhrdContractMst getEnt_SupplementContractDetail() {
        return ent_SupplementContractDetail;
    }

    public void setEnt_SupplementContractDetail(FhrdContractMst ent_SupplementContractDetail) {
        List<FhrdContractMst> lst_List = tbl_supplement_contracts;
        lst_List.remove(ent_SupplementContractDetail);
        setDdl_supplement_contract();
        setTbl_leave_edrule();
        this.ent_SupplementContractDetail = ent_SupplementContractDetail;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_supplement_contracts">
    List<FhrdContractMst> tbl_supplement_contracts;

    public List<FhrdContractMst> getTbl_supplement_contracts() {
        return tbl_supplement_contracts;
    }

    public void setTbl_supplement_contracts(List<FhrdContractMst> tbl_supplement_contracts) {
        this.tbl_supplement_contracts = tbl_supplement_contracts;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_leaverule">
    List<FhrdLeaverule> tbl_leaverule;

    public List<FhrdLeaverule> getTbl_leaverule() {
        return tbl_leaverule;
    }

    public void setTbl_leaverule(List<FhrdLeaverule> tbl_leaverule) {
        this.tbl_leaverule = tbl_leaverule;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_edrule">
    List<FhrdEarndedn> tbl_edrule;

    public List<FhrdEarndedn> getTbl_edrule() {
        return tbl_edrule;
    }

    public void setTbl_edrule(List<FhrdEarndedn> tbl_edrule) {
        this.tbl_edrule = tbl_edrule;
    }

    private void setTbl_leave_edrule() {
        List<BigDecimal> lst_cm_srg_key = new ArrayList<BigDecimal>();
        lst_cm_srg_key.add(new BigDecimal(ddl_contract));
        if (tbl_supplement_contracts != null) {
            for (FhrdContractMst c : tbl_supplement_contracts) {
                lst_cm_srg_key.add(c.getCmSrgKey());
            }
        }
        tbl_leaverule = fhrdLeaveruleFacade.findLeaveRuleContractwise(ent_empmst_setContract.getUserId().toString(), lst_cm_srg_key, cal_tran_start_date, cal_contract_end_date, null);
        tbl_edrule = fhrdEarndednFacade.findEDRuleContractwise(ent_empmst_setContract.getUserId().toString(), lst_cm_srg_key, cal_tran_start_date, cal_contract_end_date);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ent LeaveRule">
    FhrdLeaverule ent_view_LeaveRule;

    public FhrdLeaverule getEnt_view_LeaveRule() {
        return ent_view_LeaveRule;
    }

    public void setEnt_view_LeaveRule(FhrdLeaverule ent_view_LeaveRule) {
        this.ent_view_LeaveRule = ent_view_LeaveRule;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ent_empmst_setContract">
    FhrdEmpmst ent_empmst_setContract;

    public FhrdEmpmst getEnt_empmst_setContract() {
        return ent_empmst_setContract;
    }

    public void setEnt_empmst_setContract(FhrdEmpmst ent_empmst_setContract) {
        this.ent_empmst_setContract = ent_empmst_setContract;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_view_leaveRule_click ">

    public void btn_view_leaveRule_clicked(FhrdLeaverule fhrdLeaverule) {
        ent_view_LeaveRule = fhrdLeaverule;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ent_FhrdEarndedn_detail">
    FhrdEarndedn ent_FhrdEarndedn_detail;

    public FhrdEarndedn getEnt_FhrdEarndedn_detail() {
        return ent_FhrdEarndedn_detail;
    }

    public void setEnt_FhrdEarndedn_detail(FhrdEarndedn ent_FhrdEarndedn_detail) {
        this.ent_FhrdEarndedn_detail = ent_FhrdEarndedn_detail;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_view_ed_clicked">

    public void btn_view_ed_clicked(FhrdEarndedn fhrdEarndedn) {
        ent_FhrdEarndedn_detail = fhrdEarndedn;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ent_Earndednmst">
    FhrdEarndednmst ent_Earndednmst;

    public FhrdEarndednmst getEnt_Earndednmst() {
        return ent_Earndednmst;
    }

    public void setEnt_Earndednmst(FhrdEarndednmst ent_Earndednmst) {
        this.ent_Earndednmst = ent_Earndednmst;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_assign_clicked">

    public void btn_assign_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            FhrdEmpmst ent_Empmst = new FhrdEmpmst(ent_empmst_setContract.getUserId());
            FhrdEmpContractMst entContractMst = new FhrdEmpContractMst();
            List<FhrdEmpContractMst> lstContractMsts = new ArrayList<FhrdEmpContractMst>();
            FhrdContractMst eMst = new FhrdContractMst(new BigDecimal(ddl_contract));
            entContractMst.setOumUnitSrno(globalData.getWorking_ou());
            entContractMst.setCmSrgKey(eMst);
            entContractMst.setStartDate(cal_contract_start_date);
            entContractMst.setEndDate(cal_contract_end_date);
            entContractMst.setTranStartDate(cal_tran_start_date == null ? cal_contract_start_date : cal_tran_start_date);
            entContractMst.setTranEndDate(cal_contract_end_date);
            entContractMst.setCreateby(globalData.getEnt_login_user());
            entContractMst.setCreatedt(new Date());
            entContractMst.setEcmSrgKey(new BigDecimal("-1"));
            entContractMst.setEmpcmSrgKey(new FhrdEmpCategoryMst(new BigDecimal("1")));
            entContractMst.setRefOumUnitSrno(globalData.getWorking_ou());
            entContractMst.setUserId(ent_Empmst);
            entContractMst.setContSuddenEndFlag("N");
            entContractMst.setContractCategory("M");
            entContractMst.setNoticePeriod(new BigInteger(txt_notice_period));
            if (chk_desig_flag) {
                entContractMst.setDsgmSrgKey(new FhrdDesigmst(new BigDecimal(ddl_new_desig)));
            } else {
                entContractMst.setDsgmSrgKey(new FhrdDesigmst(new BigDecimal(ddl_current_desig)));
            }
            lstContractMsts.add(entContractMst);
            if (tbl_supplement_contracts != null) {
                for (int i = 0; i < tbl_supplement_contracts.size(); i++) {
                    FhrdEmpContractMst entSupplContractMst = new FhrdEmpContractMst();
                    FhrdContractMst esuppl_Mst = new FhrdContractMst(tbl_supplement_contracts.get(i).getCmSrgKey());
                    entSupplContractMst.setOumUnitSrno(globalData.getWorking_ou());
                    entSupplContractMst.setCmSrgKey(esuppl_Mst);
                    entSupplContractMst.setStartDate(tbl_supplement_contracts.get(i).getStartDate());
                    entSupplContractMst.setEndDate(tbl_supplement_contracts.get(i).getEndDate());
                    entSupplContractMst.setTranStartDate(cal_tran_start_date == null ? cal_contract_start_date : cal_tran_start_date);
                    entSupplContractMst.setTranEndDate(cal_contract_end_date);
                    entSupplContractMst.setCreateby(globalData.getEnt_login_user());
                    entSupplContractMst.setCreatedt(new Date());
                    entSupplContractMst.setEcmSrgKey(new BigDecimal("-2"));
                    entSupplContractMst.setEmpcmSrgKey(new FhrdEmpCategoryMst(new BigDecimal("1")));
                    entSupplContractMst.setRefOumUnitSrno(globalData.getWorking_ou());
                    entSupplContractMst.setUserId(ent_Empmst);
                    entSupplContractMst.setContSuddenEndFlag("N");
                    entSupplContractMst.setContractCategory("S");
                    entSupplContractMst.setNoticePeriod(new BigInteger(txt_notice_period));
                    if (chk_desig_flag) {
                        entSupplContractMst.setDsgmSrgKey(new FhrdDesigmst(new BigDecimal(ddl_new_desig)));
                    } else {
                        entSupplContractMst.setDsgmSrgKey(new FhrdDesigmst(new BigDecimal(ddl_current_desig)));
                    }
                    lstContractMsts.add(entSupplContractMst);
                }
            }
            try {
                boolean isTransactionComplete = customJpaController.assignEmpContract(lstContractMsts);
                if (isTransactionComplete) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Contract successfully assigned to Employee " + ent_empmst_setContract.getUserName()));
                    RequestContext.getCurrentInstance().execute("pw_dlg_set_new_contract.hide()");
                } else {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred."));
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_assign_clicked", null, null);
                }
            } catch (EJBException e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred."));
            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred."));
            }
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred."));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_assign_clicked", null, null);
        }
    }
    //</editor-fold>
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Add New Balance">
    //<editor-fold defaultstate="collapsed" desc="showAddNewBalanceDialog">
    public void showAddNewBalanceDialog(FhrdEmpleavebal fhrdEmpleavebal) {
        ent_edit_FhrdLeaveEmpleavebal = fhrdEmpleavebal;
        txt_add_new_balance = null;
        RequestContext.getCurrentInstance().execute("pw_dlg_update_balance.show()");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_add_new_balance">
    String txt_add_new_balance;

    public String getTxt_add_new_balance() {
        return txt_add_new_balance;
    }

    public void setTxt_add_new_balance(String txt_add_new_balance) {
        this.txt_add_new_balance = txt_add_new_balance;
    }
    //</editor-fold>

    public void addNewBalance(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            FhrdEmpleavebal ent_Empleavebal = fhrdEmpleavebalFacade.find(ent_edit_FhrdLeaveEmpleavebal.getEmplbSrgKey());
            ent_Empleavebal.setTotalAddNewLeavebal(ent_Empleavebal.getTotalAddNewLeavebal().add(new BigDecimal(txt_add_new_balance)));
            ent_Empleavebal.setCurrentBal(ent_Empleavebal.getCurrentBal().add(new BigDecimal(txt_add_new_balance)));
            ent_Empleavebal.setNetLeaveBalance(ent_Empleavebal.getNetLeaveBalance().add(new BigDecimal(txt_add_new_balance)));
            ent_Empleavebal.setUpdateby(globalData.getEnt_login_user());
            ent_Empleavebal.setUpdatedt(new Date());
            fhrdEmpleavebalFacade.edit(ent_Empleavebal);
            RequestContext.getCurrentInstance().execute("pw_dlg_update_balance.hide()");
            setEnt_view_contract_detail();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "New leave balance set Successfully"));
        } catch (Exception e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_edit_leavebal_clicked", null, e);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Warning", "Record not saved successfully.Please check all the entries."));
        }
    }
    //</editor-fold>
    ///////////////////////    
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalResources">

    protected GlobalResources getGlobalResources() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalResources) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalResources");
    }// </editor-fold>
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setFilter_first_entry_flag();
        setTblEmpInfo();
        setDdl_contract();
        setDdl_supplement_contract();
        setDdl_new_desig_options();
    }
    //</editor-fold>
}