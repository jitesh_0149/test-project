/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.pis.masters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.customJpaController;
import org.fes.pis.entities.*;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdLeaveruleFacadeLocal;
import org.fes.pis.sessions.FhrdRuleManagGroupMstFacadeLocal;
import org.fes.pis.sessions.FtasLeavemstFacadeLocal;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Leave_Rule_Master")
@ViewScoped
public class PIS_Leave_Rule_Master implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="EJB Declaration and default functions">
    System_Properties system_Properties = new System_Properties();
    @EJB
    private customJpaController customJpaController;
    @EJB
    private FhrdRuleManagGroupMstFacadeLocal fhrdRuleManagGroupMstFacade;
    @EJB
    private FtasLeavemstFacadeLocal ftasLeavemstFacade;
    @EJB
    private FhrdLeaveruleFacadeLocal fhrdLeaveruleFacade;

    /**
     * Creates a new instance of PIS_Leave_Rule_Master
     */
    public PIS_Leave_Rule_Master() {
    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tbl_leave_rule"> 
    List<FhrdLeaverule> tbl_leave_rule;
    List<FhrdLeaverule> tbl_leave_rule_filtered;

    public List<FhrdLeaverule> getTbl_leave_rule_filtered() {
        return tbl_leave_rule_filtered;
    }

    public void setTbl_leave_rule_filtered(List<FhrdLeaverule> tbl_leave_rule_filtered) {
        this.tbl_leave_rule_filtered = tbl_leave_rule_filtered;
    }

    public List<FhrdLeaverule> getTbl_leave_rule() {
        return tbl_leave_rule;
    }

    public void setTbl_leave_rule(List<FhrdLeaverule> tbl_leave_rule) {
        this.tbl_leave_rule = tbl_leave_rule;
    }

    public void setTbl_leave_rule() {
        tbl_leave_rule = fhrdLeaveruleFacade.findAll(globalData.getWorking_ou());
    }
    // </editor-fold>         
    //<editor-fold defaultstate="collapsed" desc="lst_gender_options"> 
    SelectItem[] lst_gender_options;

    public SelectItem[] getLst_gender_options() {
        return lst_gender_options;
    }

    public void setLst_gender_options(SelectItem[] lst_gender_options) {
        this.lst_gender_options = lst_gender_options;
    }

    public void setLst_gender_options() {
        lst_gender_options = new SelectItem[4];
        lst_gender_options[0] = new SelectItem("");
        lst_gender_options[1] = new SelectItem("A", "All");
        lst_gender_options[2] = new SelectItem("M", "Male");
        lst_gender_options[3] = new SelectItem("F", "Female");
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="ddl_cont_due_carry_forward_rule"> 
    String ddl_cont_due_carry_forward_rule;
    List<SelectItem> ddl_cont_due_carry_forward_rule_options;

    public String getDdl_cont_due_carry_forward_rule() {
        return ddl_cont_due_carry_forward_rule;
    }

    public void setDdl_cont_due_carry_forward_rule(String ddl_cont_due_carry_forward_rule) {
        this.ddl_cont_due_carry_forward_rule = ddl_cont_due_carry_forward_rule;
    }

    public List<SelectItem> getDdl_cont_due_carry_forward_rule_options() {
        return ddl_cont_due_carry_forward_rule_options;
    }

    public void setDdl_cont_due_carry_forward_rule_options(List<SelectItem> ddl_cont_due_carry_forward_rule_options) {
        this.ddl_cont_due_carry_forward_rule_options = ddl_cont_due_carry_forward_rule_options;
    }

    public void setDdl_cont_due_carry_forward_rule_options() {
        ddl_cont_due_carry_forward_rule_options = new ArrayList<SelectItem>();
        if (ddl_rmgm_group != null) {
            List<FhrdLeaverule> lst_e = fhrdLeaveruleFacade.findAll(globalData.getWorking_ou());
            if (lst_e.isEmpty()) {
                ddl_cont_due_carry_forward_rule_options.add(new SelectItem(null, "--- Not Available ---"));
            } else {
                ddl_cont_due_carry_forward_rule_options.add(new SelectItem(null, "---- Select ----"));
                for (FhrdLeaverule e : lst_e) {
                    ddl_cont_due_carry_forward_rule_options.add(new SelectItem(e.getLrSrgKey(), e.getGroupLeaveDesc()));
                }
            }
        } else {
            ddl_cont_due_carry_forward_rule_options.add(new SelectItem(null, "--- Not Available ---"));
        }
        ddl_cont_due_carry_forward_rule = null;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ddl_leavecd">
    String ddl_leavecd;
    List<SelectItem> ddl_leavecd_options;
    FtasLeavemst selected_leavecd;
    boolean notVirtual;

    public FtasLeavemst getSelected_leavecd() {
        return selected_leavecd;
    }

    public void setSelected_leavecd(FtasLeavemst selected_leavecd) {
        this.selected_leavecd = selected_leavecd;
    }

    public boolean isNotVirtual() {
        return notVirtual;
    }

    public void setNotVirtual(boolean notVirtual) {
        this.notVirtual = notVirtual;
    }

    public String getDdl_leavecd() {
        return ddl_leavecd;
    }

    public void setDdl_leavecd(String ddl_leavecd) {
        this.ddl_leavecd = ddl_leavecd;
    }

    public List<SelectItem> getDdl_leavecd_options() {
        return ddl_leavecd_options;
    }

    public void setDdl_leavecd_options(List<SelectItem> ddl_leavecd_options) {
        this.ddl_leavecd_options = ddl_leavecd_options;
    }

    private void setDdl_leavecd_options() {
        ddl_leavecd_options = new ArrayList<SelectItem>();
        if (ddl_rmgm_group != null) {
            List<FtasLeavemst> lst_e = ftasLeavemstFacade.findAllforManageGroup(ddl_rmgm_group);
            if (lst_e.isEmpty()) {
                ddl_leavecd_options.add(new SelectItem(null, "--- Not Available ---"));
            } else {
                ddl_leavecd_options.add(new SelectItem(null, "---- Select ----"));
                for (FtasLeavemst e : lst_e) {
                    ddl_leavecd_options.add(new SelectItem(e.getLmSrgKey(), e.getLeavecd() + " -- " + e.getLeaveDesc()));
                }
            }
        } else {
            ddl_leavecd_options.add(new SelectItem(null, "--- Not Available ---"));
        }
        ddl_leavecd = null;
    }

    public void ddl_leavecd_changed(ValueChangeEvent event) {
        ddl_leavecd = event.getNewValue() == null ? null : event.getNewValue().toString();
        if (ddl_leavecd != null) {
            selected_leavecd = ftasLeavemstFacade.find(new BigDecimal(ddl_leavecd));
            notVirtual = false;
            RequestContext.getCurrentInstance().execute("$('.not-virtual').hide()");
            if (selected_leavecd.getVirtualLeave().equals("N") && selected_leavecd.getLeaveFlag().equals("Y")) {
                RequestContext.getCurrentInstance().execute("$('.not-virtual').show()");
                notVirtual = true;
            }
//            if (selected_leavecd.getLeaveFlag().equals("Y") == false) {
//                RequestContext.getCurrentInstance().execute("$('.not-virtual').show()");
//                notVirtual = true;
//            }

            //notVirtual = selected_leavecd.getVirtualLeave().equals("N") ? true : false;
        } else {
            notVirtual = false;
        }
        setCal_start_end_date();

    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ADD NEW BINDING ATTRIBUTES">
    // <editor-fold defaultstate="collapsed" desc="text fields">
    String txt_grp_no;
    String txt_grp_name;
    String txt_entitlement;
    String txt_freq_per_period;
    String txt_freq_period;
    String txt_appl_min_leave;
    String txt_appl_max_leave;
    String txt_deduct_payable_days;
    String txt_per_payable_leave;
    String txt_max_accumulation;
    String txt_earn_leaves_per_period;
    String txt_earn_leaves_period;
    String txt_days_per_leave;
    String txt_carry_forward_max;
    String txt_remarks;
    String txt_min_encash_leave;
    String txt_max_encash_leave;
    String txt_freq_per_period_enc;
    String txt_freq_period_enc;
//    String txt_child_leave_ratio;
//    String txt_max_carry_forward_balance;
    String txt_leave_formula;
    String txt_cont_due_carry_forward_max;
    String txt_days_per_leave_bal_comp;
    String txt_eligibility_period;
    String txt_req_bal_on_retirement;

    // <editor-fold defaultstate="collapsed" desc="getter/setter">
    public String getTxt_appl_max_leave() {
        return txt_appl_max_leave;
    }

    public void setTxt_appl_max_leave(String txt_appl_max_leave) {
        this.txt_appl_max_leave = txt_appl_max_leave;
    }

    public String getTxt_eligibility_period() {
        return txt_eligibility_period;
    }

    public void setTxt_eligibility_period(String txt_eligibility_period) {
        this.txt_eligibility_period = txt_eligibility_period;
    }

    public String getTxt_days_per_leave_bal_comp() {
        return txt_days_per_leave_bal_comp;
    }

    public void setTxt_days_per_leave_bal_comp(String txt_days_per_leave_bal_comp) {
        this.txt_days_per_leave_bal_comp = txt_days_per_leave_bal_comp;
    }

    public String getTxt_earn_leaves_period() {
        return txt_earn_leaves_period;
    }

    public String getTxt_cont_due_carry_forward_max() {
        return txt_cont_due_carry_forward_max;
    }

    public void setTxt_cont_due_carry_forward_max(String txt_cont_due_carry_forward_max) {
        this.txt_cont_due_carry_forward_max = txt_cont_due_carry_forward_max;
    }

    public void setTxt_earn_leaves_period(String txt_earn_leaves_period) {
        this.txt_earn_leaves_period = txt_earn_leaves_period;
    }

    public String getTxt_freq_period() {
        return txt_freq_period;
    }

    public void setTxt_freq_period(String txt_freq_period) {
        this.txt_freq_period = txt_freq_period;
    }

    public String getTxt_grp_no() {
        return txt_grp_no;
    }

    public void setTxt_grp_no(String txt_grp_no) {
        this.txt_grp_no = txt_grp_no;
    }

    public String getTxt_grp_name() {
        return txt_grp_name;
    }

    public void setTxt_grp_name(String txt_grp_name) {
        this.txt_grp_name = txt_grp_name;
    }

    public String getTxt_appl_min_leave() {
        return txt_appl_min_leave;
    }

    public void setTxt_appl_min_leave(String txt_appl_min_leave) {
        this.txt_appl_min_leave = txt_appl_min_leave;
    }

    public String getTxt_carry_forward_max() {
        return txt_carry_forward_max;
    }

    public void setTxt_carry_forward_max(String txt_carry_forward_max) {
        this.txt_carry_forward_max = txt_carry_forward_max;
    }

//    public String getTxt_child_leave_ratio() {
//        return txt_child_leave_ratio;
//    }
//
//    public void setTxt_child_leave_ratio(String txt_child_leave_ratio) {
//        this.txt_child_leave_ratio = txt_child_leave_ratio;
//    }
    public String getTxt_days_per_leave() {
        return txt_days_per_leave;
    }

    public void setTxt_days_per_leave(String txt_days_per_leave) {
        this.txt_days_per_leave = txt_days_per_leave;
    }

    public String getTxt_deduct_payable_days() {
        return txt_deduct_payable_days;
    }

    public void setTxt_deduct_payable_days(String txt_deduct_payable_days) {
        this.txt_deduct_payable_days = txt_deduct_payable_days;
    }

    public String getTxt_earn_leaves_per_period() {
        return txt_earn_leaves_per_period;
    }

    public void setTxt_earn_leaves_per_period(String txt_earn_leaves_per_period) {
        this.txt_earn_leaves_per_period = txt_earn_leaves_per_period;
    }

    public String getTxt_entitlement() {
        return txt_entitlement;
    }

    public void setTxt_entitlement(String txt_entitlement) {
        this.txt_entitlement = txt_entitlement;
    }

    public String getTxt_freq_per_period() {
        return txt_freq_per_period;
    }

    public void setTxt_freq_per_period(String txt_freq_per_period) {
        this.txt_freq_per_period = txt_freq_per_period;
    }

    public String getTxt_freq_per_period_enc() {
        return txt_freq_per_period_enc;
    }

    public void setTxt_freq_per_period_enc(String txt_freq_per_period_enc) {
        this.txt_freq_per_period_enc = txt_freq_per_period_enc;
    }

    public String getTxt_max_accumulation() {
        return txt_max_accumulation;
    }

    public void setTxt_max_accumulation(String txt_max_accumulation) {
        this.txt_max_accumulation = txt_max_accumulation;
    }

//    public String getTxt_max_carry_forward_balance() {
//        return txt_max_carry_forward_balance;
//    }
//
//    public void setTxt_max_carry_forward_balance(String txt_max_carry_forward_balance) {
//        this.txt_max_carry_forward_balance = txt_max_carry_forward_balance;
//    }
    public String getTxt_max_encash_leave() {
        return txt_max_encash_leave;
    }

    public void setTxt_max_encash_leave(String txt_max_encash_leave) {
        this.txt_max_encash_leave = txt_max_encash_leave;
    }

    public String getTxt_min_encash_leave() {
        return txt_min_encash_leave;
    }

    public void setTxt_min_encash_leave(String txt_min_encash_leave) {
        this.txt_min_encash_leave = txt_min_encash_leave;
    }

    public String getTxt_freq_period_enc() {
        return txt_freq_period_enc;
    }

    public void setTxt_freq_period_enc(String txt_freq_period_enc) {
        this.txt_freq_period_enc = txt_freq_period_enc;
    }

    public String getTxt_per_payable_leave() {
        return txt_per_payable_leave;
    }

    public void setTxt_per_payable_leave(String txt_per_payable_leave) {
        this.txt_per_payable_leave = txt_per_payable_leave;
    }

    public String getTxt_remarks() {
        return txt_remarks;
    }

    public void setTxt_remarks(String txt_remarks) {
        this.txt_remarks = txt_remarks;
    }

    public String getTxt_leave_formula() {
        return txt_leave_formula;
    }

    public void setTxt_leave_formula(String txt_leave_formula) {
        this.txt_leave_formula = txt_leave_formula;
    }

    public String getTxt_req_bal_on_retirement() {
        return txt_req_bal_on_retirement;
    }

    public void setTxt_req_bal_on_retirement(String txt_req_bal_on_retirement) {
        this.txt_req_bal_on_retirement = txt_req_bal_on_retirement;
    }
    // </editor-fold>
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Dropdown Fields">
    String ddl_gender_flag;
    String ddl_entitlement_flag;
    String ddl_freq_period_unit;
    String ddl_freq_considered_from;
    String ddl_balance_check_application;
    String ddl_proportionate_balance;
    String ddl_earn_leaves_period_unit;
    String ddl_holiday_embedded;
    String ddl_carry_forward;
    String ddl_carry_forward_remaining;
    String ddl_freq_period_unit_enc;
//    String ddl_carry_forward_parent_bal;
//    String ddl_parent_carry_forward_remaining;
    String ddl_halfday_applicable;
    String ddl_club_with_other;
    String ddl_fix_month;
    String ddl_start_month;
    String ddl_end_month;
    String ddl_cont_due_carry_forward;
    String ddl_cont_due_carry_forward_remaining;
    String ddl_eligibility_unit;
    String ddl_exhaust_other_leave;
    String ddl_auto_encash;
    String ddl_auto_encash_month;
    String ddl_proportionate_at_joining;
    String ddl_conti_allowed_flag;

    public String getDdl_conti_allowed_flag() {
        return ddl_conti_allowed_flag;
    }

    public void setDdl_conti_allowed_flag(String ddl_conti_allowed_flag) {
        this.ddl_conti_allowed_flag = ddl_conti_allowed_flag;
    }

    public String getDdl_proportionate_at_joining() {
        return ddl_proportionate_at_joining;
    }

    public void setDdl_proportionate_at_joining(String ddl_proportionate_at_joining) {
        this.ddl_proportionate_at_joining = ddl_proportionate_at_joining;
    }
    //<editor-fold defaultstate="collapsed" desc="ddl_leave_laps">
    String ddl_leave_laps;

    public String getDdl_leave_laps() {
        return ddl_leave_laps;
    }

    public void setDdl_leave_laps(String ddl_leave_laps) {
        this.ddl_leave_laps = ddl_leave_laps;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="getter/setter">

    public String getDdl_exhaust_other_leave() {
        return ddl_exhaust_other_leave;
    }

    public void setDdl_exhaust_other_leave(String ddl_exhaust_other_leave) {
        this.ddl_exhaust_other_leave = ddl_exhaust_other_leave;
    }

    public String getDdl_balance_check_application() {
        return ddl_balance_check_application;
    }

    public void setDdl_balance_check_application(String ddl_balance_check_application) {
        this.ddl_balance_check_application = ddl_balance_check_application;
    }

    public String getDdl_eligibility_unit() {
        return ddl_eligibility_unit;
    }

    public void setDdl_eligibility_unit(String ddl_eligibility_unit) {
        this.ddl_eligibility_unit = ddl_eligibility_unit;
    }

    public String getDdl_cont_due_carry_forward_remaining() {
        return ddl_cont_due_carry_forward_remaining;
    }

    public void setDdl_cont_due_carry_forward_remaining(String ddl_cont_due_carry_forward_remaining) {
        this.ddl_cont_due_carry_forward_remaining = ddl_cont_due_carry_forward_remaining;
    }

    public String getDdl_halfday_applicable() {
        return ddl_halfday_applicable;
    }

    public void setDdl_halfday_applicable(String ddl_halfday_applicable) {
        this.ddl_halfday_applicable = ddl_halfday_applicable;
    }

    public String getDdl_gender_flag() {
        return ddl_gender_flag;
    }

    public void setDdl_gender_flag(String ddl_gender_flag) {
        this.ddl_gender_flag = ddl_gender_flag;
    }

    public String getDdl_carry_forward() {
        return ddl_carry_forward;
    }

    public void setDdl_carry_forward(String ddl_carry_forward) {
        this.ddl_carry_forward = ddl_carry_forward;
    }

//    public String getDdl_carry_forward_parent_bal() {
//        return ddl_carry_forward_parent_bal;
//    }
//
//    public void setDdl_carry_forward_parent_bal(String ddl_carry_forward_parent_bal) {
//        this.ddl_carry_forward_parent_bal = ddl_carry_forward_parent_bal;
//    }
    public String getDdl_carry_forward_remaining() {
        return ddl_carry_forward_remaining;
    }

    public String getDdl_end_month() {
        return ddl_end_month;
    }

    public void setDdl_end_month(String ddl_end_month) {
        this.ddl_end_month = ddl_end_month;
    }

    public String getDdl_start_month() {
        return ddl_start_month;
    }

    public void setDdl_start_month(String ddl_start_month) {
        this.ddl_start_month = ddl_start_month;
    }

    public String getDdl_fix_month() {
        return ddl_fix_month;
    }

    public void setDdl_fix_month(String ddl_fix_month) {
        this.ddl_fix_month = ddl_fix_month;
    }

    public void setDdl_carry_forward_remaining(String ddl_carry_forward_remaining) {
        this.ddl_carry_forward_remaining = ddl_carry_forward_remaining;
    }

    public String getDdl_club_with_other() {
        return ddl_club_with_other;
    }

    public void setDdl_club_with_other(String ddl_club_with_other) {
        this.ddl_club_with_other = ddl_club_with_other;
    }

    public String getDdl_entitlement_flag() {
        return ddl_entitlement_flag;
    }

    public void setDdl_entitlement_flag(String ddl_entitlement_flag) {
        this.ddl_entitlement_flag = ddl_entitlement_flag;
    }

    public String getDdl_freq_considered_from() {
        return ddl_freq_considered_from;
    }

    public void setDdl_freq_considered_from(String ddl_freq_considered_from) {
        this.ddl_freq_considered_from = ddl_freq_considered_from;
    }

    public String getDdl_freq_period_unit() {
        return ddl_freq_period_unit;
    }

    public void setDdl_freq_period_unit(String ddl_freq_period_unit) {
        this.ddl_freq_period_unit = ddl_freq_period_unit;
    }

    public String getDdl_holiday_embedded() {
        return ddl_holiday_embedded;
    }

    public void setDdl_holiday_embedded(String ddl_holiday_embedded) {
        this.ddl_holiday_embedded = ddl_holiday_embedded;
    }

//    public String getDdl_parent_carry_forward_remaining() {
//        return ddl_parent_carry_forward_remaining;
//    }
//
//    public void setDdl_parent_carry_forward_remaining(String ddl_parent_carry_forward_remaining) {
//        this.ddl_parent_carry_forward_remaining = ddl_parent_carry_forward_remaining;
//    }
    public String getDdl_earn_leaves_period_unit() {
        return ddl_earn_leaves_period_unit;
    }

    public void setDdl_earn_leaves_period_unit(String ddl_earn_leaves_period_unit) {
        this.ddl_earn_leaves_period_unit = ddl_earn_leaves_period_unit;
    }

    public String getDdl_freq_period_unit_enc() {
        return ddl_freq_period_unit_enc;
    }

    public void setDdl_freq_period_unit_enc(String ddl_freq_period_unit_enc) {
        this.ddl_freq_period_unit_enc = ddl_freq_period_unit_enc;
    }

    public String getDdl_proportionate_balance() {
        return ddl_proportionate_balance;
    }

    public void setDdl_proportionate_balance(String ddl_proportionate_balance) {
        this.ddl_proportionate_balance = ddl_proportionate_balance;
    }

    public String getDdl_cont_due_carry_forward() {
        return ddl_cont_due_carry_forward;
    }

    public void setDdl_cont_due_carry_forward(String ddl_cont_due_carry_forward) {
        this.ddl_cont_due_carry_forward = ddl_cont_due_carry_forward;
    }

    public String getDdl_auto_encash() {
        return ddl_auto_encash;
    }

    public void setDdl_auto_encash(String ddl_auto_encash) {
        this.ddl_auto_encash = ddl_auto_encash;
    }

    public String getDdl_auto_encash_month() {
        return ddl_auto_encash_month;
    }

    public void setDdl_auto_encash_month(String ddl_auto_encash_month) {
        this.ddl_auto_encash_month = ddl_auto_encash_month;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="boolean checkboxes">
    boolean chk_encash;

    public boolean getChk_encash() {
        return chk_encash;
    }

    public void setChk_encash(boolean chk_encash) {
        this.chk_encash = chk_encash;
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="chk_eligibility"> 
    boolean chk_eligibility;

    public boolean isChk_eligibility() {
        return chk_eligibility;
    }

    public void setChk_eligibility(boolean chk_eligibility) {
        this.chk_eligibility = chk_eligibility;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_eligibility_considered_from"> 
    String ddl_eligibility_considered_from;

    public String getDdl_eligibility_considered_from() {
        return ddl_eligibility_considered_from;
    }

    public void setDdl_eligibility_considered_from(String ddl_eligibility_considered_from) {
        this.ddl_eligibility_considered_from = ddl_eligibility_considered_from;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_rmgm_group"> 
    String ddl_rmgm_group;
    List<SelectItem> ddl_rmgm_group_options;

    public String getDdl_rmgm_group() {
        return ddl_rmgm_group;
    }

    public void setDdl_rmgm_group(String ddl_rmgm_group) {
        this.ddl_rmgm_group = ddl_rmgm_group;
    }

    public List<SelectItem> getDdl_rmgm_group_options() {
        return ddl_rmgm_group_options;
    }

    public void setDdl_rmgm_group_options(List<SelectItem> ddl_rmgm_group_options) {
        this.ddl_rmgm_group_options = ddl_rmgm_group_options;
    }

    public void setDdl_rmgm_group() {
        ddl_rmgm_group_options = new ArrayList<SelectItem>();
        List<FhrdRuleManagGroupMst> lst_e = fhrdRuleManagGroupMstFacade.findAll(globalData.getWorking_ou(), null, "ADMIN", null, null, null, false);
        if (lst_e.isEmpty()) {
            ddl_rmgm_group_options.add(new SelectItem(null, "--- Not Available ---"));
        } else {
            ddl_rmgm_group_options.add(new SelectItem(null, "--- Select ---"));
            for (FhrdRuleManagGroupMst e : lst_e) {
                ddl_rmgm_group_options.add(new SelectItem(e.getRmgmSrgKey(), e.getManagGroupDesc()));
            }
        }
        ddl_rmgm_group = null;
        setCal_start_end_date();
    }

    public void ddl_rmgm_group_changed(ValueChangeEvent event) {
        ddl_rmgm_group = event.getNewValue() != null ? event.getNewValue().toString() : null;
        setDdl_leavecd_options();
        setCal_start_end_date();
        setDdl_cont_due_carry_forward_rule_options();
//        setDdl_parent_rule();
    }
    //</editor-fold>    
    //    //<editor-fold defaultstate="collapsed" desc="ddl_parent_rule"> 
//    String ddl_parent_rule;
//    List<SelectItem> ddl_parent_rule_options;
//
//    public String getDdl_parent_rule() {
//        return ddl_parent_rule;
//    }
//
//    public void setDdl_parent_rule(String ddl_parent_rule) {
//        this.ddl_parent_rule = ddl_parent_rule;
//    }
//
//    public List<SelectItem> getDdl_parent_rule_options() {
//        return ddl_parent_rule_options;
//    }
//
//    public void setDdl_parent_rule_options(List<SelectItem> ddl_parent_rule_options) {
//        this.ddl_parent_rule_options = ddl_parent_rule_options;
//    }
//
//    public void setDdl_parent_rule() {
//        ddl_parent_rule_options = new ArrayList<SelectItem>();
//        List<FhrdLeaverule> lst_e = fhrdLeaveruleFacade.findParentRules(globalData.getWorking_ou(), cal_start_date, ddl_rmgm_group);
//        if (lst_e.isEmpty()) {
//            SelectItem s = new SelectItem(null, "--- Not Available ---");
//            ddl_parent_rule_options.add(s);
//        } else {
//            SelectItem s = new SelectItem(null, "---- Select ----");
//            ddl_parent_rule_options.add(s);
//            Integer n = lst_e.size();
//            for (int i = 0; i < n; i++) {
//                s = new SelectItem(lst_e.get(i).getLrSrgKey(), lst_e.get(i).getRmgmSrgKey().getManagGroupDesc() + " - " + lst_e.get(i).getLmSrgKey().getLeaveDesc());
//                ddl_parent_rule_options.add(s);
//            }
//        }
//        ddl_parent_rule = null;
//    }
//    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Date">
    //<editor-fold defaultstate="collapsed" desc="cal_wef_dates">
    Date cal_wef_date;
    String cal_wef_date_min;
    String cal_wef_date_max;

    public String getCal_wef_date_max() {
        return cal_wef_date_max;
    }

    public void setCal_wef_date_max(String cal_wef_date_max) {
        this.cal_wef_date_max = cal_wef_date_max;
    }

    public String getCal_wef_date_min() {
        return cal_wef_date_min;
    }

    public void setCal_wef_date_min(String cal_wef_date_min) {
        this.cal_wef_date_min = cal_wef_date_min;
    }

    public Date getCal_wef_date() {
        return cal_wef_date;
    }

    public void setCal_wef_date(Date cal_wef_date) {
        this.cal_wef_date = cal_wef_date;
    }

    private void setCal_wef_date() {
        //cal_wef_date = DateTimeUtility.startDateOfMonthFromDate(new Date());
        cal_wef_date = new Date(new Date().getYear(), 0, 1);
        cal_wef_date_min = DateTimeUtility.ChangeDateFormat(cal_wef_date, null);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_end_date">
    Date cal_end_date;
    String cal_end_date_min;
    String cal_end_date_max;

    public Date getCal_end_date() {
        return cal_end_date;
    }

    public void setCal_end_date(Date cal_end_date) {
        this.cal_end_date = cal_end_date;
    }

    public String getCal_end_date_max() {
        return cal_end_date_max;
    }

    public void setCal_end_date_max(String cal_end_date_max) {
        this.cal_end_date_max = cal_end_date_max;
    }

    public String getCal_end_date_min() {
        return cal_end_date_min;
    }

    public void setCal_end_date_min(String cal_end_date_min) {
        this.cal_end_date_min = cal_end_date_min;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_start_date">
    Date cal_start_date;
    String cal_start_date_min;
    String cal_start_date_max;

    public Date getCal_start_date() {
        return cal_start_date;
    }

    public void setCal_start_date(Date cal_start_date) {
        this.cal_start_date = cal_start_date;
    }

    public String getCal_start_date_max() {
        return cal_start_date_max;
    }

    public void setCal_start_date_max(String cal_start_date_max) {
        this.cal_start_date_max = cal_start_date_max;
    }

    public String getCal_start_date_min() {
        return cal_start_date_min;
    }

    public void setCal_start_date_min(String cal_start_date_min) {
        this.cal_start_date_min = cal_start_date_min;
    }

    public void cal_start_date_changed(SelectEvent event) {
        cal_start_date = (Date) event.getObject();
        if (cal_end_date != null) {
            if (cal_end_date.before(cal_start_date)) {
                cal_end_date = cal_start_date;
            }
        }
        cal_end_date_min = DateTimeUtility.ChangeDateFormat(cal_start_date, null);
        cal_wef_date = cal_start_date;
    }

    private void setCal_start_end_date() {
        Date v_min = null, v_max = null;
        if (ddl_rmgm_group != null) {
            FhrdRuleManagGroupMst selected_GroupMst = fhrdRuleManagGroupMstFacade.find(new BigDecimal(ddl_rmgm_group));
            v_min = selected_GroupMst.getStartDate();
            v_max = selected_GroupMst.getEndDate();
        }
        if (ddl_leavecd != null) {
            FtasLeavemst selected_Leavemst = ftasLeavemstFacade.find(new BigDecimal(ddl_leavecd));
            v_min = DateTimeUtility.findBiggestDate(v_min, selected_Leavemst.getStartDate());
            v_max = DateTimeUtility.findSmallestDate(v_max, selected_Leavemst.getEndDate());
        }
        cal_start_date = v_min;
        cal_end_date = v_max;
        cal_wef_date = v_min;
        cal_start_date_min = cal_end_date_min = DateTimeUtility.ChangeDateFormat(v_min, null);
        cal_start_date_max = cal_end_date_max = DateTimeUtility.ChangeDateFormat(v_max, null);
        cal_wef_date_max = cal_end_date_max = DateTimeUtility.ChangeDateFormat(v_max, null);
    }

    public void resetEndDate(ActionEvent event) {
        cal_end_date = null;
    }
    //</editor-fold>

    // </editor-fold>
    // </editor-fold>     
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="detail view">
    public void btn_view_detail_action(ActionEvent event) {
    }
    FhrdLeaverule entFhrdLeaverule_detail;

    public FhrdLeaverule getEntFhrdLeaverule_detail() {
        return entFhrdLeaverule_detail;
    }

    public void setEntFhrdLeaverule_detail(FhrdLeaverule entFhrdLeaverule_detail) {
        this.entFhrdLeaverule_detail = entFhrdLeaverule_detail;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="btn save action event">

    public void btn_add_new_action(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            FhrdEmpmst ent_Empmst = new FhrdEmpmst(globalData.getUser_id());
            SystOrgUnitMst ent_OrgUnitMst = new SystOrgUnitMst(globalData.getWorking_ou());
            BigDecimal v_lr_srgkey = getGlobalSettings().getUniqueSrno(null, "FHRD_LEAVERULE");
            FtasLeavemst entFtasLeavemst = ftasLeavemstFacade.find(new BigDecimal(ddl_leavecd));
            FhrdLeaverule ent_FhrdLeaverule = new FhrdLeaverule(v_lr_srgkey);
            //<editor-fold defaultstate="collapsed" desc="General Properties">
            ent_FhrdLeaverule.setRmgmSrgKey(new FhrdRuleManagGroupMst(new BigDecimal(ddl_rmgm_group)));
            ent_FhrdLeaverule.setLeavecd(entFtasLeavemst.getLeavecd());
            ent_FhrdLeaverule.setLmSrgKey(entFtasLeavemst);
            ent_FhrdLeaverule.setLmAltSrno(entFtasLeavemst.getLmAltSrno());
            ent_FhrdLeaverule.setGenderFlag(ddl_gender_flag);
            //</editor-fold>  
            //<editor-fold defaultstate="collapsed" desc="Leave Limits Information">            
            ddl_proportionate_balance.equals("N");
            ent_FhrdLeaverule.setProportionateAtJoinOnly("N");
            ent_FhrdLeaverule.setContAllowedFlag("N");

            ent_FhrdLeaverule.setEarnIncludeFlag("N");
            if (ddl_fix_month != null) {
                ent_FhrdLeaverule.setFixMonth(ddl_fix_month);
            }
            ent_FhrdLeaverule.setProportionateBalance("N");
            ent_FhrdLeaverule.setBalanceEarnType("N");
            ent_FhrdLeaverule.setCarryForward("N");
            ent_FhrdLeaverule.setContrdueCarryForwrd("N");
            ent_FhrdLeaverule.setLeaveLapsInClubFlag("N");
            ent_FhrdLeaverule.setBalCheckedInAppliFlag("N");
            ent_FhrdLeaverule.setEncashment("N");
            ent_FhrdLeaverule.setExhaustOtherLeaveFlag(ddl_exhaust_other_leave);
            if (!txt_req_bal_on_retirement.isEmpty()) {
                ent_FhrdLeaverule.setReqBalOnRelieve(Integer.valueOf(txt_req_bal_on_retirement));
            }
            if (ddl_proportionate_at_joining != null) {
                ent_FhrdLeaverule.setProportionateAtJoinOnly(ddl_proportionate_at_joining);
            }
            if (ddl_conti_allowed_flag != null) {
                ent_FhrdLeaverule.setContAllowedFlag("Y");
            }
            if (chk_encash) {
                ent_FhrdLeaverule.setEncashment("Y");
            }

            ent_FhrdLeaverule.setEligibilityApplicable("N");

            if (selected_leavecd.getVirtualLeave().equals("N")) {
                ent_FhrdLeaverule.setNoOfLeave(new BigDecimal(txt_entitlement));
                ent_FhrdLeaverule.setBalanceEarnType(ddl_entitlement_flag);
                ent_FhrdLeaverule.setCarryForward(ddl_carry_forward);
                ent_FhrdLeaverule.setContrdueCarryForwrd(ddl_cont_due_carry_forward);
                if (ddl_cont_due_carry_forward != null) {
                    ent_FhrdLeaverule.setContrdueCarryForwrd(ddl_cont_due_carry_forward);
                } else {
                    ent_FhrdLeaverule.setContrdueCarryForwrd("N");
                }
                if (!txt_max_accumulation.isEmpty() && !ent_FhrdLeaverule.getBalanceEarnType().equals("M")) {
                    ent_FhrdLeaverule.setAccumulation(new BigDecimal(txt_max_accumulation));
                }
                if (ent_FhrdLeaverule.getBalanceEarnType().equals("C")) {
                    ent_FhrdLeaverule.setEarnIncludeFlag("Y");
                } else if (ent_FhrdLeaverule.getBalanceEarnType().equals("P")) {
                    ent_FhrdLeaverule.setProportionateBalance(ddl_proportionate_balance);
                } else if (ent_FhrdLeaverule.getBalanceEarnType().equals("O")) {
                } else if (ent_FhrdLeaverule.getBalanceEarnType().equals("M")) {
                    ent_FhrdLeaverule.setProportionateBalance("N");
                    ent_FhrdLeaverule.setFixMonth(null);
                    ent_FhrdLeaverule.setAccumulation(null);
                    ent_FhrdLeaverule.setContrdueCarryForwrd("N");
                    ent_FhrdLeaverule.setCarryForward("N");
                    if (chk_eligibility) {
                        ent_FhrdLeaverule.setEligibilityApplicable("Y");
                    }
                    ent_FhrdLeaverule.setEncashment("N");
                }

                if (!txt_deduct_payable_days.isEmpty()) {
                    ent_FhrdLeaverule.setDeductDays(new BigDecimal(txt_deduct_payable_days));
                    ent_FhrdLeaverule.setDeductLeaves(new BigDecimal(txt_per_payable_leave));
                }

                ent_FhrdLeaverule.setBalCheckedInAppliFlag(ddl_balance_check_application);
                ent_FhrdLeaverule.setLeaveLapsInClubFlag(ddl_leave_laps);
            }
            if (ent_FhrdLeaverule.getProportionateBalance().equals("Y")) {
                ent_FhrdLeaverule.setFixMonth("F");

            }
            if (ent_FhrdLeaverule.getFixMonth() != null && ent_FhrdLeaverule.getFixMonth().equals("F")) {
                ent_FhrdLeaverule.setStartMonth(ddl_start_month);
                ent_FhrdLeaverule.setEndMonth(ddl_end_month);
            }
            ent_FhrdLeaverule.setDaysPerLeaveBalComparison(new BigDecimal(txt_days_per_leave_bal_comp));
            if (!txt_freq_per_period.isEmpty()) {
                ent_FhrdLeaverule.setFreqPeriod(Integer.valueOf(txt_freq_period));
                ent_FhrdLeaverule.setFreqPerPeriod(Integer.valueOf(txt_freq_per_period));
                ent_FhrdLeaverule.setFreqPeriodUnit(ddl_freq_period_unit);
                ent_FhrdLeaverule.setFreqConsFrom(ddl_freq_considered_from);
            }
            ent_FhrdLeaverule.setMinTaken(new BigDecimal(txt_appl_min_leave));
            if (!txt_appl_max_leave.isEmpty()) {
                ent_FhrdLeaverule.setMaxTaken(new BigDecimal(txt_appl_max_leave));
            }
            ent_FhrdLeaverule.setStartDate(cal_start_date);
            if (cal_end_date != null) {
                ent_FhrdLeaverule.setEndDate(cal_end_date);
            }
            ent_FhrdLeaverule.setWefDate(cal_wef_date);
            //</editor-fold>
            //<editor-fold defaultstate="collapsed" desc="Leave's General Properties">
            if (ent_FhrdLeaverule.getEarnIncludeFlag().equals("Y")) {
                ent_FhrdLeaverule.setEarnLeavePerPeriod(new BigDecimal(txt_earn_leaves_per_period));
                ent_FhrdLeaverule.setEarnLeavePeriod(new BigDecimal(txt_earn_leaves_period));
                ent_FhrdLeaverule.setEarnLeavePeriodUnit(ddl_earn_leaves_period_unit);
            }
            ent_FhrdLeaverule.setHolidayEmbedded(ddl_holiday_embedded);

//            if (ent_FhrdLeaverule.getCarryForward().equals("Y")) {
//                ent_FhrdLeaverule.setCarryForwardMax(new BigDecimal(txt_carry_forward_max));
//                ent_FhrdLeaverule.setCarryForwardRemaining(ddl_carry_forward_remaining);
//            }

            if (!txt_carry_forward_max.isEmpty()) {
                ent_FhrdLeaverule.setContrdueCarryForwrdMax(new BigDecimal(txt_carry_forward_max));
            }
            if (ddl_carry_forward_remaining != null) {
                ent_FhrdLeaverule.setContrdueCarryforwrdRemaining(ddl_carry_forward_remaining);
            }
//            if (ent_FhrdLeaverule.getContrdueCarryForwrd().equals("Y")) {
//                ent_FhrdLeaverule.setContrdueCarryForwrdMax(new BigDecimal(txt_cont_due_carry_forward_max));
//                ent_FhrdLeaverule.setContrdueCarryforwrdRemaining(ddl_cont_due_carry_forward_remaining);
//            }
            if (!txt_cont_due_carry_forward_max.isEmpty()) {
                ent_FhrdLeaverule.setContrdueCarryForwrdMax(new BigDecimal(txt_cont_due_carry_forward_max));
            }
            if (ddl_cont_due_carry_forward_remaining != null) {
                ent_FhrdLeaverule.setContrdueCarryforwrdRemaining(ddl_cont_due_carry_forward_remaining);
            }
            ent_FhrdLeaverule.setHalfdayApplicable(ddl_halfday_applicable);
            if (!txt_remarks.trim().isEmpty()) {
                ent_FhrdLeaverule.setRemarks(txt_remarks.trim().toUpperCase());
            }

            //</editor-fold> 
            //<editor-fold defaultstate="collapsed" desc="Encashment">
            if (ent_FhrdLeaverule.getEncashment().equals("Y") && selected_leavecd.getVirtualLeave().equals("N")) {

                if (!txt_min_encash_leave.trim().isEmpty()) {
                    ent_FhrdLeaverule.setEncashMinLeave(new BigDecimal(txt_min_encash_leave));
                }
                if (!txt_max_encash_leave.trim().isEmpty()) {
                    ent_FhrdLeaverule.setEncashMaxLeave(new BigDecimal(txt_max_encash_leave));
                }
                if (!txt_freq_per_period_enc.trim().isEmpty()) {
                    ent_FhrdLeaverule.setEncashFreq(new BigDecimal(txt_freq_per_period_enc));
                    ent_FhrdLeaverule.setEncashFreqPeriod(new BigDecimal(txt_freq_period_enc));
                    ent_FhrdLeaverule.setEncashFreqPeriodUnit(ddl_freq_period_unit_enc);
                }
                if (ddl_auto_encash != null) {
                    ent_FhrdLeaverule.setEncashAutoFlag(ddl_auto_encash);
                }
                if (ddl_auto_encash_month != null) {
                    ent_FhrdLeaverule.setEncashAutoMonth(ddl_auto_encash_month);
                }
            }
            //</editor-fold>            
            //<editor-fold defaultstate="collapsed" desc="Eligibility">
            if (ent_FhrdLeaverule.getEligibilityApplicable().equals("Y")) {
                ent_FhrdLeaverule.setEligibilityConsidered(ddl_eligibility_considered_from);
                ent_FhrdLeaverule.setEligibilityPeriod(Short.valueOf(txt_eligibility_period));
                ent_FhrdLeaverule.setEligibilityUnit(ddl_eligibility_unit);
            }
            //</editor-fold>            
            FhrdLeaverule entFhrdLeaverule_old = null;
            //<editor-fold defaultstate="collapsed" desc="Parent Detail">
//            if (ddl_parent_rule != null) {
//                ent_FhrdLeaverule.setParentLrSrgKey(new FhrdLeaverule(new BigDecimal(ddl_parent_rule)));
//                ent_FhrdLeaverule.setParentCarryforwardRemaining(ddl_parent_carry_forward_remaining);
//                if (selected_leavecd.getVirtualLeave().equals("N")) {
//                    ent_FhrdLeaverule.setParentCarryForward(ddl_carry_forward_parent_bal);
//                    if (ddl_carry_forward_parent_bal.equals("Y")) {
//                        ent_FhrdLeaverule.setParentLeaveRatio(new BigDecimal(txt_child_leave_ratio));
//                        if (!txt_max_carry_forward_balance.isEmpty()) {
//                            ent_FhrdLeaverule.setParentCarryForwardMax(new BigDecimal(txt_max_carry_forward_balance));
//                        }
//                    }
//                }
//
//                entFhrdLeaverule_old = fhrdLeaveruleFacade.find(new BigDecimal(ddl_parent_rule));
//                entFhrdLeaverule_old.setUpdateby(ent_Empmst);
//                entFhrdLeaverule_old.setUpdatedt(new Date());
//                entFhrdLeaverule_old.setEndDate(DateTimeUtility.findPrevDateFromDays(cal_start_date, 1));
//            } else {
//                ent_FhrdLeaverule.setParentCarryForward("N");
//            }
            //</editor-fold>

            ent_FhrdLeaverule.setClubWithOther("Y");
            ent_FhrdLeaverule.setVirtualLeave(selected_leavecd.getVirtualLeave());
            ent_FhrdLeaverule.setSrno(1);
            ent_FhrdLeaverule.setCreateby(ent_Empmst);
            ent_FhrdLeaverule.setCreatedt(new Date());
            ent_FhrdLeaverule.setOumUnitSrno(ent_OrgUnitMst);
            ent_FhrdLeaverule.setEarnConfigFlag("N");
//            boolean isTransactionComplete = true;
            boolean isTransactionComplete = customJpaController.createFhrdLeaverule(ent_FhrdLeaverule, entFhrdLeaverule_old);
            if (isTransactionComplete) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully"));
                resetAllComponents();
            } else {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred during saving record"));
            }
        } catch (Exception e) {
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_add_new_action", null, e);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occurred during saving record"));
        }
    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetAllComponents()"> 

    private void resetAllComponents() {
        setTbl_leave_rule();
        RequestContext.getCurrentInstance().execute("$('.not-virtual').hide()");
        notVirtual = false;
        resetEntryComponents();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetEntryComponents()"> 

    private void resetEntryComponents() {
        //General Components
        ddl_rmgm_group = null;
        ddl_leavecd = null;
        ddl_gender_flag = null;

        //Leave Limitation Information
        txt_entitlement = null;
        ddl_entitlement_flag = null;
        txt_freq_per_period = null;
        txt_freq_period = null;
        ddl_freq_period_unit = null;
        ddl_freq_considered_from = null;
        txt_appl_min_leave = null;
        txt_appl_max_leave = null;
        ddl_balance_check_application = null;
        cal_start_date = new Date();


        cal_end_date = null;
        ddl_start_month = "JAN";
        ddl_end_month = "DEC";
        ddl_fix_month = null;
        setCal_wef_date();
        ddl_proportionate_balance = null;
        ddl_leave_laps = null;
        txt_leave_formula = null;
        txt_req_bal_on_retirement = null;

        //Leave's General Properties
        txt_deduct_payable_days = null;
        txt_days_per_leave_bal_comp = null;
        txt_per_payable_leave = null;
        txt_max_accumulation = null;
        txt_earn_leaves_per_period = null;
        txt_earn_leaves_period = null;
        ddl_holiday_embedded = null;
        txt_days_per_leave = null;
        ddl_carry_forward = null;
        txt_carry_forward_max = null;
        ddl_carry_forward_remaining = null;
        ddl_cont_due_carry_forward = null;
        txt_cont_due_carry_forward_max = null;
        ddl_cont_due_carry_forward_remaining = null;
        txt_remarks = null;
        ddl_halfday_applicable = null;


        // Encashment
        chk_encash = false;
        txt_min_encash_leave = null;
        txt_max_encash_leave = null;
        txt_freq_per_period_enc = null;
        txt_freq_period_enc = null;
        ddl_freq_period_unit_enc = null;
        ddl_auto_encash = null;
        ddl_auto_encash = null;
        //Eligibility
        chk_eligibility = false;
        txt_eligibility_period = null;
        ddl_eligibility_considered_from = null;
        ddl_eligibility_unit = null;
        //Parent Detail
//        ddl_parent_rule = null;
//        ddl_carry_forward_parent_bal = null;
//        txt_max_carry_forward_balance = null;
//        ddl_parent_carry_forward_remaining = null;
//        txt_child_leave_ratio = null;

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Apply End Date">

    public void btn_apply_date_action(ActionEvent event) {
        cal_apply_end_date_min = DateTimeUtility.ChangeDateFormat(new Date(), null);
    }
    FhrdLeaverule entFhrdLeaverule_edit;

    public FhrdLeaverule getEntFhrdLeaverule_edit() {
        return entFhrdLeaverule_edit;
    }

    public void setEntFhrdLeaverule_edit(FhrdLeaverule entFhrdLeaverule_edit) {
        this.entFhrdLeaverule_edit = entFhrdLeaverule_edit;
    }
    Date cal_apply_end_date;
    String cal_apply_end_date_min;

    public String getCal_apply_end_date_min() {
        return cal_apply_end_date_min;
    }

    public void setCal_apply_end_date_min(String cal_apply_end_date_min) {
        this.cal_apply_end_date_min = cal_apply_end_date_min;
    }

    public Date getCal_apply_end_date() {
        return cal_apply_end_date;
    }

    public void setCal_apply_end_date(Date cal_apply_end_date) {
        this.cal_apply_end_date = cal_apply_end_date;
    }

    public void btn_apply_end_date_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        BigDecimal v_lr_srg_key = entFhrdLeaverule_edit.getLrSrgKey();
        FhrdLeaverule entFhrdLeaverule = fhrdLeaveruleFacade.find(v_lr_srg_key);
        entFhrdLeaverule.setEndDate(cal_apply_end_date);
        entFhrdLeaverule.setUpdateby(globalData.getEnt_login_user());
        entFhrdLeaverule.setUpdatedt(new Date());
        try {
            fhrdLeaveruleFacade.edit(entFhrdLeaverule);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record updated Successfully"));
            setTbl_leave_rule();
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some error occured during updating Rule"));
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_apply_end_date_clicked", null, e);
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_reset_clicked"> 

    public void btn_reset_clicked(ActionEvent event) {
        resetEntryComponents();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setTbl_leave_rule();
        setDdl_rmgm_group();
        setDdl_leavecd_options();
//        setDdl_parent_rule();
        ddl_entitlement_flag = null;
        cal_start_date = new Date();
        cal_wef_date = new Date();
        cal_end_date_min = DateTimeUtility.ChangeDateFormat(cal_start_date, null);
        ddl_start_month = "JAN";
        ddl_end_month = "DEC";
        setLst_gender_options();
        setCal_wef_date();
        setDdl_cont_due_carry_forward_rule_options();
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    //</editor-fold>
}
