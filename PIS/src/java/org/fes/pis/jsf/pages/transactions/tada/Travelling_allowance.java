/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.transactions.tada;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.db.fes_project.pack.Ftas_Pack;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.*;
import org.fes.pis.jsf.GlobalResources;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.jsf.selectableDataModels.SD_FhrdEmpmst;
import org.fes.pis.jsf.selectableDataModels.SD_FtasAbsentees;
import org.fes.pis.sessions.FhrdEmpleavebalFacadeLocal;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;
import org.fes.pis.sessions.FtasAbsenteesFacadeLocal;
import org.fes.pis.sessions.FtasTadaConvMstFacade;
import org.joda.time.DateTime;
import org.primefaces.context.RequestContext;

/**
 *
 * @author anu
 */
@ManagedBean
@ViewScoped
public class Travelling_allowance implements Serializable {

    @EJB
    private FtasTadaConvMstFacade ftasTadaConvMstFacade;
    @EJB
    private FtasAbsenteesFacadeLocal ftasAbsenteesFacade;
    @EJB
    private FhrdEmpleavebalFacadeLocal fhrdEmpleavebalFacade;
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    Ou_Pack ou_Pack = new Ou_Pack();
    Ftas_Pack ftas_Pack = new Ftas_Pack();
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="TA/DA Bill basic inputs settings">
    //<editor-fold defaultstate="collapsed" desc="selectedTadaBill">
    FtasTadaBillMst selectedTadaBill;

    public FtasTadaBillMst getSelectedTadaBill() {
        return selectedTadaBill;
    }

    public void setSelectedTadaBill(FtasTadaBillMst selectedTadaBill) {
        this.selectedTadaBill = selectedTadaBill;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="User Selection Methods">
    //<editor-fold defaultstate="collapsed" desc="txt_user_id">
    String txt_user_id;
    String err_txt_user_id;

    public String getTxt_user_id() {
        return txt_user_id;
    }

    public void setTxt_user_id(String txt_user_id) {
        this.txt_user_id = txt_user_id;
    }

    public String getErr_txt_user_id() {
        return err_txt_user_id;
    }

    public void setErr_txt_user_id(String err_txt_user_id) {
        this.err_txt_user_id = err_txt_user_id;
    }

    public void txt_user_id_changed(ValueChangeEvent event) {
        txt_user_id = event.getNewValue().toString();
        err_txt_user_id = null;
        txt_user_id_changed();
    }

    private void txt_user_id_changed() {
        selectedUser = null;
        if (txt_user_id != null && txt_user_id.equals("") != true) {
            selectedUser = fhrdEmpmstFacade.find(Integer.valueOf(txt_user_id));
            if (selectedUser == null) {
                err_txt_user_id = " (User Id is invalid)";
            }
        }
        userSelected();
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="selectedUser">
    FhrdEmpmst selectedUser;

    public FhrdEmpmst getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(FhrdEmpmst selectedUser) {
        this.selectedUser = selectedUser;

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="userSelectionValid">
    boolean userSelectionValid;

    public boolean isUserSelectionValid() {
        return userSelectionValid;
    }

    public void setUserSelectionValid(boolean userSelectionValid) {
        this.userSelectionValid = userSelectionValid;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="userSelected">

    private void userSelected() {
        userSelectionValid = false;
        if (selectedUser != null) {
            txt_user_id = selectedUser.getUserId().toString();
            if (!getPIS_GlobalSettings().getSysetemUser_Fhrd() && !globalData.getUser_id().equals(selectedUser.getUserId())) {
                if (!fhrdEmpmstFacade.hasAccessSpanO(globalData.getUser_id(), selectedUser.getPostingOumUnitSrno().getOumUnitSrno())) {
                    err_txt_user_id = " (You do not have privilege to fill the travelling allowance for selected user)";
                }
            } else if ((selectedUser.getOrgRelieveDate() != null || selectedUser.getTerminationDate() != null)
                    && (!fhrdEmpleavebalFacade.isUserTransactionPending(selectedUser.getUserId()))) {
                err_txt_user_id = " (This employee has relieved/terminated the organisation)";
            }
            if (err_txt_user_id == null) {
                userSelectionValid = true;
            }
        } else {
            //err_txt_user_id = " (Invalid User Id)";
            txt_user_id = null;
        }
        resetTrainingAllowance();
    }
    //</editor-fold>    
    ///<editor-fold defaultstate="collapsed" desc="User Selectable List Settings">
    //<editor-fold defaultstate="collapsed" desc="selectedUserFromList">
    FhrdEmpmst selectedUserFromList;

    public FhrdEmpmst getSelectedUserFromList() {
        return selectedUserFromList;
    }

    public void setSelectedUserFromList(FhrdEmpmst selectedUserFromList) {
        this.selectedUserFromList = selectedUserFromList;
        err_txt_user_id = null;
        if (selectedUserFromList != null) {
            selectedUser = selectedUserFromList;
            userSelected();
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lst_employee_selectable">
    SD_FhrdEmpmst lst_employee_selectable;

    public SD_FhrdEmpmst getLst_employee_selectable() {
        return lst_employee_selectable;
    }

    public void setLst_employee_selectable(SD_FhrdEmpmst lst_employee_selectable) {
        this.lst_employee_selectable = lst_employee_selectable;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lnk_view_emplist">
    public void lnk_view_emplist(ActionEvent event) {
        boolean isSysUser = getPIS_GlobalSettings().getSysetemUser_Fhrd();
        lst_employee_selectable = new SD_FhrdEmpmst(fhrdEmpmstFacade.findAll(globalData.getWorking_ou(), false, false, false, null, isSysUser ? null : globalData.getUser_id(), null, true, true, false, null));
    }
    //</editor-fold>
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Tour/Training Selection Methods">
    //<editor-fold defaultstate="collapsed" desc="selectedTourTraining">
    FtasAbsentees selectedTourTraining;

    public FtasAbsentees getSelectedTourTraining() {
        return selectedTourTraining;
    }

    public void setSelectedTourTraining(FtasAbsentees selectedTourTraining) {
        this.selectedTourTraining = selectedTourTraining;
        resetFareDetail();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_tour_training_selection">
    SD_FtasAbsentees tbl_tour_training_selection;

    public SD_FtasAbsentees getTbl_tour_training_selection() {
        return tbl_tour_training_selection;
    }

    public void setTbl_tour_training_selection(SD_FtasAbsentees tbl_tour_training_selection) {
        this.tbl_tour_training_selection = tbl_tour_training_selection;
    }

    private void setTbl_tour_training_selection() {
        if (selectedTadaBill.getUserId() != null) {
            tbl_tour_training_selection = new SD_FtasAbsentees(ftasAbsenteesFacade.findAllForTABill(selectedTadaBill.getUserId().getUserId()));
        } else {
            tbl_tour_training_selection = null;
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="showSelectTourDialog">

    public void showSelectTourDialog(ActionEvent event) {
        RequestContext.getCurrentInstance().execute("pw_dlg_tbl_tour_training_selection.show()");
    }
    //</editor-fold>
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Common Methods, Inputs & Parameters">
    //<editor-fold defaultstate="collapsed" desc="cal_tour_min_max_date">
    String cal_tour_min_date;
    String cal_tour_max_date;

    public String getCal_tour_max_date() {
        return cal_tour_max_date;
    }

    public void setCal_tour_max_date(String cal_tour_max_date) {
        this.cal_tour_max_date = cal_tour_max_date;
    }

    public String getCal_tour_min_date() {
        return cal_tour_min_date;
    }

    public void setCal_tour_min_date(String cal_tour_min_date) {
        this.cal_tour_min_date = cal_tour_min_date;
    }

    private void setCal_tour_min_max_date() {
        if (selectedTourTraining != null) {
            cal_tour_min_date = DateTimeUtility.ChangeDateFormat(selectedTourTraining.getFromDate(), "dd-MMM-yyyy HH:mm");
            cal_tour_max_date = DateTimeUtility.ChangeDateFormat(new DateTime(selectedTourTraining.getToDate()).plusDays(1).minusMinutes(1).toDate(), "dd-MMM-yyyy HH:mm");
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_conv_mst_options">
    List<FtasTadaConvMst> ddl_conv_mst_options;

    public List<FtasTadaConvMst> getDdl_conv_mst_options() {
        return ddl_conv_mst_options;
    }

    public void setDdl_conv_mst_options(List<FtasTadaConvMst> ddl_conv_mst_options) {
        this.ddl_conv_mst_options = ddl_conv_mst_options;
    }

    private void setDdl_conv_mst_options() {
        ddl_conv_mst_options = ftasTadaConvMstFacade.findAll(globalData.getWorking_ou(), true);
        ddl_conv_mst_options.add(0, new FtasTadaConvMst());
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Fare Details Settings">
    //<editor-fold defaultstate="collapsed" desc="tbl_fare_detail">
    List<FtasTadaFareDtl> tbl_fare_detail;

    public List<FtasTadaFareDtl> getTbl_fare_detail() {
        return tbl_fare_detail;
    }

    public void setTbl_fare_detail(List<FtasTadaFareDtl> tbl_fare_detail) {
        this.tbl_fare_detail = tbl_fare_detail;
    }

    private void setTbl_fare_detail() {
        tbl_fare_detail = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="removeFareDetail">

    public void removeFareDetail(FtasTadaFareDtl ftasTadaFareDtl) {
        int index = tbl_fare_detail.indexOf(ftasTadaFareDtl);
        tbl_fare_detail.remove(index);
        for (int i = index; i < tbl_fare_detail.size(); i++) {
            tbl_fare_detail.get(i).setFtasTadaFareDtlPK(new FtasTadaFareDtlPK(selectedTadaBill.getTbmSrgKey(), i + 1));
        }

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="addNewFare">

    public void addNewFare(ActionEvent event) {
        if (tbl_fare_detail == null) {
            tbl_fare_detail = new ArrayList<FtasTadaFareDtl>();
        }
        tbl_fare_detail.add(new FtasTadaFareDtl(selectedTadaBill.getTbmSrgKey(), tbl_fare_detail.size() + 1));
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetFareDetail">

    private void resetFareDetail() {
        tbl_fare_detail = null;
        setCal_tour_min_max_date();
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Conveyance Charges Settings">
    //<editor-fold defaultstate="collapsed" desc="tbl_conv_charges">
    List<FtasTadaConvExpDtl> tbl_conv_charges;

    public List<FtasTadaConvExpDtl> getTbl_conv_charges() {
        return tbl_conv_charges;
    }

    public void setTbl_conv_charges(List<FtasTadaConvExpDtl> tbl_conv_charges) {
        this.tbl_conv_charges = tbl_conv_charges;
    }

    private void setTbl_conv_charges() {
        tbl_conv_charges = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="addNewConvCharge">

    public void addNewConvCharge(ActionEvent event) {
        if (tbl_conv_charges == null) {
            tbl_conv_charges = new ArrayList<FtasTadaConvExpDtl>();
        }
        tbl_conv_charges.add(new FtasTadaConvExpDtl(selectedTadaBill.getTbmSrgKey(), tbl_conv_charges.size() + 1));
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="removeConvCharge">

    public void removeConvCharge(FtasTadaConvExpDtl ftasTadaConvExpDtl) {
        int index = tbl_conv_charges.indexOf(ftasTadaConvExpDtl);
        tbl_conv_charges.remove(index);
        for (int i = index; i < tbl_conv_charges.size(); i++) {
            tbl_conv_charges.get(i).setFtasTadaConvExpDtlPK(new FtasTadaConvExpDtlPK(selectedTadaBill.getTbmSrgKey(), i + 1));
        }

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetFareDetail">

    private void resetConvCharges() {
        tbl_conv_charges = null;
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Other Details Settings">
    //<editor-fold defaultstate="collapsed" desc="tbl_other_details">
    List<FtasTadaConvExpDtl> tbl_other_details;

    public List<FtasTadaConvExpDtl> getTbl_other_details() {
        return tbl_other_details;
    }

    public void setTbl_other_details(List<FtasTadaConvExpDtl> tbl_other_details) {
        this.tbl_other_details = tbl_other_details;
    }

    private void setTbl_other_details() {
        tbl_other_details = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="addNewOtherDetail">

    public void addNewOtherDetail(ActionEvent event) {
        if (tbl_other_details == null) {
            tbl_other_details = new ArrayList<FtasTadaConvExpDtl>();
        }
        tbl_other_details.add(new FtasTadaConvExpDtl(selectedTadaBill.getTbmSrgKey(), tbl_other_details.size() + 1));
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="removeOtherDetail">

    public void removeOtherDetail(FtasTadaConvExpDtl ftasTadaConvExpDtl) {
        int index = tbl_other_details.indexOf(ftasTadaConvExpDtl);
        tbl_other_details.remove(index);
        for (int i = index; i < tbl_other_details.size(); i++) {
            tbl_other_details.get(i).setFtasTadaConvExpDtlPK(new FtasTadaConvExpDtlPK(selectedTadaBill.getTbmSrgKey(), i + 1));
        }

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetFareDetail">

    private void resetOtherDetails() {
        tbl_other_details = null;
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetTrainingAllowance">

    private void resetTrainingAllowance() {
        selectedTadaBill = new FtasTadaBillMst(BigDecimal.ZERO);
        selectedTadaBill.setUserId(selectedUser);
        selectedTourTraining = null;
        setTbl_tour_training_selection();
        setTbl_fare_detail();
        resetConvCharges();
        resetOtherDetails();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setDdl_conv_mst_options();
        resetTrainingAllowance();

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Default Constructor & Other Beans Methods">
    // <editor-fold defaultstate="collapsed" desc="default constructor">

    /**
     * Creates a new instance of PIS_Daily_Attendance
     */
    public Travelling_allowance() {
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Resources">

    protected GlobalResources getGlobalResources() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalResources) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalResources");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
    //</editor-fold>
}
