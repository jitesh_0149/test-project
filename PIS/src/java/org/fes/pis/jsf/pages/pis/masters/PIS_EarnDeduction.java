package org.fes.pis.jsf.pages.pis.masters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.*;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEarndednFacadeLocal;
import org.fes.pis.sessions.FhrdEarndednmstFacadeLocal;
import org.fes.pis.sessions.FhrdRuleManagGroupMstFacadeLocal;
import org.fes.pis.sessions.FtasLeavemstFacadeLocal;
import org.primefaces.event.SelectEvent;

@ManagedBean(name = "PIS_EarnDeduction")
@ViewScoped
public class PIS_EarnDeduction implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="EJB Declaration and Constructor">
    System_Properties system_Properties = new System_Properties();
    @EJB
    private FtasLeavemstFacadeLocal ftasLeavemstFacade;
    @EJB
    private FhrdEarndednmstFacadeLocal fhrdEarndednmstFacade;
    @EJB
    private FhrdEarndednFacadeLocal fhrdEarndednFacade;
    @EJB
    private FhrdRuleManagGroupMstFacadeLocal fhrdRuleManagGroupMstFacade;
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="Master Table Settings">
    // <editor-fold defaultstate="collapsed" desc="tbl_earndedn_mst">
    List<FhrdEarndedn> tbl_earndedn_mst;
    List<FhrdEarndedn> tbl_earndedn_mst_filtered;

    public List<FhrdEarndedn> getTbl_earndedn_mst_filtered() {
        return tbl_earndedn_mst_filtered;
    }

    public void setTbl_earndedn_mst_filtered(List<FhrdEarndedn> tbl_earndedn_mst_filtered) {
        this.tbl_earndedn_mst_filtered = tbl_earndedn_mst_filtered;
    }

    public List<FhrdEarndedn> getTbl_earndedn_mst() {
        return tbl_earndedn_mst;
    }

    public void setTbl_earndedn_mst(List<FhrdEarndedn> tbl_earndedn_mst) {
        this.tbl_earndedn_mst = tbl_earndedn_mst;
    }

    public void setTbl_earndedn_mst() {
        List<FhrdEarndedn> lst_earndedn_mst = fhrdEarndednFacade.findAll(globalData.getWorking_ou(), null, null);
        Integer n = lst_earndedn_mst.size();
//        for (int i = 0; i < n; i++) {
//            if (lst_earndedn_mst.get(i).getEarndednType().equalsIgnoreCase("o")) {
//                lst_earndedn_mst.get(i).setEarndednType("Occasional");
//            } else if (lst_earndedn_mst.get(i).getEarndednType().equalsIgnoreCase("r")) {
//                lst_earndedn_mst.get(i).setEarndednType("Regular");
//            }
//            if (lst_earndedn_mst.get(i).getFormulaAmtFlag() == null) {
//            } else {
//                if (lst_earndedn_mst.get(i).getFormulaAmtFlag().equalsIgnoreCase("f")) {
//                    lst_earndedn_mst.get(i).setFormulaAmtFlag("Formula");
//                } else if (lst_earndedn_mst.get(i).getFormulaAmtFlag().equalsIgnoreCase("v")) {
//                    lst_earndedn_mst.get(i).setFormulaAmtFlag("Amount");
//                } else if (lst_earndedn_mst.get(i).getFormulaAmtFlag().equalsIgnoreCase("b")) {
//                    lst_earndedn_mst.get(i).setFormulaAmtFlag("Block");
//                }
//            }
//        }
        tbl_earndedn_mst = lst_earndedn_mst;
    }
    // </editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Formula Generation Settings">
    // <editor-fold defaultstate="collapsed" desc="Dialogbox ">// <editor-fold defaultstate="collapsed" desc="dialog binding attributes">
    String ddl_generate_for;
    String ddl_maths;
    String txt_str_box;

    public String getTxt_str_box() {
        return txt_str_box;
    }

    public void setTxt_str_box(String txt_str_box) {
        this.txt_str_box = txt_str_box;
    }

    public String getDdl_maths() {
        return ddl_maths;
    }

    public void setDdl_maths(String ddl_maths) {
        this.ddl_maths = ddl_maths;
    }

    public String getDdl_generate_for() {
        return ddl_generate_for;
    }

    public void setDdl_generate_for(String ddl_generate_for) {
        this.ddl_generate_for = ddl_generate_for;
    }// </editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="ddl_variables">
    String ddl_payable_variables;

    public String getDdl_payable_variables() {
        return ddl_payable_variables;
    }

    public void setDdl_payable_variables(String ddl_payable_variables) {
        this.ddl_payable_variables = ddl_payable_variables;
    }
    //</editor-fold>       
    // <editor-fold defaultstate="collapsed" desc="ddl_function">
    String ddl_function;
    List<SelectItem> ddl_function_options;

    public String getDdl_function() {
        return ddl_function;
    }

    public void setDdl_function(String ddl_function) {
        this.ddl_function = ddl_function;
    }

    public List<SelectItem> getDdl_function_options() {
        return ddl_function_options;
    }

    public void setDdl_function_options(List<SelectItem> ddl_function_options) {
        this.ddl_function_options = ddl_function_options;
    }

    public void setDdl_function() {
        List<SelectItem> lst_fun_option = new ArrayList<SelectItem>();
        if (ddl_function_options == null) {
            SelectItem s = new SelectItem(null, "--- Select ---");
            lst_fun_option.add(s);
            lst_fun_option.add(new SelectItem("ABS"));
            lst_fun_option.add(new SelectItem("ACOS"));
            lst_fun_option.add(new SelectItem("ADD_MONTHS"));
            lst_fun_option.add(new SelectItem("ATAN"));
            lst_fun_option.add(new SelectItem("ATAN2"));
            lst_fun_option.add(new SelectItem("CEIL"));
            lst_fun_option.add(new SelectItem("COS"));
            lst_fun_option.add(new SelectItem("COSH"));
            lst_fun_option.add(new SelectItem("EXP"));
            lst_fun_option.add(new SelectItem("FLOOR"));
            lst_fun_option.add(new SelectItem("LN"));
            lst_fun_option.add(new SelectItem("LOG"));
            lst_fun_option.add(new SelectItem("MOD"));
            lst_fun_option.add(new SelectItem("POWER"));
            lst_fun_option.add(new SelectItem("ROUND"));
            lst_fun_option.add(new SelectItem("SIGN"));
            lst_fun_option.add(new SelectItem("SIN"));
            lst_fun_option.add(new SelectItem("SINH"));
            lst_fun_option.add(new SelectItem("SQRT"));
            lst_fun_option.add(new SelectItem("TAN"));
            lst_fun_option.add(new SelectItem("TANH"));
            lst_fun_option.add(new SelectItem("TRUNC"));
        }
        ddl_function_options = lst_fun_option;
        ddl_function = ddl_function_options.get(0).toString();
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ddl_leave_code_list">
    String ddl_leave_code_list;
    List<SelectItem> ddl_leave_code_list_options;

    public String getDdl_leave_code_list() {
        return ddl_leave_code_list;
    }

    public void setDdl_leave_code_list(String ddl_leave_code_list) {
        this.ddl_leave_code_list = ddl_leave_code_list;
    }

    public List<SelectItem> getDdl_leave_code_list_options() {
        return ddl_leave_code_list_options;
    }

    public void setDdl_leave_code_list_options(List<SelectItem> ddl_leave_code_list_options) {
        this.ddl_leave_code_list_options = ddl_leave_code_list_options;
    }

    public void setDdl_leavecode_lst() {
        List<SelectItem> lst_leave_option = new ArrayList<SelectItem>();
        List<FtasLeavemst> lst_e;
        if (ddl_leave_code_list_options == null) {
            lst_e = ftasLeavemstFacade.findAll(globalData.getWorking_ou());
            if (lst_e.isEmpty()) {
                SelectItem s = new SelectItem("--- Not Available ---");
                lst_leave_option.add(s);
            } else {
                SelectItem s = new SelectItem(null, "--- Select ---");
                lst_leave_option.add(s);
                Integer n = lst_e.size();
                for (int i = 0; i < n; i++) {
                    s = new SelectItem(lst_e.get(i).getShortName());
                    lst_leave_option.add(s);
                }

            }
        }
        ddl_leave_code_list_options = lst_leave_option;
        ddl_leave_code_list = ddl_leave_code_list_options.get(0).toString();
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ddl_ed_code_list">
    String ddl_ed_code_list;
    List<SelectItem> ddl_ed_code_list_options;

    public String getDdl_ed_code_list() {
        return ddl_ed_code_list;
    }

    public void setDdl_ed_code_list(String ddl_ed_code_list) {
        this.ddl_ed_code_list = ddl_ed_code_list;
    }

    public List<SelectItem> getDdl_ed_code_list_options() {
        return ddl_ed_code_list_options;
    }

    public void setDdl_ed_code_list_options(List<SelectItem> ddl_ed_code_list_options) {
        this.ddl_ed_code_list_options = ddl_ed_code_list_options;
    }

    public void setDdl_edcode_lst() {
        List<SelectItem> lst_ed_option = new ArrayList<SelectItem>();
        if (ddl_ed_code_list_options == null) {
            List<FhrdEarndednmst> lst_e = fhrdEarndednmstFacade.findAll(globalData.getWorking_ou());
            if (lst_e.isEmpty()) {
                SelectItem s = new SelectItem("--- Not Available ---");
                lst_ed_option.add(s);
            } else {
                SelectItem s = new SelectItem(null, "--- Select ---");
                lst_ed_option.add(s);
                Integer n = lst_e.size();
                for (int i = 0; i < n; i++) {
                    s = new SelectItem(lst_e.get(i).getEarndedncd());
                    lst_ed_option.add(s);
                }
            }
        }
        ddl_ed_code_list_options = lst_ed_option;
        ddl_ed_code_list = ddl_ed_code_list_options.get(0).toString();
    }
    // </editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="New ED Rule Settings">
    //<editor-fold defaultstate="collapsed" desc="Objects & Declarations">
    // <editor-fold defaultstate="collapsed" desc="Date">
    //<editor-fold defaultstate="collapsed" desc="cal_start_date">
    Date cal_start_date;
    String cal_start_date_min;
    String cal_start_date_max;

    public Date getCal_start_date() {
        return cal_start_date;
    }

    public void setCal_start_date(Date cal_start_date) {
        this.cal_start_date = cal_start_date;
    }

    public String getCal_start_date_max() {
        return cal_start_date_max;
    }

    public void setCal_start_date_max(String cal_start_date_max) {
        this.cal_start_date_max = cal_start_date_max;
    }

    public String getCal_start_date_min() {
        return cal_start_date_min;
    }

    public void setCal_start_date_min(String cal_start_date_min) {
        this.cal_start_date_min = cal_start_date_min;
    }

    public void cal_start_date_changed(SelectEvent event) {
        cal_start_date = (Date) event.getObject();
        if (cal_end_date != null) {
            if (cal_end_date.before(cal_start_date)) {
                cal_end_date = cal_start_date;
            }
        }
        cal_end_date_min = DateTimeUtility.ChangeDateFormat(cal_start_date, null);
        setDdl_replaced_rule();
    }

    private void setCal_start_end_date() {
        Date v_min = null, v_max = null;
        if (ddl_rmgm_group != null) {
            FhrdRuleManagGroupMst selected_GroupMst = fhrdRuleManagGroupMstFacade.find(new BigDecimal(ddl_rmgm_group));
            v_min = selected_GroupMst.getStartDate();
            v_max = selected_GroupMst.getEndDate();
        }
        if (ddl_ed_code != null) {
            FhrdEarndednmst selected_Earndednmst = fhrdEarndednmstFacade.find(new BigDecimal(ddl_ed_code));
            v_min = DateTimeUtility.findBiggestDate(v_min, selected_Earndednmst.getStartDate());
            v_max = DateTimeUtility.findSmallestDate(v_max, selected_Earndednmst.getEndDate());
        }
        cal_start_date = v_min;
        cal_end_date = v_max;
        cal_start_date_min = cal_end_date_min = DateTimeUtility.ChangeDateFormat(v_min, null);
        cal_start_date_max = cal_end_date_max = DateTimeUtility.ChangeDateFormat(v_max, null);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_end_date">
    Date cal_end_date;
    String cal_end_date_min;
    String cal_end_date_max;

    public Date getCal_end_date() {
        return cal_end_date;
    }

    public void setCal_end_date(Date cal_end_date) {
        this.cal_end_date = cal_end_date;
    }

    public String getCal_end_date_min() {
        return cal_end_date_min;
    }

    public void setCal_end_date_min(String cal_end_date_min) {
        this.cal_end_date_min = cal_end_date_min;
    }

    public String getCal_end_date_max() {
        return cal_end_date_max;
    }

    public void setCal_end_date_max(String cal_end_date_max) {
        this.cal_end_date_max = cal_end_date_max;
    }

    public void cal_end_date_changed(SelectEvent event) {
        cal_end_date = (Date) event.getObject();
        setDdl_replaced_rule();
    }

    public void resetEndDate(ActionEvent event) {
        cal_end_date = null;
        setDdl_replaced_rule();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_wef_date">
    Date cal_wef_date;

    public Date getCal_wef_date() {
        return cal_wef_date;
    }

    public void setCal_wef_date(Date cal_wef_date) {
        this.cal_wef_date = cal_wef_date;
    }
    //</editor-fold>
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_formula_amt_flag">
    String ddl_formula_amt_flag;

    public String getDdl_formula_amt_flag() {
        return ddl_formula_amt_flag;
    }

    public void setDdl_formula_amt_flag(String ddl_formula_amt_flag) {
        this.ddl_formula_amt_flag = ddl_formula_amt_flag;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_amount">
    String txt_amount;

    public String getTxt_amount() {
        return txt_amount;
    }

    public void setTxt_amount(String txt_amount) {
        this.txt_amount = txt_amount;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_formula_amt_value">
    String txt_formula_amt_value;

    public String getTxt_formula_amt_value() {
        return txt_formula_amt_value;
    }

    public void setTxt_formula_amt_value(String txt_formula_amt_value) {
        this.txt_formula_amt_value = txt_formula_amt_value;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_rmgm_group">
    String ddl_rmgm_group;
    List<SelectItem> ddl_rmgm_group_options;

    public String getDdl_rmgm_group() {
        return ddl_rmgm_group;
    }

    public void setDdl_rmgm_group(String ddl_rmgm_group) {
        this.ddl_rmgm_group = ddl_rmgm_group;
    }

    public List<SelectItem> getDdl_rmgm_group_options() {
        return ddl_rmgm_group_options;
    }

    public void setDdl_rmgm_group_options(List<SelectItem> ddl_rmgm_group_options) {
        this.ddl_rmgm_group_options = ddl_rmgm_group_options;
    }

    public void setDdl_rmgm_group() {
        ddl_rmgm_group_options = new ArrayList<SelectItem>();
        List<FhrdRuleManagGroupMst> lst_e = fhrdRuleManagGroupMstFacade.findAll(globalData.getWorking_ou(), null, "ACCOUNT", null, null, null, false);
        if (lst_e.isEmpty()) {
            ddl_rmgm_group_options.add(new SelectItem(null, "--- Not Available ---"));
        } else {
            ddl_rmgm_group_options.add(new SelectItem(null, "--- Select ---"));
            Integer n = lst_e.size();
            for (int i = 0; i < n; i++) {
                ddl_rmgm_group_options.add(new SelectItem(lst_e.get(i).getRmgmSrgKey(), lst_e.get(i).getManagGroupDesc()));
            }
        }
        ddl_rmgm_group = null;
    }

    public void ddl_rmgm_group_changed(ValueChangeEvent event) {
        ddl_rmgm_group = event.getNewValue() == null ? null : event.getNewValue().toString();
        setDdl_ed_code();
        setCal_start_end_date();

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_rule_config_uni">
    String ddl_rule_config_unit;

    public String getDdl_rule_config_unit() {
        return ddl_rule_config_unit;
    }

    public void setDdl_rule_config_unit(String ddl_rule_config_unit) {
        this.ddl_rule_config_unit = ddl_rule_config_unit;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="chk dependent fields">
    boolean chk_desig_dependent;

    public boolean isChk_desig_dependent() {
        return chk_desig_dependent;
    }

    public void setChk_desig_dependent(boolean chk_desig_dependent) {
        this.chk_desig_dependent = chk_desig_dependent;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_replaced_rule">
    FhrdEarndedn ddl_replaced_rule;
    List<FhrdEarndedn> ddl_replaced_rule_options;

    public FhrdEarndedn getDdl_replaced_rule() {
        return ddl_replaced_rule;
    }

    public void setDdl_replaced_rule(FhrdEarndedn ddl_replaced_rule) {
        this.ddl_replaced_rule = ddl_replaced_rule;
    }

    public List<FhrdEarndedn> getDdl_replaced_rule_options() {
        return ddl_replaced_rule_options;
    }

    public void setDdl_replaced_rule_options(List<FhrdEarndedn> ddl_replaced_rule_options) {
        this.ddl_replaced_rule_options = ddl_replaced_rule_options;
    }

    public void setDdl_replaced_rule() {
        ddl_replaced_rule_options = new ArrayList<FhrdEarndedn>();
        List<FhrdEarndedn> lst_e = fhrdEarndednFacade.findAll(globalData.getWorking_ou(), cal_start_date, cal_end_date);
        if (lst_e.isEmpty()) {
//            ddl_replaced_rule_options.add(new SelectItem(null, "--- Not Available ---"));
        } else {
//            ddl_replaced_rule_options.add(new SelectItem(null, "--- Select ---"));
            Integer n = lst_e.size();
            for (int i = 0; i < n; i++) {
                ddl_replaced_rule_options.add(lst_e.get(i));
            }
        }
        ddl_replaced_rule = null;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Earn-Deduction Code">
    String ddl_ed_code;
    List<SelectItem> ddl_ed_code_options;

    public String getDdl_ed_code() {
        return ddl_ed_code;
    }

    public void setDdl_ed_code(String ddl_ed_code) {
        this.ddl_ed_code = ddl_ed_code;
    }

    public List<SelectItem> getDdl_ed_code_options() {
        return ddl_ed_code_options;
    }

    public void setDdl_ed_code_options(List<SelectItem> ddl_ed_code_options) {
        this.ddl_ed_code_options = ddl_ed_code_options;
    }

    public void setDdl_ed_code() {
        ddl_ed_code_options = new ArrayList<SelectItem>();
        if (ddl_rmgm_group != null) {
            FhrdRuleManagGroupMst ent_ManagGroupMst = fhrdRuleManagGroupMstFacade.find(new BigDecimal(ddl_rmgm_group));
            List<FhrdEarndednmst> lst_e = fhrdEarndednmstFacade.findAllforManageGroup(ddl_rmgm_group, ent_ManagGroupMst.getStartDate(), ent_ManagGroupMst.getEndDate());
            if (lst_e.isEmpty()) {
                ddl_ed_code_options.add(new SelectItem(null, "--- Not Available ---"));
            } else {
                ddl_ed_code_options.add(new SelectItem(null, "--- Select ---"));
                Integer n = lst_e.size();
                for (int i = 0; i < n; i++) {
                    ddl_ed_code_options.add(new SelectItem(lst_e.get(i).getEdmSrgKey(), lst_e.get(i).getEarndedncd()));
                }

            }
        } else {
            ddl_ed_code_options.add(new SelectItem(null, "--- Select Rule Group ---"));
        }
        ddl_ed_code = null;
        setCal_start_end_date();

    }

    public void ddl_ed_code_changed(ValueChangeEvent event) {
        ddl_ed_code = event.getNewValue() == null ? null : event.getNewValue().toString();
        setCal_start_end_date();
    }
    // </editor-fold>
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="btn_add_new_action">
    public void btn_add_new_action(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            BigDecimal v_ed_srgkey = getGlobalSettings().getUniqueSrno(null, "FHRD_EARNDEDN");
            FhrdEmpmst ent_Empmst = new FhrdEmpmst(globalData.getUser_id());
            SystOrgUnitMst ent_OrgUnitMst = new SystOrgUnitMst(globalData.getWorking_ou());
            FhrdEarndedn ent_Earndedn = new FhrdEarndedn();
            FhrdRuleManagGroupMst entFhrdRuleManagGroupMst = new FhrdRuleManagGroupMst(new BigDecimal(ddl_rmgm_group));

            ent_Earndedn.setEdSrgKey(v_ed_srgkey);
            ent_Earndedn.setRmgmSrgKey(entFhrdRuleManagGroupMst);
            ent_Earndedn.setEdmAltSrno(1);
            ent_Earndedn.setEarndedncd("1");
            ent_Earndedn.setEarndednSrno(1);
            ent_Earndedn.setEarndednType("1");
            ent_Earndedn.setPrintIf0Flag("1");
            ent_Earndedn.setBasecodeFlag("1");
            ent_Earndedn.setEarndednFlag("1");
            ent_Earndedn.setPrintInPaySlip("1");
            ent_Earndedn.setTaxableFlag("1");
            ent_Earndedn.setStatutoryFlag("1");
            ent_Earndedn.setPayableOnValue("1");
            ent_Earndedn.setFirstEntryFlag("M");
            ent_Earndedn.setDesigDependent("N");
            if (chk_desig_dependent) {
                ent_Earndedn.setDesigDependent("Y");
            }
            ent_Earndedn.setEdmSrgKey(new FhrdEarndednmst(new BigDecimal(ddl_ed_code)));
            ent_Earndedn.setFormulaAmtFlag(ddl_formula_amt_flag);
            if (ddl_formula_amt_flag.equals("F")) {
                ent_Earndedn.setFormulaAmtValue(txt_formula_amt_value);
            } else if (ddl_formula_amt_flag.equals("V")) {
                ent_Earndedn.setFormulaAmtValue(txt_amount);
            } else if (ddl_formula_amt_flag.equals("B")) {
                ent_Earndedn.setFormulaAmtValue("BLOCK");
            }

            //if (ddl_replaced_rule != null) {
            //ent_Earndedn.setReplacedRuleSrno(ddl_replaced_rule.getEdSrgKey());
            //}
            ent_Earndedn.setRuleConfigUnit(ddl_rule_config_unit);
            ent_Earndedn.setStartDate(cal_start_date);
            ent_Earndedn.setEndDate(cal_end_date);
            ent_Earndedn.setWefDate(cal_wef_date);
            ent_Earndedn.setOumUnitSrno(ent_OrgUnitMst);
            ent_Earndedn.setCreateby(ent_Empmst);
            ent_Earndedn.setCreatedt(new Date());
            ent_Earndedn.setEligibilityApplicable("N");
            fhrdEarndednFacade.create(ent_Earndedn);
            resetNewEntryDetail();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully"));
        } catch (Exception ex) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occurred during saving record"));
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_add_new_action", null, ex);
        }

    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetNewEntryDetail()">

    public void resetNewEntryDetail() {
        setTbl_earndedn_mst();
        setDdl_ed_code();
        setDdl_rmgm_group();
        ddl_formula_amt_flag = "F";
        ddl_rule_config_unit = null;
        ddl_formula_amt_flag = "F";
        txt_amount = null;
        txt_formula_amt_value = null;
        cal_start_date = null;
        cal_end_date = null;
        cal_wef_date = null;
        chk_desig_dependent = false;
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Detail Settings">
    //<editor-fold defaultstate="collapsed" desc="Earn Deduction Master">
    //<editor-fold defaultstate="collapsed" desc="ent_Earndednmst">
    FhrdEarndednmst ent_Earndednmst;

    public FhrdEarndednmst getEnt_Earndednmst() {
        return ent_Earndednmst;
    }

    public void setEnt_Earndednmst(FhrdEarndednmst ent_Earndednmst) {
        this.ent_Earndednmst = ent_Earndednmst;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="hyp_view_ed_master_clicked">
    public void hyp_view_ed_master_clicked(ActionEvent event) {
        ent_Earndednmst = fhrdEarndednmstFacade.find(new BigDecimal(ddl_ed_code));
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_view_detail_action">

    public void btn_view_detail_action(ActionEvent event) {
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ent_FhrdEarndedn_detail"> 
    FhrdEarndedn ent_FhrdEarndedn_detail;

    public FhrdEarndedn getEnt_FhrdEarndedn_detail() {
        return ent_FhrdEarndedn_detail;
    }

    public void setEnt_FhrdEarndedn_detail(FhrdEarndedn ent_FhrdEarndedn_detail) {
        this.ent_FhrdEarndedn_detail = ent_FhrdEarndedn_detail;
    }
    //</editor-fold>

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Apply End Date">
    public void btn_apply_date_action(ActionEvent event) {
    }
    FhrdEarndedn entFhrdEdRule_edit;

    public FhrdEarndedn getEntFhrdEdRule_edit() {
        return entFhrdEdRule_edit;
    }

    public void setEntFhrdEdRule_edit(FhrdEarndedn entFhrdEdRule_edit) {
        this.entFhrdEdRule_edit = entFhrdEdRule_edit;
        cal_apply_end_date_min = DateTimeUtility.ChangeDateFormat(entFhrdEdRule_edit.getStartDate(), null);
    }
    Date cal_apply_end_date;
    String cal_apply_end_date_min;

    public Date getCal_apply_end_date() {
        return cal_apply_end_date;
    }

    public void setCal_apply_end_date(Date cal_apply_end_date) {
        this.cal_apply_end_date = cal_apply_end_date;
    }

    public String getCal_apply_end_date_min() {
        return cal_apply_end_date_min;
    }

    public void setCal_apply_end_date_min(String cal_apply_end_date_min) {
        this.cal_apply_end_date_min = cal_apply_end_date_min;
    }

    public void btn_apply_end_date_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        BigDecimal v_ed_srg_key = entFhrdEdRule_edit.getEdSrgKey();
        FhrdEarndedn entFhrdEarndedn = fhrdEarndednFacade.find(v_ed_srg_key);
        entFhrdEarndedn.setEndDate(cal_apply_end_date);
        entFhrdEarndedn.setUpdateby(globalData.getEnt_login_user());
        entFhrdEarndedn.setUpdatedt(new Date());
        try {
            fhrdEarndednFacade.edit(entFhrdEarndedn);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record updated Successfully"));
            setTbl_earndedn_mst();
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some error occured during updating Rule"));
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_apply_end_date_clicked", null, e);
        }
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="init"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setTbl_earndedn_mst();
        setDdl_rmgm_group();
        setDdl_ed_code();
        setDdl_function();
        setDdl_leavecode_lst();
        setDdl_edcode_lst();
        setDdl_replaced_rule();
        chk_desig_dependent = false;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Default Constructor & GlobalSettings">

    //<editor-fold defaultstate="collapsed" desc="Default Constructor">
    public PIS_EarnDeduction() {
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    //</editor-fold>
}
