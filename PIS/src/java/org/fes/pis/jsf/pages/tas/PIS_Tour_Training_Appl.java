package org.fes.pis.jsf.pages.tas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.Database_Output;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.ejb_utilities_pisLocal;
import org.fes.pis.db.fes_project.pack.Ftas_Pack;
import org.fes.pis.entities.*;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.jsf.selectableDataModels.SD_FhrdEmpmst;
import org.fes.pis.jsf.utilities.JSFMessages;
import org.fes.pis.sessions.*;
import org.joda.time.DateTime;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Tour_Training_Appl")
@ViewScoped
public class PIS_Tour_Training_Appl implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="globalData">
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    PIS_GlobalData globalData;
    Ftas_Pack ftas_Pack = new Ftas_Pack();

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="EJB Declaration">
    @EJB
    private FhrdEmpleavebalFacadeLocal fhrdEmpleavebalFacade;
    @EJB
    private FtasTrainingMstFacadeLocal ftasTrainingMstFacade;
    @EJB
    private FtasLeavemstFacadeLocal ftasLeavemstFacade;
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    @EJB
    private FtasAbsenteesFacadeLocal ftasAbsenteesFacade;
    @EJB
    private FtasCategoryMstFacadeLocal ftasCategoryMstFacade;
    @EJB
    private ejb_utilities_pisLocal ejb_utilities_pis;
    // </editor-fold>   
    //<editor-fold defaultstate="collapsed" desc="User Selection Methods">
    ///<editor-fold defaultstate="collapsed" desc="User Selectable List Settings">
    //<editor-fold defaultstate="collapsed" desc="selectedUserFromList">
    FhrdEmpmst selectedUserFromList;

    public FhrdEmpmst getSelectedUserFromList() {
        return selectedUserFromList;
    }

    public void setSelectedUserFromList(FhrdEmpmst selectedUserFromList) {
        this.selectedUserFromList = selectedUserFromList;
        err_txt_user_id = null;
        if (selectedUserFromList != null) {
            selectedUser = fhrdEmpmstFacade.find(selectedUserFromList.getUserId());
            userSelected();
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lst_employee_selectable">
    SD_FhrdEmpmst lst_employee_selectable;

    public SD_FhrdEmpmst getLst_employee_selectable() {
        return lst_employee_selectable;
    }

    public void setLst_employee_selectable(SD_FhrdEmpmst lst_employee_selectable) {
        this.lst_employee_selectable = lst_employee_selectable;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lnk_view_emplist">
    public void lnk_view_emplist(ActionEvent event) {
        boolean isSysUser = getPIS_GlobalSettings().getSysetemUser_Fhrd();
        lst_employee_selectable = new SD_FhrdEmpmst(fhrdEmpmstFacade.findAll(globalData.getWorking_ou(), false, false, false, null, isSysUser ? null : globalData.getUser_id(), null, true, true, false, null));
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="User Selection">
    //<editor-fold defaultstate="collapsed" desc="txt_user_id">
    String txt_user_id;
    String err_txt_user_id;

    public String getErr_txt_user_id() {
        return err_txt_user_id;
    }

    public void setErr_txt_user_id(String err_txt_user_id) {
        this.err_txt_user_id = err_txt_user_id;
    }

    public String getTxt_user_id() {
        return txt_user_id;
    }

    public void setTxt_user_id(String txt_user_id) {
        this.txt_user_id = txt_user_id;
    }
    // <editor-fold defaultstate="collapsed" desc="txt_user_id_changed">

    public void txt_user_id_changed(ValueChangeEvent event) {
        txt_user_id = event.getNewValue().toString();
        err_txt_user_id = null;
        txt_user_id_changed();
    }

    private void txt_user_id_changed() {
        selectedUser = null;
        if (txt_user_id != null && txt_user_id.equals("") != true) {
            selectedUser = fhrdEmpmstFacade.find(Integer.valueOf(txt_user_id));
            if (selectedUser == null) {
                err_txt_user_id = " (User Id is invalid)";
            }
        }
        userSelected();
    }
    // </editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="User Selection Methods">
    //<editor-fold defaultstate="collapsed" desc="selectedUser">
    FhrdEmpmst selectedUser;

    public FhrdEmpmst getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(FhrdEmpmst selectedUser) {
        this.selectedUser = selectedUser;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="userSelectionValid">
    boolean userSelectionValid;

    public boolean isUserSelectionValid() {
        return userSelectionValid;
    }

    public void setUserSelectionValid(boolean userSelectionValid) {
        this.userSelectionValid = userSelectionValid;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="userSelected">

    private void userSelected() {
        userSelectionValid = false;
        if (selectedUser != null) {
            txt_user_id = selectedUser.getUserId().toString();
            if (!getPIS_GlobalSettings().getSysetemUser_Fhrd() && !globalData.getUser_id().equals(selectedUser.getUserId())) {
                if (!fhrdEmpmstFacade.hasAccessSpanO(globalData.getUser_id(), selectedUser.getPostingOumUnitSrno().getOumUnitSrno())) {
                    err_txt_user_id = " (You do not have privilege to mark tour/training of selected user)";
                }
            } else if (!fhrdEmpleavebalFacade.isUserTransactionPending(selectedUser.getUserId())) {
                if (selectedUser.getOrgRelieveDate() != null || selectedUser.getTerminationDate() != null) {
                    err_txt_user_id = " (This employee has relieved/terminated the organisation)";
                } else {
                    err_txt_user_id = " (Either contract is not given or not does not have any active contract)";
                }
            }
            if (err_txt_user_id == null) {
                userSelectionValid = true;
            }
        } else {
            txt_user_id = null;
        }
        setUserManagedFlag();
        setDdl_actionby();
        resetTourTraining();
        if (selectedUser != null) {
            Date v_last_locked_date = ejb_utilities_pis.getLastLockedDate(selectedUser.getUserId(), selectedUser.getPostingOumUnitSrno().getOumUnitSrno());
            cal_tour_office_leaving_min = DateTimeUtility.ChangeDateFormat(DateTimeUtility.nextDate(v_last_locked_date), null);
        }
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_actionby">
    String ddl_actionby;
    List<SelectItem> ddl_actionby_options;

    public String getDdl_actionby() {
        return ddl_actionby;
    }

    public void setDdl_actionby(String ddl_actionby) {
        this.ddl_actionby = ddl_actionby;
    }

    public void setDdl_actionby() {
        FacesContext context = FacesContext.getCurrentInstance();
        ddl_actionby_options = new ArrayList<SelectItem>();
        List<FhrdEmpmst> lst_Empmsts;
        if (selectedUser == null) {
            ddl_actionby_options = new ArrayList<SelectItem>();
            ddl_actionby_options.add(new SelectItem(null, "--- Enter User Id ---"));
            ddl_actionby = null;

        } else {

            lst_Empmsts = fhrdEmpmstFacade.findReportingUser(selectedUser.getPostingOumUnitSrno().getOumUnitSrno(), selectedUser.getUserId());
            if (lst_Empmsts.isEmpty()) {
                lst_Empmsts = fhrdEmpmstFacade.findReportingUser(globalData.getWorking_ou(), globalData.getUser_id());
            }
            int n = lst_Empmsts.size();
            if (n != 0) {
                for (int i = 0; i < n; i++) {
                    ddl_actionby_options.add(new SelectItem(lst_Empmsts.get(i).getUserId(), lst_Empmsts.get(i).getUserName()));
                }
            } else {
                ddl_actionby_options = new ArrayList<SelectItem>();
                ddl_actionby_options.add(new SelectItem(null, "--- Not Found ---"));
                ddl_actionby = null;
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Information", "Approval Authority is not defined for Organization Unit: " + globalData.getEnt_working_ou().getOumName()));
            }


            ddl_actionby = selectedUser.getReportingUserId().getUserId().toString();
        }
    }

    public List<SelectItem> getDdl_actionby_options() {
        return ddl_actionby_options;
    }

    public void setDdl_actionby_options(List<SelectItem> ddl_actionby_options) {
        this.ddl_actionby_options = ddl_actionby_options;
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="userManagedFlag">
    String userManagedFlag;

    public String getUserManagedFlag() {
        return userManagedFlag;
    }

    public void setUserManagedFlag(String userManagedFlag) {
        this.userManagedFlag = userManagedFlag;
    }

    private void setUserManagedFlag() {
        if (selectedUser != null) {
            userManagedFlag = "Y";
            if (!globalData.getUser_id().equals(selectedUser.getUserId())
                    && (globalData.getUser_id().equals(selectedUser.getReportingUserId().getUserId())
                    || globalData.isSystemUserFHRD())) {
                userManagedFlag = null;
            }
        }
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Saving Methods">
    //<editor-fold defaultstate="collapsed" desc="Tour Office Leaving & Arriving Settings">
    //<editor-fold defaultstate="collapsed" desc="cal_tour_office_leaving">
    Date cal_tour_office_leaving;
    String cal_tour_office_leaving_min;

    public Date getCal_tour_office_leaving() {
        return cal_tour_office_leaving;
    }

    public void setCal_tour_office_leaving(Date cal_tour_office_leaving) {
        this.cal_tour_office_leaving = cal_tour_office_leaving;
    }

    private void setCal_tour_office_leaving() {
        cal_tour_office_leaving = null;
        setDdl_tour_office_leaving_half();
    }

    public String getCal_tour_office_leaving_min() {
        return cal_tour_office_leaving_min;
    }

    public void setCal_tour_office_leaving_min(String cal_tour_office_leaving_min) {
        this.cal_tour_office_leaving_min = cal_tour_office_leaving_min;
    }

    public void cal_tour_office_leaving_changed(SelectEvent event) {
        cal_tour_office_leaving = event.getObject() == null ? null : (Date) event.getObject();
        setDdl_tour_office_leaving_half();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_tour_office_leaving_half">
    String ddl_tour_office_leaving_half;
    List<SelectItem> ddl_tour_office_leaving_half_options;

    public String getDdl_tour_office_leaving_half() {
        return ddl_tour_office_leaving_half;
    }

    public void setDdl_tour_office_leaving_half(String ddl_tour_office_leaving_half) {
        this.ddl_tour_office_leaving_half = ddl_tour_office_leaving_half;
    }

    public List<SelectItem> getDdl_tour_office_leaving_half_options() {
        return ddl_tour_office_leaving_half_options;
    }

    public void setDdl_tour_office_leaving_half_options(List<SelectItem> ddl_tour_office_leaving_half_options) {
        this.ddl_tour_office_leaving_half_options = ddl_tour_office_leaving_half_options;
    }

    private void setDdl_tour_office_leaving_half() {
        ddl_tour_office_leaving_half_options = new ArrayList<SelectItem>();
        if (cal_tour_office_leaving != null) {
            ddl_tour_office_leaving_half_options.add(new SelectItem("0", "Fullday"));
            ddl_tour_office_leaving_half_options.add(new SelectItem("1", "First Half"));
            ddl_tour_office_leaving_half_options.add(new SelectItem("2", "Second Half"));
            ddl_tour_office_leaving_half_options.add(new SelectItem("3", "Before Office Hours"));
            ddl_tour_office_leaving_half_options.add(new SelectItem("4", "After  Office Hours"));
            ddl_tour_office_leaving_half = "0";
        } else {
            ddl_tour_office_leaving_half = null;
        }
        setCal_tour_office_arriving();
    }

    public void ddl_tour_office_leaving_half_changed(ValueChangeEvent event) {
        ddl_tour_office_leaving_half = event.getNewValue() == null ? null : event.getNewValue().toString();
        setCal_tour_office_arriving();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_tour_office_arriving">
    Date cal_tour_office_arriving;
    String cal_tour_office_arriving_min;

    public Date getCal_tour_office_arriving() {
        return cal_tour_office_arriving;
    }

    public void setCal_tour_office_arriving(Date cal_tour_office_arriving) {
        this.cal_tour_office_arriving = cal_tour_office_arriving;
    }

    public String getCal_tour_office_arriving_min() {
        return cal_tour_office_arriving_min;
    }

    public void setCal_tour_office_arriving_min(String cal_tour_office_arriving_min) {
        this.cal_tour_office_arriving_min = cal_tour_office_arriving_min;
    }

    private void setCal_tour_office_arriving() {
        cal_tour_office_arriving = null;
        if (ddl_tour_office_leaving_half != null && ddl_tour_office_leaving_half.equals("4")) {
            cal_tour_office_arriving_min = DateTimeUtility.ChangeDateFormat(DateTimeUtility.nextDate(cal_tour_office_leaving), null);
        } else {
            cal_tour_office_arriving_min = DateTimeUtility.ChangeDateFormat(cal_tour_office_leaving, null);
        }
        setDdl_tour_office_arriving_half();
    }

    public void cal_tour_office_arriving_changed(SelectEvent event) {
        cal_tour_office_arriving = event.getObject() == null ? null : (Date) event.getObject();
        setDdl_tour_office_arriving_half();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_tour_office_arriving_half">
    String ddl_tour_office_arriving_half;
    List<SelectItem> ddl_tour_office_arriving_half_options;

    public String getDdl_tour_office_arriving_half() {
        return ddl_tour_office_arriving_half;
    }

    public void setDdl_tour_office_arriving_half(String ddl_tour_office_arriving_half) {
        this.ddl_tour_office_arriving_half = ddl_tour_office_arriving_half;
    }

    public List<SelectItem> getDdl_tour_office_arriving_half_options() {
        return ddl_tour_office_arriving_half_options;
    }

    public void setDdl_tour_office_arriving_half_options(List<SelectItem> ddl_tour_office_arriving_half_options) {
        this.ddl_tour_office_arriving_half_options = ddl_tour_office_arriving_half_options;
    }

    private void setDdl_tour_office_arriving_half() {
        ddl_tour_office_arriving_half_options = new ArrayList<SelectItem>();
        ddl_tour_office_arriving_half = null;

        if (cal_tour_office_leaving != null && cal_tour_office_arriving != null) {

            if (cal_tour_office_leaving.equals(cal_tour_office_arriving)) {
                if (ddl_tour_office_leaving_half.equals("0")) {
                    ddl_tour_office_arriving_half_options.add(new SelectItem("0", "Fullday"));
                    ddl_tour_office_arriving_half = "0";
                } else if (ddl_tour_office_leaving_half.equals("1")) {
                    ddl_tour_office_arriving_half_options.add(new SelectItem("1", "First Half"));
                    //                    ddl_tour_office_arriving_half_options.add(new SelectItem("2", "Second Half"));
                    ddl_tour_office_arriving_half = "1";
                } else if (ddl_tour_office_leaving_half.equals("2")) {
                    ddl_tour_office_arriving_half_options.add(new SelectItem("2", "Second Half"));
                    ddl_tour_office_arriving_half_options.add(new SelectItem("4", "After  Office Hours"));
                    ddl_tour_office_arriving_half = "2";
                } else if (ddl_tour_office_leaving_half.equals("3")) {
                    ddl_tour_office_arriving_half_options.add(new SelectItem("1", "First Half"));
                    ddl_tour_office_arriving_half_options.add(new SelectItem("2", "Second Half"));
                    ddl_tour_office_arriving_half_options.add(new SelectItem("4", "After  Office Hours"));
                    ddl_tour_office_arriving_half = "1";
                }

            } else {
                ddl_tour_office_arriving_half_options.add(new SelectItem("0", "Fullday"));
                ddl_tour_office_arriving_half_options.add(new SelectItem("1", "First Half"));
                //ddl_tour_office_arriving_half_options.add(new SelectItem("2", "Second Half"));
                ddl_tour_office_arriving_half_options.add(new SelectItem("3", "Before Office Hours"));
                ddl_tour_office_arriving_half_options.add(new SelectItem("4", "After  Office Hours"));
                ddl_tour_office_arriving_half = "0";
            }
            resetTourTrainingDetail();
        }
        tbl_tour_place = null;
        dateTime_tour_office_changed();

    }

    public void ddl_tour_office_arriving_half_changed(ValueChangeEvent event) {
        ddl_tour_office_arriving_half = event.getNewValue() == null ? null : event.getNewValue().toString();
        dateTime_tour_office_changed();
        resetTourTrainingDetail();
    }
    // <editor-fold defaultstate="collapsed" desc="datetime_tour_office_valuechange">

    private void dateTime_tour_office_changed() {
        float v_days;
        if (cal_tour_office_leaving != null && cal_tour_office_arriving != null) {
            v_days = setDays(cal_tour_office_leaving, cal_tour_office_arriving);
            if (ddl_tour_office_leaving_half.equalsIgnoreCase("2")) {
                v_days = (float) (v_days - 0.5);
            }
            if (ddl_tour_office_leaving_half.equalsIgnoreCase("4")) {
                v_days = (float) (v_days - 1);
            }
            if (ddl_tour_office_arriving_half.equalsIgnoreCase("3")) {
                v_days = (float) (v_days - 1);
            }
            if (ddl_tour_office_arriving_half.equalsIgnoreCase("1")) {
                v_days = (float) (v_days - 0.5);
            }
            txt_tour_total_days = String.valueOf(v_days);
        } else {
            txt_tour_total_days = null;
        }
    }// </editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_tour_total_days">
    String txt_tour_total_days;

    public String getTxt_tour_total_days() {
        return txt_tour_total_days;
    }

    public void setTxt_tour_total_days(String txt_tour_total_days) {
        this.txt_tour_total_days = txt_tour_total_days;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_reason">
    String txt_reason;

    public String getTxt_reason() {
        return txt_reason;
    }

    public void setTxt_reason(String txt_reason) {
        this.txt_reason = txt_reason;
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="rdb_appl_type">
    String rdb_appl_type;

    public String getRdb_appl_type() {
        return rdb_appl_type;
    }

    public void setRdb_appl_type(String rdb_appl_type) {
        this.rdb_appl_type = rdb_appl_type;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="rdb_appl_type_changed">

    public void rdb_appl_type_changed(ValueChangeEvent event) {
        rdb_appl_type = event.getNewValue() == null ? null : event.getNewValue().toString();
        tbl_tour_place = null;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="btn_apply_tour_action">

    public void btn_apply_tour_action(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if (tbl_tour_place == null || tbl_tour_place.isEmpty()) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "Please fill up Place Details."));
                return;
            }

            Database_Output database_Output = ftas_Pack.Check_Leave_TTR_ContOverlap(this.getClass().getName(),
                    "btn_apply_tour_action", rdb_appl_type, "S", null,
                    selectedUser.getUserId(), cal_tour_office_leaving, cal_tour_office_arriving,
                    ddl_tour_office_leaving_half, ddl_tour_office_arriving_half);
            if (database_Output.isExecuted_successfully()) {
                if (database_Output.getString1().equals("Y") == false) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Leave/Tour/Training already applied", database_Output.getString1()));
                    return;
                }
            } else {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error in checking previously added Applications."));
                return;
            }
            List<FhrdEmpleavebal> lst_entEmpleavebal =
                    fhrdEmpleavebalFacade.findAllFromLeave(
                    selectedUser.getPostingOumUnitSrno().getOumUnitSrno(),
                    selectedUser.getUserId(), true, rdb_appl_type, true, "N",
                    null, null, null, "Y", null, false, null, null, null, cal_tour_office_leaving, cal_tour_office_arriving, userManagedFlag);


            Integer v_cal_year = (cal_tour_office_leaving.getYear() + 1900);
            BigDecimal v_abs_srgkey = getGlobalSettings().getUniqueSrno(v_cal_year, "FTAS_ABSENTEES");
            BigDecimal v_absd_srgkey = getGlobalSettings().getUniqueSrno(v_cal_year, "FTAS_ABSENTEES_DTL");
            Integer v_apprv_auth_no = Integer.valueOf(ddl_actionby);
            Integer v_apprv_auth_oum = fhrdEmpmstFacade.find(v_apprv_auth_no).getPostingOumUnitSrno().getOumUnitSrno();

            List<FtasTourScheduleDtl> lst_FtasScheduleDtls = new ArrayList<FtasTourScheduleDtl>();
            List<FtasTrainingScheduleDtl> lst_FtasTrainingScheduleDtls = new ArrayList<FtasTrainingScheduleDtl>();
            List<FtasAbsenteesDtl> lstAbsenteesDtls = new ArrayList<FtasAbsenteesDtl>();

            FhrdEmpmst ent_FhrdEmpmst = fhrdEmpmstFacade.find(selectedUser.getUserId());
            FtasAbsentees ent_FtasAbsentees = new FtasAbsentees();
            FtasAbsenteesDtl ent_AbsenteesDtl = new FtasAbsenteesDtl();

            ent_FtasAbsentees.setAbsSrgKey(v_abs_srgkey);
            ent_FtasAbsentees.setUserId(ent_FhrdEmpmst);
            ent_FtasAbsentees.setSrno(1);
            ent_FtasAbsentees.setAppliType("S");
            ent_FtasAbsentees.setDays(BigDecimal.valueOf(Double.valueOf(txt_tour_total_days)));
            ent_FtasAbsentees.setEmpno(ent_FhrdEmpmst.getEmpno());
            ent_FtasAbsentees.setTranYear(Short.valueOf(v_cal_year.toString()));

            ent_FtasAbsentees.setFromDate((Date) cal_tour_office_leaving);
            ent_FtasAbsentees.setToDate((Date) cal_tour_office_arriving);
            ent_FtasAbsentees.setFromhalf(ddl_tour_office_leaving_half);
            ent_FtasAbsentees.setTohalf(ddl_tour_office_arriving_half);
            ent_FtasAbsentees.setReason1(txt_reason.trim().toUpperCase());
            ent_FtasAbsentees.setActnby(new FhrdEmpmst(v_apprv_auth_no));
            ent_FtasAbsentees.setAprvAuthOum(new SystOrgUnitMst(v_apprv_auth_oum));
            ent_FtasAbsentees.setActnpurp("A");
            ent_FtasAbsentees.setApplicationFlag(rdb_appl_type);

            ent_FtasAbsentees.setCreateby(new FhrdEmpmst(globalData.getUser_id()));
            ent_FtasAbsentees.setCreatedt(new Date());
            ent_FtasAbsentees.setOumUnitSrno(selectedUser.getPostingOumUnitSrno());



            if (lst_entEmpleavebal.isEmpty()) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", "Tour rule not exist for Employee " + selectedUser.getUserId() + " with same period,  Please contact admin."));
                return;
            }

            for (FhrdEmpleavebal empleavebal : lst_entEmpleavebal) {
                ent_AbsenteesDtl.setElrSrgKey(empleavebal.getElrSrgKey());
                ent_AbsenteesDtl.setEmplbSrgKey(empleavebal);
                ent_AbsenteesDtl.setLmSrgKey(empleavebal.getLmSrgKey());
                ent_AbsenteesDtl.setActualElrSrgKey(empleavebal.getElrSrgKey());
                ent_AbsenteesDtl.setActualEmplbSrgKey(empleavebal);
                ent_AbsenteesDtl.setActualLmSrgKey(empleavebal.getLmSrgKey());
                ent_AbsenteesDtl.setLastLeavecd("Y");
                ent_AbsenteesDtl.setAbsSrgKey(ent_FtasAbsentees);
                ent_AbsenteesDtl.setAbsdSrgKey(v_absd_srgkey);
                ent_AbsenteesDtl.setUserId(ent_FhrdEmpmst);
                ent_AbsenteesDtl.setCreateby(new FhrdEmpmst(globalData.getUser_id()));
                ent_AbsenteesDtl.setCreatedt(new Date());
                ent_AbsenteesDtl.setLeavecd(empleavebal.getLeavecd());
                ent_AbsenteesDtl.setDays(BigDecimal.valueOf(Double.valueOf(txt_tour_total_days)));
                ent_AbsenteesDtl.setTotalDays(ent_AbsenteesDtl.getDays());
                ent_AbsenteesDtl.setFromDate(DateTimeUtility.findBiggestDate(empleavebal.getOpeningDate(), cal_tour_office_leaving));
                ent_AbsenteesDtl.setToDate(DateTimeUtility.findSmallestDate(empleavebal.getClosingDate(), cal_tour_office_arriving));
                ent_AbsenteesDtl.setFromhalf(ddl_tour_office_leaving_half);
                ent_AbsenteesDtl.setTohalf(ddl_tour_office_arriving_half);
                ent_AbsenteesDtl.setOumUnitSrno(selectedUser.getPostingOumUnitSrno());
                ent_AbsenteesDtl.setEmplbSrgKey(empleavebal);
                ent_AbsenteesDtl.setElrSrgKey(empleavebal.getElrSrgKey());
                ent_AbsenteesDtl.setLmSrgKey(empleavebal.getLmSrgKey());
                ent_AbsenteesDtl.setTranYear(Short.valueOf(v_cal_year.toString()));
                ent_AbsenteesDtl.setLeaveBalLaps(BigDecimal.ZERO);
                ent_AbsenteesDtl.setCarryForwardFromOldBal(BigDecimal.ZERO);
                lstAbsenteesDtls.add(ent_AbsenteesDtl);

            }

            if (rdb_appl_type.equals("T")) {
                Integer n = tbl_tour_place.size();
                for (int i = 0; i < n; i++) {
                    BigDecimal v_tosd_srg_key = getGlobalSettings().getUniqueSrno(v_cal_year, "FTAS_TOUR_SCHEDULE_DTL");
                    FtasTourScheduleDtl ent_FtasScheduleDtl = new FtasTourScheduleDtl();
                    ent_FtasScheduleDtl.setAbsSrgKey(ent_FtasAbsentees);
                    ent_FtasScheduleDtl.setTosdSrgKey(v_tosd_srg_key);
                    ent_FtasScheduleDtl.setSubSrno(i + 1);
                    ent_FtasScheduleDtl.setFromDatetime(tbl_tour_place.get(i).getFromDatetime());
                    ent_FtasScheduleDtl.setToDatetime(tbl_tour_place.get(i).getToDatetime());
                    ent_FtasScheduleDtl.setPurpose(tbl_tour_place.get(i).getPurpose());
                    ent_FtasScheduleDtl.setFromplace(tbl_tour_place.get(i).getFromplace());
                    ent_FtasScheduleDtl.setToplace(tbl_tour_place.get(i).getToplace());
                    ent_FtasScheduleDtl.setCreateby(new FhrdEmpmst(globalData.getUser_id()));
                    ent_FtasScheduleDtl.setCreatedt(new Date());
                    ent_FtasScheduleDtl.setOumUnitSrno(selectedUser.getPostingOumUnitSrno());
                    ent_FtasScheduleDtl.setTranYear(Short.valueOf(v_cal_year.toString()));
                    if (ent_FtasAbsentees.getPlace1() == null) {
                        ent_FtasAbsentees.setPlace1(ent_FtasScheduleDtl.getPurpose());
                    } else {
                        ent_FtasAbsentees.setPlace1(ent_FtasAbsentees.getPlace1() + "<br/>" + ent_FtasScheduleDtl.getPurpose());
                    }
                    lst_FtasScheduleDtls.add(ent_FtasScheduleDtl);
                }
                ent_FtasAbsentees.setFtasTourScheduleDtlCollection(lst_FtasScheduleDtls);
            } else if (rdb_appl_type.equals("R")) {
                Integer n = tbl_tour_place.size();
                for (int i = 0; i < n; i++) {
                    BigDecimal v_trsd_srg_key = getGlobalSettings().getUniqueSrno(v_cal_year, "FTAS_TRAINING_SCHEDULE_DTL");
                    FtasTrainingScheduleDtl ent_FtasScheduleDtl = new FtasTrainingScheduleDtl();
                    ent_FtasScheduleDtl.setAbsSrgKey(ent_FtasAbsentees);
                    ent_FtasScheduleDtl.setTrsdSrgKey(v_trsd_srg_key);
                    ent_FtasScheduleDtl.setSubSrno(i + 1);
                    ent_FtasScheduleDtl.setFromDatetime(tbl_tour_place.get(i).getFromDatetime());
                    ent_FtasScheduleDtl.setToDatetime(tbl_tour_place.get(i).getToDatetime());
                    ent_FtasScheduleDtl.setDescription(tbl_tour_place.get(i).getPurpose());
                    ent_FtasScheduleDtl.setCreateby(new FhrdEmpmst(globalData.getUser_id()));
                    ent_FtasScheduleDtl.setCreatedt(new Date());
                    ent_FtasScheduleDtl.setOumUnitSrno(selectedUser.getPostingOumUnitSrno());
                    ent_FtasScheduleDtl.setTranYear(Short.valueOf(v_cal_year.toString()));
                    if (ent_FtasAbsentees.getReason1() == null) {
                        ent_FtasAbsentees.setReason1(ent_FtasScheduleDtl.getDescription());
                    } else {
                        ent_FtasAbsentees.setReason1(ent_FtasAbsentees.getReason1() + "<br/>" + ent_FtasScheduleDtl.getDescription());
                    }
                    lst_FtasTrainingScheduleDtls.add(ent_FtasScheduleDtl);
                }
                ent_FtasAbsentees.setFtasTrainingScheduleDtlCollection(lst_FtasTrainingScheduleDtls);
            }
            ent_FtasAbsentees.setFtasAbsenteesDtlCollection(lstAbsenteesDtls);
            try {

                ftasAbsenteesFacade.create(ent_FtasAbsentees);
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully."));
                txt_user_id=null;
                txt_user_id_changed();
            } catch (Exception e) {
                context.addMessage(null, JSFMessages.getErrorMessage(e));
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_apply_tour_action", null, e);
            }
        } catch (Exception e) {
            context.addMessage(null, JSFMessages.getErrorMessage(e));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_apply_tour_action", null, e);
        }
    }// </editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Tour/Training Detail Settings">
    //<editor-fold defaultstate="collapsed" desc="cal_tour_from_date">
    Date cal_tour_from_date;

    public Date getCal_tour_from_date() {
        return cal_tour_from_date;
    }

    public void setCal_tour_from_date(Date cal_tour_from_date) {
        this.cal_tour_from_date = cal_tour_from_date;
    }

    private void setCal_tour_from_date() {
        if (tbl_tour_place == null || tbl_tour_place.isEmpty()) {
            cal_tour_from_date = null;
            cal_tour_from_date_min = DateTimeUtility.ChangeDateFormat(cal_tour_office_leaving, "dd-MMM-yyyy HH:mm");
            cal_tour_from_date_max = DateTimeUtility.ChangeDateFormat(new DateTime(cal_tour_office_arriving).plusDays(1).minusMinutes(1).toDate(), "dd-MMM-yyyy HH:mm");
        } else {
            int n = tbl_tour_place.size();
            cal_tour_from_date = tbl_tour_place.get(n - 1).getToDatetime();
            //            cal_tour_from_date_min = DateTimeUtility.ChangeDateFormat(cal_tour_from_date, "dd-MMM-yyyy hh:mm");
            //          cal_tour_from_date_max = DateTimeUtility.ChangeDateFormat(cal_tour_from_date, "dd-MMM-yyyy hh:mm");
        }

        setCal_tour_to_date();
    }

    public void cal_tour_from_date_changed(SelectEvent event) {
        cal_tour_from_date = event.getObject() == null ? null : (Date) event.getObject();
        setCal_tour_to_date();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_tour_to_date">
    Date cal_tour_to_date;

    public Date getCal_tour_to_date() {
        return cal_tour_to_date;
    }

    private void setCal_tour_to_date() {
        if (cal_tour_from_date == null) {
            cal_tour_to_date_min = DateTimeUtility.ChangeDateFormat(cal_tour_office_leaving, "dd-MMM-yyyy HH:mm");
        } else {
            cal_tour_to_date_min = DateTimeUtility.ChangeDateFormat(cal_tour_from_date, "dd-MMM-yyyy HH:mm");
        }
        cal_tour_to_date_max = DateTimeUtility.ChangeDateFormat(new DateTime(cal_tour_office_arriving).plusDays(1).minusMinutes(1).toDate(), "dd-MMM-yyyy HH:mm");
        cal_tour_to_date = null;
    }

    public void setCal_tour_to_date(Date cal_tour_to_date) {
        this.cal_tour_to_date = cal_tour_to_date;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tour date min/max">
    String cal_tour_from_date_min;
    String cal_tour_from_date_max;
    String cal_tour_to_date_min;
    String cal_tour_to_date_max;

    public String getCal_tour_from_date_max() {
        return cal_tour_from_date_max;
    }

    public void setCal_tour_from_date_max(String cal_tour_from_date_max) {
        this.cal_tour_from_date_max = cal_tour_from_date_max;
    }

    public String getCal_tour_from_date_min() {
        return cal_tour_from_date_min;
    }

    public void setCal_tour_from_date_min(String cal_tour_from_date_min) {
        this.cal_tour_from_date_min = cal_tour_from_date_min;
    }

    public String getCal_tour_to_date_max() {
        return cal_tour_to_date_max;
    }

    public void setCal_tour_to_date_max(String cal_tour_to_date_max) {
        this.cal_tour_to_date_max = cal_tour_to_date_max;
    }

    public String getCal_tour_to_date_min() {
        return cal_tour_to_date_min;
    }

    public void setCal_tour_to_date_min(String cal_tour_to_date_min) {
        this.cal_tour_to_date_min = cal_tour_to_date_min;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tour place purpose">
    String txt_tour_to_place;
    String txt_tour_purpose;
    String txt_tour_from_place;

    public String getTxt_tour_from_place() {
        return txt_tour_from_place;
    }

    public void setTxt_tour_from_place(String txt_tour_from_place) {
        this.txt_tour_from_place = txt_tour_from_place;
    }

    public String getTxt_tour_to_place() {
        return txt_tour_to_place;
    }

    public void setTxt_tour_to_place(String txt_tour_to_place) {
        this.txt_tour_to_place = txt_tour_to_place;
    }

    public String getTxt_tour_purpose() {
        return txt_tour_purpose;
    }

    public void setTxt_tour_purpose(String txt_tour_purpose) {
        this.txt_tour_purpose = txt_tour_purpose;
    }

    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_add_tour_training_detail_clicked">
    public void btn_add_tour_training_detail_clicked(ActionEvent event) {
        RequestContext.getCurrentInstance().execute("pw_dlg_pnl_new_tour_training_entry.show()");
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tbl_tour_place">
    List<FtasTourScheduleDtl> tbl_tour_place;

    public List<FtasTourScheduleDtl> getTbl_tour_place() {
        return tbl_tour_place;
    }

    public void setTbl_tour_place(List<FtasTourScheduleDtl> tbl_tour_place) {
        this.tbl_tour_place = tbl_tour_place;
    }
// </editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="btn_tour_add_to_list_action">

    public void btn_tour_add_to_list_action(ActionEvent event) {
        if (tbl_tour_place == null) {
            tbl_tour_place = new ArrayList<FtasTourScheduleDtl>();
        }
        FtasTourScheduleDtl ent_TourTraining1 = new FtasTourScheduleDtl(new BigDecimal(tbl_tour_place.size()));

        ent_TourTraining1.setTosdSrgKey(new BigDecimal(tbl_tour_place.size()));
        if (txt_tour_from_place != null) {
            ent_TourTraining1.setFromplace(txt_tour_from_place.toUpperCase());
        }
        if (txt_tour_to_place != null) {
            ent_TourTraining1.setToplace(txt_tour_to_place.toUpperCase());
        }
        ent_TourTraining1.setPurpose(txt_tour_purpose.toUpperCase());
        ent_TourTraining1.setFromDatetime(cal_tour_from_date);
        ent_TourTraining1.setToDatetime(cal_tour_to_date);
        tbl_tour_place.add(ent_TourTraining1);
        resetTourTrainingDetail();
        RequestContext.getCurrentInstance().execute("pw_dlg_pnl_new_tour_training_entry.hide()");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_tour_delete_action">

    public void btn_tour_delete_action(FtasTourScheduleDtl ent_DtlEntry) {
        tbl_tour_place.remove(ent_DtlEntry);
        setCal_tour_from_date();
    }

    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Reset Settings">
    //<editor-fold defaultstate="collapsed" desc="resetTourTraining()">
    public void resetTourTraining() {
        setCal_tour_office_leaving();
        txt_reason = null;
        tbl_tour_place = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetTourTrainingDetail()">

    private void resetTourTrainingDetail() {
        setCal_tour_from_date();
        txt_tour_to_place = null;
        txt_tour_from_place = null;
        txt_tour_purpose = null;
    }
    //</editor-fold>
    //</editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="setDays">

    public int setDays(Date p_fromDate, Date p_toDate) {
        double v_diff_limit;
        float temp_newdiff;
        v_diff_limit = p_toDate.getTime() - p_fromDate.getTime();
        temp_newdiff = (int) (v_diff_limit / 86400000);
        temp_newdiff += 1;
        return (int) temp_newdiff;
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Archived">
    // <editor-fold defaultstate="collapsed" desc="setDays with date and time">
    //
    //    public String setDays(Date p_cal_from, Date p_cal_to, String p_fromDate, String p_toDate,
    //            String p_fromhr, String p_frommin,
    //            String p_tohr, String p_tomin) {
    //        String return_days;
    //        Date d1 = p_cal_from;
    //        Calendar temp_cal = Calendar.getInstance();
    //        temp_cal.set(d1.getYear(), d1.getMonth(), d1.getDate());
    //        d1 = temp_cal.getTime();
    //        Date d2 = p_cal_to;
    //        temp_cal.set(d2.getYear(), d2.getMonth(), d2.getDate());
    //        d2 = temp_cal.getTime();
    //        double v_diff_limit;
    //        float temp_newdiff;
    //        v_diff_limit = d2.getTime() - d1.getTime();
    //        temp_newdiff = (int) (v_diff_limit / 86400000);
    //        temp_newdiff += 1;
    //        Date v_from_date = p_cal_from;
    //        Date v_to_date = p_cal_to;
    //
    //        String tempDate_from = sdf.format(v_from_date);
    //        String tempDate_to = sdf.format(v_to_date);
    //        String str_fromdate_time = tempDate_from + " " + p_fromhr + ":" + p_frommin;
    //        String str_todate_time = tempDate_to + " " + p_tohr + ":" + p_tomin;
    //
    //        String str_tempfrom_930am = tempDate_from + " 09:30";
    //        String str_tempfrom_2pm = tempDate_from + " 14:00";
    //        String str_tempfrom_12am = tempDate_from + " 12:00";
    //        String str_tempfrom_6pm = tempDate_from + " 18:00";
    //
    //        String str_tempto_930am = tempDate_to + " 09:30";
    //        String str_tempto_2pm = tempDate_to + " 14:00";
    //        String str_tempto_12am = tempDate_to + " 12:00";
    //        String str_tempto_6pm = tempDate_to + " 18:00";
    //        try {
    //            Date newDate_from = dfmt.parse(str_fromdate_time);
    //            Date newDate_to = dfmt.parse(str_todate_time);
    //
    //            Date tempfrom_12am = dfmt.parse(str_tempfrom_12am);
    //            Date tempfrom_2pm = dfmt.parse(str_tempfrom_2pm);
    //            Date tempfrom_930am = dfmt.parse(str_tempfrom_930am);
    //            Date tempfrom_6pm = dfmt.parse(str_tempfrom_6pm);
    //
    //            Date tempto_12am = dfmt.parse(str_tempto_12am);
    //            Date tempto_2pm = dfmt.parse(str_tempto_2pm);
    //            Date tempto_930am = dfmt.parse(str_tempto_930am);
    //            Date tempto_6pm = dfmt.parse(str_tempto_6pm);
    //
    //            if ((newDate_from.after(tempfrom_2pm) == true) && (newDate_from.before(tempfrom_6pm))) {
    //                temp_newdiff = (float) (temp_newdiff - 0.5);
    //            } else if ((newDate_from.after(tempfrom_6pm) == true)) {
    //                temp_newdiff = (float) (temp_newdiff - 1);
    //            }
    //
    //            if (newDate_to.before(tempto_930am) == true) {
    //                temp_newdiff = (float) (temp_newdiff - 1);
    //            } else if ((newDate_to.after(tempto_930am) == true)) {
    //                if ((newDate_to.before(tempto_2pm) == true)) {
    //                    temp_newdiff = (float) (temp_newdiff - 0.5);
    //                }
    //            }
    //        } catch (ParseException ex) {
    //            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setDays", null, ex);
    //        }
    //        return_days = String.valueOf(temp_newdiff);
    //
    //        return return_days;
    //    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="getHalfValue">

    //    public String getHalfValue(Date p_date, String p_hr, String p_min) {
    //        String v_return_halfValue = "0";
    //        String tempDate = sdf.format(p_date);
    //        String str_date_time = tempDate + " " + p_hr + ":" + p_min;
    //        String str_temp_930am = tempDate + " 09:30";
    //        String str_temp_2pm = tempDate + " 14:00";
    //        String str_temp_12am = tempDate + " 00:00";
    //        String str_temp_6pm = tempDate + " 18:00";
    //        try {
    //            Date newDate = dfmt.parse(str_date_time);
    //            Date temp_12am = dfmt.parse(str_temp_12am);
    //            Date temp_2pm = dfmt.parse(str_temp_2pm);
    //            Date temp_930am = dfmt.parse(str_temp_930am);
    //            Date temp_6pm = dfmt.parse(str_temp_6pm);
    //
    //            if ((newDate.after(temp_12am) && newDate.before(temp_930am)) || newDate.equals(temp_12am)) {
    //                v_return_halfValue = "3";
    //            } else if ((newDate.after(temp_930am) && newDate.before(temp_2pm)) || newDate.equals(temp_930am)) {
    //                v_return_halfValue = "1";
    //            } else if ((newDate.after(temp_2pm) && newDate.before(temp_6pm)) || newDate.equals(temp_2pm)) {
    //                v_return_halfValue = "2";
    //            } else if ((newDate.after(temp_6pm) && newDate.before(temp_12am)) || newDate.equals(temp_6pm)) {
    //                v_return_halfValue = "4";
    //            }
    //
    //        } catch (ParseException ex) {
    //            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "getHalfValue", null, ex);
    //        }
    //        return v_return_halfValue;
    //    }
    // </editor-fold>
    //</editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="Default Constructor & Global Settings Methods">
    //<editor-fold defaultstate="collapsed" desc="Default Constructor">
    public PIS_Tour_Training_Appl() {
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setDdl_actionby();
        Date v_last_locked_date = ejb_utilities_pis.getLastLockedDate(new Integer(globalData.getUser_id()), globalData.getWorking_ou());
        cal_tour_office_leaving_min = DateTimeUtility.ChangeDateFormat(DateTimeUtility.nextDate(v_last_locked_date), null);
        rdb_appl_type = "T";
    }
    //</editor-fold>
}
