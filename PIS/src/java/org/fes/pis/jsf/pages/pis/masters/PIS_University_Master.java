/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.pis.masters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.FhrdUniversitymst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdUniversitymstFacadeLocal;

/**
 *
 * @author fes
 */
@ManagedBean(name = "PIS_University_Master")
@ViewScoped
public class PIS_University_Master implements Serializable {

    @EJB
    private FhrdUniversitymstFacadeLocal fhrdUniversitymstFacade;
    //<editor-fold defaultstate="collapsed" desc="Objects and EJB Declarations">
    @EJB
    FhrdUniversitymstFacadeLocal universitymstFacade;
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="Constructor and other Bean methods">

    /**
     * Creates a new instance of PIS_University_Master
     *
     */
    public PIS_University_Master() {
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="getter/setter">
    public String txt_university_name;
    public String txt_city;
    public String txt_state;
    public String txt_university_code;
    public String txt_country;
    public String txt_pin_code;

    public String getTxt_university_code() {
        return txt_university_code;
    }

    public void setTxt_university_code(String txt_university_code) {
        this.txt_university_code = txt_university_code;
    }

    public String getTxt_city() {
        return txt_city;
    }

    public void setTxt_city(String txt_city) {
        this.txt_city = txt_city;
    }

    public String getTxt_country() {
        return txt_country;
    }

    public void setTxt_country(String txt_country) {
        this.txt_country = txt_country;
    }

    public String getTxt_pin_code() {
        return txt_pin_code;
    }

    public void setTxt_pin_code(String txt_pin_code) {
        this.txt_pin_code = txt_pin_code;
    }

    public String getTxt_state() {
        return txt_state;
    }

    public void setTxt_state(String txt_state) {
        this.txt_state = txt_state;
    }

    public String getTxt_university_name() {
        return txt_university_name;
    }

    public void setTxt_university_name(String txt_university_name) {
        this.txt_university_name = txt_university_name;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tblList">
    List<FhrdUniversitymst> tbl_university_master;
    List<FhrdUniversitymst> tbl_university_master_filtered;

    public List<FhrdUniversitymst> getTbl_university_master_filtered() {
        return tbl_university_master_filtered;
    }

    public void setTbl_university_master_filtered(List<FhrdUniversitymst> tbl_university_master_filtered) {
        this.tbl_university_master_filtered = tbl_university_master_filtered;
    }

    public List<FhrdUniversitymst> getTbl_university_master() {
        return tbl_university_master;
    }

    public void setTbl_university_master(List<FhrdUniversitymst> tbl_university_master) {
        this.tbl_university_master = tbl_university_master;
    }

    public void setTblList() {
        tbl_university_master = universitymstFacade.findAll(globalData.getWorking_ou());
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_save_action">
    public void btn_save_action() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (!validateEntry()) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "Same University Code or University Name exists in same City and State"));
            return;
        }
        BigDecimal v_lm_srgkey = getGlobalSettings().getUniqueSrno(null, "FHRD_UNIVERSITYMST");
        FhrdUniversitymst ent_FhrdUniversitymst = new FhrdUniversitymst(v_lm_srgkey);
        ent_FhrdUniversitymst.setUniversitySrno(-1);
        ent_FhrdUniversitymst.setUniversitycd(txt_university_code.toUpperCase().trim());
        ent_FhrdUniversitymst.setUniversityName(txt_university_name.toUpperCase().trim());
        if (!txt_city.isEmpty()) {
            ent_FhrdUniversitymst.setCity(txt_city.toUpperCase().trim());
        }
        ent_FhrdUniversitymst.setState(txt_state.toUpperCase().trim());
        if (!txt_pin_code.isEmpty()) {
            ent_FhrdUniversitymst.setPincode(txt_pin_code.toUpperCase());
        }
        ent_FhrdUniversitymst.setCountry(txt_country.toUpperCase());
        ent_FhrdUniversitymst.setOumUnitSrno(globalData.getEnt_working_ou());
        ent_FhrdUniversitymst.setCreateby(globalData.getEnt_login_user());
        ent_FhrdUniversitymst.setCreatedt(new Date());
        try {
            universitymstFacade.create(ent_FhrdUniversitymst);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucess", "Record has been saved Succesfully"));
            resetFields();
            setTblList();
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occerred during saving records"));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_save_action", null, e);
        }
    }

    private boolean validateEntry() {
        if (fhrdUniversitymstFacade.isUniversityExist(globalData.getWorking_ou(), txt_university_code.toUpperCase().trim(), txt_university_name.toUpperCase().trim(), txt_city.toUpperCase().trim(), txt_state.trim().toUpperCase())) {
            return false;
        }
        return true;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Init">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setTblList();
    }
    //</editor-fold>   
    //<editor-fold defaultstate="collapsed" desc="Global Declarations">
    //<editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //</editor-fold>   
    //<editor-fold defaultstate="collapsed" desc="Reset form">

    private void resetFields() {
        txt_university_code = null;
        txt_city = null;
        txt_country = null;
        txt_state = null;
        txt_university_name = null;
        txt_pin_code = null;
    }
    //</editor-fold>
}
