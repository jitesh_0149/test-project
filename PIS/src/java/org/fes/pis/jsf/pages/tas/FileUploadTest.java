/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.tas;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.jsf.GlobalResources;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.primefaces.component.fileupload.FileUpload;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author anu
 */
@ManagedBean(name = "FileUploadTest")
@ViewScoped
public class FileUploadTest implements Serializable {


    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    public UploadedFile uploadedFile = null;
    public String uploadedFile_extension = null;
    public String uploadedFilePath = "/home/anu/Anu/NetbeansProjects/PIS/PIS-app/PIS/web/images/emp/";///home/anu/Anu/NetbeansProjects/PISApplication/GF3.2.1/images
    public String uploadedFilePath_temp = "/home/anu/Anu/NetbeansProjects/PIS/PIS-app/PIS/web/images/emp_temp/";

    /**
     * Creates a new instance of FileUploadTest
     */
    public FileUploadTest() {
    }
    Integer v_property_master = 1;
    String emp_image_url;

    public String getEmp_image_url() {
        return emp_image_url;
    }

    public void setEmp_image_url(String emp_image_url) {
        this.emp_image_url = emp_image_url;
    }

    public void setEmpImage() {
        emp_image_url = "/images/emp/default.png";

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="pfu_emp_upload">
    FileUpload pfu_emp_upload = new FileUpload();

    public FileUpload getPfu_emp_upload() {
        return pfu_emp_upload;
    }

    public void setPfu_emp_upload(FileUpload pfu_emp_upload) {
        this.pfu_emp_upload = pfu_emp_upload;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="handleFileUpload">

    public void handleFileUpload(FileUploadEvent event) {
        if (event.getFile().getSize() != 0) {
            uploadedFile = event.getFile();
            String v_file_name = event.getFile().getFileName();
            String v_file_extension = v_file_name.substring(v_file_name.lastIndexOf(".") + 1);
            uploadedFile_extension = v_file_extension;
            String v_image_path = uploadedFilePath_temp + event.getFile().getFileName();// + "." + uploadedFile_extension;

            if (uploadedFile != null) {
                InputStream uploadInStream = null;
                try {
                    uploadInStream = uploadedFile.getInputstream();
                    FileOutputStream fOut = new FileOutputStream(v_image_path);
                    int c;
                    while ((c = uploadInStream.read()) != -1) {
                        fOut.write(c);
                    }
                    fOut.flush();
                    fOut.close();
                } catch (IOException ex) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "handleFileUpload", null, ex);
                } finally {
                    try {
                        uploadInStream.close();
                    } catch (IOException ex) {
                        LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "handleFileUpload", null, ex);
                    }
                }
            }
            emp_image_url = "/images/emp_temp/" + event.getFile().getFileName();//     + "." + uploadedFile_extension);
        }
    }

    @PostConstruct
    public void init() {
        uploadedFile = null;
        setEmpImage();
        if (getGlobalResources().getProject_path().contains("database.fes.org.in")) {
            uploadedFilePath = "/opt/images/emp/";//path for GF 3.2.1[IBM]
            uploadedFilePath_temp = "/opt/images/emp_temp/";//path for GF 3.2.1[IBM]
        } else if (getGlobalResources().getProject_path().contains("service.fes.org.in")) {
            uploadedFilePath = "/opt/images/emp/";//path for GF 3.2.1[SUSE]
            uploadedFilePath_temp = "/opt/images/emp_temp/";//path for GF 3.2.1[SUSE]
        } else {
            uploadedFilePath = "/home/anu/Anu/NetbeansProjects/PIS/PIS-app/PIS/web/images/emp/";///home/anu/Anu/NetbeansProjects/PISApplication/GF3.2.1/images
            uploadedFilePath_temp = "/home/anu/Anu/NetbeansProjects/PIS/PIS-app/PIS/web/images/emp_temp/";
        }
    }
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalResources">

    protected GlobalResources getGlobalResources() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalResources) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalResources");
    }// </editor-fold>
    // </editor-fold>
}