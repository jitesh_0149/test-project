/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.tas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.Daily_Missing_Attendance;
import org.fes.pis.custom_entities.Database_Output;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.db.fes_project.pack.Ftas_Pack;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.FtasLeavemst;
import org.fes.pis.jsf.GlobalResources;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.jsf.selectableDataModels.SD_FhrdEmpmst;
import org.fes.pis.jsf.utilities.JSFMessages;
import org.fes.pis.sessions.*;
import org.primefaces.context.RequestContext;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Daily_Attendance")
@ViewScoped
public class PIS_Daily_Attendance implements Serializable {

    @EJB
    private FhrdEmpleavebalFacadeLocal fhrdEmpleavebalFacade;
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    Ou_Pack ou_Pack = new Ou_Pack();
    Ftas_Pack ftas_Pack = new Ftas_Pack();
    @EJB
    private SystMenumstFacadeLocal systMenumstFacade;
    @EJB
    private FtasDailyFacadeLocal ftasDailyFacade;
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    @EJB
    private FtasLeavemstFacadeLocal ftasLeavemstFacade;
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="Daily Attendance">
    //<editor-fold defaultstate="collapsed" desc="txt_user_id">
    String txt_user_id;
    String err_txt_user_id;

    public String getTxt_user_id() {
        return txt_user_id;
    }

    public void setTxt_user_id(String txt_user_id) {
        this.txt_user_id = txt_user_id;
    }

    public String getErr_txt_user_id() {
        return err_txt_user_id;
    }

    public void setErr_txt_user_id(String err_txt_user_id) {
        this.err_txt_user_id = err_txt_user_id;
    }

    public void txt_user_id_changed(ValueChangeEvent event) {
        txt_user_id = event.getNewValue().toString();
        err_txt_user_id = null;
        txt_user_id_changed();
    }

    private void txt_user_id_changed() {
        selectedUser = null;
        if (txt_user_id != null && txt_user_id.equals("") != true) {
            selectedUser = fhrdEmpmstFacade.find(Integer.valueOf(txt_user_id));
            if (selectedUser == null) {
                err_txt_user_id = " (User Id is invalid)";
            }
        }
        userSelected();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="UserSelection Method">
    //<editor-fold defaultstate="collapsed" desc="selectedUser">
    FhrdEmpmst selectedUser;

    public FhrdEmpmst getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(FhrdEmpmst selectedUser) {
        this.selectedUser = selectedUser;

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="userSelectionValid">
    boolean userSelectionValid;

    public boolean isUserSelectionValid() {
        return userSelectionValid;
    }

    public void setUserSelectionValid(boolean userSelectionValid) {
        this.userSelectionValid = userSelectionValid;
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="userSelected">

    private void userSelected() {
        userSelectionValid = false;
        if (selectedUser != null) {
            txt_user_id = selectedUser.getUserId().toString();
            if (!getPIS_GlobalSettings().getSysetemUser_Fhrd() && !globalData.getUser_id().equals(selectedUser.getUserId())) {
                if (!fhrdEmpmstFacade.hasAccessSpanO(globalData.getUser_id(), selectedUser.getPostingOumUnitSrno().getOumUnitSrno())) {
                    err_txt_user_id = " (You do not have privilege to mark daily attendance)";
                }
            } else if ((selectedUser.getOrgRelieveDate() != null || selectedUser.getTerminationDate() != null)
                    && (!fhrdEmpleavebalFacade.isUserTransactionPending(selectedUser.getUserId()))) {
                err_txt_user_id = " (This employee has relieved/terminated the organisation)";
            }
            if (err_txt_user_id == null) {
                userSelectionValid = true;
            }
        } else {
            //err_txt_user_id = " (Invalid User Id)";
            txt_user_id = null;
        }
        resetDailyAttendance();
        resetMissingAttendance();
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="hdn_check_marked_full">
    String hdn_check_marked_full;

    public String getHdn_check_marked_full() {
        return hdn_check_marked_full;
    }

    public void setHdn_check_marked_full(String hdn_check_marked_full) {
        this.hdn_check_marked_full = hdn_check_marked_full;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="rdb_presence_for">
    Integer today_attd_status;
    String rdb_presence_for = "0";

    public Integer getToday_attd_status() {
        return today_attd_status;
    }

    public void setToday_attd_status(Integer today_attd_status) {
        this.today_attd_status = today_attd_status;
    }

    public String getRdb_presence_for() {
        return rdb_presence_for;
    }

    public void setRdb_presence_for(String rdb_presence_for) {
        this.rdb_presence_for = rdb_presence_for;
    }

    private void setRdb_presense_for() {
        FacesContext context = FacesContext.getCurrentInstance();
        rdb_presence_for = "0";
        hdn_check_marked_full = "0";
        if (userSelectionValid) {
            hdn_check_marked_full = "3";
            rdb_presence_for = null;
            today_attd_status = ftasDailyFacade.checkTodaysAtt(selectedUser.getUserId(), new Date());
            if (today_attd_status == 1) {
                hdn_check_marked_full = "1";
                rdb_presence_for = "1";
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", "Your second half is marked as presance, so can only mark attendance for first half!!!"));
            } else if (today_attd_status == 2) {
                hdn_check_marked_full = "2";
                rdb_presence_for = "2";
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", "Your first half is marked as presance, so can only mark attendance for second half!!!"));
            } else if (today_attd_status == -1) {
                hdn_check_marked_full = "0";
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "You have already marked an attendance for today, so you can not mark Attendance."));
            } else if (today_attd_status == -2) {
                hdn_check_marked_full = "0";
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Contract is not given  so you can not mark Attendance."));
            } else {
                rdb_presence_for = "0";
            }
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="rdb_place">
    String rdb_place;
    List<SelectItem> rdb_place_options;

    public String getRdb_place() {
        return rdb_place;
    }

    public void setRdb_place(String rdb_place) {
        this.rdb_place = rdb_place;
    }

    public List<SelectItem> getRdb_place_options() {
        return rdb_place_options;
    }

    public void setRdb_place_options(List<SelectItem> rdb_place_options) {
        this.rdb_place_options = rdb_place_options;
    }

    private void setRdb_place_options() {
        rdb_place_options = new ArrayList<SelectItem>();
        if (rdb_place == null) {
            List<FtasLeavemst> lst_e = ftasLeavemstFacade.findAllActualAttd(globalData.getWorking_ou());
            if (lst_e.isEmpty()) {
                rdb_place_options.add(new SelectItem(null, "--- Not Available ---"));
            } else {
                for (FtasLeavemst e : lst_e) {
                    rdb_place_options.add(new SelectItem(e.getLmSrgKey(), e.getLeaveDesc()));
                }
            }
        }
        rdb_place = rdb_place_options.get(0).getValue().toString();
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="btn_mark_attendance_clicked">

    public void btn_mark_attendance_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        String rdb_presence_for_value = rdb_presence_for;
        try {
            String v_half1 = null;
            String v_half2 = null;
            if (userSelectionValid) {
                boolean v_attd_marked = false;
                if (rdb_presence_for_value.equalsIgnoreCase("0") == true) {
                    v_half1 = rdb_place;
                    v_half2 = rdb_place;
                }
                if (rdb_presence_for_value.equalsIgnoreCase("1") == true) {
                    v_half1 = rdb_place;
                    v_half2 = null;
                }
                if (rdb_presence_for_value.equalsIgnoreCase("2") == true) {
                    v_half1 = null;
                    v_half2 = rdb_place;
                }
                Database_Output database_Output =
                        ftas_Pack.SetAttendance(this.getClass().getName(), "btn_mark_attendance_clicked",
                        selectedUser.getUserId(), new Date(), new Date(),
                        v_half1 == null ? null : new BigDecimal(v_half1),
                        v_half2 == null ? null : new BigDecimal(v_half2),
                        null,
                        null, "N",
                        globalData.getUser_id(),
                        null);

                if (database_Output.isExecuted_successfully()) {
                    v_attd_marked = true;
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully"));
                    txt_user_id = null;
                    txt_user_id_changed();
                } else {
                    context.addMessage(null, database_Output.getFacesMessage());
                }
//                System.out.println("redirect " + redirectUrl);
                if (v_attd_marked && redirectUrl != null) {
                    RequestContext.getCurrentInstance().execute("window.location='" + redirectUrl + "'");
                }
            } else {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "Please select valid employee"));
            }
        } catch (Exception e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_mark_attendance_clicked", null, e);
            context.addMessage(null, JSFMessages.getErrorMessage(e));
        }
    }// </editor-fold> 
    // <editor-fold defaultstate="collapsed" desc="resetUserDetail">

    private void resetDailyAttendance() {
        setRdb_presense_for();
    }

    public void resetUserDetail() {
        selectedAttendances = null;
        tbl_Missing_Attendance_list = null;
        txt_user_id = null;
        selectedUser = null;
    }// </editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Missing Attendance">
    //<editor-fold defaultstate="collapsed" desc="tbl_Missing_Attendance_list">
    PIS_Missing_Attendance_Checkbox tbl_Missing_Attendance_list;

    public PIS_Missing_Attendance_Checkbox getTbl_Missing_Attendance_list() {
        return tbl_Missing_Attendance_list;
    }

    public void setTbl_Missing_Attendance_list(PIS_Missing_Attendance_Checkbox tbl_Missing_Attendance_list) {
        this.tbl_Missing_Attendance_list = tbl_Missing_Attendance_list;
    }

    private void setTbl_Missing_Attendance_list() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (userSelectionValid) {
            List<Daily_Missing_Attendance> lsList = getLst_missing_att();
            tbl_Missing_Attendance_list = new PIS_Missing_Attendance_Checkbox(lsList);
            if (!lsList.isEmpty()) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Missing Attendance", "Some missing attendance are there, please check out the table below for missing attendance"));
            }
            selectedAttendances = null;
        } else {
            tbl_Missing_Attendance_list = null;
            selectedAttendances = null;
        }
    }
    // <editor-fold defaultstate="collapsed" desc="getLst_missing_att()">

    private List<Daily_Missing_Attendance> getLst_missing_att() {
        List<Daily_Missing_Attendance> lst_missing_att = new ArrayList<Daily_Missing_Attendance>();
        if (userSelectionValid) {
            FacesContext context = FacesContext.getCurrentInstance();
            String CMD_missing_days = "SELECT MISSING_DATE, "
                    + " HALF_NO, STATEMENT_OPERATION "
                    + " FROM TABLE(FTAS_PACK.GET_MISSING_ATTENDANCE(" + selectedUser.getUserId() + ",'" + DateTimeUtility.ChangeDateFormat(new Date(), null) + "'))";
            Connection cn = null;
            try {
                Statement st = null;
                try {
                    ResultSet res = null;
                    try {
                        cn = getGlobalUtilities().getjdbcFES_PROJECT_Oracle().getConnection();
                        int i = 0;
                        st = cn.createStatement();
                        res = st.executeQuery(CMD_missing_days);
                        while (res.next()) {
                            Daily_Missing_Attendance ent_Missing_Atten1 = new Daily_Missing_Attendance(i);
                            ent_Missing_Atten1.setMissing_attendance(res.getDate("MISSING_DATE"));
                            ent_Missing_Atten1.setStatement_operation(res.getString("STATEMENT_OPERATION"));
                            ent_Missing_Atten1.setPartial_half_no(res.getInt("HALF_NO"));
                            lst_missing_att.add(i, ent_Missing_Atten1);
                            i++;
                        }
                    } catch (Exception e) {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "1Can't Find Missing Attendance", "Some unexpected error occurred during finding missing attendance"));
                        LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "getLst_missing_att()", null, e);
                    } finally {
                        res.close();
                    }
                } catch (Exception e) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "2Can't Find Missing Attendance", "Some unexpected error occurred during finding missing attendance"));
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "getLst_missing_att()", null, e);
                } finally {
                    st.close();
                }
            } catch (Exception ex) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "3Can't Find Missing Attendance", "Some unexpected error occurred during finding missing attendance"));
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "getLst_missing_att()", null, ex);
            } finally {
                if (cn != null) {
                    try {
                        if (!cn.isClosed()) {
                            cn.close();
                        }
                    } catch (Exception e) {
                    }
                }
            }
        }
        return lst_missing_att;
    }
    // </editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="selectedAttendances">
    Daily_Missing_Attendance[] selectedAttendances;

    public Daily_Missing_Attendance[] getSelectedAttendances() {
        return selectedAttendances;
    }

    public void setSelectedAttendances(Daily_Missing_Attendance[] selectedAttendances) {
        this.selectedAttendances = selectedAttendances;
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="button save missing attendance Action">
    public void btn_save_missing_att_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (selectedAttendances.length == 0) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "First select any missing attendance"));
        } else {
            try {

                String v_str_rec = "";
                for (int i = 0; i < selectedAttendances.length; i++) {
                    Daily_Missing_Attendance selectedDate = selectedAttendances[i];
                    String v_rec_list;
                    v_rec_list = DateTimeUtility.ChangeDateFormat(selectedDate.getMissing_attendance(), null).toString();
                    if (selectedDate.getPartial_half_no() == 0) {
                        v_rec_list += ":" + selectedDate.placeHalf1 + ":" + selectedDate.placeHalf2;
                    } else if (selectedDate.getPartial_half_no() == 1) {
                        v_rec_list += ":" + selectedDate.placeHalf1 + ":N";
                    } else if (selectedDate.getPartial_half_no() == 2) {
                        v_rec_list += ":N:" + selectedDate.placeHalf2;
                    }
                    v_rec_list += ":" + selectedDate.getStatement_operation();
                    v_str_rec += v_rec_list + ",";
                }
                v_str_rec = v_str_rec.substring(0, v_str_rec.length() - 1);
                try {
                    Database_Output database_Output = ftas_Pack.SetMissingAttendance(this.getClass().getName(), "btn_save_missing_att_clicked", v_str_rec, selectedUser.getUserId(), (selectedAttendances[0].getMissing_attendance().getYear() + 1900), globalData.getUser_id(), selectedUser.getPostingOumUnitSrno().getOumUnitSrno(), "Y");
                    if (database_Output.isExecuted_successfully()) {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully"));
                        tbl_Missing_Attendance_list = new PIS_Missing_Attendance_Checkbox(getLst_missing_att());
                        selectedAttendances = null;
                    } else {
                        context.addMessage(null, database_Output.getFacesMessage());
                    }
                } catch (Exception e) {
                    context.addMessage(null, JSFMessages.getErrorMessage(e));
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_save_missing_att_clicked", null, e);
                }
            } catch (Exception e) {
                context.addMessage(null, JSFMessages.getErrorMessage(e));
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_save_missing_att_clicked", null, e);
            }
        }
    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetMissingAttendance">

    private void resetMissingAttendance() {
        setTbl_Missing_Attendance_list();
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Date Time Display Settings">
    String lbl_Date_display = new String();
    String lbl_Time_display = new String();

    public String getLbl_Date_display() {
        String date_display;
        date_display = DateTimeUtility.ChangeDateFormat(new Date(), null);
        lbl_Date_display = date_display;
        return lbl_Date_display;
    }

    public void setLbl_Date_display(String lbl_Date_display) {
        this.lbl_Date_display = lbl_Date_display;
    }

    public String getLbl_Time_display() {
        FacesContext context = FacesContext.getCurrentInstance();
        Database_Output ent_Database_Output = ou_Pack.GetSysTime(
                this.getClass().getName(), "getLbl_Time_display");
        if (ent_Database_Output.isExecuted_successfully()) {
            lbl_Time_display = ent_Database_Output.getDate1().toString();
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some error occured during getting Native Ou"));
        }
        return lbl_Time_display;
    }

    public void setLbl_Time_display(String lbl_Time_display) {
        this.lbl_Time_display = lbl_Time_display;
    }
    //</editor-fold>
    ///<editor-fold defaultstate="collapsed" desc="User Selectable List Settings">
    //<editor-fold defaultstate="collapsed" desc="selectedUserFromList">
    FhrdEmpmst selectedUserFromList;

    public FhrdEmpmst getSelectedUserFromList() {
        return selectedUserFromList;
    }

    public void setSelectedUserFromList(FhrdEmpmst selectedUserFromList) {
        this.selectedUserFromList = selectedUserFromList;
        err_txt_user_id = null;
        if (selectedUserFromList != null) {
            selectedUser = selectedUserFromList;
            userSelected();
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lst_employee_selectable">
    SD_FhrdEmpmst lst_employee_selectable;

    public SD_FhrdEmpmst getLst_employee_selectable() {
        return lst_employee_selectable;
    }

    public void setLst_employee_selectable(SD_FhrdEmpmst lst_employee_selectable) {
        this.lst_employee_selectable = lst_employee_selectable;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lnk_view_emplist">
    public void lnk_view_emplist(ActionEvent event) {
        boolean isSysUser = getPIS_GlobalSettings().getSysetemUser_Fhrd();
        lst_employee_selectable = new SD_FhrdEmpmst(fhrdEmpmstFacade.findAll(globalData.getWorking_ou(), false, false, false, null, isSysUser ? null : globalData.getUser_id(), null, true, true, false, null));
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="URL Redirection Settings">
    //<editor-fold defaultstate="collapsed" desc="redirectUrl">
    String redirectUrl;

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        String refUrl = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().isEmpty() ? null : FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("ref_url");
        if (globalData.getUser_id() == 1) {
            redirectUrl = refUrl == null ? null : getGlobalResources().getProject_root_url() + "PIS/pages/tas/PIS_Approval_Leave_Application.htm?ref_url=" + refUrl;
        } else {
            redirectUrl = refUrl == null ? null : getGlobalResources().getProject_root_url() + refUrl;
        }
        setRdb_place_options();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Default Constructor & Other Beans Methods">
    // <editor-fold defaultstate="collapsed" desc="default constructor">

    /**
     * Creates a new instance of PIS_Daily_Attendance
     */
    public PIS_Daily_Attendance() {
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Resources">

    protected GlobalResources getGlobalResources() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalResources) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalResources");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
    //</editor-fold>
}
