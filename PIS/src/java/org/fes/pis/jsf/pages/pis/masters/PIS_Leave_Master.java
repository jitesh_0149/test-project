/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.pis.masters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.FtasLeavemst;
import org.fes.pis.entities.SystOrgUnitMst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FtasLeavemstFacadeLocal;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Leave_Master")
@ViewScoped
public class PIS_Leave_Master implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="Declarations">
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="EJB Declaration">
    @EJB
    FtasLeavemstFacadeLocal ftasLeavemstFacade;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Objects Declaration">
    //<editor-fold defaultstate="collapsed" desc="ddl_print_order">
    String ddl_print_order;
    List<SelectItem> ddl_print_order_options;

    public String getDdl_print_order() {
        return ddl_print_order;
    }

    public void setDdl_print_order(String ddl_print_order) {
        this.ddl_print_order = ddl_print_order;
    }

    public List<SelectItem> getDdl_print_order_options() {
        return ddl_print_order_options;
    }

    public void setDdl_print_order_options(List<SelectItem> ddl_print_order_options) {
        this.ddl_print_order_options = ddl_print_order_options;
    }

    public void setDdl_print_order() {
        ddl_print_order_options = new ArrayList<SelectItem>();
        List<BigDecimal> lst_temp = ftasLeavemstFacade.findAvailablePrintOrders(globalData.getWorking_ou());
        ddl_print_order_options.add(new SelectItem(null, "-- None --"));
        for (BigDecimal b : lst_temp) {
            ddl_print_order_options.add(new SelectItem(b.toString()));
        }
        ddl_print_order = null;
    }
    //</editor-fold>
    String txt_leave_code;
    String txt_leave_description;
    String txt_short_name;
    String ddl_leave_flag;
    String ddl_actual_attendance;
    String ddl_consider_attendance_flag;
    String ddl_print_payslip;
    String ddl_virtual_leave;
    String ddl_application_required;
    String ddl_parentable;
    String ddl_ttr_parentable;
    Date cal_apply_date;
    Date cal_expire_date;
    String cal_expire_date_min;
    String ddl_consider_actual_attendance;
    String ddl_holiday;
    String ddl_multiple_rule;
    String ddl_print_if_0_cons;
    String ddl_resign_relieve_cons_flag;

    // <editor-fold defaultstate="collapsed" desc="getter/setter methods">
    public String getTxt_leave_code() {
        return txt_leave_code;
    }

    public void setTxt_leave_code(String txt_leave_code) {
        this.txt_leave_code = txt_leave_code;
    }

    public String getDdl_multiple_rule() {
        return ddl_multiple_rule;
    }

    public void setDdl_multiple_rule(String ddl_multiple_rule) {
        this.ddl_multiple_rule = ddl_multiple_rule;
    }

    public String getDdl_holiday() {
        return ddl_holiday;
    }

    public void setDdl_holiday(String ddl_holiday) {
        this.ddl_holiday = ddl_holiday;
    }

    public String getDdl_consider_actual_attendance() {
        return ddl_consider_actual_attendance;
    }

    public void setDdl_consider_actual_attendance(String ddl_consider_actual_attendance) {
        this.ddl_consider_actual_attendance = ddl_consider_actual_attendance;
    }

    public String getDdl_ttr_parentable() {
        return ddl_ttr_parentable;
    }

    public void setDdl_ttr_parentable(String ddl_ttr_parentable) {
        this.ddl_ttr_parentable = ddl_ttr_parentable;
    }

    public String getTxt_short_name() {
        return txt_short_name;
    }

    public void setTxt_short_name(String txt_short_name) {
        this.txt_short_name = txt_short_name;
    }

    public String getTxt_leave_description() {
        return txt_leave_description;
    }

    public void setTxt_leave_description(String txt_leave_description) {
        this.txt_leave_description = txt_leave_description;
    }

    public String getDdl_actual_attendance() {
        return ddl_actual_attendance;
    }

    public void setDdl_actual_attendance(String ddl_actual_attendance) {
        this.ddl_actual_attendance = ddl_actual_attendance;
    }

    public String getDdl_application_required() {
        return ddl_application_required;
    }

    public void setDdl_application_required(String ddl_application_required) {
        this.ddl_application_required = ddl_application_required;
    }

    public String getDdl_consider_attendance_flag() {
        return ddl_consider_attendance_flag;
    }

    public void setDdl_consider_attendance_flag(String ddl_consider_attendance_flag) {
        this.ddl_consider_attendance_flag = ddl_consider_attendance_flag;
    }

    public String getDdl_leave_flag() {
        return ddl_leave_flag;
    }

    public void setDdl_leave_flag(String ddl_leave_flag) {
        this.ddl_leave_flag = ddl_leave_flag;
    }

    public String getDdl_print_payslip() {
        return ddl_print_payslip;
    }

    public void setDdl_print_payslip(String ddl_print_payslip) {
        this.ddl_print_payslip = ddl_print_payslip;
    }

    public String getDdl_virtual_leave() {
        return ddl_virtual_leave;
    }

    public void setDdl_virtual_leave(String ddl_virtual_leave) {
        this.ddl_virtual_leave = ddl_virtual_leave;
    }

    public Date getCal_apply_date() {
        return cal_apply_date;
    }

    public void setCal_apply_date(Date cal_apply_date) {
        this.cal_apply_date = cal_apply_date;
    }

    public Date getCal_expire_date() {
        return cal_expire_date;
    }

    public void setCal_expire_date(Date cal_expire_date) {
        this.cal_expire_date = cal_expire_date;
    }

    public String getCal_expire_date_min() {
        return cal_expire_date_min;
    }

    public void setCal_expire_date_min(String cal_expire_date_min) {
        this.cal_expire_date_min = cal_expire_date_min;
    }

    public String getDdl_parentable() {
        return ddl_parentable;
    }

    public void setDdl_parentable(String ddl_parentable) {
        this.ddl_parentable = ddl_parentable;
    }

    public String getDdl_print_if_0_cons() {
        return ddl_print_if_0_cons;
    }

    public void setDdl_print_if_0_cons(String ddl_print_if_0_cons) {
        this.ddl_print_if_0_cons = ddl_print_if_0_cons;
    }

    public String getDdl_resign_relieve_cons_flag() {
        return ddl_resign_relieve_cons_flag;
    }

    public void setDdl_resign_relieve_cons_flag(String ddl_resign_relieve_cons_flag) {
        this.ddl_resign_relieve_cons_flag = ddl_resign_relieve_cons_flag;
    }
    // </editor-fold>
    // </editor-fold>       
    // <editor-fold defaultstate="collapsed" desc="tbl_leavemst">
    List<FtasLeavemst> tbl_leavemst;
    List<FtasLeavemst> tbl_leavemst_filtered;

    public List<FtasLeavemst> getTbl_leavemst_filtered() {
        return tbl_leavemst_filtered;
    }

    public void setTbl_leavemst_filtered(List<FtasLeavemst> tbl_leavemst_filtered) {
        this.tbl_leavemst_filtered = tbl_leavemst_filtered;
    }

    public List<FtasLeavemst> getTbl_leavemst() {
        return tbl_leavemst;
    }

    public void setTbl_leavemst(List<FtasLeavemst> tbl_leavemst) {
        this.tbl_leavemst = tbl_leavemst;
    }

    public void setTblLeaveMst() {
        tbl_leavemst = ftasLeavemstFacade.findAll(globalData.getWorking_ou());
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_code_alternative">
    String ddl_code_alternative;
    List<SelectItem> ddl_code_alternative_options;

    public String getDdl_code_alternative() {
        return ddl_code_alternative;
    }

    public void setDdl_code_alternative(String ddl_code_alternative) {
        this.ddl_code_alternative = ddl_code_alternative;
    }

    public List<SelectItem> getDdl_code_alternative_options() {
        return ddl_code_alternative_options;
    }

    public void setDdl_code_alternative_options(List<SelectItem> ddl_code_alternative_options) {
        this.ddl_code_alternative_options = ddl_code_alternative_options;
    }

    public void setDdl_code_alternative() {
        List<FtasLeavemst> lstFtasLeavemsts = ftasLeavemstFacade.findAll(globalData.getWorking_ou());
        ddl_code_alternative_options = new ArrayList<SelectItem>();
        if (lstFtasLeavemsts.isEmpty()) {
            SelectItem s = new SelectItem(null, "--- Not Availalble ---");
            ddl_code_alternative_options.add(s);
        } else {
            SelectItem s = new SelectItem(null, "--- Select ---");
            ddl_code_alternative_options.add(s);
            Integer n = lstFtasLeavemsts.size();
            for (int i = 0; i < n; i++) {
                s = new SelectItem(lstFtasLeavemsts.get(i).getLmSrgKey().toString(), lstFtasLeavemsts.get(i).getLeaveDesc());
                ddl_code_alternative_options.add(s);
            }
        }
        ddl_code_alternative = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="entFtasLeavemst_detail"> 
    FtasLeavemst entFtasLeavemst_detail;

    public FtasLeavemst getEntFtasLeavemst_detail() {
        return entFtasLeavemst_detail;
    }

    public void setEntFtasLeavemst_detail(FtasLeavemst entFtasLeavemst_detail) {
        this.entFtasLeavemst_detail = entFtasLeavemst_detail;
    }

    public void btn_view_deatil_clicked(FtasLeavemst ftasLeavemst) {
        entFtasLeavemst_detail = ftasLeavemst;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="button Save action event">

    public void btn_save_action(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if (txt_leave_code.trim().isEmpty()
                    || txt_leave_description.trim().isEmpty()
                    || cal_apply_date == null) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "Fill All Requierd Entries"));
                return;
            }
//            BigDecimal v_lm_srgkey = getGlobalSettings().getUniqueSrno(null, "FTAS_LEAVEMST");
            FhrdEmpmst ent_FhrdEmpmst = new FhrdEmpmst(globalData.getUser_id());
            SystOrgUnitMst ent_SystOrgUnitMst = new SystOrgUnitMst(globalData.getWorking_ou());
            FtasLeavemst ent_FtasLeavemst = new FtasLeavemst();
            ent_FtasLeavemst.setLmSrgKey(new BigDecimal("1"));
            ent_FtasLeavemst.setLeavecd(txt_leave_code.trim().toUpperCase());
            ent_FtasLeavemst.setOumUnitSrno(ent_SystOrgUnitMst);
            ent_FtasLeavemst.setLeaveDesc(txt_leave_description.trim().toUpperCase());
            ent_FtasLeavemst.setShortName(txt_short_name.trim().toUpperCase());
            ent_FtasLeavemst.setLeaveFlag(ddl_leave_flag);
            ent_FtasLeavemst.setActualAttendanceFlag(ddl_actual_attendance);
            ent_FtasLeavemst.setApplicationRequired(ddl_application_required);
            ent_FtasLeavemst.setConsiderAsAttendanceFlag(ddl_consider_attendance_flag);
            ent_FtasLeavemst.setPrintInPaySlipFlag(ddl_print_payslip);
            ent_FtasLeavemst.setPrintIf0Cons(ddl_print_if_0_cons.toUpperCase());
            ent_FtasLeavemst.setResignRelieveConsFlag(ddl_resign_relieve_cons_flag.toUpperCase());
            ent_FtasLeavemst.setVirtualLeave(ddl_virtual_leave);
            ent_FtasLeavemst.setParentable(ddl_parentable);
            ent_FtasLeavemst.setConsiderAsActualAttdFlag(ddl_consider_actual_attendance);
            ent_FtasLeavemst.setHolidayFlag(ddl_holiday);
            ent_FtasLeavemst.setMultipleRuleFlag(ddl_multiple_rule);
            ent_FtasLeavemst.setTTrParent(ddl_ttr_parentable);
            ent_FtasLeavemst.setStartDate(cal_apply_date);
            ent_FtasLeavemst.setEndDate(cal_expire_date);

            if (ddl_print_order != null) {
                ent_FtasLeavemst.setPrintOrder(Short.valueOf(ddl_print_order));
            }
            if (ddl_code_alternative != null) {
                ent_FtasLeavemst.setLmAltSrno(ftasLeavemstFacade.find(new BigDecimal(ddl_code_alternative)).getLmAltSrno());
            }
            ent_FtasLeavemst.setCreateby(ent_FhrdEmpmst);
            ent_FtasLeavemst.setCreatedt(new Date());
            try {
                ftasLeavemstFacade.create(ent_FtasLeavemst);
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully"));
                setTblLeaveMst();
                ddl_actual_attendance = null;
                ddl_application_required = null;
                ddl_consider_attendance_flag = null;
                ddl_code_alternative = null;
                ddl_consider_actual_attendance = null;
                ddl_holiday = null;
                ddl_ttr_parentable = null;
                ddl_multiple_rule = null;
                ddl_leave_flag = null;
                ddl_parentable = null;
                ddl_print_payslip = null;
                ddl_virtual_leave = null;
                txt_leave_code = null;
                txt_leave_description = null;
                ddl_print_order = null;
                txt_short_name = null;
                cal_apply_date = null;
                cal_expire_date = null;
                ddl_resign_relieve_cons_flag = null;
                ddl_print_if_0_cons = null;
                setDdl_print_order();
            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occerred during saving record"));
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_save_action", null, e);
            }
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Record not saved due to some wrong Information..."));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_save_action", null, e);
        }
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="txt_leave_code_changed">

    public void txt_leave_code_changed(ValueChangeEvent event) {
        txt_leave_code = event.getNewValue().toString();
        validateLeaveCode();
    }

    private boolean validateLeaveCode() {
        if (ftasLeavemstFacade.isLeaveCodeExist(globalData.getWorking_ou(), txt_leave_code.toString().toUpperCase(), cal_apply_date, cal_expire_date)) {
            RequestContext.getCurrentInstance().execute("$('#err_txt_leave_code').text('Leave code already available in given period')");
            return false;
        }
        return true;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="cal_date_changed">

    public void cal_date_changed(SelectEvent event) {
        cal_apply_date = (Date) event.getObject();
        cal_expire_date_min = DateTimeUtility.ChangeDateFormat(cal_apply_date, null);
        if (cal_expire_date != null && cal_expire_date.before(cal_apply_date)) {
            cal_expire_date = null;
        }
    }// </editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="lst_virtual_options"> 
    SelectItem[] lst_virtual_options;

    public SelectItem[] getLst_virtual_options() {
        return lst_virtual_options;
    }

    public void setLst_virtual_options(SelectItem[] lst_virtual_options) {
        this.lst_virtual_options = lst_virtual_options;
    }

    public void setLst_virtual_options() {
        lst_virtual_options = new SelectItem[4];
        lst_virtual_options[0] = new SelectItem("");
        lst_virtual_options[1] = new SelectItem("L", "Yes for Leave");
        lst_virtual_options[2] = new SelectItem("T", "Yes for Tour-Training");
        lst_virtual_options[3] = new SelectItem("N", "No");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lst_yes_no_options">
    SelectItem[] lst_yes_no_options;

    public SelectItem[] getLst_yes_no_options() {
        return lst_yes_no_options;
    }

    public void setLst_yes_no_options(SelectItem[] lst_yes_no_options) {
        this.lst_yes_no_options = lst_yes_no_options;
    }

    public void setLst_yes_no_options() {
        lst_yes_no_options = new SelectItem[3];
        lst_yes_no_options[0] = new SelectItem("");
        lst_yes_no_options[1] = new SelectItem("Y", "Yes");
        lst_yes_no_options[2] = new SelectItem("N", "No");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_alternatives"> 
    List<FtasLeavemst> tbl_alternatives;

    public List<FtasLeavemst> getTbl_alternatives() {
        return tbl_alternatives;
    }

    public void setTbl_alternatives(List<FtasLeavemst> tbl_alternatives) {
        this.tbl_alternatives = tbl_alternatives;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_view_alternatives"> 

    public void btn_view_alternatives(FtasLeavemst ftasLeavemst) {
        tbl_alternatives = ftasLeavemstFacade.findAllAlternatives(globalData.getWorking_ou(), ftasLeavemst.getLmSrgKey());
        RequestContext.getCurrentInstance().execute("pw_dlg_alter_leave_masters.show()");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Apply End Date Settings">

    public void btn_apply_date_action(ActionEvent event) {
    }
    FtasLeavemst entFtasLeavemst_edit;

    public FtasLeavemst getEntFtasLeavemst_edit() {
        return entFtasLeavemst_edit;
    }

    public void setEntFtasLeavemst_edit(FtasLeavemst entFtasLeavemst_edit) {
        this.entFtasLeavemst_edit = entFtasLeavemst_edit;
        cal_apply_end_date_min = DateTimeUtility.ChangeDateFormat(entFtasLeavemst_edit.getStartDate(), null);
        cal_apply_end_date = null;
        RequestContext.getCurrentInstance().execute("pw_dlg_apply_end_date.show();");
    }
    Date cal_apply_end_date;
    String cal_apply_end_date_min;

    public String getCal_apply_end_date_min() {
        return cal_apply_end_date_min;
    }

    public void setCal_apply_end_date_min(String cal_apply_end_date_min) {
        this.cal_apply_end_date_min = cal_apply_end_date_min;
    }

    public Date getCal_apply_end_date() {
        return cal_apply_end_date;
    }

    public void setCal_apply_end_date(Date cal_apply_end_date) {
        this.cal_apply_end_date = cal_apply_end_date;
    }

    public void btn_apply_end_date_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        BigDecimal v_ed_srg_key = entFtasLeavemst_edit.getLmSrgKey();
        FtasLeavemst entFtasLeavemst = ftasLeavemstFacade.find(v_ed_srg_key);
        entFtasLeavemst.setEndDate(cal_apply_end_date);
        entFtasLeavemst.setUpdateby(globalData.getEnt_login_user());
        entFtasLeavemst.setUpdatedt(new Date());
        try {
            ftasLeavemstFacade.edit(entFtasLeavemst);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record updated Successfully"));
            setTblLeaveMst();
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some error occured during updating Rule"));
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_apply_end_date_clicked", null, e);
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        cal_apply_date = new Date();
        cal_expire_date_min = DateTimeUtility.ChangeDateFormat(cal_apply_date, null);
        setDdl_print_order();
        setLst_yes_no_options();
        setLst_virtual_options();
        setTblLeaveMst();
        setDdl_code_alternative();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Default Constructor & Other Beans">
    // <editor-fold defaultstate="collapsed" desc="default functions">

    /**
     * Creates a new instance of PIS_Leave_Master
     */
    public PIS_Leave_Master() {
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    //</editor-fold>
}
