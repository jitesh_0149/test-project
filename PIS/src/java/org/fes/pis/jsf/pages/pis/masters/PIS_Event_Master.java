package org.fes.pis.jsf.pages.pis.masters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.FhrdEventMst;
import org.fes.pis.entities.SystOrgUnitMst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;
import org.fes.pis.sessions.FhrdEventMstFacadeLocal;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Event_Master")
@ViewScoped
public class PIS_Event_Master implements Serializable {

    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    //<editor-fold defaultstate="collapsed" desc="declarations">
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    // </editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="EJB Declaration">
    @EJB
    private FhrdEventMstFacadeLocal fhrdEventMstFacade;

    /**
     * Creates a new instance of PYRL_Event_Master
     */
    public PIS_Event_Master() {
        ent_fhrdEventMst = null;
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tbl_event_master">
    List<FhrdEventMst> tbl_event_master;
    List<FhrdEventMst> tbl_event_master_filtered;

    public List<FhrdEventMst> getTbl_event_master_filtered() {
        return tbl_event_master_filtered;
    }

    public void setTbl_event_master_filtered(List<FhrdEventMst> tbl_event_master_filtered) {
        this.tbl_event_master_filtered = tbl_event_master_filtered;
    }

    public List<FhrdEventMst> getTbl_event_master() {
        return tbl_event_master;
    }

    public void setTbl_event_master(List<FhrdEventMst> tbl_event_master) {
        this.tbl_event_master = tbl_event_master;
    }

    public void setTblEventMst() {
        List<FhrdEventMst> lst_event_mst = fhrdEventMstFacade.findAll(globalData.getWorking_ou());
        Integer n = lst_event_mst.size();
        for (int i = 0; i < n; i++) {
            if (lst_event_mst.get(i).getEventType().equalsIgnoreCase("O")) {
                lst_event_mst.get(i).setEventType("OPTIONAL");
            } else if (lst_event_mst.get(i).getEventType().equalsIgnoreCase("C")) {
                lst_event_mst.get(i).setEventType("COMPULSORY");
            } else if (lst_event_mst.get(i).getEventType().equalsIgnoreCase("R")) {
                lst_event_mst.get(i).setEventType("REGULAR");
            }
        }
        tbl_event_master = lst_event_mst;
    }
    // </editor-fold> 
    // <editor-fold defaultstate="collapsed" desc="Event Detail Entity">     
    FhrdEventMst ent_fhrdEventMst = new FhrdEventMst();

    public FhrdEventMst getEnt_fhrdEventMst() {
        return ent_fhrdEventMst;
    }

    public void setEnt_fhrdEventMst(FhrdEventMst ent_fhrdEventMst) {
        if (ent_fhrdEventMst != null) {
            if (ent_fhrdEventMst.getEventType().toString().equalsIgnoreCase("C")) {
                ent_fhrdEventMst.setEventType("COMPULSORY");
            } else if (ent_fhrdEventMst.getEventType().toString().equalsIgnoreCase("R")) {
                ent_fhrdEventMst.setEventType("REGULER");
            } else if (ent_fhrdEventMst.getEventType().toString().equalsIgnoreCase("O")) {
                ent_fhrdEventMst.setEventType("OPTIONAL");
            }

            if (ent_fhrdEventMst.getEventPeriodRequiredFlag().toString().equalsIgnoreCase("Y")) {
                ent_fhrdEventMst.setEventPeriodRequiredFlag("YES");
            } else if (ent_fhrdEventMst.getEventPeriodRequiredFlag().toString().equalsIgnoreCase("N")) {
                ent_fhrdEventMst.setEventPeriodRequiredFlag("NO");
            }

            if (ent_fhrdEventMst.getAlertStartPeriodUnit() != null) {
                if (ent_fhrdEventMst.getAlertStartPeriodUnit().toString().equalsIgnoreCase("D")) {
                    ent_fhrdEventMst.setAlertStartPeriodUnit("DAY");
                } else if (ent_fhrdEventMst.getAlertStartPeriodUnit().toString().equalsIgnoreCase("M")) {
                    ent_fhrdEventMst.setAlertStartPeriodUnit("MONTH");
                } else if (ent_fhrdEventMst.getAlertStartPeriodUnit().toString().equalsIgnoreCase("Y")) {
                    ent_fhrdEventMst.setAlertStartPeriodUnit("YEAR");
                }
            }
            if (ent_fhrdEventMst.getEventTwoAlertsGapUnit() != null) {
                if (ent_fhrdEventMst.getEventTwoAlertsGapUnit().toString().equalsIgnoreCase("D")) {
                    ent_fhrdEventMst.setEventTwoAlertsGapUnit("DAY");
                } else if (ent_fhrdEventMst.getEventTwoAlertsGapUnit().toString().equalsIgnoreCase("M")) {
                    ent_fhrdEventMst.setEventTwoAlertsGapUnit("MONTH");
                } else if (ent_fhrdEventMst.getEventTwoAlertsGapUnit().toString().equalsIgnoreCase("Y")) {
                    ent_fhrdEventMst.setEventTwoAlertsGapUnit("YEAR");
                }
            }
            if (ent_fhrdEventMst.getEventNameColumnUse().toString().equalsIgnoreCase("S")) {
                ent_fhrdEventMst.setEventNameColumnUse("For System Users");
            } else if (ent_fhrdEventMst.getEventNameColumnUse().toString().equalsIgnoreCase("U")) {
                ent_fhrdEventMst.setEventNameColumnUse("For Normal Users");
            }
        }
        this.ent_fhrdEventMst = ent_fhrdEventMst;
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="binding Attributes">
    //<editor-fold defaultstate="collapsed" desc="ddl_alert_rece_person">
    String ddl_alert_rece_person;
    List<SelectItem> ddl_alert_rece_person_options;

    public String getDdl_alert_rece_person() {
        return ddl_alert_rece_person;
    }

    public void setDdl_alert_rece_person(String ddl_alert_rece_person) {
        this.ddl_alert_rece_person = ddl_alert_rece_person;
    }

    public List<SelectItem> getDdl_alert_rece_person_options() {
        return ddl_alert_rece_person_options;
    }

    public void setDdl_alert_rece_person_options(List<SelectItem> ddl_alert_rece_person_options) {
        this.ddl_alert_rece_person_options = ddl_alert_rece_person_options;
    }

    private void setDdl_alert_rece_person() {
        List<FhrdEmpmst> lst_temp = fhrdEmpmstFacade.findAll(globalData.getWorking_ou(), false, false, false, null, null, null, false, false, false, null);
        ddl_alert_rece_person_options = new ArrayList<SelectItem>();
        ddl_alert_rece_person_options.add(new SelectItem(null, "--- Select  ---"));
        for (FhrdEmpmst f : lst_temp) {
            ddl_alert_rece_person_options.add(new SelectItem(f.getUserId(), f.getUserName()));
        }
        ddl_alert_rece_person = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_alert_rece_person_alt">
    String ddl_alert_rece_person_alt;
    List<SelectItem> ddl_alert_rece_person_alt_options;

    public String getDdl_alert_rece_person_alt() {
        return ddl_alert_rece_person_alt;
    }

    public void setDdl_alert_rece_person_alt(String ddl_alert_rece_person_alt) {
        this.ddl_alert_rece_person_alt = ddl_alert_rece_person_alt;
    }

    public List<SelectItem> getDdl_alert_rece_person_alt_options() {
        return ddl_alert_rece_person_alt_options;
    }

    public void setDdl_alert_rece_person_alt_options(List<SelectItem> ddl_alert_rece_person_alt_options) {
        this.ddl_alert_rece_person_alt_options = ddl_alert_rece_person_alt_options;
    }

    private void setDdl_alert_rece_person_alt() {
        ddl_alert_rece_person_alt_options = ddl_alert_rece_person_options;
        ddl_alert_rece_person_alt = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Other Objects">
    String txt_event_name;
    String txt_event_desc;
    String ddl_event_type;
    String txt_event_message;
    String txt_event_order;
    String txt_alert_start_period;
    String ddl_alert_start_unit;
    String txt_alert_two_period;
    String ddl_alert_two_unit;
    String ddl_event_name_column_use;
    String txt_event_column_use_desc;
    String ddl_event_period_req_flag;
    String ddl_event_effective_time;
    // <editor-fold defaultstate="collapsed" desc="getter/setter">

    public String getDdl_event_effective_time() {
        return ddl_event_effective_time;
    }

    public void setDdl_event_effective_time(String ddl_event_effective_time) {
        this.ddl_event_effective_time = ddl_event_effective_time;
    }

    public String getDdl_event_period_req_flag() {
        return ddl_event_period_req_flag;
    }

    public void setDdl_event_period_req_flag(String ddl_event_period_req_flag) {
        this.ddl_event_period_req_flag = ddl_event_period_req_flag;
    }

    public String getTxt_alert_start_period() {
        return txt_alert_start_period;
    }

    public void setTxt_alert_start_period(String txt_alert_start_period) {
        this.txt_alert_start_period = txt_alert_start_period;
    }

    public String getTxt_alert_two_period() {
        return txt_alert_two_period;
    }

    public void setTxt_alert_two_period(String txt_alert_two_period) {
        this.txt_alert_two_period = txt_alert_two_period;
    }

    public String getDdl_alert_start_unit() {
        return ddl_alert_start_unit;
    }

    public void setDdl_alert_start_unit(String ddl_alert_start_unit) {
        this.ddl_alert_start_unit = ddl_alert_start_unit;
    }

    public String getDdl_alert_two_unit() {
        return ddl_alert_two_unit;
    }

    public void setDdl_alert_two_unit(String ddl_alert_two_unit) {
        this.ddl_alert_two_unit = ddl_alert_two_unit;
    }

    public String getTxt_event_column_use_desc() {
        return txt_event_column_use_desc;
    }

    public void setTxt_event_column_use_desc(String txt_event_column_use_desc) {
        this.txt_event_column_use_desc = txt_event_column_use_desc;
    }

    public String getTxt_event_desc() {
        return txt_event_desc;
    }

    public void setTxt_event_desc(String txt_event_desc) {
        this.txt_event_desc = txt_event_desc;
    }

    public String getTxt_event_message() {
        return txt_event_message;
    }

    public void setTxt_event_message(String txt_event_message) {
        this.txt_event_message = txt_event_message;
    }

    public String getTxt_event_name() {
        return txt_event_name;
    }

    public void setTxt_event_name(String txt_event_name) {
        this.txt_event_name = txt_event_name;
    }

    public String getDdl_event_name_column_use() {
        return ddl_event_name_column_use;
    }

    public void setDdl_event_name_column_use(String ddl_event_name_column_use) {
        this.ddl_event_name_column_use = ddl_event_name_column_use;
    }

    public String getTxt_event_order() {
        return txt_event_order;
    }

    public void setTxt_event_order(String txt_event_order) {
        this.txt_event_order = txt_event_order;
    }

    public String getDdl_event_type() {
        return ddl_event_type;
    }

    public void setDdl_event_type(String ddl_event_type) {
        this.ddl_event_type = ddl_event_type;
    }
    // </editor-fold>
    //</editor-fold>
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="error binding attributes"> 
    String err_event_name;
    String err_event_desc;
    String err_event_type;
    String err_event_period_req_flag;
    String err_event_message;
    String err_event_name_column_use;
    String err_event_column_use_desc;

    public String getErr_event_column_use_desc() {
        return err_event_column_use_desc;
    }

    public void setErr_event_column_use_desc(String err_event_column_use_desc) {
        this.err_event_column_use_desc = err_event_column_use_desc;
    }

    public String getErr_event_desc() {
        return err_event_desc;
    }

    public void setErr_event_desc(String err_event_desc) {
        this.err_event_desc = err_event_desc;
    }

    public String getErr_event_message() {
        return err_event_message;
    }

    public void setErr_event_message(String err_event_message) {
        this.err_event_message = err_event_message;
    }

    public String getErr_event_name() {
        return err_event_name;
    }

    public void setErr_event_name(String err_event_name) {
        this.err_event_name = err_event_name;
    }

    public String getErr_event_name_column_use() {
        return err_event_name_column_use;
    }

    public void setErr_event_name_column_use(String err_event_name_column_use) {
        this.err_event_name_column_use = err_event_name_column_use;
    }

    public String getErr_event_period_req_flag() {
        return err_event_period_req_flag;
    }

    public void setErr_event_period_req_flag(String err_event_period_req_flag) {
        this.err_event_period_req_flag = err_event_period_req_flag;
    }

    public String getErr_event_type() {
        return err_event_type;
    }

    public void setErr_event_type(String err_event_type) {
        this.err_event_type = err_event_type;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="btn_add_new_clicked">

    public void btn_add_new_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            BigDecimal v_em_srgkey = getGlobalSettings().getUniqueSrno(null, "FHRD_EVENT_MST");
            FhrdEventMst ent_EventMst = new FhrdEventMst(v_em_srgkey);
            ent_EventMst.setEventName(txt_event_name.trim().toUpperCase());
            ent_EventMst.setEventDesc(txt_event_desc.trim().toUpperCase());
            ent_EventMst.setEventType(ddl_event_type);
            ent_EventMst.setEventPeriodRequiredFlag(ddl_event_period_req_flag);
            ent_EventMst.setEventMessage(txt_event_message.trim().toUpperCase());
            ent_EventMst.setEventNameColumnUse(ddl_event_name_column_use);
            ent_EventMst.setEventNameColumnUseDesc(txt_event_column_use_desc.trim().toUpperCase());
            ent_EventMst.setCreateby(new FhrdEmpmst(globalData.getUser_id()));
            ent_EventMst.setCreatedt(new Date());
            ent_EventMst.setOumUnitSrno(new SystOrgUnitMst(globalData.getWorking_ou()));
            if (!txt_event_order.isEmpty()) {
                ent_EventMst.setEventOrder(Short.valueOf(txt_event_order));
            }
            if (!txt_alert_start_period.isEmpty()) {
                ent_EventMst.setAlertStartPeriodNo(Integer.valueOf(txt_alert_start_period));
            }
            if (ddl_alert_start_unit != null) {
                ent_EventMst.setAlertStartPeriodUnit(ddl_alert_start_unit);
            }
            if (!txt_alert_two_period.isEmpty()) {
                ent_EventMst.setEventTwoAlertsGapNo(Integer.valueOf(txt_alert_two_period));
            }
            if (ddl_alert_two_unit != null) {
                ent_EventMst.setEventTwoAlertsGapUnit(ddl_alert_two_unit);
            }
            if (ddl_alert_rece_person == null) {
                ent_EventMst.setAlertReceivingPerson(new FhrdEmpmst(new Integer(ddl_alert_rece_person)));
            }
            if (ddl_alert_rece_person_alt == null) {
                ent_EventMst.setAlertReceivingPerson2(new FhrdEmpmst(new Integer(ddl_alert_rece_person_alt)));
            }
            ent_EventMst.setEventEffectTime(ddl_event_effective_time);
            try {
                fhrdEventMstFacade.create(ent_EventMst);
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully"));
                setTblEventMst();
                ddl_alert_start_unit = null;
                ddl_alert_two_unit = null;
                ddl_event_name_column_use = null;
                ddl_event_period_req_flag = null;
                ddl_event_type = null;
                ddl_alert_rece_person = "";
                txt_alert_start_period = "";
                txt_alert_two_period = "";
                txt_event_column_use_desc = "";
                txt_event_desc = "";
                txt_event_message = "";
                txt_event_name = "";
                txt_event_order = "";

            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occerred during saving records"));
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_add_new_clicked", null, e);

            }
        } catch (Exception ex) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Record not saved."));
            LogGenerator.generateLog(systemName, Level.SEVERE, PIS_Event_Master.class.getName(), "btn_add_new_clicked", null, ex);
        }

    }// </editor-fold>  
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lst_yes_no_options">
    SelectItem[] lst_yes_no_options;

    public SelectItem[] getLst_yes_no_options() {
        return lst_yes_no_options;
    }

    public void setLst_yes_no_options(SelectItem[] lst_yes_no_options) {
        this.lst_yes_no_options = lst_yes_no_options;
    }

    public void setLst_yes_no_options() {
        lst_yes_no_options = new SelectItem[3];
        lst_yes_no_options[0] = new SelectItem("");
        lst_yes_no_options[1] = new SelectItem("Y", "Yes");
        lst_yes_no_options[2] = new SelectItem("N", "No");
    }

    //</editor-fold>
    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setTblEventMst();
        setLst_yes_no_options();
        setDdl_alert_rece_person();
        setDdl_alert_rece_person_alt();
    }
}
