/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.view.pis;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;
import org.primefaces.context.RequestContext;

/**
 *
 * @author jitesh
 */
@ManagedBean(name = "PIS_View_Resign_Relieve")
@ViewScoped
public class PIS_View_Resign_Relieve implements Serializable {

    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;

    
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>  
    //<editor-fold defaultstate="collapsed" desc="ddl_status">
    String ddl_status;

    public String getDdl_status() {
        return ddl_status;
    }

    public void setDdl_status(String ddl_status) {
        this.ddl_status = ddl_status;
    }

    public void ddl_status_changed(ValueChangeEvent event) {
        ddl_status = event.getNewValue() == null ? null : event.getNewValue().toString();
        setTblEmpInfo();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_emp_mst"> 
    List<FhrdEmpmst> tbl_emp_info;

    public List<FhrdEmpmst> getTbl_emp_info() {
        return tbl_emp_info;
    }

    public void setTbl_emp_info(List<FhrdEmpmst> tbl_emp_info) {
        this.tbl_emp_info = tbl_emp_info;
    }

    public void setTblEmpInfo() {
        if (globalData.isSystemUserFHRD()) {
            tbl_emp_info = fhrdEmpmstFacade.findAll(globalData.getWorking_ou(), false, false, false, null, null, ddl_status, false, false, false, null);
        } else {
            tbl_emp_info = fhrdEmpmstFacade.findAll(globalData.getWorking_ou(), false, false, true, null, globalData.getUser_id(), ddl_status, false, false, false, null);
        }
    }
    SelectItem[] lst_yes_no_options;
    //</editor-fold>   
    //<editor-fold defaultstate="collapsed" desc="tbl_emp_info_filtered"> 
    List<FhrdEmpmst> tbl_emp_info_filtered;

    public List<FhrdEmpmst> getTbl_emp_info_filtered() {
        return tbl_emp_info_filtered;
    }

    public void setTbl_emp_info_filtered(List<FhrdEmpmst> tbl_emp_info_filtered) {
        this.tbl_emp_info_filtered = tbl_emp_info_filtered;
    }
    //</editor-fold>  
    //<editor-fold defaultstate="collapsed" desc="refreshTable">

    public void refreshTable() {
        setTblEmpInfo();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="returnUrlToView"> 

    public String returnUrlToView() {
        RequestContext.getCurrentInstance().execute("openWindow('PIS_View_Employee.htm')");
        getPIS_GlobalSettings().setForVerification(false);
        return null;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="init">
    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        ddl_status = null;
        setTblEmpInfo();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Default Constructor and Other Global Methods">

    /**
     * Creates a new instance of PIS_View_Resign_Relieve
     */
    public PIS_View_Resign_Relieve() {
    }

    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Utilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    //</editor-fold>
}
