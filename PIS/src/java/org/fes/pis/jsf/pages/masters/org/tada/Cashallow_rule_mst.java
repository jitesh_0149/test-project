package org.fes.pis.jsf.pages.masters.org.tada;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.*;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.jsf.utilities.JSFMessages;
import org.fes.pis.sessions.FhrdDesigmstFacadeLocal;
import org.fes.pis.sessions.FtasTadaCashallowRuleMstFacadeLocal;
import org.fes.pis.sessions.FtasTadaGradeCityLinkFacade;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Mayuri
 */
@ManagedBean
@ViewScoped
public class Cashallow_rule_mst implements Serializable {

    @EJB
    private FtasTadaCashallowRuleMstFacadeLocal ftasTadaCashallowRuleMstFacade;
    @EJB
    private FhrdDesigmstFacadeLocal fhrdDesigmstFacade;
    @EJB
    private FtasTadaGradeCityLinkFacade ftasTadaGradeCityLinkFacade;
    
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Saving Methods">
    //<editor-fold defaultstate="collapsed" desc="ddl_grade_city_link">
    String ddl_grade_city_link;
    List<SelectItem> ddl_grade_city_link_options;

    public String getDdl_grade_city_link() {
        return ddl_grade_city_link;
    }

    public void setDdl_grade_city_link(String ddl_grade_city_link) {
        this.ddl_grade_city_link = ddl_grade_city_link;
    }

    public List<SelectItem> getDdl_grade_city_link_options() {
        return ddl_grade_city_link_options;
    }

    public void setDdl_grade_city_link_options(List<SelectItem> ddl_grade_city_link_options) {
        this.ddl_grade_city_link_options = ddl_grade_city_link_options;
    }

    private void setDdl_grade_city_link() {
        ddl_grade_city_link_options = new ArrayList<SelectItem>();
        ddl_grade_city_link_options.add(new SelectItem(null, "--- Select ----"));
        List<FtasTadaGradeCityLink> lst_CityLinks = ftasTadaGradeCityLinkFacade.findAll(globalData.getWorking_ou());
        for (FtasTadaGradeCityLink gcl : lst_CityLinks) {
            ddl_grade_city_link_options.add(new SelectItem(gcl.getGclSrgKey().toString(), gcl.getCity() + " (" + gcl.getGmSrgKey().getGmDesc() + ")"));
        }
        ddl_grade_city_link = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_desigmst">
    String ddl_desigmst;
    List<SelectItem> ddl_desigmst_options;

    public String getDdl_desigmst() {
        return ddl_desigmst;
    }

    public void setDdl_desigmst(String ddl_desigmst) {
        this.ddl_desigmst = ddl_desigmst;
    }

    public List<SelectItem> getDdl_desigmst_options() {
        return ddl_desigmst_options;
    }

    public void setDdl_desigmst_options(List<SelectItem> ddl_desigmst_options) {
        this.ddl_desigmst_options = ddl_desigmst_options;
    }

    private void setDdl_desigmst() {
        ddl_desigmst_options = new ArrayList<SelectItem>();
        ddl_desigmst_options.add(new SelectItem(null, "--- Select ----"));
        List<FhrdDesigmst> lst_Desigmsts = fhrdDesigmstFacade.findAll(globalData.getWorking_ou());
        for (FhrdDesigmst dm : lst_Desigmsts) {
            ddl_desigmst_options.add(new SelectItem(dm.getDsgmSrgKey().toString(), dm.getDesigDesc()));
        }
        ddl_desigmst = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_from_day">
    String txt_from_day;

    public String getTxt_from_day() {
        return txt_from_day;
    }

    public void setTxt_from_day(String txt_from_day) {
        this.txt_from_day = txt_from_day;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_to_day">
    String txt_to_day;

    public String getTxt_to_day() {
        return txt_to_day;
    }

    public void setTxt_to_day(String txt_to_day) {
        this.txt_to_day = txt_to_day;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_cash_allow">
    String txt_cash_allow;

    public String getTxt_cash_allow() {
        return txt_cash_allow;
    }

    public void setTxt_cash_allow(String txt_cash_allow) {
        this.txt_cash_allow = txt_cash_allow;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_cash_allow_rate">
    String txt_cash_allow_rate;

    public String getTxt_cash_allow_rate() {
        return txt_cash_allow_rate;
    }

    public void setTxt_cash_allow_rate(String txt_cash_allow_rate) {
        this.txt_cash_allow_rate = txt_cash_allow_rate;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_start_date">
    Date cal_start_date;

    public Date getCal_start_date() {
        return cal_start_date;
    }

    public void setCal_start_date(Date cal_start_date) {
        this.cal_start_date = cal_start_date;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_end_date">
    Date cal_end_date;

    public Date getCal_end_date() {
        return cal_end_date;
    }

    public void setCal_end_date(Date cal_end_date) {
        this.cal_end_date = cal_end_date;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetEntryComponent()">
    private void resetEntryComponent() {

        ddl_grade_city_link = null;
        ddl_desigmst = null;
        txt_from_day = null;
        txt_to_day = null;
        txt_cash_allow = null;
        txt_cash_allow_rate = null;
        cal_start_date = null;
        cal_end_date = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_save">

    public void btn_save(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            FtasTadaCashallowRuleMst ent_CashallowRuleMst = new FtasTadaCashallowRuleMst();
            ent_CashallowRuleMst.setCarmSrgKey(new Long("-1"));
            ent_CashallowRuleMst.setGclSrgKey(new FtasTadaGradeCityLink(new Long(ddl_grade_city_link)));
            ent_CashallowRuleMst.setDsgmSrgKey(new FhrdDesigmst(new BigDecimal(ddl_desigmst)));
            ent_CashallowRuleMst.setFromDay(new BigDecimal(txt_from_day));
            ent_CashallowRuleMst.setToDay(new BigDecimal(txt_to_day));
            ent_CashallowRuleMst.setCashAllowance(new BigDecimal(txt_cash_allow));
            ent_CashallowRuleMst.setCashAllowRate(new BigDecimal(txt_cash_allow_rate));
            ent_CashallowRuleMst.setStartDate(cal_start_date);
            ent_CashallowRuleMst.setEndDate(cal_end_date);
            ent_CashallowRuleMst.setCreateby(new FhrdEmpmst(globalData.getUser_id()));
            ent_CashallowRuleMst.setCreatedt(new Date());
            ent_CashallowRuleMst.setOumUnitSrno(new SystOrgUnitMst(globalData.getWorking_ou()));
            ftasTadaCashallowRuleMstFacade.create(ent_CashallowRuleMst);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cash Allowance Rule Master Created", "Cash Allowance Rule created successfully"));
            setTbl_carule_mst();
            resetEntryComponent();
        } catch (Exception e) {
            context.addMessage(null, JSFMessages.getErrorMessage(e));
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_accept_clicked", null, e);
        }
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="main table methods">
    //<editor-fold defaultstate="collapsed" desc="tbl_conv_mst">
    List<FtasTadaCashallowRuleMst> tbl_carule_mst;
    List<FtasTadaCashallowRuleMst> tbl_carule_mst_filter;

    public List<FtasTadaCashallowRuleMst> getTbl_carule_mst() {
        return tbl_carule_mst;
    }

    public void setTbl_carule_mst(List<FtasTadaCashallowRuleMst> tbl_carule_mst) {
        this.tbl_carule_mst = tbl_carule_mst;
    }

    public List<FtasTadaCashallowRuleMst> getTbl_carule_mst_filter() {
        return tbl_carule_mst_filter;
    }

    public void setTbl_carule_mst_filter(List<FtasTadaCashallowRuleMst> tbl_carule_mst_filter) {
        this.tbl_carule_mst_filter = tbl_carule_mst_filter;
    }

    private void setTbl_carule_mst() {
        tbl_carule_mst = ftasTadaCashallowRuleMstFacade.findAll(globalData.getWorking_ou());
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Apply End Date Methods">
    //<editor-fold defaultstate="collapsed" desc="selectedCashallowRuleMst">
    FtasTadaCashallowRuleMst selectedCashallowRuleMst;

    public FtasTadaCashallowRuleMst getSelectedCashallowRuleMst() {
        return selectedCashallowRuleMst;
    }

    public void setSelectedCashallowRuleMst(FtasTadaCashallowRuleMst selectedCashallowRuleMst) {
        this.selectedCashallowRuleMst = selectedCashallowRuleMst;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="applyEndDate">

    public void applyEndDate(FtasTadaCashallowRuleMst ftasTadaCashallowRuleMst) {
        selectedCashallowRuleMst = ftasTadaCashallowRuleMst;
        cal_apply_end_date = null;
        cal_apply_end_date_min = DateTimeUtility.ChangeDateFormat(ftasTadaCashallowRuleMst.getStartDate(), null);
        RequestContext.getCurrentInstance().execute("pw_dlg_apply_end_date.show()");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_apply_end_date">
    Date cal_apply_end_date;
    String cal_apply_end_date_min;

    public Date getCal_apply_end_date() {
        return cal_apply_end_date;
    }

    public void setCal_apply_end_date(Date cal_apply_end_date) {
        this.cal_apply_end_date = cal_apply_end_date;
    }

    public String getCal_apply_end_date_min() {
        return cal_apply_end_date_min;
    }

    public void setCal_apply_end_date_min(String cal_apply_end_date_min) {
        this.cal_apply_end_date_min = cal_apply_end_date_min;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_apply_end_date">

    public void btn_apply_end_date(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            selectedCashallowRuleMst = ftasTadaCashallowRuleMstFacade.find(selectedCashallowRuleMst.getCarmSrgKey());
            selectedCashallowRuleMst.setEndDate(cal_apply_end_date);
            selectedCashallowRuleMst.setUpdateby(new FhrdEmpmst(globalData.getUser_id()));
            ftasTadaCashallowRuleMstFacade.edit(selectedCashallowRuleMst);
            setTbl_carule_mst();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "End Date Applied", "End Date applied successfully"));
            RequestContext.getCurrentInstance().execute("pw_dlg_apply_end_date.hide()");
        } catch (Exception e) {
            context.addMessage(null, JSFMessages.getErrorMessage(e));
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_accept_clicked", null, e);
        }
    }
    //</editor-fold>  
    //</editor-fold>  
    //<editor-fold defaultstate="collapsed" desc="init()">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setDdl_grade_city_link();
        setDdl_desigmst();
        setTbl_carule_mst();

    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Default Constructor & Other Beans Methods">
    // <editor-fold defaultstate="collapsed" desc="Default Constructor">
    public Cashallow_rule_mst() {
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
    //</editor-fold>
}
