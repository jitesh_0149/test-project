/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.pis.payroll;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.PaytransMonthlyVariableBean;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.customJpaController;
import org.fes.pis.custom_sessions.ejb_utilities_pisLocal;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.*;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.*;
import org.joda.time.DateTime;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Monthly_Variable")
@ViewScoped
public class PIS_Monthly_Variable implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="EJB Declarations">
    @EJB
    private FhrdEarndednmstFacadeLocal fhrdEarndednmstFacade;
    @EJB
    private FhrdPaytransMonvarFacadeLocal fhrdPaytransMonvarFacade;
    @EJB
    private FhrdEmpearndednFacadeLocal fhrdEmpearndednFacade;
    @EJB
    private ejb_utilities_pisLocal ejb_utilities_pis;
    @EJB
    private SystOrgUnitMstFacadeLocal systOrgUnitMstFacade;
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    @EJB
    private customJpaController customJpaController;
    @EJB
    private FtasLockDtlFacadeLocal ftasLockDtlFacade;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Declarations">
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    Ou_Pack ou_Pack = new Ou_Pack();
    public static Integer v_user_type = 1;
    //</editor-fold>  
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_org_unit"> 
    String ddl_org_unit;
    List<SelectItem> ddl_org_unit_options;

    public String getDdl_org_unit() {
        return ddl_org_unit;
    }

    public void setDdl_org_unit(String ddl_org_unit) {
        this.ddl_org_unit = ddl_org_unit;
    }

    public List<SelectItem> getDdl_org_unit_options() {
        return ddl_org_unit_options;
    }

    public void setDdl_org_unit_options(List<SelectItem> ddl_org_unit_options) {
        this.ddl_org_unit_options = ddl_org_unit_options;
    }

    public void setDdl_org_unit() {
        ddl_org_unit_options = new ArrayList<SelectItem>();
        List<SystOrgUnitMst> lst = systOrgUnitMstFacade.findNativeOuForMonVar(globalData.getWorking_ou());
        Integer n = lst.size();
        if (lst.isEmpty()) {
            ddl_org_unit_options.add(new SelectItem(null, "---No Data Found---"));
        } else {
            ddl_org_unit_options.add(new SelectItem(null, "---Select---"));
            for (int i = 0; i < n; i++) {
                ddl_org_unit_options.add(new SelectItem(lst.get(i).getOumUnitSrno(), lst.get(i).getOumName()));
            }
        }
        ddl_org_unit = null;
    }

    public void ddl_org_unit_changed(ValueChangeEvent event) {
        ddl_org_unit = event.getNewValue() == null ? null : event.getNewValue().toString();
        setDdl_employee();
        setDdl_ed_code_options();
        setTbl_user_paytransMonvar();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_selection_type"> 
    String ddl_selection_type;

    public String getDdl_selection_type() {
        return ddl_selection_type;
    }

    public void setDdl_selection_type(String ddl_selection_type) {
        this.ddl_selection_type = ddl_selection_type;
    }

    public void ddl_selection_type_changed(ValueChangeEvent event) {
        ddl_selection_type = event.getNewValue() == null ? null : event.getNewValue().toString();
        tbl_user_paytransMonvar = new ArrayList<PaytransMonthlyVariableBean>();
        setDdl_employee();
        setDdl_ed_code_options();
        setTbl_user_paytransMonvar();
    }
    //</editor-fold>  
    //<editor-fold defaultstate="collapsed" desc="min max lock date">
    Date min_lock_date;
    Date max_lock_date;

    private void setMinMaxDate() {
        Date v_date = ftasLockDtlFacade.findMinLockDate(globalData.getWorking_ou().toString());
        min_lock_date = new DateTime(v_date).plusMonths(1).toDate();
        min_lock_date = new Date(min_lock_date.getYear(), min_lock_date.getMonth(), 1);
        max_lock_date = new Date(new Date().getYear(), new Date().getMonth(), 1);
        setDdl_year();
    }
    //</editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="ddl_salary_year"> 
    String ddl_salary_year;
    List<SelectItem> ddl_salary_year_options;

    public String getDdl_salary_year() {
        return ddl_salary_year;
    }

    public void setDdl_salary_year(String ddl_salary_year) {
        this.ddl_salary_year = ddl_salary_year;
    }

    public List<SelectItem> getDdl_salary_year_options() {
        return ddl_salary_year_options;
    }

    public void setDdl_salary_year_options(List<SelectItem> ddl_salary_year_options) {
        this.ddl_salary_year_options = ddl_salary_year_options;
    }

    public void setDdl_year() {
        Integer v_from_year = min_lock_date.getYear() + 1900;
        Integer v_to_year = max_lock_date.getYear() + 1900;
        ddl_salary_year_options = new ArrayList<SelectItem>();
        while (v_from_year <= v_to_year) {
            ddl_salary_year_options.add(new SelectItem(v_from_year));
            v_from_year++;
        }
        if (ddl_salary_year_options.isEmpty()) {
            ddl_salary_year = null;
        } else {
            ddl_salary_year = String.valueOf((new Date()).getYear() + 1900);
        }
//        System.out.println("year " + ddl_year);
        setDdl_month();
    }

//    public void setDdl_salary_year() {
//        ddl_salary_year_options = new ArrayList<SelectItem>();
//        ddl_salary_month_options = new ArrayList<SelectItem>();
//        Date v_locked_date = ejb_utilities_pis.getLastLockedDate(globalData.getUser_id(), globalData.getWorking_ou());
//        Integer v_year = v_locked_date.getYear() + 1900;
//        Integer v_month = v_locked_date.getMonth() + 1;
//        if (v_month != 12) {
//            ddl_salary_year_options.add(new SelectItem(v_year));
//            ddl_salary_year_options.add(new SelectItem(v_year + 1));
//            for (int i = v_month - 1; i < 11; i++) {
//                ddl_salary_month_options.add(new SelectItem(i, DateTimeUtility.formatMonth(i, "MMMM")));
//            }
//        } else {
//            v_year++;
//            ddl_salary_year_options.add(new SelectItem(v_year));
//            ddl_salary_year_options.add(new SelectItem(v_year + 1));
//            for (int i = 0; i < 12; i++) {
//                ddl_salary_month_options.add(new SelectItem(i, DateTimeUtility.formatMonth(i, "MMMM")));
//            }
//        }
//    }
    public void ddl_salary_year_changed(ValueChangeEvent event) {
        ddl_salary_year = event.getNewValue().toString();
        Date v_locked_date = ejb_utilities_pis.getLastLockedDate(globalData.getUser_id(), globalData.getWorking_ou());
        Integer v_year = v_locked_date.getYear() + 1900;
        Integer v_month = v_locked_date.getMonth() + 1;
        ddl_salary_month_options = new ArrayList<SelectItem>();
        if (v_year.equals(Integer.valueOf(ddl_salary_year))) {
            if (v_month != 12) {
                ddl_salary_year_options.add(new SelectItem(v_year));
                ddl_salary_year_options.add(new SelectItem(v_year + 1));
                for (int i = v_month + 1; i <= 12; i++) {
                    ddl_salary_month_options.add(new SelectItem(i, DateTimeUtility.formatMonth(i, "MMMM")));
                }
            } else {
                v_year++;
                ddl_salary_year_options.add(new SelectItem(v_year));
                ddl_salary_year_options.add(new SelectItem(v_year + 1));
                for (int i = 1; i <= 12; i++) {
                    ddl_salary_month_options.add(new SelectItem(i, DateTimeUtility.formatMonth(i, "MMMM")));
                }
            }
        } else {
            for (int i = 1; i <= 12; i++) {
                ddl_salary_month_options.add(new SelectItem(i, DateTimeUtility.formatMonth(i, "MMMM")));
            }
        }
        setTbl_user_paytransMonvar();
    }
    //</editor-fold>     
    //<editor-fold defaultstate="collapsed" desc="ddl_salary_month">
    String ddl_salary_month;
    List<SelectItem> ddl_salary_month_options;

    public String getDdl_salary_month() {
        return ddl_salary_month;
    }

    public void setDdl_salary_month(String ddl_salary_month) {
        this.ddl_salary_month = ddl_salary_month;
    }

    public List<SelectItem> getDdl_salary_month_options() {
        return ddl_salary_month_options;
    }

    public void setDdl_salary_month_options(List<SelectItem> ddl_salary_month_options) {
        this.ddl_salary_month_options = ddl_salary_month_options;
    }

    public void ddl_salary_month_changed(ValueChangeEvent event) {
        ddl_salary_month = event.getNewValue().toString();
        setTbl_user_paytransMonvar();
    }

    private void setDdl_month() {
        ddl_salary_month_options = new ArrayList<SelectItem>();
        if (ddl_salary_year == null) {
            ddl_salary_month_options.add(new SelectItem(null, "--- Not Available ---"));
            ddl_salary_month = null;
        } else {
            Date v_year_start_date = new Date(Integer.valueOf(ddl_salary_year) - 1900, 0, 1);
            Date v_year_end_date = new Date(Integer.valueOf(ddl_salary_year) - 1900, 11, 31);
            Date v_min_lock_date = min_lock_date;
            Date v_max_lock_date = max_lock_date;
            while (v_min_lock_date.before(v_max_lock_date) || v_min_lock_date.equals(v_max_lock_date)) {
                if (!(v_min_lock_date.before(v_year_start_date))
                        && !(v_min_lock_date.after(v_year_end_date))) {
                    ddl_salary_month_options.add(new SelectItem(DateTimeUtility.ChangeDateFormat(v_min_lock_date, "MM").toString(), DateTimeUtility.ChangeDateFormat(v_min_lock_date, "MMM")));
                }
                v_min_lock_date = new DateTime(v_min_lock_date).plusMonths(1).toDate();
            }
            ddl_salary_month = ddl_salary_month_options.get(0).getValue().toString();

        }
    }
    //</editor-fold>      
    //<editor-fold defaultstate="collapsed" desc="rdb_selection"> 
    String rdb_selection;

    public String getRdb_selection() {
        return rdb_selection;
    }

    public void setRdb_selection(String rdb_selection) {
        this.rdb_selection = rdb_selection;
    }

    public void rdb_selection_changed(ValueChangeEvent event) {
        rdb_selection = event.getNewValue() == null ? null : event.getNewValue().toString();
        setTbl_user_paytransMonvar();
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="ddl_employee">
    String ddl_employee;
    List<SelectItem> ddl_employee_options;

    public String getDdl_employee() {
        return ddl_employee;
    }

    public void setDdl_employee(String ddl_employee) {
        this.ddl_employee = ddl_employee;
    }

    public List<SelectItem> getDdl_employee_options() {
        return ddl_employee_options;
    }

    public void setDdl_employee_options(List<SelectItem> ddl_employee_options) {
        this.ddl_employee_options = ddl_employee_options;
    }

    private void setDdl_employee() {
        ddl_employee_options = new ArrayList<SelectItem>();
        if (ddl_org_unit != null && ddl_salary_month != null && ddl_salary_year != null) {
            Date v_start_date = new Date((Integer.valueOf(ddl_salary_year) - 1900), (Integer.valueOf(ddl_salary_month) - 1), 1);
            Date v_end_date = DateTimeUtility.endDateOfMonthFromDate(v_start_date);
            List<FhrdEmpmst> lst_Empmst = fhrdEmpmstFacade.findAllForMonVar(Integer.valueOf(ddl_org_unit), DateTimeUtility.ChangeDateFormat(v_start_date, null), DateTimeUtility.ChangeDateFormat(v_end_date, null), ddl_selection_type);
            Integer n = lst_Empmst.size();
            if (lst_Empmst.isEmpty()) {
                ddl_employee_options.add(new SelectItem(null, "--No Data Found--"));
            } else {
                ddl_employee_options.add(new SelectItem(null, "--Select--"));
                for (int i = 0; i < n; i++) {
                    String v_user_name = lst_Empmst.get(i).getUserFirstName() == null ? lst_Empmst.get(i).getUserName() : (lst_Empmst.get(i).getUserFirstName() + " " + lst_Empmst.get(i).getUserLastName());
                    ddl_employee_options.add(new SelectItem(lst_Empmst.get(i).getUserId(), v_user_name));
                }
            }
        } else {
            ddl_employee_options.add(new SelectItem(null, "--No Data Found--"));
        }
        ddl_employee = null;
    }

    public void ddl_employee_changed(ValueChangeEvent event) {
        ddl_employee = event.getNewValue() == null ? null : event.getNewValue().toString();
        setTbl_user_paytransMonvar();
    }
    //</editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="ddl_ed_code"> 
    String ddl_ed_code;
    List<SelectItem> ddl_ed_code_options;

    public String getDdl_ed_code() {
        return ddl_ed_code;
    }

    public void setDdl_ed_code(String ddl_ed_code) {
        this.ddl_ed_code = ddl_ed_code;
    }

    public List<SelectItem> getDdl_ed_code_options() {
        return ddl_ed_code_options;
    }

    public void setDdl_ed_code_options(List<SelectItem> ddl_ed_code_options) {
        this.ddl_ed_code_options = ddl_ed_code_options;
    }

    public void setDdl_ed_code_options() {
//        findAllMonthlyVariableCodes(Integer p_oum_unit_srno, String p_date)
        ddl_ed_code_options = new ArrayList<SelectItem>();
        if (ddl_org_unit != null && ddl_salary_month != null && ddl_salary_year != null) {
            Date v_start_date = new Date((Integer.valueOf(ddl_salary_year) - 1900), (Integer.valueOf(ddl_salary_month) - 1), 1);
//            Date v_end_date = DateTimeUtility.endDateOfMonthFromDate(v_start_date);
            List<FhrdEarndednmst> lst_Earndednmsts = fhrdEarndednmstFacade.findAllMonthlyVariableCodes(Integer.valueOf(ddl_org_unit), DateTimeUtility.ChangeDateFormat(v_start_date, null));
            Integer n = lst_Earndednmsts.size();
            if (lst_Earndednmsts.isEmpty()) {
                ddl_ed_code_options.add(new SelectItem(null, "--No Data Found--"));
            } else {
                ddl_ed_code_options.add(new SelectItem(null, "--Select--"));
                for (int i = 0; i < n; i++) {
//                    String v_user_name = lst_Earndednmsts.get(i).getUserFirstName() == null ? lst_Empmst.get(i).getUserName() : (lst_Empmst.get(i).getUserFirstName() + " " + lst_Empmst.get(i).getUserLastName());
                    ddl_ed_code_options.add(new SelectItem(lst_Earndednmsts.get(i).getEdmSrgKey(), lst_Earndednmsts.get(i).getEarndednName()));
                }
            }
        } else {
            ddl_ed_code_options.add(new SelectItem(null, "--No Data Found--"));
        }
        ddl_ed_code = null;

    }

    public void ddl_ed_code_changed(ValueChangeEvent event) {
        ddl_ed_code = event.getNewValue() == null ? null : event.getNewValue().toString();
        setTbl_user_paytransMonvar();
    }
    //</editor-fold>  
    //<editor-fold defaultstate="collapsed" desc="tbl_user_paytransMonvar"> 
    List<PaytransMonthlyVariableBean> tbl_user_paytransMonvar;

    public List<PaytransMonthlyVariableBean> getTbl_user_paytransMonvar() {
        return tbl_user_paytransMonvar;
    }

    public void setTbl_user_paytransMonvar(List<PaytransMonthlyVariableBean> tbl_user_paytransMonvar) {
        this.tbl_user_paytransMonvar = tbl_user_paytransMonvar;
    }

    public void setTbl_user_paytransMonvar() {
        System.out.println("status " + ddl_selection_type);
        System.out.println("ddl_employee " + ddl_employee);
        System.out.println("ddl_ed_code " + ddl_ed_code);
        if (ddl_org_unit != null && ddl_employee != null) {
            System.out.println("in if...");
            Date v_start_date = new Date((Integer.valueOf(ddl_salary_year) - 1900), (Integer.valueOf(ddl_salary_month) - 1), 1);
            Date v_end_date = DateTimeUtility.endDateOfMonthFromDate(v_start_date);
            List<PaytransMonthlyVariableBean> lstPaytransMonvars = null;
            if (ddl_selection_type.equals("A")) {
                lstPaytransMonvars = findAllMonthlyVariable(globalData.getWorking_ou(), v_start_date, v_end_date, new Integer(ddl_employee), null, "A");
            } else if (ddl_selection_type.equals("P")) {
                lstPaytransMonvars = findAllMonthlyVariable(globalData.getWorking_ou(), v_start_date, v_end_date, new Integer(ddl_employee), null, "P");
            } else if (ddl_selection_type.equals("S")) {
                lstPaytransMonvars = findAllMonthlyVariable(globalData.getWorking_ou(), v_start_date, v_end_date, new Integer(ddl_employee), null, "S");
            }
            tbl_user_paytransMonvar = lstPaytransMonvars;
        } else if (ddl_org_unit != null && ddl_ed_code != null) {
            System.out.println("in else...");
            Date v_start_date = new Date((Integer.valueOf(ddl_salary_year) - 1900), (Integer.valueOf(ddl_salary_month) - 1), 1);
            Date v_end_date = DateTimeUtility.endDateOfMonthFromDate(v_start_date);
            List<PaytransMonthlyVariableBean> lstPaytransMonvars = null;
            if (ddl_selection_type.equals("A")) {
                lstPaytransMonvars = findAllMonthlyVariable(globalData.getWorking_ou(), v_start_date, v_end_date, null, ddl_ed_code, "A");
            } else if (ddl_selection_type.equals("P")) {
                lstPaytransMonvars = findAllMonthlyVariable(globalData.getWorking_ou(), v_start_date, v_end_date, null, ddl_ed_code, "P");
            } else if (ddl_selection_type.equals("S")) {
                lstPaytransMonvars = findAllMonthlyVariable(globalData.getWorking_ou(), v_start_date, v_end_date, null, ddl_ed_code, "S");
            }
            tbl_user_paytransMonvar = lstPaytransMonvars;
        } else {
            tbl_user_paytransMonvar = null;
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ent_eed_edit"> 
    public void btn_set_mv_value_clicked(ActionEvent event) {
        System.out.println("in btn_set_mv_value_clicked");
    }
    FhrdEmpearndedn ent_eed_edit;

    public FhrdEmpearndedn getEnt_eed_edit() {
        return ent_eed_edit;
    }

    public void setEnt_eed_edit(FhrdEmpearndedn ent_eed_edit) {
        this.ent_eed_edit = ent_eed_edit;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_save_all_mv_values_clicked">

    public void btn_save_all_mv_values_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (tbl_user_paytransMonvar != null) {
            Integer n = tbl_user_paytransMonvar.size();
            Integer v_cal_year = Integer.valueOf(ddl_salary_year);
            Date v_start_date = new Date((Integer.valueOf(ddl_salary_year) - 1900), (Integer.valueOf(ddl_salary_month) - 1), 1);
            Date v_end_date = DateTimeUtility.endDateOfMonthFromDate(v_start_date);
            if (n != 0) {
                List<FhrdPaytransMonvar> lsFhrdPaytransMonvars_insert = new ArrayList<FhrdPaytransMonvar>();
                List<FhrdPaytransMonvar> lsFhrdPaytransMonvars_update = new ArrayList<FhrdPaytransMonvar>();
                for (int i = 0; i < n; i++) {
                    FhrdPaytransMonvar entPaytransMonvar = new FhrdPaytransMonvar();
                    if (tbl_user_paytransMonvar.get(i).getPtmvSrgKey() == null) {
                        entPaytransMonvar.setPtmvSrgKey(new BigDecimal("-" + (i + 1)));
                        entPaytransMonvar.setTranYear(Short.valueOf(v_cal_year.toString()));
                        entPaytransMonvar.setUserId(new FhrdEmpmst(tbl_user_paytransMonvar.get(i).getUserId()));
                        entPaytransMonvar.setOrgSalaryFromDate(v_start_date);
                        entPaytransMonvar.setOrgSalaryToDate(v_end_date);
                        entPaytransMonvar.setEedSrgKey(new FhrdEmpearndedn(tbl_user_paytransMonvar.get(i).getEedSrg_key()));
                        entPaytransMonvar.setCreateby(globalData.getEnt_login_user());
                        entPaytransMonvar.setCreatedt(new Date());
                        entPaytransMonvar.setOumUnitSrno(globalData.getWorking_ou());
                        entPaytransMonvar.setEarndednFlag("1");
                        entPaytransMonvar.setEarndedncd("1");
                        entPaytransMonvar.setPrintIf0Flag("1");
                        entPaytransMonvar.setPrintInPaySlip("1");
                        entPaytransMonvar.setVariableAmt(tbl_user_paytransMonvar.get(i).getVariableAmt());
                        lsFhrdPaytransMonvars_insert.add(entPaytransMonvar);
                    } else {
                        entPaytransMonvar = fhrdPaytransMonvarFacade.find(tbl_user_paytransMonvar.get(i).getPtmvSrgKey());
                        entPaytransMonvar.setVariableAmt(tbl_user_paytransMonvar.get(i).getVariableAmt());
                        entPaytransMonvar.setUpdateby(globalData.getEnt_login_user());
                        entPaytransMonvar.setUpdatedt(new Date());
                        lsFhrdPaytransMonvars_update.add(entPaytransMonvar);
                    }
                }
                try {
                    //                    boolean isTransactionComplete = true;
                    boolean isTransactionComplete = customJpaController.setMonthlyVariableValues(lsFhrdPaytransMonvars_insert, lsFhrdPaytransMonvars_update);
                    if (isTransactionComplete) {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Saved Successfully"));
                        ddl_selection_type = "A";
//                        setDdl_salary_year();
                        setDdl_org_unit();
                        setMinMaxDate();
                        setDdl_employee();
                        setDdl_ed_code_options();
                        setTbl_user_paytransMonvar();
                    } else {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Record not saved"));
                    }
                } catch (EJBException e) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "EJB Exception"));
                } catch (Exception e) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Exception"));
                }

            }
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="findAllMonthlyVariable"> 

    public List<PaytransMonthlyVariableBean> findAllMonthlyVariable(Integer p_oum_unit_srno, Date p_start_date, Date p_end_date, Integer p_user_id, String p_edcode, String p_status) {
        String v_start_date = DateTimeUtility.ChangeDateFormat(p_start_date, null);
        String v_end_date = DateTimeUtility.ChangeDateFormat(p_end_date, null);
        List<PaytransMonthlyVariableBean> lst_b_data = new ArrayList<PaytransMonthlyVariableBean>();
        Connection cn = null;
        String v_query = "SELECT EED.USER_ID USER_ID,"
                + "     MV.PTMV_SRG_KEY PTMV_SRG_KEY,"
                + "     MV.ORG_SALARY_FROM_DATE,"
                + "     MV.ORG_SALARY_TO_DATE,"
                + "     EED.EED_SRG_KEY EED_SRG_KEY,"
                + "     EED.EARNDEDNCD EARNDEDNCD,"
                + "     EM.EARNDEDN_NAME EARNDEDN_NAME,"
                + "     MV.VARIABLE_AMT VARIABLE_AMT"
                + " FROM FHRD_EARNDEDNMST EM,"
                + "     FHRD_EMPEARNDEDN EED"
                + " LEFT OUTER JOIN FHRD_PAYTRANS_MONVAR MV"
                + " ON (EED.USER_ID              = MV.USER_ID"
                + " AND EED.EED_SRG_KEY         =MV.EED_SRG_KEY"
                + " AND (MV.ORG_SALARY_FROM_DATE='" + v_start_date + "'"
                + " AND MV.ORG_SALARY_TO_DATE   ='" + v_end_date + "'))"
                + " WHERE EM.EDM_SRG_KEY         =EED.EDM_SRG_KEY"
                + " AND EED.PAYABLE_ON_VALUE    = 'MV'";
        if (p_user_id != null) {
            v_query += " AND EED.USER_ID             = " + p_user_id;  //change userid to ed_srg_key for earndedn filter
        } else if (p_edcode != null) {
            v_query += " AND EED.EDM_SRG_KEY             = " + p_edcode;  //change userid to ed_srg_key for earndedn filter
        }
        v_query += " AND EED.USER_ID  IN (SELECT USER_ID FROM FHRD_EMPMST"
                + "                         WHERE POSTING_OUM_UNIT_SRNO IN"
                + "                              (SELECT * FROM TABLE(OU_PACK.GET_SUB_OU(OU_PACK.GET_ORG_SRNO(" + p_oum_unit_srno + ") ,'D','Y'))"
                + "                              )"
                + "                      )"
                + "AND ((TO_DATE('" + v_start_date + "','DD-MON-YYYY') BETWEEN EED.OPENING_DATE AND EED.CLOSING_DATE)"
                + "  OR (TO_DATE('" + v_end_date + "','DD-MON-YYYY') BETWEEN EED.OPENING_DATE AND EED.CLOSING_DATE))";

        if (p_status.equals("S")) {
            v_query += " AND MV.PTMV_SRG_KEY IS NOT NULL";
        }
        if (p_status.equals("P")) {
            v_query += " AND MV.PTMV_SRG_KEY IS NULL";
        }
        System.out.println("query " + v_query);
        try {
            Statement st = null;
            try {
                ResultSet res = null;
                try {
                    cn = getGlobalUtilities().getjdbcFES_PROJECT_Oracle().getConnection();
                    int i = 0;
                    st = cn.createStatement();
                    ////                    System.out.println("query "+v_query);
                    res = st.executeQuery(v_query);
                    while (res.next()) {
                        PaytransMonthlyVariableBean ent_VariableBean = new PaytransMonthlyVariableBean();
                        ent_VariableBean.setUserId(res.getInt("USER_ID"));
                        ent_VariableBean.setPtmvSrgKey(res.getBigDecimal("PTMV_SRG_KEY"));
                        ent_VariableBean.setEedSrg_key(res.getBigDecimal("EED_SRG_KEY"));
                        ent_VariableBean.setEarndedncd(res.getString("EARNDEDNCD"));
                        ent_VariableBean.setEarndednName(res.getString("EARNDEDN_NAME"));
                        if (res.getString("VARIABLE_AMT") == null) {
                            ent_VariableBean.setVariableAmt(null);
                        } else {
                            ent_VariableBean.setVariableAmt(new BigInteger(res.getString("VARIABLE_AMT")));
                        }
                        ent_VariableBean.setRowId(i);
                        lst_b_data.add(ent_VariableBean);
                        i++;
                    }
                } catch (SQLException e) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "findAllMonthlyVariable", null, e);
                } finally {
                    res.close();
                }
            } catch (SQLException sQLException) {
            } finally {
                st.close();
            }
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "findAllMonthlyVariable", null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        cn = null;
                    }
                } catch (Exception e) {
                }
            }
        }
        return lst_b_data;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings and default methods">

    /**
     * Creates a new instance of PIS_Monthly_Variable
     */
    public PIS_Monthly_Variable() {
    }
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="init()"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        ddl_selection_type = "A";
//        setDdl_salary_year();
        setDdl_org_unit();
        setMinMaxDate();
        rdb_selection = "E";
        setDdl_employee();
        setDdl_ed_code_options();

    }
    //</editor-fold>
}
