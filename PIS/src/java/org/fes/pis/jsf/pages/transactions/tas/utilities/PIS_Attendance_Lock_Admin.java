/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.transactions.tas.utilities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.AttendanceLockDataList;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.customJpaController;
import org.fes.pis.entities.FtasAbsentees;
import org.fes.pis.entities.FtasLockDtl;
import org.fes.pis.entities.SystOrgUnitMst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.jsf.selectableDataModels.SD_AttendanceLockDataList;
import org.fes.pis.sessions.FtasAbsenteesFacadeLocal;
import org.fes.pis.sessions.FtasLockDtlFacadeLocal;
import org.fes.pis.sessions.SystOrgUnitMstFacadeLocal;
import org.joda.time.DateTime;
import org.primefaces.context.RequestContext;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Attendance_Lock_Admin")
@ViewScoped
public class PIS_Attendance_Lock_Admin implements Serializable {

    @EJB
    private FtasAbsenteesFacadeLocal ftasAbsenteesFacade;
    @EJB
    private FtasLockDtlFacadeLocal ftasLockDtlFacade;
    @EJB
    private customJpaController customJpaController;
    @EJB
    private SystOrgUnitMstFacadeLocal systOrgUnitMstFacade;

    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    
    boolean isValidUser = false;
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="min max lock date">
    Date min_lock_date;
    Date max_lock_date;

    private void setMinMaxDate() {
        Date v_date = ftasLockDtlFacade.findMinLockDate(ddl_oum_unit_srno);
        min_lock_date = new DateTime(v_date).plusMonths(1).toDate();
        min_lock_date = new Date(min_lock_date.getYear(), min_lock_date.getMonth(), 1);
        max_lock_date = new Date(new Date().getYear(), new Date().getMonth(), 1);
        setDdl_year();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_oum_unit_srno">
    String ddl_oum_unit_srno;
    List<SelectItem> ddl_oum_unit_srno_options;

    public String getDdl_oum_unit_srno() {
        return ddl_oum_unit_srno;
    }

    public void setDdl_oum_unit_srno(String ddl_oum_unit_srno) {
        this.ddl_oum_unit_srno = ddl_oum_unit_srno;
    }

    public List<SelectItem> getDdl_oum_unit_srno_options() {
        return ddl_oum_unit_srno_options;
    }

    public void setDdl_oum_unit_srno_options(List<SelectItem> ddl_oum_unit_srno_options) {
        this.ddl_oum_unit_srno_options = ddl_oum_unit_srno_options;
    }

    public void setDdl_oum_unit_srno_options() {
        ddl_oum_unit_srno_options = new ArrayList<SelectItem>();
        List<SystOrgUnitMst> lst = systOrgUnitMstFacade.findAll(globalData.getWorking_ou(), false, false, false, false, false, globalData.getUser_id(), "O");
        Integer n = lst.size();
        for (int i = 0; i < n; i++) {
            ddl_oum_unit_srno_options.add(new SelectItem(lst.get(i).getOumUnitSrno().toString(), lst.get(i).getOumName()));
        }
        ddl_oum_unit_srno = lst.get(0).getOumUnitSrno().toString();
    }

    public void ddl_oum_unit_srno_changed(ValueChangeEvent event) {
        ddl_oum_unit_srno = event.getNewValue() == null ? null : event.getNewValue().toString();
        setTbl_emp_list();
        setTbl_pending_emp_list();
    }
    //</editor-fold>        
    // <editor-fold defaultstate="collapsed" desc="ddl year"> 
    String ddl_year;
    List<SelectItem> ddl_year_options;

    public String getDdl_year() {
        return ddl_year;
    }

    public void setDdl_year(String ddl_year) {
        this.ddl_year = ddl_year;
    }

    public List<SelectItem> getDdl_year_options() {
        return ddl_year_options;
    }

    public void setDdl_year_options(List<SelectItem> ddl_year_options) {
        this.ddl_year_options = ddl_year_options;
    }

    public void ddl_year_changed(ValueChangeEvent event) {
        ddl_year = event.getNewValue() == null ? null : event.getNewValue().toString();
        setDdl_month();
    }

    private void setDdl_year() {
        Integer v_from_year = min_lock_date.getYear() + 1900;
        Integer v_to_year = max_lock_date.getYear() + 1900;
        ddl_year_options = new ArrayList<SelectItem>();
        while (v_from_year <= v_to_year) {
            ddl_year_options.add(new SelectItem(v_from_year));
            v_from_year++;
        }
        if (ddl_year_options.isEmpty()) {
            ddl_year = null;
        } else {
            ddl_year = String.valueOf((new Date()).getYear() + 1900);
        }
        setDdl_month();
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ddl_month">
    String ddl_month;
    List<SelectItem> ddl_month_options;

    public List<SelectItem> getDdl_month_options() {
        return ddl_month_options;
    }

    public void setDdl_month_options(List<SelectItem> ddl_month_options) {
        this.ddl_month_options = ddl_month_options;
    }

    public String getDdl_month() {
        return ddl_month;
    }

    public void setDdl_month(String ddl_month) {
        this.ddl_month = ddl_month;
    }

    public void ddl_month_changed(ValueChangeEvent event) {
        ddl_month = event.getNewValue() == null ? null : event.getNewValue().toString();
        setTbl_emp_list();
        setTbl_pending_emp_list();
    }

    private void setDdl_month() {
        ddl_month_options = new ArrayList<SelectItem>();
        if (ddl_year == null) {
            ddl_month_options.add(new SelectItem(null, "--- Not Available ---"));
            ddl_month = null;
        } else {
            Date v_year_start_date = new Date(Integer.valueOf(ddl_year) - 1900, 0, 1);
            Date v_year_end_date = new Date(Integer.valueOf(ddl_year) - 1900, 11, 31);
            Date v_min_lock_date = min_lock_date;
            Date v_max_lock_date = max_lock_date;
            while (v_min_lock_date.before(v_max_lock_date) || v_min_lock_date.equals(v_max_lock_date)) {
                if (!(v_min_lock_date.before(v_year_start_date))
                        && !(v_min_lock_date.after(v_year_end_date))) {
                    ddl_month_options.add(new SelectItem(DateTimeUtility.ChangeDateFormat(v_min_lock_date, "MM").toString(), DateTimeUtility.ChangeDateFormat(v_min_lock_date, "MMM")));
                }
                v_min_lock_date = new DateTime(v_min_lock_date).plusMonths(1).toDate();
            }
//            if (!v_min_lock_date.before(v_max_lock_date)) {
//                ddl_month_options.add(new SelectItem(null, "---N/A---"));
//                ddl_month = null;
//            } else {
//                ddl_month = ddl_month_options.get(0).getValue().toString();
//            }
            ddl_month = ddl_month_options.get(0).getValue().toString();
            setTbl_emp_list();
            setTbl_pending_emp_list();

        }
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_emp_list"> 
    SD_AttendanceLockDataList tbl_emp_list;

    public SD_AttendanceLockDataList getTbl_emp_list() {
        return tbl_emp_list;
    }

    public void setTbl_emp_list(SD_AttendanceLockDataList tbl_emp_list) {
        this.tbl_emp_list = tbl_emp_list;
    }

    public void setTbl_emp_list() {
        if (ddl_month != null && ddl_year.isEmpty() == false) {
            int v_year = Integer.parseInt(ddl_year);
            Calendar c2 = Calendar.getInstance();
            c2.set(v_year, Integer.parseInt(ddl_month) - 1, 5);
            String v_to_date = DateTimeUtility.ChangeDateFormat(c2.getTime(), null);
            tbl_emp_list = new SD_AttendanceLockDataList();
            if (ddl_oum_unit_srno != null && !v_to_date.isEmpty()) {
                tbl_emp_list = new SD_AttendanceLockDataList(getLst_att_lock(Integer.valueOf(ddl_oum_unit_srno), v_to_date, "C"));
            } else {
                tbl_emp_list = new SD_AttendanceLockDataList(getLst_att_lock(globalData.getWorking_ou(), v_to_date, "C"));
            }
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="getLst_att_lock"> 

    public List<AttendanceLockDataList> getLst_att_lock(Integer p_oum_unit_srno, String p_to_date, String p_status) {
        List<AttendanceLockDataList> lst_emp = new ArrayList<AttendanceLockDataList>();
        FacesContext context = FacesContext.getCurrentInstance();
        String CMD_EMP_LIST = "SELECT * FROM TABLE(FTAS_PACK.GET_USER_FOR_LOCK(" + p_oum_unit_srno + ",'" + p_to_date + "','" + p_status + "')) ORDER BY 1";
        Connection cn = null;
        try {
            Statement st = null;
            try {
                ResultSet res = null;
                try {
                    cn = getGlobalUtilities().getjdbcFES_PROJECT_Oracle().getConnection();
                    int i = 0;
                    st = cn.createStatement();
                    res = st.executeQuery(CMD_EMP_LIST);
                    while (res.next()) {
                        AttendanceLockDataList ent_Atten1 = new AttendanceLockDataList();
                        ent_Atten1.setRowid(i);
                        ent_Atten1.setUser_id(res.getInt("USER_ID"));
                        ent_Atten1.setUser_name(res.getString("USER_NAME"));
                        ent_Atten1.setFrom_date(res.getDate("FROM_DATE"));
                        ent_Atten1.setTo_date(res.getDate("TO_DATE"));
                        ent_Atten1.setLeaves(res.getBigDecimal("LEAVES"));
                        ent_Atten1.setPending_days(res.getBigDecimal("PENDING_DAYS"));

                        lst_emp.add(i, ent_Atten1);
                        i++;
                    }
                } catch (Exception e) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Can't Find Attendance List", "Some unexpected error occurred during finding missing attendance"));
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "getLst_att_lock", null, e);
                } finally {
                    res.close();
                }
            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Can't Find Attendance List", "Some unexpected error occurred during finding missing attendance"));
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "getLst_att_lock", null, e);
            } finally {
                st.close();
            }
        } catch (Exception ex) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "3Can't Find Missing Attendance", "Some unexpected error occurred during finding missing attendance"));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "getLst_missing_att()", null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                    }
                } catch (Exception e) {
                }
            }
        }
        return lst_emp;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_pending_emp_list"> 
    List<AttendanceLockDataList> tbl_pending_emp_list;

    public List<AttendanceLockDataList> getTbl_pending_emp_list() {
        return tbl_pending_emp_list;
    }

    public void setTbl_pending_emp_list(List<AttendanceLockDataList> tbl_pending_emp_list) {
        this.tbl_pending_emp_list = tbl_pending_emp_list;
    }
    AttendanceLockDataList[] selectedEmployees;

    public AttendanceLockDataList[] getSelectedEmployees() {
        return selectedEmployees;
    }

    public void setSelectedEmployees(AttendanceLockDataList[] selectedEmployees) {
        this.selectedEmployees = selectedEmployees;
    }

    public void setTbl_pending_emp_list() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (ddl_month != null && ddl_year.isEmpty() == false) {
            int v_year = Integer.parseInt(ddl_year);
            Calendar c2 = Calendar.getInstance();
            c2.set(v_year, Integer.parseInt(ddl_month) - 1, 5);
            String v_to_date = DateTimeUtility.ChangeDateFormat(c2.getTime(), null);
            tbl_pending_emp_list = new ArrayList<AttendanceLockDataList>();
            if (ddl_oum_unit_srno != null && !v_to_date.isEmpty()) {
                tbl_pending_emp_list = getLst_att_lock(Integer.valueOf(ddl_oum_unit_srno), v_to_date, "P");
            } else {
                tbl_pending_emp_list = getLst_att_lock(globalData.getWorking_ou(), v_to_date, "P");
            }
            if (tbl_pending_emp_list.size() > 0) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Pending Attendance", "There are some pending Attendances for some of the employees. Please check out the given link."));
            }
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_approve_clicked"> 

    public void btn_lock_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (selectedEmployees.length > 0) {
            try {
                List<FtasLockDtl> lsLockDtls = new ArrayList<FtasLockDtl>();
                for (int i = 0; i < selectedEmployees.length; i++) {
                    BigDecimal v_lckd_srgkey = getGlobalSettings().getUniqueSrno((selectedEmployees[i].getFrom_date().getYear() + 1900), "FTAS_LOCK_DTL");
                    FtasLockDtl entLockDtl = new FtasLockDtl();
                    entLockDtl.setLckdSrgKey(v_lckd_srgkey);
                    entLockDtl.setUserId(selectedEmployees[i].getUser_id());
                    entLockDtl.setSelfStatusFlag("A");
                    entLockDtl.setCreateby(globalData.getEnt_login_user());
                    entLockDtl.setCreatedt(new Date());
                    entLockDtl.setOumUnitSrno(globalData.getEnt_working_ou());
                    entLockDtl.setSelfActionBy(globalData.getUser_id());
                    entLockDtl.setSelfActionDate(new Date());

                    entLockDtl.setFromDate(selectedEmployees[i].getFrom_date());
                    entLockDtl.setToDate(selectedEmployees[i].getTo_date());
                    lsLockDtls.add(entLockDtl);

                }
                boolean isTransactionDone = customJpaController.setEmpAttendanceLock(lsLockDtls);
                if (isTransactionDone) {
                    try {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Application Approved", "Selected Application Approved Successfully."));
                        setTbl_emp_list();
                        setTbl_pending_emp_list();
                    } catch (Exception e) {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed reset data", "Selected Application Approved Successfully but failed reset data. Please reload the page"));
                        LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_accept_clicked", null, e);
                    }
                } else {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during Accepting Application."));
                }
            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during Accepting Application."));
                LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_accept_clicked", null, e);
            }
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Select Application", "Please select any Application to Approve/Reject."));
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_leave_detail">
    List<FtasAbsentees> tbl_leave_detail;

    public List<FtasAbsentees> getTbl_leave_detail() {
        return tbl_leave_detail;
    }

    public void setTbl_leave_detail(List<FtasAbsentees> tbl_leave_detail) {
        this.tbl_leave_detail = tbl_leave_detail;
    }
    AttendanceLockDataList ent_view_detail;

    public AttendanceLockDataList getEnt_view_detail() {
        return ent_view_detail;
    }

    public void setEnt_view_detail(AttendanceLockDataList ent_view_detail) {
        tbl_leave_detail = ftasAbsenteesFacade.findAll(Integer.valueOf(ddl_oum_unit_srno), ent_view_detail.getUser_id(), false, false, ent_view_detail.getFrom_date(), ent_view_detail.getTo_date(), null, false, null, false, null);
        RequestContext.getCurrentInstance().execute("pw_dlg_view_detail.show();");
        this.ent_view_detail = ent_view_detail;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_view_detail_click"> 

    public void btn_view_detail_click(ActionEvent event) {
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="init()"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setDdl_oum_unit_srno_options();
        setMinMaxDate();
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings and default methods">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>

    /**
     * Creates a new instance of PIS_Attendance_Lock
     */
    public PIS_Attendance_Lock_Admin() {
    }
    // </editor-fold> 
}