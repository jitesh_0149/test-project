package org.fes.pis.jsf.pages.tas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.Database_Output;
import org.fes.pis.custom_entities.FtastAbsenteesDtlEntry;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.customJpaController;
import org.fes.pis.custom_sessions.ejb_utilities_pisLocal;
import org.fes.pis.db.fes_project.pack.Ftas_Pack;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.*;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.jsf.selectableDataModels.SD_FhrdEmpmst;
import org.fes.pis.jsf.utilities.JSFMessages;
import org.fes.pis.sessions.*;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_LeaveApplication")
@ViewScoped
public class PIS_LeaveApplication implements Serializable {

    @EJB
    private SystMenumstFacadeLocal systMenumstFacade;
    @EJB
    private FhrdEmpleavebalFacadeLocal fhrdEmpleavebalFacade;
    @EJB
    private ejb_utilities_pisLocal ejb_utilities_pis;
    @EJB
    private customJpaController customJpaController;
    @EJB
    private FtasAbsenteesFacadeLocal ftasAbsenteesFacade;
    @EJB
    private FtasHolidayDtlFacadeLocal ftasHolidayDtlFacade;
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    Ou_Pack ou_Pack = new Ou_Pack();
    Ftas_Pack ftas_Pack = new Ftas_Pack();
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Common Details">
    //<editor-fold defaultstate="collapsed" desc="User Selection">
    //<editor-fold defaultstate="collapsed" desc="txt_user_id">
    String txt_user_id;
    String err_txt_user_id;

    public String getErr_txt_user_id() {
        return err_txt_user_id;
    }

    public void setErr_txt_user_id(String err_txt_user_id) {
        this.err_txt_user_id = err_txt_user_id;
    }

    public String getTxt_user_id() {
        return txt_user_id;
    }

    public void setTxt_user_id(String txt_user_id) {
        this.txt_user_id = txt_user_id;
    }
    // <editor-fold defaultstate="collapsed" desc="txt_user_id_changed">

    public void txt_user_id_changed(ValueChangeEvent event) {
        txt_user_id = event.getNewValue().toString();
        err_txt_user_id = null;
        txt_user_id_changed();
    }

    private void txt_user_id_changed() {
        selectedUser = null;
        if (txt_user_id != null && txt_user_id.equals("") != true) {
            selectedUser = fhrdEmpmstFacade.find(Integer.valueOf(txt_user_id));
            if (selectedUser == null) {
                err_txt_user_id = " (User Id is invalid)";
            }
        }
        userSelected();
    }
    // </editor-fold>
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="User Selection Methods">
    //<editor-fold defaultstate="collapsed" desc="selectedUser">
    FhrdEmpmst selectedUser;

    public FhrdEmpmst getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(FhrdEmpmst selectedUser) {
        this.selectedUser = selectedUser;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="userSelectionValid">
    boolean userSelectionValid;

    public boolean isUserSelectionValid() {
        return userSelectionValid;
    }

    public void setUserSelectionValid(boolean userSelectionValid) {
        this.userSelectionValid = userSelectionValid;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="userSelected">

    private void userSelected() {
        userSelectionValid = false;
        if (selectedUser != null) {
            txt_user_id = selectedUser.getUserId().toString();
            if (!getPIS_GlobalSettings().getSysetemUser_Fhrd() && !globalData.getUser_id().equals(selectedUser.getUserId())) {
                if (!fhrdEmpmstFacade.hasAccessSpanO(globalData.getUser_id(), selectedUser.getPostingOumUnitSrno().getOumUnitSrno())) {
                    err_txt_user_id = " (You do not have privilege to mark leave of selected user)";
                }
            } else if (!fhrdEmpleavebalFacade.isUserTransactionPending(selectedUser.getUserId())) {
                if (selectedUser.getOrgRelieveDate() != null || selectedUser.getTerminationDate() != null) {
                    err_txt_user_id = " (This employee has relieved/terminated the organisation)";
                } else {
                    err_txt_user_id = " (Either contract is not given or not does not have any active contract)";
                }
            }
            if (err_txt_user_id == null) {
                userSelectionValid = true;
            }
        } else {
            //err_txt_user_id = " (Invalid User Id)";
            txt_user_id = null;
        }

        rdb_leave_type = "S";
        txt_reason = null;
        setUserManagedFlag();
        setAppliedApplicationList();
        setV_user_last_locked_date();
        setDdl_actionby();
        setDdl_holidays();
        setDdl_leave_period_snl();
        setLst_AbsenteesDtlsClub();
        RequestContext.getCurrentInstance().execute("rdb_leave_type_onchange()");
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="v_user_last_locked_date">
    Date v_user_last_locked_date;

    public Date getV_user_last_locked_date() {
        return v_user_last_locked_date;
    }

    public void setV_user_last_locked_date(Date v_user_last_locked_date) {
        this.v_user_last_locked_date = v_user_last_locked_date;
    }

    private void setV_user_last_locked_date() {
        if (userSelectionValid) {
            v_user_last_locked_date = ejb_utilities_pis.getLastLockedDate(selectedUser.getUserId(), selectedUser.getPostingOumUnitSrno().getOumUnitSrno());
            v_user_last_locked_date = DateTimeUtility.findNextDateFromDays(v_user_last_locked_date, 1);
        } else {
            v_user_last_locked_date = null;
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_actionby">
    String ddl_actionby;
    List<SelectItem> ddl_actionby_options;

    public String getDdl_actionby() {
        return ddl_actionby;
    }

    public void setDdl_actionby(String ddl_actionby) {
        this.ddl_actionby = ddl_actionby;
    }

    public void setDdl_actionby() {
        FacesContext context = FacesContext.getCurrentInstance();
        ddl_actionby_options = new ArrayList<SelectItem>();
        List<FhrdEmpmst> lst_Empmsts;
        if (selectedUser == null) {
            ddl_actionby_options = new ArrayList<SelectItem>();
            ddl_actionby_options.add(new SelectItem(null, "--- Enter User Id ---"));
            ddl_actionby = null;

        } else {
            lst_Empmsts = fhrdEmpmstFacade.findReportingUser(selectedUser.getPostingOumUnitSrno().getOumUnitSrno(), selectedUser.getUserId());
            if (lst_Empmsts.isEmpty()) {
                lst_Empmsts = fhrdEmpmstFacade.findReportingUser(globalData.getWorking_ou(), globalData.getUser_id());
            }
            int n = lst_Empmsts.size();
            if (n != 0) {
                for (int i = 0; i < n; i++) {
                    ddl_actionby_options.add(new SelectItem(lst_Empmsts.get(i).getUserId(), lst_Empmsts.get(i).getUserName()));
                }
            } else {
                ddl_actionby_options = new ArrayList<SelectItem>();
                ddl_actionby_options.add(new SelectItem(null, "--- Not Found ---"));
                ddl_actionby = null;
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Information", "Approval Authority is not defined for Organization Unit: " + globalData.getEnt_working_ou().getOumName()));
            }
            ddl_actionby = selectedUser.getReportingUserId().getUserId().toString();
        }
    }

    public List<SelectItem> getDdl_actionby_options() {
        return ddl_actionby_options;
    }

    public void setDdl_actionby_options(List<SelectItem> ddl_actionby_options) {
        this.ddl_actionby_options = ddl_actionby_options;
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Holiday Detail">
    List<Date> lst_holidays;
    String ddl_holidays;
    List<SelectItem> ddl_holidays_options;

    public String getDdl_holidays() {
        return ddl_holidays;
    }

    public void setDdl_holidays(String ddl_holidays) {
        this.ddl_holidays = ddl_holidays;
    }

    public List<SelectItem> getDdl_holidays_options() {
        return ddl_holidays_options;
    }

    public void setDdl_holidays_options(List<SelectItem> ddl_holidays_options) {
        this.ddl_holidays_options = ddl_holidays_options;
    }

    private void setDdl_holidays() {
        if (userSelectionValid) {
            List<FtasHolidayDtl> lst_HolidayDtls = ftasHolidayDtlFacade.findAll(selectedUser.getPostingOumUnitSrno().getOumUnitSrno());
            lst_holidays = new ArrayList<Date>();
            ddl_holidays_options = new ArrayList<SelectItem>();
            for (FtasHolidayDtl h : lst_HolidayDtls) {
                Date v_date = h.getFtasHolidayDtlPK().getHoliDate();
                String v_date_string = DateTimeUtility.ChangeDateFormat(h.getFtasHolidayDtlPK().getHoliDate(), null);
                lst_holidays.add(v_date);
                ddl_holidays_options.add(new SelectItem(v_date_string + " : " + h.getHoliDesc()));
            }
        } else {
            lst_holidays = null;
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_applied_applications"> 
    String ddl_applied_applications;
    List<SelectItem> ddl_applied_applications_options;

    public String getDdl_applied_applications() {
        return ddl_applied_applications;
    }

    public void setDdl_applied_applications(String ddl_applied_applications) {
        this.ddl_applied_applications = ddl_applied_applications;
    }

    public List<SelectItem> getDdl_applied_applications_options() {
        return ddl_applied_applications_options;
    }

    public void setDdl_applied_applications_options(List<SelectItem> ddl_applied_applications_options) {
        this.ddl_applied_applications_options = ddl_applied_applications_options;
    }

    public void setAppliedApplicationList() {
        ddl_applied_applications_options = new ArrayList<SelectItem>();
        if (userSelectionValid) {
            SelectItem s = new SelectItem(null, "N/A");
            ddl_applied_applications_options.add(s);
            List<String> v_obj = ftasAbsenteesFacade.getAppliedApplication(selectedUser.getUserId(), selectedUser.getPostingOumUnitSrno().getOumUnitSrno());
            ddl_applied_applications_options = new ArrayList<SelectItem>();
            for (String l : v_obj) {
                ddl_applied_applications_options.add(new SelectItem(l.toString()));
            }
        } else {
            ddl_applied_applications = null;
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_reason">
    String txt_reason;

    public String getTxt_reason() {
        return txt_reason;
    }

    public void setTxt_reason(String txt_reason) {
        this.txt_reason = txt_reason;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="rdb_leave_type">
    String rdb_leave_type;

    public String getRdb_leave_type() {
        return rdb_leave_type;
    }

    public void setRdb_leave_type(String rdb_leave_type) {
        this.rdb_leave_type = rdb_leave_type;
    }

    public void rdb_leave_type_changed(ValueChangeEvent event) {
        rdb_leave_type = event.getNewValue() == null ? null : event.getNewValue().toString();

    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="userManagedFlag">
    String userManagedFlag;

    public String getUserManagedFlag() {
        return userManagedFlag;
    }

    public void setUserManagedFlag(String userManagedFlag) {
        this.userManagedFlag = userManagedFlag;
    }

    private void setUserManagedFlag() {
        if (selectedUser != null) {
            userManagedFlag = "Y";
            if (!globalData.getUser_id().equals(selectedUser.getUserId())
                    && (globalData.getUser_id().equals(selectedUser.getReportingUserId().getUserId())
                    || globalData.isSystemUserFHRD())) {
                userManagedFlag = null;
            }
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Single Application Settings">
    //<editor-fold defaultstate="collapsed" desc="out_total_leaves_snl">
    String out_total_leaves_snl;

    public String getOut_total_leaves_snl() {
        return out_total_leaves_snl;
    }

    public void setOut_total_leaves_snl(String out_total_leaves_snl) {
        this.out_total_leaves_snl = out_total_leaves_snl;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="selectedEmpleavebalSnl">
    FhrdEmpleavebal selectedEmpleavebalSnl;

    public FhrdEmpleavebal getSelectedEmpleavebalSnl() {
        return selectedEmpleavebalSnl;
    }

    public void setSelectedEmpleavebalSnl(FhrdEmpleavebal selectedEmpleavebalSnl) {
        this.selectedEmpleavebalSnl = selectedEmpleavebalSnl;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_leave_period_snl">
    FhrdEmpleavebal ddl_leave_period_snl;
    List<FhrdEmpleavebal> ddl_leave_period_snl_options;

    public FhrdEmpleavebal getDdl_leave_period_snl() {
        return ddl_leave_period_snl;
    }

    public void setDdl_leave_period_snl(FhrdEmpleavebal ddl_leave_period_snl) {
        this.ddl_leave_period_snl = ddl_leave_period_snl;
    }

    public List<FhrdEmpleavebal> getDdl_leave_period_snl_options() {
        return ddl_leave_period_snl_options;
    }

    public void setDdl_leave_period_snl_options(List<FhrdEmpleavebal> ddl_leave_period_snl_options) {
        this.ddl_leave_period_snl_options = ddl_leave_period_snl_options;
    }

    private void setDdl_leave_period_snl() {
        out_total_leaves_snl = "0";
        selectedEmpleavebalSnl = null;
        ddl_leave_period_snl = null;
        ddl_parent_leave_snl_null_label = "--- Select Allotment Period ---";
        ddl_parent_leave_snl_mand = false;
        if (userSelectionValid) {
            ddl_leave_period_snl_options = fhrdEmpleavebalFacade.findAllFromLeave(selectedUser.getPostingOumUnitSrno().getOumUnitSrno(), selectedUser.getUserId(), true, "N", false, "Y", null, null, null, "Y", null, false, null, null, null, null, null, userManagedFlag);
        } else {
            ddl_leave_period_snl_options = null;
        }
        setDdl_ttr_leave_period_snl();
    }

    public void ddl_leave_period_snl_changed(ValueChangeEvent event) {
        ddl_leave_period_snl = event.getNewValue() == null ? null : fhrdEmpleavebalFacade.find(((FhrdEmpleavebal) event.getNewValue()).getEmplbSrgKey());
        setDdl_ttr_leave_period_snl();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_ttr_leave_period_snl">
    FhrdEmpleavebal ddl_ttr_leave_period_snl;
    List<FhrdEmpleavebal> ddl_ttr_leave_period_snl_options;
    String ddl_ttr_leave_period_snl_null_label;
    boolean ddl_ttr_leave_period_snl_mand = false;

    public FhrdEmpleavebal getDdl_ttr_leave_period_snl() {
        return ddl_ttr_leave_period_snl;
    }

    public void setDdl_ttr_leave_period_snl(FhrdEmpleavebal ddl_ttr_leave_period_snl) {
        this.ddl_ttr_leave_period_snl = ddl_ttr_leave_period_snl;
    }

    public List<FhrdEmpleavebal> getDdl_ttr_leave_period_snl_options() {
        return ddl_ttr_leave_period_snl_options;
    }

    public void setDdl_ttr_leave_period_snl_options(List<FhrdEmpleavebal> ddl_ttr_leave_period_snl_options) {
        this.ddl_ttr_leave_period_snl_options = ddl_ttr_leave_period_snl_options;
    }

    public String getDdl_ttr_leave_period_snl_null_label() {
        return ddl_ttr_leave_period_snl_null_label;
    }

    public void setDdl_ttr_leave_period_snl_null_label(String ddl_ttr_leave_period_snl_null_label) {
        this.ddl_ttr_leave_period_snl_null_label = ddl_ttr_leave_period_snl_null_label;
    }

    public boolean isDdl_ttr_leave_period_snl_mand() {
        return ddl_ttr_leave_period_snl_mand;
    }

    public void setDdl_ttr_leave_period_snl_mand(boolean ddl_ttr_leave_period_snl_mand) {
        this.ddl_ttr_leave_period_snl_mand = ddl_ttr_leave_period_snl_mand;
    }

    private void setDdl_ttr_leave_period_snl() {
        selectedEmpleavebalSnl = null;
        ddl_ttr_leave_period_snl_mand = false;
        ddl_ttr_leave_period_snl_options = null;
        ddl_ttr_leave_period_snl = null;
        ddl_ttr_leave_period_snl_null_label = "--- Not Available ---";
        if (ddl_leave_period_snl != null) {
            String v_virtual_leave = ddl_leave_period_snl.getElrSrgKey().getLmSrgKey().getVirtualLeave();
            if (v_virtual_leave.equals("T")) {
                ddl_ttr_leave_period_snl_mand = true;
                ddl_ttr_leave_period_snl_options = fhrdEmpleavebalFacade.findAllFromLeave(selectedUser.getPostingOumUnitSrno().getOumUnitSrno(), selectedUser.getUserId(), true, null, true, "Y", null, null, "Y", "Y", null, false, null, null, null, null, null, userManagedFlag);
                if (!ddl_ttr_leave_period_snl_options.isEmpty()) {
                    ddl_ttr_leave_period_snl_null_label = "--- Select ---";
                }
            } else {
                ddl_ttr_leave_period_snl_null_label = "--- N/A ---";
            }
        }
        if (!ddl_ttr_leave_period_snl_mand) {
            selectedEmpleavebalSnl = ddl_leave_period_snl;
        }
        setDdl_parent_leave_period_snl();
    }

    public void ddl_ttr_leave_period_snl_changed(ValueChangeEvent event) {
        ddl_ttr_leave_period_snl = event.getNewValue() == null ? null : fhrdEmpleavebalFacade.find(((FhrdEmpleavebal) event.getNewValue()).getEmplbSrgKey());
        if (ddl_ttr_leave_period_snl_mand) {
            selectedEmpleavebalSnl = ddl_ttr_leave_period_snl;
        }
        setDdl_parent_leave_period_snl();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_parent_leave_period_snl">
    FhrdEmpleavebal ddl_parent_leave_period_snl;
    List<FhrdEmpleavebal> ddl_parent_leave_period_snl_options;
    String ddl_parent_leave_snl_null_label;
    BigDecimal leave_ratio_snl;
    boolean ddl_parent_leave_snl_mand = false;

    public FhrdEmpleavebal getDdl_parent_leave_period_snl() {
        return ddl_parent_leave_period_snl;
    }

    public void setDdl_parent_leave_period_snl(FhrdEmpleavebal ddl_parent_leave_period_snl) {
        this.ddl_parent_leave_period_snl = ddl_parent_leave_period_snl;
    }

    public List<FhrdEmpleavebal> getDdl_parent_leave_period_snl_options() {
        return ddl_parent_leave_period_snl_options;
    }

    public void setDdl_parent_leave_period_snl_options(List<FhrdEmpleavebal> ddl_parent_leave_period_snl_options) {
        this.ddl_parent_leave_period_snl_options = ddl_parent_leave_period_snl_options;
    }

    public String getDdl_parent_leave_snl_null_label() {
        return ddl_parent_leave_snl_null_label;
    }

    public void setDdl_parent_leave_snl_null_label(String ddl_parent_leave_snl_null_label) {
        this.ddl_parent_leave_snl_null_label = ddl_parent_leave_snl_null_label;
    }

    public boolean isDdl_parent_leave_snl_mand() {
        return ddl_parent_leave_snl_mand;
    }

    public void setDdl_parent_leave_snl_mand(boolean ddl_parent_leave_snl_mand) {
        this.ddl_parent_leave_snl_mand = ddl_parent_leave_snl_mand;
    }

    public BigDecimal getLeave_ratio_snl() {
        return leave_ratio_snl;
    }

    public void setLeave_ratio_snl(BigDecimal leave_ratio_snl) {
        this.leave_ratio_snl = leave_ratio_snl;
    }

    private void setDdl_parent_leave_period_snl() {
        leave_ratio_snl = null;
        selectedEmpleavebalSnl = null;
        ddl_parent_leave_snl_mand = false;
        ddl_parent_leave_period_snl_options = null;
        ddl_parent_leave_period_snl = null;
        ddl_parent_leave_snl_null_label = "--- Not Available ---";
        FhrdEmpleavebal ent_temp = ddl_ttr_leave_period_snl_mand ? ddl_ttr_leave_period_snl : ddl_leave_period_snl;
        if (ent_temp != null) {
            String v_virtual_leave = ent_temp.getElrSrgKey().getLmSrgKey().getVirtualLeave();
            if (v_virtual_leave.equals("L")) {
                ddl_parent_leave_snl_mand = true;
                ddl_parent_leave_period_snl_options = fhrdEmpleavebalFacade.findAllFromLeave(selectedUser.getPostingOumUnitSrno().getOumUnitSrno(), selectedUser.getUserId(), true, null, true, "Y", "N", "Y", null, "Y", null, false, null, null, null, null, null, userManagedFlag);
                if (!ddl_parent_leave_period_snl_options.isEmpty()) {
                    ddl_parent_leave_snl_null_label = "--- Select ---";
                }
            } else {
                ddl_parent_leave_snl_null_label = "--- N/A ---";
            }
        }
        if (!ddl_parent_leave_snl_mand) {
            selectedEmpleavebalSnl = ent_temp;
        }
        setCal_from_date_snl();
    }

    public void ddl_parent_leave_period_snl_changed(ValueChangeEvent event) {
        ddl_parent_leave_period_snl = event.getNewValue() == null ? null : fhrdEmpleavebalFacade.find(((FhrdEmpleavebal) event.getNewValue()).getEmplbSrgKey());
        if (ddl_parent_leave_snl_mand) {
            selectedEmpleavebalSnl = ddl_parent_leave_period_snl;
            if (ddl_parent_leave_period_snl != null) {
                if (ddl_ttr_leave_period_snl_mand) {
                    leave_ratio_snl = ftasAbsenteesFacade.getLeaveRatio(ddl_ttr_leave_period_snl.getLrSrgKey().getLrSrgKey(), ddl_parent_leave_period_snl.getLrSrgKey().getLrSrgKey());
                } else {
                    leave_ratio_snl = ftasAbsenteesFacade.getLeaveRatio(ddl_leave_period_snl.getLrSrgKey().getLrSrgKey(), ddl_parent_leave_period_snl.getLrSrgKey().getLrSrgKey());
                }
            } else {
                leave_ratio_snl = null;
            }
        }
        setCal_from_date_snl();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_from_date_snl">
    Date cal_from_date_snl;
    String cal_from_date_snl_min;
    String cal_from_date_snl_max;

    public Date getCal_from_date_snl() {
        return cal_from_date_snl;
    }

    public void setCal_from_date_snl(Date cal_from_date_snl) {
        this.cal_from_date_snl = cal_from_date_snl;
    }

    public String getCal_from_date_snl_max() {
        return cal_from_date_snl_max;
    }

    public void setCal_from_date_snl_max(String cal_from_date_snl_max) {
        this.cal_from_date_snl_max = cal_from_date_snl_max;
    }

    public String getCal_from_date_snl_min() {
        return cal_from_date_snl_min;
    }

    public void setCal_from_date_snl_min(String cal_from_date_snl_min) {
        this.cal_from_date_snl_min = cal_from_date_snl_min;
    }

    public void cal_from_date_snl_changed(SelectEvent event) {
        cal_from_date_snl = (Date) event.getObject();
        boolean holidaySelected = false;
        if (lst_holidays.contains(cal_from_date_snl)) {
            holidaySelected = true;
        }
        if (holidaySelected && selectedEmpleavebalSnl.getElrSrgKey().getHolidayEmbedded().equals("N")) {
            while (lst_holidays.contains(cal_from_date_snl)) {
                cal_from_date_snl = DateTimeUtility.findNextDateFromDays(cal_from_date_snl, 1);
            }
            if (cal_from_date_snl.after(DateTimeUtility.stringToDate(cal_from_date_snl_max, null))) {
                setMessageDetail(FacesMessage.SEVERITY_WARN, "Wrong from date", "Can not select your date as it might be exceeding your leave period after availble holiday");
                cal_from_date_snl = null;
            } else {
                setMessageDetail(FacesMessage.SEVERITY_WARN, "Holiday is selected", "Can not select date on holiday to apply from so selecting next date from the holiday");
            }
        }
        setDdl_from_half_snl();
    }

    private void setCal_from_date_snl() {
        cal_from_date_snl = null;
        if (selectedEmpleavebalSnl != null) {
            cal_from_date_snl_min = DateTimeUtility.ChangeDateFormat(DateTimeUtility.findBiggestDate(v_user_last_locked_date, maxOpeningDateSnl()), null);
            cal_from_date_snl_max = DateTimeUtility.ChangeDateFormat(minClosingDateSnl(), null);
        }
        setDdl_from_half_snl();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_from_half_snl">
    String ddl_from_half_snl;
    List<SelectItem> ddl_from_half_snl_options;

    public List<SelectItem> getDdl_from_half_snl_options() {
        return ddl_from_half_snl_options;
    }

    public void setDdl_from_half_snl_options(List<SelectItem> ddl_from_half_snl_options) {
        this.ddl_from_half_snl_options = ddl_from_half_snl_options;
    }

    public String getDdl_from_half_snl() {
        return ddl_from_half_snl;
    }

    public void setDdl_from_half_snl(String ddl_from_half_snl) {
        this.ddl_from_half_snl = ddl_from_half_snl;
    }

    public void setDdl_from_half_snl() {
        ddl_from_half_snl = null;
        ddl_from_half_snl_options = new ArrayList<SelectItem>();
        if (cal_from_date_snl != null) {
            if (selectedEmpleavebalSnl.getElrSrgKey().getHalfdayApplicable().equals("Y")) {
                ddl_from_half_snl_options.add(new SelectItem("1", "First Half"));
                ddl_from_half_snl_options.add(new SelectItem("2", "Second Half"));
            }
            ddl_from_half_snl_options.add(0, new SelectItem("0", "Full Day"));
            ddl_from_half_snl = "0";
        }
        setCal_to_date_snl();
    }

    public void ddl_from_half_snl_changed(ValueChangeEvent event) {
        ddl_from_half_snl = event.getNewValue() == null ? null : event.getNewValue().toString();
        setCal_to_date_snl();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_to_date_snl">    
    Date cal_to_date_snl;
    String cal_to_date_snl_min;
    String cal_to_date_snl_max;

    public Date getCal_to_date_snl() {
        return cal_to_date_snl;
    }

    public void setCal_to_date_snl(Date cal_to_date_snl) {
        this.cal_to_date_snl = cal_to_date_snl;
    }

    public String getCal_to_date_snl_max() {
        return cal_to_date_snl_max;
    }

    public void setCal_to_date_snl_max(String cal_to_date_snl_max) {
        this.cal_to_date_snl_max = cal_to_date_snl_max;
    }

    public String getCal_to_date_snl_min() {
        return cal_to_date_snl_min;
    }

    public void setCal_to_date_snl_min(String cal_to_date_snl_min) {
        this.cal_to_date_snl_min = cal_to_date_snl_min;
    }

    public void cal_to_date_snl_changed(SelectEvent event) {
        cal_to_date_snl = (Date) event.getObject();
        boolean holidaySelected = false;
        if (lst_holidays.contains(cal_to_date_snl)) {
            holidaySelected = true;
        }
        if (holidaySelected && selectedEmpleavebalSnl.getElrSrgKey().getHolidayEmbedded().equals("N")) {
            while (lst_holidays.contains(cal_to_date_snl)) {
                cal_to_date_snl = DateTimeUtility.findPrevDateFromDays(cal_to_date_snl, 1);
            }
            if (cal_to_date_snl.after(DateTimeUtility.stringToDate(cal_to_date_snl_max, null))) {
                setMessageDetail(FacesMessage.SEVERITY_WARN, "Wrong to date", "Can not select your date as it might be exceeding your leave period before availble holiday");
                cal_to_date_snl = null;
            } else {
                setMessageDetail(FacesMessage.SEVERITY_WARN, "Holiday is selected", "Can not select date on holiday to apply to so selecting previous date to the holiday");
            }
        }
        setDdl_to_half_snl();
    }

    private void setCal_to_date_snl() {
        cal_to_date_snl = null;
        if (ddl_from_half_snl != null) {
            if (ddl_from_half_snl.equals("1")) {
                cal_to_date_snl_min = DateTimeUtility.ChangeDateFormat(cal_from_date_snl, null);
                cal_to_date_snl_max = DateTimeUtility.ChangeDateFormat(cal_from_date_snl, null);
            } else {
                double dbl_min_taken = Double.valueOf(minTakenSnl().toString());
                double v_min_taken = Integer.valueOf(String.valueOf(Math.round(Math.floor(dbl_min_taken))));
                Date v_max_date = null;
                Date v_min_date;
                if (ddl_from_half_snl.equals("2")) {
                    if (dbl_min_taken >= v_min_taken) {
                        v_min_taken = v_min_taken + 1;
                    }
                } else {
                    if (dbl_min_taken > v_min_taken) {
                        v_min_taken = v_min_taken + 1;
                    }
                }
                v_min_date = DateTimeUtility.findNextDateFromDays(cal_from_date_snl, (int) (v_min_taken - 1));
                BigDecimal v_min_balance = minBalanceSnl();
                if (v_min_balance != null) {
                    double dbl_balance = Double.valueOf(v_min_balance.toString());
                    double v_balance = Integer.valueOf(String.valueOf(Math.round(Math.floor(dbl_balance))));
                    if ((dbl_balance > v_balance && selectedEmpleavebalSnl.getElrSrgKey().getHalfdayApplicable().equals("Y")) || ddl_from_half_snl.equals("2")) {
                        v_balance = v_balance + 1;
                    }
                    v_max_date = DateTimeUtility.findNextDateFromDays(cal_from_date_snl, (int) (v_balance - 1));

                }
                BigDecimal v_max_taken = maxTakenSnl();
                if (v_max_taken != null) {
                    Date v_temp_date;
                    if (ddl_from_half_snl.equals("2")) {
                        v_temp_date = DateTimeUtility.findNextDateFromDays(cal_from_date_snl, v_max_taken.intValue());
                    } else {
                        v_temp_date = DateTimeUtility.findNextDateFromDays(cal_from_date_snl, v_max_taken.intValue() - 1);
                    }
                    v_max_date = DateTimeUtility.findSmallestDate(v_max_date, v_temp_date);
                }
                if (v_max_date != null && selectedEmpleavebalSnl.getElrSrgKey().getHolidayEmbedded().equals("N")) {
                    Date v_temp_date = cal_from_date_snl;
                    while (!v_temp_date.after(v_max_date)) {
                        if (lst_holidays.contains(v_temp_date)) {
                            v_max_date = DateTimeUtility.findNextDateFromDays(v_max_date, 1);
                        }
                        v_temp_date = DateTimeUtility.findNextDateFromDays(v_temp_date, 1);
                    }
                }


                v_max_date = DateTimeUtility.findSmallestDate(v_max_date, minClosingDateSnl());
                cal_to_date_snl_min = DateTimeUtility.ChangeDateFormat(v_min_date, null);
                cal_to_date_snl_max = DateTimeUtility.ChangeDateFormat(v_max_date, null);
            }
        }
        setDdl_to_half_snl();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_to_half_snl">
    String ddl_to_half_snl;
    List<SelectItem> ddl_to_half_snl_options;

    public List<SelectItem> getDdl_to_half_snl_options() {
        return ddl_to_half_snl_options;
    }

    public void setDdl_to_half_snl_options(List<SelectItem> ddl_to_half_snl_options) {
        this.ddl_to_half_snl_options = ddl_to_half_snl_options;
    }

    public String getDdl_to_half_snl() {
        return ddl_to_half_snl;
    }

    public void setDdl_to_half_snl(String ddl_to_half_snl) {
        this.ddl_to_half_snl = ddl_to_half_snl;
    }

    public void setDdl_to_half_snl() {
        ddl_to_half_snl = null;
        ddl_to_half_snl_options = new ArrayList<SelectItem>();
        if (cal_to_date_snl != null) {
            if (selectedEmpleavebalSnl.getElrSrgKey().getHalfdayApplicable().equals("N")) {
                ddl_to_half_snl_options = new ArrayList<SelectItem>();
                ddl_to_half_snl_options.add(new SelectItem("0", "Full Day"));
                ddl_to_half_snl = "0";
            } else if (cal_from_date_snl.equals(cal_to_date_snl) && ddl_from_half_snl.equals("0")) {
                ddl_to_half_snl_options = new ArrayList<SelectItem>();
                ddl_to_half_snl_options.add(new SelectItem("0", "Full Day"));
                ddl_to_half_snl = "0";
            } else if (cal_from_date_snl.equals(cal_to_date_snl) && ddl_from_half_snl.equals("1")) {
                ddl_to_half_snl_options = new ArrayList<SelectItem>();
                ddl_to_half_snl_options.add(new SelectItem("1", "First Half"));
                ddl_to_half_snl = "1";
            } else if (cal_from_date_snl.equals(cal_to_date_snl) && ddl_from_half_snl.equals("2")) {
                ddl_to_half_snl_options = new ArrayList<SelectItem>();
                ddl_to_half_snl_options.add(new SelectItem("2", "Second Half"));
                ddl_to_half_snl = "2";
            } else if (!cal_from_date_snl.equals(cal_to_date_snl)) {
                if (DateTimeUtility.stringToDate(cal_to_date_snl_max, null).equals(cal_to_date_snl)) {
                    double dbl_balance = Double.valueOf(minBalanceSnl().toString());
                    double v_balance = Integer.valueOf(String.valueOf(Math.round(Math.floor(dbl_balance))));
                    boolean reCalculateForLeaveEnd = false;
                    if (ddl_from_half_snl.equals("2") && dbl_balance == v_balance) {
                        ddl_to_half_snl_options = new ArrayList<SelectItem>();
                        ddl_to_half_snl_options.add(new SelectItem("1", "First Half"));
                        ddl_to_half_snl = "1";
                        reCalculateForLeaveEnd = true;
                    } else if (!ddl_from_half_snl.equals("2") && dbl_balance > v_balance) {
                        ddl_to_half_snl_options = new ArrayList<SelectItem>();
                        ddl_to_half_snl_options.add(new SelectItem("1", "First Half "));
                        ddl_to_half_snl = "1";
                        reCalculateForLeaveEnd = true;
                    } else {
                        ddl_to_half_snl_options = new ArrayList<SelectItem>();
                        ddl_to_half_snl_options.add(new SelectItem("0", "Full Day"));
                        ddl_to_half_snl_options.add(new SelectItem("1", "First Half"));
                        ddl_to_half_snl = "0";
                    }
                    if (reCalculateForLeaveEnd) {
                        calculateTotalLeaveSnl();
                        if (Double.valueOf(out_total_leaves_snl).compareTo(dbl_balance) == -1) {
                            ddl_to_half_snl_options = new ArrayList<SelectItem>();
                            ddl_to_half_snl_options.add(new SelectItem("0", "Full Day"));
                            ddl_to_half_snl_options.add(new SelectItem("1", "First Half"));
                            ddl_to_half_snl = "0";
                        }
                    }
                } else {
                    ddl_to_half_snl_options = new ArrayList<SelectItem>();
                    ddl_to_half_snl_options.add(new SelectItem("0", "Full Day"));
                    ddl_to_half_snl_options.add(new SelectItem("1", "First Half"));
                    ddl_to_half_snl = "0";
                }
            }

        }
        calculateTotalLeaveSnl();
    }

    public void ddl_to_half_snl_changed(ValueChangeEvent event) {
        ddl_to_half_snl = event.getNewValue() == null ? null : event.getNewValue().toString();
        calculateTotalLeaveSnl();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calculateTotalLeaveSnl">

    private void calculateTotalLeaveSnl() {
        if (ddl_to_half_snl != null) {
            double totalDays = Double.valueOf(String.valueOf(DateTimeUtility.findDuration(cal_from_date_snl, cal_to_date_snl, "D"))) + 1;
            if (ddl_from_half_snl.equals("2")) {
                totalDays = totalDays - 0.5;
            }
            if (ddl_to_half_snl.equals("1")) {
                totalDays = totalDays - 0.5;
            }
            if (selectedEmpleavebalSnl.getElrSrgKey().getHolidayEmbedded().equals("N")) {
                int totalHolidayCount = 0;
                Date v_from_date = cal_from_date_snl;
                while (!v_from_date.after(cal_to_date_snl)) {
                    if (lst_holidays.contains(v_from_date)) {
                        totalHolidayCount++;
                    }
                    v_from_date = DateTimeUtility.findNextDateFromDays(v_from_date, 1);
                }
                totalDays = totalDays - totalHolidayCount;
            }
            out_total_leaves_snl = String.valueOf(totalDays);
        } else {
            out_total_leaves_snl = "0";
        }
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="btn_apply_leave_snl_clicked">

    public void btn_apply_leave_snl_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            selectedEmpleavebalSnl = fhrdEmpleavebalFacade.find(selectedEmpleavebalSnl.getEmplbSrgKey());
//            Database_Output database_Output = ftas_Pack.Check_Leave_TTR_ContOverlap(this.getClass().getName(),
//                    "btn_add_to_list_leave_club_clicked", "L",
//                    selectedUser.getUserId(), cal_from_date_snl, cal_to_date_snl,
//                    ddl_from_half_snl, ddl_to_half_snl);
//            if (database_Output.isExecuted_successfully()) {
//                if (database_Output.getString1().equals("Y") == false) {
//                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Leave/Tour/Training already applied", "You already applied Application for this Period " + database_Output.getString1()));
//                    return;
//                }
//            } else {
//                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error in checking previously added Applications."));
//                return;
//            }
            Database_Output database_Output = ftas_Pack.VALIDATE_ABSENTEES_DTL(this.getClass().getName(),
                    "btn_apply_leave_snl_clicked",
                    ddl_leave_period_snl.getEmplbSrgKey(),
                    selectedEmpleavebalSnl.getEmplbSrgKey(),
                    ddl_parent_leave_snl_mand ? (ddl_ttr_leave_period_snl_mand ? ddl_ttr_leave_period_snl.getEmplbSrgKey() : ddl_leave_period_snl.getEmplbSrgKey()) : null,
                    ddl_parent_leave_snl_mand ? ddl_parent_leave_period_snl.getEmplbSrgKey() : null,
                    ddl_ttr_leave_period_snl_mand ? ddl_leave_period_snl.getEmplbSrgKey() : null,
                    "L",
                    "S",
                    cal_from_date_snl,
                    cal_to_date_snl,
                    ddl_from_half_snl,
                    ddl_to_half_snl,
                    new BigDecimal(out_total_leaves_snl),
                    new BigDecimal(out_total_leaves_snl),
                    BigDecimal.ZERO,
                    "Y",
                    globalData.getUser_id(),
                    null,
                    "Y",
                    "N");
            if (database_Output.isExecuted_successfully()) {
                if (database_Output.getString1().equals("Y") == false) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "Reason : " + database_Output.getString1()));
                    return;
                }
            } else {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error in checking previously added Applications."));
                return;
            }

            if (ddl_ttr_leave_period_snl_mand) {
                BigDecimal v_count_for_whileOnTourTraining = ftasAbsenteesFacade.checkForWhileOnTourTraining(selectedUser.getUserId(), cal_from_date_snl, cal_to_date_snl);
                if (v_count_for_whileOnTourTraining.equals(BigDecimal.ZERO)) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Tour/Training must be applied and approved before applying the " + ddl_leave_period_snl.getLmSrgKey().getLeaveDesc() + ", for the period From : " + DateTimeUtility.ChangeDateFormat(cal_from_date_snl, null) + " To : " + DateTimeUtility.ChangeDateFormat(cal_to_date_snl, null) + ""));
                    return;
                }
            }
            FtasAbsentees entFtasAbsentees = new FtasAbsentees();
            Integer v_cal_year = (cal_from_date_snl.getYear() + 1900);
            BigDecimal v_abs_srgkey = getGlobalSettings().getUniqueSrno(v_cal_year, "FTAS_ABSENTEES");
            Integer v_apprv_auth_no = Integer.valueOf(ddl_actionby);
            Integer v_apprv_auth_oum = fhrdEmpmstFacade.find(v_apprv_auth_no).getPostingOumUnitSrno().getOumUnitSrno();

            entFtasAbsentees.setAbsSrgKey(v_abs_srgkey);
            entFtasAbsentees.setTranYear(Short.valueOf(v_cal_year.toString()));
            entFtasAbsentees.setUserId(selectedUser);
            entFtasAbsentees.setSrno(-1);
            entFtasAbsentees.setDays(new BigDecimal(out_total_leaves_snl));
            entFtasAbsentees.setEmpno(selectedUser.getEmpno());
            entFtasAbsentees.setFromDate(cal_from_date_snl);
            entFtasAbsentees.setToDate(cal_to_date_snl);
            entFtasAbsentees.setFromhalf(ddl_from_half_snl);
            entFtasAbsentees.setTohalf(ddl_to_half_snl);
            entFtasAbsentees.setActnby(new FhrdEmpmst(v_apprv_auth_no));
            entFtasAbsentees.setAprvAuthOum(new SystOrgUnitMst(v_apprv_auth_oum));
            entFtasAbsentees.setActnpurp("A");
            entFtasAbsentees.setApplicationFlag("L");
            entFtasAbsentees.setAppliType("S");
            entFtasAbsentees.setLeaveBalLaps(BigDecimal.ZERO);
            entFtasAbsentees.setReason1(txt_reason.toUpperCase().trim());
            entFtasAbsentees.setCreateby(globalData.getEnt_login_user());
            entFtasAbsentees.setOumUnitSrno(selectedUser.getPostingOumUnitSrno());
            entFtasAbsentees.setCreatedt(new Date());
            List<FtasAbsenteesDtl> lst_FtasAbsenteesesDtl = new ArrayList<FtasAbsenteesDtl>();

            FtasAbsenteesDtl entAbsenteesDtl = new FtasAbsenteesDtl();
            BigDecimal v_absd_srgkey = getGlobalSettings().getUniqueSrno(v_cal_year, "FTAS_ABSENTEES_DTL");
            entAbsenteesDtl.setAbsdSrgKey(v_absd_srgkey);
            entAbsenteesDtl.setAbsSrgKey(entFtasAbsentees);
            entAbsenteesDtl.setTranYear(Short.valueOf(v_cal_year.toString()));
            entAbsenteesDtl.setUserId(selectedUser);
            entAbsenteesDtl.setDays(new BigDecimal(out_total_leaves_snl));
            entAbsenteesDtl.setTotalDays(entAbsenteesDtl.getDays());
            entAbsenteesDtl.setFromDate(cal_from_date_snl);
            entAbsenteesDtl.setToDate(cal_to_date_snl);
            entAbsenteesDtl.setFromhalf(ddl_from_half_snl);
            entAbsenteesDtl.setTranYear(Short.valueOf(v_cal_year.toString()));
            entAbsenteesDtl.setTohalf(ddl_to_half_snl);
            entAbsenteesDtl.setLastLeavecd("Y");
            entAbsenteesDtl.setActualEmplbSrgKey(selectedEmpleavebalSnl);
            entAbsenteesDtl.setActualElrSrgKey(entAbsenteesDtl.getActualEmplbSrgKey().getElrSrgKey());
            entAbsenteesDtl.setActualLmSrgKey(entAbsenteesDtl.getActualEmplbSrgKey().getLmSrgKey());
            entAbsenteesDtl.setEmplbSrgKey(ddl_leave_period_snl);
            entAbsenteesDtl.setElrSrgKey(entAbsenteesDtl.getEmplbSrgKey().getElrSrgKey());
            entAbsenteesDtl.setLmSrgKey(entAbsenteesDtl.getEmplbSrgKey().getLmSrgKey());
            entAbsenteesDtl.setLeavecd(entAbsenteesDtl.getEmplbSrgKey().getLmSrgKey().getLeavecd());


            if (ddl_parent_leave_snl_mand) {
                entAbsenteesDtl.setParentEmplbSrgKey(ddl_parent_leave_period_snl);
                if (ddl_ttr_leave_period_snl_mand) {
                    entAbsenteesDtl.setChildEmplbSrgKey(ddl_ttr_leave_period_snl);
                } else {
                    entAbsenteesDtl.setChildEmplbSrgKey(ddl_leave_period_snl);
                }

            }
            if (ddl_ttr_leave_period_snl_mand) {
                entAbsenteesDtl.setTtrEmplbSrgKey(ddl_leave_period_snl);
            }
            entAbsenteesDtl.setLeavecd(ddl_leave_period_snl.getLeavecd());
            entAbsenteesDtl.setLeaveBalLaps(BigDecimal.ZERO);
            entAbsenteesDtl.setCarryForwardFromOldBal(BigDecimal.ZERO);
            entAbsenteesDtl.setCreateby(globalData.getEnt_login_user());
            entAbsenteesDtl.setOumUnitSrno(selectedUser.getPostingOumUnitSrno());
            entAbsenteesDtl.setCreatedt(new Date());
            lst_FtasAbsenteesesDtl.add(entAbsenteesDtl);
            entFtasAbsentees.setFtasAbsenteesDtlCollection(lst_FtasAbsenteesesDtl);
            try {
                ftasAbsenteesFacade.create(entFtasAbsentees);
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Application saved Successfully."));
                try {
                    txt_user_id=null;
                    txt_user_id_changed();
                } catch (Exception ex) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Error reseting component", "Record saved successfully but there is some issue in reseting components so please refresh this page"));
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_apply_leave_snl_clicked", null, ex);
                }
            } catch (Exception ex) {
                context.addMessage(null, JSFMessages.getErrorMessage(ex));
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_apply_leave_snl_clicked", null, ex);
            }

        } catch (Exception ex) {
            context.addMessage(null, JSFMessages.getErrorMessage(ex));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_apply_leave_snl_clicked", null, ex);
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Opening,Closing dates & Min,Max Balance Settings">

    private Date maxOpeningDateSnl() {
        Date v_max_date = ddl_leave_period_snl.getOpeningDate();
        if (ddl_ttr_leave_period_snl_mand) {
            v_max_date = DateTimeUtility.findBiggestDate(v_max_date, ddl_ttr_leave_period_snl.getOpeningDate());
        }
        if (ddl_parent_leave_snl_mand) {
            v_max_date = DateTimeUtility.findBiggestDate(v_max_date, ddl_parent_leave_period_snl.getOpeningDate());
        }
        return v_max_date;
    }

    private Date minClosingDateSnl() {
        Date v_min_date = ddl_leave_period_snl.getClosingDate();
        if (ddl_ttr_leave_period_snl_mand) {
            v_min_date = DateTimeUtility.findSmallestDate(v_min_date, ddl_ttr_leave_period_snl.getClosingDate());
        }
        if (ddl_parent_leave_snl_mand) {
            v_min_date = DateTimeUtility.findSmallestDate(v_min_date, ddl_parent_leave_period_snl.getClosingDate());
        }
        return v_min_date;
    }

    private BigDecimal minTakenSnl() {
        BigDecimal v_min_taken = ddl_leave_period_snl.getElrSrgKey().getMinTaken();
        if (ddl_ttr_leave_period_snl_mand) {
            v_min_taken = (v_min_taken.compareTo(ddl_ttr_leave_period_snl.getElrSrgKey().getMinTaken()) == -1) ? ddl_ttr_leave_period_snl.getElrSrgKey().getMinTaken() : v_min_taken;
        }
        if (ddl_parent_leave_snl_mand) {
            BigDecimal v_parent_leave_min = ddl_parent_leave_period_snl.getElrSrgKey().getMinTaken().divide(leave_ratio_snl);
            v_min_taken = (v_min_taken.compareTo(v_parent_leave_min) == -1) ? v_parent_leave_min : v_min_taken;
        }
        return v_min_taken;
    }

    private BigDecimal maxTakenSnl() {
        BigDecimal v_max_taken = ddl_leave_period_snl.getElrSrgKey().getMaxTaken();
        if (ddl_ttr_leave_period_snl_mand) {
            if (v_max_taken != null) {
                v_max_taken = (v_max_taken.compareTo(ddl_ttr_leave_period_snl.getElrSrgKey().getMaxTaken()) == 1) ? ddl_ttr_leave_period_snl.getElrSrgKey().getMaxTaken() : v_max_taken;
            } else {
                v_max_taken = ddl_ttr_leave_period_snl.getElrSrgKey().getMaxTaken();
            }
        }
        if (ddl_parent_leave_snl_mand) {
            BigDecimal v_parent_leave_max = ddl_parent_leave_period_snl.getElrSrgKey().getMaxTaken() == null ? null : ddl_parent_leave_period_snl.getElrSrgKey().getMaxTaken().divide(leave_ratio_snl);
            if (v_max_taken != null) {
                v_max_taken = (v_max_taken.compareTo(v_parent_leave_max) == 1) ? v_parent_leave_max : v_max_taken;
            } else {
                v_max_taken = v_parent_leave_max;
            }
        }
        return v_max_taken;
    }

    private BigDecimal minBalanceSnl() {
        BigDecimal v_min_balance = null;
        if (ddl_leave_period_snl.getElrSrgKey().getBalCheckedInAppliFlag().equals("Y")
                || (ddl_ttr_leave_period_snl_mand && ddl_ttr_leave_period_snl.getElrSrgKey().getBalCheckedInAppliFlag().equals("Y"))
                || (ddl_parent_leave_snl_mand && ddl_parent_leave_period_snl.getElrSrgKey().getBalCheckedInAppliFlag().equals("Y"))) {
            if (ddl_parent_leave_snl_mand) {
                if (ddl_ttr_leave_period_snl_mand) {
                    if (ddl_ttr_leave_period_snl.getElrSrgKey().getBalCheckedInAppliFlag().equals("Y")) {
                        v_min_balance = ddl_ttr_leave_period_snl.getCurrentBal();
                    }
                    BigDecimal v_ratio_value = ddl_parent_leave_period_snl.getCurrentBal().divide(leave_ratio_snl);
                    if (v_min_balance == null) {
                        v_min_balance = v_ratio_value;
                    } else {
                        v_min_balance = v_min_balance.compareTo(v_ratio_value) == -1 ? v_min_balance : v_ratio_value;
                    }

                } else {
                    if (ddl_leave_period_snl.getElrSrgKey().getBalCheckedInAppliFlag().equals("Y")) {
                        v_min_balance = ddl_leave_period_snl.getCurrentBal();
                    }
                    BigDecimal v_ratio_value = ddl_parent_leave_period_snl.getCurrentBal().divide(leave_ratio_snl);
                    if (v_min_balance == null) {
                        v_min_balance = v_ratio_value;
                    } else {
                        v_min_balance = v_min_balance.compareTo(v_ratio_value) == -1 ? v_min_balance : v_ratio_value;
                    }
                }
            } else {
                v_min_balance = selectedEmpleavebalSnl.getCurrentBal();
            }
        }
        return v_min_balance;
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Club Application Settings">
    //<editor-fold defaultstate="collapsed" desc="lastLeavecd">
    boolean lastLeavecd;

    public boolean isLastLeavecd() {
        return lastLeavecd;
    }

    public void setLastLeavecd(boolean lastLeavecd) {
        this.lastLeavecd = lastLeavecd;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lastDtlEntry">
    FtastAbsenteesDtlEntry lastDtlEntry;

    public FtastAbsenteesDtlEntry getLastDtlEntry() {
        return lastDtlEntry;
    }

    public void setLastDtlEntry(FtastAbsenteesDtlEntry lastDtlEntry) {
        this.lastDtlEntry = lastDtlEntry;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="out_total_leaves_club">
    String out_total_leaves_club;

    public String getOut_total_leaves_club() {
        return out_total_leaves_club;
    }

    public void setOut_total_leaves_club(String out_total_leaves_club) {
        this.out_total_leaves_club = out_total_leaves_club;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="out_total_leaves_laps_club">
    String out_total_leaves_laps_club;

    public String getOut_total_leaves_laps_club() {
        return out_total_leaves_laps_club;
    }

    public void setOut_total_leaves_laps_club(String out_total_leaves_laps_club) {
        this.out_total_leaves_laps_club = out_total_leaves_laps_club;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="selectedEmpleavebalClub">
    FhrdEmpleavebal selectedEmpleavebalClub;

    public FhrdEmpleavebal getSelectedEmpleavebalClub() {
        return selectedEmpleavebalClub;
    }

    public void setSelectedEmpleavebalClub(FhrdEmpleavebal selectedEmpleavebalClub) {
        this.selectedEmpleavebalClub = selectedEmpleavebalClub;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_leave_period_club">
    FhrdEmpleavebal ddl_leave_period_club;
    List<FhrdEmpleavebal> ddl_leave_period_club_options;

    public FhrdEmpleavebal getDdl_leave_period_club() {
        return ddl_leave_period_club;
    }

    public void setDdl_leave_period_club(FhrdEmpleavebal ddl_leave_period_club) {
        this.ddl_leave_period_club = ddl_leave_period_club;
    }

    public List<FhrdEmpleavebal> getDdl_leave_period_club_options() {
        return ddl_leave_period_club_options;
    }

    public void setDdl_leave_period_club_options(List<FhrdEmpleavebal> ddl_leave_period_club_options) {
        this.ddl_leave_period_club_options = ddl_leave_period_club_options;
    }

    private void setDdl_leave_period_club() {
        out_total_leaves_club = "0";
        selectedEmpleavebalClub = null;
        ddl_leave_period_club = null;
        ddl_parent_leave_club_null_label = "--- Select Allotment Period ---";
        ddl_parent_leave_club_mand = false;
        if (userSelectionValid) {
            if (lst_AbsenteesDtlsClub == null) {
                ddl_leave_period_club_options = fhrdEmpleavebalFacade.findAllFromLeave(selectedUser.getPostingOumUnitSrno().getOumUnitSrno(), selectedUser.getUserId(), true, "N", true, "Y", null, null, null, "Y", null, false, null, null, null, null, null, userManagedFlag);
            } else {
                lastDtlEntry = lst_AbsenteesDtlsClub.get(lst_AbsenteesDtlsClub.size() - 1);
                boolean v_halfApplicable = lastDtlEntry.getFtasAbsenteesDtl().getTohalf().equals("1");
                Date v_clubStartDate = lastDtlEntry.getFtasAbsenteesDtl().getToDate();
                if (lastDtlEntry != null && lastDtlEntry.getFtasAbsenteesDtl().getLastLeavecd().equals("N")) {
                    ddl_leave_period_club_options = fhrdEmpleavebalFacade.findAllFromLeave(selectedUser.getPostingOumUnitSrno().getOumUnitSrno(), selectedUser.getUserId(), true, "N", true, "Y", null, null, null, "Y", (lastDtlEntry.getFtasAbsenteesDtl().getParentEmplbSrgKey() != null || lastDtlEntry.getFtasAbsenteesDtl().getTtrEmplbSrgKey() != null) ? null : lastDtlEntry.getFtasAbsenteesDtl().getEmplbSrgKey().getEmplbSrgKey().toString(), v_halfApplicable, v_clubStartDate, null, lastDtlEntry.getFtasAbsenteesDtl().getEmplbSrgKey().getLrSrgKey().getLrSrgKey(), null, null, userManagedFlag);
                } else {
                    String v_except_emplb = "";
                    for (FtastAbsenteesDtlEntry e : lst_AbsenteesDtlsClub) {
                        v_except_emplb += (e.getFtasAbsenteesDtl().getActualEmplbSrgKey().getEmplbSrgKey().toString()) + ",";
                    }
                    v_except_emplb = v_except_emplb.substring(0, v_except_emplb.length() - 1);
                    ddl_leave_period_club_options = fhrdEmpleavebalFacade.findAllFromLeave(selectedUser.getPostingOumUnitSrno().getOumUnitSrno(), selectedUser.getUserId(), true, "N", true, "Y", null, null, null, "Y", v_except_emplb, v_halfApplicable, v_clubStartDate, null, null, null, null, userManagedFlag);
                }
            }
        } else {
            ddl_leave_period_club_options = null;
        }
        setFooterSettings();
        setDdl_ttr_leave_period_club();
    }

    public void ddl_leave_period_club_changed(ValueChangeEvent event) {
        ddl_leave_period_club = event.getNewValue() == null ? null : fhrdEmpleavebalFacade.find(((FhrdEmpleavebal) event.getNewValue()).getEmplbSrgKey());
        setDdl_ttr_leave_period_club();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_ttr_leave_period_club">
    FhrdEmpleavebal ddl_ttr_leave_period_club;
    List<FhrdEmpleavebal> ddl_ttr_leave_period_club_options;
    String ddl_ttr_leave_period_club_null_label;
    boolean ddl_ttr_leave_period_club_mand = false;

    public FhrdEmpleavebal getDdl_ttr_leave_period_club() {
        return ddl_ttr_leave_period_club;
    }

    public void setDdl_ttr_leave_period_club(FhrdEmpleavebal ddl_ttr_leave_period_club) {
        this.ddl_ttr_leave_period_club = ddl_ttr_leave_period_club;
    }

    public List<FhrdEmpleavebal> getDdl_ttr_leave_period_club_options() {
        return ddl_ttr_leave_period_club_options;
    }

    public void setDdl_ttr_leave_period_club_options(List<FhrdEmpleavebal> ddl_ttr_leave_period_club_options) {
        this.ddl_ttr_leave_period_club_options = ddl_ttr_leave_period_club_options;
    }

    public String getDdl_ttr_leave_period_club_null_label() {
        return ddl_ttr_leave_period_club_null_label;
    }

    public void setDdl_ttr_leave_period_club_null_label(String ddl_ttr_leave_period_club_null_label) {
        this.ddl_ttr_leave_period_club_null_label = ddl_ttr_leave_period_club_null_label;
    }

    public boolean isDdl_ttr_leave_period_club_mand() {
        return ddl_ttr_leave_period_club_mand;
    }

    public void setDdl_ttr_leave_period_club_mand(boolean ddl_ttr_leave_period_club_mand) {
        this.ddl_ttr_leave_period_club_mand = ddl_ttr_leave_period_club_mand;
    }

    private void setDdl_ttr_leave_period_club() {
        selectedEmpleavebalClub = null;
        ddl_ttr_leave_period_club_mand = false;
        ddl_ttr_leave_period_club_options = null;
        ddl_ttr_leave_period_club = null;
        ddl_ttr_leave_period_club_null_label = "--- Not Available ---";
        if (ddl_leave_period_club != null) {
            String v_virtual_leave = ddl_leave_period_club.getElrSrgKey().getLmSrgKey().getVirtualLeave();
            if (v_virtual_leave.equals("T")) {
                ddl_ttr_leave_period_club_mand = true;
                if (lst_AbsenteesDtlsClub == null) {
                    ddl_ttr_leave_period_club_options =
                            fhrdEmpleavebalFacade.findAllFromLeave(selectedUser.getPostingOumUnitSrno().getOumUnitSrno(), selectedUser.getUserId(), true, "N", true, "Y", null, null, "Y", "Y", null, false, null, null, null, null, null, userManagedFlag);
                } else {

                    boolean v_halfApplicable = lastDtlEntry.getFtasAbsenteesDtl().getTohalf().equals("1");
                    Date v_clubStartDate = lastDtlEntry.getFtasAbsenteesDtl().getToDate();
                    if (lastDtlEntry != null && lastDtlEntry.getFtasAbsenteesDtl().getLastLeavecd().equals("N")) {

                        ddl_ttr_leave_period_club_options = fhrdEmpleavebalFacade.findAllFromLeave(selectedUser.getPostingOumUnitSrno().getOumUnitSrno(), selectedUser.getUserId(), true, "N", true, "Y", null, null, "Y", "Y",
                                lastDtlEntry.getFtasAbsenteesDtl().getParentEmplbSrgKey() == null ? lastDtlEntry.getFtasAbsenteesDtl().getActualEmplbSrgKey().getEmplbSrgKey().toString() : null,
                                v_halfApplicable, v_clubStartDate, null,
                                lastDtlEntry.getFtasAbsenteesDtl().getParentEmplbSrgKey() == null ? lastDtlEntry.getFtasAbsenteesDtl().getActualEmplbSrgKey().getLrSrgKey().getLrSrgKey() : lastDtlEntry.getFtasAbsenteesDtl().getChildEmplbSrgKey().getLrSrgKey().getLrSrgKey(), null, null, userManagedFlag);
                    } else {
                        String v_except_emplb = "";
                        for (FtastAbsenteesDtlEntry e : lst_AbsenteesDtlsClub) {
                            v_except_emplb += e.getFtasAbsenteesDtl().getActualEmplbSrgKey().getEmplbSrgKey().toString() + ",";
                        }
                        v_except_emplb = v_except_emplb.substring(0, v_except_emplb.length() - 1);
                        ddl_ttr_leave_period_club_options = fhrdEmpleavebalFacade.findAllFromLeave(selectedUser.getPostingOumUnitSrno().getOumUnitSrno(), selectedUser.getUserId(), true, "N", true, "Y", null, null, "Y", "Y", v_except_emplb, v_halfApplicable, v_clubStartDate, null, null, null, null, userManagedFlag);

                    }
                }
                //ddl_ttr_leave_period_club_options = fhrdEmpleavebalFacade.findAllFromLeave(selectedUser.getUserId(), true, null, true, "Y", null, null, "Y", "Y", null, false, null);

                if (!ddl_ttr_leave_period_club_options.isEmpty()) {
                    ddl_ttr_leave_period_club_null_label = "--- Select ---";
                }
            } else {
                ddl_ttr_leave_period_club_null_label = "--- N/A ---";
            }
        }
        if (!ddl_ttr_leave_period_club_mand) {
            selectedEmpleavebalClub = ddl_leave_period_club;
        }
        setDdl_parent_leave_period_club();
    }

    public void ddl_ttr_leave_period_club_changed(ValueChangeEvent event) {
        ddl_ttr_leave_period_club = event.getNewValue() == null ? null : fhrdEmpleavebalFacade.find(((FhrdEmpleavebal) event.getNewValue()).getEmplbSrgKey());
        if (ddl_ttr_leave_period_club_mand) {
            selectedEmpleavebalClub = ddl_ttr_leave_period_club;
        }
        setDdl_parent_leave_period_club();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_parent_leave_period_club">
    FhrdEmpleavebal ddl_parent_leave_period_club;
    List<FhrdEmpleavebal> ddl_parent_leave_period_club_options;
    String ddl_parent_leave_club_null_label;
    BigDecimal leave_ratio_club;
    boolean ddl_parent_leave_club_mand = false;

    public FhrdEmpleavebal getDdl_parent_leave_period_club() {
        return ddl_parent_leave_period_club;
    }

    public void setDdl_parent_leave_period_club(FhrdEmpleavebal ddl_parent_leave_period_club) {
        this.ddl_parent_leave_period_club = ddl_parent_leave_period_club;
    }

    public List<FhrdEmpleavebal> getDdl_parent_leave_period_club_options() {
        return ddl_parent_leave_period_club_options;
    }

    public void setDdl_parent_leave_period_club_options(List<FhrdEmpleavebal> ddl_parent_leave_period_club_options) {
        this.ddl_parent_leave_period_club_options = ddl_parent_leave_period_club_options;
    }

    public String getDdl_parent_leave_club_null_label() {
        return ddl_parent_leave_club_null_label;
    }

    public void setDdl_parent_leave_club_null_label(String ddl_parent_leave_club_null_label) {
        this.ddl_parent_leave_club_null_label = ddl_parent_leave_club_null_label;
    }

    public boolean isDdl_parent_leave_club_mand() {
        return ddl_parent_leave_club_mand;
    }

    public void setDdl_parent_leave_club_mand(boolean ddl_parent_leave_club_mand) {
        this.ddl_parent_leave_club_mand = ddl_parent_leave_club_mand;
    }

    public BigDecimal getLeave_ratio_club() {
        return leave_ratio_club;
    }

    public void setLeave_ratio_club(BigDecimal leave_ratio_club) {
        this.leave_ratio_club = leave_ratio_club;
    }

    private void setDdl_parent_leave_period_club() {
        leave_ratio_club = null;
        selectedEmpleavebalClub = null;
        ddl_parent_leave_club_mand = false;
        ddl_parent_leave_period_club_options = null;
        ddl_parent_leave_period_club = null;
        ddl_parent_leave_club_null_label = "--- Not Available ---";
        FhrdEmpleavebal ent_temp = ddl_ttr_leave_period_club_mand ? ddl_ttr_leave_period_club : ddl_leave_period_club;
        if (ent_temp != null) {
            String v_virtual_leave = ent_temp.getElrSrgKey().getLmSrgKey().getVirtualLeave();
            if (v_virtual_leave.equals("L")) {
                ddl_parent_leave_club_mand = true;


                if (lastDtlEntry != null && lastDtlEntry.getFtasAbsenteesDtl().getLastLeavecd().equals("N")) {
                    ddl_parent_leave_period_club_options = fhrdEmpleavebalFacade.findAllFromLeave(selectedUser.getPostingOumUnitSrno().getOumUnitSrno(), selectedUser.getUserId(), true, null, true, "Y", "N", "Y", null, "Y", lastDtlEntry.getFtasAbsenteesDtl().getActualEmplbSrgKey().getEmplbSrgKey().toString(), false, null, null, lastDtlEntry.getFtasAbsenteesDtl().getParentEmplbSrgKey() != null ? null : lastDtlEntry.getFtasAbsenteesDtl().getParentEmplbSrgKey().getLrSrgKey().getLrSrgKey(), null, null, userManagedFlag);
                } else if (lastDtlEntry != null && lastDtlEntry.getFtasAbsenteesDtl().getLastLeavecd().equals("Y")) {
                    ddl_parent_leave_period_club_options = fhrdEmpleavebalFacade.findAllFromLeave(selectedUser.getPostingOumUnitSrno().getOumUnitSrno(), selectedUser.getUserId(), true, null, true, "Y", "N", "Y", null, "Y", lastDtlEntry.getFtasAbsenteesDtl().getActualEmplbSrgKey().getEmplbSrgKey().toString(), false, null, null, null, null, null, userManagedFlag);
                } else {
                    ddl_parent_leave_period_club_options = fhrdEmpleavebalFacade.findAllFromLeave(selectedUser.getPostingOumUnitSrno().getOumUnitSrno(), selectedUser.getUserId(), true, null, true, "Y", "N", "Y", null, "Y", null, false, null, null, null, null, null, userManagedFlag);
                }
                if (!ddl_parent_leave_period_club_options.isEmpty()) {
                    ddl_parent_leave_club_null_label = "--- Select ---";
                }
            } else {
                ddl_parent_leave_club_null_label = "--- N/A ---";
            }
        }
        if (!ddl_parent_leave_club_mand) {
            selectedEmpleavebalClub = ent_temp;
        }
        setCal_from_date_club();
    }

    public void ddl_parent_leave_period_club_changed(ValueChangeEvent event) {
        ddl_parent_leave_period_club = event.getNewValue() == null ? null : fhrdEmpleavebalFacade.find(((FhrdEmpleavebal) event.getNewValue()).getEmplbSrgKey());
        if (ddl_parent_leave_club_mand) {
            selectedEmpleavebalClub = ddl_parent_leave_period_club;
            if (ddl_parent_leave_period_club != null) {
                if (ddl_ttr_leave_period_club_mand) {
                    leave_ratio_club = ftasAbsenteesFacade.getLeaveRatio(ddl_ttr_leave_period_club.getLrSrgKey().getLrSrgKey(), ddl_parent_leave_period_club.getLrSrgKey().getLrSrgKey());
                } else {
                    leave_ratio_club = ftasAbsenteesFacade.getLeaveRatio(ddl_leave_period_club.getLrSrgKey().getLrSrgKey(), ddl_parent_leave_period_club.getLrSrgKey().getLrSrgKey());
                }
            } else {
                leave_ratio_club = null;
            }
        }
        setCal_from_date_club();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_from_date_club">
    Date cal_from_date_club;
    String cal_from_date_club_min;
    String cal_from_date_club_max;

    public Date getCal_from_date_club() {
        return cal_from_date_club;
    }

    public void setCal_from_date_club(Date cal_from_date_club) {
        this.cal_from_date_club = cal_from_date_club;
    }

    public String getCal_from_date_club_max() {
        return cal_from_date_club_max;
    }

    public void setCal_from_date_club_max(String cal_from_date_club_max) {
        this.cal_from_date_club_max = cal_from_date_club_max;
    }

    public String getCal_from_date_club_min() {
        return cal_from_date_club_min;
    }

    public void setCal_from_date_club_min(String cal_from_date_club_min) {
        this.cal_from_date_club_min = cal_from_date_club_min;
    }

    public void cal_from_date_club_changed(SelectEvent event) {
        cal_from_date_club = (Date) event.getObject();
        setDdl_from_half_club();
    }

    private void setCal_from_date_club() {

        cal_from_date_club = null;
        if (selectedEmpleavebalClub != null) {
            Date v_max_opening_date = maxOpeningDateClub();
            Date v_min_closing_date = minClosingDateClub();
            if (lst_AbsenteesDtlsClub == null) {
                cal_from_date_club_min = DateTimeUtility.ChangeDateFormat(DateTimeUtility.findBiggestDate(v_user_last_locked_date, v_max_opening_date), null);
                cal_from_date_club_max = DateTimeUtility.ChangeDateFormat(v_min_closing_date, null);
            } else {
                if (lastDtlEntry.getFtasAbsenteesDtl().getTohalf().equals("1")) {
                    Date v_from_date = lastDtlEntry.getFtasAbsenteesDtl().getToDate();
                    cal_from_date_club_min = DateTimeUtility.ChangeDateFormat(DateTimeUtility.findBiggestDate(v_user_last_locked_date, v_from_date), null);
                    cal_from_date_club_max = DateTimeUtility.ChangeDateFormat(v_from_date, null);
                    cal_from_date_club = v_from_date;
                } else {
                    Date v_from_date = DateTimeUtility.findNextDateFromDays(lastDtlEntry.getFtasAbsenteesDtl().getToDate(), 1);
                    cal_from_date_club_min = DateTimeUtility.ChangeDateFormat(DateTimeUtility.findBiggestDate(v_user_last_locked_date, v_from_date), null);
                    cal_from_date_club_max = DateTimeUtility.ChangeDateFormat(v_from_date, null);
                    cal_from_date_club = v_from_date;
                }

            }

        }
        setDdl_from_half_club();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_from_half_club">
    String ddl_from_half_club;
    List<SelectItem> ddl_from_half_club_options;

    public List<SelectItem> getDdl_from_half_club_options() {
        return ddl_from_half_club_options;
    }

    public void setDdl_from_half_club_options(List<SelectItem> ddl_from_half_club_options) {
        this.ddl_from_half_club_options = ddl_from_half_club_options;
    }

    public String getDdl_from_half_club() {
        return ddl_from_half_club;
    }

    public void setDdl_from_half_club(String ddl_from_half_club) {
        this.ddl_from_half_club = ddl_from_half_club;
    }

    public void setDdl_from_half_club() {
        ddl_from_half_club = null;
        if (cal_from_date_club != null) {
            if (lst_AbsenteesDtlsClub != null) {
                if (lastDtlEntry.getFtasAbsenteesDtl().getTohalf().equals("1")) {
                    ddl_from_half_club_options = new ArrayList<SelectItem>();
                    ddl_from_half_club_options.add(new SelectItem("2", "Second Half"));
                    ddl_from_half_club = "2";
                } else {
                    ddl_from_half_club_options = new ArrayList<SelectItem>();
                    ddl_from_half_club_options.add(new SelectItem("0", "Full Day"));
                    ddl_from_half_club_options.add(new SelectItem("1", "First Half"));
                    ddl_from_half_club = "0";
                }
            } else {
                ddl_from_half_club_options = new ArrayList<SelectItem>();
                if (selectedEmpleavebalClub.getElrSrgKey().getHalfdayApplicable().equals("Y")) {
                    ddl_from_half_club_options.add(new SelectItem("1", "First Half"));
                    ddl_from_half_club_options.add(new SelectItem("2", "Second Half"));
                }
                ddl_from_half_club_options.add(0, new SelectItem("0", "Full Day"));
                ddl_from_half_club = "0";
            }
        } else {
            ddl_from_half_club_options = new ArrayList<SelectItem>();
        }
        setCal_to_date_club();
    }

    public void ddl_from_half_club_changed(ValueChangeEvent event) {
        ddl_from_half_club = event.getNewValue() == null ? null : event.getNewValue().toString();
        setCal_to_date_club();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_to_date_club">    
    Date cal_to_date_club;
    String cal_to_date_club_min;
    String cal_to_date_club_max;

    public Date getCal_to_date_club() {
        return cal_to_date_club;
    }

    public void setCal_to_date_club(Date cal_to_date_club) {
        this.cal_to_date_club = cal_to_date_club;
    }

    public String getCal_to_date_club_max() {
        return cal_to_date_club_max;
    }

    public void setCal_to_date_club_max(String cal_to_date_club_max) {
        this.cal_to_date_club_max = cal_to_date_club_max;
    }

    public String getCal_to_date_club_min() {
        return cal_to_date_club_min;
    }

    public void setCal_to_date_club_min(String cal_to_date_club_min) {
        this.cal_to_date_club_min = cal_to_date_club_min;
    }

    public void cal_to_date_club_changed(SelectEvent event) {
        cal_to_date_club = (Date) event.getObject();
        boolean holidaySelected = false;
        if (lst_holidays.contains(cal_to_date_club)) {
            holidaySelected = true;
        }
//        if (holidaySelected && selectedEmpleavebalClub.getElrSrgKey().getHolidayEmbedded().equals("N")) {
//            while (lst_holidays.contains(cal_to_date_club)) {
//                cal_to_date_club = DateTimeUtility.findPrevDateFromDays(cal_to_date_club, 1);
//            }
//            if (cal_to_date_club.after(DateTimeUtility.stringToDate(cal_to_date_club_max, null))) {
//                setMessageDetail(FacesMessage.SEVERITY_WARN, "Wrong to date", "Can not select your date as it might be exceeding your leave period before availble holiday");
//                cal_to_date_club = null;
//            } else {
//                setMessageDetail(FacesMessage.SEVERITY_WARN, "Holiday is selected", "Can not select date on holiday to apply to so selecting previous date to the holiday");
//            }
//        }
        setDdl_to_half_club();
    }

    private void setCal_to_date_club() {
        cal_to_date_club = null;
        if (ddl_from_half_club != null) {
            if (ddl_from_half_club.equals("1")) {
                cal_to_date_club_min = DateTimeUtility.ChangeDateFormat(cal_from_date_club, null);
                cal_to_date_club_max = DateTimeUtility.ChangeDateFormat(cal_from_date_club, null);
            } else {
                BigDecimal bd_min_taken = minTakenClub();
                if (lastDtlEntry != null && lastDtlEntry.getFtasAbsenteesDtl().getLastLeavecd().equals("N")) {
                    bd_min_taken = bd_min_taken.subtract(lastDtlEntry.getFtasAbsenteesDtl().getDays());
                } else if (bd_min_taken.compareTo(BigDecimal.ONE) == 1 && lastDtlEntry != null && lastDtlEntry.getFtasAbsenteesDtl().getActualEmplbSrgKey().getLrSrgKey().equals(selectedEmpleavebalClub.getLrSrgKey())) {
                    bd_min_taken = BigDecimal.ONE;
                }
                double dbl_min_taken = Double.valueOf(bd_min_taken.toString());
                double v_min_taken = Integer.valueOf(String.valueOf(Math.round(Math.floor(dbl_min_taken))));
                Date v_max_date = null;
                Date v_min_date;
                if (ddl_from_half_club.equals("2")) {
                    if (dbl_min_taken >= v_min_taken) {
                        v_min_taken = v_min_taken + 1;
                    }
                } else {
                    if (dbl_min_taken > v_min_taken) {
                        v_min_taken = v_min_taken + 1;
                    }
                }
                v_min_date = DateTimeUtility.findNextDateFromDays(cal_from_date_club, (int) (v_min_taken - 1));
                if (v_min_date.after(selectedEmpleavebalClub.getClosingDate())) {
                    v_min_date = selectedEmpleavebalClub.getClosingDate();
                    if (v_min_date.before(cal_from_date_club)) {
                        v_min_date = cal_from_date_club;
                    }
                    lastLeavecd = false;
                } else {
                    lastLeavecd = true;
                }

                BigDecimal v_min_balance = minBalanceClub();
                if (v_min_balance != null) {
                    double dbl_balance = Double.valueOf(v_min_balance.toString());
                    double v_balance = Integer.valueOf(String.valueOf(Math.round(Math.floor(dbl_balance))));
                    if ((dbl_balance > v_balance && selectedEmpleavebalClub.getElrSrgKey().getHalfdayApplicable().equals("Y")) || ddl_from_half_club.equals("2")) {
                        v_balance = v_balance + 1;
                    }
                    v_max_date = DateTimeUtility.findNextDateFromDays(cal_from_date_club, (int) (v_balance - 1));

                }
                BigDecimal v_max_taken = maxTakenClub();
                if (v_max_taken != null) {
                    Date v_temp_date;
                    if (ddl_from_half_club.equals("2")) {
                        v_temp_date = DateTimeUtility.findNextDateFromDays(cal_from_date_club, v_max_taken.intValue());
                    } else {
                        v_temp_date = DateTimeUtility.findNextDateFromDays(cal_from_date_club, v_max_taken.intValue() - 1);
                    }
                    v_max_date = DateTimeUtility.findSmallestDate(v_max_date, v_temp_date);
                }
                //Embedded Leave                
//                if (selectedEmpleavebalClub.getElrSrgKey().getHolidayEmbedded().equals("N")) {
//                    Date v_temp_date = cal_from_date_club;
//                    while (!v_temp_date.after(v_max_date)) {
//                        if (lst_holidays.contains(v_temp_date)) {
//                            v_max_date = DateTimeUtility.findNextDateFromDays(v_max_date, 1);
//                        }
//                        v_temp_date = DateTimeUtility.findNextDateFromDays(v_temp_date, 1);
//                    }
//                }

                v_max_date = DateTimeUtility.findSmallestDate(v_max_date, minClosingDateClub());
                cal_to_date_club_min = DateTimeUtility.ChangeDateFormat(v_min_date, null);
                cal_to_date_club_max = DateTimeUtility.ChangeDateFormat(v_max_date, null);
            }
        }
        setDdl_to_half_club();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_to_half_club">
    String ddl_to_half_club;
    List<SelectItem> ddl_to_half_club_options;

    public List<SelectItem> getDdl_to_half_club_options() {
        return ddl_to_half_club_options;
    }

    public void setDdl_to_half_club_options(List<SelectItem> ddl_to_half_club_options) {
        this.ddl_to_half_club_options = ddl_to_half_club_options;
    }

    public String getDdl_to_half_club() {
        return ddl_to_half_club;
    }

    public void setDdl_to_half_club(String ddl_to_half_club) {
        this.ddl_to_half_club = ddl_to_half_club;
    }

    public void setDdl_to_half_club() {
        ddl_to_half_club = null;
        ddl_to_half_club_options = new ArrayList<SelectItem>();
        if (cal_to_date_club != null) {
            if (selectedEmpleavebalClub.getElrSrgKey().getHalfdayApplicable().equals("N")) {
                ddl_to_half_club_options = new ArrayList<SelectItem>();
                ddl_to_half_club_options.add(new SelectItem("0", "Full Day"));
                ddl_to_half_club = "0";
            } else if (cal_from_date_club.equals(cal_to_date_club) && ddl_from_half_club.equals("0")) {
                ddl_to_half_club_options = new ArrayList<SelectItem>();
                ddl_to_half_club_options.add(new SelectItem("0", "Full Day"));
                ddl_to_half_club = "0";
            } else if (cal_from_date_club.equals(cal_to_date_club) && ddl_from_half_club.equals("1")) {
                ddl_to_half_club_options = new ArrayList<SelectItem>();
                ddl_to_half_club_options.add(new SelectItem("1", "First Half"));
                ddl_to_half_club = "1";
            } else if (cal_from_date_club.equals(cal_to_date_club) && ddl_from_half_club.equals("2")) {
                ddl_to_half_club_options = new ArrayList<SelectItem>();
                ddl_to_half_club_options.add(new SelectItem("2", "Second Half"));
                ddl_to_half_club = "2";
            } else if (!cal_from_date_club.equals(cal_to_date_club)) {
                if (DateTimeUtility.stringToDate(cal_to_date_club_max, null).equals(cal_to_date_club)) {
                    double dbl_balance = Double.valueOf(minBalanceClub().toString());
                    double v_balance = Integer.valueOf(String.valueOf(Math.round(Math.floor(dbl_balance))));
                    boolean reCalculateForLeaveEnd = false;
                    if (ddl_from_half_club.equals("2") && dbl_balance == v_balance) {
                        ddl_to_half_club_options = new ArrayList<SelectItem>();
                        ddl_to_half_club_options.add(new SelectItem("1", "First Half"));
                        ddl_to_half_club = "1";
                        reCalculateForLeaveEnd = true;
                    } else if (!ddl_from_half_club.equals("2") && dbl_balance > v_balance) {
                        ddl_to_half_club_options = new ArrayList<SelectItem>();
                        ddl_to_half_club_options.add(new SelectItem("1", "First Half "));
                        ddl_to_half_club = "1";
                        reCalculateForLeaveEnd = true;
                    } else {
                        ddl_to_half_club_options = new ArrayList<SelectItem>();
                        ddl_to_half_club_options.add(new SelectItem("0", "Full Day"));
                        ddl_to_half_club_options.add(new SelectItem("1", "First Half"));
                        ddl_to_half_club = "0";
                    }
                    if (reCalculateForLeaveEnd) {
                        calculateTotalLeaveClub();
                        if (Double.valueOf(out_total_leaves_club).compareTo(dbl_balance) == -1) {
                            ddl_to_half_club_options = new ArrayList<SelectItem>();
                            ddl_to_half_club_options.add(new SelectItem("0", "Full Day"));
                            ddl_to_half_club_options.add(new SelectItem("1", "First Half"));
                            ddl_to_half_club = "0";
                        }
                    }
                } else {
                    ddl_to_half_club_options = new ArrayList<SelectItem>();
                    ddl_to_half_club_options.add(new SelectItem("0", "Full Day"));
                    ddl_to_half_club_options.add(new SelectItem("1", "First Half"));
                    ddl_to_half_club = "0";
                }
            }

        }
        calculateTotalLeaveClub();
    }

    public void ddl_to_half_club_changed(ValueChangeEvent event) {
        ddl_to_half_club = event.getNewValue() == null ? null : event.getNewValue().toString();
        calculateTotalLeaveClub();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="calculateTotalLeaveClub">

    private void calculateTotalLeaveClub() {
        if (ddl_to_half_club != null) {
            double totalDays = Double.valueOf(String.valueOf(DateTimeUtility.findDuration(cal_from_date_club, cal_to_date_club, "D"))) + 1;
            if (ddl_to_half_club.equals("2")) {
                totalDays = totalDays - 0.5;
            }
            if (ddl_to_half_club.equals("1")) {
                totalDays = totalDays - 0.5;
            }
            out_total_leaves_club = String.valueOf(totalDays);
            if (selectedEmpleavebalClub.getElrSrgKey().getLeaveLapsInClubFlag().equals("Y")) {
                out_total_leaves_laps_club = selectedEmpleavebalClub.getCurrentBal().subtract(new BigDecimal(out_total_leaves_club)).toString();
            }
        } else {
            out_total_leaves_club = "0";
            out_total_leaves_laps_club = "0";
        }
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="btn_add_to_list_leave_club_clicked">

    public void btn_add_to_list_leave_club_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            Database_Output database_Output = ftas_Pack.VALIDATE_ABSENTEES_DTL(this.getClass().getName(),
                    "btn_apply_leave_club_clicked",
                    ddl_leave_period_club.getEmplbSrgKey(),
                    selectedEmpleavebalClub.getEmplbSrgKey(),
                    ddl_parent_leave_club_mand ? (ddl_ttr_leave_period_club_mand ? ddl_ttr_leave_period_club.getEmplbSrgKey() : ddl_leave_period_club.getEmplbSrgKey()) : null,
                    ddl_parent_leave_club_mand ? ddl_parent_leave_period_club.getEmplbSrgKey() : null,
                    ddl_ttr_leave_period_club_mand ? ddl_leave_period_club.getEmplbSrgKey() : null,
                    "L",
                    "C",
                    cal_from_date_club,
                    cal_to_date_club,
                    ddl_from_half_club,
                    ddl_to_half_club,
                    new BigDecimal(out_total_leaves_club),
                    lastDtlEntry != null && lastDtlEntry.getFtasAbsenteesDtl().getActualEmplbSrgKey().getLrSrgKey().equals(selectedEmpleavebalClub.getLrSrgKey()) ? lastDtlEntry.getFtasAbsenteesDtl().getTotalDays().add(new BigDecimal(out_total_leaves_club)) : new BigDecimal(out_total_leaves_club),
                    BigDecimal.ZERO,
                    lastLeavecd ? "Y" : "N",
                    globalData.getUser_id(),
                    null,
                    "Y",
                    "N");
            if (database_Output.isExecuted_successfully()) {
                if (database_Output.getString1().equals("Y") == false) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "Reason : " + database_Output.getString1()));
                    return;
                }
            } else {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error in checking previously added Applications."));
                return;
            }
            database_Output = ftas_Pack.Check_Leave_TTR_ContOverlap(this.getClass().getName(),
                    "btn_add_to_list_leave_club_clicked", "L", rdb_leave_type,
                    null,
                    selectedUser.getUserId(), cal_from_date_club, cal_to_date_club,
                    ddl_from_half_club, ddl_to_half_club);
            if (database_Output.isExecuted_successfully()) {
                if (database_Output.getString1().equals("Y") == false) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation failed", "Reason : " + database_Output.getString1()));
                    return;
                }
            } else {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error in checking previously added Applications."));
                return;
            }
            if (ddl_ttr_leave_period_club_mand) {
                BigDecimal v_count_for_whileOnTourTraining = ftasAbsenteesFacade.checkForWhileOnTourTraining(selectedUser.getUserId(), cal_from_date_club, cal_to_date_club);
                if (v_count_for_whileOnTourTraining.equals(BigDecimal.ZERO)) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "Tour/Training must be applied and approved before applying the " + ddl_leave_period_club.getLmSrgKey().getLeaveDesc() + ", for the period From : " + DateTimeUtility.ChangeDateFormat(cal_from_date_club, null) + " To : " + DateTimeUtility.ChangeDateFormat(cal_to_date_club, null) + ""));
                    return;
                }
            }

            selectedEmpleavebalClub = fhrdEmpleavebalFacade.find(selectedEmpleavebalClub.getEmplbSrgKey());
            calculateTotalLeaveClub();
            boolean leaveLaps = false;
            BigDecimal v_leave_laps = null;
            if (selectedEmpleavebalClub.getElrSrgKey().getLeaveLapsInClubFlag().equals("Y")) {
                v_leave_laps = selectedEmpleavebalClub.getCurrentBal().subtract(new BigDecimal(out_total_leaves_club));
                if (v_leave_laps.compareTo(BigDecimal.ZERO) != 0) {
                    leaveLaps = true;
                }
            }
            if (leaveLaps) {
                dlg_confirm_header = "Information";
                dlg_confirm_detail = "Total " + v_leave_laps + " '" + selectedEmpleavebalClub.getElrSrgKey().getLmSrgKey().getLeaveDesc() + "' will be exhausted, Do you want to continue?";
                RequestContext.getCurrentInstance().execute("pw_dlg_confirm_close.show()");
            } else {
                AddLeaveToList();
            }
        } catch (Exception e) {
            context.addMessage(null, JSFMessages.getErrorMessage(e));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_add_to_list_leave_club_clicked", null, e);
        }
    }

    public void btn_leave_laps_clicked(ActionEvent event) {
        AddLeaveToList();
    }

    private void AddLeaveToList() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if (lst_AbsenteesDtlsClub == null) {
                lst_AbsenteesDtlsClub = new ArrayList<FtastAbsenteesDtlEntry>();
            }
            FtastAbsenteesDtlEntry ent_DtlEntry = new FtastAbsenteesDtlEntry();
            FtasAbsenteesDtl entAbsenteesDtl = new FtasAbsenteesDtl();

            entAbsenteesDtl.setAbsdSrgKey(new BigDecimal(String.valueOf(lst_AbsenteesDtlsClub.size())));
            entAbsenteesDtl.setAbsSrgKey(new FtasAbsentees(new BigDecimal("-1")));
            entAbsenteesDtl.setUserId(selectedUser);
            entAbsenteesDtl.setDays(new BigDecimal(out_total_leaves_club));


            entAbsenteesDtl.setFromDate(cal_from_date_club);
            entAbsenteesDtl.setToDate(cal_to_date_club);
            entAbsenteesDtl.setFromhalf(ddl_from_half_club);
            entAbsenteesDtl.setTohalf(ddl_to_half_club);
            entAbsenteesDtl.setLastLeavecd(lastLeavecd ? "Y" : "N");

            entAbsenteesDtl.setActualEmplbSrgKey(selectedEmpleavebalClub);
            entAbsenteesDtl.setActualElrSrgKey(entAbsenteesDtl.getActualEmplbSrgKey().getElrSrgKey());
            entAbsenteesDtl.setActualLmSrgKey(entAbsenteesDtl.getActualEmplbSrgKey().getLmSrgKey());
            entAbsenteesDtl.setEmplbSrgKey(ddl_leave_period_club);
            entAbsenteesDtl.setElrSrgKey(entAbsenteesDtl.getEmplbSrgKey().getElrSrgKey());
            entAbsenteesDtl.setLmSrgKey(entAbsenteesDtl.getEmplbSrgKey().getLmSrgKey());
            entAbsenteesDtl.setLeavecd(entAbsenteesDtl.getEmplbSrgKey().getLeavecd());
            if (!lst_AbsenteesDtlsClub.isEmpty()
                    && lst_AbsenteesDtlsClub.get(lst_AbsenteesDtlsClub.size() - 1).getFtasAbsenteesDtl().getActualEmplbSrgKey().getLrSrgKey().equals(entAbsenteesDtl.getActualEmplbSrgKey().getLrSrgKey())) {
                entAbsenteesDtl.setTotalDays(lst_AbsenteesDtlsClub.get(lst_AbsenteesDtlsClub.size() - 1).getFtasAbsenteesDtl().getTotalDays().add(entAbsenteesDtl.getDays()));
            } else {
                entAbsenteesDtl.setTotalDays(entAbsenteesDtl.getDays());
            }
            if (ddl_parent_leave_club_mand) {
                entAbsenteesDtl.setParentEmplbSrgKey(ddl_parent_leave_period_club);
                if (ddl_ttr_leave_period_club_mand) {
                    entAbsenteesDtl.setChildEmplbSrgKey(ddl_ttr_leave_period_club);
                } else {
                    entAbsenteesDtl.setChildEmplbSrgKey(ddl_leave_period_club);
                }

            }
            if (ddl_ttr_leave_period_club_mand) {
                entAbsenteesDtl.setTtrEmplbSrgKey(ddl_leave_period_club);
            }
            if (selectedEmpleavebalClub.getElrSrgKey().getLeaveLapsInClubFlag().equals("Y")) {
                BigDecimal v_leave_laps = selectedEmpleavebalClub.getCurrentBal().subtract(entAbsenteesDtl.getDays());
                entAbsenteesDtl.setLeaveBalLaps(v_leave_laps);
            } else {
                entAbsenteesDtl.setLeaveBalLaps(BigDecimal.ZERO);
            }
            entAbsenteesDtl.setCarryForwardFromOldBal(BigDecimal.ZERO);
            entAbsenteesDtl.setCreateby(globalData.getEnt_login_user());
            entAbsenteesDtl.setOumUnitSrno(selectedUser.getPostingOumUnitSrno());
            entAbsenteesDtl.setCreatedt(new Date());
            if (ddl_parent_leave_club_mand) {
                ent_DtlEntry.setActualLeavesUsed(leave_ratio_club.multiply(entAbsenteesDtl.getDays()));
            } else {
                ent_DtlEntry.setActualLeavesUsed(entAbsenteesDtl.getDays());
            }
            ent_DtlEntry.setFtasAbsenteesDtl(entAbsenteesDtl);
            ent_DtlEntry.setLastRow(true);
            if (validateLeave(ent_DtlEntry)) {
                if (lst_AbsenteesDtlsClub != null) {
                    if (lst_AbsenteesDtlsClub.size() > 0) {
                        lst_AbsenteesDtlsClub.get(lst_AbsenteesDtlsClub.size() - 1).setLastRow(false);
                    }
                }
                lst_AbsenteesDtlsClub.add(ent_DtlEntry);
                RequestContext.getCurrentInstance().execute("pw_dlg_confirm_close.hide();pw_dlg_club_application.hide()");
                setDdl_leave_period_club();
            }
        } catch (Exception ex) {
            context.addMessage(null, JSFMessages.getErrorMessage(ex));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_apply_leave_club_clicked", null, ex);
        }
    }

    private boolean validateLeave(FtastAbsenteesDtlEntry ftastAbsenteesDtlEntry) {
        FacesContext context = FacesContext.getCurrentInstance();
        BigDecimal totalLeavesOccupied = BigDecimal.ZERO;
        BigDecimal totalLeavesAvailable = selectedEmpleavebalClub.getCurrentBal();
        boolean needComparision = false;
        if (lst_AbsenteesDtlsClub != null) {
            for (FtastAbsenteesDtlEntry d : lst_AbsenteesDtlsClub) {
                if (d.getFtasAbsenteesDtl().getEmplbSrgKey().equals(selectedEmpleavebalClub)) {
                    needComparision = true;
                    totalLeavesOccupied = totalLeavesOccupied.add(d.getActualLeavesUsed());
                }
            }
        }
        boolean isValid = needComparision ? (totalLeavesAvailable.subtract(totalLeavesOccupied).compareTo(ftastAbsenteesDtlEntry.getActualLeavesUsed()) != -1) : true;
        if (!isValid) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "Invalid leave input for \"" + selectedEmpleavebalClub.getLeavecd() + " - " + selectedEmpleavebalClub.getElrSrgKey().getLmSrgKey().getLeaveDesc() + "\" as total " + totalLeavesOccupied.setScale(0) + " leaves are already used from available " + selectedEmpleavebalClub.getCurrentBal() + " leaves"));
        }
        return isValid;
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lst_AbsenteesDtlsClub">
    List<FtastAbsenteesDtlEntry> lst_AbsenteesDtlsClub;

    public List<FtastAbsenteesDtlEntry> getLst_AbsenteesDtlsClub() {
        return lst_AbsenteesDtlsClub;
    }

    public void setLst_AbsenteesDtlsClub(List<FtastAbsenteesDtlEntry> lst_AbsenteesDtlsClub) {
        this.lst_AbsenteesDtlsClub = lst_AbsenteesDtlsClub;
    }

    private void setLst_AbsenteesDtlsClub() {
        lst_AbsenteesDtlsClub = null;
        lastLeavecd = true;
        lastDtlEntry = null;
        setDdl_leave_period_club();

    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="reomveLeaveFromClub">
    public void reomveLeaveFromClub(FtastAbsenteesDtlEntry removedLeave) {
        lst_AbsenteesDtlsClub.remove(removedLeave);
        if (lst_AbsenteesDtlsClub.isEmpty()) {
            setLst_AbsenteesDtlsClub();
        } else {
            int n = lst_AbsenteesDtlsClub.size();
            lst_AbsenteesDtlsClub.get(n - 1).setLastRow(true);
            lastDtlEntry = lst_AbsenteesDtlsClub.get(n - 1);
            setDdl_leave_period_club();
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Footer Settings">
    String foot_from;
    String foot_to;
    String foot_days;
    String foot_laps;

    public String getFoot_days() {
        return foot_days;
    }

    public void setFoot_days(String foot_days) {
        this.foot_days = foot_days;
    }

    public String getFoot_from() {
        return foot_from;
    }

    public void setFoot_from(String foot_from) {
        this.foot_from = foot_from;
    }

    public String getFoot_laps() {
        return foot_laps;
    }

    public void setFoot_laps(String foot_laps) {
        this.foot_laps = foot_laps;
    }

    public String getFoot_to() {
        return foot_to;
    }

    public void setFoot_to(String foot_to) {
        this.foot_to = foot_to;
    }

    private void setFooterSettings() {
        if (lst_AbsenteesDtlsClub != null) {
            foot_from = DateTimeUtility.ChangeDateFormat(lst_AbsenteesDtlsClub.get(0).getFtasAbsenteesDtl().getFromDate(), null) + " (" + (lst_AbsenteesDtlsClub.get(0).getFtasAbsenteesDtl().getFromhalf().equals("0") ? "Full Day" : (lst_AbsenteesDtlsClub.get(0).getFtasAbsenteesDtl().getFromhalf().equals("1") ? "First Half" : "Second Half")) + ")";
            foot_to = DateTimeUtility.ChangeDateFormat(lst_AbsenteesDtlsClub.get(lst_AbsenteesDtlsClub.size() - 1).getFtasAbsenteesDtl().getToDate(), null) + " (" + (lst_AbsenteesDtlsClub.get(lst_AbsenteesDtlsClub.size() - 1).getFtasAbsenteesDtl().getTohalf().equals("0") ? "Full Day" : (lst_AbsenteesDtlsClub.get(lst_AbsenteesDtlsClub.size() - 1).getFtasAbsenteesDtl().getTohalf().equals("1") ? "First Half" : "Second Half")) + ")";
            BigDecimal v_total_days = BigDecimal.ZERO;
            BigDecimal v_total_laps = BigDecimal.ZERO;
            for (FtastAbsenteesDtlEntry a : lst_AbsenteesDtlsClub) {
                v_total_days = v_total_days.add(a.getFtasAbsenteesDtl().getDays());
                v_total_laps = v_total_laps.add(a.getFtasAbsenteesDtl().getLeaveBalLaps());
            }
            foot_days = v_total_days.toString();
            foot_laps = v_total_laps.toString();
        } else {
            foot_from = null;
            foot_to = null;
            foot_days = null;
            foot_laps = null;
        }
    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_apply_leave_club_clicked">

    public void btn_apply_leave_club_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if (lst_AbsenteesDtlsClub != null && lst_AbsenteesDtlsClub.size() > 1) {
                FtasAbsentees entFtasAbsentees = new FtasAbsentees();
                Integer v_cal_year = (lst_AbsenteesDtlsClub.get(0).getFtasAbsenteesDtl().getFromDate().getYear() + 1900);
                BigDecimal v_abs_srgkey = getGlobalSettings().getUniqueSrno(v_cal_year, "FTAS_ABSENTEES");
                Integer v_apprv_auth_no = Integer.valueOf(ddl_actionby);
                Integer v_apprv_auth_oum = fhrdEmpmstFacade.find(v_apprv_auth_no).getPostingOumUnitSrno().getOumUnitSrno();

                entFtasAbsentees.setAbsSrgKey(v_abs_srgkey);
                entFtasAbsentees.setTranYear(Short.valueOf(v_cal_year.toString()));
                entFtasAbsentees.setUserId(selectedUser);
                entFtasAbsentees.setSrno(-1);
                entFtasAbsentees.setEmpno(selectedUser.getEmpno());

                entFtasAbsentees.setActnby(new FhrdEmpmst(v_apprv_auth_no));
                entFtasAbsentees.setAprvAuthOum(new SystOrgUnitMst(v_apprv_auth_oum));
                entFtasAbsentees.setActnpurp("A");
                entFtasAbsentees.setApplicationFlag("L");
                entFtasAbsentees.setAppliType("C");
                entFtasAbsentees.setReason1(txt_reason.toUpperCase().trim());
                entFtasAbsentees.setCreateby(globalData.getEnt_login_user());
                entFtasAbsentees.setOumUnitSrno(selectedUser.getPostingOumUnitSrno());
                entFtasAbsentees.setCreatedt(new Date());
                BigDecimal v_total_days = BigDecimal.ZERO;
                BigDecimal v_total_laps = BigDecimal.ZERO;
                List<FtasAbsenteesDtl> lst_FtasAbsenteesesDtl = new ArrayList<FtasAbsenteesDtl>();
                for (FtastAbsenteesDtlEntry a : lst_AbsenteesDtlsClub) {
                    FtasAbsenteesDtl ent_dtl = a.getFtasAbsenteesDtl();
                    FtasAbsenteesDtl entAbsenteesDtl = new FtasAbsenteesDtl();
                    BigDecimal v_absd_srgkey = getGlobalSettings().getUniqueSrno(v_cal_year, "FTAS_ABSENTEES_DTL");
                    entAbsenteesDtl.setAbsdSrgKey(v_absd_srgkey);
                    entAbsenteesDtl.setTranYear(Short.valueOf(v_cal_year.toString()));
                    entAbsenteesDtl.setAbsSrgKey(entFtasAbsentees);
                    entAbsenteesDtl.setUserId(selectedUser);
                    entAbsenteesDtl.setDays(ent_dtl.getDays());
                    entAbsenteesDtl.setTotalDays(ent_dtl.getTotalDays());
                    entAbsenteesDtl.setFromDate(ent_dtl.getFromDate());
                    entAbsenteesDtl.setToDate(ent_dtl.getToDate());
                    entAbsenteesDtl.setFromhalf(ent_dtl.getFromhalf());
                    entAbsenteesDtl.setTohalf(ent_dtl.getTohalf());
                    entAbsenteesDtl.setParentEmplbSrgKey(ent_dtl.getParentEmplbSrgKey());
                    entAbsenteesDtl.setChildEmplbSrgKey(ent_dtl.getChildEmplbSrgKey());
                    entAbsenteesDtl.setTtrEmplbSrgKey(ent_dtl.getTtrEmplbSrgKey());
                    entAbsenteesDtl.setLastLeavecd(ent_dtl.getLastLeavecd());
                    entAbsenteesDtl.setActualEmplbSrgKey(ent_dtl.getActualEmplbSrgKey());
                    entAbsenteesDtl.setActualElrSrgKey(ent_dtl.getElrSrgKey());
                    entAbsenteesDtl.setActualLmSrgKey(ent_dtl.getLmSrgKey());
                    entAbsenteesDtl.setEmplbSrgKey(ent_dtl.getEmplbSrgKey());
                    entAbsenteesDtl.setElrSrgKey(ent_dtl.getElrSrgKey());
                    entAbsenteesDtl.setLmSrgKey(ent_dtl.getLmSrgKey());
                    entAbsenteesDtl.setLeavecd(ent_dtl.getLeavecd());

                    entAbsenteesDtl.setLeaveBalLaps(ent_dtl.getLeaveBalLaps());
                    entAbsenteesDtl.setCarryForwardFromOldBal(BigDecimal.ZERO);
                    entAbsenteesDtl.setCreateby(globalData.getEnt_login_user());
                    entAbsenteesDtl.setOumUnitSrno(selectedUser.getPostingOumUnitSrno());
                    entAbsenteesDtl.setCreatedt(new Date());
                    v_total_days = v_total_days.add(ent_dtl.getDays());
                    v_total_laps = v_total_laps.add(ent_dtl.getLeaveBalLaps());
                    lst_FtasAbsenteesesDtl.add(entAbsenteesDtl);
                }
                entFtasAbsentees.setFromDate(lst_AbsenteesDtlsClub.get(0).getFtasAbsenteesDtl().getFromDate());
                entFtasAbsentees.setFromhalf(lst_AbsenteesDtlsClub.get(0).getFtasAbsenteesDtl().getFromhalf());
                entFtasAbsentees.setToDate(lst_AbsenteesDtlsClub.get(lst_AbsenteesDtlsClub.size() - 1).getFtasAbsenteesDtl().getToDate());
                entFtasAbsentees.setTohalf(lst_AbsenteesDtlsClub.get(lst_AbsenteesDtlsClub.size() - 1).getFtasAbsenteesDtl().getTohalf());
                entFtasAbsentees.setDays(v_total_days);
                entFtasAbsentees.setLeaveBalLaps(BigDecimal.ZERO);
                try {
                    Database_Output database_Output = customJpaController.createClubLeaveApplication(entFtasAbsentees, lst_FtasAbsenteesesDtl);
                    if (database_Output.isExecuted_successfully()) {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Application saved Successfully."));
                        try {
                            txt_user_id=null;
                            txt_user_id_changed();
                        } catch (Exception ex) {
                            context.addMessage(null, JSFMessages.getErrorMessage(ex));
                            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_apply_leave_club_clicked", null, ex);
                        }
                    } else {
                        context.addMessage(null, database_Output.getFacesMessage());
                    }

                } catch (Exception ex) {
                    context.addMessage(null, JSFMessages.getErrorMessage(ex));
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_apply_leave_club_clicked", null, ex);
                }
            } else {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "Please select atleast two leaves to apply for Club Leave Application"));
            }
        } catch (Exception e) {
            context.addMessage(null, JSFMessages.getErrorMessage(e));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_apply_leave_club_clicked", null, e);
        }
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="Opening,Closing dates & Min,Max Balance Settings">

    private Date maxOpeningDateClub() {
        Date v_max_date = ddl_leave_period_club.getOpeningDate();
        if (ddl_ttr_leave_period_club_mand) {
            v_max_date = DateTimeUtility.findBiggestDate(v_max_date, ddl_ttr_leave_period_club.getOpeningDate());
        }
        if (ddl_parent_leave_club_mand) {
            v_max_date = DateTimeUtility.findBiggestDate(v_max_date, ddl_parent_leave_period_club.getOpeningDate());
        }

        return v_max_date;
    }

    private Date minClosingDateClub() {
        Date v_min_date = ddl_leave_period_club.getClosingDate();
        if (ddl_ttr_leave_period_club_mand) {
            v_min_date = DateTimeUtility.findSmallestDate(v_min_date, ddl_ttr_leave_period_club.getClosingDate());
        }
        if (ddl_parent_leave_club_mand) {
            v_min_date = DateTimeUtility.findSmallestDate(v_min_date, ddl_parent_leave_period_club.getClosingDate());
        }

        return v_min_date;
    }

    private BigDecimal minTakenClub() {
        BigDecimal v_min_taken = ddl_leave_period_club.getElrSrgKey().getMinTaken();
        if (ddl_ttr_leave_period_club_mand) {
            v_min_taken = (v_min_taken.compareTo(ddl_ttr_leave_period_club.getElrSrgKey().getMinTaken()) == -1) ? ddl_ttr_leave_period_club.getElrSrgKey().getMinTaken() : v_min_taken;
        }
        if (ddl_parent_leave_club_mand) {
            BigDecimal v_parent_leave_min = ddl_parent_leave_period_club.getElrSrgKey().getMinTaken().divide(leave_ratio_club);
            v_min_taken = (v_min_taken.compareTo(v_parent_leave_min) == -1) ? v_parent_leave_min : v_min_taken;
        }
        return v_min_taken;
    }

    private BigDecimal maxTakenClub() {
        BigDecimal v_max_taken = ddl_leave_period_club.getElrSrgKey().getMaxTaken();
        if (ddl_ttr_leave_period_club_mand) {
            if (v_max_taken != null) {
                v_max_taken = (v_max_taken.compareTo(ddl_ttr_leave_period_club.getElrSrgKey().getMaxTaken()) == 1) ? ddl_ttr_leave_period_club.getElrSrgKey().getMaxTaken() : v_max_taken;
            } else {
                v_max_taken = ddl_ttr_leave_period_club.getElrSrgKey().getMaxTaken();
            }
        }
        if (ddl_parent_leave_club_mand) {
            BigDecimal v_parent_leave_max = ddl_parent_leave_period_club.getElrSrgKey().getMaxTaken() == null ? null : ddl_parent_leave_period_club.getElrSrgKey().getMaxTaken().divide(leave_ratio_club);
            if (v_max_taken != null) {
                v_max_taken = (v_max_taken.compareTo(v_parent_leave_max) == 1) ? v_parent_leave_max : v_max_taken;
            } else {
                v_max_taken = v_parent_leave_max;
            }
        }
        return v_max_taken;
    }

    private BigDecimal minBalanceClub() {
        BigDecimal v_min_balance = null;
        if (ddl_leave_period_club.getElrSrgKey().getBalCheckedInAppliFlag().equals("Y")
                || (ddl_ttr_leave_period_club_mand && ddl_ttr_leave_period_club.getElrSrgKey().getBalCheckedInAppliFlag().equals("Y"))
                || (ddl_parent_leave_club_mand && ddl_parent_leave_period_club.getElrSrgKey().getBalCheckedInAppliFlag().equals("Y"))) {

            if (ddl_parent_leave_club_mand) {
                if (ddl_ttr_leave_period_club_mand) {
                    if (ddl_ttr_leave_period_club.getElrSrgKey().getBalCheckedInAppliFlag().equals("Y")) {
                        v_min_balance = ddl_ttr_leave_period_club.getCurrentBal();
                    }
                    BigDecimal v_ratio_value = ddl_parent_leave_period_club.getCurrentBal().divide(leave_ratio_club);
                    if (v_min_balance == null) {
                        v_min_balance = v_ratio_value;
                    } else {
                        v_min_balance = v_min_balance.compareTo(v_ratio_value) == -1 ? v_min_balance : v_ratio_value;
                    }

                } else {
                    if (ddl_leave_period_club.getElrSrgKey().getBalCheckedInAppliFlag().equals("Y")) {
                        v_min_balance = ddl_leave_period_club.getCurrentBal();
                    }
                    BigDecimal v_ratio_value = ddl_parent_leave_period_club.getCurrentBal().divide(leave_ratio_club);
                    if (v_min_balance == null) {
                        v_min_balance = v_ratio_value;
                    } else {
                        v_min_balance = v_min_balance.compareTo(v_ratio_value) == -1 ? v_min_balance : v_ratio_value;
                    }
                }
            } else {
                v_min_balance = selectedEmpleavebalClub.getCurrentBal();
            }
        }
        return v_min_balance;
    }
    //</editor-fold>    
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="setMessageDetail">

    private void setMessageDetail(FacesMessage.Severity p_severity, String p_summary, String p_detail) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(p_severity, p_summary, p_detail));
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Confirm Dialog Settings">
    String dlg_confirm_header;
    String dlg_confirm_detail;

    public String getDlg_confirm_detail() {
        return dlg_confirm_detail;
    }

    public void setDlg_confirm_detail(String dlg_confirm_detail) {
        this.dlg_confirm_detail = dlg_confirm_detail;
    }

    public String getDlg_confirm_header() {
        return dlg_confirm_header;
    }

    public void setDlg_confirm_header(String dlg_confirm_header) {
        this.dlg_confirm_header = dlg_confirm_header;
    }
    //</editor-fold>
    ///<editor-fold defaultstate="collapsed" desc="User Selectable List Settings">
    //<editor-fold defaultstate="collapsed" desc="selectedUserFromList">
    FhrdEmpmst selectedUserFromList;

    public FhrdEmpmst getSelectedUserFromList() {
        return selectedUserFromList;
    }

    public void setSelectedUserFromList(FhrdEmpmst selectedUserFromList) {
        this.selectedUserFromList = selectedUserFromList;
        err_txt_user_id = null;
        if (selectedUserFromList != null) {
            selectedUser = fhrdEmpmstFacade.find(selectedUserFromList.getUserId());
            userSelected();
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lst_employee_selectable">
    SD_FhrdEmpmst lst_employee_selectable;

    public SD_FhrdEmpmst getLst_employee_selectable() {
        return lst_employee_selectable;
    }

    public void setLst_employee_selectable(SD_FhrdEmpmst lst_employee_selectable) {
        this.lst_employee_selectable = lst_employee_selectable;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lnk_view_emplist">
    public void lnk_view_emplist(ActionEvent event) {
        boolean isSysUser = getPIS_GlobalSettings().getSysetemUser_Fhrd();
        lst_employee_selectable = new SD_FhrdEmpmst(fhrdEmpmstFacade.findAll(globalData.getWorking_ou(), false, false, false, null, isSysUser ? null : globalData.getUser_id(), null, true, true, false, null));
    }
    //</editor-fold>
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="init()"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        rdb_leave_type = "S";
        setDdl_actionby();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Default constructor and other beans methods">

    public PIS_LeaveApplication() {
    }
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
    //</editor-fold>
}
