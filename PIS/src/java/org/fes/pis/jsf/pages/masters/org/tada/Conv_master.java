package org.fes.pis.jsf.pages.masters.org.tada;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.FtasTadaConvMst;
import org.fes.pis.entities.SystOrgUnitMst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.jsf.utilities.JSFMessages;
import org.fes.pis.sessions.FtasTadaConvMstFacade;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Mayuri
 */
@ManagedBean
@ViewScoped
public class Conv_master implements Serializable {

    @EJB
    private FtasTadaConvMstFacade ftasTadaConvMstFacade;
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="saving methods">
    //<editor-fold defaultstate="collapsed" desc="txt_cm_desc">
    String txt_cm_desc;

    public String getTxt_cm_desc() {
        return txt_cm_desc;
    }

    public void setTxt_cm_desc(String txt_cm_desc) {
        this.txt_cm_desc = txt_cm_desc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_cm_rate">
    String txt_cm_rate;

    public String getTxt_cm_rate() {
        return txt_cm_rate;
    }

    public void setTxt_cm_rate(String txt_cm_rate) {
        this.txt_cm_rate = txt_cm_rate;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_start_date">
    Date cal_start_date;

    public Date getCal_start_date() {
        return cal_start_date;
    }

    public void setCal_start_date(Date cal_start_date) {
        this.cal_start_date = cal_start_date;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_end_date">
    Date cal_end_date;

    public Date getCal_end_date() {
        return cal_end_date;
    }

    public void setCal_end_date(Date cal_end_date) {
        this.cal_end_date = cal_end_date;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetEntryComponent()">

    private void resetEntryComponent() {
        txt_cm_desc = null;
        cal_start_date = null;
        cal_end_date = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_save">

    public void btn_save(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            FtasTadaConvMst ent_FtasTadaConvMst = new FtasTadaConvMst();
            ent_FtasTadaConvMst.setCmSrgKey(new Long("-1"));
            ent_FtasTadaConvMst.setCmDesc(txt_cm_desc.toUpperCase().trim());
            if (!txt_cm_rate.isEmpty()) {
                ent_FtasTadaConvMst.setCmRate(new BigDecimal(txt_cm_rate));
            }
            ent_FtasTadaConvMst.setStartDate(cal_start_date);
            ent_FtasTadaConvMst.setEndDate(cal_end_date);
            ent_FtasTadaConvMst.setCreateby(new FhrdEmpmst(globalData.getUser_id()));
            ent_FtasTadaConvMst.setCreatedt(new Date());
            ent_FtasTadaConvMst.setOumUnitSrno(new SystOrgUnitMst(globalData.getWorking_ou()));
            ftasTadaConvMstFacade.create(ent_FtasTadaConvMst);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Conveyance Master Created", "Conveyance created successfully"));
            RequestContext.getCurrentInstance().execute("pw_dlg_new_conv.hide()");
            setTbl_conv_mst();
            resetEntryComponent();
        } catch (Exception e) {
            context.addMessage(null, JSFMessages.getErrorMessage(e));
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_accept_clicked", null, e);
        }
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="main table methods">
    //<editor-fold defaultstate="collapsed" desc="tbl_conv_mst">
    List<FtasTadaConvMst> tbl_conv_mst;
    List<FtasTadaConvMst> tbl_conv_mst_filter;

    public List<FtasTadaConvMst> getTbl_conv_mst() {
        return tbl_conv_mst;
    }

    public void setTbl_conv_mst(List<FtasTadaConvMst> tbl_conv_mst) {
        this.tbl_conv_mst = tbl_conv_mst;
    }

    public List<FtasTadaConvMst> getTbl_conv_mst_filter() {
        return tbl_conv_mst_filter;
    }

    public void setTbl_conv_mst_filter(List<FtasTadaConvMst> tbl_conv_mst_filter) {
        this.tbl_conv_mst_filter = tbl_conv_mst_filter;
    }

    private void setTbl_conv_mst() {
        tbl_conv_mst = ftasTadaConvMstFacade.findAll(globalData.getWorking_ou(), false);
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Apply End Date Methods">
    //<editor-fold defaultstate="collapsed" desc="selectedConvMst">
    FtasTadaConvMst selectedConvMst;

    public FtasTadaConvMst getSelectedConvMst() {
        return selectedConvMst;
    }

    public void setSelectedGradeMst(FtasTadaConvMst selectedConvMst) {
        this.selectedConvMst = selectedConvMst;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="applyEndDate">
    public void applyEndDate(FtasTadaConvMst ftasTadaConvMst) {
        selectedConvMst = ftasTadaConvMst;
        cal_apply_end_date = null;
        cal_apply_end_date_min = DateTimeUtility.ChangeDateFormat(ftasTadaConvMst.getStartDate(), null);
        RequestContext.getCurrentInstance().execute("pw_dlg_apply_end_date.show()");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_apply_end_date">
    Date cal_apply_end_date;
    String cal_apply_end_date_min;

    public Date getCal_apply_end_date() {
        return cal_apply_end_date;
    }

    public void setCal_apply_end_date(Date cal_apply_end_date) {
        this.cal_apply_end_date = cal_apply_end_date;
    }

    public String getCal_apply_end_date_min() {
        return cal_apply_end_date_min;
    }

    public void setCal_apply_end_date_min(String cal_apply_end_date_min) {
        this.cal_apply_end_date_min = cal_apply_end_date_min;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_apply_end_date">

    public void btn_apply_end_date(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            selectedConvMst = ftasTadaConvMstFacade.find(selectedConvMst.getCmSrgKey());
            selectedConvMst.setEndDate(cal_apply_end_date);
            selectedConvMst.setUpdateby(new FhrdEmpmst(globalData.getUser_id()));
            ftasTadaConvMstFacade.edit(selectedConvMst);
            setTbl_conv_mst();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "End Date Applied", "End Date applied successfully"));
            RequestContext.getCurrentInstance().execute("pw_dlg_apply_end_date.hide()");
        } catch (Exception e) {
            context.addMessage(null, JSFMessages.getErrorMessage(e));
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_accept_clicked", null, e);
        }
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setTbl_conv_mst();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Default Constructor & Other Beans Methods">
    // <editor-fold defaultstate="collapsed" desc="Default Constructor">

    public Conv_master() {
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
    //</editor-fold>
}
