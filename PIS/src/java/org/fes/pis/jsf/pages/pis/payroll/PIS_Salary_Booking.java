/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.pis.payroll;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.db.fes_project.pack.Ftas_Pack;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.BmwpAgencyProjectLink;
import org.fes.pis.entities.BmwpApBudgetMst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.BmwpAgencyProjectLinkFacadeLocal;
import org.fes.pis.sessions.BmwpApBudgetMstFacadeLocal;
import org.fes.pis.sessions.BmwpPartyTypeDtlFacadeLocal;
import org.fes.pis.sessions.BmwpWorkplanFacadeLocal;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Salary_Booking")
@ViewScoped
public class PIS_Salary_Booking implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="EJB & all declarations">
    @EJB
    private BmwpAgencyProjectLinkFacadeLocal bmwpAgencyProjectLinkFacade;
    @EJB
    private BmwpWorkplanFacadeLocal bmwpWorkplanFacade;
    @EJB
    private BmwpPartyTypeDtlFacadeLocal bmwpPartyTypeDtlFacade;
    @EJB
    private BmwpApBudgetMstFacadeLocal bmwpApBudgetMstFacade;
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    Ou_Pack ou_Pack = new Ou_Pack();
    Ftas_Pack ftas_Pack = new Ftas_Pack();
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="common methods">

    /**
     * Creates a new instance of PIS_Salary_Booking
     */
    public PIS_Salary_Booking() {
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_finyear">
    String ddl_finyear;
    List<SelectItem> ddl_finyear_options;

    public String getDdl_finyear() {
        return ddl_finyear;
    }

    public void setDdl_finyear(String ddl_finyear) {
        this.ddl_finyear = ddl_finyear;
    }

    public List<SelectItem> getDdl_finyear_options() {
        return ddl_finyear_options;
    }

    public void setDdl_finyear_options(List<SelectItem> ddl_finyear_options) {
        this.ddl_finyear_options = ddl_finyear_options;
    }

    public void setDdl_finyear_options() {
        ddl_finyear_options = new ArrayList<SelectItem>();
        ddl_finyear_options.add(new SelectItem(20132014, "20132014"));
        ddl_finyear_options.add(new SelectItem(20142015, "20142015"));
        ddl_finyear = ddl_finyear_options.get(0).getValue().toString();
    }

    public void ddl_finyear_changed(ValueChangeEvent event) {
        ddl_finyear = event.getNewValue() == null ? null : event.getNewValue().toString();
        if (ddl_finyear != null) {
            setDdl_agency_project();
        }
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ddl_agency_project Settings">
    String ddl_agency_project;
    List<SelectItem> ddl_agency_project_options;

    public List<SelectItem> getDdl_agency_project_options() {
        return ddl_agency_project_options;
    }

    public void setDdl_agency_project_options(List<SelectItem> ddl_agency_project_options) {
        this.ddl_agency_project_options = ddl_agency_project_options;
    }

    public String getDdl_agency_project() {
        return ddl_agency_project;
    }

    public void setDdl_agency_project() {
        List<BmwpAgencyProjectLink> lst_AgencyProjectLinks = bmwpWorkplanFacade.findAgencyProjectLinkNativeQuery(
                globalData.getWorking_ou(),
                Integer.valueOf(ddl_finyear),
                "F", true, false, null, null);
        List<SelectItem> lst_temp = new ArrayList<SelectItem>();
        int n = lst_AgencyProjectLinks.size();
        if (n == 0) {
            lst_temp.add(new SelectItem(null, "--- Not Available ---"));
        } else {
            for (int i = 0; i < n; i++) {
                lst_temp.add(new SelectItem(lst_AgencyProjectLinks.get(i).getAplUniqueSrno().toString(),
                        lst_AgencyProjectLinks.get(i).getAplFamUniqueSrno().getFamAgencyShortName() + "--" + lst_AgencyProjectLinks.get(i).getAplPmUniqueSrno().getPmProjShortName()));
            }
        }
        ddl_agency_project_options = lst_temp;
        ddl_agency_project = n > 0 ? lst_temp.get(0).getValue().toString() : null;
    }

    public void setDdl_agency_project(String ddl_agency_project) {
        this.ddl_agency_project = ddl_agency_project;
    }

    public void ddl_agency_project_changed(ValueChangeEvent event) {
        ddl_agency_project = event.getNewValue() == null ? null : event.getNewValue().toString();
        setDdl_ap_budget();
    }
    //<editor-fold defaultstate="collapsed" desc="hyp_view_ap_detail_clicked">
    //<editor-fold defaultstate="collapsed" desc="ent_agency_project_detail">
    BmwpAgencyProjectLink ent_agency_project_detail;

    public BmwpAgencyProjectLink getEnt_agency_project_detail() {
        return ent_agency_project_detail;
    }

    public void setEnt_agency_project_detail(BmwpAgencyProjectLink ent_agency_project_detail) {
        this.ent_agency_project_detail = ent_agency_project_detail;
    }

    public void hyp_view_ap_detail_clicked(ActionEvent event) {
        ent_agency_project_detail = bmwpAgencyProjectLinkFacade.find(new BigDecimal(ddl_agency_project));

    }
    //</editor-fold>
    //</editor-fold>
    // </editor-fold>     
    //<editor-fold defaultstate="collapsed" desc="ddl_ap_budget"> 
    String ddl_ap_budget;
    List<SelectItem> ddl_ap_budget_options;

    public String getDdl_ap_budget() {
        return ddl_ap_budget;
    }

    public void setDdl_ap_budget(String ddl_ap_budget) {
        this.ddl_ap_budget = ddl_ap_budget;
    }

    public List<SelectItem> getDdl_ap_budget_options() {
        return ddl_ap_budget_options;
    }

    public void setDdl_ap_budget_options(List<SelectItem> ddl_ap_budget_options) {
        this.ddl_ap_budget_options = ddl_ap_budget_options;
    }

    public void setDdl_ap_budget() {
        ddl_ap_budget_options = new ArrayList<SelectItem>();
        List<BmwpApBudgetMst> lstApBudgetMsts = bmwpApBudgetMstFacade.findAllAPwise(Integer.valueOf(ddl_agency_project));
        Integer n = lstApBudgetMsts.size();
        if (n == 0) {
            ddl_ap_budget_options.add(new SelectItem(null, "---No Data Found---"));
        } else {
            ddl_ap_budget_options.add(new SelectItem(null, "---Select---"));
            for (int i = 0; i < n; i++) {
                ddl_ap_budget_options.add(new SelectItem(lstApBudgetMsts.get(i).getApbmUniqueSrno(), lstApBudgetMsts.get(i).getApbmBudgetDesc()));
            }
        }
        ddl_ap_budget = null;
    }
    //</editor-fold>   
    //<editor-fold defaultstate="collapsed" desc="ddl_budget"> 
    String ddl_budget;
    List<SelectItem> ddl_budget_options;

    public String getDdl_budget() {
        return ddl_budget;
    }

    public void setDdl_budget(String ddl_budget) {
        this.ddl_budget = ddl_budget;
    }

    public List<SelectItem> getDdl_budget_options() {
        return ddl_budget_options;
    }

    public void setDdl_budget_options(List<SelectItem> ddl_budget_options) {
        this.ddl_budget_options = ddl_budget_options;
    }

    public void setDdl_budget_options() {
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="init()"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setDdl_finyear_options();
        setDdl_agency_project();
        setDdl_ap_budget();
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
}
