package org.fes.pis.jsf.pages.pis.masters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.FhrdEarndednmst;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.SystOrgUnitMst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEarndednmstFacadeLocal;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "PIS_EarnDed_Master")
@ViewScoped
public class PIS_EarnDed_Master implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="EJB Declaration and Constructor">
    @EJB
    private FhrdEarndednmstFacadeLocal fhrdEarndednmstFacade;
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();

    /**
     * Creates a new instance of PYRL_EarnDed_Master
     */
    public PIS_EarnDed_Master() {
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tbl_earndedn_mst">
    List<FhrdEarndednmst> tbl_earndedn_mst;
    List<FhrdEarndednmst> tbl_earndedn_mst_filtered;

    public List<FhrdEarndednmst> getTbl_earndedn_mst_filtered() {
        return tbl_earndedn_mst_filtered;
    }

    public void setTbl_earndedn_mst_filtered(List<FhrdEarndednmst> tbl_earndedn_mst_filtered) {
        this.tbl_earndedn_mst_filtered = tbl_earndedn_mst_filtered;
    }

    public List<FhrdEarndednmst> getTbl_earndedn_mst() {
        return tbl_earndedn_mst;
    }

    public void setTbl_earndedn_mst(List<FhrdEarndednmst> tbl_earndedn_mst) {
        this.tbl_earndedn_mst = tbl_earndedn_mst;
    }

    public void setTbl_earndedn_mst() {
        List<FhrdEarndednmst> lst_earndedn_mst = fhrdEarndednmstFacade.findAll(globalData.getWorking_ou());
        List<FhrdEarndednmst> lst_earndedn_temp = lst_earndedn_mst;
        Integer n = lst_earndedn_mst.size();
//        for (int i = 0; i < n; i++) {
//            if (lst_earndedn_temp.get(i).getEarndednFlag().toString().equalsIgnoreCase("E")) {
//                lst_earndedn_temp.get(i).setEarndednFlag("Earning");
//            } else if (lst_earndedn_temp.get(i).getEarndednFlag().toString().equalsIgnoreCase("D")) {
//                lst_earndedn_temp.get(i).setEarndednFlag("Deduction");
//            } else if (lst_earndedn_temp.get(i).getEarndednFlag().toString().equalsIgnoreCase("B")) {
//                lst_earndedn_temp.get(i).setEarndednFlag("Employer's Contribution");
//            } else if (lst_earndedn_temp.get(i).getEarndednFlag().toString().equalsIgnoreCase("R")) {
//                lst_earndedn_temp.get(i).setEarndednFlag("Reimbusrement");
//            } else if (lst_earndedn_temp.get(i).getEarndednFlag().toString().equalsIgnoreCase("C")) {
//                lst_earndedn_temp.get(i).setEarndednFlag("Calculation");
//            } else {
//                lst_earndedn_temp.get(i).setEarndednFlag("-");
//            }
//
//            if (lst_earndedn_temp.get(i).getEarndednType().toString().equalsIgnoreCase("O")) {
//                lst_earndedn_temp.get(i).setEarndednType("Occasional");
//            } else if (lst_earndedn_temp.get(i).getEarndednType().toString().equalsIgnoreCase("R")) {
//                lst_earndedn_temp.get(i).setEarndednType("Regular");
//            } else {
//                lst_earndedn_temp.get(i).setEarndednType("-");
//            }
//
//            if (lst_earndedn_temp.get(i).getBasecodeFlag().toString().equalsIgnoreCase("Y")) {
//                lst_earndedn_temp.get(i).setBasecodeFlag("YES");
//            } else if (lst_earndedn_temp.get(i).getBasecodeFlag().toString().equalsIgnoreCase("N")) {
//                lst_earndedn_temp.get(i).setBasecodeFlag("NO");
//            } else if (lst_earndedn_temp.get(i).getBasecodeFlag().toString().equalsIgnoreCase("S")) {
//                lst_earndedn_temp.get(i).setBasecodeFlag("Secondary");
//            } else {
//                lst_earndedn_temp.get(i).setBasecodeFlag("-");
//            }
//        }
        tbl_earndedn_mst = lst_earndedn_temp;
    }

    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_view_ed_detail_click">
    public void btn_view_ed_detail_click(ActionEvent event) {
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="tbl_earn_dedn_dtl">
    FhrdEarndednmst tbl_earn_dedn_dtl;

    public FhrdEarndednmst getTbl_earn_dedn_dtl() {
        return tbl_earn_dedn_dtl;
    }

    public void setTbl_earn_dedn_dtl(FhrdEarndednmst tbl_earn_dedn_dtl) {
        this.tbl_earn_dedn_dtl = tbl_earn_dedn_dtl;
    }
    List<FhrdEarndednmst> lst_earndedn_dtl;
    FhrdEarndednmst selection_earndedn;

    public FhrdEarndednmst getSelection_earndedn() {
        return selection_earndedn;
    }

    public void setSelection_earndedn(FhrdEarndednmst selection_earndedn) {
        if (selection_earndedn != null) {
//            if (selection_earndedn.getPrintInPaySlip() == null) {
//                selection_earndedn.setPrintInPaySlip("-");
//            } else if (selection_earndedn.getPrintInPaySlip().toString().equalsIgnoreCase("y")) {
//                selection_earndedn.setPrintInPaySlip("Yes");
//            } else if (selection_earndedn.getPrintInPaySlip().toString().equalsIgnoreCase("n")) {
//                selection_earndedn.setPrintInPaySlip("No");
//            } else {
//                selection_earndedn.setPrintInPaySlip("-");
//            }
//            if (selection_earndedn.getPrintIf0Flag() == null) {
//                selection_earndedn.setPrintIf0Flag("-");
//            } else if (selection_earndedn.getPrintIf0Flag().toString().equalsIgnoreCase("y")) {
//                selection_earndedn.setPrintIf0Flag("Yes");
//            } else if (selection_earndedn.getPrintIf0Flag().toString().equalsIgnoreCase("n")) {
//                selection_earndedn.setPrintIf0Flag("No");
//            } else {
//                selection_earndedn.setPrintIf0Flag("-");
//            }
//            if (selection_earndedn.getPayableOnValue() == null) {
//                selection_earndedn.setPayableOnValue("-");
//            } else if (selection_earndedn.getPayableOnValue().toString().equalsIgnoreCase("op")) {
//                selection_earndedn.setPayableOnValue("Organization Payable Days");
//            } else if (selection_earndedn.getPayableOnValue().toString().equalsIgnoreCase("ep")) {
//                selection_earndedn.setPayableOnValue("Employee Payable Days");
//            } else if (selection_earndedn.getPayableOnValue().toString().equalsIgnoreCase("ow")) {
//                selection_earndedn.setPayableOnValue("Organization Working Days");
//            } else if (selection_earndedn.getPayableOnValue().toString().equalsIgnoreCase("ew")) {
//                selection_earndedn.setPayableOnValue("Employee Working Days");
//            } else if (selection_earndedn.getPayableOnValue().toString().equalsIgnoreCase("mv")) {
//                selection_earndedn.setPayableOnValue("Monthly Variable Values");
//            } else {
//                selection_earndedn.setPayableOnValue("-");
//            }
            tbl_earn_dedn_dtl = selection_earndedn;
        }
        this.selection_earndedn = selection_earndedn;
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="binded attributes">
    //*
    String txt_earndedn_cd;
    String txt_print_order;
    String txt_earndedn_name;
    String ddl_earndedn_flag;
    String ddl_earndedn_type;
    String ddl_basecd_flag;
    String ddl_print_in_pay_flag;
    String ddl_print_if_zero_flag;
    String ddl_payable_on_value_flag;
    String txt_preference_no;
    String txt_level_no;
    String hdn_basecd;
    String hdn_edm_srg_key;
    Date cal_start_date;
    Date cal_end_date;
    String ddl_first_entry_flag;
    String ddl_contract_type;
    String ddl_taxable;
    String ddl_statutory;
    // <editor-fold defaultstate="collapsed" desc="getter/setter methods">

    public String getDdl_earndedn_flag() {
        return ddl_earndedn_flag;
    }

    public void setDdl_earndedn_flag(String ddl_earndedn_flag) {
        this.ddl_earndedn_flag = ddl_earndedn_flag;
    }

    public String getDdl_statutory() {
        return ddl_statutory;
    }

    public void setDdl_statutory(String ddl_statutory) {
        this.ddl_statutory = ddl_statutory;
    }

    public String getDdl_taxable() {
        return ddl_taxable;
    }

    public void setDdl_taxable(String ddl_taxable) {
        this.ddl_taxable = ddl_taxable;
    }

    public String getHdn_edm_srg_key() {
        return hdn_edm_srg_key;
    }

    public void setHdn_edm_srg_key(String hdn_edm_srg_key) {
        this.hdn_edm_srg_key = hdn_edm_srg_key;
    }

    public String getHdn_basecd() {
        return hdn_basecd;
    }

    public void setHdn_basecd(String hdn_basecd) {
        this.hdn_basecd = hdn_basecd;
    }

    public String getDdl_basecd_flag() {
        return ddl_basecd_flag;
    }

    public void setDdl_basecd_flag(String ddl_basecd_flag) {
        this.ddl_basecd_flag = ddl_basecd_flag;
    }

    public String getTxt_earndedn_name() {
        return txt_earndedn_name;
    }

    public void setTxt_earndedn_name(String txt_earndedn_name) {
        this.txt_earndedn_name = txt_earndedn_name;
    }

    public String getTxt_print_order() {
        return txt_print_order;
    }

    public void setTxt_print_order(String txt_print_order) {
        this.txt_print_order = txt_print_order;
    }

    public String getTxt_level_no() {
        return txt_level_no;
    }

    public void setTxt_level_no(String txt_level_no) {
        this.txt_level_no = txt_level_no;
    }

    public String getTxt_preference_no() {
        return txt_preference_no;
    }

    public void setTxt_preference_no(String txt_preference_no) {
        this.txt_preference_no = txt_preference_no;
    }

    public String getTxt_earndedn_cd() {
        return txt_earndedn_cd;
    }

    public void setTxt_earndedn_cd(String txt_earndedn_cd) {
        this.txt_earndedn_cd = txt_earndedn_cd;
    }

    public String getDdl_payable_on_value_flag() {
        return ddl_payable_on_value_flag;
    }

    public void setDdl_payable_on_value_flag(String ddl_payable_on_value_flag) {
        this.ddl_payable_on_value_flag = ddl_payable_on_value_flag;
    }

    public String getDdl_earndedn_type() {
        return ddl_earndedn_type;
    }

    public void setDdl_earndedn_type(String ddl_earndedn_type) {
        this.ddl_earndedn_type = ddl_earndedn_type;
    }

    public String getDdl_print_if_zero_flag() {
        return ddl_print_if_zero_flag;
    }

    public void setDdl_print_if_zero_flag(String ddl_print_if_zero_flag) {
        this.ddl_print_if_zero_flag = ddl_print_if_zero_flag;
    }

    public String getDdl_print_in_pay_flag() {
        return ddl_print_in_pay_flag;
    }

    public void setDdl_print_in_pay_flag(String ddl_print_in_pay_flag) {
        this.ddl_print_in_pay_flag = ddl_print_in_pay_flag;
    }

    public Date getCal_end_date() {
        return cal_end_date;
    }

    public void setCal_end_date(Date cal_end_date) {
        this.cal_end_date = cal_end_date;
    }

    public Date getCal_start_date() {
        return cal_start_date;
    }

    public void setCal_start_date(Date cal_start_date) {
        this.cal_start_date = cal_start_date;
    }

    public String getDdl_first_entry_flag() {
        return ddl_first_entry_flag;
    }

    public void setDdl_first_entry_flag(String ddl_first_entry_flag) {
        this.ddl_first_entry_flag = ddl_first_entry_flag;
    }

    public String getDdl_contract_type() {
        return ddl_contract_type;
    }

    public void setDdl_contract_type(String ddl_contract_type) {
        this.ddl_contract_type = ddl_contract_type;
    }
    // </editor-fold>
// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_code_alternative"> 
    String ddl_code_alternative;
    List<SelectItem> ddl_code_alternative_options;

    public String getDdl_code_alternative() {
        return ddl_code_alternative;
    }

    public void setDdl_code_alternative(String ddl_code_alternative) {
        this.ddl_code_alternative = ddl_code_alternative;
    }

    public List<SelectItem> getDdl_code_alternative_options() {
        return ddl_code_alternative_options;
    }

    public void setDdl_code_alternative_options(List<SelectItem> ddl_code_alternative_options) {
        this.ddl_code_alternative_options = ddl_code_alternative_options;
    }

    private void setDdl_code_alternative() {
        List<SelectItem> lst_temp;
        List<FhrdEarndednmst> lstEarndednmsts = fhrdEarndednmstFacade.findAll(globalData.getWorking_ou());
        lst_temp = new ArrayList<SelectItem>();
        if (lstEarndednmsts.isEmpty()) {
            SelectItem s = new SelectItem(null, "--- Not Available ---");
            lst_temp.add(s);
        } else {
            SelectItem s = new SelectItem(null, "--- None ---");
            lst_temp.add(s);
            Integer n = lstEarndednmsts.size();
            for (int i = 0; i < n; i++) {
                s = new SelectItem(lstEarndednmsts.get(i).getEdmSrgKey().toString(), lstEarndednmsts.get(i).getEarndednName());
                lst_temp.add(s);
            }
        }
        ddl_code_alternative_options = lst_temp;
        ddl_code_alternative = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Apply End Date Settings">

    public void btn_apply_date_action(ActionEvent event) {
    }
    FhrdEarndednmst entEarndednmst_edit;

    public FhrdEarndednmst getEntEarndednmst_edit() {
        return entEarndednmst_edit;
    }

    public void setEntEarndednmst_edit(FhrdEarndednmst entEarndednmst_edit) {
        this.entEarndednmst_edit = entEarndednmst_edit;
        cal_apply_end_date_min = DateTimeUtility.ChangeDateFormat(entEarndednmst_edit.getStartDate(), null);
        cal_apply_end_date = null;
    }
    Date cal_apply_end_date;
    String cal_apply_end_date_min;

    public Date getCal_apply_end_date() {
        return cal_apply_end_date;
    }

    public void setCal_apply_end_date(Date cal_apply_end_date) {
        this.cal_apply_end_date = cal_apply_end_date;
    }

    public String getCal_apply_end_date_min() {
        return cal_apply_end_date_min;
    }

    public void setCal_apply_end_date_min(String cal_apply_end_date_min) {
        this.cal_apply_end_date_min = cal_apply_end_date_min;
    }

    public void btn_apply_end_date_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        BigDecimal v_edm_srg_key = entEarndednmst_edit.getEdmSrgKey();
        FhrdEarndednmst entFhrdEarndedn = fhrdEarndednmstFacade.find(v_edm_srg_key);
        entFhrdEarndedn.setEndDate(cal_apply_end_date);
        entFhrdEarndedn.setUpdateby(globalData.getEnt_login_user());
        entFhrdEarndedn.setUpdatedt(new Date());
        try {
            fhrdEarndednmstFacade.edit(entFhrdEarndedn);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record updated Successfully"));
            setTbl_earndedn_mst();
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some error occured during updating Rule"));
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_apply_end_date_clicked", null, e);
        }
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="btn_add_new_action">

    public void btn_add_new_action(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        String v_start_date = DateTimeUtility.ChangeDateFormat(cal_start_date, null);
        String v_end_date = "";
        if (cal_end_date != null) {
            v_end_date = DateTimeUtility.ChangeDateFormat(cal_end_date, null);
        }
        if (fhrdEarndednmstFacade.isEdCodeDescExists(globalData.getWorking_ou(), cal_start_date, cal_end_date, txt_earndedn_cd.trim().toUpperCase(), txt_earndedn_name.trim().toUpperCase())) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "Same Earn Deduction Code/Name exists in given period"));
            return;
        }
        if (ddl_basecd_flag.equals("Y")) {
            List<FhrdEarndednmst> lst_Earndednmsts = fhrdEarndednmstFacade.find_Basecode(v_start_date, v_end_date, "Y", globalData.getWorking_ou());
            if (!lst_Earndednmsts.isEmpty()) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "Cannot have 2 Primary base code.Check Previous Earn-Deduction Basecode in Master Table.."));
                return;
            }
        }

        if (ddl_basecd_flag.equals("S")) {
            List<FhrdEarndednmst> lst_Earndednmsts = fhrdEarndednmstFacade.find_Basecode(v_start_date, v_end_date, "S", globalData.getWorking_ou());
            if (!lst_Earndednmsts.isEmpty()) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "Cannot have 2 Secondary base code.Check Previous Earn-Deduction Basecode in Master Table.."));
                return;
            }
        }
//        BigDecimal v_edm_srgkey = getGlobalSettings().getUniqueSrno(null, "FHRD_EARNDEDNMST");
        FhrdEarndednmst ent_Earndednmst = new FhrdEarndednmst();
        ent_Earndednmst.setEdmSrgKey(new BigDecimal("1"));
        ent_Earndednmst.setEarndedncd(txt_earndedn_cd.trim().toUpperCase());
        ent_Earndednmst.setEarndednName(txt_earndedn_name.trim().toUpperCase());
        ent_Earndednmst.setStartDate(cal_start_date);
        if (cal_end_date != null) {
            ent_Earndednmst.setEndDate(cal_end_date);
        }
        ent_Earndednmst.setEarndednFlag(ddl_earndedn_flag);
        ent_Earndednmst.setEarndednType(ddl_earndedn_type);
        ent_Earndednmst.setBasecodeFlag(ddl_basecd_flag);
        if (!txt_print_order.isEmpty()) {
            ent_Earndednmst.setPrintOrder(Short.valueOf(txt_print_order));
        }
        if (!txt_level_no.isEmpty()) {
            ent_Earndednmst.setLevelNo(Short.valueOf(txt_level_no));
        }
        if (!txt_preference_no.isEmpty()) {
            ent_Earndednmst.setPreferenceNo(Short.valueOf(txt_preference_no));
        }
        ent_Earndednmst.setTaxableFlag(ddl_taxable);
        ent_Earndednmst.setStatutoryFlag(ddl_statutory);
        ent_Earndednmst.setPrintInPaySlip(ddl_print_in_pay_flag);
        ent_Earndednmst.setPrintIf0Flag(ddl_print_if_zero_flag);
        ent_Earndednmst.setPayableOnValue(ddl_payable_on_value_flag);
        ent_Earndednmst.setFirstEntryFlag(ddl_first_entry_flag);
        ent_Earndednmst.setContractTypeFlag(ddl_contract_type);
        if (ddl_code_alternative != null) {
            FhrdEarndednmst e = fhrdEarndednmstFacade.find(new BigDecimal(ddl_code_alternative));
            ent_Earndednmst.setEdmAltSrno(e.getEdmAltSrno());
            ent_Earndednmst.setEdmAltSubsrno(e.getEdmAltSubsrno());
        }
        FhrdEmpmst ent_Empmst = new FhrdEmpmst(globalData.getUser_id());
        SystOrgUnitMst ent_OrgUnitMst = new SystOrgUnitMst(globalData.getWorking_ou());
        ent_Earndednmst.setOumUnitSrno(ent_OrgUnitMst);
        ent_Earndednmst.setCreateby(ent_Empmst);
        ent_Earndednmst.setCreatedt(new Date());
        try {
            fhrdEarndednmstFacade.create(ent_Earndednmst);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully"));
            resetAddNewEntry();
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Record not saved.Some unexpected Error Occured.."));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_add_new_action", null, e);
        }

    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetAddNewEntry()"> 

    public void resetAddNewEntry() {
        setTbl_earndedn_mst();
        txt_earndedn_cd = "";
        txt_earndedn_name = "";
        txt_level_no = "";
        txt_preference_no = "";
        txt_print_order = "";
        ddl_basecd_flag = null;
        setDdl_code_alternative();
        ddl_contract_type = null;
        ddl_earndedn_flag = null;
        ddl_earndedn_type = null;
        ddl_first_entry_flag = null;
        ddl_payable_on_value_flag = null;
        ddl_print_if_zero_flag = null;
        ddl_print_in_pay_flag = null;
        cal_start_date = null;
        cal_end_date = null;
        ddl_statutory = null;
        ddl_taxable = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Alternative Detail Settings">
    //<editor-fold defaultstate="collapsed" desc="tbl_alternatives">
    List<FhrdEarndednmst> tbl_alternatives;

    public List<FhrdEarndednmst> getTbl_alternatives() {
        return tbl_alternatives;
    }

    public void setTbl_alternatives(List<FhrdEarndednmst> tbl_alternatives) {
        this.tbl_alternatives = tbl_alternatives;
    }

    public List<FhrdEarndednmst> getLst_earndedn_dtl() {
        return lst_earndedn_dtl;
    }

    public void setLst_earndedn_dtl(List<FhrdEarndednmst> lst_earndedn_dtl) {
        this.lst_earndedn_dtl = lst_earndedn_dtl;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="hyp_view_alternate_clicked">

    public void hyp_view_alternate_clicked(ActionEvent event) {
        tbl_alternatives = fhrdEarndednmstFacade.findAllAlternatives(globalData.getWorking_ou(), new BigDecimal(ddl_code_alternative));
        RequestContext.getCurrentInstance().execute("pw_dlg_alter_ed_masters.show()");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_view_alternatives"> 

    public void btn_view_alternatives(FhrdEarndednmst fhrdEarndednmst) {
        tbl_alternatives = fhrdEarndednmstFacade.findAllAlternatives(globalData.getWorking_ou(), fhrdEarndednmst.getEdmSrgKey());
        RequestContext.getCurrentInstance().execute("pw_dlg_alter_ed_masters.show()");
    }
    //</editor-fold>
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>        
    //<editor-fold defaultstate="collapsed" desc="init()"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setTbl_earndedn_mst();
        setDdl_code_alternative();
    }
    //</editor-fold>
}
