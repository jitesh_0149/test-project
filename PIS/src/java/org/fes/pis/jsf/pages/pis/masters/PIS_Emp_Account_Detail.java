/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.pis.masters;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;
import org.primefaces.context.RequestContext;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Emp_Account_Detail")
@ViewScoped
public class PIS_Emp_Account_Detail implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="EJB Declarations">
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="default">

    /**
     * Creates a new instance of PIS_Emp_Account_Detail
     */
    public PIS_Emp_Account_Detail() {
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_emp_mst"> 
    List<FhrdEmpmst> tbl_emp_info;

    public List<FhrdEmpmst> getTbl_emp_info() {
        return tbl_emp_info;
    }

    public void setTbl_emp_info(List<FhrdEmpmst> tbl_emp_info) {
        this.tbl_emp_info = tbl_emp_info;
    }

    public void setTblEmpInfo() {
        tbl_emp_info = fhrdEmpmstFacade.findAll(globalData.getWorking_ou(), false, false, true, null, null, null, false, false, true, null);
//        if (globalData.isSystemUserFHRD()) {
//            tbl_emp_info = fhrdEmpmstFacade.findAll(getGlobalSettings().getWorking_ou(), null);
//        } else {
//            tbl_emp_info = fhrdEmpmstFacade.findByAccessSpan(getGlobalSettings().getUser_id(), globalData.getWorking_ou());
//        }
    }
    SelectItem[] lst_yes_no_options;
    //</editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="tbl_emp_info_filtered"> 
    List<FhrdEmpmst> tbl_emp_info_filtered;

    public List<FhrdEmpmst> getTbl_emp_info_filtered() {
        return tbl_emp_info_filtered;
    }

    public void setTbl_emp_info_filtered(List<FhrdEmpmst> tbl_emp_info_filtered) {
        this.tbl_emp_info_filtered = tbl_emp_info_filtered;
    }
    //</editor-fold>      
    //<editor-fold defaultstate="collapsed" desc="employeeAccDetail"> 
    FhrdEmpmst employeeAccDetail;

    public FhrdEmpmst getEmployeeAccDetail() {
        return employeeAccDetail;
    }

    public void setEmployeeAccDetail(FhrdEmpmst employeeAccDetail) {
        txt_bank_account_no = null;
        txt_bank_location = null;
        txt_bank_name = null;
        txt_ifsc_code = null;
        txt_ntgcfpf_no = null;
        txt_pan_no = null;
        txt_emailid = null;
        ddl_gender = null;
        txt_uan_no = null;
        if (employeeAccDetail.getBankAccountno() != null) {
            txt_bank_account_no = employeeAccDetail.getBankAccountno().toString();
        }
        if (employeeAccDetail.getBankLocation() != null) {
            txt_bank_location = employeeAccDetail.getBankLocation();
        }
        if (employeeAccDetail.getBankName() != null) {
            txt_bank_name = employeeAccDetail.getBankName();
        }
        if (employeeAccDetail.getIfscCode() != null) {
            txt_ifsc_code = employeeAccDetail.getIfscCode().toString();
        }
        if (employeeAccDetail.getNtgcfpfno() != null) {
            txt_ntgcfpf_no = employeeAccDetail.getNtgcfpfno().toString();
        }
        if (employeeAccDetail.getPanno() != null) {
            txt_pan_no = employeeAccDetail.getPanno().toString();
        }
        if (employeeAccDetail.getGender() != null) {
            ddl_gender = employeeAccDetail.getGender().toString();
        }
        if (employeeAccDetail.getEmailId() != null) {
            txt_emailid = employeeAccDetail.getEmailId().toString();
        }
        if (employeeAccDetail.getUanPfNo() != null) {
            txt_uan_no = employeeAccDetail.getUanPfNo().toString();
        }
        this.employeeAccDetail = employeeAccDetail;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="btn_edit_ecc_detail_clicked"> 
    public void btn_edit_ecc_detail_clicked(ActionEvent event) {
        RequestContext.getCurrentInstance().execute("pw_dlg_acc_detail.show();");

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="fields">
    String txt_ntgcfpf_no;
    String txt_ifsc_code;
    String txt_bank_account_no;
    String txt_pan_no;
    String txt_bank_name;
    String txt_bank_location;
    String txt_emailid;
    String txt_uan_no;

    public String getTxt_uan_no() {
        return txt_uan_no;
    }

    public void setTxt_uan_no(String txt_uan_no) {
        this.txt_uan_no = txt_uan_no;
    }

    public String getTxt_emailid() {
        return txt_emailid;
    }

    public void setTxt_emailid(String txt_emailid) {
        this.txt_emailid = txt_emailid;
    }

    public String getTxt_bank_account_no() {
        return txt_bank_account_no;
    }

    public void setTxt_bank_account_no(String txt_bank_account_no) {
        this.txt_bank_account_no = txt_bank_account_no;
    }

    public String getTxt_bank_location() {
        return txt_bank_location;
    }

    public void setTxt_bank_location(String txt_bank_location) {
        this.txt_bank_location = txt_bank_location;
    }

    public String getTxt_bank_name() {
        return txt_bank_name;
    }

    public void setTxt_bank_name(String txt_bank_name) {
        this.txt_bank_name = txt_bank_name;
    }

    public String getTxt_ifsc_code() {
        return txt_ifsc_code;
    }

    public void setTxt_ifsc_code(String txt_ifsc_code) {
        this.txt_ifsc_code = txt_ifsc_code;
    }

    public String getTxt_ntgcfpf_no() {
        return txt_ntgcfpf_no;
    }

    public void setTxt_ntgcfpf_no(String txt_ntgcfpf_no) {
        this.txt_ntgcfpf_no = txt_ntgcfpf_no;
    }

    public String getTxt_pan_no() {
        return txt_pan_no;
    }

    public void setTxt_pan_no(String txt_pan_no) {
        this.txt_pan_no = txt_pan_no;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_gender">
    String ddl_gender;

    public String getDdl_gender() {
        return ddl_gender;
    }

    public void setDdl_gender(String ddl_gender) {
        this.ddl_gender = ddl_gender;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_view_ecc_detail_clicked">
    FhrdEmpmst viewEmployeeAccDetail;

    public FhrdEmpmst getViewEmployeeAccDetail() {
        return viewEmployeeAccDetail;
    }

    public void setViewEmployeeAccDetail(FhrdEmpmst viewEmployeeAccDetail) {
        this.viewEmployeeAccDetail = viewEmployeeAccDetail;
    }

    public void btn_view_ecc_detail_clicked(ActionEvent event) {
        RequestContext.getCurrentInstance().execute("pw_dlg_view_acc_detail.show();");
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="btn_save_acc_details_clicked"> 
    public void btn_save_acc_details_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            FhrdEmpmst entEmpmst = fhrdEmpmstFacade.find(employeeAccDetail.getUserId());

            if (!txt_bank_account_no.isEmpty()) {
                entEmpmst.setBankAccountno(txt_bank_account_no.toUpperCase());
            } else {
                entEmpmst.setBankAccountno("");
            }
            if (!txt_bank_location.isEmpty()) {
                entEmpmst.setBankLocation(txt_bank_location.toUpperCase());
            } else {
                entEmpmst.setBankLocation("");
            }
            if (!txt_bank_name.isEmpty()) {
                entEmpmst.setBankName(txt_bank_name.toUpperCase());
            } else {
                entEmpmst.setBankName("");
            }
            if (!txt_ifsc_code.isEmpty()) {
                entEmpmst.setIfscCode(txt_ifsc_code.toUpperCase());
            } else {
                entEmpmst.setIfscCode("");
            }
            if (!txt_pan_no.isEmpty()) {
                entEmpmst.setPanno(txt_pan_no.toUpperCase());
            } else {
                entEmpmst.setPanno("");
            }
            if (!txt_ntgcfpf_no.isEmpty()) {
                entEmpmst.setNtgcfpfno(txt_ntgcfpf_no.toUpperCase());
            } else {
                entEmpmst.setNtgcfpfno("");
            }
            if (ddl_gender != null) {
                entEmpmst.setGender(ddl_gender);
            }
            if (!txt_emailid.isEmpty()) {
                entEmpmst.setEmailId(txt_emailid);
            }
            if (!txt_uan_no.isEmpty()) {
                entEmpmst.setUanPfNo(new Long(txt_uan_no));
            }
            entEmpmst.setUpdateby(globalData.getUser_id());
            entEmpmst.setUpdatedt(new Date());
            fhrdEmpmstFacade.edit(entEmpmst);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Employee Account Detail saved Successfully"));
            RequestContext.getCurrentInstance().execute("pw_dlg_acc_detail.hide();");
            setTblEmpInfo();
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred."));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_save_acc_details_clicked", null, e);
        }

    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="init()"> 
    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setTblEmpInfo();
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
}
