package org.fes.pis.jsf.pages;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.SystOrgUnitMst;
import org.fes.pis.jsf.GlobalResources;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.SystOrgUnitMstFacadeLocal;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author Krishna
 */
@ManagedBean
@SessionScoped
public class Template implements Serializable {

    PIS_GlobalData globalData;
    @EJB
    private SystOrgUnitMstFacadeLocal systOrgUnitMstFacade;
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    // <editor-fold defaultstate="collapsed" desc="Header Methods">
    //<editor-fold defaultstate="collapsed" desc="user_name">
    private String user_name = null;

    public String getUser_name() {
        return user_name.toLowerCase();
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Oraganisation List">
    List<SelectItem> ddl_organisation_unit_options = new ArrayList<SelectItem>();
    String ddl_organisation_unit = null;

    public List<SelectItem> getDdl_organisation_unit_options() {
        return ddl_organisation_unit_options;
    }

    public void setDdl_organisation_unit_options(List<SelectItem> ddl_organisation_unit_options) {
        this.ddl_organisation_unit_options = ddl_organisation_unit_options;
    }

    public String getDdl_organisation_unit() {
        return ddl_organisation_unit;
    }

    public void setDdl_organisation_unit(String ddl_organisation_unit) {
        this.ddl_organisation_unit = ddl_organisation_unit;
    }

    public void setDdl_organisation_unit() {
        List<SystOrgUnitMst> lst_systOrgUnitMsts = systOrgUnitMstFacade.findAllForUser(globalData.getUser_id(), globalData.getEnt_login_user().getPostingOumUnitSrno().getOumUnitSrno());
        ddl_organisation_unit_options = new ArrayList<SelectItem>();
        SystOrgUnitMst ent_systOrgUnitMst;
        int n = lst_systOrgUnitMsts.size();
        for (int i = 0; i < n; i++) {
            ent_systOrgUnitMst = lst_systOrgUnitMsts.get(i);
            ddl_organisation_unit_options.add(new SelectItem(ent_systOrgUnitMst.getOumUnitSrno().toString(), ent_systOrgUnitMst.getOumName()));
        }
        ddl_organisation_unit = globalData.getWorking_ou().toString();
        if (!lst_systOrgUnitMsts.contains(globalData.getEnt_working_ou())) {
            getGlobalSettings().setEnt_working_ou(lst_systOrgUnitMsts.get(0));
            getPIS_GlobalSettings().working_ou_changed();
        }

    }

    public void btn_change_organisation_unit_clicked(ActionEvent event) {
        getGlobalSettings().setEnt_working_ou(systOrgUnitMstFacade.find(new Integer(ddl_organisation_unit)));
        getPIS_GlobalSettings().working_ou_changed();
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setMenuModel();
    }
    // </editor-fold>
    // </editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="Menubar Methods">
    MenuModel menuModel;

    public void setMenuModel() {
        menuModel = new DefaultMenuModel();
        List<Object> stack = new ArrayList<Object>();
        Connection cn = null;
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String baseURL = req.getRequestURL().toString().replace(req.getRequestURI().substring(1), req.getContextPath());
        baseURL = baseURL.split("//", 2)[0] + "//" + baseURL.split("//", 2)[1].replace("//", "/");
        try {
            cn = getGlobalUtilities().getjdbcFES_PROJECT_Oracle().getConnection();
            Statement stmt = cn.createStatement();
            ResultSet rs;

            String q_find_menu =
                    " SELECT MENUDESC,URL,MENUID,LEVEL,CONNECT_BY_ISLEAF" + "\"IsLeaf\""
                    + " FROM SYST_MENUMST"
                    + " WHERE OUM_UNIT_SRNO IN OU_PACK.GET_OUM_UNIT_SRNO('SYST_MENUMST'," + globalData.getWorking_ou() + ") "
                    + " AND MENUID LIKE 'PIS%'"
                    + " AND WORKING_FLAG= 'Y'"
                    + " AND LEVEL > 1"
                    + " AND (REQ_ADMIN_SYST_LIST IS NULL";
            if (globalData.isSystemUserFHRD()) {
                q_find_menu += " OR INSTR(REQ_ADMIN_SYST_LIST,'FHRD')<>0";
            }
            if (globalData.isSystemUserFTAS()) {
                q_find_menu += " OR INSTR(REQ_ADMIN_SYST_LIST,'FTAS')<>0";
            }

            q_find_menu += ")";
            q_find_menu += " AND (ACCESS_SPAN_FLAG IS NULL"
                    + " OR ACCESS_SPAN_FLAG="
                    + " (SELECT ACCESS_SPAN FROM SYST_ORG_ACCESS_RIGHTS"
                    + " WHERE  OUM_UNIT_SRNO = " + globalData.getWorking_ou()
                    + " AND USER_ID = " + globalData.getUser_id()
                    + " AND APPLI_NAME = 'FHRD')"
                    + " )";
            q_find_menu += " START WITH PARENTID IS NULL   "
                    + " CONNECT BY PRIOR  MENUID = PARENTID "
                    + " ORDER SIBLINGS BY MENUID";
//            System.out.println("menu query : " + q_find_menu);
            rs = stmt.executeQuery(q_find_menu);
            String v_menu_desc;
            String v_URL;
            String v_OnClick;
            if (rs.next()) {
                int v_current_level = rs.getInt("LEVEL");
                int v_level_difference = v_current_level - 1;
                int v_actual_level = v_current_level - v_level_difference;
                stack.add(v_actual_level - 1, menuModel);
                do {
                    v_current_level = rs.getInt("LEVEL");
                    v_actual_level = v_current_level - v_level_difference;
                    //v_menu_desc = StringEscapeUtils.escapeEcmaScript(rs.getString("MENUDESC"));
                    v_menu_desc = rs.getString("MENUDESC");
                    v_URL = rs.getString("URL");
                    v_OnClick = null;
                    if (v_URL == null || v_URL.equals("null") || v_URL.isEmpty()) {
                        v_URL = "#";
                    } else {
                        if (v_URL.contains("onclick")) {
                            v_OnClick = v_URL.replace("onclick=", "").replaceAll("\"", "");
                            v_URL = "javascript:void(0);";
                        } else {
                            v_URL = baseURL + "/" + v_URL;
                        }
                    }
                    if (rs.getInt("IsLeaf") == 0) {
                        DefaultSubMenu submenu = new DefaultSubMenu(v_menu_desc);
                        if ((v_actual_level - 1) == 0) {
                            ((MenuModel) stack.get(v_actual_level - 1)).addElement(submenu);
                        } else {
                            ((DefaultSubMenu) stack.get(v_actual_level - 1)).addElement(submenu);
                        }
                        stack.add(v_actual_level, submenu);
                    } else {
                        DefaultMenuItem menuItem = new DefaultMenuItem(v_menu_desc);
                        if (v_OnClick == null) {
                            menuItem.setUrl(v_URL);
                        } else {
                            menuItem.setOnclick(v_OnClick);
                            menuItem.setUrl(v_URL);
                        }
                        if ((v_actual_level - 1) == 0) {
                            ((MenuModel) stack.get(v_actual_level - 1)).addElement(menuItem);
                        } else {
                            ((DefaultSubMenu) stack.get(v_actual_level - 1)).addElement(menuItem);
                        }
                    }

                } while (rs.next());
            }
            rs.close();
            stmt.close();

        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setMenuModel", null, ex);
        } catch (Exception ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setMenuModel", null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                    }
                } catch (Exception ex) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setMenuModel", null, ex);
                }
            }
        }

    }

    public MenuModel getMenuModel() {

        return menuModel;
    }

    public void setMenuModel(MenuModel menuModel) {
        this.menuModel = menuModel;
    }
    // </editor-fold>
    //    // <editor-fold defaultstate="collapsed" desc="System Menu">
//    private List<SystMenumst> lst_SystMenumsts = new ArrayList<SystMenumst>();
//
//    public List<SystMenumst> getLst_SystMenumsts() throws SQLException {
//        getSystem_Menu();
//        return lst_SystMenumsts;
//    }
//
//    public void setLst_SystMenumsts(List<SystMenumst> lst_SystMenumsts) {
//        this.lst_SystMenumsts = lst_SystMenumsts;
//    }
//
//    public void getSystem_Menu() throws SQLException {
//        Connection cn = getGlobalUtilities().getjdbcFES_PROJECT_Oracle().getConnection();
//        try {
//            Statement stmt = cn.createStatement();
//            try {
//                ResultSet rs;
//                String q_find_menu =
//                        "  SELECT menudesc,URL,LEVEL"
//                        + "  FROM SYST_MENUMST"
//                        + "  WHERE OUM_UNIT_SRNO= OU_PACK.GET_ORG_SRNO(" + globalData.getWorking_ou() + ")"
//                        + "  AND LEVEL =1"
//                        + "  AND URL IS NOT NULL"
//                        + "  START WITH PARENTID IS NULL"
//                        + "  CONNECT BY PRIOR  MENUID = PARENTID"
//                        + "  ORDER BY MENUDESC";
//                rs = stmt.executeQuery(q_find_menu);
//                lst_SystMenumsts.clear();
//                try {
//
//                    while (rs.next()) {
//                        SystMenumst systMenumst = new SystMenumst();
//                        systMenumst.setMenudesc(rs.getString("menudesc"));
//                        systMenumst.setUrl(rs.getString("url"));
//                        lst_SystMenumsts.add(systMenumst);
//                    }
//
//                } finally {
//                    rs.close();
//                }
//            } finally {
//                stmt.close();
//            }
//        } finally {
//            cn.close();
//        }
//    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Theme Setting">
    //<editor-fold defaultstate="collapsed" desc="changeTheme">

    public void changeTheme(ActionEvent event) {
        getGlobalResources().ChangeTheme(selectedTheme);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="selectedTheme">
    String selectedTheme = null;

    public String getSelectedTheme() {
        return selectedTheme;
    }

    public void setSelectedTheme(String selectedTheme) {
        this.selectedTheme = selectedTheme;
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Font Setting">
    String font_size = null;

    public String getFont_size() {
        return font_size;
    }

    public void setFont_size(String font_size) {
        this.font_size = font_size;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="PostConstruct">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setDdl_organisation_unit();
        setMenuModel();
        user_name = globalData.getEnt_login_user().getUserName();
        font_size = "11";
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Default Constructor & Other Bean">
    // <editor-fold defaultstate="collapsed" desc="Default Constructor">

    public Template() {
        setResources();
    }

    private void setResources() {
        getGlobalResources().setUserFromGlobalSettings(getGlobalSettings().getEnt_login_user());
        //String user_theme=ent_login_user.getUserTheme();
        String user_theme = null;
        if (user_theme != null) {
            getGlobalResources().ChangeTheme(user_theme);
        }
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="getGlobalUtilities">
    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>  
    // <editor-fold defaultstate="collapsed" desc="getGlobalResources">

    protected GlobalResources getGlobalResources() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalResources) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalResources");
    }// </editor-fold>  
    // <editor-fold defaultstate="collapsed" desc="getGlobalSettings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="getPIS_GlobalSettings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Sign Out">

    public String Logout(ActionEvent event) throws IOException {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if (session != null) {
            session.invalidate();
        }
        FacesContext.getCurrentInstance().getExternalContext().redirect("/PIS/Logout");
        return null;

    }// </editor-fold>
}
