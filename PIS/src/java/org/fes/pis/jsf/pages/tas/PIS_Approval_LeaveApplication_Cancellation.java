package org.fes.pis.jsf.pages.tas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.customJpaController;
import org.fes.pis.entities.FtasAbsenteesDtl;
import org.fes.pis.entities.FtasCancelabs;
import org.fes.pis.entities.FtasLeavemst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.jsf.selectableDataModels.SD_FtasCancelabs;
import org.fes.pis.sessions.FtasAbsenteesDtlFacadeLocal;
import org.fes.pis.sessions.FtasCancelabsFacadeLocal;
import org.fes.pis.sessions.FtasLeavemstFacadeLocal;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Approval_LeaveApplication_Cancellation")
@ViewScoped
public class PIS_Approval_LeaveApplication_Cancellation implements Serializable {

    @EJB
    private customJpaController customJpaController;
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="EJB/OTHER Declaration">
    @EJB
    private FtasLeavemstFacadeLocal ftasLeavemstFacade;
    @EJB
    private FtasAbsenteesDtlFacadeLocal ftasAbsenteesDtlFacade;
    @EJB
    private FtasCancelabsFacadeLocal ftasCancelabsFacade;
    System_Properties system_Properties = new System_Properties();
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="constructor">

    /**
     * Creates a new instance of PIS_Approval_LeaveApplication_Cancellation
     */
    public PIS_Approval_LeaveApplication_Cancellation() {
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_cabs_appl"> 
    public SD_FtasCancelabs tbl_cabs_appl;
    List<FtasCancelabs> tbl_cabs_appl_filtered;

    public SD_FtasCancelabs getTbl_cabs_appl() {
        return tbl_cabs_appl;
    }

    public void setTbl_cabs_appl(SD_FtasCancelabs tbl_cabs_appl) {
        this.tbl_cabs_appl = tbl_cabs_appl;
    }

    public List<FtasCancelabs> getTbl_cabs_appl_filtered() {
        return tbl_cabs_appl_filtered;
    }

    public void setTbl_cabs_appl_filtered(List<FtasCancelabs> tbl_cabs_appl_filtered) {
        this.tbl_cabs_appl_filtered = tbl_cabs_appl_filtered;
    }

    public void setTbl_cabs_appl() {
        tbl_cabs_appl = new SD_FtasCancelabs(ftasCancelabsFacade.findAll_ApprovalList(globalData.getUser_id()));
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="view detail">
    public void btn_view_detail_action(ActionEvent event) {
    }
    FtasCancelabs entFtasCancelabs_detail;

    public FtasCancelabs getEntFtasCancelabs_detail() {
        return entFtasCancelabs_detail;
    }

    public void setEntFtasCancelabs_detail(FtasCancelabs entFtasCancelabs_detail) {
        int v_emp_userid;
        int v_oum_unit_srno;
        int v_srno;
        v_emp_userid = entFtasCancelabs_detail.getUserId().getUserId();
        v_srno = entFtasCancelabs_detail.getSrno();
        v_oum_unit_srno = entFtasCancelabs_detail.getOumUnitSrno();
        lbl_txt_emp_userid = String.valueOf(v_emp_userid);
        tbl_appli_detail_list = ftasAbsenteesDtlFacade.find_ApprovalListDtl(entFtasCancelabs_detail.getAbsSrgKey().getAbsSrgKey());
        Integer n = tbl_appli_detail_list.size();
        for (int i = 0; i < n; i++) {
            if (tbl_appli_detail_list.get(i).getFromhalf().equalsIgnoreCase("0") == true) {
                tbl_appli_detail_list.get(i).setFromhalf("Full Day");
            } else if (tbl_appli_detail_list.get(i).getFromhalf().equalsIgnoreCase("1") == true) {
                tbl_appli_detail_list.get(i).setFromhalf("Half-1");
            } else if (tbl_appli_detail_list.get(i).getFromhalf().equalsIgnoreCase("2") == true) {
                tbl_appli_detail_list.get(i).setFromhalf("Half-2");
            } else if (tbl_appli_detail_list.get(i).getFromhalf().equalsIgnoreCase("3") == true) {
                tbl_appli_detail_list.get(i).setFromhalf("Before Office Hrs");
            } else if (tbl_appli_detail_list.get(i).getFromhalf().equalsIgnoreCase("4") == true) {
                tbl_appli_detail_list.get(i).setFromhalf("After Office Hrs");
            }
            if (tbl_appli_detail_list.get(i).getTohalf().equalsIgnoreCase("0") == true) {
                tbl_appli_detail_list.get(i).setTohalf("Full Day");
            } else if (tbl_appli_detail_list.get(i).getTohalf().equalsIgnoreCase("1") == true) {
                tbl_appli_detail_list.get(i).setTohalf("Half-1");
            } else if (tbl_appli_detail_list.get(i).getTohalf().equalsIgnoreCase("2") == true) {
                tbl_appli_detail_list.get(i).setTohalf("Half-2");
            } else if (tbl_appli_detail_list.get(i).getTohalf().equalsIgnoreCase("3") == true) {
                tbl_appli_detail_list.get(i).setTohalf("Before Ofiice Hrs");
            } else if (tbl_appli_detail_list.get(i).getTohalf().equalsIgnoreCase("4") == true) {
                tbl_appli_detail_list.get(i).setTohalf("After Office Hrs");
            }
            if (tbl_appli_detail_list.get(i).getLeaveBalLaps() == null) {
                tbl_appli_detail_list.get(i).setLeaveBalLaps(BigDecimal.ZERO);
            }
            FtasLeavemst entFtasLeavemst = ftasLeavemstFacade.findLeaveCode_OrgWise(tbl_appli_detail_list.get(i).getLeavecd().toString(), v_oum_unit_srno);
            tbl_appli_detail_list.get(i).setLeavecd(entFtasLeavemst.getLeaveDesc());
            lbl_txt_emp_username = tbl_appli_detail_list.get(i).getUserId().getUserName();
        }
        lbl_dtl_txt_srno = String.valueOf(v_srno);
        this.entFtasCancelabs_detail = entFtasCancelabs_detail;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="selectedApplications"> 
    FtasCancelabs[] selectedApplications;

    public FtasCancelabs[] getSelectedApplications() {
        return selectedApplications;
    }

    public void setSelectedApplications(FtasCancelabs[] selectedApplications) {
        this.selectedApplications = selectedApplications;
    }
    //</editor-fold>

    public void btn_approve_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (selectedApplications.length > 0) {
            try {
                boolean isTransactionDone = customJpaController.ApproveRejectCancelAppls(selectedApplications, "Y", globalData.getUser_id());
                if (isTransactionDone) {
                    try {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Application Approved", "Selected Application Approved Successfully."));
                        setTbl_cabs_appl();
                        entFtasCancelabs_detail = null;
                    } catch (Exception e) {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed reset data", "Selected Application Approved Successfully but failed reset data. Please reload the page"));
                        LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_accept_clicked", null, e);
                    }
                } else {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during Accepting Application."));
                }
            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during Accepting Application."));
                LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_accept_clicked", null, e);
            }
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Select Application", "Please select any Application to Approve/Reject."));
        }
    }

    public void btn_reject_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (selectedApplications.length > 0) {
            try {
                boolean isTransactionDone = customJpaController.ApproveRejectCancelAppls(selectedApplications, "N", globalData.getUser_id());
                if (isTransactionDone) {
                    try {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Application Rejected", "Selected Application Rejected Successfully."));
                        setTbl_cabs_appl();
                        entFtasCancelabs_detail = null;
                    } catch (Exception e) {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed reset data", "Selected Application Rejected Successfully but failed reset data. Please reload the page"));
                        LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_reject_clicked", null, e);
                    }
                } else {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during Rejecting Application."));
                }
            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during Rejecting Application."));
                LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_reject_clicked", null, e);
            }
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Select Application", "Please select any Application to Approve/Reject."));
        }
    }
    //<editor-fold defaultstate="collapsed" desc="Detail View">
    String lbl_txt_emp_userid;
    String lbl_txt_emp_username;
    String lbl_dtl_txt_srno;

    public String getLbl_dtl_txt_srno() {
        return lbl_dtl_txt_srno;
    }

    public void setLbl_dtl_txt_srno(String lbl_dtl_txt_srno) {
        this.lbl_dtl_txt_srno = lbl_dtl_txt_srno;
    }

    public String getLbl_txt_emp_userid() {
        return lbl_txt_emp_userid;
    }

    public void setLbl_txt_emp_userid(String lbl_txt_emp_userid) {
        this.lbl_txt_emp_userid = lbl_txt_emp_userid;
    }

    public String getLbl_txt_emp_username() {
        return lbl_txt_emp_username;
    }

    public void setLbl_txt_emp_username(String lbl_txt_emp_username) {
        this.lbl_txt_emp_username = lbl_txt_emp_username;
    }
    //</editor-fold>
    List<FtasAbsenteesDtl> tbl_appli_detail_list;

    public List<FtasAbsenteesDtl> getTbl_appli_detail_list() {
        return tbl_appli_detail_list;
    }

    public void setTbl_appli_detail_list(List<FtasAbsenteesDtl> tbl_appli_detail_list) {
        this.tbl_appli_detail_list = tbl_appli_detail_list;
    }

    //Global Functions
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Utilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="init">
    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setTbl_cabs_appl();
    }
    //</editor-fold>
}
