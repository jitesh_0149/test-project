/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.pis.emp_info;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.customJpaController;
import org.fes.pis.entities.FhrdEmpEventDtl;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.FhrdEventMst;
import org.fes.pis.jsf.GlobalResources;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEmpEventDtlFacadeLocal;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;
import org.fes.pis.sessions.FhrdEventMstFacadeLocal;
import org.primefaces.context.RequestContext;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Set_Employee_Event")
@ViewScoped
public class PIS_Set_Employee_Event implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="EJB Declarations">
    @EJB
    private customJpaController customJpaController;
    @EJB
    private FhrdEventMstFacadeLocal fhrdEventMstFacade;
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    @EJB
    private FhrdEmpEventDtlFacadeLocal fhrdEmpEventDtlFacade;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="default">

    /**
     * Creates a new instance of PIS_Set_Emp_Event
     */
    public PIS_Set_Employee_Event() {
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="globalData and other declarations">

    
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold> 
///////////////Employee Table
    //<editor-fold defaultstate="collapsed" desc="tbl_emp_info"> 
    List<FhrdEmpmst> tbl_emp_info;

    public List<FhrdEmpmst> getTbl_emp_info() {
        return tbl_emp_info;
    }

    public void setTbl_emp_info(List<FhrdEmpmst> tbl_emp_info) {
        this.tbl_emp_info = tbl_emp_info;
    }

    public void setTblEmpInfo() {
        tbl_emp_info = fhrdEmpmstFacade.findAllEmployeeForContract(globalData.getWorking_ou(), getPIS_GlobalSettings().getSysetemUser_Fhrd() ? null : globalData.getUser_id());
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_emp_info_filtered"> 
    List<FhrdEmpmst> tbl_emp_info_filtered;

    public List<FhrdEmpmst> getTbl_emp_info_filtered() {
        return tbl_emp_info_filtered;
    }

    public void setTbl_emp_info_filtered(List<FhrdEmpmst> tbl_emp_info_filtered) {
        this.tbl_emp_info_filtered = tbl_emp_info_filtered;
    }
    //</editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="ent_empmst_detail"> 
    FhrdEmpmst ent_empmst_detail;

    public FhrdEmpmst getEnt_empmst_detail() {
        return ent_empmst_detail;
    }

    public void setEnt_empmst_detail(FhrdEmpmst ent_empmst_detail) {
        tbl_view_event = fhrdEmpEventDtlFacade.findAllEmpwise(ent_empmst_detail.getUserId());
        this.ent_empmst_detail = ent_empmst_detail;
    }
    //</editor-fold>  
//// view employee event detail
    //<editor-fold defaultstate="collapsed" desc="tbl_view_event">
    List<FhrdEmpEventDtl> tbl_view_event;

    public List<FhrdEmpEventDtl> getTbl_view_event() {
        return tbl_view_event;
    }

    public void setTbl_view_event(List<FhrdEmpEventDtl> tbl_view_event) {
        this.tbl_view_event = tbl_view_event;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ent_FhrdEmpEvent_detail"> 
    FhrdEmpEventDtl ent_FhrdEmpEvent_detail;

    public FhrdEmpEventDtl getEnt_FhrdEmpEvent_detail() {
        return ent_FhrdEmpEvent_detail;
    }

    public void setEnt_FhrdEmpEvent_detail(FhrdEmpEventDtl ent_FhrdEmpEvent_detail) {
        this.ent_FhrdEmpEvent_detail = ent_FhrdEmpEvent_detail;
    }
    //</editor-fold>
//// Set new Event to employee
    //<editor-fold defaultstate="collapsed" desc="entEmpmst_setEvent"> 
    FhrdEmpmst entEmpmst_setEvent;

    public FhrdEmpmst getEntEmpmst_setEvent() {
        return entEmpmst_setEvent;
    }

    public void setEntEmpmst_setEvent(FhrdEmpmst entEmpmst_setEvent) {
        this.entEmpmst_setEvent = entEmpmst_setEvent;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="entEventMst_selected"> 
    FhrdEventMst entEventMst_selected;

    public FhrdEventMst getEntEventMst_selected() {
        return entEventMst_selected;
    }

    public void setEntEventMst_selected(FhrdEventMst entEventMst_selected) {
        this.entEventMst_selected = entEventMst_selected;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_event"> 
    List<SelectItem> ddl_event_option;
    String ddl_event;

    public String getDdl_event() {
        return ddl_event;
    }

    public void setDdl_event(String ddl_event) {
        this.ddl_event = ddl_event;
    }

    public List<SelectItem> getDdl_event_option() {
        return ddl_event_option;
    }

    public void setDdl_event_option(List<SelectItem> ddl_event_option) {
        this.ddl_event_option = ddl_event_option;
    }

    public void setDdl_event() {
        ddl_event_option = new ArrayList<SelectItem>();
        List<FhrdEventMst> lstEventMsts = fhrdEventMstFacade.findAll(globalData.getWorking_ou());
        if (lstEventMsts.isEmpty()) {
            SelectItem s = new SelectItem(null, "--- Not Available ---");
            ddl_event_option.add(s);
        } else {
            SelectItem s = new SelectItem(null, "--- Select ---");
            ddl_event_option.add(s);
            Integer n = lstEventMsts.size();
            for (int i = 0; i < n; i++) {
                s = new SelectItem(lstEventMsts.get(i).getEmSrgKey(), lstEventMsts.get(i).getEventDesc());
                ddl_event_option.add(s);
            }
        }
        ddl_event = null;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ddl_event_changed">
    public void ddl_event_changed(ValueChangeEvent event) {
        hdn_period_required = "N";
        if (event.getNewValue() != null) {
            ddl_event = event.getNewValue().toString();
            FhrdEventMst entEventMst = fhrdEventMstFacade.find(new BigDecimal(ddl_event));
            if (entEventMst.getEventPeriodRequiredFlag().equalsIgnoreCase("Y")) {
                hdn_period_required = "Y";
            }
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_new_event_list"> 
    List<FhrdEmpEventDtl> tbl_new_event_list;

    public List<FhrdEmpEventDtl> getTbl_new_event_list() {
        return tbl_new_event_list;
    }

    public void setTbl_new_event_list(List<FhrdEmpEventDtl> tbl_new_event_list) {
        this.tbl_new_event_list = tbl_new_event_list;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_event_date"> 
    Date cal_event_date;

    public Date getCal_event_date() {
        return cal_event_date;
    }

    public void setCal_event_date(Date cal_event_date) {
        this.cal_event_date = cal_event_date;
    }
    //</editor-fold>
//// Add delete event
    //<editor-fold defaultstate="collapsed" desc="txt_event_period"> 
    String txt_event_period;

    public String getTxt_event_period() {
        return txt_event_period;
    }

    public void setTxt_event_period(String txt_event_period) {
        this.txt_event_period = txt_event_period;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_event_period"> 
    String ddl_event_period_unit;

    public String getDdl_event_period_unit() {
        return ddl_event_period_unit;
    }

    public void setDdl_event_period_unit(String ddl_event_period_unit) {
        this.ddl_event_period_unit = ddl_event_period_unit;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_doc_ref_no"> 
    String txt_doc_ref_no;

    public String getTxt_doc_ref_no() {
        return txt_doc_ref_no;
    }

    public void setTxt_doc_ref_no(String txt_doc_ref_no) {
        this.txt_doc_ref_no = txt_doc_ref_no;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="hdn_period_required"> 
    String hdn_period_required;

    public String getHdn_period_required() {
        return hdn_period_required;
    }

    public void setHdn_period_required(String hdn_period_required) {
        this.hdn_period_required = hdn_period_required;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_event_effect_time"> 
    String ddl_event_effect_time;

    public String getDdl_event_effect_time() {
        return ddl_event_effect_time;
    }

    public void setDdl_event_effect_time(String ddl_event_effect_time) {
        this.ddl_event_effect_time = ddl_event_effect_time;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_add_to_list_clicked"> 

    public void btn_add_to_list_clicked(ActionEvent event) {
        List<FhrdEmpEventDtl> lstEventMsts = new ArrayList<FhrdEmpEventDtl>();
        if (tbl_new_event_list != null) {
            lstEventMsts = (List<FhrdEmpEventDtl>) tbl_new_event_list;
        }
//        FhrdEventMst entEventMst = fhrdEventMstFacade.find(new BigDecimal(ddl_event));
        FhrdEmpEventDtl ent_FhrdEventMst = new FhrdEmpEventDtl();
        ent_FhrdEventMst.setEmSrgKey(entEventMst_selected);
        ent_FhrdEventMst.setUserId(ent_empmst_detail);
        ent_FhrdEventMst.setEventDate(cal_event_date);
        if (!txt_event_period.isEmpty()) {
            ent_FhrdEventMst.setEventPeriodNo(Integer.valueOf(txt_event_period));
            ent_FhrdEventMst.setEventPeriodUnit(ddl_event_period_unit);
        }
        if (!txt_doc_ref_no.isEmpty()) {
            ent_FhrdEventMst.setEventDocRefNo(txt_doc_ref_no);
        }
        ent_FhrdEventMst.setEventEffectTime(ddl_event_effect_time);
        lstEventMsts.add(ent_FhrdEventMst);

        tbl_new_event_list = lstEventMsts;
        cal_event_date = null;
        ddl_event = null;
        ddl_event_period_unit = null;
        txt_doc_ref_no = null;
        txt_event_period = null;
        ddl_event_effect_time = null;
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="ent_EventDelete"> 
    FhrdEmpEventDtl ent_EventDelete;

    public FhrdEmpEventDtl getEnt_EventDelete() {
        return ent_EventDelete;
    }

    public void setEnt_EventDelete(FhrdEmpEventDtl ent_EventDelete) {
        List<FhrdEmpEventDtl> lst_List = tbl_new_event_list;
        lst_List.remove(ent_EventDelete);
        this.ent_EventDelete = ent_EventDelete;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_save_emp_event_clicked">

    public void btn_save_emp_event_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        List<FhrdEmpEventDtl> lstFhrdEmpEventDtls = new ArrayList<FhrdEmpEventDtl>();
        for (int i = 0; i < tbl_new_event_list.size(); i++) {
            FhrdEmpmst ent_FhrdEmpmst = fhrdEmpmstFacade.find(tbl_new_event_list.get(i).getUserId().getUserId());
            FhrdEventMst entEventMst = fhrdEventMstFacade.find(tbl_new_event_list.get(i).getEmSrgKey().getEmSrgKey());
            FhrdEmpEventDtl entFhrdEmpEventDtl = new FhrdEmpEventDtl();
            entFhrdEmpEventDtl.setEmpedSrgKey(new BigDecimal("-1"));
            entFhrdEmpEventDtl.setSrno(-1);
            entFhrdEmpEventDtl.setCreateby(globalData.getEnt_login_user());
            entFhrdEmpEventDtl.setCreatedt(new Date());
            entFhrdEmpEventDtl.setEventDate(tbl_new_event_list.get(i).getEventDate());
            entFhrdEmpEventDtl.setEventPeriodNo(tbl_new_event_list.get(i).getEventPeriodNo());
            entFhrdEmpEventDtl.setEventPeriodUnit(tbl_new_event_list.get(i).getEventPeriodUnit());
            entFhrdEmpEventDtl.setEmSrgKey(entEventMst);
            entFhrdEmpEventDtl.setOumUnitSrno(ent_FhrdEmpmst.getPostingOumUnitSrno().getOumUnitSrno());
            entFhrdEmpEventDtl.setUserId(ent_FhrdEmpmst);
            entFhrdEmpEventDtl.setEventEffectTime(entEventMst.getEventEffectTime());
            lstFhrdEmpEventDtls.add(entFhrdEmpEventDtl);
        }
        try {
            boolean isSuccess = customJpaController.createEmpEvent(lstFhrdEmpEventDtls);
            resetEmpEvent();
            if (isSuccess) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Employee event(s) saved Successfully."));

            } else {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Record not saved successfully.Please check all the entries."));
            }
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Record not saved successfully.Please check all the entries."));
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_save_emp_event_clicked", null, e);
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetEmpEvent() "> 

    public void resetEmpEvent() {
        RequestContext.getCurrentInstance().execute("pw_dlg_set_new_event.hide()");
        txt_doc_ref_no = null;
        txt_event_period = null;
        ddl_event = null;
        ddl_event_effect_time = null;
        ddl_event_period_unit = null;
        tbl_new_event_list = null;
        tbl_view_event = null;
    }
    //</editor-fold>
//// other methods    
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalResources">

    protected GlobalResources getGlobalResources() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalResources) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalResources");
    }// </editor-fold>
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setTblEmpInfo();
        setDdl_event();
    }
    //</editor-fold>
}
