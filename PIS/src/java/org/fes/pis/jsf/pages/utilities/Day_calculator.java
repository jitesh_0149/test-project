/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.utilities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;

/**
 *
 * @author jitesh
 */
@ManagedBean(name = "Day_calculator")
@ViewScoped
public class Day_calculator implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="EJB and other declarations"> 
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>  
    //<editor-fold defaultstate="collapsed" desc="Type 1">
    //<editor-fold defaultstate="collapsed" desc="txt_1">
    String txt_1;

    public String getTxt_1() {
        return txt_1;
    }

    public void setTxt_1(String txt_1) {
        this.txt_1 = txt_1;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_from_date_after">
    Date cal_from_date_after;

    public Date getCal_from_date_after() {
        return cal_from_date_after;
    }

    public void setCal_from_date_after(Date cal_from_date_after) {
        this.cal_from_date_after = cal_from_date_after;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_year">
    String txt_year;

    public String getTxt_year() {
        return txt_year;
    }

    public void setTxt_year(String txt_year) {
        this.txt_year = txt_year;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_month">
    String txt_month;

    public String getTxt_month() {
        return txt_month;
    }

    public void setTxt_month(String txt_month) {
        this.txt_month = txt_month;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_days">
    String txt_days;

    public String getTxt_days() {
        return txt_days;
    }

    public void setTxt_days(String txt_days) {
        this.txt_days = txt_days;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_calculate">
    public void btn_calculate(ActionEvent event) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(cal_from_date_after);
        if (!txt_year.isEmpty()) {
            cal.add(Calendar.YEAR, Integer.valueOf(txt_year));
        }
        if (!txt_month.isEmpty()) {
            cal.add(Calendar.MONTH, Integer.valueOf(txt_month));
        }
        if (!txt_days.isEmpty()) {
            cal.add(Calendar.DATE, Integer.valueOf(txt_days));
        }
        Date nextYear = cal.getTime();
        txt_1 = DateTimeUtility.ChangeDateFormat(nextYear, null);
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Type 2">
    //<editor-fold defaultstate="collapsed" desc="rdb_selection">
    String rdb_selection;

    public String getRdb_selection() {
        return rdb_selection;
    }

    public void setRdb_selection(String rdb_selection) {
        this.rdb_selection = rdb_selection;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal from to dates">
    Date cal_from_date;
    Date cal_to_date;
    String cal_to_date_min;

    public String getCal_to_date_min() {
        return cal_to_date_min;
    }

    public void setCal_to_date_min(String cal_to_date_min) {
        this.cal_to_date_min = cal_to_date_min;
    }

    public Date getCal_from_date() {
        return cal_from_date;
    }

    public void setCal_from_date(Date cal_from_date) {
        this.cal_from_date = cal_from_date;
    }

    public Date getCal_to_date() {
        return cal_to_date;
    }

    public void setCal_to_date(Date cal_to_date) {
        this.cal_to_date = cal_to_date;
    }

    public void cal_from_date_changed(ValueChangeEvent event) {
        cal_from_date = event.getNewValue() == null ? null : (Date) event.getNewValue();
        cal_to_date_min = cal_from_date == null ? null : DateTimeUtility.ChangeDateFormat(cal_from_date, null);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_period">
    String txt_period;

    public String getTxt_period() {
        return txt_period;
    }

    public void setTxt_period(String txt_period) {
        this.txt_period = txt_period;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_calculate_period_clicked">

    public void btn_calculate_period_clicked(ActionEvent event) {
        long test = DateTimeUtility.findDuration(cal_from_date, cal_to_date, "D");
        txt_period = String.valueOf(test + 1);
        cal_to_date_min = DateTimeUtility.ChangeDateFormat(cal_from_date, null);
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        cal_from_date_after = new Date();
        cal_from_date = new Date();
        cal_to_date = new Date();
        cal_to_date_min = DateTimeUtility.ChangeDateFormat(cal_from_date, null);
        rdb_selection = "D";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Default Constructor and Other Global Methods">

    /**
     * Creates a new instance of PIS_View_Resign_Relieve
     */
    public Day_calculator() {
    }

    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Utilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    //</editor-fold>
}
