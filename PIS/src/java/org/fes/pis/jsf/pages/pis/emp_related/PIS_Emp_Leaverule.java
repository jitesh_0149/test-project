/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.pis.emp_related;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.FhrdEmpleavebal;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEmpleaveruleFacadeLocal;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Emp_Leaverule")
@ViewScoped
public class PIS_Emp_Leaverule implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="EJB Declarations">
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    @EJB
    private FhrdEmpleaveruleFacadeLocal fhrdEmpleaveruleFacade;
    Ou_Pack ou_Pack = new Ou_Pack();
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="Default">

    /**
     * Creates a new instance of PIS_Emp_Leaverule
     */
    public PIS_Emp_Leaverule() {
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="getter/setter">
    String txt_user_id;
    String lbl_username;
    String lbl_empnm_err;
    boolean hdn_valid_userid;

    public boolean isHdn_valid_userid() {
        return hdn_valid_userid;
    }

    public void setHdn_valid_userid(boolean hdn_valid_userid) {
        this.hdn_valid_userid = hdn_valid_userid;
    }

    public String getLbl_empnm_err() {
        return lbl_empnm_err;
    }

    public void setLbl_empnm_err(String lbl_empnm_err) {
        this.lbl_empnm_err = lbl_empnm_err;
    }

    public String getLbl_username() {
        return lbl_username;
    }

    public void setLbl_username(String lbl_username) {
        this.lbl_username = lbl_username;
    }

    public String getTxt_user_id() {
        return txt_user_id;
    }

    public void setTxt_user_id(String txt_user_id) {
        this.txt_user_id = txt_user_id;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="txt_userid_processvaluechange event">

    public void txt_user_id_processValueChange(ValueChangeEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        txt_user_id = event.getNewValue().toString();
        lbl_username = "";
        lbl_empnm_err = "";
        hdn_valid_userid = false;
        if (txt_user_id.equals("") == true) {
            hdn_valid_userid = false;
        } else {
            FhrdEmpmst entEmpmst1 = getEmployeeName(Integer.valueOf(event.getNewValue().toString()));
            if (entEmpmst1 != null) {
                lbl_username = entEmpmst1.getUserName();
                hdn_valid_userid = true;
                setTbl_emp_leaverule(entEmpmst1.getUserId(), entEmpmst1.getPostingOumUnitSrno().getOumUnitSrno());
            } else {
                hdn_valid_userid = false;
                lbl_username = "";
                lbl_empnm_err = "User Does Not Exist";
            }
        }

    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="getEmployeeName">

    public FhrdEmpmst getEmployeeName(Integer p_userid) {
        FhrdEmpmst entEmpmst = fhrdEmpmstFacade.find(p_userid);
        return entEmpmst;
    }// </editor-fold>     
    //<editor-fold defaultstate="collapsed" desc="tbl_emp_leaverule"> 
    List<FhrdEmpleavebal> tbl_emp_leaverule;

    public List<FhrdEmpleavebal> getTbl_emp_leaverule() {
        return tbl_emp_leaverule;
    }

    public void setTbl_emp_leaverule(List<FhrdEmpleavebal> tbl_emp_leaverule) {
        this.tbl_emp_leaverule = tbl_emp_leaverule;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="setTbl_emp_leaverule"> 

    public void setTbl_emp_leaverule(Integer p_userid, Integer p_oum_unit_srno) {
        if (p_userid == null) {
            tbl_emp_leaverule = null;
        } else {
            tbl_emp_leaverule = fhrdEmpleaveruleFacade.getLeaveRuleEmpwise(p_userid, p_oum_unit_srno, null, null, null);
        }
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="btn_view_detail_click"> 

    public void btn_view_detail_click(ActionEvent event) {
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="ent_view_empleaverule"> 
    FhrdEmpleavebal ent_view_empleaverule;

    public FhrdEmpleavebal getEnt_view_empleaverule() {
        return ent_view_empleaverule;
    }

    public void setEnt_view_empleaverule(FhrdEmpleavebal ent_view_empleaverule) {
        this.ent_view_empleaverule = ent_view_empleaverule;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
}
