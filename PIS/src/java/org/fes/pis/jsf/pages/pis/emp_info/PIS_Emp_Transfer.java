/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.pis.emp_info;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.FhrdEmpContractMst;
import org.fes.pis.entities.FhrdEmpeventTransfer;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.SystOrgUnitMst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.*;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Emp_Transfer")
@ViewScoped
public class PIS_Emp_Transfer implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="EJB Declarations">
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    @EJB
    private FhrdEmpeventTransferFacadeLocal fhrdEmpeventTransferFacade;
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    @EJB
    private SystOrgUnitMstFacadeLocal systOrgUnitMstFacade;
    @EJB
    private FhrdEventMstFacadeLocal fhrdEventMstFacade;
    @EJB
    private FhrdEmpContractMstFacadeLocal fhrdEmpContractMstFacade;
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="UserId Selection">
    String txt_user_id;
    String txt_username;
    String err_txt_user_id;
    boolean userSelectionValid;
    FhrdEmpmst selectedUser;

    public boolean isUserSelectionValid() {
        return userSelectionValid;
    }

    public void setUserSelectionValid(boolean userSelectionValid) {
        this.userSelectionValid = userSelectionValid;
    }

    public String getErr_txt_user_id() {
        return err_txt_user_id;
    }

    public void setErr_txt_user_id(String err_txt_user_id) {
        this.err_txt_user_id = err_txt_user_id;
    }

    public String getTxt_username() {
        return txt_username;
    }

    public void setTxt_username(String txt_username) {
        this.txt_username = txt_username;
    }

    public String getTxt_user_id() {
        return txt_user_id;
    }

    public void setTxt_user_id(String txt_user_id) {
        this.txt_user_id = txt_user_id;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="txt_userid_processvaluechange event">

    public void txt_user_id_processValueChange(ValueChangeEvent event) {
        txt_user_id = event.getNewValue().toString();
        txt_username = "";
        err_txt_user_id = "";
        txt_from_oum = "";
        txt_from_oum_unit_srno = null;
        txt_current_rep_userid = null;
        txt_current_rep_userid_name = "";
        userSelectionValid = false;
        if (txt_user_id.equals("") == true) {
            userSelectionValid = false;
            err_txt_user_id = "Enter UserID";
        } else {
            selectedUser = fhrdEmpmstFacade.find(Integer.valueOf(event.getNewValue().toString()));
            if (selectedUser != null) {
                if (selectedUser.getOrgRelieveDate() != null || selectedUser.getTerminationDate() != null) {
                    err_txt_user_id = "Employee already relieved/terminated";
                    userSelectionValid = false;
                } else {
                    userSelectionValid = true;
                    txt_username = selectedUser.getUserName();
                    txt_from_oum = selectedUser.getPostingOumUnitSrno().getOumName();
                    txt_from_oum_unit_srno = selectedUser.getPostingOumUnitSrno().getOumUnitSrno();
                    txt_current_rep_userid = selectedUser.getReportingUserId().getUserId();
                    txt_current_rep_userid_name = selectedUser.getReportingUserId().getUserName();
                }
            } else {
                userSelectionValid = false;
                txt_username = "";
                err_txt_user_id = "User Does Not Exist";
            }
        }
        setTransfer_date();

    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_remarks"> 
    String txt_remarks;

    public String getTxt_remarks() {
        return txt_remarks;
    }

    public void setTxt_remarks(String txt_remarks) {
        this.txt_remarks = txt_remarks;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_from_oum"> 
    String txt_from_oum;
    Integer txt_from_oum_unit_srno;

    public Integer getTxt_from_oum_unit_srno() {
        return txt_from_oum_unit_srno;
    }

    public void setTxt_from_oum_unit_srno(Integer txt_from_oum_unit_srno) {
        this.txt_from_oum_unit_srno = txt_from_oum_unit_srno;
    }

    public String getTxt_from_oum() {
        return txt_from_oum;
    }

    public void setTxt_from_oum(String txt_from_oum) {
        this.txt_from_oum = txt_from_oum;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_to_oum">
    String ddl_to_oum;
    List<SelectItem> ddl_to_oum_options;

    public String getDdl_to_oum() {
        return ddl_to_oum;
    }

    public void setDdl_to_oum(String ddl_to_oum) {
        this.ddl_to_oum = ddl_to_oum;
    }

    public List<SelectItem> getDdl_to_oum_options() {
        return ddl_to_oum_options;
    }

    public void setDdl_to_oum_options(List<SelectItem> ddl_to_oum_options) {
        this.ddl_to_oum_options = ddl_to_oum_options;
    }

    public void setDdl_to_oum() {
        ddl_to_oum_options = new ArrayList<SelectItem>();
        List<SystOrgUnitMst> lst = systOrgUnitMstFacade.findPostingOuForNewEmployee(globalData.getWorking_ou());
        Integer n = lst.size();
        if (lst.isEmpty()) {
            ddl_to_oum_options.add(new SelectItem(null, "---No Data Found---"));
        } else {
            ddl_to_oum_options.add(new SelectItem(null, "---Select---"));
            for (int i = 0; i < n; i++) {
                ddl_to_oum_options.add(new SelectItem(lst.get(i).getOumUnitSrno(), lst.get(i).getOumName()));
            }
        }
        ddl_to_oum = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_current_rep_userid">
    Integer txt_current_rep_userid;
    String txt_current_rep_userid_name;

    public Integer getTxt_current_rep_userid() {
        return txt_current_rep_userid;
    }

    public void setTxt_current_rep_userid(Integer txt_current_rep_userid) {
        this.txt_current_rep_userid = txt_current_rep_userid;
    }

    public String getTxt_current_rep_userid_name() {
        return txt_current_rep_userid_name;
    }

    public void setTxt_current_rep_userid_name(String txt_current_rep_userid_name) {
        this.txt_current_rep_userid_name = txt_current_rep_userid_name;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_new_rep_userid">
    String ddl_new_rep_userid;
    List<SelectItem> ddl_new_rep_userid_options;

    public String getDdl_new_rep_userid() {
        return ddl_new_rep_userid;
    }

    public void setDdl_new_rep_userid(String ddl_new_rep_userid) {
        this.ddl_new_rep_userid = ddl_new_rep_userid;
    }

    public List<SelectItem> getDdl_new_rep_userid_options() {
        return ddl_new_rep_userid_options;
    }

    public void setDdl_new_rep_userid_options(List<SelectItem> ddl_new_rep_userid_options) {
        this.ddl_new_rep_userid_options = ddl_new_rep_userid_options;
    }

    public void setDdl_new_rep_userid_options() {
        ddl_new_rep_userid_options = new ArrayList<SelectItem>();
        List<FhrdEmpmst> lst = fhrdEmpmstFacade.findAll(globalData.getWorking_ou(), false, false, true, null, null, null, false, false, false, null);
        Integer n = lst.size();
        if (lst.isEmpty()) {
            ddl_new_rep_userid_options.add(new SelectItem(null, "---No Data Found---"));
        } else {
            ddl_new_rep_userid_options.add(new SelectItem(null, "---Select---"));
            for (int i = 0; i < n; i++) {
                ddl_new_rep_userid_options.add(new SelectItem(lst.get(i).getUserId().toString(), lst.get(i).getUserName()));
            }
        }
        ddl_new_rep_userid = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_new_ou_joining_date"> 
    Date cal_new_ou_joining_date;
    String cal_new_ou_joining_date_min;
    String cal_new_ou_joining_date_max;

    public Date getCal_new_ou_joining_date() {
        return cal_new_ou_joining_date;
    }

    public void setCal_new_ou_joining_date(Date cal_new_ou_joining_date) {
        this.cal_new_ou_joining_date = cal_new_ou_joining_date;
    }

    public String getCal_new_ou_joining_date_max() {
        return cal_new_ou_joining_date_max;
    }

    public void setCal_new_ou_joining_date_max(String cal_new_ou_joining_date_max) {
        this.cal_new_ou_joining_date_max = cal_new_ou_joining_date_max;
    }

    public String getCal_new_ou_joining_date_min() {
        return cal_new_ou_joining_date_min;
    }

    public void setCal_new_ou_joining_date_min(String cal_new_ou_joining_date_min) {
        this.cal_new_ou_joining_date_min = cal_new_ou_joining_date_min;
    }

    public void setOuJoining_date() {
        if (userSelectionValid) {
            if (cal_transfer_date != null) {
                cal_new_ou_joining_date_min = DateTimeUtility.ChangeDateFormat(cal_transfer_date, null);
            } else {
                cal_new_ou_joining_date_min = cal_transfer_date_min;
            }
            cal_new_ou_joining_date_max = cal_transfer_date_max;
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_transfer_date"> 
    Date cal_transfer_date;
    String cal_transfer_date_min;
    String cal_transfer_date_max;

    public Date getCal_transfer_date() {
        return cal_transfer_date;
    }

    public void setCal_transfer_date(Date cal_transfer_date) {
        this.cal_transfer_date = cal_transfer_date;
    }

    public String getCal_transfer_date_max() {
        return cal_transfer_date_max;
    }

    public void setCal_transfer_date_max(String cal_transfer_date_max) {
        this.cal_transfer_date_max = cal_transfer_date_max;
    }

    public String getCal_transfer_date_min() {
        return cal_transfer_date_min;
    }

    public void setCal_transfer_date_min(String cal_transfer_date_min) {
        this.cal_transfer_date_min = cal_transfer_date_min;
    }

    public void cal_transfer_date_changed(ValueChangeEvent event) {
        cal_transfer_date = event.getNewValue() == null ? null : (Date) event.getNewValue();
        setOuJoining_date();
    }

    //<editor-fold defaultstate="collapsed" desc="setTransfer_date()"> 
    public void setTransfer_date() {
        if (userSelectionValid) {
            List<FhrdEmpContractMst> lstContractMst = fhrdEmpContractMstFacade.findAllContractEmpwise(selectedUser.getUserId(), "'M'");
            Date v_contract_start_date = null;
            Date v_contract_end_date = null;
            int n = lstContractMst.size();
            for (int i = 0; i < lstContractMst.size(); i++) {
                if (new Date().before(lstContractMst.get(i).getEndDate())) {
                    v_contract_start_date = lstContractMst.get(i).getStartDate();
                    v_contract_end_date = lstContractMst.get(i).getEndDate();
                }
            }
            cal_transfer_date_min = DateTimeUtility.ChangeDateFormat(v_contract_start_date, null);
            cal_transfer_date_max = DateTimeUtility.ChangeDateFormat(v_contract_end_date, null);
            setOuJoining_date();
        }
    }
    //</editor-fold>
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="btn_transfer_clicked"> 
    public void btn_transfer_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();

        FhrdEmpeventTransfer entEmpeventTransfer = new FhrdEmpeventTransfer();
        entEmpeventTransfer.setEmpetSrgKey(getGlobalSettings().getUniqueSrno((cal_transfer_date.getYear() + 1900), "FHRD_EMPEVENT_TRANSFER"));
        entEmpeventTransfer.setCreateby(globalData.getEnt_login_user());
        entEmpeventTransfer.setCreatedt(new Date());
        entEmpeventTransfer.setTransferDate(cal_transfer_date);
        entEmpeventTransfer.setOuJoinDate(cal_new_ou_joining_date);
        entEmpeventTransfer.setFromOumUnitSrno(new SystOrgUnitMst(txt_from_oum_unit_srno));
        entEmpeventTransfer.setToOumUnitSrno(new SystOrgUnitMst(Integer.valueOf(ddl_to_oum)));
        entEmpeventTransfer.setOumUnitSrno(globalData.getEnt_working_ou());
        entEmpeventTransfer.setRefOumUnitSrno(globalData.getWorking_ou());
        entEmpeventTransfer.setRemarks(txt_remarks);
        entEmpeventTransfer.setUserId(selectedUser);
        entEmpeventTransfer.setCurrentReportingUserId(new FhrdEmpmst(txt_current_rep_userid));
        entEmpeventTransfer.setNewReportingUserId(new FhrdEmpmst(Integer.valueOf(ddl_new_rep_userid)));
        try {
            fhrdEmpeventTransferFacade.create(entEmpeventTransfer);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Employee Transfer set Successfully"));

            resetTransferDetails();
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred."));
        }
    }

    public void resetTransferDetails() {
        cal_transfer_date = null;
        txt_from_oum = null;
        txt_username = null;
        txt_from_oum_unit_srno = null;
        ddl_to_oum = null;
        txt_remarks = null;
        txt_user_id = null;
        txt_current_rep_userid = null;
        txt_current_rep_userid_name = null;
        setDdl_new_rep_userid_options();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="init()"> 
    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        userSelectionValid = false;
        setDdl_to_oum();
        setDdl_new_rep_userid_options();

    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings and default methods">
    //<editor-fold defaultstate="collapsed" desc="default">

    /**
     * Creates a new instance of PIS_Emp_Transfer
     */
    public PIS_Emp_Transfer() {
    }
    //</editor-fold>  
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold> 
}
