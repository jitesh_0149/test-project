/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.view.tas;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.FtasAbsentees;
import org.fes.pis.entities.FtasLeavemst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FtasAbsenteesFacadeLocal;
import org.fes.pis.sessions.FtasLeavemstFacadeLocal;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Emp_Leaves")
@ViewScoped
public class PIS_Emp_Leaves implements Serializable {

    @EJB
    private FtasAbsenteesFacadeLocal ftasAbsenteesFacade;
    @EJB
    private FtasLeavemstFacadeLocal ftasLeavemstFacade;

    
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="ddl_year"> 
    String ddl_year;
    List<SelectItem> ddl_year_options;

    public String getDdl_year() {
        return ddl_year;
    }

    public void setDdl_year(String ddl_year) {
        this.ddl_year = ddl_year;
    }

    public List<SelectItem> getDdl_year_options() {
        return ddl_year_options;
    }

    public void setDdl_year_options(List<SelectItem> ddl_year_options) {
        this.ddl_year_options = ddl_year_options;
    }

    public void setDdl_year_options() {
        ddl_year_options = new ArrayList<SelectItem>();
        ddl_year_options.add(new SelectItem("2015", "2015"));
        ddl_year_options.add(new SelectItem("2014", "2014"));
        ddl_year = ddl_year_options.get(0).getValue().toString();
    }

    public void ddl_year_changed(ValueChangeEvent event) {
        ddl_year = event.getNewValue() == null ? null : event.getNewValue().toString();
        setTbl_leave_detail();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_leavecd"> 
    String ddl_leavecd;
    List<SelectItem> ddl_leavecd_options;

    public String getDdl_leavecd() {
        return ddl_leavecd;
    }

    public void setDdl_leavecd(String ddl_leavecd) {
        this.ddl_leavecd = ddl_leavecd;
    }

    public List<SelectItem> getDdl_leavecd_options() {
        return ddl_leavecd_options;
    }

    public void setDdl_leavecd_options(List<SelectItem> ddl_leavecd_options) {
        this.ddl_leavecd_options = ddl_leavecd_options;
    }

    public void setDdl_leavecd_options() {
        ddl_leavecd_options = new ArrayList<SelectItem>();
        List<FtasLeavemst> lstList = ftasLeavemstFacade.findAllLeaves(getGlobalSettings().getWorking_ou());
        if (lstList.isEmpty()) {
            ddl_leavecd_options.add(new SelectItem(null, "---Not Found---"));
        } else {
            int n = lstList.size();
            ddl_leavecd_options.add(new SelectItem(null, "---All---"));
            for (int i = 0; i < n; i++) {
                ddl_leavecd_options.add(new SelectItem(lstList.get(i).getLmSrgKey().toString(), lstList.get(i).getLeavecd().toString() + " - " + lstList.get(i).getLeaveDesc().toString()));
            }
        }
        ddl_leavecd = null;
    }

    public void ddl_leavecd_changed(ValueChangeEvent event) {
        ddl_leavecd = event.getNewValue() == null ? null : event.getNewValue().toString();
        setTbl_leave_detail();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Main Table Settings">
    //<editor-fold defaultstate="collapsed" desc="tbl_leave_detail">
    List<FtasAbsentees> tbl_leave_detail;
    List<FtasAbsentees> tbl_leave_detail_filtered;

    public List<FtasAbsentees> getTbl_leave_detail_filtered() {
        return tbl_leave_detail_filtered;
    }

    public void setTbl_leave_detail_filtered(List<FtasAbsentees> tbl_leave_detail_filtered) {
        this.tbl_leave_detail_filtered = tbl_leave_detail_filtered;
    }

    public List<FtasAbsentees> getTbl_leave_detail() {
        return tbl_leave_detail;
    }

    public void setTbl_leave_detail(List<FtasAbsentees> tbl_leave_detail) {
        this.tbl_leave_detail = tbl_leave_detail;
    }

    public void setTbl_leave_detail() {
        Date v_from_date = new Date(Integer.valueOf(ddl_year) - 1900, 0, 1);
        Date v_to_date = new Date(Integer.valueOf(ddl_year) - 1900, 11, 31);
        tbl_leave_detail = ftasAbsenteesFacade.findAll(globalData.getWorking_ou(), globalData.getUser_id(), true, false, v_from_date, v_to_date, ddl_leavecd, true, null, false,
                "FROM_DATE DESC,"
                + "DECODE(NVL(CLONE_OF_ABS_SRG_KEY,0),0,NULL,FROM_DATE),CREATEDT DESC");
        tbl_leave_detail_filtered = null;
        setFilter_status_options();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="filter_status_options">
    List<SelectItem> filter_status_options;

    public List<SelectItem> getFilter_status_options() {
        return filter_status_options;
    }

    public void setFilter_status_options(List<SelectItem> filter_status_options) {
        this.filter_status_options = filter_status_options;
    }

    private void setFilter_status_options() {
        filter_status_options = new ArrayList<SelectItem>();
        filter_status_options.add(new SelectItem("", ""));
        filter_status_options.add(new SelectItem("P", "Pending"));
        filter_status_options.add(new SelectItem("Y", "Approved"));
        filter_status_options.add(new SelectItem("N", "Rejected"));
        filter_status_options.add(new SelectItem("YCP", "Cancellation Applied"));
        filter_status_options.add(new SelectItem("YCY", "Cancelled"));
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="View Detail Methods">
    //<editor-fold defaultstate="collapsed" desc="viewLeaveDetail">

    public void viewLeaveDetail(FtasAbsentees selectedLeave) {
        this.selectedLeave = selectedLeave;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="selectedLeave">
    FtasAbsentees selectedLeave;

    public FtasAbsentees getSelectedLeave() {
        return selectedLeave;
    }

    public void setSelectedLeave(FtasAbsentees selectedLeave) {
        this.selectedLeave = selectedLeave;
    }
    //</editor-fold>
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="init()"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setFilter_status_options();
        setDdl_year_options();
        setDdl_leavecd_options();
        setTbl_leave_detail();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Default method and global settings">

    /**
     * Creates a new instance of PIS_Emp_Leaves
     */
    public PIS_Emp_Leaves() {
    }

    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Utilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    //</editor-fold>
}
