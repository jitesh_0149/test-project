/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.transactions.tas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.db.fes_project.pack.Ftas_Pack;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.FhrdEmpEncash;
import org.fes.pis.entities.FhrdEmpleavebal;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.jsf.GlobalResources;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.jsf.selectableDataModels.SD_FhrdEmpmst;
import org.fes.pis.jsf.utilities.JSFMessages;
import org.fes.pis.sessions.FhrdEmpEncashFacadeLocal;
import org.fes.pis.sessions.FhrdEmpleavebalFacadeLocal;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;
import org.fes.pis.sessions.FtasAbsenteesFacadeLocal;

/**
 *
 * @author anu
 */
@ManagedBean
@ViewScoped
public class Encashment_application implements Serializable {

    @EJB
    private FhrdEmpEncashFacadeLocal fhrdEmpEncashFacade;
    @EJB
    private FtasAbsenteesFacadeLocal ftasAbsenteesFacade;
    @EJB
    private FhrdEmpleavebalFacadeLocal fhrdEmpleavebalFacade;
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    Ou_Pack ou_Pack = new Ou_Pack();
    Ftas_Pack ftas_Pack = new Ftas_Pack();
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="User Selection Methods">
    //<editor-fold defaultstate="collapsed" desc="txt_user_id">
    String txt_user_id;
    String err_txt_user_id;

    public String getTxt_user_id() {
        return txt_user_id;
    }

    public void setTxt_user_id(String txt_user_id) {
        this.txt_user_id = txt_user_id;
    }

    public String getErr_txt_user_id() {
        return err_txt_user_id;
    }

    public void setErr_txt_user_id(String err_txt_user_id) {
        this.err_txt_user_id = err_txt_user_id;
    }

    public void txt_user_id_changed(ValueChangeEvent event) {
        txt_user_id = event.getNewValue().toString();
        err_txt_user_id = null;
        txt_user_id_changed();
    }

    private void txt_user_id_changed() {
        selectedUser = null;
        if (txt_user_id != null && txt_user_id.equals("") != true) {
            selectedUser = fhrdEmpmstFacade.find(Integer.valueOf(txt_user_id));
            if (selectedUser == null) {
                err_txt_user_id = " (User Id is invalid)";
            }
        }
        userSelected();
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="selectedUser">
    FhrdEmpmst selectedUser;

    public FhrdEmpmst getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(FhrdEmpmst selectedUser) {
        this.selectedUser = selectedUser;

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="userSelectionValid">
    boolean userSelectionValid;

    public boolean isUserSelectionValid() {
        return userSelectionValid;
    }

    public void setUserSelectionValid(boolean userSelectionValid) {
        this.userSelectionValid = userSelectionValid;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="userSelected">

    private void userSelected() {
        userSelectionValid = false;
        if (selectedUser != null) {
            txt_user_id = selectedUser.getUserId().toString();
            if (!getPIS_GlobalSettings().getSysetemUser_Fhrd() && !globalData.getUser_id().equals(selectedUser.getUserId())) {
                if (!fhrdEmpmstFacade.hasAccessSpanO(globalData.getUser_id(), selectedUser.getPostingOumUnitSrno().getOumUnitSrno())) {
                    err_txt_user_id = " (You do not have privilege to fill the travelling allowance for selected user)";
                }
            } else if ((selectedUser.getOrgRelieveDate() != null || selectedUser.getTerminationDate() != null)
                    && (!fhrdEmpleavebalFacade.isUserTransactionPending(selectedUser.getUserId()))) {
                err_txt_user_id = " (This employee has relieved/terminated the organisation)";
            }
            if (err_txt_user_id == null) {
                userSelectionValid = true;
            }
        } else {
            txt_user_id = null;
        }
        resetComponents();

    }
    //</editor-fold>    
    ///<editor-fold defaultstate="collapsed" desc="User Selectable List Settings">
    //<editor-fold defaultstate="collapsed" desc="selectedUserFromList">
    FhrdEmpmst selectedUserFromList;

    public FhrdEmpmst getSelectedUserFromList() {
        return selectedUserFromList;
    }

    public void setSelectedUserFromList(FhrdEmpmst selectedUserFromList) {
        this.selectedUserFromList = selectedUserFromList;
        err_txt_user_id = null;
        if (selectedUserFromList != null) {
            selectedUser = selectedUserFromList;
            userSelected();
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lst_employee_selectable">
    SD_FhrdEmpmst lst_employee_selectable;

    public SD_FhrdEmpmst getLst_employee_selectable() {
        return lst_employee_selectable;
    }

    public void setLst_employee_selectable(SD_FhrdEmpmst lst_employee_selectable) {
        this.lst_employee_selectable = lst_employee_selectable;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lnk_view_emplist">
    public void lnk_view_emplist(ActionEvent event) {
        boolean isSysUser = getPIS_GlobalSettings().getSysetemUser_Fhrd();
        lst_employee_selectable = new SD_FhrdEmpmst(fhrdEmpmstFacade.findAll(globalData.getWorking_ou(), false, false, false, null, isSysUser ? null : globalData.getUser_id(), null, true, true, false, null));
    }
    //</editor-fold>
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_leave_period">
    FhrdEmpleavebal ddl_leave_period;
    List<FhrdEmpleavebal> ddl_leave_period_options;

    public FhrdEmpleavebal getDdl_leave_period() {
        return ddl_leave_period;
    }

    public void setDdl_leave_period(FhrdEmpleavebal ddl_leave_period) {
        this.ddl_leave_period = ddl_leave_period;
    }

    public List<FhrdEmpleavebal> getDdl_leave_period_options() {
        return ddl_leave_period_options;
    }

    public void setDdl_leave_period_options(List<FhrdEmpleavebal> ddl_leave_period_options) {
        this.ddl_leave_period_options = ddl_leave_period_options;
    }

    private void setDdl_leave_period() {
        ddl_leave_period = null;
        if (userSelectionValid) {
            ddl_leave_period_options = fhrdEmpleavebalFacade.findAllForEncashmentApplication(selectedUser.getUserId());
        } else {
            ddl_leave_period_options = null;
        }

    }

    public void ddl_leave_period_changed(ValueChangeEvent event) {
        ddl_leave_period = event.getNewValue() == null ? null : fhrdEmpleavebalFacade.find(((FhrdEmpleavebal) event.getNewValue()).getEmplbSrgKey());
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_encash_leaves">
    String txt_encash_leaves;

    public String getTxt_encash_leaves() {
        return txt_encash_leaves;
    }

    public void setTxt_encash_leaves(String txt_encash_leaves) {
        this.txt_encash_leaves = txt_encash_leaves;
    }
    //</editor-fold>   
    //<editor-fold defaultstate="collapsed" desc="ddl_actionby">
    String ddl_actionby;
    List<SelectItem> ddl_actionby_options;

    public String getDdl_actionby() {
        return ddl_actionby;
    }

    public void setDdl_actionby(String ddl_actionby) {
        this.ddl_actionby = ddl_actionby;
    }

    public List<SelectItem> getDdl_actionby_options() {
        return ddl_actionby_options;
    }

    public void setDdl_actionby_options(List<SelectItem> ddl_actionby_options) {
        this.ddl_actionby_options = ddl_actionby_options;
    }

    private void setDdl_actionby() {
        FacesContext context = FacesContext.getCurrentInstance();
        ddl_actionby_options = new ArrayList<SelectItem>();
        List<FhrdEmpmst> lst_Empmsts;
        if (selectedUser == null) {
            ddl_actionby_options = new ArrayList<SelectItem>();
            ddl_actionby_options.add(new SelectItem(null, "--- Enter User Id ---"));
            ddl_actionby = null;

        } else {
            lst_Empmsts = fhrdEmpmstFacade.findReportingUser(selectedUser.getPostingOumUnitSrno().getOumUnitSrno(), selectedUser.getUserId());
            if (lst_Empmsts.isEmpty()) {
                lst_Empmsts = fhrdEmpmstFacade.findReportingUser(globalData.getWorking_ou(), globalData.getUser_id());
            }
            int n = lst_Empmsts.size();
            if (n != 0) {
                for (int i = 0; i < n; i++) {
                    ddl_actionby_options.add(new SelectItem(lst_Empmsts.get(i).getUserId(), lst_Empmsts.get(i).getUserName()));
                }
            } else {
                ddl_actionby_options = new ArrayList<SelectItem>();
                ddl_actionby_options.add(new SelectItem(null, "--- Not Found ---"));
                ddl_actionby = null;
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Information", "Approval Authority is not defined for Organization Unit: " + globalData.getEnt_working_ou().getOumName()));
            }
            ddl_actionby = selectedUser.getReportingUserId().getUserId().toString();
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_remarks">
    String txt_remarks;

    public String getTxt_remarks() {
        return txt_remarks;
    }

    public void setTxt_remarks(String txt_remarks) {
        this.txt_remarks = txt_remarks;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="applyEncashment">

    public void applyEncashment(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            FhrdEmpEncash ftasEmpEncash = new FhrdEmpEncash();
            Integer v_cal_year = ddl_leave_period.getStartDate().getYear() + 1900;
            BigDecimal v_enc_srg_key = getGlobalSettings().getUniqueSrno(v_cal_year, "FHRD_EMP_ENCASH");
            ftasEmpEncash.setEncSrgKey(v_enc_srg_key);
            ftasEmpEncash.setUserId(selectedUser);
            ftasEmpEncash.setApplicationDate(new Date());
            ftasEmpEncash.setEmplbSrgKey(ddl_leave_period);
            ftasEmpEncash.setLeavecd(ddl_leave_period.getLeavecd());
            ftasEmpEncash.setLmSrgKey(ddl_leave_period.getLmSrgKey());
            ftasEmpEncash.setLrSrgKey(ddl_leave_period.getLrSrgKey());
            ftasEmpEncash.setNoOfDays(new BigDecimal(txt_encash_leaves));
            ftasEmpEncash.setAvailableBalance(BigDecimal.ZERO);
            ftasEmpEncash.setRemarks(txt_remarks.trim().toUpperCase());
            ftasEmpEncash.setApplicationFlag("U");
            ftasEmpEncash.setAttdCalcTimeEntryFlag("N");
            ftasEmpEncash.setActnby(new FhrdEmpmst(new Integer(ddl_actionby)));
            ftasEmpEncash.setCarryForwardFromOldBal(BigDecimal.ZERO);
            ftasEmpEncash.setCreateby(globalData.getEnt_login_user());
            ftasEmpEncash.setCreatedt(new Date());
            ftasEmpEncash.setOumUnitSrno(selectedUser.getPostingOumUnitSrno());
            ftasEmpEncash.setTranYear(Short.valueOf(v_cal_year.toString()));
            fhrdEmpEncashFacade.create(ftasEmpEncash);
            txt_user_id = null;
            txt_user_id_changed();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully"));
        } catch (Exception e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_mark_attendance_clicked", null, e);
            context.addMessage(null, JSFMessages.getErrorMessage(e));
        }

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetComponents">

    private void resetComponents() {
        setDdl_leave_period();
        setDdl_actionby();
        txt_encash_leaves = null;
        txt_remarks = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setDdl_actionby();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Default Constructor & Other Beans Methods">
    // <editor-fold defaultstate="collapsed" desc="default constructor">

    /**
     * Creates a new instance of PIS_Daily_Attendance
     */
    public Encashment_application() {
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Resources">

    protected GlobalResources getGlobalResources() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalResources) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalResources");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
    //</editor-fold>
}
