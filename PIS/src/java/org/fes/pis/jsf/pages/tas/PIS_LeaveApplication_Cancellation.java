/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.tas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.customJpaController;
import org.fes.pis.custom_sessions.ejb_utilities_pisLocal;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.FtasAbsentees;
import org.fes.pis.entities.FtasCancelabs;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.jsf.selectableDataModels.SD_FhrdEmpmst;
import org.fes.pis.sessions.FhrdEmpleavebalFacadeLocal;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;
import org.fes.pis.sessions.FtasAbsenteesFacadeLocal;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_LeaveApplication_Cancellation")
@ViewScoped
public class PIS_LeaveApplication_Cancellation implements Serializable {

    @EJB
    private ejb_utilities_pisLocal ejb_utilities_pis;
    @EJB
    private FhrdEmpleavebalFacadeLocal fhrdEmpleavebalFacade;
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    @EJB
    private customJpaController customJpaController;
    @EJB
    private FtasAbsenteesFacadeLocal ftasAbsenteesFacade;

    
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="User Selection">
    //<editor-fold defaultstate="collapsed" desc="txt_user_id">
    String txt_user_id;
    String err_txt_user_id;

    public String getErr_txt_user_id() {
        return err_txt_user_id;
    }

    public void setErr_txt_user_id(String err_txt_user_id) {
        this.err_txt_user_id = err_txt_user_id;
    }

    public String getTxt_user_id() {
        return txt_user_id;
    }

    public void setTxt_user_id(String txt_user_id) {
        this.txt_user_id = txt_user_id;
    }
    // <editor-fold defaultstate="collapsed" desc="txt_user_id_changed">

    public void txt_user_id_changed(ValueChangeEvent event) {
        txt_user_id = event.getNewValue().toString();
        selectedUser = null;
        if (txt_user_id.equals("") != true) {
            selectedUser = fhrdEmpmstFacade.find(Integer.valueOf(txt_user_id));
        }
        userSelected();


    }
    // </editor-fold>
    //</editor-fold>     
    //<editor-fold defaultstate="collapsed" desc="User Selection Methods">
    //<editor-fold defaultstate="collapsed" desc="selectedUser">
    FhrdEmpmst selectedUser;

    public FhrdEmpmst getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(FhrdEmpmst selectedUser) {
        this.selectedUser = selectedUser;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="userSelected">

    private void userSelected() {
        userSelectionValid = false;
        if (selectedUser != null) {
            txt_user_id = selectedUser.getUserId().toString();
            if (!getPIS_GlobalSettings().getSysetemUser_Fhrd() && !globalData.getUser_id().equals(selectedUser.getUserId())) {
                if (!fhrdEmpmstFacade.hasAccessSpanO(globalData.getUser_id(), selectedUser.getPostingOumUnitSrno().getOumUnitSrno())) {
                    err_txt_user_id = " (You do not have privilege to cancel leave/tour/training application of selected user)";
                }
            } else if (!fhrdEmpleavebalFacade.isUserTransactionPending(selectedUser.getUserId())) {
                if (selectedUser.getOrgRelieveDate() != null || selectedUser.getTerminationDate() != null) {
                    err_txt_user_id = " (This employee has relieved/terminated the organisation)";
                } else {
                    err_txt_user_id = " (Either contract is not given or not does not have any active contract)";
                }
            }
            if (err_txt_user_id == null) {
                userSelectionValid = true;
            }
        } else {
            txt_user_id = null;
        }
        txt_cancellation_reason = null;
        setTbl_approved_applications();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="userSelectionValid">
    boolean userSelectionValid;

    public boolean isUserSelectionValid() {
        return userSelectionValid;
    }

    public void setUserSelectionValid(boolean userSelectionValid) {
        this.userSelectionValid = userSelectionValid;
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_actionby">
    String ddl_actionby;
    List<SelectItem> ddl_actionby_options;

    public String getDdl_actionby() {
        return ddl_actionby;
    }

    public void setDdl_actionby(String ddl_actionby) {
        this.ddl_actionby = ddl_actionby;
    }

    public void setDdl_actionby() {
        FacesContext context = FacesContext.getCurrentInstance();
        ddl_actionby_options = new ArrayList<SelectItem>();
        List<FhrdEmpmst> lst_Empmsts;
        if (selectedUser == null) {
            ddl_actionby_options = new ArrayList<SelectItem>();
            ddl_actionby_options.add(new SelectItem(null, "--- Enter User Id ---"));
            ddl_actionby = null;

        } else {

            lst_Empmsts = fhrdEmpmstFacade.findReportingUser(selectedUser.getPostingOumUnitSrno().getOumUnitSrno(), selectedUser.getUserId());
            if (lst_Empmsts.isEmpty()) {
                lst_Empmsts = fhrdEmpmstFacade.findReportingUser(globalData.getWorking_ou(), globalData.getUser_id());
            }
            int n = lst_Empmsts.size();
            if (n != 0) {
                for (int i = 0; i < n; i++) {
                    ddl_actionby_options.add(new SelectItem(lst_Empmsts.get(i).getUserId(), lst_Empmsts.get(i).getUserName()));
                }
            } else {
                ddl_actionby_options = new ArrayList<SelectItem>();
                ddl_actionby_options.add(new SelectItem(null, "--- Not Found ---"));
                ddl_actionby = null;
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Information", "Approval Authority is not defined for Organization Unit: " + globalData.getEnt_working_ou().getOumName()));
            }


            ddl_actionby = selectedUser.getReportingUserId().getUserId().toString();
        }
    }

    public List<SelectItem> getDdl_actionby_options() {
        return ddl_actionby_options;
    }

    public void setDdl_actionby_options(List<SelectItem> ddl_actionby_options) {
        this.ddl_actionby_options = ddl_actionby_options;
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cancellationPossible">
    boolean cancellationPossible;

    public boolean isCancellationPossible() {
        return cancellationPossible;
    }

    public void setCancellationPossible(boolean cancellationPossible) {
        this.cancellationPossible = cancellationPossible;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Partial Cancellation Settings">
    //<editor-fold defaultstate="collapsed" desc="lastLockedDate">
    Date lastLockedDate;

    public Date getLastLockedDate() {
        return lastLockedDate;
    }

    public void setLastLockedDate(Date lastLockedDate) {
        this.lastLockedDate = lastLockedDate;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="partialCancellation">
    boolean partialCancellation;

    public boolean isPartialCancellation() {
        return partialCancellation;
    }

    public void setPartialCancellation(boolean partialCancellation) {
        this.partialCancellation = partialCancellation;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_partial_from_date">
    Date cal_partial_from_date;
    String cal_partial_from_date_min;
    String cal_partial_from_date_max;

    public Date getCal_partial_from_date() {
        return cal_partial_from_date;
    }

    public void setCal_partial_from_date(Date cal_partial_from_date) {
        this.cal_partial_from_date = cal_partial_from_date;
    }

    public String getCal_partial_from_date_max() {
        return cal_partial_from_date_max;
    }

    public void setCal_partial_from_date_max(String cal_partial_from_date_max) {
        this.cal_partial_from_date_max = cal_partial_from_date_max;
    }

    public String getCal_partial_from_date_min() {
        return cal_partial_from_date_min;
    }

    public void setCal_partial_from_date_min(String cal_partial_from_date_min) {
        this.cal_partial_from_date_min = cal_partial_from_date_min;
    }

    private void setCal_partial_from_date() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (partialCancellation) {
            String v_last_date = ejb_utilities_pis.getPartialCancFromDate(selectedApplication.getAbsSrgKey(), null);
            cal_partial_from_date = DateTimeUtility.stringToDate(v_last_date.split(":", 2)[0], null);
            setDdl_partial_from_half(v_last_date.split(":", 2)[1].split(":"));
            cal_partial_from_date_min = DateTimeUtility.ChangeDateFormat(cal_partial_from_date, null);
            cal_partial_from_date_max = DateTimeUtility.ChangeDateFormat(selectedApplication.getToDate(), null);
        } else {
            cal_partial_from_date = selectedApplication.getFromDate();
        }
        if (cal_partial_from_date.after(selectedApplication.getToDate())) {
            cancellationPossible = false;
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Application cannot be cancelled", "Selected Application cannot be cancelled as application period is locked and cannot shrink it over the minimum requirement"));
        }

    }

    public void cal_partial_from_date_changed(SelectEvent event) {
        cal_partial_from_date = (Date) event.getObject();
        String v_last_date = ejb_utilities_pis.getPartialCancFromDate(selectedApplication.getAbsSrgKey(), cal_partial_from_date);
        setDdl_partial_from_half(v_last_date.split(":", 2)[1].split(":"));
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_partial_from_half">
    String ddl_partial_from_half;
    List<SelectItem> ddl_partial_from_half_options;

    public String getDdl_partial_from_half() {
        return ddl_partial_from_half;
    }

    public void setDdl_partial_from_half(String ddl_partial_from_half) {
        this.ddl_partial_from_half = ddl_partial_from_half;
    }

    public List<SelectItem> getDdl_partial_from_half_options() {
        return ddl_partial_from_half_options;
    }

    public void setDdl_partial_from_half_options(List<SelectItem> ddl_partial_from_half_options) {
        this.ddl_partial_from_half_options = ddl_partial_from_half_options;
    }

    private void setDdl_partial_from_half(String[] p_half) {
        ddl_partial_from_half_options = new ArrayList<SelectItem>();
        for (String s : p_half) {
            ddl_partial_from_half_options.add(new SelectItem(s, s.equals("0") ? "Full Day" : (s.equals("1") ? "First Half" : "Second Half")));
        }
        ddl_partial_from_half = ddl_partial_from_half_options.get(0).getValue().toString();

    }
    //</editor-fold>
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="txt_cancellation_reason">
    String txt_cancellation_reason;

    public String getTxt_cancellation_reason() {
        return txt_cancellation_reason;
    }

    public void setTxt_cancellation_reason(String txt_cancellation_reason) {
        this.txt_cancellation_reason = txt_cancellation_reason;
    }
    // </editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="tbl_approved_applications"> 
    public List<FtasAbsentees> tbl_approved_applications;

    public List<FtasAbsentees> getTbl_approved_applications() {
        return tbl_approved_applications;
    }

    public void setTbl_approved_applications(List<FtasAbsentees> tbl_approved_applications) {
        this.tbl_approved_applications = tbl_approved_applications;
    }

    public void setTbl_approved_applications() {
        FacesContext context = FacesContext.getCurrentInstance();
        selectedApplication = null;
        if (userSelectionValid) {
            tbl_approved_applications = ftasAbsenteesFacade.findAll_ApprovedApplList(selectedUser.getUserId(), selectedUser.getPostingOumUnitSrno().getOumUnitSrno(), true);
            if (tbl_approved_applications.isEmpty()) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "No any approved applications", "There is no any approved applications for selected user"));
            }
            lastLockedDate = ejb_utilities_pis.getLastLockedDate(selectedUser.getUserId(), selectedUser.getPostingOumUnitSrno().getOumUnitSrno());
        } else {
            tbl_approved_applications = null;
        }

    }
    // </editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="selectedApplication"> 
    FtasAbsentees selectedApplication;

    public FtasAbsentees getSelectedApplication() {
        return selectedApplication;
    }

    public void setSelectedApplication(FtasAbsentees selectedApplication) {
        this.selectedApplication = selectedApplication;
        if (lastLockedDate.before(selectedApplication.getFromDate())) {
            partialCancellation = false;
        } else {
            partialCancellation = true;
            cancellationPossible = true;
            setCal_partial_from_date();

        }
    }
    //</editor-fold> 
    // <editor-fold defaultstate="collapsed" desc="btn_save_clicked">

    public void btn_save_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (selectedApplication == null) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Kindly select Application From Approved Application List."));
        } else if (txt_cancellation_reason.isEmpty()) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "First Fill up the Cancel Application Reason Field and then Apply."));
        } else {
            Integer v_cal_year = (selectedApplication.getFromDate().getYear() + 1900);
            BigDecimal v_cabs_srgkey = getGlobalSettings().getUniqueSrno(v_cal_year, "FTAS_CANCELABS");

            FtasCancelabs ent_Cancelabs = new FtasCancelabs();
            FtasAbsentees ent_Absentees = ftasAbsenteesFacade.find(selectedApplication.getAbsSrgKey());
            try {

                FhrdEmpmst ent_FhrdEmpmst = new FhrdEmpmst(ent_Absentees.getUserId().getUserId());
                ent_Cancelabs.setCabsSrgKey(v_cabs_srgkey);
                ent_Cancelabs.setUserId(ent_FhrdEmpmst);
                ent_Cancelabs.setSrno(ent_Absentees.getSrno());
                ent_Cancelabs.setCancelSrno(-1);
                ent_Cancelabs.setOumUnitSrno(selectedApplication.getUserId().getPostingOumUnitSrno().getOumUnitSrno());
                ent_Cancelabs.setAbsOumUnitSrno(ent_Absentees.getOumUnitSrno().getOumUnitSrno());
                ent_Cancelabs.setSubSrno(1);
                if (partialCancellation) {
                    ent_Cancelabs.setFromDate(cal_partial_from_date);
                    ent_Cancelabs.setFromhalf(ddl_partial_from_half);
                    ent_Cancelabs.setPartialCancelFlag("Y");
                } else {
                    ent_Cancelabs.setFromDate(selectedApplication.getFromDate());
                    ent_Cancelabs.setFromhalf(selectedApplication.getFromhalf());
                    ent_Cancelabs.setPartialCancelFlag("N");
                }
                ent_Cancelabs.setToDate(selectedApplication.getToDate());
                ent_Cancelabs.setTohalf(selectedApplication.getTohalf());
                ent_Cancelabs.setCreateby(globalData.getEnt_login_user());
                ent_Cancelabs.setCreatedt(new Date());
                ent_Cancelabs.setActnby(ent_Absentees.getAprvby());
                ent_Cancelabs.setReason(txt_cancellation_reason.toUpperCase());
                ent_Cancelabs.setAbsSrgKey(ent_Absentees);
                ent_Cancelabs.setTranYear(Short.valueOf(v_cal_year.toString()));
                boolean isSuccess = customJpaController.cancelLeaveApplication(ent_Cancelabs, ent_Absentees);
//                boolean isSuccess = true;
                if (isSuccess) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully"));
                    selectedUser = null;
                    userSelected();

                } else {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during Cancelling Application."));
                }
            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during Cancelling Application."));
                LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_save_clicked", null, e);
            }
        }

    }// </editor-fold>
    ///<editor-fold defaultstate="collapsed" desc="User Selectable List Settings">
    //<editor-fold defaultstate="collapsed" desc="selectedUserFromList">
    FhrdEmpmst selectedUserFromList;

    public FhrdEmpmst getSelectedUserFromList() {
        return selectedUserFromList;
    }

    public void setSelectedUserFromList(FhrdEmpmst selectedUserFromList) {
        this.selectedUserFromList = selectedUserFromList;
        err_txt_user_id = null;
        if (selectedUserFromList != null) {
            selectedUser = fhrdEmpmstFacade.find(selectedUserFromList.getUserId());            
            userSelected();
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lst_employee_selectable">
    SD_FhrdEmpmst lst_employee_selectable;

    public SD_FhrdEmpmst getLst_employee_selectable() {
        return lst_employee_selectable;
    }

    public void setLst_employee_selectable(SD_FhrdEmpmst lst_employee_selectable) {
        this.lst_employee_selectable = lst_employee_selectable;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lnk_view_emplist">
    public void lnk_view_emplist(ActionEvent event) {
        boolean isSysUser = getPIS_GlobalSettings().getSysetemUser_Fhrd();
        lst_employee_selectable = new SD_FhrdEmpmst(fhrdEmpmstFacade.findAll(globalData.getWorking_ou(), false, false, false, null, isSysUser ? null : globalData.getUser_id(), null, true, true, false, null));
    }
    //</editor-fold>
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="init()">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Defautl Constructor & Other Beans Settings">
    // <editor-fold defaultstate="collapsed" desc="default constructor">

    public PIS_LeaveApplication_Cancellation() {
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
}
