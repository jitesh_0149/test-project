package org.fes.pis.jsf.pages.transactions.tas.utilities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.custom_entities.Database_Output;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.db.fes_project.pack.Ftas_Attdcalc;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.FhrdPaytransAttdCalcLog;
import org.fes.pis.entities.SystOrgUnitMst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;
import org.fes.pis.sessions.FhrdPaytransAttdCalcLogFacadeLocal;
import org.fes.pis.sessions.FtasLockDtlFacadeLocal;
import org.fes.pis.sessions.SystOrgUnitMstFacadeLocal;
import org.joda.time.DateTime;
import org.primefaces.model.DualListModel;

@ManagedBean(name = "PIS_Attendance_Calculation")
@ViewScoped
public class PIS_Attendance_Calculation implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="EJB and Other Declarations">

    
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    Ou_Pack ou_Pack = new Ou_Pack();
    public static Integer v_user_type = 1;
    Ftas_Attdcalc ftas_Attdcalc = new Ftas_Attdcalc();
    @EJB
    private SystOrgUnitMstFacadeLocal systOrgUnitMstFacade;
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    @EJB
    private FhrdPaytransAttdCalcLogFacadeLocal fhrdPaytransAttdCalcLogFacade;
    @EJB
    private FtasLockDtlFacadeLocal ftasLockDtlFacade;
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>  
    //</editor-fold>      
    // <editor-fold defaultstate="collapsed" desc="ddl_org_unit_list">
    String ddl_org_unit_list;
    List<SelectItem> ddl_org_unit_list_options;

    public String getDdl_org_unit_list() {
        return ddl_org_unit_list;
    }

    public void setDdl_org_unit_list(String ddl_org_unit_list) {
        this.ddl_org_unit_list = ddl_org_unit_list;
    }

    public List<SelectItem> getDdl_org_unit_list_options() {
        return ddl_org_unit_list_options;
    }

    public void setDdl_org_unit_list_options(List<SelectItem> ddl_org_unit_list_options) {
        this.ddl_org_unit_list_options = ddl_org_unit_list_options;
    }

    public void ddl_org_unit_list_valuechange(ValueChangeEvent event) {
        ddl_org_unit_list = event.getNewValue() == null ? null : event.getNewValue().toString();
        setPicklst_emp();
    }
    //<editor-fold defaultstate="collapsed" desc="setOrgList()">

    public void setDdl_org_unit_list_options() {
        ddl_org_unit_list_options = new ArrayList<SelectItem>();
        List<SystOrgUnitMst> lst_e = systOrgUnitMstFacade.findAll(globalData.getWorking_ou(), false, false, false, false, true, null, null);
        if (lst_e.isEmpty()) {
            SelectItem s = new SelectItem(null, "No Data Found");
            ddl_org_unit_list_options.add(s);
        } else {
            Integer n = lst_e.size();
            for (int i = 0; i < n; i++) {
                ddl_org_unit_list_options.add(new SelectItem(lst_e.get(i).getOumUnitSrno(), lst_e.get(i).getOumName()));
            }
        }
        ddl_org_unit_list = ddl_org_unit_list_options.get(0) == null ? null : ddl_org_unit_list_options.get(0).getValue().toString();
    }
    //</editor-fold> 
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="min max lock date">
    Date min_lock_date;
    Date max_lock_date;

    private void setMinMaxDate() {
        Date v_date = ftasLockDtlFacade.findMinLockDate(ddl_org_unit_list);
        min_lock_date = new DateTime(v_date).plusMonths(1).toDate();
        min_lock_date = new Date(min_lock_date.getYear(), min_lock_date.getMonth(), 1);
        max_lock_date = new Date(new Date().getYear(), new Date().getMonth(), 1);
        setDdl_year();
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ddl year options "> 
    String ddl_year;
    List<SelectItem> ddl_year_options;

    public String getDdl_year() {
        return ddl_year;
    }

    public void setDdl_year(String ddl_year) {
        this.ddl_year = ddl_year;
    }

    public List<SelectItem> getDdl_year_options() {
        return ddl_year_options;
    }

    public void setDdl_year_options(List<SelectItem> ddl_year_options) {
        this.ddl_year_options = ddl_year_options;
    }

    public void setDdl_year() {
        Integer v_from_year = min_lock_date.getYear() + 1900;
        Integer v_to_year = max_lock_date.getYear() + 1900;
        ddl_year_options = new ArrayList<SelectItem>();
        while (v_from_year <= v_to_year) {
            ddl_year_options.add(new SelectItem(v_from_year));
            v_from_year++;
        }
        if (ddl_year_options.isEmpty()) {
            ddl_year = null;
        } else {
            ddl_year = String.valueOf((new Date()).getYear() + 1900);
        }
//        System.out.println("year " + ddl_year);
        setDdl_month();
    }

    public void ddl_year_valueChange(ValueChangeEvent event) {
        ddl_year = event.getNewValue() == null ? null : event.getNewValue().toString();
        setDdl_month();
        setPicklst_emp();
    }
    // </editor-fold>   
    //<editor-fold defaultstate="collapsed" desc="Attendance and Salary Dates">
    //<editor-fold defaultstate="collapsed" desc="cal_salary_to_date">
    Date cal_salary_to_date;

    public Date getCal_salary_to_date() {
        return cal_salary_to_date;
    }

    public void setCal_salary_to_date(Date cal_salary_to_date) {
        this.cal_salary_to_date = cal_salary_to_date;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_att_to_date">
    Date cal_att_to_date;

    public Date getCal_att_to_date() {
        return cal_att_to_date;
    }

    public void setCal_att_to_date(Date cal_att_to_date) {
        this.cal_att_to_date = cal_att_to_date;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_salary_from_date">
    Date cal_salary_from_date;

    public Date getCal_salary_from_date() {
        return cal_salary_from_date;
    }

    public void setCal_salary_from_date(Date cal_salary_from_date) {
        this.cal_salary_from_date = cal_salary_from_date;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_att_from_date">
    Date cal_att_from_date;

    public Date getCal_att_from_date() {
        return cal_att_from_date;
    }

    public void setCal_att_from_date(Date cal_att_from_date) {
        this.cal_att_from_date = cal_att_from_date;
    }
    //</editor-fold>
    //</editor-fold>   
    // <editor-fold defaultstate="collapsed" desc="ddl_month ">
    String ddl_month;
    List<SelectItem> ddl_month_options;

    public List<SelectItem> getDdl_month_options() {
        return ddl_month_options;
    }

    public void setDdl_month_options(List<SelectItem> ddl_month_options) {
        this.ddl_month_options = ddl_month_options;
    }

    public String getDdl_month() {
        return ddl_month;
    }

    public void setDdl_month(String ddl_month) {
        this.ddl_month = ddl_month;
    }

    private void setDdl_month() {
        ddl_month_options = new ArrayList<SelectItem>();
        if (ddl_year == null) {
            ddl_month_options.add(new SelectItem(null, "--- Not Available ---"));
            ddl_month = null;
        } else {
            Date v_year_start_date = new Date(Integer.valueOf(ddl_year) - 1900, 0, 1);
            Date v_year_end_date = new Date(Integer.valueOf(ddl_year) - 1900, 11, 31);
            Date v_min_lock_date = min_lock_date;
            Date v_max_lock_date = max_lock_date;
            while (v_min_lock_date.before(v_max_lock_date) || v_min_lock_date.equals(v_max_lock_date)) {
                if (!(v_min_lock_date.before(v_year_start_date))
                        && !(v_min_lock_date.after(v_year_end_date))) {
                    ddl_month_options.add(new SelectItem(DateTimeUtility.ChangeDateFormat(v_min_lock_date, "MM").toString(), DateTimeUtility.ChangeDateFormat(v_min_lock_date, "MMM")));
                }
                v_min_lock_date = new DateTime(v_min_lock_date).plusMonths(1).toDate();
            }
//            System.out.println("year "+ddl_year);
//            if (ddl_year.equals("2014")) {
//                ddl_month_options.add(new SelectItem(12, "December"));
//            } else if (ddl_year.equals("2015")) {
//                ddl_month_options.add(new SelectItem(1, "January"));
//            }
            ddl_month = ddl_month_options.get(0).getValue().toString();

        }
    }

    public void ddl_month_valueChange(ValueChangeEvent event) {
        ddl_month = event.getNewValue() == null ? null : event.getNewValue().toString();
        setPicklst_emp();
    }
// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="chk_all">
    boolean chk_all;

    public boolean getChk_all() {
        return chk_all;
    }

    public void setChk_all(boolean chk_all) {
        this.chk_all = chk_all;
    }

    public void chk_all_valuechange(ValueChangeEvent event) {
        chk_all = Boolean.valueOf(event.getNewValue().toString());
        setPicklst_emp();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="picklst_emp"> 
    DualListModel<String> picklst_emp;
    List<String> emplistSource;
    List<String> emplistTarget;
//    private DualListModel<String> employee_list;

    public DualListModel<String> getPicklst_emp() {
        return picklst_emp;
    }

    public void setPicklst_emp(DualListModel<String> picklst_emp) {
        this.picklst_emp = picklst_emp;
    }

    private void setPicklst_emp() {
        emplistSource = new ArrayList<String>();
        emplistTarget = new ArrayList<String>();
        picklst_emp = null;


        if (ddl_month != null && ddl_year.isEmpty() == false) {
            int v_month = Integer.parseInt(ddl_month) - 1;
            Calendar c1 = Calendar.getInstance();
            c1.set(Integer.parseInt(ddl_year), v_month, 1);

            cal_salary_from_date = c1.getTime();
            c1.add(Calendar.MONTH, 1);
            c1.add(Calendar.DATE, -1);

            cal_salary_to_date = c1.getTime();
            Calendar c2 = Calendar.getInstance();
            c2.set(Integer.parseInt(ddl_year), v_month, 5);
            cal_att_to_date = c2.getTime();
            c2.add(Calendar.MONTH, -1);
            c2.add(Calendar.DATE, 1);
            cal_att_from_date = c2.getTime();
            if (!chk_all) {
                emplistSource.clear();
                emplistTarget.clear();
                String[] split_ou_list = ddl_org_unit_list.split("-");
                String v_oum_srno = split_ou_list[0].trim();
                try {
                    String v_start_date = DateTimeUtility.ChangeDateFormat(cal_att_from_date, null);
                    String v_end_date = DateTimeUtility.ChangeDateFormat(cal_att_to_date, null);
                    List<FhrdEmpmst> lst_temp = fhrdEmpmstFacade.findAllForAttendanceCalc(Integer.valueOf(v_oum_srno), v_start_date, v_end_date);
                    Integer n = lst_temp.size();
                    for (int i = 0; i < n; i++) {
                        emplistSource.add(lst_temp.get(i).getUserId() + "-" + lst_temp.get(i).getUserName());
                    }
                } catch (Exception e) {
                }
                picklst_emp = new DualListModel<String>(emplistSource, emplistTarget);
            } else {
                emplistSource.clear();
                emplistTarget.clear();
                String[] split_ou_list = ddl_org_unit_list.split("-");
                String v_oum_srno = split_ou_list[0].trim();
                try {
                    String v_start_date = DateTimeUtility.ChangeDateFormat(cal_att_from_date, null);
                    String v_end_date = DateTimeUtility.ChangeDateFormat(cal_att_to_date, null);
                    List<FhrdEmpmst> lst_temp = fhrdEmpmstFacade.findAllForAttendanceCalc(Integer.valueOf(v_oum_srno), v_start_date, v_end_date);
                    Integer n = lst_temp.size();
                    for (int i = 0; i < n; i++) {
                        emplistTarget.add(lst_temp.get(i).getUserId() + "-" + lst_temp.get(i).getUserName());
                    }
                } catch (Exception e) {
                }
                picklst_emp = new DualListModel<String>(emplistSource, emplistTarget);
            }
        }

    }
    //</editor-fold>       
    //<editor-fold defaultstate="collapsed" desc="errorLog"> 
    boolean errorLog;

    public boolean isErrorLog() {
        return errorLog;
    }

    public void setErrorLog(boolean errorLog) {
        this.errorLog = errorLog;
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="tbl_AttendanceCalcLogs"> 
    List<FhrdPaytransAttdCalcLog> tbl_AttendanceCalcLogs;

    public List<FhrdPaytransAttdCalcLog> getTbl_AttendanceCalcLogs() {
        return tbl_AttendanceCalcLogs;
    }

    public void setTbl_AttendanceCalcLogs(List<FhrdPaytransAttdCalcLog> tbl_AttendanceCalcLogs) {
        this.tbl_AttendanceCalcLogs = tbl_AttendanceCalcLogs;
    }
    //</editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="btn save action event">

    public void btn_save_action(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        String[] split_ou_list = ddl_org_unit_list.split("-");
        String v_oum_srno = split_ou_list[0].trim();
        Database_Output entDatabase_Output = ou_Pack.check_privileges(this.getClass().getName(), "btn_save_action", "FHRD", globalData.getUser_id());
        String v_check_privileges = entDatabase_Output.getString1();
        if (v_check_privileges!=null) {
            Database_Output v_calc_att_return;
            String v_lst_employee_userid = "";

            DualListModel<String> employee_list1 = picklst_emp;
            int v_emptarget_length = employee_list1.getTarget().size();
            for (int i = 0; i < v_emptarget_length; i++) {
                String[] split_emp_target_list = employee_list1.getTarget().get(i).toString().split("-");
                if (i == 0) {
                    v_lst_employee_userid = split_emp_target_list[0].trim();
                } else {
                    v_lst_employee_userid = v_lst_employee_userid + "," + split_emp_target_list[0].trim();
                }
            }
            BigDecimal v_attd_srg_key = fhrdPaytransAttdCalcLogFacade.getAttdSrgKey();
            v_calc_att_return = ftas_Attdcalc.Calculate_Attendance_Proc(this.getClass().getName(),
                    "btn_save_action", v_attd_srg_key,
                    v_lst_employee_userid,
                    cal_att_from_date, "B",
                    cal_att_to_date, "E",
                    cal_salary_from_date, "B",
                    globalData.getUser_id(),
                    Integer.valueOf(v_oum_srno),
                    "N");
            if (v_calc_att_return.isExecuted_successfully() == true) {
                List<FhrdPaytransAttdCalcLog> lstLogs = fhrdPaytransAttdCalcLogFacade.findAllByAttdSrgKey(v_attd_srg_key);
                if (lstLogs.isEmpty()) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully"));
                    //setDdl_org_unit_list_options();
                    //setMinMaxDate();
                    chk_all = true;
                    errorLog = false;
                    setPicklst_emp();
                } else {
                    tbl_AttendanceCalcLogs = lstLogs;
                    errorLog = true;
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Check", "Some record(s) is/are not saved.. Please check error log."));
                }
            } else {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error in attendance calculation"));
            }

        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "You Do Not Have Previlege to Perform This Task..."));
        }
    }// </editor-fold> 
    // <editor-fold defaultstate="collapsed" desc="Global Settings and default methods">

    // <editor-fold defaultstate="collapsed" desc="default menthods">
    /**
     * Creates a new instance of PIS_Attendance_Calculation
     */
    public PIS_Attendance_Calculation() {
    }// </editor-fold>   
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="init()"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setDdl_org_unit_list_options();
        setMinMaxDate();
        chk_all = true;
        errorLog = false;
        setPicklst_emp();
    }
    //</editor-fold>
}