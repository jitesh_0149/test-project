package org.fes.pis.jsf.pages.pis.masters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.FhrdDesigmst;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.SystOrgUnitMst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdDesigmstFacadeLocal;
import org.primefaces.context.RequestContext;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Designation_Master")
@ViewScoped
public class PIS_Designation_Master implements Serializable {
    //<editor-fold defaultstate="collapsed" desc="globalData">

    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="declarations">
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    @EJB
    private FhrdDesigmstFacadeLocal fhrdDesigmstFacade;
    //</editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="tbl_desigmst">
    List<FhrdDesigmst> tbl_desigmst;
    List<FhrdDesigmst> tbl_desigmst_filtered;

    public List<FhrdDesigmst> getTbl_desigmst() {
        return tbl_desigmst;
    }

    public void setTbl_desigmst(List<FhrdDesigmst> tbl_desigmst) {
        this.tbl_desigmst = tbl_desigmst;
    }

    public void setTbl_desigmst_value() {
        tbl_desigmst = fhrdDesigmstFacade.findAll(globalData.getWorking_ou());
    }

    public List<FhrdDesigmst> getTbl_desigmst_filtered() {
        return tbl_desigmst_filtered;
    }

    public void setTbl_desigmst_filtered(List<FhrdDesigmst> tbl_desigmst_filtered) {
        this.tbl_desigmst_filtered = tbl_desigmst_filtered;
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_apply_date_action"> 
    FhrdDesigmst entFhrdDesigmst_edit;

    public FhrdDesigmst getEntFhrdDesigmst_edit() {
        return entFhrdDesigmst_edit;
    }

    public void setEntFhrdDesigmst_edit(FhrdDesigmst entFhrdDesigmst_edit) {
        this.entFhrdDesigmst_edit = entFhrdDesigmst_edit;
    }

    public void applyEndDate(FhrdDesigmst desigmst) {
        entFhrdDesigmst_edit = desigmst;
        cal_apply_end_date = null;
        Date v_maxTransDate = fhrdDesigmstFacade.findMaxTransactionDate(desigmst.getDsgmSrgKey());
        cal_apply_end_date_min = DateTimeUtility.ChangeDateFormat(DateTimeUtility.findBiggestDate(desigmst.getStartDate(), v_maxTransDate), null);
        cal_apply_end_date = null;
        RequestContext.getCurrentInstance().execute("pw_dlg_apply_end_date.show()");
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="cal_apply_end_date"> 
    Date cal_apply_end_date;
    String cal_apply_end_date_min;

    public Date getCal_apply_end_date() {
        return cal_apply_end_date;
    }

    public void setCal_apply_end_date(Date cal_apply_end_date) {
        this.cal_apply_end_date = cal_apply_end_date;
    }

    public String getCal_apply_end_date_min() {
        return cal_apply_end_date_min;
    }

    public void setCal_apply_end_date_min(String cal_apply_end_date_min) {
        this.cal_apply_end_date_min = cal_apply_end_date_min;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_apply_end_date_clicked"> 
    public void btn_apply_end_date_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        BigDecimal v_dsgm_srg_key = entFhrdDesigmst_edit.getDsgmSrgKey();
        FhrdDesigmst entFhrdDesigmst = fhrdDesigmstFacade.find(v_dsgm_srg_key);
        entFhrdDesigmst.setEndDate(cal_apply_end_date);
        entFhrdDesigmst.setUpdateby(globalData.getUser_id());
        entFhrdDesigmst.setUpdatedt(new Date());
        try {
            fhrdDesigmstFacade.edit(entFhrdDesigmst);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record updated Successfully"));
            setTbl_desigmst_value();
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some error occured during updating Rule"));
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_apply_end_date_clicked", null, e);
        }
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="binded attributes">
    Date cal_start_date;
    Date cal_end_date;
    String txt_desig_desc;
    String lbl_cal_from_err;
    String lbl_desig_desc_err;

    // <editor-fold defaultstate="collapsed" desc="getter/setter">
    public Date getCal_end_date() {
        return cal_end_date;
    }

    public void setCal_end_date(Date cal_end_date) {
        this.cal_end_date = cal_end_date;
    }

    public String getTxt_desig_desc() {
        return txt_desig_desc;
    }

    public void setTxt_desig_desc(String txt_desig_desc) {
        this.txt_desig_desc = txt_desig_desc;
    }

    public Date getCal_start_date() {
        return cal_start_date;
    }

    public void setCal_start_date(Date cal_start_date) {
        this.cal_start_date = cal_start_date;
    }

    public String getLbl_cal_from_err() {
        return lbl_cal_from_err;
    }

    public void setLbl_cal_from_err(String lbl_cal_from_err) {
        this.lbl_cal_from_err = lbl_cal_from_err;
    }

    public String getLbl_desig_desc_err() {
        return lbl_desig_desc_err;
    }

    public void setLbl_desig_desc_err(String lbl_desig_desc_err) {
        this.lbl_desig_desc_err = lbl_desig_desc_err;
    }
// </editor-fold>
    // </editor-fold>   
    //<editor-fold defaultstate="collapsed" desc="btn_add_new_clicked"> 

    public void btn_add_new_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            SystOrgUnitMst ent_SystOrgUnitMst = new SystOrgUnitMst(globalData.getWorking_ou());
            BigDecimal v_dsgm_srgkey = getGlobalSettings().getUniqueSrno(null, "FHRD_DESIGMST");
            FhrdDesigmst ent_Desigmst = new FhrdDesigmst();
            ent_Desigmst.setDesigDesc(txt_desig_desc.toUpperCase());
            ent_Desigmst.setStartDate(cal_start_date);
            if (cal_end_date != null) {
                ent_Desigmst.setEndDate(cal_end_date);
            }
            ent_Desigmst.setCreateby(new FhrdEmpmst(globalData.getUser_id()).getUserId());
            ent_Desigmst.setCreatedt(new Date());
            ent_Desigmst.setDesigSrno(-1);
            ent_Desigmst.setDsgmSrgKey(v_dsgm_srgkey);
            ent_Desigmst.setOumUnitSrno(ent_SystOrgUnitMst);
            try {
                fhrdDesigmstFacade.create(ent_Desigmst);
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record saved Successfully."));
                txt_desig_desc = "";
                cal_start_date = null;
                cal_end_date = null;
                setTbl_desigmst_value();
            } catch (Exception ex) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occerred during saving records"));
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_add_new_clicked", null, ex);
            }
        } catch (Exception e) {
        }

    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="Default Constructor & Other Beans">
    // <editor-fold defaultstate="collapsed" desc="Default Constructor">

    public PIS_Designation_Master() {
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setTbl_desigmst_value();
    }
    //</editor-fold>
}
