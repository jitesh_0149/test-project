package org.fes.pis.jsf.pages.pis.emp_info;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.db.fes_project.pack.Fcad_Pack;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.jsf.GlobalResources;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;
import org.primefaces.context.RequestContext;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Employee_Information")
@ViewScoped
public class PIS_Employee_Information implements Serializable {
    //<editor-fold defaultstate="collapsed" desc="globalData">

    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="EJB Declarations">    
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    Fcad_Pack fcad_Pack = new Fcad_Pack();
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="declarations">
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    Ou_Pack ou_Pack = new Ou_Pack();
    // </editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="default constructor"> 

    /**
     * Creates a new instance of PIS_Employee_Information
     */
    public PIS_Employee_Information() {
    }
    //</editor-fold>  
    //<editor-fold defaultstate="collapsed" desc="tbl_emp_mst"> 
    List<FhrdEmpmst> tbl_emp_info;

    public List<FhrdEmpmst> getTbl_emp_info() {
        return tbl_emp_info;
    }

    public void setTbl_emp_info(List<FhrdEmpmst> tbl_emp_info) {
        this.tbl_emp_info = tbl_emp_info;
    }

    public void setTblEmpInfo() {
        if (globalData.isSystemUserFHRD()) {
            tbl_emp_info = fhrdEmpmstFacade.findAll(globalData.getWorking_ou(), false, false, true, null, null, ddl_verif_status, false, false, false, null);
        } else {
            tbl_emp_info = fhrdEmpmstFacade.findAll(globalData.getWorking_ou(), false, false, true, null, globalData.getUser_id(), ddl_verif_status, false, false, false, null);
        }
    }
    SelectItem[] lst_yes_no_options;
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="refreshTable">

    public void refreshTable() {
        setTblEmpInfo();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_emp_info_filtered"> 
    List<FhrdEmpmst> tbl_emp_info_filtered;

    public List<FhrdEmpmst> getTbl_emp_info_filtered() {
        return tbl_emp_info_filtered;
    }

    public void setTbl_emp_info_filtered(List<FhrdEmpmst> tbl_emp_info_filtered) {
        this.tbl_emp_info_filtered = tbl_emp_info_filtered;
    }
    //</editor-fold>  
    ////////////////////////  employee detail verification
    //<editor-fold defaultstate="collapsed" desc="returnUrlToVerify"> 

    public String returnUrlToVerify() {
        getPIS_GlobalSettings().setForVerification(true);
        RequestContext.getCurrentInstance().execute("openWindow('PIS_Edit_Employee.htm')");
        return null;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="returnUrlToView"> 
    public String returnUrlToView() {
        RequestContext.getCurrentInstance().execute("openWindow('PIS_View_Employee.htm')");
        getPIS_GlobalSettings().setForVerification(false);
        return null;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="returnUrlToUpdate"> 
    public String returnUrlToUpdate() {
        RequestContext.getCurrentInstance().execute("openWindow('PIS_Edit_Employee.htm')");
        return null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Filter Settings">
    //<editor-fold defaultstate="collapsed" desc="ddl_verif_status">
    String ddl_verif_status;

    public String getDdl_verif_status() {
        return ddl_verif_status;
    }

    public void setDdl_verif_status(String ddl_verif_status) {
        this.ddl_verif_status = ddl_verif_status;
    }

    public void ddl_verif_status_changed(ValueChangeEvent event) {
        ddl_verif_status = event.getNewValue() == null ? null : event.getNewValue().toString();
        setTblEmpInfo();
    }
    //</editor-fold>
    //</editor-fold>
    //////////////////////////////////
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalResources">

    protected GlobalResources getGlobalResources() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalResources) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalResources");
    }// </editor-fold>
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        ddl_verif_status = null;
        setTblEmpInfo();
    }
    //</editor-fold>
}
