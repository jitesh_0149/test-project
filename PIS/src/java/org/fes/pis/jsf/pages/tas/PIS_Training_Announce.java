/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.tas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.validation.ConstraintViolationException;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.*;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;
import org.fes.pis.sessions.FtasCategoryMstFacadeLocal;
import org.fes.pis.sessions.FtasTrainingMstFacadeLocal;
import org.fes.pis.sessions.SystOrgUnitMstFacadeLocal;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author jitesh
 */
@ManagedBean(name = "PIS_Training_Announce")
@ViewScoped
public class PIS_Training_Announce implements Serializable {

    @EJB
    private FtasTrainingMstFacadeLocal ftasTrainingMstFacade;
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    @EJB
    private SystOrgUnitMstFacadeLocal systOrgUnitMstFacade;

    
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="EJB Declarations">
    @EJB
    private FtasCategoryMstFacadeLocal ftasCategoryMstFacade;
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ddl_announce_type">
    String ddl_announce_type;
    List<SelectItem> ddl_announce_type_options;

    public String getDdl_announce_type() {
        return ddl_announce_type;
    }

    public void setDdl_announce_type(String ddl_announce_type) {
        this.ddl_announce_type = ddl_announce_type;
    }

    public List<SelectItem> getDdl_announce_type_options() {
        return ddl_announce_type_options;
    }

    public void setDdl_announce_type_options(List<SelectItem> ddl_announce_type_options) {
        this.ddl_announce_type_options = ddl_announce_type_options;
    }

    public void setDdl_announce_type() {
        ddl_announce_type_options = new ArrayList<SelectItem>();
        List<FtasCategoryMst> lst_cat_option = ftasCategoryMstFacade.find_category("ANNO");
        if (lst_cat_option.isEmpty()) {
            ddl_announce_type_options.add(new SelectItem(null, "None"));
        } else {
            ddl_announce_type_options.add(new SelectItem(null, "---Select One---"));
            Integer n = lst_cat_option.size();
            for (int i = 0; i < n; i++) {
                ddl_announce_type_options.add(new SelectItem(lst_cat_option.get(i).getCatmSrgKey(), lst_cat_option.get(i).getCatValueDesc()));
            }
        }
        ddl_announce_type = ddl_announce_type_options.get(0).toString();

    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ddl_category">
    String ddl_category;
    List<SelectItem> ddl_category_options;

    public String getDdl_category() {
        return ddl_category;
    }

    public void setDdl_category(String ddl_category) {
        this.ddl_category = ddl_category;
    }

    public List<SelectItem> getDdl_category_options() {
        return ddl_category_options;
    }

    public void setDdl_category_options(List<SelectItem> ddl_category_options) {
        this.ddl_category_options = ddl_category_options;
    }

    public void setDdl_category_options() {
        ddl_category_options = new ArrayList<SelectItem>();
        List<FtasCategoryMst> lst_cat_option = ftasCategoryMstFacade.find_category("ANCT");
        if (lst_cat_option.isEmpty()) {
            ddl_category_options.add(new SelectItem(null, "None"));
        } else {
            ddl_category_options.add(new SelectItem(null, "---Select One---"));
            Integer n = lst_cat_option.size();
            for (int i = 0; i < n; i++) {
                ddl_category_options.add(new SelectItem(lst_cat_option.get(i).getCatmSrgKey(), lst_cat_option.get(i).getCatValueDesc()));
            }
        }
        ddl_category = ddl_category_options.get(0).toString();
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_subject"> 
    String txt_subject;

    public String getTxt_subject() {
        return txt_subject;
    }

    public void setTxt_subject(String txt_subject) {
        this.txt_subject = txt_subject;
    }
    //</editor-fold>     
    //<editor-fold defaultstate="collapsed" desc="coordinator's details">
    // <editor-fold defaultstate="collapsed" desc="ddl_training_salutation">
    String ddl_training_salutation;
    List<SelectItem> ddl_training_salutation_options;

    public String getDdl_training_salutation() {
        return ddl_training_salutation;
    }

    public void setDdl_training_salutation(String ddl_training_salutation) {
        this.ddl_training_salutation = ddl_training_salutation;
    }

    public List<SelectItem> getDdl_training_salutation_options() {
        return ddl_training_salutation_options;
    }

    public void setDdl_training_salutation_options(List<SelectItem> ddl_training_salutation_options) {
        this.ddl_training_salutation_options = ddl_training_salutation_options;
    }

    public void setDdl_training_salutation_options() {
        ddl_training_salutation_options = new ArrayList<SelectItem>();
        List<FtasCategoryMst> lst_cat_option = ftasCategoryMstFacade.find_category("SLTN");
        if (lst_cat_option.isEmpty()) {
            ddl_training_salutation_options.add(new SelectItem("None"));
        } else {
            Integer n = lst_cat_option.size();
            for (int i = 0; i < n; i++) {
                ddl_training_salutation_options.add(new SelectItem(lst_cat_option.get(i).getCatmSrgKey(), lst_cat_option.get(i).getCatValueDesc()));
            }
        }
        ddl_training_salutation = ddl_training_salutation_options.get(0).getValue().toString();
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_training_co_first_name">
    String txt_training_co_first_name;

    public String getTxt_training_co_first_name() {
        return txt_training_co_first_name;
    }

    public void setTxt_training_co_first_name(String txt_training_co_first_name) {
        this.txt_training_co_first_name = txt_training_co_first_name;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_training_co_last_name">
    String txt_training_co_last_name;

    public String getTxt_training_co_last_name() {
        return txt_training_co_last_name;
    }

    public void setTxt_training_co_last_name(String txt_training_co_last_name) {
        this.txt_training_co_last_name = txt_training_co_last_name;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_training_announcement_co_contact_no">
    String txt_training_announcement_co_contact_no;

    public String getTxt_training_announcement_co_contact_no() {
        return txt_training_announcement_co_contact_no;
    }

    public void setTxt_training_announcement_co_contact_no(String txt_training_announcement_co_contact_no) {
        this.txt_training_announcement_co_contact_no = txt_training_announcement_co_contact_no;
    }
    //</editor-fold>
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="Institute fields">
    //<editor-fold defaultstate="collapsed" desc="txt_institute_name">
    String txt_institute_name;

    public String getTxt_institute_name() {
        return txt_institute_name;
    }

    public void setTxt_institute_name(String txt_institute_name) {
        this.txt_institute_name = txt_institute_name;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_institute_address">
    String txt_institute_address;

    public String getTxt_institute_address() {
        return txt_institute_address;
    }

    public void setTxt_institute_address(String txt_institute_address) {
        this.txt_institute_address = txt_institute_address;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_institute_city">
    String txt_institute_city;

    public String getTxt_institute_city() {
        return txt_institute_city;
    }

    public void setTxt_institute_city(String txt_institute_city) {
        this.txt_institute_city = txt_institute_city;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_institute_state">
    String txt_institute_state;

    public String getTxt_institute_state() {
        return txt_institute_state;
    }

    public void setTxt_institute_state(String txt_institute_state) {
        this.txt_institute_state = txt_institute_state;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_institute_pincode">
    String txt_institute_pincode;

    public String getTxt_institute_pincode() {
        return txt_institute_pincode;
    }

    public void setTxt_institute_pincode(String txt_institute_pincode) {
        this.txt_institute_pincode = txt_institute_pincode;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_institute_country">
    String txt_institute_country;

    public String getTxt_institute_country() {
        return txt_institute_country;
    }

    public void setTxt_institute_country(String txt_institute_country) {
        this.txt_institute_country = txt_institute_country;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_institute_contacts">
    String txt_institute_contacts;

    public String getTxt_institute_contacts() {
        return txt_institute_contacts;
    }

    public void setTxt_institute_contacts(String txt_institute_contacts) {
        this.txt_institute_contacts = txt_institute_contacts;
    }
    //</editor-fold>'
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="Venue Fields">
    //<editor-fold defaultstate="collapsed" desc="txt_venue_name">
    String txt_venue_name;

    public String getTxt_venue_name() {
        return txt_venue_name;
    }

    public void setTxt_venue_name(String txt_venue_name) {
        this.txt_venue_name = txt_venue_name;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_venue_address">
    String txt_venue_address;

    public String getTxt_venue_address() {
        return txt_venue_address;
    }

    public void setTxt_venue_address(String txt_venue_address) {
        this.txt_venue_address = txt_venue_address;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_venue_city">
    String txt_venue_city;

    public String getTxt_venue_city() {
        return txt_venue_city;
    }

    public void setTxt_venue_city(String txt_venue_city) {
        this.txt_venue_city = txt_venue_city;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_venue_contacts">
    String txt_venue_contacts;

    public String getTxt_venue_contacts() {
        return txt_venue_contacts;
    }

    public void setTxt_venue_contacts(String txt_venue_contacts) {
        this.txt_venue_contacts = txt_venue_contacts;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_venue_pincode">
    String txt_venue_pincode;

    public String getTxt_venue_pincode() {
        return txt_venue_pincode;
    }

    public void setTxt_venue_pincode(String txt_venue_pincode) {
        this.txt_venue_pincode = txt_venue_pincode;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_venue_state">
    String txt_venue_state;

    public String getTxt_venue_state() {
        return txt_venue_state;
    }

    public void setTxt_venue_state(String txt_venue_state) {
        this.txt_venue_state = txt_venue_state;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_venue_country">
    String txt_venue_country;

    public String getTxt_venue_country() {
        return txt_venue_country;
    }

    public void setTxt_venue_country(String txt_venue_country) {
        this.txt_venue_country = txt_venue_country;
    }
    //</editor-fold>
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="Objective and fees fields"> 
    //<editor-fold defaultstate="collapsed" desc="txt_objective">
    String txt_objective;

    public String getTxt_objective() {
        return txt_objective;
    }

    public void setTxt_objective(String txt_objective) {
        this.txt_objective = txt_objective;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_topics">
    String txt_topics;

    public String getTxt_topics() {
        return txt_topics;
    }

    public void setTxt_topics(String txt_topics) {
        this.txt_topics = txt_topics;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_benefits">
    String txt_benefits;

    public String getTxt_benefits() {
        return txt_benefits;
    }

    public void setTxt_benefits(String txt_benefits) {
        this.txt_benefits = txt_benefits;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_eligibility">
    String txt_eligibility;

    public String getTxt_eligibility() {
        return txt_eligibility;
    }

    public void setTxt_eligibility(String txt_eligibility) {
        this.txt_eligibility = txt_eligibility;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_tuition_fees">
    String txt_tuition_fees = "0";

    public String getTxt_tuition_fees() {
        return txt_tuition_fees;
    }

    public void setTxt_tuition_fees(String txt_tuition_fees) {
        this.txt_tuition_fees = txt_tuition_fees;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_tuition_lodging">
    String txt_tuition_lodging = "0";

    public String getTxt_tuition_lodging() {
        return txt_tuition_lodging;
    }

    public void setTxt_tuition_lodging(String txt_tuition_lodging) {
        this.txt_tuition_lodging = txt_tuition_lodging;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_tuition_lodging_boarding">
    String txt_tuition_lodging_boarding = "0";

    public String getTxt_tuition_lodging_boarding() {
        return txt_tuition_lodging_boarding;
    }

    public void setTxt_tuition_lodging_boarding(String txt_tuition_lodging_boarding) {
        this.txt_tuition_lodging_boarding = txt_tuition_lodging_boarding;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_other_fees_desc">
    String txt_other_fees_desc;

    public String getTxt_other_fees_desc() {
        return txt_other_fees_desc;
    }

    public void setTxt_other_fees_desc(String txt_other_fees_desc) {
        this.txt_other_fees_desc = txt_other_fees_desc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_other_fees_amount">
    String txt_other_fees_amount = "0";

    public String getTxt_other_fees_amount() {
        return txt_other_fees_amount;
    }

    public void setTxt_other_fees_amount(String txt_other_fees_amount) {
        this.txt_other_fees_amount = txt_other_fees_amount;
    }
    //</editor-fold>
    //</editor-fold>   
    //<editor-fold defaultstate="collapsed" desc="Table Schedule Details">
    //<editor-fold defaultstate="collapsed" desc="cal_training_from_date">
    Date cal_training_from_date;
    String cal_training_from_date_min;
    String cal_training_from_date_max;

    public String getCal_training_from_date_max() {
        return cal_training_from_date_max;
    }

    public void setCal_training_from_date_max(String cal_training_from_date_max) {
        this.cal_training_from_date_max = cal_training_from_date_max;
    }

    public String getCal_training_from_date_min() {
        return cal_training_from_date_min;
    }

    public void setCal_training_from_date_min(String cal_training_from_date_min) {
        this.cal_training_from_date_min = cal_training_from_date_min;
    }

    public Date getCal_training_from_date() {
        return cal_training_from_date;
    }

    public void setCal_training_from_date(Date cal_training_from_date) {
        this.cal_training_from_date = cal_training_from_date;
    }

    private void setCal_training_from_date() {
        if (tbl_training_ann_detail == null || tbl_training_ann_detail.isEmpty()) {
            cal_training_from_date = null;
            cal_training_from_date_min = null;
            cal_training_from_date_max = null;
        } else {
            int n = tbl_training_ann_detail.size();
            cal_training_from_date = tbl_training_ann_detail.get(n - 1).getToDatetime();
            cal_training_from_date_min = DateTimeUtility.ChangeDateFormat(cal_training_from_date, "dd-MMM-yyyy hh:mm");
            cal_training_to_date_min = DateTimeUtility.ChangeDateFormat(cal_training_from_date, "dd-MMM-yyyy hh:mm");
        }

        setCal_training_to_date();
    }

    public void cal_training_from_date_changed(SelectEvent event) {
        cal_training_from_date = event.getObject() == null ? null : (Date) event.getObject();
        setCal_training_to_date();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_training_to_date">
    Date cal_training_to_date;
    String cal_training_to_date_min;
    String cal_training_to_date_max;

    public String getCal_training_to_date_max() {
        return cal_training_to_date_max;
    }

    public void setCal_training_to_date_max(String cal_training_to_date_max) {
        this.cal_training_to_date_max = cal_training_to_date_max;
    }

    public String getCal_training_to_date_min() {
        return cal_training_to_date_min;
    }

    public void setCal_training_to_date_min(String cal_training_to_date_min) {
        this.cal_training_to_date_min = cal_training_to_date_min;
    }

    public Date getCal_training_to_date() {
        return cal_training_to_date;
    }

    public void setCal_training_to_date(Date cal_training_to_date) {
        this.cal_training_to_date = cal_training_to_date;
    }

    public void setCal_training_to_date() {
        if (cal_training_from_date == null) {
            cal_training_to_date_min = null;
        } else {
            cal_training_to_date_min = DateTimeUtility.ChangeDateFormat(cal_training_from_date, "dd-MMM-yyyy hh:mm");
        }
        cal_training_to_date = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_training_description">
    String txt_training_description;

    public String getTxt_training_description() {
        return txt_training_description;
    }

    public void setTxt_training_description(String txt_training_description) {
        this.txt_training_description = txt_training_description;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_add_training_detail_clicked">

    public void btn_add_training_detail_clicked(ActionEvent event) {
        RequestContext.getCurrentInstance().execute("pw_dlg_pnl_new_training_entry.show()");
        setCal_training_from_date();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_training_add_to_list_action">

    public void btn_training_add_to_list_action(ActionEvent event) {
        Date v_from;
        Date v_to;
        if (tbl_training_ann_detail == null) {
            tbl_training_ann_detail = new ArrayList<FtasTrainingAnnDetail>();
            f_from_date = DateTimeUtility.ChangeDateFormat(cal_training_from_date, "dd-MMM-yyyy hh:mm");
        }
        FtasTrainingAnnDetail ent_TrainingAnnDtl = new FtasTrainingAnnDetail(new BigDecimal(tbl_training_ann_detail.size()));

        ent_TrainingAnnDtl.setTradSrgKey(new BigDecimal(tbl_training_ann_detail.size()));
        ent_TrainingAnnDtl.setFromDatetime(cal_training_from_date);
        ent_TrainingAnnDtl.setToDatetime(cal_training_to_date);
        f_to_date = DateTimeUtility.ChangeDateFormat(cal_training_to_date, "dd-MMM-yyyy hh:mm");
        ent_TrainingAnnDtl.setDescription(txt_training_description.toUpperCase());
        tbl_training_ann_detail.add(ent_TrainingAnnDtl);
        v_from = tbl_training_ann_detail.get(0).getFromDatetime();
        v_to = cal_training_to_date;
        Integer v_total_days = setDays(v_from, v_to);
        f_total_days = String.valueOf(v_total_days);
        resetTrainingDetail();
        RequestContext.getCurrentInstance().execute("pw_dlg_pnl_new_training_entry.hide()");
    }

    // <editor-fold defaultstate="collapsed" desc="setDays">
    public int setDays(Date p_fromDate, Date p_toDate) {
        Date d1 = p_fromDate;
        java.util.Calendar temp_cal = java.util.Calendar.getInstance();
        temp_cal.set(d1.getYear(), d1.getMonth(), d1.getDate());
        d1 = temp_cal.getTime();
        Date d2 = p_toDate;
        temp_cal.set(d2.getYear(), d2.getMonth(), d2.getDate());
        d2 = temp_cal.getTime();
        double v_diff_limit;
        float temp_newdiff;
        v_diff_limit = d2.getTime() - d1.getTime();
        temp_newdiff = (int) (v_diff_limit / 86400000);
        temp_newdiff += 1;
        return (int) temp_newdiff;
    }// </editor-fold>

    public void resetTrainingDetail() {
        cal_training_from_date = null;
        cal_training_to_date = null;
        txt_training_description = null;

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_training_delete_action">

    public void btn_training_delete_action(FtasTrainingAnnDetail ent) {
        tbl_training_ann_detail.remove(ent);
        setCal_training_from_date();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="footer attributes">
    String f_from_date;
    String f_to_date;
    String f_total_days;

    public String getF_from_date() {
        return f_from_date;
    }

    public void setF_from_date(String f_from_date) {
        this.f_from_date = f_from_date;
    }

    public String getF_to_date() {
        return f_to_date;
    }

    public void setF_to_date(String f_to_date) {
        this.f_to_date = f_to_date;
    }

    public String getF_total_days() {
        return f_total_days;
    }

    public void setF_total_days(String f_total_days) {
        this.f_total_days = f_total_days;

    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="tbl_training_ann_detail">
    List<FtasTrainingAnnDetail> tbl_training_ann_detail;

    public List<FtasTrainingAnnDetail> getTbl_training_ann_detail() {
        return tbl_training_ann_detail;
    }

    public void setTbl_training_ann_detail(List<FtasTrainingAnnDetail> tbl_training_ann_detail) {
        this.tbl_training_ann_detail = tbl_training_ann_detail;
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="selectedOrgUnit"> 
    String[] selectedOrgUnit;

    public String[] getSelectedOrgUnit() {
        return selectedOrgUnit;
    }

    public void setSelectedOrgUnit(String[] selectedOrgUnit) {
        this.selectedOrgUnit = selectedOrgUnit;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_action_by">
    String txt_announce_by;
    FhrdEmpmst selectedAnnounceBy;
    String err_txt_announce_by;

    public String getErr_txt_announce_by() {
        return err_txt_announce_by;
    }

    public void setErr_txt_announce_by(String err_txt_announce_by) {
        this.err_txt_announce_by = err_txt_announce_by;
    }

    public FhrdEmpmst getSelectedAnnounceBy() {
        return selectedAnnounceBy;
    }

    public void setSelectedAnnounceBy(FhrdEmpmst selectedAnnounceBy) {
        this.selectedAnnounceBy = selectedAnnounceBy;
    }

    public String getTxt_announce_by() {
        return txt_announce_by;
    }

    public void setTxt_announce_by(String txt_announce_by) {
        this.txt_announce_by = txt_announce_by;
    }

    public void txt_announce_by_processValueChange(ValueChangeEvent event) {
        txt_announce_by = event.getNewValue() == null ? null : event.getNewValue().toString();
        err_txt_announce_by = null;
        if (!txt_announce_by.isEmpty()) {
            selectedAnnounceBy = fhrdEmpmstFacade.find(Integer.valueOf(event.getNewValue().toString()));
            if (selectedAnnounceBy == null) {
                err_txt_announce_by = "User Does Not Exist";
            }

        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="chkOrgUnit_options"> 
    List<SelectItem> chkOrgUnit_options;

    public List<SelectItem> getChkOrgUnit_options() {
        return chkOrgUnit_options;
    }

    public void setChkOrgUnit_options(List<SelectItem> chkOrgUnit_options) {
        this.chkOrgUnit_options = chkOrgUnit_options;
    }

    public void setChkOrgUnit_options() {
        chkOrgUnit_options = new ArrayList<SelectItem>();
        List<SystOrgUnitMst> lstSystOrgUnitMsts = systOrgUnitMstFacade.findPostingOuForNewEmployee(globalData.getWorking_ou());
        if (lstSystOrgUnitMsts.isEmpty()) {
            SelectItem s = new SelectItem(null, "---Not Found---");
            chkOrgUnit_options.add(s);
        } else {
            Integer n = lstSystOrgUnitMsts.size();
            for (int i = 0; i < n; i++) {
                chkOrgUnit_options.add(new SelectItem(lstSystOrgUnitMsts.get(i).getOumUnitSrno(), lstSystOrgUnitMsts.get(i).getOumName()));
            }
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="btn_save_action"> 
    public void btn_save_action(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();


        FtasTrainingMst entFtasTrainingMst = new FtasTrainingMst();
        Integer v_cal_year = DateTimeUtility.stringToDate(f_from_date, "dd-MMM-yyyy hh:mm").getYear() + 1900;
        BigDecimal v_trann_srgkey = getGlobalSettings().getUniqueSrno(v_cal_year, "FTAS_TRAINING_MST");
        try {
            entFtasTrainingMst.setTrannSrgKey(v_trann_srgkey);
            FhrdEmpmst ent_em_ann = new FhrdEmpmst(Integer.valueOf(txt_announce_by));
            entFtasTrainingMst.setAnnounceBy(ent_em_ann);
            entFtasTrainingMst.setAnnouncementFlag('Y');
            entFtasTrainingMst.setAnnouncementType(new FtasCategoryMst(new BigDecimal(ddl_announce_type)));
            entFtasTrainingMst.setCategoryType(new FtasCategoryMst(new BigDecimal(ddl_category)));
            entFtasTrainingMst.setSubject(txt_subject.toUpperCase());
            entFtasTrainingMst.setCoOrdinatorSalut(new FtasCategoryMst(new BigDecimal(ddl_training_salutation)));
            entFtasTrainingMst.setCoOrdinatorFName(txt_training_co_first_name.toUpperCase());
            entFtasTrainingMst.setCoOrdinatorLName(txt_training_co_last_name.toUpperCase());
            entFtasTrainingMst.setCoOrdinatorContact(txt_training_announcement_co_contact_no);
            entFtasTrainingMst.setFromDate(DateTimeUtility.stringToDate(f_from_date, "dd-MMM-yyyy hh:mm"));
            entFtasTrainingMst.setToDate(DateTimeUtility.stringToDate(f_to_date, "dd-MMM-yyyy hh:mm"));
            entFtasTrainingMst.setTotalDays(new BigDecimal(f_total_days));
            entFtasTrainingMst.setInstituteAddress(txt_institute_address.toUpperCase());
            entFtasTrainingMst.setInstituteCity(txt_institute_city.toUpperCase());
            entFtasTrainingMst.setInstituteState(txt_institute_state.toUpperCase());
            entFtasTrainingMst.setInstituteCountry(txt_institute_country.toUpperCase());
            entFtasTrainingMst.setInstituteContact(txt_institute_contacts.toUpperCase());
            entFtasTrainingMst.setInstituteName(txt_institute_name.toUpperCase());
            entFtasTrainingMst.setVenueName(txt_venue_name.toUpperCase());
            entFtasTrainingMst.setInstitutePincode(txt_institute_pincode);
            entFtasTrainingMst.setTuitionFees(Long.valueOf(txt_tuition_fees));
            entFtasTrainingMst.setTuitionLodgingFees(Long.valueOf(txt_tuition_lodging));
            entFtasTrainingMst.setTuitionLodgingBoardingFees(Long.valueOf(txt_tuition_lodging_boarding));
            if (!txt_other_fees_desc.isEmpty()) {
                entFtasTrainingMst.setOtherFeesDesc(txt_other_fees_desc);
                entFtasTrainingMst.setOtherFeesAmount(Long.valueOf(txt_other_fees_amount));
            }

            entFtasTrainingMst.setVenueAddress(txt_venue_address.toUpperCase());
            entFtasTrainingMst.setVenueCity(txt_venue_city.toUpperCase());
            entFtasTrainingMst.setVenueContact(txt_venue_contacts.toUpperCase());
            entFtasTrainingMst.setVenueCountry(txt_venue_country.toUpperCase());
            entFtasTrainingMst.setVenuePincode(txt_venue_pincode);
            entFtasTrainingMst.setVenueState(txt_venue_state.toUpperCase());

            entFtasTrainingMst.setBenefits(txt_benefits.toUpperCase());
            entFtasTrainingMst.setEligibility(txt_eligibility.toUpperCase());
            entFtasTrainingMst.setObjective(txt_objective.toUpperCase());
            entFtasTrainingMst.setTopicsIncluded(txt_topics.toUpperCase());
            entFtasTrainingMst.setCreateby(globalData.getEnt_login_user());
            entFtasTrainingMst.setCreatedt(new Date());
            entFtasTrainingMst.setOumUnitSrno(globalData.getWorking_ou());
            entFtasTrainingMst.setTranYear(Short.valueOf(v_cal_year.toString()));
            // <editor-fold defaultstate="collapsed" desc="FtasTrainingOrgDtl">
            int v_orgunit_length = selectedOrgUnit.length;
            List<FtasTrainingOrgUnitDtl> lst_FtasTrainingOrgDtls = new ArrayList<FtasTrainingOrgUnitDtl>();
            for (int i = 0; i < v_orgunit_length; i++) {
                FtasTrainingOrgUnitDtl ent_ftasOrgDtl = new FtasTrainingOrgUnitDtl();
                BigDecimal v_troud_srgkey = getGlobalSettings().getUniqueSrno(v_cal_year, "FTAS_TRAINING_ORG_UNIT_DTL");

                ent_ftasOrgDtl.setTroudSrgKey(v_troud_srgkey);
                ent_ftasOrgDtl.setSystOrgUnitSrno(Integer.valueOf(selectedOrgUnit[i].toString()));
                ent_ftasOrgDtl.setTrannSrgKey(entFtasTrainingMst);
                ent_ftasOrgDtl.setSubSrno(i + 1);
                ent_ftasOrgDtl.setCreateby(globalData.getEnt_login_user());
                ent_ftasOrgDtl.setOumUnitSrno(globalData.getEnt_working_ou());
                ent_ftasOrgDtl.setCreatedt(new Date());
                ent_ftasOrgDtl.setTranYear(Short.valueOf(v_cal_year.toString()));
                lst_FtasTrainingOrgDtls.add(ent_ftasOrgDtl);
            }// </editor-fold>
            // <editor-fold defaultstate="collapsed" desc="FtasScheduleDtl">
            List<FtasTrainingAnnDetail> lst_FtasAnnDtls = new ArrayList<FtasTrainingAnnDetail>();
            List<FtasTrainingAnnDetail> lst_FtasAnnDtl_temp = tbl_training_ann_detail;
            for (int i = 0; i < lst_FtasAnnDtl_temp.size(); i++) {
                FtasTrainingAnnDetail entFtasAnnDtl = new FtasTrainingAnnDetail();
                BigDecimal v_trad_srgkey = getGlobalSettings().getUniqueSrno(v_cal_year, "FTAS_TRAINING_ANN_DETAIL");
                entFtasAnnDtl.setTradSrgKey(v_trad_srgkey);
                entFtasAnnDtl.setTrannSrgKey(entFtasTrainingMst);
                entFtasAnnDtl.setSubSrno(i + 1);
                entFtasAnnDtl.setCreateby(globalData.getEnt_login_user());
                entFtasAnnDtl.setCreatedt(new Date());
                entFtasAnnDtl.setDescription(lst_FtasAnnDtl_temp.get(i).getDescription());
                entFtasAnnDtl.setFromDatetime(lst_FtasAnnDtl_temp.get(i).getFromDatetime());
                entFtasAnnDtl.setToDatetime(lst_FtasAnnDtl_temp.get(i).getToDatetime());
                entFtasAnnDtl.setOumUnitSrno(globalData.getEnt_working_ou());
                entFtasAnnDtl.setTranYear(Short.valueOf(v_cal_year.toString()));
                lst_FtasAnnDtls.add(entFtasAnnDtl);
            }// </editor-fold>
            entFtasTrainingMst.setFtasTrainingOrgUnitDtlCollection(lst_FtasTrainingOrgDtls);
            entFtasTrainingMst.setFtasTrainingAnnDetailCollection(lst_FtasAnnDtls);
            try {
                ftasTrainingMstFacade.create(entFtasTrainingMst);
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully."));
                resetForm();
            } catch (EJBException e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "eRecord not saved.Error in Create..."));
            } catch (ConstraintViolationException e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "cRecord not saved.Error in Create..."));
            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "sRecord not saved.Error in Create..."));
            }
        } catch (Exception ex) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Record not saved.Check All the Entries..."));
        }

    }

    public void resetForm() {
        selectedAnnounceBy = null;
        selectedOrgUnit = null;
        txt_announce_by = null;
        setChkOrgUnit_options();
        setDdl_announce_type();
        setDdl_category_options();
        setDdl_training_salutation_options();
        txt_training_co_first_name = null;
        txt_training_co_last_name = null;
        txt_training_announcement_co_contact_no = null;
        txt_subject = null;


        txt_institute_address = null;
        txt_institute_city = null;
        txt_institute_contacts = null;
        txt_institute_country = null;
        txt_institute_name = null;
        txt_institute_pincode = null;
        txt_institute_state = null;

        txt_venue_address = null;
        txt_venue_city = null;
        txt_venue_contacts = null;
        txt_venue_country = null;
        txt_venue_name = null;
        txt_venue_pincode = null;
        txt_venue_state = null;

        txt_objective = null;
        txt_topics = null;
        txt_eligibility = null;
        txt_benefits = null;

        txt_tuition_fees = "0";
        txt_tuition_lodging = "0";
        txt_tuition_lodging_boarding = "0";
        txt_other_fees_desc = null;
        txt_other_fees_amount = "0";
        tbl_training_ann_detail = null;
        f_from_date = null;
        f_to_date = null;
        f_total_days = null;

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setDdl_announce_type();
        setDdl_category_options();
        setDdl_training_salutation_options();
        setChkOrgUnit_options();

    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="default constructor and Global Settings">
    //<editor-fold defaultstate="collapsed" desc="Default constructor">
    /**
     * Creates a new instance of PIS_Training_Announce
     */
    public PIS_Training_Announce() {
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
}
