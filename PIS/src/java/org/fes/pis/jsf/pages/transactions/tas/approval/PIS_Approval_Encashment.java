/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.transactions.tas.approval;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.customJpaController;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.FhrdEmpEncash;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEmpEncashFacadeLocal;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Approval_Encashment")
@RequestScoped
public class PIS_Approval_Encashment implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="EJB and other declarations">
    @EJB
    private customJpaController customJpaController;
    @EJB
    private FhrdEmpEncashFacadeLocal fhrdEmpEncashFacade;
    private LogGenerator logGenerator = new LogGenerator();
    private System_Properties system_Properties = new System_Properties();
    private String systemName = system_Properties.getSystemName();
    private Ou_Pack ou_Pack = new Ou_Pack();

    public PIS_Approval_Encashment() {
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="globalData">
    private PIS_GlobalData globalData;
    
    public PIS_GlobalData getGlobalData() {
        return globalData;
    }
    
    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_approval_list"> 
    List<FhrdEmpEncash> tbl_approval_list;
    List<FhrdEmpEncash> tbl_approval_list_filtered;
    
    public List<FhrdEmpEncash> getTbl_approval_list() {
        return tbl_approval_list;
    }
    
    public void setTbl_approval_list(List<FhrdEmpEncash> tbl_approval_list) {
        this.tbl_approval_list = tbl_approval_list;
    }
    
    public List<FhrdEmpEncash> getTbl_approval_list_filtered() {
        return tbl_approval_list_filtered;
    }
    
    public void setTbl_approval_list_filtered(List<FhrdEmpEncash> tbl_approval_list_filtered) {
        this.tbl_approval_list_filtered = tbl_approval_list_filtered;
    }
    
    public void setTbl_approval_list() {
        tbl_approval_list = fhrdEmpEncashFacade.findAllForApproval(globalData.getUser_id().toString(), globalData.getWorking_ou());
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="selectedApplications">
    FhrdEmpEncash[] selectedApplications;
    
    public FhrdEmpEncash[] getSelectedApplications() {
        return selectedApplications;
    }
    
    public void setSelectedApplications(FhrdEmpEncash[] selectedApplications) {
        this.selectedApplications = selectedApplications;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="btn_approve_clicked">
    public void btn_approve_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (selectedApplications.length > 0) {
            
            try {
                boolean isTransactionDone = customJpaController.ApproveRejectEncAppls(selectedApplications, "Y", globalData.getUser_id());
                if (isTransactionDone) {
                    try {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Application Approved", "Selected Application Approved Successfully."));
                        setTbl_approval_list();
                    } catch (Exception e) {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed reset data", "Selected Application Approved Successfully but failed reset data. Please reload the page"));
                        logGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_accept_clicked", null, e);
                    }
                } else {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during Accepting Application."));
                }
            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during Accepting Application."));
                logGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_accept_clicked", null, e);
            }
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Select Application", "Please select any Application to Approve/Reject."));
        }
        
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="btn_reject_clicked"> 
    public void btn_reject_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (selectedApplications.length > 0) {
            try {
                boolean isTransactionDone = customJpaController.ApproveRejectEncAppls(selectedApplications, "N", globalData.getUser_id());
                if (isTransactionDone) {
                    try {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Application Rejected", "Selected Application Rejected Successfully."));
                        setTbl_approval_list();
                    } catch (Exception e) {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed reset data", "Selected Application Rejected Successfully but failed reset data. Please reload the page"));
                        logGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_reject_clicked", null, e);
                    }
                } else {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during Rejecting Application."));
                }
            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during Rejecting Application."));
                logGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_reject_clicked", null, e);
            }
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Select Application", "Please select any Application to Approve/Reject."));
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="init()"> 
    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setTbl_approval_list();
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>    
}
