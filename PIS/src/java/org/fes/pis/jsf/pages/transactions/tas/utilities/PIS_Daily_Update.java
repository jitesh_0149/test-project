/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.transactions.tas.utilities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.Bulk_Att_Display;
import org.fes.pis.custom_entities.Database_Output;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.db.fes_project.pack.Ftas_Pack;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.FtasDaily;
import org.fes.pis.entities.SystOrgUnitMst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;
import org.fes.pis.sessions.FtasDailyFacadeLocal;
import org.fes.pis.sessions.SystOrgUnitMstFacadeLocal;
import org.primefaces.model.DualListModel;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Daily_Update")
@RequestScoped
public class PIS_Daily_Update implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="default constructor">
    /**
     * Creates a new instance of PIS_Daily_Update
     */
    public PIS_Daily_Update() {
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Declarations"> 

    
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    boolean isValidUser = false;
    Ou_Pack ou_Pack = new Ou_Pack();
    Ftas_Pack ftas_Pack = new Ftas_Pack();
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="EJB Declaration">
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    @EJB
    private SystOrgUnitMstFacadeLocal systOrgUnitMstFacade;
    @EJB
    private FtasDailyFacadeLocal ftasDailyFacade;
    // </editor-fold>
    // For Tab1 Daily Update
    // <editor-fold defaultstate="collapsed" desc="Binded Attributes">
    String txt_from_employee;
    String txt_to_employee;
    String txt_oum_unit;
    String min_to_date;
    String rdb_place_firsthalf_binding;
    String rdb_place_secondhalf_binding;
    String rdb_dlg_from_half;
    String rdb_dlg_to_half;
    boolean chk_for_firsthalf;
    boolean chk_all = true;
    boolean chk_for_secondhalf;
    boolean chk_from_half;
    boolean chk_to_half;
    boolean chk_presence_for = true;
    boolean chk_for_firsthalf_val = true;
    boolean chk_for_secondhalf_val = true;
    List<Bulk_Att_Display> tbl_mark_att_detail;
    List<Bulk_Att_Display> tbl_view_unsaved;
    ///for tab1
    String hdn_user_id;
    String hdn_oum;
    String hdn_att_date;
    String hdn_from_half;
    String hdn_to_half;
    String hdn_emp_list;
    String hdn_from_date;
    String hdn_to_date;
    Date cal_from_date;
    Date cal_to_date;
    List<Bulk_Att_Display> tbl_lst_FtasDaily;
    DualListModel<String> picklst_emp = new DualListModel<String>();

    // <editor-fold defaultstate="collapsed" desc="Getter/Setter mathods">
    public boolean getChk_all() {
        return chk_all;
    }

    public void setChk_all(boolean chk_all) {
        this.chk_all = chk_all;
    }

    public String getTxt_from_employee() {
        return txt_from_employee;
    }

    public void setTxt_from_employee(String txt_from_employee) {
        this.txt_from_employee = txt_from_employee;
    }

    public Date getCal_from_date() {
        return cal_from_date;
    }

    public void setCal_from_date(Date cal_from_date) {
        this.cal_from_date = cal_from_date;
    }

    public Date getCal_to_date() {
        return cal_to_date;
    }

    public void setCal_to_date(Date cal_to_date) {
        this.cal_to_date = cal_to_date;
    }

    public String getTxt_oum_unit() {
        return txt_oum_unit;
    }

    public void setTxt_oum_unit(String txt_oum_unit) {
        this.txt_oum_unit = txt_oum_unit;
    }

    public String getTxt_to_employee() {
        return txt_to_employee;
    }

    public void setTxt_to_employee(String txt_to_employee) {
        this.txt_to_employee = txt_to_employee;
    }

    public boolean getChk_for_firsthalf() {
        return chk_for_firsthalf;
    }

    public void setChk_for_firsthalf(boolean chk_for_firsthalf) {
        this.chk_for_firsthalf = chk_for_firsthalf;
    }

    public boolean isChk_for_firsthalf_val() {
        return chk_for_firsthalf_val;
    }

    public void setChk_for_firsthalf_val(boolean chk_for_firsthalf_val) {
        this.chk_for_firsthalf_val = chk_for_firsthalf_val;
    }

    public boolean getChk_for_secondhalf() {
        return chk_for_secondhalf;
    }

    public void setChk_for_secondhalf(boolean chk_for_secondhalf) {
        this.chk_for_secondhalf = chk_for_secondhalf;
    }

    public boolean isChk_for_secondhalf_val() {
        return chk_for_secondhalf_val;
    }

    public void setChk_for_secondhalf_val(boolean chk_for_secondhalf_val) {
        this.chk_for_secondhalf_val = chk_for_secondhalf_val;
    }

    public String getMin_to_date() {
        return min_to_date;
    }

    public void setMin_to_date(String min_to_date) {
        this.min_to_date = min_to_date;
    }

    public DualListModel<String> getPicklst_emp() {
        if (picklst_emp.getSource() == null) {
            picklst_emp = new DualListModel<String>(emplistSource, emplistTarget);
        }
        return picklst_emp;
    }

    public void setPicklst_emp(DualListModel<String> picklst_emp) {
        this.picklst_emp = picklst_emp;
    }

    public boolean getChk_presence_for() {
        return chk_presence_for;
    }

    public void setChk_presence_for(boolean chk_presence_for) {
        this.chk_presence_for = chk_presence_for;
    }

    public String getRdb_place_firsthalf_binding() {
        return rdb_place_firsthalf_binding;
    }

    public void setRdb_place_firsthalf_binding(String rdb_place_firsthalf_binding) {
        this.rdb_place_firsthalf_binding = rdb_place_firsthalf_binding;
    }

    public String getRdb_place_secondhalf_binding() {
        return rdb_place_secondhalf_binding;
    }

    public void setRdb_place_secondhalf_binding(String rdb_place_secondhalf_binding) {
        this.rdb_place_secondhalf_binding = rdb_place_secondhalf_binding;
    }

    public List<Bulk_Att_Display> getTbl_mark_att_detail() {
        return tbl_mark_att_detail;
    }

    public void setTbl_mark_att_detail(List<Bulk_Att_Display> tbl_mark_att_detail) {
        this.tbl_mark_att_detail = tbl_mark_att_detail;
    }

    public List<Bulk_Att_Display> getTbl_lst_FtasDaily() {
        return tbl_lst_FtasDaily;
    }

    public void setTbl_lst_FtasDaily(List<Bulk_Att_Display> tbl_lst_FtasDaily) {
        this.tbl_lst_FtasDaily = tbl_lst_FtasDaily;
    }

    public boolean getChk_from_half() {
        return chk_from_half;
    }

    public void setChk_from_half(boolean chk_from_half) {
        this.chk_from_half = chk_from_half;
    }

    public boolean getChk_to_half() {
        return chk_to_half;
    }

    public void setChk_to_half(boolean chk_to_half) {
        this.chk_to_half = chk_to_half;
    }

    public String getHdn_att_date() {
        return hdn_att_date;
    }

    public void setHdn_att_date(String hdn_att_date) {
        this.hdn_att_date = hdn_att_date;
    }

    public String getHdn_oum() {
        return hdn_oum;
    }

    public void setHdn_oum(String hdn_oum) {
        this.hdn_oum = hdn_oum;
    }

    public String getHdn_user_id() {
        return hdn_user_id;
    }

    public void setHdn_user_id(String hdn_user_id) {
        this.hdn_user_id = hdn_user_id;
    }

    public String getRdb_dlg_from_half() {
        return rdb_dlg_from_half;
    }

    public void setRdb_dlg_from_half(String rdb_dlg_from_half) {
        this.rdb_dlg_from_half = rdb_dlg_from_half;
    }

    public String getRdb_dlg_to_half() {
        return rdb_dlg_to_half;
    }

    public void setRdb_dlg_to_half(String rdb_dlg_to_half) {
        this.rdb_dlg_to_half = rdb_dlg_to_half;
    }

    public String getHdn_from_half() {
        return hdn_from_half;
    }

    public void setHdn_from_half(String hdn_from_half) {
        this.hdn_from_half = hdn_from_half;
    }

    public String getHdn_to_half() {
        return hdn_to_half;
    }

    public void setHdn_to_half(String hdn_to_half) {
        this.hdn_to_half = hdn_to_half;
    }

    public List<Bulk_Att_Display> getTbl_view_unsaved() {
        return tbl_view_unsaved;
    }

    public void setTbl_view_unsaved(List<Bulk_Att_Display> tbl_view_unsaved) {
        this.tbl_view_unsaved = tbl_view_unsaved;
    }

    public String getHdn_emp_list() {
        return hdn_emp_list;
    }

    public void setHdn_emp_list(String hdn_emp_list) {
        this.hdn_emp_list = hdn_emp_list;
    }

    public String getHdn_from_date() {
        return hdn_from_date;
    }

    public void setHdn_from_date(String hdn_from_date) {
        this.hdn_from_date = hdn_from_date;
    }

    public String getHdn_to_date() {
        return hdn_to_date;
    }

    public void setHdn_to_date(String hdn_to_date) {
        this.hdn_to_date = hdn_to_date;
    }
    // </editor-fold>
    // </editor-fold>
    // value of dropdown lists...............................
    // <editor-fold defaultstate="collapsed" desc="List of Daily Attendance of Employees">
    private List<FtasDaily> lst_FtasDaily;

    public List<FtasDaily> getLst_FtasDaily() {
        return lst_FtasDaily;
    }

    public void setLst_FtasDaily(List<FtasDaily> lst_FtasDaily) {
        this.lst_FtasDaily = lst_FtasDaily;
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Mark List of Daily Attendance of Employees">
    private List<FtasDaily> lst_FtasDaily_mark_att_detail;

    public List<FtasDaily> getLst_FtasDaily_mark_att_detail() {
        return lst_FtasDaily_mark_att_detail;
    }

    public void setLst_FtasDaily_mark_att_detail(List<FtasDaily> lst_FtasDaily_mark_att_detail) {
        this.lst_FtasDaily_mark_att_detail = lst_FtasDaily_mark_att_detail;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="list of ddl org unit list binding tab1">
    String ddl_org_unit_list;
    List<SelectItem> ddl_org_unit_list_options;

    public List<SelectItem> getDdl_org_unit_list_options() {
        return ddl_org_unit_list_options;
    }

    public void setDdl_org_unit_list_options(List<SelectItem> ddl_org_unit_list_options) {
        this.ddl_org_unit_list_options = ddl_org_unit_list_options;
    }

    public String getDdl_org_unit_list() {
        return ddl_org_unit_list;
    }

    public void setDdl_org_unit_list(String ddl_org_unit_list) {
        this.ddl_org_unit_list = ddl_org_unit_list;
    }

    public void setDdl_org_unit_list() {
        List<SelectItem> lst_ou_option;
        lst_ou_option = setOrgList();
        ddl_org_unit_list_options = lst_ou_option;
        ddl_org_unit_list = lst_ou_option.get(0).toString();
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employee list pick-list for tab1">
    List<String> emplistSource = new ArrayList<String>();
    List<String> emplistTarget = new ArrayList<String>();
    private DualListModel<String> employee_list;

    public DualListModel<String> getEmployee_list() {
        return employee_list;
    }

    public void setEmployee_list(DualListModel<String> employee_list) {
        this.employee_list = employee_list;
    }// </editor-fold>

    // valuechange events.....................................
    // <editor-fold defaultstate="collapsed" desc="ddl orgunitlist valuechange event tab1">
    public void ddl_org_unit_list_valuechange(ValueChangeEvent event) {
        ddl_org_unit_list = event.getNewValue().toString();
        if (event.getNewValue().toString().equalsIgnoreCase("") == false) {
            if (chk_all == false) {
                emplistSource.clear();
                String v_oum_srno = ddl_org_unit_list;
                try {
                    List<FhrdEmpmst> lst_temp = fhrdEmpmstFacade.findEmpListForBulkAtt(Integer.valueOf(v_oum_srno), cal_from_date, cal_to_date);
                    Integer n = lst_temp.size();
                    for (int i = 0; i < n; i++) {
                        emplistSource.add(lst_temp.get(i).getUserId() + "-" + lst_temp.get(i).getUserName());
                    }
                } catch (Exception e) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "ddl_org_unit_list_valuechange", null, e);
                }
                employee_list = new DualListModel<String>(emplistSource, emplistTarget);

            } else {
                emplistSource.clear();
                emplistTarget.clear();
                employee_list = new DualListModel<String>(emplistSource, emplistTarget);
            }
        } else {
            emplistSource.clear();
            emplistTarget.clear();
            employee_list = new DualListModel<String>(emplistSource, emplistTarget);
        }
        picklst_emp = employee_list;
    }// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="chk all valuechange event for tab1">
    public void chk_all_valuechange(ValueChangeEvent event) {
        if (event.getNewValue().toString().equalsIgnoreCase("false") == true) {
            emplistSource.clear();
            String[] split_ou_list = ddl_org_unit_list.split("-");
            String v_oum_srno = split_ou_list[0].trim();
            try {
                List<FhrdEmpmst> lst_temp = fhrdEmpmstFacade.findEmpListForBulkAtt(Integer.valueOf(v_oum_srno), cal_from_date, cal_to_date);
                Integer n = lst_temp.size();
                for (int i = 0; i < n; i++) {
                    emplistSource.add(lst_temp.get(i).getUserId() + "-" + lst_temp.get(i).getUserName());
                }
            } catch (Exception e) {
            }
            employee_list = new DualListModel<String>(emplistSource, emplistTarget);

        } else {
            emplistSource.clear();
            emplistTarget.clear();
            employee_list = new DualListModel<String>(emplistSource, emplistTarget);
        }
        picklst_emp = employee_list;
    }// </editor-fold>

    // btn click events........................................
    // <editor-fold defaultstate="collapsed" desc="btn view status action event">
    public void btn_view_status_action(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        DateFormat d_formatter;
        d_formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat d_dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        if (ddl_org_unit_list.equalsIgnoreCase("")) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "First Select Any Organization Unit Sr. no..."));
            return;
        }
        if (cal_from_date == null || cal_to_date == null) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "First Select From/To Date and Then Go to View Status..."));
            return;
        }
        if (chk_all == true) {
            String[] split_ou_list = ddl_org_unit_list.split("-");
            String v_oum_srno = split_ou_list[0].trim();
            List<FhrdEmpmst> all_emp_list = null;
            try {
                all_emp_list = fhrdEmpmstFacade.findEmpListForBulkAtt(Integer.valueOf(v_oum_srno), cal_from_date, cal_to_date);
            } catch (Exception e) {
            }
            String v_emp_list = "";
            boolean v_check_previleges = false;
            Integer n = all_emp_list.size();
            for (int i = 0; i < n; i++) {
                Integer v_emp_userid = all_emp_list.get(i).getUserId();
                Database_Output entDatabase_Output = ou_Pack.check_privileges(this.getClass().getName(),
                        "btn_view_status_action", "FHRD", globalData.getUser_id());
                String check_privileges = entDatabase_Output.getString1();
                if (v_emp_userid == globalData.getUser_id()) {
                    v_check_previleges = true;
                } else {
                    if (check_privileges == null) {
                        v_check_previleges = false;
                        break;
                    } else {
                        v_check_previleges = true;
                    }
                }
                if (v_check_previleges == true) {
                    if (i != n - 1) {
                        v_emp_list = v_emp_list + v_emp_userid + ",";
                    } else {
                        v_emp_list = v_emp_list + v_emp_userid;
                    }
                }
            }
            if (v_check_previleges == true) {
                List<Bulk_Att_Display> lst_Att_Displays = bulk_att_data_SQL(
                        v_emp_list, DateTimeUtility.ChangeDateFormat(cal_from_date, null),
                        DateTimeUtility.ChangeDateFormat(cal_to_date, null), null,
                        null, null, null,
                        null, "1");
                Integer m = lst_Att_Displays.size();
                for (int i = 0; i < m; i++) {
                    try {
                        Date d1 = (Date) d_formatter.parse(lst_Att_Displays.get(i).getAtt_date());
                        lst_Att_Displays.get(i).setAtt_date(d_dateFormat.format(d1));
                    } catch (ParseException ex) {
                        LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_view_status_action", null, ex);
                    }
                }
                tbl_lst_FtasDaily = lst_Att_Displays;
            } else {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "You do not have privileges to mark attendance for this Employee.You can only Mark Your own Attendance "));
            }
        } else {
            boolean v_check_previleges = false;
            String v_emp_list = "";
            DualListModel<String> employee_list1 = picklst_emp;
            int v_emptarget_length = employee_list1.getTarget().size();
            if (v_emptarget_length == 0) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Select Any Employee From Employee List."));
            } else {
                for (int i = 0; i < v_emptarget_length; i++) {
                    String[] split_emp_target_list = employee_list1.getTarget().get(i).toString().split("-");
                    String v_emp_userid = split_emp_target_list[0].trim();
                    Database_Output entDatabase_Output = ou_Pack.check_privileges(this.getClass().getName(),
                            "btn_view_status_action", "FHRD", globalData.getUser_id());
                    String check_privileges = entDatabase_Output.getString1();
                    if (Integer.parseInt(v_emp_userid) == globalData.getUser_id()) {
                        v_check_previleges = true;
                    } else {
                        if (check_privileges == null) {
                            v_check_previleges = false;
                            break;
                        } else {
                            v_check_previleges = true;
                        }
                    }
                    if (v_check_previleges == true) {
                        if (i != v_emptarget_length - 1) {
                            v_emp_list = v_emp_list + v_emp_userid + ",";
                        } else {
                            v_emp_list = v_emp_list + v_emp_userid;
                        }
                    }
                }
                if (v_check_previleges == true) {
                    List<Bulk_Att_Display> lst_Att_Displays = bulk_att_data_SQL(
                            v_emp_list, DateTimeUtility.ChangeDateFormat(cal_from_date, null),
                            DateTimeUtility.ChangeDateFormat(cal_to_date, null), null,
                            null, null, null,
                            null, "1");
                    Integer n = lst_Att_Displays.size();
                    for (int i = 0; i < n; i++) {
                        try {
                            Date d1 = (Date) d_formatter.parse(lst_Att_Displays.get(i).getAtt_date());
                            lst_Att_Displays.get(i).setAtt_date(d_dateFormat.format(d1));
                        } catch (ParseException ex) {
                            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_view_status_action", null, ex);
                        }
                    }
                    tbl_lst_FtasDaily = lst_Att_Displays;
                } else {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "You do not have privileges to mark attendance for this Employee.You can only Mark Your own Attendance "));
                }
            }

        }

    }// </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="btn_dlg_update_action event">
    public void btn_dlg_update_action(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        FtasDaily ent_FtasDaily;
        DateFormat formatter;
        formatter = new SimpleDateFormat("dd-MMM-yyyy");
        Date d_att = null;
        try {
            d_att = (Date) formatter.parse(hdn_att_date);
        } catch (ParseException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_dlg_update_action", null, ex);
        }
        FhrdEmpmst ent_FhrdEmpmst = new FhrdEmpmst(globalData.getUser_id());
        ent_FtasDaily = ftasDailyFacade.find(ent_selected_rec.getDly_srg_key());
        String v_from_half = null;
        String v_to_half = null;
        if (ent_FtasDaily != null) {
            ent_FtasDaily.setBacklogflag(null);
            if (chk_from_half == true) {
                if (rdb_dlg_from_half.equalsIgnoreCase("p")) {
                    v_from_half = "P";
                } else if (rdb_dlg_from_half.equalsIgnoreCase("v")) {
                    v_from_half = "V";
                }
                ent_FtasDaily.setHalf1(v_from_half);
            }
            if (chk_to_half == true) {
                if (rdb_dlg_to_half.equalsIgnoreCase("p")) {
                    v_to_half = "P";
                } else if (rdb_dlg_to_half.equalsIgnoreCase("v")) {
                    v_to_half = "V";
                }
                ent_FtasDaily.setHalf2(v_to_half);
            }
            ent_FtasDaily.setUpdateby(ent_FhrdEmpmst);
            ent_FtasDaily.setUpdatedt(new Date());

            try {
                ftasDailyFacade.edit(ent_FtasDaily);
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully"));
            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Error", "Some unexpected error occur while updating dailt attendance"));
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_dlg_update_action", null, e);
            }
        }
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="btn view Unsaved Action Event">

    public void btn_view_unsaved(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (hdn_emp_list == null || hdn_from_date == null || hdn_to_date == null) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "There does not exist any Employee..."));
            return;
        }
        List<Bulk_Att_Display> lst_b_unsaved = bulk_att_unsaved_SQL(hdn_emp_list,
                hdn_from_date, hdn_to_date);
        if (lst_b_unsaved.isEmpty()) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "No Records Found..."));
        } else {
            tbl_view_unsaved = lst_b_unsaved;
        }

    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="setOrgList()"> 

    public List<SelectItem> setOrgList() {
        List<SelectItem> lst_org_option = new ArrayList<SelectItem>();
        List<SystOrgUnitMst> lst_e = systOrgUnitMstFacade.findAll(globalData.getWorking_ou(), false, false, false, false, true, null, null);
        if (lst_e.isEmpty()) {
            SelectItem s = new SelectItem(null, "No Data Found");
            lst_org_option.add(s);
        } else {
            SelectItem s = new SelectItem(null, "----Select Any----");
            lst_org_option.add(s);
            Integer n = lst_e.size();
            for (int i = 0; i < n; i++) {
                s = new SelectItem(lst_e.get(i).getOumUnitSrno(), lst_e.get(i).getOumUnitSrno() + " - " + lst_e.get(i).getOumName());
                lst_org_option.add(s);
            }
        }
        return lst_org_option;
    }
    //</editor-fold>    

    // Sql Procedure for Bulk Attendance  
    // <editor-fold defaultstate="collapsed" desc="Bulk Att Data Sql Query">
    public List<Bulk_Att_Display> bulk_att_data_SQL(String p_emp_list, String p_from_date,
            String p_to_date, String p_att_time, String p_from_half, String p_to_half,
            String p_createby, String p_system_userid, String p_state) {
        List<Bulk_Att_Display> lst_b_data = new ArrayList<Bulk_Att_Display>();
        Connection cn = null;
        try {
            Statement st = null;
            try {
                ResultSet res = null;
                try {
                    cn = getGlobalUtilities().getjdbcFES_PROJECT_Oracle().getConnection();
                    int i = 0;
                    st = cn.createStatement();
                    while (res.next()) {
                        Bulk_Att_Display ent_Bulk_Att_Display = new Bulk_Att_Display();
//                        ent_Bulk_Att_Display.setDly_srg_key(res.getBigDecimal("V_DLY_SRG_KEY"));
                        ent_Bulk_Att_Display.setUser_id(res.getString("V_EMP"));
                        ent_Bulk_Att_Display.setUser_name(res.getString("V_EMP_NAME"));
                        ent_Bulk_Att_Display.setAtt_date(res.getString("V_DATE"));
                        ent_Bulk_Att_Display.setAtt_time(res.getString("V_TIME"));
                        if (res.getString("V_TIME") == null) {
                            ent_Bulk_Att_Display.setAtt_time("-");
                        }
                        ent_Bulk_Att_Display.setFrom_half(res.getString("V_FROM_HALF"));
                        if (res.getString("V_FROM_HALF_TIME") == null) {
                            ent_Bulk_Att_Display.setFrom_half_time("-");
                        } else if (res.getString("V_FROM_HALF_TIME").equalsIgnoreCase("-1") == false) {
                            ent_Bulk_Att_Display.setFrom_half_time(res.getString("V_FROM_HALF_TIME"));
                        }
                        ent_Bulk_Att_Display.setTo_half(res.getString("V_TO_HALF"));
                        if (res.getString("V_TO_HALF_TIME") == null) {
                            ent_Bulk_Att_Display.setTo_half_time("-");
                        } else if (res.getString("V_TO_HALF_TIME").equalsIgnoreCase("-1") == false) {
                            ent_Bulk_Att_Display.setTo_half_time(res.getString("V_TO_HALF_TIME"));
                        }
                        ent_Bulk_Att_Display.setOum_unit_srno(res.getString("V_OUM_UNIT_SRNO"));
                        lst_b_data.add(ent_Bulk_Att_Display);
                        i++;
                    }
                } catch (SQLException e) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "bulk_att_data_SQL", null, e);
                } finally {
                    res.close();
                }
            } catch (SQLException sQLException) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "bulk_att_data_SQL", null, sQLException);
            } finally {
                st.close();
            }
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "bulk_att_data_SQL", null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        cn = null;
                    }
                } catch (Exception e) {
                }
            }
        }
        return lst_b_data;
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Bulk Att Unsaved Data Sql Query">

    public List<Bulk_Att_Display> bulk_att_unsaved_SQL(String p_emp_list, String p_from_date,
            String p_to_date) {
        List<Bulk_Att_Display> lst_b_data = new ArrayList<Bulk_Att_Display>();
        Connection cn = null;
        try {
            Statement st = null;
            try {
                ResultSet res = null;
                try {
                    cn = getGlobalUtilities().getjdbcFES_PROJECT_Oracle().getConnection();
                    int i = 0;
                    st = cn.createStatement();
                    res = st.executeQuery("select * from ftas_bulk_att_temp "
                            + " where user_id in (" + p_emp_list + ") and ATTDDT between '" + p_from_date + "' and '" + p_to_date + "'");
                    while (res.next()) {
                        Bulk_Att_Display ent_Bulk_Att_Display = new Bulk_Att_Display();
                        ent_Bulk_Att_Display.setUser_id(res.getString("USER_ID"));
                        ent_Bulk_Att_Display.setUser_name(res.getString("USER_NAME"));
                        ent_Bulk_Att_Display.setAtt_date(res.getString("ATTDDT"));
                        ent_Bulk_Att_Display.setAtt_time(res.getString("ATTDTIME"));
                        ent_Bulk_Att_Display.setFrom_half(res.getString("FROM_HALF"));
                        ent_Bulk_Att_Display.setTo_half(res.getString("TO_HALF"));
                        ent_Bulk_Att_Display.setOum_unit_srno(res.getString("OUM_UNIT_SRNO"));
                        lst_b_data.add(ent_Bulk_Att_Display);
                        i++;
                    }
                } catch (SQLException e) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "bulk_att_unsaved_SQL", null, e);
                } finally {
                    res.close();
                }

            } catch (SQLException sQLException) {
            } finally {
                st.close();
            }
        } catch (SQLException ex) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "bulk_att_unsaved_SQL", null, ex);
        } finally {
            if (cn != null) {
                try {
                    if (!cn.isClosed()) {
                        cn.close();
                        cn = null;
                    }
                } catch (Exception e) {
                }
            }
        }
        return lst_b_data;
    }// </editor-fold>
    // functions 2.....................
    // <editor-fold defaultstate="collapsed" desc="for selected row to upadet daily att record">
    Bulk_Att_Display ent_selected_rec = new Bulk_Att_Display();

    public Bulk_Att_Display getEnt_selected_rec() {
        return ent_selected_rec;
    }

    public void setEnt_selected_rec(Bulk_Att_Display ent_selected_rec) {
        hdn_att_date = ent_selected_rec.getAtt_date();
        hdn_oum = ent_selected_rec.getOum_unit_srno();
        hdn_user_id = ent_selected_rec.getUser_id();
        hdn_from_half = ent_selected_rec.getFrom_half();
        hdn_to_half = ent_selected_rec.getTo_half();
        this.ent_selected_rec = ent_selected_rec;
    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setDdl_org_unit_list();
        rdb_dlg_from_half = "p";
        rdb_dlg_to_half = "p";
        rdb_place_firsthalf_binding = "p";
        rdb_place_secondhalf_binding = "p";
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Default Constructor & Other Beans">

    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="getPIS_GlobalSettings()">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    //</editor-fold>
}
