package org.fes.pis.jsf.pages.pis.masters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.AlternateGroupSet;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.Rule_Group_Relation_bean;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.customJpaController;
import org.fes.pis.entities.*;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdRuleApplGroupMstFacadeLocal;
import org.fes.pis.sessions.FhrdRuleGroupCategoryFacadeLocal;
import org.fes.pis.sessions.FhrdRuleManagGroupMstFacadeLocal;
import org.primefaces.component.treetable.TreeTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.DualListModel;
import org.primefaces.model.TreeNode;

@ManagedBean(name = "PIS_Rule_Group_Master")
@ViewScoped
public class PIS_Rule_Group_Master implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="declaration">
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    Integer v_current_fin_year = getGlobalUtilities().getCurrent_fin_year();
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="EJB Declaration and Default Methods">
    @EJB
    private customJpaController customJpaController;
    @EJB
    private FhrdRuleApplGroupMstFacadeLocal fhrdRuleApplGroupMstFacade;
    @EJB
    private FhrdRuleManagGroupMstFacadeLocal fhrdRuleManagGroupMstFacade;
    @EJB
    private FhrdRuleGroupCategoryFacadeLocal fhrdRuleGroupCategoryFacade;

    /**
     * Creates a new instance of PYRL_Rule_Group_Master
     */
    public PIS_Rule_Group_Master() {
    }// </editor-fold>   
    // <editor-fold defaultstate="collapsed" desc="tbl_rule_group_mst"> 
    List<FhrdRuleManagGroupMst> tbl_rule_group_mst;
    List<FhrdRuleManagGroupMst> tbl_rule_group_mst_filtered;

    public List<FhrdRuleManagGroupMst> getTbl_rule_group_mst_filtered() {
        return tbl_rule_group_mst_filtered;
    }

    public void setTbl_rule_group_mst_filtered(List<FhrdRuleManagGroupMst> tbl_rule_group_mst_filtered) {
        this.tbl_rule_group_mst_filtered = tbl_rule_group_mst_filtered;
    }

    public List<FhrdRuleManagGroupMst> getTbl_rule_group_mst() {
        return tbl_rule_group_mst;
    }

    public void setTbl_rule_group_mst(List<FhrdRuleManagGroupMst> tbl_rule_group_mst) {
        this.tbl_rule_group_mst = tbl_rule_group_mst;
    }

    public void setTbl_rule_group_mst() {
        tbl_rule_group_mst = fhrdRuleManagGroupMstFacade.findAll(globalData.getWorking_ou());
    }
    //<editor-fold defaultstate="collapsed" desc="filter_contract_type">
    List<SelectItem> filter_contract_type;

    public List<SelectItem> getFilter_contract_type() {
        return filter_contract_type;
    }

    public void setFilter_contract_type(List<SelectItem> filter_contract_type) {
        this.filter_contract_type = filter_contract_type;
    }

    private void setFilter_contract_type() {
        filter_contract_type = new ArrayList<SelectItem>();
        filter_contract_type.add(new SelectItem("", "All"));
        filter_contract_type.add(new SelectItem("M", "Main"));
        filter_contract_type.add(new SelectItem("S", "Supplement"));
    }
    //</editor-fold>
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Apply End Date for Manageable groups">
    FhrdRuleManagGroupMst entRuleManagGroupMst_edit;

    public FhrdRuleManagGroupMst getEntRuleManagGroupMst_edit() {
        return entRuleManagGroupMst_edit;
    }

    public void setEntRuleManagGroupMst_edit(FhrdRuleManagGroupMst entRuleManagGroupMst_edit) {
        this.entRuleManagGroupMst_edit = entRuleManagGroupMst_edit;
        cal_apply_end_date_m_min = DateTimeUtility.ChangeDateFormat(entRuleManagGroupMst_edit.getStartDate(), null);
        cal_apply_end_date_m = null;
    }

    public void btn_apply_date_m_action(ActionEvent event) {
    }
    Date cal_apply_end_date_m;
    String cal_apply_end_date_m_min;

    public String getCal_apply_end_date_m_min() {
        return cal_apply_end_date_m_min;
    }

    public void setCal_apply_end_date_m_min(String cal_apply_end_date_m_min) {
        this.cal_apply_end_date_m_min = cal_apply_end_date_m_min;
    }

    public Date getCal_apply_end_date_m() {
        return cal_apply_end_date_m;
    }

    public void setCal_apply_end_date_m(Date cal_apply_end_date_m) {
        this.cal_apply_end_date_m = cal_apply_end_date_m;
    }

    public void btn_apply_end_date_m_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        BigDecimal v_rmgm_srg_key = entRuleManagGroupMst_edit.getRmgmSrgKey();
        FhrdRuleManagGroupMst entFhrdRuleManagGroupMst = fhrdRuleManagGroupMstFacade.find(v_rmgm_srg_key);
        entFhrdRuleManagGroupMst.setEndDate(cal_apply_end_date_m);
        entFhrdRuleManagGroupMst.setUpdateby(globalData.getEnt_login_user());
        entFhrdRuleManagGroupMst.setUpdatedt(new Date());
        try {
            fhrdRuleManagGroupMstFacade.edit(entFhrdRuleManagGroupMst);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record updated Successfully"));
            setTbl_rule_group_mst();
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some error occured during updating Rule"));
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_apply_end_date_clicked", null, e);
        }
    }
    //</editor-fold> 
    // <editor-fold defaultstate="collapsed" desc="tbl_rule_appl_group_mst ">
    List<FhrdRuleApplGroupMst> tbl_rule_appl_group_mst;
    List<FhrdRuleApplGroupMst> tbl_rule_appl_group_mst_filtered;

    public List<FhrdRuleApplGroupMst> getTbl_rule_appl_group_mst_filtered() {
        return tbl_rule_appl_group_mst_filtered;
    }

    public void setTbl_rule_appl_group_mst_filtered(List<FhrdRuleApplGroupMst> tbl_rule_appl_group_mst_filtered) {
        this.tbl_rule_appl_group_mst_filtered = tbl_rule_appl_group_mst_filtered;
    }

    public List<FhrdRuleApplGroupMst> getTbl_rule_appl_group_mst() {
        return tbl_rule_appl_group_mst;
    }

    public void setTbl_rule_appl_group_mst(List<FhrdRuleApplGroupMst> tbl_rule_appl_group_mst) {
        this.tbl_rule_appl_group_mst = tbl_rule_appl_group_mst;
    }

    public void setTbl_rule_appl_group_mst() {
        tbl_rule_appl_group_mst = fhrdRuleApplGroupMstFacade.findAll(globalData.getWorking_ou());
    }
    // </editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="Apply End date for Applicable groups">
    FhrdRuleApplGroupMst entRuleApplGroupMst_edit;

    public FhrdRuleApplGroupMst getEntRuleApplGroupMst_edit() {
        return entRuleApplGroupMst_edit;
    }

    public void setEntRuleApplGroupMst_edit(FhrdRuleApplGroupMst entRuleApplGroupMst_edit) {
        this.entRuleApplGroupMst_edit = entRuleApplGroupMst_edit;
        cal_apply_end_date_a_min = DateTimeUtility.ChangeDateFormat(entRuleApplGroupMst_edit.getStartDate(), null);
        cal_apply_end_date_a = null;
    }

    public void btn_apply_date_a_action(ActionEvent event) {
    }
    Date cal_apply_end_date_a;
    String cal_apply_end_date_a_min;

    public String getCal_apply_end_date_a_min() {
        return cal_apply_end_date_a_min;
    }

    public void setCal_apply_end_date_a_min(String cal_apply_end_date_a_min) {
        this.cal_apply_end_date_a_min = cal_apply_end_date_a_min;
    }

    public Date getCal_apply_end_date_a() {
        return cal_apply_end_date_a;
    }

    public void setCal_apply_end_date_a(Date cal_apply_end_date_a) {
        this.cal_apply_end_date_a = cal_apply_end_date_a;
    }

    public void btn_apply_end_date_a_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        BigDecimal v_ragm_srg_key = entRuleApplGroupMst_edit.getRagmSrgKey();
        FhrdRuleApplGroupMst entFhrdRuleApplGroupMst = fhrdRuleApplGroupMstFacade.find(v_ragm_srg_key);
        entFhrdRuleApplGroupMst.setEndDate(cal_apply_end_date_a);
        entFhrdRuleApplGroupMst.setUpdateby(globalData.getEnt_login_user());
        entFhrdRuleApplGroupMst.setUpdatedt(new Date());
        try {
            fhrdRuleApplGroupMstFacade.edit(entFhrdRuleApplGroupMst);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record updated Successfully"));
            setTbl_rule_appl_group_mst();
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some error occured during updating Rule"));
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_apply_end_date_clicked", null, e);
        }
    }
    //</editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="tree_tbl_group">
    TreeTable tree_tbl_group = new TreeTable();

    public TreeTable getTree_tbl_group() {
        return tree_tbl_group;
    }

    public void setTree_tbl_group(TreeTable tree_tbl_group) {
        this.tree_tbl_group = tree_tbl_group;
    }

    public void set_tree_tbl_group() {
        FacesContext context = FacesContext.getCurrentInstance();
        TreeNode tree_root;
        tree_root = new DefaultTreeNode("Root", null);
        try {
            Connection cn = getGlobalUtilities().getjdbcFES_PROJECT_Oracle().getConnection();
            try {
                Statement stmt = cn.createStatement();
                try {
                    ResultSet rs;
                    String v_query =
                            " SELECT RAGM, "
                            + " PARENT_RAGM, "
                            + " rmgm_srg_key, "
                            + " GRP_DESC, "
                            + " START_DATE, "
                            + " END_DATE, "
                            + " CAT_DESC, "
                            + " LEV "
                            + " FROM "
                            + " (SELECT A.RAGM_SRG_KEY RAGM, "
                            + " NULL PARENT_RAGM, "
                            + " NULL rmgm_srg_key, "
                            + " A.APPL_GROUP_DESC GRP_DESC, "
                            + " NULL START_DATE, "
                            + " NULL END_DATE, "
                            + " 1 LEV, "
                            + " NULL CAT_DESC "
                            + " FROM FHRD_RULE_APPL_GROUP_MST A "
                            + " WHERE A.RAGM_SRG_KEY IN "
                            + " (SELECT DISTINCT(RAGM_SRG_KEY) FROM FHRD_RULE_GROUP_RELATION "
                            + " ) "
                            + " AND A.OUM_UNIT_SRNO IN (OU_PACK.GET_OUM_UNIT_SRNO('FHRD_RULE_APPL_GROUP_MST'," + globalData.getWorking_ou() + ")) "
                            + " UNION "
                            + " SELECT null RAGM, "
                            + " R.RAGM_SRG_KEY PARENT_RAGM, "
                            + " m.rmgm_srg_key rmgm_srg_key, "
                            + " M.MANAG_GROUP_DESC GRP_DESC, "
                            + " R.START_DATE, "
                            + " R.END_DATE, "
                            + " 2 LEV, "
                            + " C.CAT_DESC cat_desc "
                            + " FROM FHRD_RULE_MANAG_GROUP_MST M, "
                            + " FHRD_RULE_GROUP_RELATION R, "
                            + " FHRD_RULE_GROUP_CATEGORY C "
                            + " WHERE M.RMGM_SRG_KEY =R.RMGM_SRG_KEY "
                            + " AND M.RGC_SRG_KEY    =C.RGC_SRG_KEY "
                            + " AND C.OUM_UNIT_SRNO IN (OU_PACK.GET_OUM_UNIT_SRNO('FHRD_RULE_GROUP_CATEGORY'," + globalData.getWorking_ou() + ")) "
                            + " ) "
                            + " start with parent_ragm is null "
                            + " connect by prior ragm=parent_ragm "
                            + " ORDER siblings BY GRP_DESC ";
                    rs = stmt.executeQuery(v_query);
                    try {
                        ArrayList<TreeNode> stack = new ArrayList<TreeNode>();
                        while (rs.next()) {
                            Rule_Group_Relation_bean entRule_Group_Relation_bean = new Rule_Group_Relation_bean();
                            if (rs.getString("RAGM") == null) {
                                //entRule_Group_Relation_bean.setRagm_srg_key(new BigDecimal("0"));
                            } else {
                                entRule_Group_Relation_bean.setRagm_srg_key(new BigDecimal(rs.getString("RAGM")));
                            }

                            if (rs.getString("RMGM_srg_key") == null) {
                                // entRule_Group_Relation_bean.setRmgm_srg_key(new BigDecimal("0"));
                            } else {
                                entRule_Group_Relation_bean.setRmgm_srg_key(new BigDecimal(rs.getString("RmGM_srg_key")));
                            }

                            entRule_Group_Relation_bean.setGroup_desc(rs.getString("GRP_DESC"));
                            entRule_Group_Relation_bean.setStart_date(rs.getDate("START_DATE"));
                            entRule_Group_Relation_bean.setEnd_date(rs.getDate("END_DATE"));
                            entRule_Group_Relation_bean.setCat_desc(rs.getString("CAT_DESC"));
                            Integer v_current_level = rs.getInt("LEV") - 1;
                            entRule_Group_Relation_bean.setLevel(v_current_level + 1);
                            if (v_current_level == 0) {
                                stack.add(0, new DefaultTreeNode(entRule_Group_Relation_bean, tree_root));
                            } else {
                                stack.add(1, new DefaultTreeNode(entRule_Group_Relation_bean, stack.get(0)));
                            }

                        }

                    } finally {
                        tree_tbl_group.setValue(tree_root);
                        rs.close();
                    }


                } finally {
                    stmt.close();
                }
            } finally {
                cn.close();
            }

        } catch (SQLException e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "set_tree_tbl_group()", null, e);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error in getting Group Relations"));
        }
    }// </editor-fold>
    // add group ...
    // <editor-fold defaultstate="collapsed" desc="binding attributes">
    String txt_group_desc;
    String ddl_group_purpose;

    public String getTxt_group_desc() {
        return txt_group_desc;
    }

    public void setTxt_group_desc(String txt_group_desc) {
        this.txt_group_desc = txt_group_desc;
    }

    public String getDdl_group_purpose() {
        return ddl_group_purpose;
    }

    public void setDdl_group_purpose(String ddl_group_purpose) {
        this.ddl_group_purpose = ddl_group_purpose;
    }
    //<editor-fold defaultstate="collapsed" desc="cal_end_date">
    Date cal_end_date;
    String cal_end_date_min;

    public Date getCal_end_date() {
        return cal_end_date;
    }

    public void setCal_end_date(Date cal_end_date) {
        this.cal_end_date = cal_end_date;
    }

    public String getCal_end_date_min() {
        return cal_end_date_min;
    }

    public void setCal_end_date_min(String cal_end_date_min) {
        this.cal_end_date_min = cal_end_date_min;
    }

    public void cal_end_date_changed(SelectEvent event) {
        cal_end_date = (Date) event.getObject();
        setDdl_category();
        setDdl_alternate_group();
        setPicklstGroup();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_start_date">
    Date cal_start_date;

    public Date getCal_start_date() {
        return cal_start_date;
    }

    public void setCal_start_date(Date cal_start_date) {
        this.cal_start_date = cal_start_date;
    }

    private void setCal_start_date() {
        cal_start_date = new Date();
        cal_end_date_min = DateTimeUtility.ChangeDateFormat(cal_start_date, null);
        cal_end_date = null;
    }

    public void cal_start_date_changed(SelectEvent event) {
        cal_start_date = (Date) event.getObject();
        if (cal_end_date != null) {
            if (cal_end_date.before(cal_start_date)) {
                cal_end_date = cal_start_date;
            }
        }
        cal_end_date_min = DateTimeUtility.ChangeDateFormat(cal_start_date, null);
        setDdl_category();
        setDdl_alternate_group();
        setPicklstGroup();
    }

    public void reset_cal_end_date(ActionEvent evnet) {
        cal_end_date = null;
        cal_end_date_min = DateTimeUtility.ChangeDateFormat(cal_start_date, null);
        setDdl_category();
        setDdl_alternate_group();
        setPicklstGroup();

    }
    //</editor-fold>
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_group_purpose_changed">

    public void ddl_group_purpose_changed(ValueChangeEvent event) {
        ddl_group_purpose = event.getNewValue() == null ? null : event.getNewValue().toString();
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="picklst_group">
    DualListModel<FhrdRuleManagGroupMst> picklst_group = new DualListModel<FhrdRuleManagGroupMst>();

    public DualListModel<FhrdRuleManagGroupMst> getPicklst_group() {
        return picklst_group;
    }

    public void setPicklst_group(DualListModel<FhrdRuleManagGroupMst> picklst_group) {
        this.picklst_group = picklst_group;
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ddl_category">
    private String ddl_category = new String();
    List<SelectItem> ddl_category_options;

    public List<SelectItem> getDdl_category_options() {
        return ddl_category_options;
    }

    public void setDdl_category_options(List<SelectItem> ddl_category_options) {
        this.ddl_category_options = ddl_category_options;
    }

    public String getDdl_category() {
        return ddl_category;
    }

    public void setDdl_category(String ddl_category) {
        this.ddl_category = ddl_category;
    }

    public void setDdl_category() {
        ddl_category_options = new ArrayList<SelectItem>();
        List<FhrdRuleGroupCategory> lst_e = fhrdRuleGroupCategoryFacade.findAll(globalData.getWorking_ou(), cal_start_date, cal_end_date);
        if (lst_e.isEmpty()) {
            ddl_category_options.add(new SelectItem(null, "--- Not Available ---"));
        } else {
            ddl_category_options.add(new SelectItem(null, "--- Select ---"));
            Integer n = lst_e.size();
            for (int i = 0; i < n; i++) {
                ddl_category_options.add(new SelectItem(lst_e.get(i).getRgcSrgKey(), lst_e.get(i).getCatDesc()));
            }
        }
        ddl_category = null;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ddl_category_changed">

    public void ddl_category_changed(ValueChangeEvent event) {
        ddl_category = event.getNewValue() == null ? null : event.getNewValue().toString();
        setDdl_alternate_group();
    }// </editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="setPicklstGroup()"> 

    public void setPicklstGroup() {
        FacesContext context = FacesContext.getCurrentInstance();
        List<FhrdRuleManagGroupMst> picklst_group_source = new ArrayList<FhrdRuleManagGroupMst>();
        if (ddl_contract_type != null) {
            try {
                List<FhrdRuleManagGroupMst> lst_grouplist;
                lst_grouplist = fhrdRuleManagGroupMstFacade.findAll(globalData.getWorking_ou(), null, null, ddl_contract_type, cal_start_date, cal_end_date, true);
                Integer n = lst_grouplist.size();
                for (int i = 0; i < n; i++) {
                    picklst_group_source.add(lst_grouplist.get(i));
                }
            } catch (Exception ex) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error in getting Manageable group list."));
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "setPicklstGroup()", null, ex);
            }
        }
        picklst_group = new DualListModel<FhrdRuleManagGroupMst>(picklst_group_source, new ArrayList<FhrdRuleManagGroupMst>());

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_alternate_group"> 
    String ddl_alternate_group;
    List<SelectItem> ddl_alternate_group_options;

    public String getDdl_alternate_group() {
        return ddl_alternate_group;
    }

    public void setDdl_alternate_group(String ddl_alternate_group) {
        this.ddl_alternate_group = ddl_alternate_group;
    }

    public List<SelectItem> getDdl_alternate_group_options() {
        return ddl_alternate_group_options;
    }

    public void setDdl_alternate_group_options(List<SelectItem> ddl_alternate_group_options) {
        this.ddl_alternate_group_options = ddl_alternate_group_options;
    }

    public void setDdl_alternate_group() {
        ddl_alternate_group_options = new ArrayList<SelectItem>();
        if (ddl_category != null) {
            List<FhrdRuleManagGroupMst> lst_e = fhrdRuleManagGroupMstFacade.findAll(globalData.getWorking_ou(), new BigDecimal(ddl_category), null, ddl_contract_type, cal_start_date, cal_end_date, false);
            ddl_alternate_group_options.add(new SelectItem(null, "---- None ----"));
            Integer n = lst_e.size();
            for (int i = 0; i < n; i++) {
                ddl_alternate_group_options.add(new SelectItem(lst_e.get(i).getRmgmSrgKey().toString(), lst_e.get(i).getManagGroupDesc()));
            }
        } else {
            ddl_alternate_group_options.add(new SelectItem(null, "--- Select Category First ---"));
        }
        ddl_alternate_group = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_contract_type"> 
    String ddl_contract_type;

    public String getDdl_contract_type() {
        return ddl_contract_type;
    }

    public void setDdl_contract_type(String ddl_contract_type) {
        this.ddl_contract_type = ddl_contract_type;
    }

    public void ddl_contract_type_changed(ValueChangeEvent event) {
        ddl_contract_type = event.getNewValue() == null ? null : event.getNewValue().toString();
        if (ddl_group_purpose != null) {
            setPicklstGroup();
            setDdl_alternate_group();
        } else {
            setPicklstGroup();
            setDdl_alternate_group();
        }
    }
    //</editor-fold>

    // View Alternate Manageable Groups// <editor-fold defaultstate="collapsed" desc="btn_add_new_action">
    public void btn_add_new_action(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        FhrdEmpmst ent_Empmst = new FhrdEmpmst(globalData.getUser_id());
        SystOrgUnitMst ent_OrgUnitMst = new SystOrgUnitMst(globalData.getWorking_ou());
        if (ddl_group_purpose.equalsIgnoreCase("M")) {
            try {
                txt_group_desc = txt_group_desc.trim().toUpperCase();
                if (fhrdRuleManagGroupMstFacade.isManageGroupDescExists(globalData.getWorking_ou(), txt_group_desc, cal_start_date, cal_end_date)) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Group Description already exists", "Group Description already exists in given period"));
                    return;
                }
                BigDecimal v_rmgm_srgkey = getGlobalSettings().getUniqueSrno(null, "FHRD_RULE_MANAG_GROUP_MST");
                FhrdRuleManagGroupMst ent_FhrdRuleManagGroupMst = new FhrdRuleManagGroupMst(v_rmgm_srgkey);
                ent_FhrdRuleManagGroupMst.setCreateby(ent_Empmst);
                ent_FhrdRuleManagGroupMst.setCreatedt(new Date());
                ent_FhrdRuleManagGroupMst.setStartDate(cal_start_date);
                ent_FhrdRuleManagGroupMst.setEndDate(cal_end_date);
                ent_FhrdRuleManagGroupMst.setManagGroupDesc(txt_group_desc);
                ent_FhrdRuleManagGroupMst.setRgcSrgKey(new FhrdRuleGroupCategory(new BigDecimal(ddl_category)));
                ent_FhrdRuleManagGroupMst.setManagGroupSrno(v_rmgm_srgkey.intValue());
                ent_FhrdRuleManagGroupMst.setOumUnitSrno(ent_OrgUnitMst);
                ent_FhrdRuleManagGroupMst.setContractTypeFlag(ddl_contract_type);
                if (ddl_alternate_group != null) {
                    ent_FhrdRuleManagGroupMst.setRmgmAltSrno(fhrdRuleManagGroupMstFacade.find(new BigDecimal(ddl_alternate_group)).getRmgmAltSrno());
                }
                fhrdRuleManagGroupMstFacade.create(ent_FhrdRuleManagGroupMst);
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully"));
                resetNewGroup();
                RequestContext.getCurrentInstance().execute("ddl_group_purpose_onchange()");
            } catch (Exception ex) {
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_add_new_action", null, ex);
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occurred during saving record"));
            }
        } else if (ddl_group_purpose.equalsIgnoreCase("A")) {
            try {
                if (fhrdRuleApplGroupMstFacade.isApplGroupDescExists(globalData.getWorking_ou(), txt_group_desc, cal_start_date, cal_end_date)) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Group Description already exists", "Group Description already exists in given period"));
                    return;
                }
                int n = picklst_group.getTarget().size();
                List<AlternateGroupSet> lst_AlternateGroupSets = new ArrayList<AlternateGroupSet>();
                for (int i = 0; i < n; i++) {
                    FhrdRuleManagGroupMst ent_ManagGroupMst = picklst_group.getTarget().get(i);
                    int altSrno = ent_ManagGroupMst.getRmgmAltSrno();
                    if (!lst_AlternateGroupSets.contains(new AlternateGroupSet(altSrno))) {
                        lst_AlternateGroupSets.add(new AlternateGroupSet(altSrno));
                    }
                    lst_AlternateGroupSets.get(lst_AlternateGroupSets.indexOf(new AlternateGroupSet(altSrno))).getLst_GroupDescs().add(ent_ManagGroupMst.getManagGroupDesc());
                }
                int m = lst_AlternateGroupSets.size();
                boolean multipleSelected = false;
                StringBuilder builder = new StringBuilder();
                builder.append("Following lists out set(s) of alternate groups<br/>");
                builder.append("<ul>");
                for (int j = 0; j < m; j++) {
                    if (lst_AlternateGroupSets.get(j).getLst_GroupDescs().size() > 1) {
                        multipleSelected = true;
                        builder.append("<li>").append(lst_AlternateGroupSets.get(j).getLst_GroupDescs()).append("</li>");
                    }
                }
                builder.append("</ul>");
                if (multipleSelected) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Alternate groups are selected", builder.toString()));
                    return;
                }

                int v_grouptarget_length = picklst_group.getTarget().size();
                if (v_grouptarget_length == 0) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Select Any Manageable Group", "Select Any Manageable Group From Subset Group List."));
                } else {
                    BigDecimal v_ragm_srgkey = getGlobalSettings().getUniqueSrno(null, "FHRD_RULE_APPL_GROUP_MST");
                    FhrdRuleApplGroupMst ent_FhrdRuleApplGroupMst = new FhrdRuleApplGroupMst(v_ragm_srgkey);
                    DualListModel<FhrdRuleManagGroupMst> group_list1 = picklst_group;
                    List<FhrdRuleGroupRelation> ent_Relation = new ArrayList<FhrdRuleGroupRelation>();
                    for (int i = 0; i < v_grouptarget_length; i++) {
                        BigDecimal v_rgr_srgkey = getGlobalSettings().getUniqueSrno(null, "FHRD_RULE_GROUP_RELATION");
                        FhrdRuleManagGroupMst entFrmgm = group_list1.getTarget().get(i);
                        BigDecimal v_rmgm_srg_key = entFrmgm.getRmgmSrgKey();
                        FhrdRuleApplGroupMst ent_FhrdRuleApplGroupMst1 = new FhrdRuleApplGroupMst(new BigDecimal(v_ragm_srgkey.toString()));
                        FhrdRuleGroupRelation entFhrdRuleGroupRelation = new FhrdRuleGroupRelation(new BigDecimal(v_rgr_srgkey.toString()));
                        entFhrdRuleGroupRelation.setStartDate(cal_start_date);
                        entFhrdRuleGroupRelation.setCreateby(ent_Empmst);
                        entFhrdRuleGroupRelation.setCreatedt(new Date());
                        entFhrdRuleGroupRelation.setRagmSrgKey(ent_FhrdRuleApplGroupMst1);
                        entFhrdRuleGroupRelation.setRmgmSrgKey(entFrmgm);
                        entFhrdRuleGroupRelation.setRgcSrgKey(entFrmgm.getRgcSrgKey());
                        entFhrdRuleGroupRelation.setOumUnitSrno(ent_OrgUnitMst);
                        entFhrdRuleGroupRelation.setRmgmAltSrno(entFrmgm.getRmgmAltSrno());
                        ent_Relation.add(entFhrdRuleGroupRelation);
                    }
                    ent_FhrdRuleApplGroupMst.setFhrdRuleGroupRelationCollection(ent_Relation);
                    ent_FhrdRuleApplGroupMst.setApplGroupDesc(txt_group_desc.trim().toUpperCase());
                    ent_FhrdRuleApplGroupMst.setApplGroupSrno(-1);
                    ent_FhrdRuleApplGroupMst.setCreateby(ent_Empmst);
                    ent_FhrdRuleApplGroupMst.setCreatedt(new Date());
                    ent_FhrdRuleApplGroupMst.setEndDate(cal_end_date);
                    ent_FhrdRuleApplGroupMst.setOumUnitSrno(ent_OrgUnitMst);
                    ent_FhrdRuleApplGroupMst.setContractTypeFlag(ddl_contract_type);
                    ent_FhrdRuleApplGroupMst.setStartDate(cal_start_date);
                    //customJpaController.createFhrdRuleApplGroupMst(ent_FhrdRuleApplGroupMst);
                    fhrdRuleApplGroupMstFacade.create(ent_FhrdRuleApplGroupMst);
                    try {
                        resetNewGroup();
                        setTbl_rule_appl_group_mst();
                        RequestContext.getCurrentInstance().execute("ddl_group_purpose_onchange()");
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully"));
                    } catch (Exception e) {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", "Record Saved Successfully but some error occurred during reseting data"));
                        LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_add_new_action", null, e);
                    }
                }

            } catch (Exception ex) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occurred during saving record"));
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_add_new_action", null, ex);
            }
        }
    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetNewGroup()"> 

    public void resetNewGroup() {
        List<FhrdRuleManagGroupMst> lst_rg_mst = fhrdRuleManagGroupMstFacade.findAll(globalData.getWorking_ou());
        tbl_rule_group_mst = lst_rg_mst;
        List<FhrdRuleApplGroupMst> lst_rag_mst = fhrdRuleApplGroupMstFacade.findAll(globalData.getWorking_ou());
        tbl_rule_appl_group_mst = lst_rag_mst;
        set_tree_tbl_group();
        txt_group_desc = null;
        ddl_category = null;
        ddl_contract_type = null;
        ddl_group_purpose = null;
        setCal_start_date();
        picklst_group = new DualListModel<FhrdRuleManagGroupMst>();
        setPicklstGroup();
        setDdl_alternate_group();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Detail View Settings">
    //<editor-fold defaultstate="collapsed" desc="View Alternate Group Settings">
    public void btn_view_alternate_group_action(FhrdRuleManagGroupMst ent_FhrdRuleManagGroupMst) {
        tbl_view_alt_manageable_group = fhrdRuleManagGroupMstFacade.findAlternateManageableGroup(ent_FhrdRuleManagGroupMst.getRmgmAltSrno(), globalData.getWorking_ou());
    }

    public void hyp_view_alternate_group(ActionEvent event) {
        tbl_view_alt_manageable_group = fhrdRuleManagGroupMstFacade.findAlternateManageableGroup(fhrdRuleManagGroupMstFacade.find(new BigDecimal(ddl_alternate_group)).getRmgmAltSrno(), globalData.getWorking_ou());
    }
    List<FhrdRuleManagGroupMst> tbl_view_alt_manageable_group;

    public List<FhrdRuleManagGroupMst> getTbl_view_alt_manageable_group() {
        return tbl_view_alt_manageable_group;
    }

    public void setTbl_view_alt_manageable_group(List<FhrdRuleManagGroupMst> tbl_view_alt_manageable_group) {
        this.tbl_view_alt_manageable_group = tbl_view_alt_manageable_group;
    }
    //</editor-fold>
    //</editor-fold>
// Configure Group...
    // <editor-fold defaultstate="collapsed" desc="ent_selected_rec">
    FhrdRuleManagGroupMst ent_selected_rec = new FhrdRuleManagGroupMst();

    public FhrdRuleManagGroupMst getEnt_selected_rec() {
        return ent_selected_rec;
    }

    public void setEnt_selected_rec(FhrdRuleManagGroupMst ent_selected_rec) {
        selected_grp_cat = ent_selected_rec.getRgcSrgKey().getCatDesc();
        selected_grp_name = ent_selected_rec.getManagGroupDesc();
        selected_grp_srno = ent_selected_rec.getManagGroupSrno();
        selected_grp_rmgm_srg_key = ent_selected_rec.getRmgmSrgKey();
        this.ent_selected_rec = ent_selected_rec;
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="selected rec">
    String selected_grp_cat = new String();
    Integer selected_grp_srno;
    String selected_grp_name = new String();
    BigDecimal selected_grp_rmgm_srg_key;

    public BigDecimal getSelected_grp_rmgm_srg_key() {
        return selected_grp_rmgm_srg_key;
    }

    public void setSelected_grp_rmgm_srg_key(BigDecimal selected_grp_rmgm_srg_key) {
        this.selected_grp_rmgm_srg_key = selected_grp_rmgm_srg_key;
    }

    public String getSelected_grp_cat() {
        return selected_grp_cat;
    }

    public void setSelected_grp_cat(String selected_grp_cat) {
        this.selected_grp_cat = selected_grp_cat;
    }

    public String getSelected_grp_name() {
        return selected_grp_name;
    }

    public void setSelected_grp_name(String selected_grp_name) {
        this.selected_grp_name = selected_grp_name;
    }

    public Integer getSelected_grp_srno() {
        return selected_grp_srno;
    }

    public void setSelected_grp_srno(Integer selected_grp_srno) {
        this.selected_grp_srno = selected_grp_srno;
    }

    public String lnk_config_action() {
        return selected_grp_cat;
    }

    // </editor-fold>
    //Other DB Functions ... 
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setFilter_contract_type();
        setTbl_rule_group_mst();
        setTbl_rule_appl_group_mst();
        set_tree_tbl_group();
        setCal_start_date();
        setDdl_category();
        picklst_group = new DualListModel<FhrdRuleManagGroupMst>();
        setDdl_alternate_group();
        setPicklstGroup();
    }
    //</editor-fold>
}
