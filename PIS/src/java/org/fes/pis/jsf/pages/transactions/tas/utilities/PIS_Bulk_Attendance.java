package org.fes.pis.jsf.pages.transactions.tas.utilities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.Database_Output;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.db.fes_project.pack.Ftas_Pack;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.*;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;
import org.fes.pis.sessions.FtasBulkAttTempFacadeLocal;
import org.fes.pis.sessions.FtasLeavemstFacadeLocal;
import org.fes.pis.sessions.SystOrgUnitMstFacadeLocal;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DualListModel;

@ManagedBean(name = "PIS_Bulk_Attendance")
@ViewScoped
public class PIS_Bulk_Attendance implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="Declarations"> 

    
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    boolean isValidUser = false;
    Ou_Pack ou_Pack = new Ou_Pack();
    Ftas_Pack ftas_Pack = new Ftas_Pack();
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="EJB Declaration">
    @EJB
    private FtasLeavemstFacadeLocal ftasLeavemstFacade;
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    @EJB
    private SystOrgUnitMstFacadeLocal systOrgUnitMstFacade;
    @EJB
    private FtasBulkAttTempFacadeLocal ftasBulkAttTempFacade;
    // </editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="Binded Attributes">
    String txt_from_employee;
    String txt_to_employee;
    String txt_oum_unit;
    String min_to_date;
    Date cal_from_date;
    Date cal_to_date;
    boolean chk_for_firsthalf;
    boolean chk_all = true;
    boolean chk_for_secondhalf;
    boolean chk_presence_for = true;
    boolean chk_for_firsthalf_val = true;
    boolean chk_for_secondhalf_val = true;
    DualListModel<String> picklst_emp = new DualListModel<String>();

    public String getTxt_from_employee() {
        return txt_from_employee;
    }

    public void setTxt_from_employee(String txt_from_employee) {
        this.txt_from_employee = txt_from_employee;
    }

    public String getTxt_oum_unit() {
        return txt_oum_unit;
    }

    public void setTxt_oum_unit(String txt_oum_unit) {
        this.txt_oum_unit = txt_oum_unit;
    }

    public String getTxt_to_employee() {
        return txt_to_employee;
    }

    public void setTxt_to_employee(String txt_to_employee) {
        this.txt_to_employee = txt_to_employee;
    }

    public Date getCal_from_date() {
        return cal_from_date;
    }

    public void setCal_from_date(Date cal_from_date) {
        this.cal_from_date = cal_from_date;
    }

    public Date getCal_to_date() {
        return cal_to_date;
    }

    public void setCal_to_date(Date cal_to_date) {
        this.cal_to_date = cal_to_date;
    }

    public boolean getChk_for_firsthalf() {
        return chk_for_firsthalf;
    }

    public void setChk_for_firsthalf(boolean chk_for_firsthalf) {
        this.chk_for_firsthalf = chk_for_firsthalf;
    }

    public boolean isChk_for_firsthalf_val() {
        return chk_for_firsthalf_val;
    }

    public void setChk_for_firsthalf_val(boolean chk_for_firsthalf_val) {
        this.chk_for_firsthalf_val = chk_for_firsthalf_val;
    }

    public boolean getChk_for_secondhalf() {
        return chk_for_secondhalf;
    }

    public void setChk_for_secondhalf(boolean chk_for_secondhalf) {
        this.chk_for_secondhalf = chk_for_secondhalf;
    }

    public boolean isChk_for_secondhalf_val() {
        return chk_for_secondhalf_val;
    }

    public void setChk_for_secondhalf_val(boolean chk_for_secondhalf_val) {
        this.chk_for_secondhalf_val = chk_for_secondhalf_val;
    }

    public String getMin_to_date() {
        return min_to_date;
    }

    public void setMin_to_date(String min_to_date) {
        this.min_to_date = min_to_date;
    }

    public boolean getChk_all() {
        return chk_all;
    }

    public void setChk_all(boolean chk_all) {
        this.chk_all = chk_all;
    }

    public DualListModel<String> getPicklst_emp() {
        if (picklst_emp.getSource() == null) {
            picklst_emp = new DualListModel<String>(emplistSource, emplistTarget);
        }
        return picklst_emp;
    }

    public void setPicklst_emp(DualListModel<String> picklst_emp) {
        this.picklst_emp = picklst_emp;
    }

    public boolean getChk_presence_for() {
        return chk_presence_for;
    }

    public void setChk_presence_for(boolean chk_presence_for) {
        this.chk_presence_for = chk_presence_for;
    }
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_view_unsaved"> 
    List<FtasBulkAttTemp> tbl_view_unsaved;

    public List<FtasBulkAttTemp> getTbl_view_unsaved() {
        return tbl_view_unsaved;
    }

    public void setTbl_view_unsaved(List<FtasBulkAttTemp> tbl_view_unsaved) {
        this.tbl_view_unsaved = tbl_view_unsaved;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="rdb_place_firsthalf"> 
    String rdb_place_firsthalf;
    List<SelectItem> rdb_place_firsthalf_options;

    public String getRdb_place_firsthalf() {
        return rdb_place_firsthalf;
    }

    public void setRdb_place_firsthalf(String rdb_place_firsthalf) {
        this.rdb_place_firsthalf = rdb_place_firsthalf;
    }

    public List<SelectItem> getRdb_place_firsthalf_options() {
        return rdb_place_firsthalf_options;
    }

    public void setRdb_place_firsthalf_options(List<SelectItem> rdb_place_firsthalf_options) {
        this.rdb_place_firsthalf_options = rdb_place_firsthalf_options;
    }

    public void setRdb_place_firsthalf_options() {
        rdb_place_firsthalf_options = new ArrayList<SelectItem>();
        List<FtasLeavemst> lst_e = ftasLeavemstFacade.findAllActualAttd(globalData.getWorking_ou());
        if (lst_e.isEmpty()) {
            rdb_place_firsthalf_options.add(new SelectItem(null, "--- Not Available ---"));
        } else {
            for (FtasLeavemst e : lst_e) {
                rdb_place_firsthalf_options.add(new SelectItem(e.getLmSrgKey(), e.getLeaveDesc()));
            }
        }
        rdb_place_firsthalf = rdb_place_firsthalf_options.get(0).getValue().toString();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="rdb_place_secondhalf"> 
    String rdb_place_secondhalf;
    List<SelectItem> rdb_place_secondhalf_options;

    public String getRdb_place_secondhalf() {
        return rdb_place_secondhalf;
    }

    public void setRdb_place_secondhalf(String rdb_place_secondhalf) {
        this.rdb_place_secondhalf = rdb_place_secondhalf;
    }

    public List<SelectItem> getRdb_place_secondhalf_options() {
        return rdb_place_secondhalf_options;
    }

    public void setRdb_place_secondhalf_options(List<SelectItem> rdb_place_secondhalf_options) {
        this.rdb_place_secondhalf_options = rdb_place_secondhalf_options;
    }

    public void setRdb_place_secondhalf_options() {
        rdb_place_secondhalf_options = new ArrayList<SelectItem>();
        List<FtasLeavemst> lst_e = ftasLeavemstFacade.findAllActualAttd(globalData.getWorking_ou());
        if (lst_e.isEmpty()) {
            rdb_place_secondhalf_options.add(new SelectItem(null, "--- Not Available ---"));
        } else {
            for (FtasLeavemst e : lst_e) {
                rdb_place_secondhalf_options.add(new SelectItem(e.getLmSrgKey(), e.getLeaveDesc()));
            }
        }
        rdb_place_secondhalf = rdb_place_secondhalf_options.get(0).getValue().toString();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="unsaved"> 
    boolean unsaved;

    public boolean isUnsaved() {
        return unsaved;
    }

    public void setUnsaved(boolean unsaved) {
        this.unsaved = unsaved;
    }
    //</editor-fold>
    // value of dropdown lists...............................
    // <editor-fold defaultstate="collapsed" desc="List of Daily Attendance of Employees">
    private List<FtasDaily> lst_FtasDaily;

    public List<FtasDaily> getLst_FtasDaily() {
        return lst_FtasDaily;
    }

    public void setLst_FtasDaily(List<FtasDaily> lst_FtasDaily) {
        this.lst_FtasDaily = lst_FtasDaily;
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Mark List of Daily Attendance of Employees">
    private List<FtasDaily> lst_FtasDaily_mark_att_detail;

    public List<FtasDaily> getLst_FtasDaily_mark_att_detail() {
        return lst_FtasDaily_mark_att_detail;
    }

    public void setLst_FtasDaily_mark_att_detail(List<FtasDaily> lst_FtasDaily_mark_att_detail) {
        this.lst_FtasDaily_mark_att_detail = lst_FtasDaily_mark_att_detail;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="list of ddl org unit list binding">
    String ddl_org_unit_list;
    List<SelectItem> ddl_org_unit_list_options;

    public String getDdl_org_unit_list() {
        return ddl_org_unit_list;
    }

    public void setDdl_org_unit_list(String ddl_org_unit_list) {
        this.ddl_org_unit_list = ddl_org_unit_list;
    }

    public List<SelectItem> getDdl_org_unit_list_options() {
        return ddl_org_unit_list_options;
    }

    public void setDdl_org_unit_list_options(List<SelectItem> ddl_org_unit_list_options) {
        this.ddl_org_unit_list_options = ddl_org_unit_list_options;
    }

    public void setDdl_org_unit_list() {
        List<SelectItem> lst_ou_option;
        lst_ou_option = setOrgList();
        ddl_org_unit_list_options = lst_ou_option;
        ddl_org_unit_list = lst_ou_option.get(0).toString();
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="employee list pick-list"> 
    List<String> emplistSource = new ArrayList<String>();
    List<String> emplistTarget = new ArrayList<String>();
    private DualListModel<String> employee_list;

    public DualListModel<String> getEmployee_list() {
        return employee_list;
    }

    public void setEmployee_list(DualListModel<String> employee_list) {
        this.employee_list = employee_list;
    }// </editor-fold>

    // valuechange events.....................................
    // <editor-fold defaultstate="collapsed" desc="cal select valuechange event">
    public void cal_select_event(SelectEvent event) {
        String v_element_id = event.getComponent().getId();
        if (v_element_id.equalsIgnoreCase("cal_from_date")) {
            if (cal_to_date != null) {
                if (((Date) event.getObject()).after(cal_to_date) == true) {
                    cal_to_date = ((Date) event.getObject());
                }
            }
        } else if (v_element_id.equalsIgnoreCase("cal_to_date")) {
            if (cal_from_date != null) {
                if (cal_from_date.after((Date) event.getObject()) == true) {
                    cal_to_date = cal_from_date;
                }
            }
        }
        unsaved = false;

    }// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="ddl org unit list valuechange event">
    public void ddl_org_unit_list_valuechange(ValueChangeEvent event) {
        ddl_org_unit_list = event.getNewValue().toString();
        if (event.getNewValue().toString().equalsIgnoreCase("") == false) {
            if (chk_all == false) {
                emplistSource.clear();
                String v_oum_srno = ddl_org_unit_list;
                try {
                    List<FhrdEmpmst> lst_temp = fhrdEmpmstFacade.findEmpListForBulkAtt(Integer.valueOf(v_oum_srno), cal_from_date, cal_to_date);
                    Integer n = lst_temp.size();
                    for (int i = 0; i < n; i++) {
                        emplistSource.add(lst_temp.get(i).getUserId() + "-" + lst_temp.get(i).getUserName());
                    }
                } catch (Exception e) {
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "ddl_org_unit_list_valuechange", null, e);
                }
                employee_list = new DualListModel<String>(emplistSource, emplistTarget);
            } else {
                emplistSource.clear();
                emplistTarget.clear();
                employee_list = new DualListModel<String>(emplistSource, emplistTarget);
            }

        } else {
            emplistSource.clear();
            emplistTarget.clear();
            employee_list = new DualListModel<String>(emplistSource, emplistTarget);
        }
        picklst_emp = employee_list;
    }
// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="chk all value change event">
    public void chk_all_valuechange(ValueChangeEvent event) {
        if (event.getNewValue().toString().equalsIgnoreCase("false") == true) {
            emplistSource.clear();
            String[] split_ou_list = ddl_org_unit_list.split("-");
            String v_oum_srno = split_ou_list[0].trim();
            try {
                List<FhrdEmpmst> lst_temp = fhrdEmpmstFacade.findEmpListForBulkAtt(Integer.valueOf(v_oum_srno), cal_from_date, cal_to_date);
                Integer n = lst_temp.size();
                for (int i = 0; i < n; i++) {
                    emplistSource.add(lst_temp.get(i).getUserId() + "-" + lst_temp.get(i).getUserName());
                }
            } catch (Exception e) {
            }
            employee_list = new DualListModel<String>(emplistSource, emplistTarget);
        } else {
            emplistSource.clear();
            emplistTarget.clear();
            employee_list = new DualListModel<String>(emplistSource, emplistTarget);
        }
        picklst_emp = employee_list;
    }// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="btn_ok_action">
    public void btn_ok_action(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        String[] split_ou_list = ddl_org_unit_list.split("-");
        String v_oum_srno = split_ou_list[0].trim();
        boolean isvalid = true;
        boolean v_check_previleges = false;
        Database_Output entDatabase_Output = ou_Pack.check_privileges(this.getClass().getName(),
                "btn_ok_action", "FHRD", globalData.getUser_id());
        String check_privileges = entDatabase_Output.getString1();
        if (check_privileges == null) {
            v_check_previleges = false;
            isvalid = false;
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "You do not have privileges to mark attendance for this Employee.You can only Mark Your own Attendance "));
            return;
        } else {
            v_check_previleges = true;
        }
        /////////////////////////////

        String v_emp_list = "";
        if (v_check_previleges && isvalid) {
            if (chk_all == true) {
                List<FhrdEmpmst> all_emp_list = null;
                try {
                    all_emp_list = fhrdEmpmstFacade.findEmpListForBulkAtt(Integer.valueOf(v_oum_srno), cal_from_date, cal_to_date);
                } catch (Exception e) {
                    isvalid = false;
                }

                Integer n = all_emp_list.size();
                for (int i = 0; i < n; i++) {
                    String v_emp_userid = all_emp_list.get(i).getUserId().toString();
                    if (i != n - 1) {
                        v_emp_list = v_emp_list + v_emp_userid + ",";
                    } else {
                        v_emp_list = v_emp_list + v_emp_userid;
                    }

                }
            } else {
                DualListModel<String> employee_list1 = picklst_emp;
                int v_emptarget_length = employee_list1.getTarget().size();
                if (v_emptarget_length == 0) {
                    isvalid = false;
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Select Any Employee From Employee List."));
                } else {
                    for (int i = 0; i < v_emptarget_length; i++) {
                        String[] split_emp_target_list = employee_list1.getTarget().get(i).toString().split("-");
                        String v_emp_userid = split_emp_target_list[0].trim();
                        if (i != v_emptarget_length - 1) {
                            v_emp_list = v_emp_list + v_emp_userid + ",";
                        } else {
                            v_emp_list = v_emp_list + v_emp_userid;
                        }
                    }

                }
            }
            if (isvalid) {
                BigDecimal v_first;
                BigDecimal v_second;
                if (chk_for_firsthalf) {
                    v_first = new BigDecimal(rdb_place_firsthalf);
                } else {
                    v_first = null;
                }
                if (chk_for_secondhalf) {
                    v_second = new BigDecimal(rdb_place_secondhalf);
                } else {
                    v_second = null;
                }
                Integer v_cal_year = (cal_from_date.getYear() + 1900);
                BigDecimal v_bulk_srg_key = getGlobalSettings().getUniqueSrno(v_cal_year, "FTAS_BULK_ATT_TEMP");
//                System.out.println("cal_to_date "+cal_to_date);
                Database_Output database_Output = ftas_Pack.BULK_ATTENDANCE_INSERT_Procedure(PIS_Bulk_Attendance.class.getName(), "btn_ok_action", v_bulk_srg_key, v_emp_list, cal_from_date, cal_to_date, v_first, v_second, globalData.getUser_id());
                if (database_Output.isExecuted_successfully()) {
                    resetBulkAttendance();
                    List<FtasBulkAttTemp> lstentBulkAttTemp = ftasBulkAttTempFacade.findAllUnsavedRecord(v_bulk_srg_key);
                    if (lstentBulkAttTemp.isEmpty()) {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record saved successfully"));
                    } else {
                        tbl_view_unsaved = lstentBulkAttTemp;
                        unsaved = true;
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Some Records are not saved so Check Unsaved Data for Detail..."));
                    }
                } else {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Record not saved. Error to mark Bulk Attendance "));
                }
            }
        }

    }// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="resetBulkAttendance()">
    public void resetBulkAttendance() {
        setDdl_org_unit_list();
        setRdb_place_firsthalf_options();
        setRdb_place_secondhalf_options();
        chk_for_firsthalf = false;
        chk_for_secondhalf = false;
        emplistSource = new ArrayList<String>();
        emplistTarget = new ArrayList<String>();
        employee_list = new DualListModel<String>(emplistSource, emplistTarget);
        picklst_emp = employee_list;
        cal_from_date = null;
        cal_to_date = null;
    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="setOrgList()"> 

    public List<SelectItem> setOrgList() {
        List<SelectItem> lst_org_option = new ArrayList<SelectItem>();
        List<SystOrgUnitMst> lst_e = systOrgUnitMstFacade.findAll(globalData.getWorking_ou(), false, false, false, false, true, null, null);;
        if (lst_e.isEmpty()) {
            SelectItem s = new SelectItem(null, "No Data Found");
            lst_org_option.add(s);
        } else {
            SelectItem s = new SelectItem(null, "----Select Any----");
            lst_org_option.add(s);
            Integer n = lst_e.size();
            for (int i = 0; i < n; i++) {
                s = new SelectItem(lst_e.get(i).getOumUnitSrno(), lst_e.get(i).getOumName());
                lst_org_option.add(s);
            }
        }
        return lst_org_option;
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="init()">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setDdl_org_unit_list();
        setRdb_place_firsthalf_options();
        setRdb_place_secondhalf_options();
        unsaved = false;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Default Constructor & Other Beans">
    // <editor-fold defaultstate="collapsed" desc="default methods">

    public PIS_Bulk_Attendance() {
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="getPIS_GlobalSettings()">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    //</editor-fold>
}
