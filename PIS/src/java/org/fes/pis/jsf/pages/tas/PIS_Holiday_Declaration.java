package org.fes.pis.jsf.pages.tas;

import java.io.Serializable;
import java.util.*;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.validation.ConstraintViolationException;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.Database_Output;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.db.fes_project.pack.Ftas_Pack;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.FtasHolidayDtl;
import org.fes.pis.entities.FtasHolidayDtlPK;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FtasHolidayDtlFacadeLocal;
import org.fes.pis.sessions.SystOrgUnitMstFacadeLocal;

@ManagedBean(name = "PIS_Holiday_Declaration")
@ViewScoped
public class PIS_Holiday_Declaration implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="Declarations"> 
    
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    boolean isValidUser = false;
    Ou_Pack ou_Pack = new Ou_Pack();
    Ftas_Pack ftas_Pack = new Ftas_Pack();
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="EJB Declaraion">
    @EJB
    private SystOrgUnitMstFacadeLocal systOrgUnitMstFacade;
    @EJB
    private FtasHolidayDtlFacadeLocal ftasHolidayDtlFacade;
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Binded attributes">
    Date cal_schedule;
    Date cal_sudden;
    String txt_sudden_desc;
    String txt_dlg_holiday_desc;
    String txt_dlg_new_holiday_desc;
    List<FtasHolidayDtl> tbl_holiday;
    String min_date = null;
    String max_date = null;
    List<String> selectedWeekend = new ArrayList<String>();
    boolean chk_weekdays_disabled;

    // <editor-fold defaultstate="collapsed" desc="getter setter methods">
    public String getTxt_dlg_new_holiday_desc() {
        return txt_dlg_new_holiday_desc;
    }

    public void setTxt_dlg_new_holiday_desc(String txt_dlg_new_holiday_desc) {
        this.txt_dlg_new_holiday_desc = txt_dlg_new_holiday_desc;
    }

    public boolean isChk_weekdays_disabled() {
        return chk_weekdays_disabled;
    }

    public void setChk_weekdays_disabled(boolean chk_weekdays_disabled) {
        this.chk_weekdays_disabled = chk_weekdays_disabled;
    }

    public List<String> getSelectedWeekend() {
        return selectedWeekend;
    }

    public void setSelectedWeekend(List<String> selectedWeekend) {
        this.selectedWeekend = selectedWeekend;
    }

    public String getMax_date() {
        return max_date;
    }

    public void setMax_date(String max_date) {
        this.max_date = max_date;
    }

    public String getMin_date() {
        return min_date;
    }

    public void setMin_date(String min_date) {
        this.min_date = min_date;
    }

    public List<FtasHolidayDtl> getTbl_holiday() {
        return tbl_holiday;
    }

    public void setTbl_holiday(List<FtasHolidayDtl> tbl_holiday) {
        this.tbl_holiday = tbl_holiday;
    }

    public String getTxt_dlg_holiday_desc() {
        return txt_dlg_holiday_desc;
    }

    public void setTxt_dlg_holiday_desc(String txt_dlg_holiday_desc) {
        this.txt_dlg_holiday_desc = txt_dlg_holiday_desc;
    }

    public String getTxt_sudden_desc() {
        return txt_sudden_desc;
    }

    public void setTxt_sudden_desc(String txt_sudden_desc) {
        this.txt_sudden_desc = txt_sudden_desc;
    }

    public Date getCal_schedule() {
        return cal_schedule;
    }

    public void setCal_schedule(Date cal_schedule) {
        this.cal_schedule = cal_schedule;
    }

    public Date getCal_sudden() {
        return cal_sudden;
    }

    public void setCal_sudden(Date cal_sudden) {
        this.cal_sudden = cal_sudden;
    }
    // </editor-fold>
    // </editor-fold>
    String auto_schedule_desc;
    List<String> scheduleList;

    public String getAuto_schedule_desc() {
        return auto_schedule_desc;
    }

    public void setAuto_schedule_desc(String auto_schedule_desc) {
        this.auto_schedule_desc = auto_schedule_desc;
    }

    public List<String> getScheduleList() {
        return scheduleList;
    }

    public void setScheduleList(List<String> scheduleList) {
        this.scheduleList = scheduleList;
    }

    private void setAuto_schedule_desc() {
        scheduleList = ftasHolidayDtlFacade.findAllHoliDesc(Integer.valueOf(ddl_year));
    }

    public List<String> completeScheduleList(String query) {
        List<String> auto_college_options = new ArrayList<String>();
        for (String s : scheduleList) {
            if (s.contains(query.toUpperCase())) {
                auto_college_options.add(s);
            }
        }
        return auto_college_options;
    }
    //<editor-fold defaultstate="collapsed" desc="lst_yes_no_options">
    SelectItem[] lst_yes_no_options;

    public SelectItem[] getLst_yes_no_options() {
        return lst_yes_no_options;
    }

    public void setLst_yes_no_options(SelectItem[] lst_yes_no_options) {
        this.lst_yes_no_options = lst_yes_no_options;
    }

    public void setLst_yes_no_options() {
        lst_yes_no_options = new SelectItem[2];
        lst_yes_no_options[0] = new SelectItem("", "All");
        lst_yes_no_options[1] = new SelectItem("Y", "Yes");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lst_yes_no_options schedule">
    SelectItem[] lst_yes_no_options_s;

    public SelectItem[] getLst_yes_no_options_s() {
        return lst_yes_no_options_s;
    }

    public void setLst_yes_no_options_s(SelectItem[] lst_yes_no_options_s) {
        this.lst_yes_no_options_s = lst_yes_no_options_s;
    }

    public void setLst_yes_no_options_s() {
        lst_yes_no_options_s = new SelectItem[2];
        lst_yes_no_options_s[0] = new SelectItem("", "All");
        lst_yes_no_options_s[1] = new SelectItem("Y", "Yes");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lst_yes_no_options_d">
    SelectItem[] lst_yes_no_options_d;

    public SelectItem[] getLst_yes_no_options_d() {
        return lst_yes_no_options_d;
    }

    public void setLst_yes_no_options_d(SelectItem[] lst_yes_no_options_d) {
        this.lst_yes_no_options_d = lst_yes_no_options_d;
    }

    public void setLst_yes_no_options_d() {
        lst_yes_no_options_d = new SelectItem[2];
        lst_yes_no_options_d[0] = new SelectItem("", "All");
        lst_yes_no_options_d[1] = new SelectItem("Y", "Yes");
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="defaulf constructor">
    /**
     * Creates a new instance of PIS_Holiday_Declaration
     */
    public PIS_Holiday_Declaration() {
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="getter/setter method for oum">
    private String txt_ou_name = new String();

    public String getTxt_ou_name() {
        return txt_ou_name;
    }

    public void setTxt_ou_name(String txt_ou_name) {
        this.txt_ou_name = txt_ou_name;
    }

    public void setTxt_ou_name() {
        String v_ou_name = systOrgUnitMstFacade.find(globalData.getEnt_working_ou().getOumNativeOuNo()).getOumName();
        txt_ou_name = v_ou_name;
    }
    // </editor-fold>  
    // <editor-fold defaultstate="collapsed" desc="ddl year">
    String ddl_year;
    List<SelectItem> ddl_year_options;

    public List<SelectItem> getDdl_year_options() {
        return ddl_year_options;
    }

    public void setDdl_year_options(List<SelectItem> ddl_year_options) {
        this.ddl_year_options = ddl_year_options;
    }

    public String getDdl_year() {
        return ddl_year;
    }

    public void setDdl_year(String ddl_year) {
        this.ddl_year = ddl_year;
    }

    public void setDdl_year_options() {
        ddl_year_options = new ArrayList<SelectItem>();
        List<String> lst_year = ftasHolidayDtlFacade.getYearListForHolidayProcess(globalData.getWorking_ou());
        for (String s : lst_year) {
            ddl_year_options.add(new SelectItem(s));
        }
        ddl_year = ddl_year_options.get(0).getValue().toString();
    }

// </editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="Add Record">
    // <editor-fold defaultstate="collapsed" desc="add weekend record">
    public String btn_weekend_clicked() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (!ddl_year.isEmpty()) {
            if (!chk_weekdays_disabled) {
                String v_weekendType = "w";
                Integer n = selectedWeekend.size();
                String v_weekend_list = "";
                if (n != 0) {
                    for (int i = 0; i < n; i++) {
                        v_weekend_list += selectedWeekend.get(i).toUpperCase() + ",";
                    }
                    v_weekend_list = v_weekend_list.substring(0, v_weekend_list.length() - 1);
                    addRecord(ddl_year, v_weekend_list, v_weekendType, null);
                } else {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Warning", "Select any day for weekend"));
                }
            } else {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Warning", "Warning: Weekend already selected and added for the year-" + ddl_year));
            }
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Warning", "Warning: First you have to select Year"));
        }

        return null;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="add schedule record">

    public void btn_schedule_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();

        if (String.valueOf((cal_schedule.getYear() + 1900)).equals(ddl_year)) {
            String v_ddl_year = ddl_year;
            String v_scheduleDesc = auto_schedule_desc.toUpperCase();
            String v_scheduleType = "S";
            addRecord(v_ddl_year, v_scheduleDesc, v_scheduleType, cal_schedule);
            cal_schedule = null;
            auto_schedule_desc = "";
            setAuto_schedule_desc();
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Warning", "Warning: Select Date according to Selected Year."));
            ddl_year = null;
        }
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="add sudden record">

    public void btn_sudden_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (String.valueOf((cal_sudden.getYear() + 1900)).equals(ddl_year)) {
            String v_ddl_year = ddl_year;
            if (txt_sudden_desc.equalsIgnoreCase("") == false) {
                String v_suddenDesc = txt_sudden_desc.toUpperCase();
                String v_suddenType = "D";
                addRecord(v_ddl_year, v_suddenDesc, v_suddenType, cal_sudden);
                cal_sudden = null;
                txt_sudden_desc = "";
            }
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Warning", "Warning: Select Date according to Selected Year."));
            ddl_year = null;
        }
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="common Add Record Function">

    public void addRecord(String v_Year, String v_Holiday_Desc, String v_Holiday_Type, Date v_Holiday_Date) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if (v_Year.isEmpty() || (v_Holiday_Date == null && !v_Holiday_Type.equalsIgnoreCase("W")) || v_Holiday_Desc.isEmpty() || v_Holiday_Type.isEmpty()) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "Fill up All Entries"));
            } else {
                Database_Output database_Output = new Database_Output();
//                database_Output.setExecuted_successfully(true);
//                System.out.println("working "+globalData.getWorking_ou());
                database_Output = ftas_Pack.Holiday_Process(this.getClass().getName(), "addRecord",
                        String.valueOf(globalData.getWorking_ou()),
                        v_Year,
                        String.valueOf(globalData.getUser_id()), v_Holiday_Desc, v_Holiday_Type, v_Holiday_Date);
                if (database_Output.isExecuted_successfully()) {
                    if (v_Holiday_Type.equalsIgnoreCase("w")) {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully for Weekend As " + v_Holiday_Desc.toUpperCase()));
                    } else {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully for Holiday Date " + DateTimeUtility.ChangeDateFormat(v_Holiday_Date, null) + " As " + v_Holiday_Desc.toUpperCase()));
                    }
                    cal_schedule = null;
                    cal_sudden = null;
                    cal_sudden = null;
                    setTbl_holiday();
                } else {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred during saving records"));
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), " Method : addRecord : ", null, null);
                }
            }
        } catch (Exception e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "addRecord", null, e);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred during saving records"));
        }
    }// </editor-fold>
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Value change Events">
    // <editor-fold defaultstate="collapsed" desc="lst year value change">

    public void ddl_year_changed(ValueChangeEvent event) {
        ddl_year = event.getNewValue().toString();
        setTbl_holiday();
    }

    public void setTbl_holiday() {
        Integer v_year = Integer.valueOf(ddl_year);
        Date minDate = new Date(v_year - 1900, 0, 1);
        Date maxDate = new Date(v_year - 1900, 11, 31);
        if (v_year != null) {
            tbl_holiday = ftasHolidayDtlFacade.findHolidayYearWise(v_year, globalData.getEnt_working_ou().getOumNativeOuNo());
            if (tbl_holiday.isEmpty()) {
                chk_weekdays_disabled = false;
                selectedWeekend = null;
                tbl_holiday = null;
            } else {
                resetCheckBox(v_year);
                chk_weekdays_disabled = true;

            }
            min_date = DateTimeUtility.ChangeDateFormat(minDate, null);
            max_date = DateTimeUtility.ChangeDateFormat(maxDate, null);
        } else {
            min_date = null;
            max_date = null;
            chk_weekdays_disabled = false;
            selectedWeekend = null;
            tbl_holiday = null;
        }
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Reset check box event">

    public void resetCheckBox(Integer v_year) {
        selectedWeekend = ftasHolidayDtlFacade.findWeekendDays(v_year);
        chk_weekdays_disabled = true;
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="for selected record (to update record) ">
    private List<FtasHolidayDtl> selectedFtasHolidayDtl;
    private Map<String, Boolean> selectedFtasHolidayDtl_Desc = new HashMap<String, Boolean>();

    public List<FtasHolidayDtl> getSelectedFtasHolidayDtl() {
        return selectedFtasHolidayDtl;
    }

    public void setSelectedFtasHolidayDtl(List<FtasHolidayDtl> selectedFtasHolidayDtl) {
        this.selectedFtasHolidayDtl = selectedFtasHolidayDtl;
    }

    public Map<String, Boolean> getSelectedFtasHolidayDtl_Desc() {
        return selectedFtasHolidayDtl_Desc;
    }

    public void setSelectedFtasHolidayDtl_Desc(Map<String, Boolean> selectedFtasHolidayDtl_Desc) {
        this.selectedFtasHolidayDtl_Desc = selectedFtasHolidayDtl_Desc;
    }
    // </editor-fold>
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Btn Action events">
    // <editor-fold defaultstate="collapsed" desc="btn dlg update action event">
    // <editor-fold defaultstate="collapsed" desc="ent_selected_holiday">
    FtasHolidayDtl ent_selected_holiday = new FtasHolidayDtl();

    public FtasHolidayDtl getEnt_selected_holiday() {
        return ent_selected_holiday;
    }

    public void setEnt_selected_holiday(FtasHolidayDtl ent_selected_holiday) {
        txt_dlg_holiday_date = DateTimeUtility.ChangeDateFormat(ent_selected_holiday.getFtasHolidayDtlPK().getHoliDate(), null);
        txt_dlg_holiday_desc = ent_selected_holiday.getHoliDesc();
        if (ent_selected_holiday.getWeekendFlag() == null) {
            chk_weekend = false;
            chk_weekdays_disabled = false;
        } else if (ent_selected_holiday.getWeekendFlag().equalsIgnoreCase("y")) {
            chk_weekend = true;
            chk_weekend_disabled = true;
        } else {
            chk_weekend = false;
            chk_weekdays_disabled = false;
        }
        if (ent_selected_holiday.getScheduleFlag() == null) {
            chk_schedule = false;
            chk_schedule_disabled = false;
        } else if (ent_selected_holiday.getScheduleFlag().equalsIgnoreCase("y")) {
            chk_schedule = true;
            chk_schedule_disabled = true;
        } else {
            chk_schedule = false;
            chk_schedule_disabled = false;
        }
        if (ent_selected_holiday.getSuddenDeclareFlag() == null) {
            chk_sudden = false;
            chk_sudden_disabled = false;
        } else if (ent_selected_holiday.getSuddenDeclareFlag().equalsIgnoreCase("y")) {
            chk_sudden = true;
            chk_sudden_disabled = true;
        } else {
            chk_sudden = false;
            chk_sudden_disabled = false;
        }
        this.ent_selected_holiday = ent_selected_holiday;
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="btn_update_row_action">

    public void btn_update_row_action(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        FhrdEmpmst ent_FhrdEmpmst = new FhrdEmpmst(globalData.getUser_id());
        FtasHolidayDtlPK PK_FtasHolidayDtlPK = ent_selected_holiday.getFtasHolidayDtlPK();

        FtasHolidayDtl ent_FtasHolidayDtl = ftasHolidayDtlFacade.find(PK_FtasHolidayDtlPK);
        if (chk_schedule) {
            ent_FtasHolidayDtl.setScheduleFlag("Y");
        }
        if (chk_sudden) {
            ent_FtasHolidayDtl.setSuddenDeclareFlag("Y");
        }
        if (chk_weekend) {
            ent_FtasHolidayDtl.setWeekendFlag("Y");
        }
        ent_FtasHolidayDtl.setFtasHolidayDtlPK(PK_FtasHolidayDtlPK);
        ent_FtasHolidayDtl.setHoliDesc(txt_dlg_holiday_desc.toUpperCase() + "," + txt_dlg_new_holiday_desc.toUpperCase());
        ent_FtasHolidayDtl.setUpdateby(ent_FhrdEmpmst);
        ent_FtasHolidayDtl.setUpdatedt(new Date());
        try {
            ftasHolidayDtlFacade.edit(ent_FtasHolidayDtl);
            setTbl_holiday();
            txt_dlg_new_holiday_desc = "";
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Holiday updated successfully"));
        } catch (ConstraintViolationException e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured"));
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured"));
        }
    }// </editor-fold>

    public void btn_holiday_update_action(ActionEvent event) {
    }
    // </editor-fold>
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="dlg binding attributes">
    private String txt_dlg_holiday_date;
    private String txt_dlg_holiday_day;
    boolean chk_weekend;
    boolean chk_schedule;
    boolean chk_sudden;
    boolean chk_weekend_disabled;
    boolean chk_schedule_disabled;
    boolean chk_sudden_disabled;

    public boolean getChk_schedule() {
        return chk_schedule;
    }

    public void setChk_schedule(boolean chk_schedule) {
        this.chk_schedule = chk_schedule;
    }

    public boolean isChk_schedule_disabled() {
        return chk_schedule_disabled;
    }

    public void setChk_schedule_disabled(boolean chk_schedule_disabled) {
        this.chk_schedule_disabled = chk_schedule_disabled;
    }

    public boolean isChk_sudden_disabled() {
        return chk_sudden_disabled;
    }

    public void setChk_sudden_disabled(boolean chk_sudden_disabled) {
        this.chk_sudden_disabled = chk_sudden_disabled;
    }

    public boolean isChk_weekend_disabled() {
        return chk_weekend_disabled;
    }

    public void setChk_weekend_disabled(boolean chk_weekend_disabled) {
        this.chk_weekend_disabled = chk_weekend_disabled;
    }

    public boolean getChk_sudden() {
        return chk_sudden;
    }

    public void setChk_sudden(boolean chk_sudden) {
        this.chk_sudden = chk_sudden;
    }

    public boolean getChk_weekend() {
        return chk_weekend;
    }

    public void setChk_weekend(boolean chk_weekend) {
        this.chk_weekend = chk_weekend;
    }

    public String getTxt_dlg_holiday_date() {
        return txt_dlg_holiday_date;
    }

    public void setTxt_dlg_holiday_date(String txt_dlg_holiday_date) {
        this.txt_dlg_holiday_date = txt_dlg_holiday_date;
    }

    public String getTxt_dlg_holiday_day() {
        return txt_dlg_holiday_day;
    }

    public void setTxt_dlg_holiday_day(String txt_dlg_holiday_day) {
        this.txt_dlg_holiday_day = txt_dlg_holiday_day;
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setTxt_ou_name();
        setDdl_year_options();
        setLst_yes_no_options();
        setLst_yes_no_options_s();
        setLst_yes_no_options_d();
        setTbl_holiday();
        setAuto_schedule_desc();
    }
    //</editor-fold>
}
