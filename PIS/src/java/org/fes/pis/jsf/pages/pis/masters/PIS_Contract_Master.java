package org.fes.pis.jsf.pages.pis.masters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.customJpaController;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.FhrdContractGroupRelation;
import org.fes.pis.entities.FhrdContractMst;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.FhrdRuleApplGroupMst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdContractMstFacadeLocal;
import org.fes.pis.sessions.FhrdRuleApplGroupMstFacadeLocal;
import org.fes.pis.sessions.FhrdRuleGroupCategoryFacadeLocal;
import org.fes.pis.view_entities.FhrdContractGroupData;
import org.fes.pis.view_sessions.FhrdContractGroupDataFacade;
import org.primefaces.context.RequestContext;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Contract_Master")
@ViewScoped
public class PIS_Contract_Master implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Declarations"> 
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    Ou_Pack ou_Pack = new Ou_Pack();
    //</editor-fold> 
    // <editor-fold defaultstate="collapsed" desc="EJB Declaration">
    @EJB
    private FhrdContractGroupDataFacade fhrdContractGroupDataFacade;
    @EJB
    private customJpaController customJpaController;
    @EJB
    private FhrdRuleApplGroupMstFacadeLocal fhrdRuleApplGroupMstFacade;
    @EJB
    private FhrdRuleGroupCategoryFacadeLocal fhrdRuleGroupCategoryFacade;
    @EJB
    private FhrdContractMstFacadeLocal fhrdContractMstFacade;// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Default Method">

    /**
     * Creates a new instance of PYRL_Contract_Master
     */
    public PIS_Contract_Master() {
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="BINDING ATTRIBUTES">
    String txt_contract_desc;
    String txt_Contract_period;
    String ddl_contract_unit;
    String ddl_contract_category;

    // <editor-fold defaultstate="collapsed" desc="GETTER/SETTER">
    public String getTxt_Contract_period() {
        return txt_Contract_period;
    }

    public void setTxt_Contract_period(String txt_Contract_period) {
        this.txt_Contract_period = txt_Contract_period;
    }

    public String getDdl_contract_category() {
        return ddl_contract_category;
    }

    public void setDdl_contract_category(String ddl_contract_category) {
        this.ddl_contract_category = ddl_contract_category;
    }

    public String getTxt_contract_desc() {
        return txt_contract_desc;
    }

    public void setTxt_contract_desc(String txt_contract_desc) {
        this.txt_contract_desc = txt_contract_desc;
    }

    public String getDdl_contract_unit() {
        return ddl_contract_unit;
    }

    public void setDdl_contract_unit(String ddl_contract_unit) {
        this.ddl_contract_unit = ddl_contract_unit;
    }// </editor-fold>

    public void ddl_contract_category_changed(ValueChangeEvent event) {
        if (event.getNewValue() != null) {
            ddl_contract_category = event.getNewValue().toString();
            setDdl_contract_alternative_options(ddl_contract_category);
        } else {
            setDdl_contract_alternative_options("");
        }
        setDdl_group_desc();
    }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Date">
    Date cal_contract_start_date;
    Date cal_contract_end_date;

    public Date getCal_contract_end_date() {
        return cal_contract_end_date;
    }

    public void setCal_contract_end_date(Date cal_contract_end_date) {
        this.cal_contract_end_date = cal_contract_end_date;
    }

    public Date getCal_contract_start_date() {
        return cal_contract_start_date;
    }

    public void setCal_contract_start_date(Date cal_contract_start_date) {
        this.cal_contract_start_date = cal_contract_start_date;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ddl_group_desc">
    String ddl_group_desc;
    List<SelectItem> ddl_group_desc_options;

    public String getDdl_group_desc() {
        return ddl_group_desc;
    }

    public void setDdl_group_desc(String ddl_group_desc) {
        this.ddl_group_desc = ddl_group_desc;
    }

    public List<SelectItem> getDdl_group_desc_options() {
        return ddl_group_desc_options;
    }

    public void setDdl_group_desc_options(List<SelectItem> ddl_group_desc_options) {
        this.ddl_group_desc_options = ddl_group_desc_options;
    }

    public void setDdl_group_desc() {
        ddl_group_desc_options = new ArrayList<SelectItem>();
        if (ddl_contract_category != null) {
            List<FhrdRuleApplGroupMst> lst_grouplist = fhrdRuleApplGroupMstFacade.find_applicable_group(ddl_contract_category);
            if (lst_grouplist.isEmpty()) {
                SelectItem s = new SelectItem(null, "--- Not Available ---");
                ddl_group_desc_options.add(s);
            } else {
                SelectItem s = new SelectItem(null, "--- Select ---");
                ddl_group_desc_options.add(s);
                Integer n = lst_grouplist.size();
                for (int i = 0; i < n; i++) {
                    s = new SelectItem(lst_grouplist.get(i).getRagmSrgKey(), lst_grouplist.get(i).getApplGroupDesc());
                    ddl_group_desc_options.add(s);
                }
            }
        } else {
            SelectItem s = new SelectItem(null, "--- Not Available ---");
            ddl_group_desc_options.add(s);
        }
        ddl_group_desc = null;
    }
    // </editor-fold>
    // all table values
    // <editor-fold defaultstate="collapsed" desc="tbl contract mst">
    List<FhrdContractMst> tbl_contract_mst;
    List<FhrdContractMst> tbl_contract_mst_filtered;

    public List<FhrdContractMst> getTbl_contract_mst_filtered() {
        return tbl_contract_mst_filtered;
    }

    public void setTbl_contract_mst_filtered(List<FhrdContractMst> tbl_contract_mst_filtered) {
        this.tbl_contract_mst_filtered = tbl_contract_mst_filtered;
    }

    public List<FhrdContractMst> getTbl_contract_mst() {
        return tbl_contract_mst;
    }

    public void setTbl_contract_mst(List<FhrdContractMst> tbl_contract_mst) {
        this.tbl_contract_mst = tbl_contract_mst;
    }

    public void setTblContractMst() {
        tbl_contract_mst = fhrdContractMstFacade.findAll(globalData.getWorking_ou());
    }
    // </editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="filter_contract_type">
    List<SelectItem> filter_contract_type;

    public List<SelectItem> getFilter_contract_type() {
        return filter_contract_type;
    }

    public void setFilter_contract_type(List<SelectItem> filter_contract_type) {
        this.filter_contract_type = filter_contract_type;
    }

    private void setFilter_contract_type() {
        filter_contract_type = new ArrayList<SelectItem>();
        filter_contract_type.add(new SelectItem("", "All"));
        filter_contract_type.add(new SelectItem("M", "Main"));
        filter_contract_type.add(new SelectItem("S", "Supplement"));
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_contract_group_data">
    List<FhrdContractGroupData> tbl_contract_group_data;

    public List<FhrdContractGroupData> getTbl_contract_group_data() {
        return tbl_contract_group_data;
    }

    public void setTbl_contract_group_data(List<FhrdContractGroupData> tbl_contract_group_data) {
        this.tbl_contract_group_data = tbl_contract_group_data;
    }
    //</editor-fold>   
    //<editor-fold defaultstate="collapsed" desc="btn_view_detail_click"> 

    public void btn_view_detail_click(ActionEvent event) {
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="ent_view_contract_detail"> 
    FhrdContractMst ent_view_contract_detail;

    public FhrdContractMst getEnt_view_contract_detail() {
        return ent_view_contract_detail;
    }

    public void setEnt_view_contract_detail(FhrdContractMst ent_view_contract_detail) {
        tbl_contract_group_data = fhrdContractGroupDataFacade.findAllContractwise(ent_view_contract_detail.getCmSrgKey());
        this.ent_view_contract_detail = ent_view_contract_detail;
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="ddl_contract_alternative"> 
    String ddl_contract_alternative;
    List<SelectItem> ddl_contract_alternative_options;

    public String getDdl_contract_alternative() {
        return ddl_contract_alternative;
    }

    public void setDdl_contract_alternative(String ddl_contract_alternative) {
        this.ddl_contract_alternative = ddl_contract_alternative;
    }

    public List<SelectItem> getDdl_contract_alternative_options() {
        return ddl_contract_alternative_options;
    }

    public void setDdl_contract_alternative_options(List<SelectItem> ddl_contract_alternative_options) {
        this.ddl_contract_alternative_options = ddl_contract_alternative_options;
    }

    public void setDdl_contract_alternative_options(String p_contract_category) {
        List<SelectItem> lst_temp = new ArrayList<SelectItem>();
        if (p_contract_category.isEmpty()) {
            SelectItem s = new SelectItem(null, "--- Not Availalbe ---");
            lst_temp.add(s);
        } else {
            List<FhrdContractMst> lstContractMsts = fhrdContractMstFacade.findContractListOuwise(globalData.getWorking_ou(), p_contract_category);
            lst_temp = new ArrayList<SelectItem>();
            if (lstContractMsts.isEmpty()) {
                SelectItem s = new SelectItem(null, "--- Not Availalbe ---");
                lst_temp.add(s);
            } else {
                SelectItem s = new SelectItem(null, "--- Select ---");
                lst_temp.add(s);
                Integer n = lstContractMsts.size();
                for (int i = 0; i < n; i++) {
                    s = new SelectItem(lstContractMsts.get(i).getCmSrgKey(), lstContractMsts.get(i).getContractDesc());
                    lst_temp.add(s);
                }
            }

        }
        ddl_contract_alternative_options = lst_temp;
        ddl_contract_alternative = null;
    }

    //</editor-fold>
    // btn action events..
    // <editor-fold defaultstate="collapsed" desc="btn_add_new_action">
    public void btn_add_new_action(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            if (fhrdContractMstFacade.isContractDescExists(globalData.getWorking_ou(), txt_contract_desc.toUpperCase().trim(), cal_contract_start_date, cal_contract_end_date)) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Contract Description Exists", "Contract description is already exists in given period"));
                return;
            }
            FhrdEmpmst ent_Empmst = new FhrdEmpmst(globalData.getUser_id());
            //FhrdContractMst
            BigDecimal v_cm_srgkey = getGlobalSettings().getUniqueSrno(null, "FHRD_CONTRACT_MST");
            FhrdContractMst ent_ContractMst = new FhrdContractMst();
            ent_ContractMst.setCmSrgKey(v_cm_srgkey);

            ////ent_ContractMst.setContractType(txt_contract_type);
            ent_ContractMst.setContractDesc(txt_contract_desc.toUpperCase());
            ent_ContractMst.setContractCategory(ddl_contract_category.toUpperCase());
            ent_ContractMst.setStartDate(cal_contract_start_date);
            ent_ContractMst.setEndDate(cal_contract_end_date);
            ent_ContractMst.setCreateby(globalData.getEnt_login_user());
            ent_ContractMst.setCreatedt(new Date());
            ent_ContractMst.setOumUnitSrno(globalData.getEnt_working_ou());
            if (ddl_contract_alternative != null) {
                ent_ContractMst.setCmAltSrno(fhrdContractMstFacade.find(new BigDecimal(ddl_contract_alternative)).getCmAltSrno());
            } else {
//            BigDecimal v_alt_srno = fhrdContractMstFacade.findMaxAltSrno();
//            ent_ContractMst.setCmAltSrno(Integer.valueOf(v_alt_srno.toString()) + 1);
//            ent_ContractMst.setCmAltSubsrno(1);
            }

            //FhrdContractGroupRelation
            BigDecimal v_ragm_srg_key = new BigDecimal(ddl_group_desc);
            BigDecimal v_cgr_srgkey = getGlobalSettings().getUniqueSrno(null, "FHRD_CONTRACT_GROUP_RELATION");
            FhrdContractMst entContractMst = new FhrdContractMst(v_cm_srgkey);
            FhrdRuleApplGroupMst entApplGroupMst = new FhrdRuleApplGroupMst(v_ragm_srg_key);
            FhrdContractGroupRelation entContractGroupRelation = new FhrdContractGroupRelation();
            entContractGroupRelation.setCgrSrgKey(v_cgr_srgkey);
            entContractGroupRelation.setStartDate(cal_contract_start_date);
            entContractGroupRelation.setEndDate(cal_contract_end_date);
            entContractGroupRelation.setRagmSrgKey(entApplGroupMst);
            entContractGroupRelation.setCmSrgKey(entContractMst);
            entContractGroupRelation.setCreateby(ent_Empmst);
            entContractGroupRelation.setCreatedt(new Date());
            entContractGroupRelation.setOumUnitSrno(globalData.getEnt_working_ou());

            boolean isTransactionComplete = customJpaController.createContract(ent_ContractMst, entContractGroupRelation);
            if (isTransactionComplete) //fhrdContractMstFacade.create(ent_ContractMst);
            {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully"));
                setTblContractMst();
                txt_contract_desc = null;
//            txt_contract_type = null;
                ddl_contract_category = null;
                ddl_contract_alternative = null;
                cal_contract_start_date = null;
                cal_contract_end_date = null;
                ddl_group_desc = null;
                ent_view_contract_detail = null;
            } else {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred during saving record"));
            }
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred during saving record"));
        }
    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Apply End Date"> 

    public void btn_apply_date_action(ActionEvent event) {
    }
    FhrdContractMst entFhrdContractMst_edit;

    public FhrdContractMst getEntFhrdContractMst_edit() {
        return entFhrdContractMst_edit;
    }

    public void setEntFhrdContractMst_edit(FhrdContractMst entFhrdContractMst_edit) {
        this.entFhrdContractMst_edit = entFhrdContractMst_edit;
        cal_apply_end_date_min = DateTimeUtility.ChangeDateFormat(entFhrdContractMst_edit.getStartDate(), null);
        cal_apply_end_date = null;
    }
    Date cal_apply_end_date;
    String cal_apply_end_date_min;

    public String getCal_apply_end_date_min() {
        return cal_apply_end_date_min;
    }

    public void setCal_apply_end_date_min(String cal_apply_end_date_min) {
        this.cal_apply_end_date_min = cal_apply_end_date_min;
    }

    public Date getCal_apply_end_date() {
        return cal_apply_end_date;
    }

    public void setCal_apply_end_date(Date cal_apply_end_date) {
        this.cal_apply_end_date = cal_apply_end_date;
    }

    public void btn_apply_end_date_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        BigDecimal v_cm_srg_key = entFhrdContractMst_edit.getCmSrgKey();
        FhrdContractMst entFhrdContractMst = fhrdContractMstFacade.find(v_cm_srg_key);
        entFhrdContractMst.setEndDate(cal_apply_end_date);
        entFhrdContractMst.setUpdateby(globalData.getEnt_login_user());
        entFhrdContractMst.setUpdatedt(new Date());
        try {
            fhrdContractMstFacade.edit(entFhrdContractMst);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record updated Successfully"));
            setTblContractMst();
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some error occured during updating Rule"));
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_apply_end_date_clicked", null, e);
        }
    }
    //</editor-fold>
    // other functions
    //<editor-fold defaultstate="collapsed" desc="Alternate Contract View Settings">
    //<editor-fold defaultstate="collapsed" desc="tbl_view_alt_contract">
    List<FhrdContractMst> tbl_view_alt_contract;

    public List<FhrdContractMst> getTbl_view_alt_contract() {
        return tbl_view_alt_contract;
    }

    public void setTbl_view_alt_contract(List<FhrdContractMst> tbl_view_alt_contract) {
        this.tbl_view_alt_contract = tbl_view_alt_contract;
    }
    //</editor-fold>

    public void btn_view_alternate_contract_action(FhrdContractMst ent_ContractMst) {
        tbl_view_alt_contract = fhrdContractMstFacade.findAllAlternateContracts(ent_ContractMst.getCmAltSrno(), globalData.getWorking_ou());
        RequestContext.getCurrentInstance().execute("pw_dlg_view_alternate_contract.show();");
    }

    public void view_alternate_contract(ActionEvent event) {
        tbl_view_alt_contract = fhrdContractMstFacade.findAllAlternateContracts(fhrdContractMstFacade.find(new BigDecimal(ddl_contract_alternative)).getCmAltSrno(), globalData.getWorking_ou());
        RequestContext.getCurrentInstance().execute("pw_dlg_view_alternate_contract.show();");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setFilter_contract_type();
        setTblContractMst();
        setDdl_group_desc();
        List<SelectItem> lst = new ArrayList<SelectItem>();
        SelectItem s = new SelectItem(null, "--- Not Availalbe ---");
        lst.add(s);
        ddl_contract_alternative_options = lst;
        ddl_contract_alternative = null;

    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
}
