package org.fes.pis.jsf.pages.tas;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.customJpaController;
import org.fes.pis.entities.FtasAbsentees;
import org.fes.pis.jsf.GlobalResources;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.jsf.selectableDataModels.SD_FtasAbsentees;
import org.fes.pis.sessions.FtasAbsenteesDtlFacadeLocal;
import org.fes.pis.sessions.FtasAbsenteesFacadeLocal;
import org.fes.pis.sessions.FtasLeavemstFacadeLocal;
import org.primefaces.context.RequestContext;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Approval_Leave_Application")
@ViewScoped
public class PIS_Approval_Leave_Application implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="EJB Declaration">
    @EJB
    private FtasAbsenteesFacadeLocal ftasAbsenteesFacade;
    @EJB
    private customJpaController customJpaController;
    @EJB
    private FtasLeavemstFacadeLocal ftasLeavemstFacade;
    System_Properties system_Properties = new System_Properties();
    @EJB
    private FtasAbsenteesDtlFacadeLocal ftasAbsenteesDtlFacade;
    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>      
    // <editor-fold defaultstate="collapsed" desc="tbl_approval_list"> 
    public SD_FtasAbsentees tbl_approval_list;
    List<FtasAbsentees> tbl_approval_list_filtered;

    public List<FtasAbsentees> getTbl_approval_list_filtered() {
        return tbl_approval_list_filtered;
    }

    public void setTbl_approval_list_filtered(List<FtasAbsentees> tbl_approval_list_filtered) {
        this.tbl_approval_list_filtered = tbl_approval_list_filtered;
    }

    public SD_FtasAbsentees getTbl_approval_list() {
        return tbl_approval_list;
    }

    public void setTbl_approval_list(SD_FtasAbsentees tbl_approval_list) {
        this.tbl_approval_list = tbl_approval_list;
    }

    private void setTbl_approval_list() {
        tbl_approval_list = new SD_FtasAbsentees(ftasAbsenteesFacade.findAll_ApprovalList(globalData.getUser_id()));
        if (globalData.getUser_id() == 1) {
            if (!tbl_approval_list.isRowAvailable() && redirectUrl != null) {
                RequestContext.getCurrentInstance().execute("window.location='" + redirectUrl + "'");
            }
        }
        FtasAbsentees ent;
//        ent.getFtasAbsenteesDtlCollection().iterator().next().getEmplbSrgKey().getCurrentBal()
    }
    // </editor-fold>        
    //<editor-fold defaultstate="collapsed" desc="selectedApplications"> 
    FtasAbsentees[] selectedApplications;

    public FtasAbsentees[] getSelectedApplications() {
        return selectedApplications;
    }

    public void setSelectedApplications(FtasAbsentees[] selectedApplications) {
        this.selectedApplications = selectedApplications;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="View Detail Methods">
    //<editor-fold defaultstate="collapsed" desc="viewLeaveDetail">

    public void viewLeaveDetail(FtasAbsentees selectedLeave) {
        this.selectedLeave = selectedLeave;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="selectedLeave">
    FtasAbsentees selectedLeave;

    public FtasAbsentees getSelectedLeave() {
        return selectedLeave;
    }

    public void setSelectedLeave(FtasAbsentees selectedLeave) {
        this.selectedLeave = selectedLeave;
    }
    //</editor-fold>

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Approve Reject Methods">
    //<editor-fold defaultstate="collapsed" desc="btn_approve_clicked">
    public void btn_approve_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (selectedApplications.length > 0) {

            try {
                boolean isTransactionDone = customJpaController.ApproveRejectAppls(selectedApplications, "Y", globalData.getUser_id());
                if (isTransactionDone) {
                    try {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Application Approved", "Selected Application Approved Successfully."));
                        setTbl_approval_list();
                        selectedLeave = null;
                    } catch (Exception e) {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed reset data", "Selected Application Approved Successfully but failed reset data. Please reload the page"));
                        LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_accept_clicked", null, e);
                    }
                } else {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during Accepting Application."));
                }
            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during Accepting Application."));
                LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_accept_clicked", null, e);
            }
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Select Application", "Please select any Application to Approve/Reject."));
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_reject_clicked">

    public void btn_reject_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (selectedApplications.length > 0) {
            try {
                boolean isTransactionDone = customJpaController.ApproveRejectAppls(selectedApplications, "N", globalData.getUser_id());
                if (isTransactionDone) {
                    try {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Application Rejected", "Selected Application Rejected Successfully."));
                        setTbl_approval_list();
                        selectedLeave = null;
                    } catch (Exception e) {
                        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Failed reset data", "Selected Application Rejected Successfully but failed reset data. Please reload the page"));
                        LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_reject_clicked", null, e);
                    }
                } else {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during Rejecting Application."));
                }
            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occured during Rejecting Application."));
                LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_reject_clicked", null, e);
            }
        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Select Application", "Please select any Application to Approve/Reject."));
        }
    }
    //</editor-fold>
    //</editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="Default Constructor & Other Beans Method">
    // <editor-fold defaultstate="collapsed" desc="default constructor">

    public PIS_Approval_Leave_Application() {
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Resources">

    protected GlobalResources getGlobalResources() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalResources) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalResources");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Utilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="redirectUrl">
    String redirectUrl;

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        String refUrl = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().isEmpty() ? null : FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("ref_url");
        redirectUrl = refUrl == null ? null : getGlobalResources().getProject_root_url() + refUrl;
        setTbl_approval_list();
    }
    //</editor-fold>
}
