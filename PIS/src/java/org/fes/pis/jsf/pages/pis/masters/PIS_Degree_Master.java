package org.fes.pis.jsf.pages.pis.masters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.DegreeMaster_bean;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.customJpaController;
import org.fes.pis.entities.*;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdDegreeLevelMstFacadeLocal;
import org.fes.pis.sessions.FhrdDegreedtlFacadeLocal;
import org.fes.pis.sessions.FhrdDegreemstFacadeLocal;
import org.primefaces.context.RequestContext;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Degree_Master")
@ViewScoped
public class PIS_Degree_Master implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="EJB Declarations"> 
    @EJB
    private FhrdDegreeLevelMstFacadeLocal fhrdDegreeLevelMstFacade;
    @EJB
    private customJpaController customJpaController;
    @EJB
    private FhrdDegreedtlFacadeLocal fhrdDegreedtlFacade;
    @EJB
    private FhrdDegreemstFacadeLocal fhrdDegreemstFacade;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="declarations">
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    //</editor-fold> 
    // <editor-fold defaultstate="collapsed" desc="default methods">

    /**
     * Creates a new instance of MST_Degree_Master
     */
    public PIS_Degree_Master() {
    }// </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="binded attributes">
    String txt_degree_cd;
    String txt_degree_name;
    String txt_subject_name;

    //<editor-fold defaultstate="collapsed" desc="getter/setter">
    public String getTxt_degree_cd() {
        return txt_degree_cd;
    }

    public void setTxt_degree_cd(String txt_degree_cd) {
        this.txt_degree_cd = txt_degree_cd;
    }

    public String getTxt_degree_name() {
        return txt_degree_name;
    }

    public void setTxt_degree_name(String txt_degree_name) {
        this.txt_degree_name = txt_degree_name;
    }

    public String getTxt_subject_name() {
        return txt_subject_name;
    }

    public void setTxt_subject_name(String txt_subject_name) {
        this.txt_subject_name = txt_subject_name;
    }
    //</editor-fold>
    //</editor-fold>     
    //<editor-fold defaultstate="collapsed" desc="ddl_degree_level">
    String ddl_degree_level;
    List<SelectItem> ddl_degree_level_options;

    public String getDdl_degree_level() {
        return ddl_degree_level;
    }

    public void setDdl_degree_level(String ddl_degree_level) {
        this.ddl_degree_level = ddl_degree_level;
    }

    public List<SelectItem> getDdl_degree_level_options() {
        return ddl_degree_level_options;
    }

    public void setDdl_degree_level_options(List<SelectItem> ddl_degree_level_options) {
        this.ddl_degree_level_options = ddl_degree_level_options;
    }

    private void setDdl_degree_level() {
        List<FhrdDegreeLevelMst> lst_LevelMsts = fhrdDegreeLevelMstFacade.findAll(globalData.getWorking_ou(), true);
        ddl_degree_level_options = new ArrayList<SelectItem>();
        ddl_degree_level_options.add(new SelectItem(null, "--- Select ---"));
        for (FhrdDegreeLevelMst l : lst_LevelMsts) {
            ddl_degree_level_options.add(new SelectItem(l.getDlmSrgKey().toString(), l.getDlmDescription()));
        }
        ddl_degree_level = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_degree"> 
    List<FhrdDegreemst> tbl_degree;
    List<FhrdDegreemst> tbl_degree_filtered;

    public List<FhrdDegreemst> getTbl_degree() {
        return tbl_degree;
    }

    public void setTbl_degree(List<FhrdDegreemst> tbl_degree) {
        this.tbl_degree = tbl_degree;
    }

    public List<FhrdDegreemst> getTbl_degree_filtered() {
        return tbl_degree_filtered;
    }

    public void setTbl_degree_filtered(List<FhrdDegreemst> tbl_degree_filtered) {
        this.tbl_degree_filtered = tbl_degree_filtered;
    }

    public void setTbl_degree() {
        tbl_degree = fhrdDegreemstFacade.findAll(globalData.getWorking_ou());

    }
    //<editor-fold defaultstate="collapsed" desc="filter_degree_level">
    SelectItem[] filter_degree_level;

    public SelectItem[] getFilter_degree_level() {
        return filter_degree_level;
    }

    public void setFilter_degree_level(SelectItem[] filter_degree_level) {
        this.filter_degree_level = filter_degree_level;
    }
    //</editor-fold>

    private void setFilter_degree_level() {
        int n = ddl_degree_level_options.size();
        filter_degree_level = new SelectItem[n];
        filter_degree_level[0] = new SelectItem("", "All");
        for (int i = 1; i < n; i++) {
            filter_degree_level[i] = ddl_degree_level_options.get(i);
        }
    }
    //</editor-fold>   
    //<editor-fold defaultstate="collapsed" desc="entFhrdDegreeDtl_edit"> 
    FhrdDegreemst entFhrdDegreeDtl_edit;

    public FhrdDegreemst getEntFhrdDegreeDtl_edit() {
        return entFhrdDegreeDtl_edit;
    }

    public void setEntFhrdDegreeDtl_edit(FhrdDegreemst entFhrdDegreeDtl_edit) {
        this.entFhrdDegreeDtl_edit = entFhrdDegreeDtl_edit;
    }

    public void btn_add_subject_action(ActionEvent event) {
        RequestContext.getCurrentInstance().execute("pw_dlg_add_subject.show();");
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="add subject to list">
    //<editor-fold defaultstate="collapsed" desc="tbl_subject">
    List<DegreeMaster_bean> tbl_subject;

    public List<DegreeMaster_bean> getTbl_subject() {
        return tbl_subject;
    }

    public void setTbl_subject(List<DegreeMaster_bean> tbl_subject) {
        this.tbl_subject = tbl_subject;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ent_SubjectDetail">
    DegreeMaster_bean ent_SubjectDetail = new DegreeMaster_bean();

    public DegreeMaster_bean getEnt_SubjectDetail() {
        return ent_SubjectDetail;
    }

    public void setEnt_SubjectDetail(DegreeMaster_bean ent_SubjectDetail) {
        int v = ent_SubjectDetail.getRowid();
        List<DegreeMaster_bean> lst_List = tbl_subject;
        lst_List.remove(v);
        this.ent_SubjectDetail = ent_SubjectDetail;
        int n = lst_List.size();
        for (int i = 0; i < n; i++) {
            lst_List.get(i).setRowid(i);
        }
        tbl_subject = lst_List;
        this.ent_SubjectDetail = ent_SubjectDetail;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn events to add subject into list">

    public void btn_subject_delete_clicked(ActionEvent event) {
    }

    public void add_subject_clicked(ActionEvent event) {
        txt_subject_name = txt_subject_name.trim().toUpperCase();
        boolean subjectExists = false;
        if (tbl_subject == null) {
            tbl_subject = new ArrayList<DegreeMaster_bean>();
        }
        if (!tbl_subject.isEmpty()) {
            for (DegreeMaster_bean d : tbl_subject) {
                if (d.getSubject_name().equals(txt_subject_name)) {
                    subjectExists = true;
                }
            }
        }
        if (!subjectExists) {
            DegreeMaster_bean entDegreeMaster_bean = new DegreeMaster_bean();
            entDegreeMaster_bean.setSubject_name(txt_subject_name);
            entDegreeMaster_bean.setRowid(tbl_subject.size());
            tbl_subject.add(entDegreeMaster_bean);
            txt_subject_name = null;
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "Subject Name is already entered under the new degree"));
        }
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_add_degree_clicked">

    public void btn_add_degree_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (isDegreeValid()) {
            BigDecimal v_dgrm_srgkey = getGlobalSettings().getUniqueSrno(null, "FHRD_DEGREEMST");
            FhrdEmpmst ent_FhrdEmpmst = new FhrdEmpmst(globalData.getUser_id());
            SystOrgUnitMst ent_SystOrgUnitMst = new SystOrgUnitMst(globalData.getWorking_ou());

            FhrdDegreemst ent_FhrdDegreemst = new FhrdDegreemst(v_dgrm_srgkey);
            ent_FhrdDegreemst.setDegreeSrno(-1);
            ent_FhrdDegreemst.setDegreeName(txt_degree_name.toUpperCase().trim());
            ent_FhrdDegreemst.setDegreecd(txt_degree_cd.toUpperCase().trim());
            ent_FhrdDegreemst.setDlmSrgKey(new FhrdDegreeLevelMst(new Integer(ddl_degree_level)));
            ent_FhrdDegreemst.setCreateby(ent_FhrdEmpmst);
            ent_FhrdDegreemst.setCreatedt(new Date());
            ent_FhrdDegreemst.setOumUnitSrno(ent_SystOrgUnitMst);

            List<FhrdDegreedtl> lst_FhrdDegreedtls = new ArrayList<FhrdDegreedtl>();

            if (tbl_subject != null) {
                List<DegreeMaster_bean> lst_DegreeMaster_beans = tbl_subject;
                Integer n = lst_DegreeMaster_beans.size();
                for (int i = 0; i < n; i++) {
                    FhrdDegreedtl ent_FhrdDegreedtl = new FhrdDegreedtl(new FhrdDegreedtlPK(new BigDecimal("" + i), v_dgrm_srgkey));
                    ent_FhrdDegreedtl.setFhrdDegreemst(ent_FhrdDegreemst);
                    ent_FhrdDegreedtl.setCreateby(ent_FhrdEmpmst);
                    ent_FhrdDegreedtl.setCreatedt(new Date());
                    ent_FhrdDegreedtl.setOumUnitSrno(ent_SystOrgUnitMst);
                    ent_FhrdDegreedtl.setSubjectName(lst_DegreeMaster_beans.get(i).getSubject_name().toUpperCase());
                    lst_FhrdDegreedtls.add(ent_FhrdDegreedtl);
                }
            }

            try {
                boolean isTransactionComplete = customJpaController.createFhrdDegreeMst(ent_FhrdDegreemst, lst_FhrdDegreedtls);
                if (isTransactionComplete) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record saved Successfully."));
                    txt_degree_cd = null;
                    ddl_degree_level = null;
                    txt_degree_name = null;
                    txt_subject_name = null;
                    tbl_subject = null;
                    setTbl_degree();
                } else {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occerred during saving records"));
                }
            } catch (Exception ex) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occerred during saving records"));
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_add_degree_clicked", null, ex);
            }
        }
    }

    private boolean isDegreeValid() {
        if (!fhrdDegreemstFacade.isDegreeValid(globalData.getWorking_ou(), txt_degree_cd.trim().toUpperCase(), txt_degree_name.trim().toUpperCase())) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "Degree Code/Degree Name already exists in database."));
            return false;
        }
        return true;
    }
    //</editor-fold>
    // add dlg new subject
    //<editor-fold defaultstate="collapsed" desc="dlg add subject">
    String txt_dlg_subject_name;

    public String getTxt_dlg_subject_name() {
        return txt_dlg_subject_name;
    }

    public void setTxt_dlg_subject_name(String txt_dlg_subject_name) {
        this.txt_dlg_subject_name = txt_dlg_subject_name;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_dlg_add_subject_clicked"> 

    public void btn_dlg_add_subject_clicked(ActionEvent event) {
        txt_dlg_subject_name = txt_dlg_subject_name.toUpperCase().trim();
        if (isSubjectValid()) {
            FacesContext context = FacesContext.getCurrentInstance();
            FhrdDegreedtl ent_FhrdDegreedtl = new FhrdDegreedtl(new BigDecimal("" + -1), entFhrdDegreeDtl_edit.getDgrmSrgKey());
            ent_FhrdDegreedtl.setCreateby(globalData.getEnt_login_user());
            ent_FhrdDegreedtl.setCreatedt(new Date());
            ent_FhrdDegreedtl.setOumUnitSrno(globalData.getEnt_working_ou());
//            ent_FhrdDegreedtl.setSubjectCode(txt_dlg_subject_cd);
            ent_FhrdDegreedtl.setSubjectName(txt_dlg_subject_name);

            try {
                fhrdDegreedtlFacade.create(ent_FhrdDegreedtl);
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record saved Successfully."));
                txt_dlg_subject_name = null;
                setTbl_degree();
            } catch (Exception ex) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occerred during saving records"));
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_dlg_add_subject_clicked", null, ex);
            }
        }
    }

    private boolean isSubjectValid() {
        if (!fhrdDegreedtlFacade.isSubjectValid(globalData.getWorking_ou(), entFhrdDegreeDtl_edit.getDgrmSrgKey().toString(), null, txt_dlg_subject_name)) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "Subject Code/Subject Name already exists under same degree."));
            return false;
        }
        return true;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setTbl_degree();
        setDdl_degree_level();
        setFilter_degree_level();
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
// <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
}
