/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.tas;

import java.util.List;
import javax.faces.model.ListDataModel;
import org.fes.pis.custom_entities.Daily_Missing_Attendance;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author anu
 */
public class PIS_Missing_Attendance_Checkbox extends ListDataModel<Daily_Missing_Attendance> implements SelectableDataModel<Daily_Missing_Attendance> {

    public PIS_Missing_Attendance_Checkbox() {
    }
    
    
    public PIS_Missing_Attendance_Checkbox(List<Daily_Missing_Attendance> data) {  
        super(data);  
    }  
      
    @Override  
    public Daily_Missing_Attendance getRowData(String rowKey) {  
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  
          
        List<Daily_Missing_Attendance> attendances = (List<Daily_Missing_Attendance>) getWrappedData();  
          
        for(Daily_Missing_Attendance attendance : attendances) {  
            if(attendance.getId().toString().equals(rowKey)) {
                return attendance;
            }  
        }  
          
        return null;  
    }  
  
    @Override  
    public Object getRowKey(Daily_Missing_Attendance attendances) {  
        return attendances.getId();  
    }  
}
