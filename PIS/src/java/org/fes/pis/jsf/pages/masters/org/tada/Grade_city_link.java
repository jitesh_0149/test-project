package org.fes.pis.jsf.pages.masters.org.tada;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.FtasTadaGradeCityLink;
import org.fes.pis.entities.FtasTadaGradeMst;
import org.fes.pis.entities.SystOrgUnitMst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.jsf.utilities.JSFMessages;
import org.fes.pis.sessions.FtasTadaGradeCityLinkFacade;
import org.fes.pis.sessions.FtasTadaGradeMstFacade;
import org.primefaces.context.RequestContext;

/**
 *
 * @author anu
 */
@ManagedBean
@ViewScoped
public class Grade_city_link implements Serializable {

    @EJB
    private FtasTadaGradeMstFacade ftasTadaGradeMstFacade;
    @EJB
    private FtasTadaGradeCityLinkFacade ftasTadaGradeCityLinkFacade;
    
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="saving methods">
    //<editor-fold defaultstate="collapsed" desc="ddl_grade">
    String ddl_grade;
    List<SelectItem> ddl_grade_options;

    public String getDdl_grade() {
        return ddl_grade;
    }

    public void setDdl_grade(String ddl_grade) {
        this.ddl_grade = ddl_grade;
    }

    public List<SelectItem> getDdl_grade_options() {
        return ddl_grade_options;
    }

    public void setDdl_grade_options(List<SelectItem> ddl_grade_options) {
        this.ddl_grade_options = ddl_grade_options;
    }

    private void setDdl_grade() {
        ddl_grade_options = new ArrayList<SelectItem>();
        List<FtasTadaGradeMst> lst_FtasTadaGradeMsts = ftasTadaGradeMstFacade.findAll(globalData.getWorking_ou(), true);
        ddl_grade_options.add(new SelectItem(null, "--- Select ---"));
        for (FtasTadaGradeMst g : lst_FtasTadaGradeMsts) {
            ddl_grade_options.add(new SelectItem(g.getGmSrgKey().toString(), g.getGmDesc()));
        }
        ddl_grade = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_city">
    String txt_city;

    public String getTxt_city() {
        return txt_city;
    }

    public void setTxt_city(String txt_city) {
        this.txt_city = txt_city;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_start_date">
    Date cal_start_date;

    public Date getCal_start_date() {
        return cal_start_date;
    }

    public void setCal_start_date(Date cal_start_date) {
        this.cal_start_date = cal_start_date;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_end_date">
    Date cal_end_date;

    public Date getCal_end_date() {
        return cal_end_date;
    }

    public void setCal_end_date(Date cal_end_date) {
        this.cal_end_date = cal_end_date;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetEntryComponent()">

    private void resetEntryComponent() {
        txt_city = null;
        cal_start_date = null;
        cal_end_date = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_save">

    public void btn_save(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            FtasTadaGradeCityLink ent_FtasTadaGradeCityLink = new FtasTadaGradeCityLink();
            ent_FtasTadaGradeCityLink.setGclSrgKey(new Long("-1"));
            ent_FtasTadaGradeCityLink.setGmSrgKey(new FtasTadaGradeMst(Long.valueOf(ddl_grade)));
            ent_FtasTadaGradeCityLink.setCity(txt_city.toUpperCase().trim());
            ent_FtasTadaGradeCityLink.setStartDate(cal_start_date);
            ent_FtasTadaGradeCityLink.setEndDate(cal_end_date);
            ent_FtasTadaGradeCityLink.setCreateby(new FhrdEmpmst(globalData.getUser_id()));
            ent_FtasTadaGradeCityLink.setCreatedt(new Date());
            ent_FtasTadaGradeCityLink.setOumUnitSrno(new SystOrgUnitMst(globalData.getWorking_ou()));
            ftasTadaGradeCityLinkFacade.create(ent_FtasTadaGradeCityLink);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Grade City Link Created", "Grade City Link created successfully"));
            setTbl_grade_city_link();
            resetEntryComponent();
        } catch (Exception e) {
            context.addMessage(null, JSFMessages.getErrorMessage(e));
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_accept_clicked", null, e);
        }
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="main table methods">
    //<editor-fold defaultstate="collapsed" desc="tbl_grade_city_link">
    List<FtasTadaGradeCityLink> tbl_grade_city_link;
    List<FtasTadaGradeCityLink> tbl_grade_city_link_filter;

    public List<FtasTadaGradeCityLink> getTbl_grade_city_link() {
        return tbl_grade_city_link;
    }

    public void setTbl_grade_city_link(List<FtasTadaGradeCityLink> tbl_grade_city_link) {
        this.tbl_grade_city_link = tbl_grade_city_link;
    }

    public List<FtasTadaGradeCityLink> getTbl_grade_city_link_filter() {
        return tbl_grade_city_link_filter;
    }

    public void setTbl_grade_city_link_filter(List<FtasTadaGradeCityLink> tbl_grade_city_link_filter) {
        this.tbl_grade_city_link_filter = tbl_grade_city_link_filter;
    }

    private void setTbl_grade_city_link() {
        tbl_grade_city_link = ftasTadaGradeCityLinkFacade.findAll(globalData.getWorking_ou());
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Apply End Date Methods">
    //<editor-fold defaultstate="collapsed" desc="selectedGradeCityLink">
    FtasTadaGradeCityLink selectedGradeCityLink;

    public FtasTadaGradeCityLink getSelectedGradeCityLink() {
        return selectedGradeCityLink;
    }

    public void setSelectedGradeCityLink(FtasTadaGradeCityLink selectedGradeCityLink) {
        this.selectedGradeCityLink = selectedGradeCityLink;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="applyEndDate">
    public void applyEndDate(FtasTadaGradeCityLink ftasTadaGradeCityLink) {
        selectedGradeCityLink = ftasTadaGradeCityLink;
        cal_apply_end_date = null;
        cal_apply_end_date_min = DateTimeUtility.ChangeDateFormat(ftasTadaGradeCityLink.getStartDate(), null);
        RequestContext.getCurrentInstance().execute("pw_dlg_apply_end_date.show()");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_apply_end_date">
    Date cal_apply_end_date;
    String cal_apply_end_date_min;

    public Date getCal_apply_end_date() {
        return cal_apply_end_date;
    }

    public void setCal_apply_end_date(Date cal_apply_end_date) {
        this.cal_apply_end_date = cal_apply_end_date;
    }

    public String getCal_apply_end_date_min() {
        return cal_apply_end_date_min;
    }

    public void setCal_apply_end_date_min(String cal_apply_end_date_min) {
        this.cal_apply_end_date_min = cal_apply_end_date_min;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_apply_end_date">

    public void btn_apply_end_date(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            selectedGradeCityLink = ftasTadaGradeCityLinkFacade.find(selectedGradeCityLink.getGclSrgKey());
            selectedGradeCityLink.setEndDate(cal_apply_end_date);
            selectedGradeCityLink.setUpdateby(new FhrdEmpmst(globalData.getUser_id()));
            ftasTadaGradeCityLinkFacade.edit(selectedGradeCityLink);
            setTbl_grade_city_link();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "End Date Applied", "End Date applied successfully"));
            RequestContext.getCurrentInstance().execute("pw_dlg_apply_end_date.hide()");
        } catch (Exception e) {
            context.addMessage(null, JSFMessages.getErrorMessage(e));
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_accept_clicked", null, e);
        }
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setDdl_grade();
        setTbl_grade_city_link();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Default Constructor & Other Beans Methods">
    // <editor-fold defaultstate="collapsed" desc="Default Constructor">

    public Grade_city_link() {
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
    //</editor-fold>
}
