/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.pis.emp_info;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.FhrdDesigmst;
import org.fes.pis.entities.FhrdEmpeventPromotion;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.FhrdEventMst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdDesigmstFacadeLocal;
import org.fes.pis.sessions.FhrdEmpeventPromotionFacadeLocal;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Emp_Increment")
@ViewScoped
public class PIS_Emp_Increment implements Serializable{
    
    @EJB
    private FhrdEmpeventPromotionFacadeLocal fhrdEmpeventPromotionFacade;
    @EJB
    private FhrdDesigmstFacadeLocal fhrdDesigmstFacade;

    //<editor-fold defaultstate="collapsed" desc="default method"> 
    /**
     * Creates a new instance of PIS_Emp_Increment
     */
    public PIS_Emp_Increment() {
    }
    //</editor-fold>
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;
    
    public PIS_GlobalData getGlobalData() {
        return globalData;
    }
    
    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="EJB Declarations">
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_selection"> 
    String ddl_selection;
    
    public String getDdl_selection() {
        return ddl_selection;
    }
    
    public void setDdl_selection(String ddl_selection) {
        this.ddl_selection = ddl_selection;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="getter/setter">
    String txt_user_id;
    String lbl_username;
    String lbl_empnm_err;
    boolean hdn_valid_userid;
    Date cal_event_date;
    String txt_remarks;
    boolean chk_increment_flag;
    
    public boolean isHdn_valid_userid() {
        return hdn_valid_userid;
    }
    
    public void setHdn_valid_userid(boolean hdn_valid_userid) {
        this.hdn_valid_userid = hdn_valid_userid;
    }
    
    public String getLbl_empnm_err() {
        return lbl_empnm_err;
    }
    
    public void setLbl_empnm_err(String lbl_empnm_err) {
        this.lbl_empnm_err = lbl_empnm_err;
    }
    
    public String getLbl_username() {
        return lbl_username;
    }
    
    public void setLbl_username(String lbl_username) {
        this.lbl_username = lbl_username;
    }
    
    public String getTxt_user_id() {
        return txt_user_id;
    }
    
    public void setTxt_user_id(String txt_user_id) {
        this.txt_user_id = txt_user_id;
    }
    
    public boolean isChk_increment_flag() {
        return chk_increment_flag;
    }
    
    public void setChk_increment_flag(boolean chk_increment_flag) {
        this.chk_increment_flag = chk_increment_flag;
    }
    
    public Date getCal_event_date() {
        return cal_event_date;
    }
    
    public void setCal_event_date(Date cal_event_date) {
        this.cal_event_date = cal_event_date;
    }
    
    public String getTxt_remarks() {
        return txt_remarks;
    }
    
    public void setTxt_remarks(String txt_remarks) {
        this.txt_remarks = txt_remarks;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="txt_userid_processvaluechange event">

    public void txt_user_id_processValueChange(ValueChangeEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        txt_user_id = event.getNewValue().toString();
        lbl_username = "";
        lbl_empnm_err = "";
        hdn_valid_userid = false;
        if (txt_user_id.equals("") == true) {
            hdn_valid_userid = false;
        } else {
            FhrdEmpmst entEmpmst1 = getEmployeeName(Integer.valueOf(event.getNewValue().toString()));
            if (entEmpmst1 != null) {
                lbl_username = entEmpmst1.getUserName();
                hdn_valid_userid = true;
                setDdl_from_desig_options(entEmpmst1);
            } else {
                hdn_valid_userid = false;
                lbl_username = "";
                lbl_empnm_err = "User Does Not Exist";
            }
        }
        
    }// </editor-fold>    
    // <editor-fold defaultstate="collapsed" desc="getEmployeeName">

    public FhrdEmpmst getEmployeeName(Integer p_userid) {
        FhrdEmpmst entEmpmst = fhrdEmpmstFacade.find(p_userid);
        return entEmpmst;
    }// </editor-fold>       
    //<editor-fold defaultstate="collapsed" desc="ddl_from_desig">
    String ddl_from_desig;
    List<SelectItem> ddl_from_desig_options;
    
    public String getDdl_from_desig() {
        return ddl_from_desig;
    }
    
    public void setDdl_from_desig(String ddl_from_desig) {
        this.ddl_from_desig = ddl_from_desig;
    }
    
    public List<SelectItem> getDdl_from_desig_options() {
        return ddl_from_desig_options;
    }
    
    public void setDdl_from_desig_options(List<SelectItem> ddl_from_desig_options) {
        this.ddl_from_desig_options = ddl_from_desig_options;
    }
    
    public void setDdl_from_desig_options(FhrdEmpmst entEmpmst) {
        ddl_from_desig_options = new ArrayList<SelectItem>();
        ddl_from_desig_options.add(new SelectItem(entEmpmst.getDsgmSrgKey().getDsgmSrgKey().toString(), entEmpmst.getDsgmSrgKey().getDesigDesc()));
        ddl_from_desig = entEmpmst.getDsgmSrgKey().getDsgmSrgKey().toString();
        
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_to_desig"> 
    String ddl_to_desig;
    List<SelectItem> ddl_to_desig_options;
    
    public String getDdl_to_desig() {
        return ddl_to_desig;
    }
    
    public void setDdl_to_desig(String ddl_to_desig) {
        this.ddl_to_desig = ddl_to_desig;
    }
    
    public List<SelectItem> getDdl_to_desig_options() {
        return ddl_to_desig_options;
    }
    
    public void setDdl_to_desig_options(List<SelectItem> ddl_to_desig_options) {
        this.ddl_to_desig_options = ddl_to_desig_options;
    }
    
    public void setDdl_to_desig_options() {
        List<SelectItem> lst_select_cont = new ArrayList<SelectItem>();
        if (ddl_to_desig_options == null) {
            List<FhrdDesigmst> lst_Desigmsts = fhrdDesigmstFacade.findAll();
            if (lst_Desigmsts.isEmpty()) {
                lst_select_cont.add(new SelectItem("None"));
            } else {
                lst_select_cont.add(new SelectItem(null, "---Select One---"));
                Integer n = lst_Desigmsts.size();
                for (int i = 0; i < n; i++) {
                    lst_select_cont.add(new SelectItem(lst_Desigmsts.get(i).getDsgmSrgKey(), lst_Desigmsts.get(i).getDesigDesc()));
                }
            }
        }
        ddl_to_desig_options = lst_select_cont;
        ddl_to_desig = null;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_set_status_clicked"> 
    public void btn_set_promotion_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        FhrdEmpeventPromotion entEmpeventPromotion = new FhrdEmpeventPromotion();
        entEmpeventPromotion.setCreateby(globalData.getEnt_login_user());
        entEmpeventPromotion.setCreatedt(new Date());
        entEmpeventPromotion.setEmSrgKey(new FhrdEventMst(new BigDecimal("5")));
        entEmpeventPromotion.setEmpepSrgKey(getGlobalSettings().getUniqueSrno((cal_event_date.getYear() + 1900), "FHRD_EMPEVENT_PROMOTION"));
        entEmpeventPromotion.setEventDate(cal_event_date);
        entEmpeventPromotion.setOldDsgmSrgKey(new BigDecimal(ddl_from_desig));
        entEmpeventPromotion.setNewDsgmSrgKey(new BigDecimal(ddl_to_desig));
        entEmpeventPromotion.setSalaryIncrementFlag(chk_increment_flag ? "Y" : "N");
        entEmpeventPromotion.setRemarks(txt_remarks.toUpperCase());
        entEmpeventPromotion.setUserId(new FhrdEmpmst(Integer.valueOf(txt_user_id)));
        entEmpeventPromotion.setOumUnitSrno(globalData.getEnt_working_ou());
        entEmpeventPromotion.setRefOumUnitSrno(globalData.getWorking_ou());
        
        try {
            fhrdEmpeventPromotionFacade.create(entEmpeventPromotion);
            txt_remarks = null;
            txt_user_id = null;
            lbl_empnm_err = null;
            lbl_username = null;
            ddl_selection = null;
            cal_event_date = null;
            ddl_from_desig_options = new ArrayList<SelectItem>();
            ddl_from_desig_options.add(new SelectItem(null, "--No Data Found--"));
            ddl_from_desig = null;
            setDdl_to_desig_options();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Employee Event Detail saved Successfully"));
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred."));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_set_promotion_clicked", null, e);
        }
        
    }
    //</editor-fold>    

    //<editor-fold defaultstate="collapsed" desc="init()"> 
    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        ddl_from_desig_options = new ArrayList<SelectItem>();
        ddl_from_desig_options.add(new SelectItem(null, "--No Data Found--"));
        ddl_from_desig = null;
        setDdl_to_desig_options();
        
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings and default methods">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold> 
}
