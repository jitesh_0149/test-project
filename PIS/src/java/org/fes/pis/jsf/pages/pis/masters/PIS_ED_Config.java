/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.pis.masters;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.FhrdEarndednConfig;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEarndednConfigFacadeLocal;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_ED_Config")
@ViewScoped
public class PIS_ED_Config implements Serializable {

    @EJB
    private FhrdEarndednConfigFacadeLocal fhrdEarndednConfigFacade;
    System_Properties system_Properties = new System_Properties();
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>    	
    //<editor-fold defaultstate="collapsed" desc="Main Table">
    //<editor-fold defaultstate="collapsed" desc="tbl_ed_config">
    List<FhrdEarndednConfig> tbl_ed_config;
    List<FhrdEarndednConfig> tbl_ed_config_filtered;

    public List<FhrdEarndednConfig> getTbl_ed_config() {
        return tbl_ed_config;
    }

    public void setTbl_ed_config(List<FhrdEarndednConfig> tbl_ed_config) {
        this.tbl_ed_config = tbl_ed_config;
    }

    public void setTbl_ed_config() {
        tbl_ed_config = fhrdEarndednConfigFacade.findAll();
    }

    public List<FhrdEarndednConfig> getTbl_ed_config_filtered() {
        return tbl_ed_config_filtered;
    }

    public void setTbl_ed_config_filtered(List<FhrdEarndednConfig> tbl_ed_config_filtered) {
        this.tbl_ed_config_filtered = tbl_ed_config_filtered;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="filter_ed_flag">
    List<SelectItem> filter_ed_flag;

    public List<SelectItem> getFilter_ed_flag() {
        return filter_ed_flag;
    }

    public void setFilter_ed_flag(List<SelectItem> filter_ed_flag) {
        this.filter_ed_flag = filter_ed_flag;
    }

    private void setFilter_ed_flag() {
        filter_ed_flag = new ArrayList<SelectItem>();
        filter_ed_flag.add(new SelectItem("", "--- All ---"));
        filter_ed_flag.add(new SelectItem("E", "Earning"));
        filter_ed_flag.add(new SelectItem("D", "Deduction"));
        filter_ed_flag.add(new SelectItem("B", "Employer's Contribution"));
        filter_ed_flag.add(new SelectItem("R", "Reimbusrement"));
        filter_ed_flag.add(new SelectItem("C", "Calculation"));

    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setFilter_ed_flag();
        setTbl_ed_config();







    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="Default Constructor & GlobalSettings">

    //<editor-fold defaultstate="collapsed" desc="Default Constructor">
    public PIS_ED_Config() {
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    //</editor-fold>
}
