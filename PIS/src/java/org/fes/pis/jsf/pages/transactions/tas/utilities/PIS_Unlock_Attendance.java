package org.fes.pis.jsf.pages.transactions.tas.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.Database_Output;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.db.fes_project.pack.Ftas_Attdcalc;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;
import org.fes.pis.sessions.FhrdPaytransleaveMstFacadeLocal;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "PIS_Unlock_Attendance")
@ViewScoped
public class PIS_Unlock_Attendance implements Serializable {

    @EJB
    private FhrdPaytransleaveMstFacadeLocal fhrdPaytransleaveMstFacade;
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;

    
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    Ou_Pack ou_Pack = new Ou_Pack();
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>  
    //<editor-fold defaultstate="collapsed" desc="rdb_user_status">
    String rdb_user_status;

    public String getRdb_user_status() {
        return rdb_user_status;
    }

    public void setRdb_user_status(String rdb_user_status) {
        this.rdb_user_status = rdb_user_status;
    }

    private void setRdb_user_status() {
        rdb_user_status = "C";
        setDdl_user();
    }

    public void rdb_user_status_changed(ValueChangeEvent event) {
        rdb_user_status = event.getNewValue().toString();
        setDdl_user();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_user">
    FhrdEmpmst ddl_user;
    List<FhrdEmpmst> ddl_user_options;

    public FhrdEmpmst getDdl_user() {
        return ddl_user;
    }

    public void setDdl_user(FhrdEmpmst ddl_user) {
        this.ddl_user = ddl_user;
    }

    public List<FhrdEmpmst> getDdl_user_options() {
        return ddl_user_options;
    }

    public void setDdl_user_options(List<FhrdEmpmst> ddl_user_options) {
        this.ddl_user_options = ddl_user_options;
    }

    public void ddl_user_changed(ValueChangeEvent event) {
        ddl_user = (FhrdEmpmst) event.getNewValue();
        setOut_unlock_period();
    }

    private void setDdl_user() {
        ddl_user_options = fhrdEmpmstFacade.findAllFromAttendanceCalc(globalData.getWorking_ou(), rdb_user_status);
        ddl_user = null;
        setOut_unlock_period();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="out_unlock_period">
    String out_unlock_period;

    public String getOut_unlock_period() {
        return out_unlock_period;
    }

    public void setOut_unlock_period(String out_unlock_period) {
        this.out_unlock_period = out_unlock_period;
    }

    private void setOut_unlock_period() {
        if (ddl_user != null) {
            out_unlock_period = fhrdPaytransleaveMstFacade.findLastAttendancePeriod(ddl_user.getUserId());
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Confirmation Settings">
    //<editor-fold defaultstate="collapsed" desc="out_confirmation_message">
    String out_confirmation_message;

    public String getOut_confirmation_message() {
        return out_confirmation_message;
    }

    public void setOut_confirmation_message(String out_confirmation_message) {
        this.out_confirmation_message = out_confirmation_message;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="showConfirmDialog">
    public void showConfirmDialog(ActionEvent event) {
        out_confirmation_message = "Are you sure want to unlock attendance of User : " + ddl_user.getUserName() + " for Period : " + out_unlock_period + "?";
        RequestContext.getCurrentInstance().execute("pw_dlg_confirm_change.show()");
    }
    //</editor-fold>
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="btn save action event">

    public void btn_save_action(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        Database_Output database_Output = ou_Pack.check_privileges(this.getClass().getName(), "btn_save_action", "FHRD", globalData.getUser_id());
        String v_check_privileges = database_Output.getString1();
        if (v_check_privileges != null) {
            Ftas_Attdcalc ftas_Attdcalc = new Ftas_Attdcalc();
            database_Output = ftas_Attdcalc.REVERT_ATTENDANCE_CALCULATION(this.getClass().getName(),
                    "btn_save_action",
                    ddl_user.getUserId(),
                    DateTimeUtility.stringToDate(out_unlock_period.split(" TO ")[0], null),
                    DateTimeUtility.stringToDate(out_unlock_period.split(" TO ")[1], null),
                    globalData.getUser_id());
            if (database_Output.isExecuted_successfully() == true) {
                if (database_Output.getString1() == null) {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record Saved Successfully"));
                    setDdl_user();
                } else {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning", database_Output.getString1()));
                }
            } else {
                context.addMessage(null, database_Output.getFacesMessage());
            }

        } else {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Invalid User", "You Do Not Have Previlege to Perform This Task..."));
        }
    }// </editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="runGeneralReport()">

    public void runGeneralReport() {
        ServletOutputStream servletOutputStream = null;
        String hdn_report_loc_type = "C";
        String hdn_report_type = "P";
        String hdn_report_sub_type = null;
        String hdn_report_name = "leave_balance_report";
        String hdn_report_exp_name = "leave_balance_report";
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            servletOutputStream = response.getOutputStream();
            Map hm = new HashMap();

            FhrdEmpmst selectedUser = fhrdEmpmstFacade.find(ddl_user.getUserId());
            String[] v_dates = out_unlock_period.split(" TO ");
            hm.put("p_ou", selectedUser.getPostingOumUnitSrno().getOumUnitSrno());
            hm.put("p_ouname", selectedUser.getPostingOumUnitSrno().getOumName());
            hm.put("p_orgname", globalData.getOrganisationName());
            hm.put("p_from_date", DateTimeUtility.stringToDate(v_dates[0], null));
            hm.put("p_to_date", DateTimeUtility.stringToDate(v_dates[1], null));
            //hm.put("p_from_userid", txt_from_user_id);
            //hm.put("p_to_userid", txt_to_user_id);
            hm.put("p_native_level", "Y");
            hm.put("p_emp_status", rdb_user_status);
            hm.put("p_user_id", selectedUser.getUserId()); //pass null as default value
            hm.put("p_user_name", selectedUser.getUserName());



            //Print & Verify Paramter to debug
            boolean debugParameters = false;
            if (debugParameters) {
                System.out.println("p_ou : " + hm.get("p_ou"));
                System.out.println("p_ouname : " + hm.get("p_ouname"));
                System.out.println("p_from_date : " + hm.get("p_from_date"));
                System.out.println("p_to_date : " + hm.get("p_to_date"));
                System.out.println("p_from_user : " + hm.get("p_from_userid"));
                System.out.println("p_to_user : " + hm.get("p_to_userid"));
                System.out.println("p_native_level : " + hm.get("p_native_level"));
                System.out.println("p_user_id : " + hm.get("p_user_id"));
                System.out.println("" + hdn_report_name);
                System.out.println("" + hdn_report_exp_name);
            }

            if (hdn_report_type.equals("E")) {

                try {
                    hm.put("IS_IGNORE_PAGINATION", true);
                    Connection cn = getGlobalUtilities().getjdbcFES_PROJECT_Oracle().getConnection();
                    try {
                        InputStream reportStream = context.getExternalContext().getResourceAsStream("/reports/" + (hdn_report_loc_type.equals("C") ? "common/" : "excel/") + hdn_report_name.trim() + ".jasper");
                        JasperPrint jasperPrint = JasperFillManager.fillReport(reportStream, hm, cn);
                        response.setContentType("application/vnd.ms-excel");
                        SimpleDateFormat sdf = new SimpleDateFormat();
                        sdf.applyPattern("dd-MM-yyyy_HH-mm");
                        String v_date = (sdf.format(new Date())).toString();

                        if (hdn_report_sub_type == null) {
                            String filename = hdn_report_exp_name + "_" + v_date + ".xls";
                            response.setHeader("Content-Disposition", "inline;filename=" + filename);
                            JExcelApiExporter exporterXLS = new JExcelApiExporter();
                            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
                            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
                            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response.getOutputStream());
                            exporterXLS.exportReport();
                        } else if (hdn_report_sub_type.equals("EX")) {
                            String filename = hdn_report_exp_name + "_" + v_date + ".xlsx";
                            response.setHeader("Content-Disposition", "inline;filename=" + filename);
                            JRXlsxExporter exporterXLS = new JRXlsxExporter();
                            exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                            exporterXLS.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
                            exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
                            exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, response.getOutputStream());
                            exporterXLS.exportReport();
                        }
                        servletOutputStream.flush();
                        servletOutputStream.close();
                        context.responseComplete();
                    } catch (JRException e) {
                        LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "runGeneralReport() - reportName : " + hdn_report_name, null, e);
                    } catch (Exception e) {
                        LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "runGeneralReport() - reportName : " + hdn_report_name, null, e);
                    } finally {
                        if (!cn.isClosed()) {
                            cn.close();
                        }
                    }
                } catch (SQLException ex) {
                    LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "runGeneralReport() - reportName : " + hdn_report_name, null, ex);
                }
            } else {
                try {
                    hm.put("IS_IGNORE_PAGINATION", false);
                    Connection cn = getGlobalUtilities().getjdbcFES_PROJECT_Oracle().getConnection();
                    try {
                        InputStream reportStream = context.getExternalContext().getResourceAsStream("/reports/" + (hdn_report_loc_type.equals("C") ? "common/" : "pdf/") + hdn_report_name.trim() + ".jasper");
                        response.setContentType("application/pdf");
                        response.setHeader("Cache-Control", "public, max-age=1");
                        SimpleDateFormat sdf = new SimpleDateFormat();
                        sdf.applyPattern("dd-MM-yyyy_HH-mm");
                        String v_date = (sdf.format(new Date())).toString();
                        String filename = hdn_report_exp_name + "_" + v_date + ".pdf";
                        response.setHeader("Content-Disposition", "attachment;filename=" + filename);
                        JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, hm, cn);
                        cn.close();
                        servletOutputStream.flush();
                        servletOutputStream.close();
                        context.responseComplete();
                    } catch (JRException e) {
                        LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "runGeneralReport() - reportName : " + hdn_report_name, null, e);
                    } catch (Exception e) {
                        LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "runGeneralReport() - reportName : " + hdn_report_name, null, e);
                    } finally {
                        if (!cn.isClosed()) {
                            cn.close();
                        }
                    }
                } catch (SQLException ex) {
                    LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "runGeneralReport() - reportName : " + hdn_report_name, null, ex);
                }

            }
        } catch (IOException ex) {
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "runGeneralReport() - reportName : " + hdn_report_name, null, ex);
        } finally {
            try {
                servletOutputStream.close();
            } catch (IOException ex) {
                LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "runGeneralReport() - reportName : " + hdn_report_name, null, ex);
            }
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setRdb_user_status();
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings and default methods">

    // <editor-fold defaultstate="collapsed" desc="default menthods">
    /**
     * Creates a new instance of PIS_Attendance_Calculation
     */
    public PIS_Unlock_Attendance() {
    }// </editor-fold>   
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold> 
}
