/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.pages.pis.emp_info;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.customJpaController;
import org.fes.pis.entities.FhrdEmpContractMst;
import org.fes.pis.entities.FhrdEmpeventRelieve;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.FhrdEventMst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.FhrdEmpContractMstFacadeLocal;
import org.fes.pis.sessions.FhrdEmpeventRelieveFacadeLocal;
import org.fes.pis.sessions.FhrdEmpmstFacadeLocal;
import org.fes.pis.sessions.FhrdEventMstFacadeLocal;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author anu
 */
@ManagedBean(name = "PIS_Emp_Resign_Relieve")
@ViewScoped
public class PIS_Emp_Resign_Relieve implements Serializable {

    @EJB
    private customJpaController customJpaController;
    @EJB
    private FhrdEmpContractMstFacadeLocal fhrdEmpContractMstFacade;
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="EJB Declarations">
    @EJB
    private FhrdEventMstFacadeLocal fhrdEventMstFacade;
    @EJB
    private FhrdEmpeventRelieveFacadeLocal fhrdEmpeventRelieveFacade;
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="User Selectino Method">
    boolean userSelectionValid;
    String txt_user_id;
    FhrdEmpmst selectedUser;
    String err_txt_user_id;

    public String getErr_txt_user_id() {
        return err_txt_user_id;
    }

    public void setErr_txt_user_id(String err_txt_user_id) {
        this.err_txt_user_id = err_txt_user_id;
    }

    public FhrdEmpmst getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(FhrdEmpmst selectedUser) {
        this.selectedUser = selectedUser;
    }

    public String getTxt_user_id() {
        return txt_user_id;
    }

    public void setTxt_user_id(String txt_user_id) {
        this.txt_user_id = txt_user_id;
    }

    public boolean isUserSelectionValid() {
        return userSelectionValid;
    }

    public void setUserSelectionValid(boolean userSelectionValid) {
        this.userSelectionValid = userSelectionValid;
    }
    //<editor-fold defaultstate="collapsed" desc="txt_user_id_changed">

    public void txt_user_id_changed(ValueChangeEvent event) {
        txt_user_id = event.getNewValue().toString();
        userSelectionValid = false;
        err_txt_user_id = null;
        if (txt_user_id != null) {
            if (!txt_user_id.trim().equals("")) {
                selectedUser = fhrdEmpmstFacade.find(Integer.valueOf(txt_user_id));
                if (selectedUser != null) {
                    if (selectedUser.getOrgRelieveDate() != null || selectedUser.getTerminationDate() != null) {
                        err_txt_user_id = "Employee already relieved/terminated";
                        userSelectionValid = false;
                    } else {
                        userSelectionValid = true;
                    }
                } else {
                    selectedUser = null;
                    err_txt_user_id = "Employee does not exist";
                }
            } else {
                selectedUser = null;
            }
        } else {
            selectedUser = null;
        }
        setDdl_status();
        setCal_event_date();
    }
    //</editor-fold>    
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_status"> 
    String ddl_status;
    List<SelectItem> ddl_status_options;

    public List<SelectItem> getDdl_status_options() {
        return ddl_status_options;
    }

    public void setDdl_status_options(List<SelectItem> ddl_status_options) {
        this.ddl_status_options = ddl_status_options;
    }

    public String getDdl_status() {
        return ddl_status;
    }

    public void setDdl_status(String ddl_status) {
        this.ddl_status = ddl_status;
    }

    public void setDdl_status() {
        ddl_status_options = new ArrayList<SelectItem>();
        if (selectedUser.getTerminationDate() != null || (selectedUser.getOrgRelieveDate() != null && selectedUser.getOrgResignDate() != null)) {
            ddl_status_options.add(new SelectItem(null, "---N/A---"));
        } else if (selectedUser.getOrgRelieveDate() != null) {
            ddl_status_options.add(new SelectItem(null, "---Select Any---"));
            ddl_status_options.add(new SelectItem("R", "Resign"));
        } else if (selectedUser.getOrgResignDate() != null) {
            ddl_status_options.add(new SelectItem(null, "---Select Any---"));
            ddl_status_options.add(new SelectItem("L", "Relieve"));
        } else {
            ddl_status_options.add(new SelectItem(null, "---Select Any---"));
            ddl_status_options.add(new SelectItem("L", "Relieve"));
            ddl_status_options.add(new SelectItem("R", "Resign"));
            ddl_status_options.add(new SelectItem("T", "Termination"));
        }
        ddl_status = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_event_date">
    Date cal_event_date;
    String cal_event_date_min;
    String cal_event_date_max;

    public Date getCal_event_date() {
        return cal_event_date;
    }

    public void setCal_event_date(Date cal_event_date) {
        this.cal_event_date = cal_event_date;
    }

    public String getCal_event_date_max() {
        return cal_event_date_max;
    }

    public void setCal_event_date_max(String cal_event_date_max) {
        this.cal_event_date_max = cal_event_date_max;
    }

    public String getCal_event_date_min() {
        return cal_event_date_min;
    }

    public void setCal_event_date_min(String cal_event_date_min) {
        this.cal_event_date_min = cal_event_date_min;
    }

    private void setCal_event_date() {
        List<FhrdEmpContractMst> lst_ContractMsts = fhrdEmpContractMstFacade.findLatestContractEmpwise(selectedUser.getUserId(), "M");
        cal_event_date_max = DateTimeUtility.ChangeDateFormat(lst_ContractMsts.isEmpty() ? null : lst_ContractMsts.get(0).getEndDate(), null);
//        if (selectedUser.getOrgRelieveDate() != null || selectedUser.getTerminationDate() != null) {
//            cal_event_date_min = DateTimeUtility.ChangeDateFormat(selectedUser.getOrgResignDate(), null);
//        } else if (selectedUser.getOrgResignDate() != null) {
////            cal_event_date_min = DateTimeUtility.ChangeDateFormat(selectedUser.getOrgResignDate(), null);
//        } else {
        cal_event_date_min = DateTimeUtility.ChangeDateFormat(lst_ContractMsts.isEmpty() ? selectedUser.getOrgJoinDate() : lst_ContractMsts.get(0).getStartDate(), null);
//        }
    }

    public void cal_event_date_changed(SelectEvent event) {
        cal_event_date = (Date) event.getObject();
        setLastContract();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="lastContract">
    FhrdEmpContractMst lastContract;

    public FhrdEmpContractMst getLastContract() {
        return lastContract;
    }

    public void setLastContract(FhrdEmpContractMst lastContract) {
        this.lastContract = lastContract;
    }

    private void setLastContract() {
        if (selectedUser != null && cal_event_date != null) {
            lastContract = fhrdEmpContractMstFacade.findForRelTermNoticePeriod(selectedUser.getUserId(), cal_event_date);
        } else {
            lastContract = null;
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_remarks">
    String txt_remarks;

    public String getTxt_remarks() {
        return txt_remarks;
    }

    public void setTxt_remarks(String txt_remarks) {
        this.txt_remarks = txt_remarks;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="btn_set_status_clicked"> 
    public void btn_set_status_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        FhrdEmpmst entEmpmst = fhrdEmpmstFacade.find(Integer.valueOf(txt_user_id));
        FhrdEmpeventRelieve entEmpeventRelieve = new FhrdEmpeventRelieve();
        entEmpeventRelieve.setCreateby(globalData.getEnt_login_user());
        entEmpeventRelieve.setCreatedt(new Date());

        FhrdEventMst entEventMst = new FhrdEventMst();
        if (ddl_status.equals("R")) {
            entEventMst = fhrdEventMstFacade.findByEventName("ORG_RESIGN_DATE");
        }
        if (ddl_status.equals("L")) {
            entEventMst = fhrdEventMstFacade.findByEventName("ORG_RELIEVE_DATE");
        }
        if (ddl_status.equals("T")) {
            entEventMst = fhrdEventMstFacade.findByEventName("CONT_TERMINATION_DATE");
        }

        entEmpeventRelieve.setEmSrgKey(entEventMst.getEmSrgKey());
        entEmpeventRelieve.setEmperSrgKey(getGlobalSettings().getUniqueSrno((cal_event_date.getYear() + 1900), "FHRD_EMPEVENT_RELIEVE"));
        entEmpeventRelieve.setEventDate(cal_event_date);
        entEmpeventRelieve.setEventType(ddl_status);
        entEmpeventRelieve.setRemarks(txt_remarks.toUpperCase());
        entEmpeventRelieve.setUserId(entEmpmst);
        entEmpeventRelieve.setOumUnitSrno(globalData.getEnt_working_ou());
        entEmpeventRelieve.setRefOumUnitSrno(globalData.getWorking_ou());

        List<FhrdEmpContractMst> lstEmpContractMstsUpdate = new ArrayList<FhrdEmpContractMst>();
        if (ddl_status.equals("L") || ddl_status.equals("T")) {
            List<FhrdEmpContractMst> lstEmpContractMsts = fhrdEmpContractMstFacade.findAllToRelieveTerminate(Integer.valueOf(txt_user_id), DateTimeUtility.ChangeDateFormat(cal_event_date, null));
            int n = lstEmpContractMsts.size();

            for (int i = 0; i < n; i++) {
                FhrdEmpContractMst entEmpContractMst = lstEmpContractMsts.get(i);
                entEmpContractMst.setUpdateby(globalData.getEnt_login_user());
                entEmpContractMst.setUpdatedt(new Date());
                if (!entEmpContractMst.getEndDate().equals(cal_event_date)) {
                    entEmpContractMst.setContSuddenEndFlag("Y");
                }
                entEmpContractMst.setEndDate(cal_event_date);
                entEmpContractMst.setTranEndDate(cal_event_date);
                lstEmpContractMstsUpdate.add(entEmpContractMst);
            }
        }

        try {
            boolean isTransactionComplete = customJpaController.ResignRelieveTermination(entEmpeventRelieve, lstEmpContractMstsUpdate, globalData.getUser_id());

            if (isTransactionComplete) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Employee Event Detail saved Successfully"));
                txt_user_id = null;
                selectedUser = null;
                ddl_status = null;
                cal_event_date = null;
                txt_remarks = null;
            } else {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred."));
            }

        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred."));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_set_status_clicked", null, e);
        }
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="Notice Period Settings">
    //<editor-fold defaultstate="collapsed" desc="txt_notice_period">
    String txt_notice_period;

    public String getTxt_notice_period() {
        return txt_notice_period;
    }

    public void setTxt_notice_period(String txt_notice_period) {
        this.txt_notice_period = txt_notice_period;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="change_notice_period">

    public void open_notice_period_dialog(ActionEvent event) {
        txt_notice_period = lastContract.getNoticePeriod().toString();
        RequestContext.getCurrentInstance().execute("pw_dlg_change_notice_period.show()");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="saveNoticePeriod">

    public void saveNoticePeriod(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            lastContract = fhrdEmpContractMstFacade.find(lastContract.getEcmSrgKey());
            lastContract.setNoticePeriod(new BigInteger(txt_notice_period));
            lastContract.setUpdateby(new FhrdEmpmst(globalData.getUser_id()));
            fhrdEmpContractMstFacade.edit(lastContract);
            lastContract = fhrdEmpContractMstFacade.find(lastContract.getEcmSrgKey());
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Notice Period Updated", "Notice Period updated successfully."));
            RequestContext.getCurrentInstance().execute("pw_dlg_change_notice_period.hide()");
        } catch (Exception e) {
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_set_status_clicked", null, e);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred."));
        }

    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()"> 

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings and default methods">
    //<editor-fold defaultstate="collapsed" desc="default method">

    /**
     * Creates a new instance of PIS_Emp_Resign_Relieve
     */
    public PIS_Emp_Resign_Relieve() {
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold> 
}
