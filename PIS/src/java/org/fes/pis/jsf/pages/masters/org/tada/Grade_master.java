package org.fes.pis.jsf.pages.masters.org.tada;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.FhrdEmpmst;
import org.fes.pis.entities.FtasTadaGradeMst;
import org.fes.pis.entities.SystOrgUnitMst;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.jsf.utilities.JSFMessages;
import org.fes.pis.sessions.FtasTadaGradeMstFacade;
import org.primefaces.context.RequestContext;

/**
 *
 * @author anu
 */
@ManagedBean
@ViewScoped
public class Grade_master implements Serializable {

    @EJB
    private FtasTadaGradeMstFacade ftasTadaGradeMstFacade;
    
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="saving methods">
    //<editor-fold defaultstate="collapsed" desc="txt_grade_desc">
    String txt_grade_desc;

    public String getTxt_grade_desc() {
        return txt_grade_desc;
    }

    public void setTxt_grade_desc(String txt_grade_desc) {
        this.txt_grade_desc = txt_grade_desc;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_start_date">
    Date cal_start_date;

    public Date getCal_start_date() {
        return cal_start_date;
    }

    public void setCal_start_date(Date cal_start_date) {
        this.cal_start_date = cal_start_date;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_end_date">
    Date cal_end_date;

    public Date getCal_end_date() {
        return cal_end_date;
    }

    public void setCal_end_date(Date cal_end_date) {
        this.cal_end_date = cal_end_date;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetEntryComponent()">

    private void resetEntryComponent() {
        txt_grade_desc = null;
        cal_start_date = null;
        cal_end_date = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_save">

    public void btn_save(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            FtasTadaGradeMst ent_FtasTadaGradeMst = new FtasTadaGradeMst();
            ent_FtasTadaGradeMst.setGmSrgKey(new Long("-1"));
            ent_FtasTadaGradeMst.setGmDesc(txt_grade_desc.toUpperCase().trim());
            ent_FtasTadaGradeMst.setStartDate(cal_start_date);
            ent_FtasTadaGradeMst.setEndDate(cal_end_date);
            ent_FtasTadaGradeMst.setCreateby(new FhrdEmpmst(globalData.getUser_id()));
            ent_FtasTadaGradeMst.setCreatedt(new Date());
            ent_FtasTadaGradeMst.setOumUnitSrno(new SystOrgUnitMst(globalData.getWorking_ou()));
            ftasTadaGradeMstFacade.create(ent_FtasTadaGradeMst);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Grade Created", "Grade created successfully"));
            setTbl_grade_mst();
            resetEntryComponent();
        } catch (Exception e) {
            context.addMessage(null, JSFMessages.getErrorMessage(e));
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_accept_clicked", null, e);
        }
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="main table methods">
    //<editor-fold defaultstate="collapsed" desc="tbl_grade_mst">
    List<FtasTadaGradeMst> tbl_grade_mst;
    List<FtasTadaGradeMst> tbl_grade_mst_filter;

    public List<FtasTadaGradeMst> getTbl_grade_mst() {
        return tbl_grade_mst;
    }

    public void setTbl_grade_mst(List<FtasTadaGradeMst> tbl_grade_mst) {
        this.tbl_grade_mst = tbl_grade_mst;
    }

    public List<FtasTadaGradeMst> getTbl_grade_mst_filter() {
        return tbl_grade_mst_filter;
    }

    public void setTbl_grade_mst_filter(List<FtasTadaGradeMst> tbl_grade_mst_filter) {
        this.tbl_grade_mst_filter = tbl_grade_mst_filter;
    }

    private void setTbl_grade_mst() {
        tbl_grade_mst = ftasTadaGradeMstFacade.findAll(globalData.getWorking_ou(), false);
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Apply End Date Methods">
    //<editor-fold defaultstate="collapsed" desc="selectedGradeMst">
    FtasTadaGradeMst selectedGradeMst;

    public FtasTadaGradeMst getSelectedGradeMst() {
        return selectedGradeMst;
    }

    public void setSelectedGradeMst(FtasTadaGradeMst selectedGradeMst) {
        this.selectedGradeMst = selectedGradeMst;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="applyEndDate">
    public void applyEndDate(FtasTadaGradeMst ftasTadaGradeMst) {
        selectedGradeMst = ftasTadaGradeMst;
        cal_apply_end_date = null;
        cal_apply_end_date_min = DateTimeUtility.ChangeDateFormat(ftasTadaGradeMst.getStartDate(), null);
        RequestContext.getCurrentInstance().execute("pw_dlg_apply_end_date.show()");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_apply_end_date">
    Date cal_apply_end_date;
    String cal_apply_end_date_min;

    public Date getCal_apply_end_date() {
        return cal_apply_end_date;
    }

    public void setCal_apply_end_date(Date cal_apply_end_date) {
        this.cal_apply_end_date = cal_apply_end_date;
    }

    public String getCal_apply_end_date_min() {
        return cal_apply_end_date_min;
    }

    public void setCal_apply_end_date_min(String cal_apply_end_date_min) {
        this.cal_apply_end_date_min = cal_apply_end_date_min;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_apply_end_date">

    public void btn_apply_end_date(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            selectedGradeMst = ftasTadaGradeMstFacade.find(selectedGradeMst.getGmSrgKey());
            selectedGradeMst.setEndDate(cal_apply_end_date);
            selectedGradeMst.setUpdateby(new FhrdEmpmst(globalData.getUser_id()));
            ftasTadaGradeMstFacade.edit(selectedGradeMst);
            setTbl_grade_mst();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "End Date Applied", "End Date applied successfully"));
            RequestContext.getCurrentInstance().execute("pw_dlg_apply_end_date.hide()");
        } catch (Exception e) {
            context.addMessage(null, JSFMessages.getErrorMessage(e));
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_accept_clicked", null, e);
        }
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setTbl_grade_mst();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Default Constructor & Other Beans Methods">
    // <editor-fold defaultstate="collapsed" desc="Default Constructor">

    public Grade_master() {
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // </editor-fold>
    //</editor-fold>
}
