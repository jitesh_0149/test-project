/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.db.fes_project.pack.Ou_Pack;

/**
 *
 * @author jitesh
 */
@ManagedBean(name = "PIS_GlobalSettings")
@SessionScoped
public class PIS_GlobalSettings implements Serializable {

    // <editor-fold defaultstate="collapsed" desc="working_ou_changed()">
    public void working_ou_changed() {
    }// </editor-fold>
    Ou_Pack ou_Pack = new Ou_Pack();
    //<editor-fold defaultstate="collapsed" desc="breadCumb">
    String brPis_system = getGlobalResources().getProject_path() + "pages/HomePage.htm";
    String brPis = "#";
    String brPis_trans = "#";
    String brPis_trans_tas = "#";
    String brPis_trans_tada = "#";
    String brPis_payroll = "#";
    String brPis_mst = "#";
    String brPis_mst_org = "#";
    String brPis_mst_org_tab = "#";
    String brPis_mst_org_pi = "#";
    String brPis_mst_org_emp = "#";
    String brPis_rep = "#";
    String brPis_utl = "#";
    String brPis_mst_lnk = "#";
    String brPis_mst_lnk_fes = "#";
    String brPis_mst_lnk_br = "#";
    String brTas = "#";
    String brTas_LeaveTourTraining = "#";
    String brTas_approval = "#";
    String brTas_utilities = "#";
    String brPis_emp = "#";
    String brPis_emp_related = "#";
    String brView = "#";
    String brView_tas = "#";
    String brView_pis = "#";
    String brUtil;

    public String getBrPis_trans_tas() {
        return brPis_trans_tas;
    }

    public void setBrPis_trans_tas(String brPis_trans_tas) {
        this.brPis_trans_tas = brPis_trans_tas;
    }

    public String getBrPis_mst_org_tab() {
        return brPis_mst_org_tab;
    }

    public void setBrPis_mst_org_tab(String brPis_mst_org_tab) {
        this.brPis_mst_org_tab = brPis_mst_org_tab;
    }

    public String getBrUtil() {
        return brUtil;
    }

    public void setBrUtil(String brUtil) {
        this.brUtil = brUtil;
    }

    public String getBrView() {
        return brView;
    }

    public void setBrView(String brView) {
        this.brView = brView;
    }

    public String getBrView_tas() {
        return brView_tas;
    }

    public void setBrView_tas(String brView_tas) {
        this.brView_tas = brView_tas;
    }

    public String getBrView_pis() {
        return brView_pis;
    }

    public void setBrView_pis(String brView_pis) {
        this.brView_pis = brView_pis;
    }

    public String getBrPis_trans() {
        return brPis_trans;
    }

    public void setBrPis_trans(String brPis_trans) {
        this.brPis_trans = brPis_trans;
    }

    public String getBrPis_payroll() {
        return brPis_payroll;
    }

    public void setBrPis_payroll(String brPis_payroll) {
        this.brPis_payroll = brPis_payroll;
    }

    public String getBrTas_approval() {
        return brTas_approval;
    }

    public void setBrTas_approval(String brTas_approval) {
        this.brTas_approval = brTas_approval;
    }

    public String getBrTas_utilities() {
        return brTas_utilities;
    }

    public void setBrTas_utilities(String brTas_utilities) {
        this.brTas_utilities = brTas_utilities;
    }

    public String getBrTas_LeaveTourTraining() {
        return brTas_LeaveTourTraining;
    }

    public void setBrTas_LeaveTourTraining(String brTas_LeaveTourTraining) {
        this.brTas_LeaveTourTraining = brTas_LeaveTourTraining;
    }

    public String getBrPis_mst_org() {
        return brPis_mst_org;
    }

    public void setBrPis_mst_org(String brPis_mst_org) {
        this.brPis_mst_org = brPis_mst_org;
    }

    public String getBrPis_mst_org_emp() {
        return brPis_mst_org_emp;
    }

    public void setBrPis_mst_org_emp(String brPis_mst_org_emp) {
        this.brPis_mst_org_emp = brPis_mst_org_emp;
    }

    public String getBrPis_trans_tada() {
        return brPis_trans_tada;
    }

    public void setBrPis_trans_tada(String brPis_trans_tada) {
        this.brPis_trans_tada = brPis_trans_tada;
    }

    public String getBrPis_mst_org_pi() {
        return brPis_mst_org_pi;
    }

    public void setBrPis_mst_org_pi(String brPis_mst_org_pi) {
        this.brPis_mst_org_pi = brPis_mst_org_pi;
    }

    public String getBrPis_emp() {
        return brPis_emp;
    }

    public void setBrPis_emp(String brPis_emp) {
        this.brPis_emp = brPis_emp;
    }

    public String getBrTas() {
        return brTas;
    }

    public void setBrTas(String brTas) {
        this.brTas = brTas;
    }

    public String getBrPis_emp_related() {
        return brPis_emp_related;
    }

    public void setBrPis_emp_releted(String brPis_emp_related) {
        this.brPis_emp_related = brPis_emp_related;
    }

    public String getBrPis() {
        return brPis;
    }

    public void setBrPis(String brPis) {
        this.brPis = brPis;
    }

    public String getBrPis_mst() {
        return brPis_mst;
    }

    public void setBrPis_mst(String brPis_mst) {
        this.brPis_mst = brPis_mst;
    }

    public String getBrPis_mst_lnk() {
        return brPis_mst_lnk;
    }

    public void setBrPis_mst_lnk(String brPis_mst_lnk) {
        this.brPis_mst_lnk = brPis_mst_lnk;
    }

    public String getBrPis_mst_lnk_br() {
        return brPis_mst_lnk_br;
    }

    public void setBrPis_mst_lnk_br(String brPis_mst_lnk_br) {
        this.brPis_mst_lnk_br = brPis_mst_lnk_br;
    }

    public String getBrPis_mst_lnk_fes() {
        return brPis_mst_lnk_fes;
    }

    public void setBrPis_mst_lnk_fes(String brPis_mst_lnk_fes) {
        this.brPis_mst_lnk_fes = brPis_mst_lnk_fes;
    }

    public String getBrPis_rep() {
        return brPis_rep;
    }

    public void setBrPis_rep(String brPis_rep) {
        this.brPis_rep = brPis_rep;
    }

    public String getBrPis_system() {
        return brPis_system;
    }

    public void setBrPis_system(String brPis_system) {
        this.brPis_system = brPis_system;
    }

    public String getBrPis_utl() {
        return brPis_utl;
    }

    public void setBrPis_utl(String brPis_utl) {
        this.brPis_utl = brPis_utl;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Employee Modes for Employee Information">
    Integer employeeForUpdate;
    Integer employeeForView;
    boolean forVerification;

    public boolean isForVerification() {
        return forVerification;
    }

    public void setForVerification(boolean forVerification) {
        this.forVerification = forVerification;
    }

    public Integer getEmployeeForUpdate() {
        return employeeForUpdate;
    }

    public void setEmployeeForUpdate(Integer employeeForUpdate) {
        this.employeeForUpdate = employeeForUpdate;
    }

    public Integer getEmployeeForView() {
        return employeeForView;
    }

    public void setEmployeeForView(Integer employeeForView) {
        this.employeeForView = employeeForView;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Get System/Admin User">
    Boolean sysetemUser_Ftas;
    Boolean sysetemUser_Fhrd;

    public Boolean getSysetemUser_Fhrd() {
        if (sysetemUser_Fhrd == null) {
            sysetemUser_Fhrd = (ou_Pack.Check_Sysuser(this.getClass().getName(), "getSysetemUser()", getGlobalSettings().getUser_id(), getGlobalSettings().getPosting_ou(), "FHRD").getString1().equals("Y") ? true : false);
        }
        return sysetemUser_Fhrd;
    }

    public void setSysetemUser_Fhrd(Boolean sysetemUser_Fhrd) {
        this.sysetemUser_Fhrd = sysetemUser_Fhrd;
    }

    public Boolean getSysetemUser_Ftas() {
        if (sysetemUser_Ftas == null) {
            sysetemUser_Ftas = (ou_Pack.Check_Sysuser(this.getClass().getName(), "getSysetemUser()", getGlobalSettings().getUser_id(), getGlobalSettings().getPosting_ou(), "FTAS").getString1().equals("Y") ? true : false);
        }
        return sysetemUser_Ftas;
    }

    public void setSysetemUser_Ftas(Boolean sysetemUser_Ftas) {
        this.sysetemUser_Ftas = sysetemUser_Ftas;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="systemUser_FHRD">
    Boolean systemUser_FHRD = null;

    public Boolean getSystemUser_FHRD() {
        if (systemUser_FHRD == null) {
            systemUser_FHRD = (ou_Pack.Check_Sysuser(this.getClass().getName(), "getSystemUser()", getGlobalSettings().getUser_id(), getGlobalSettings().getPosting_ou(), "FHRD").getString1().equals("Y") ? true : false);
        }
        return systemUser_FHRD;
    }

    public void setSystemUser_FHRD(Boolean systemUser_FHRD) {
        this.systemUser_FHRD = systemUser_FHRD;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="systemUser_FTAS">
    Boolean systemUser_FTAS = null;

    public Boolean getSystemUser_FTAS() {
        if (systemUser_FTAS == null) {
            systemUser_FTAS = (ou_Pack.Check_Sysuser(this.getClass().getName(), "getSystemUser()", getGlobalSettings().getUser_id(), getGlobalSettings().getPosting_ou(), "FTAS").getString1().equals("Y") ? true : false);
        }
        return systemUser_FTAS;
    }

    public void setSystemUser_FTAS(Boolean systemUser_FTAS) {
        this.systemUser_FTAS = systemUser_FTAS;
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Permission Settings">
    boolean view_detail_master_table_fhrd = true;
    boolean create_master_tbl_record_fhrd = getSystemUser_FHRD();
    boolean update_master_tbl_record_fhrd = getSystemUser_FHRD();

    public boolean isCreate_master_tbl_record_fhrd() {
        return create_master_tbl_record_fhrd;
    }

    public void setCreate_master_tbl_record_fhrd(boolean create_master_tbl_record_fhrd) {
        this.create_master_tbl_record_fhrd = create_master_tbl_record_fhrd;
    }

    public boolean isUpdate_master_tbl_record_fhrd() {
        return update_master_tbl_record_fhrd;
    }

    public void setUpdate_master_tbl_record_fhrd(boolean update_master_tbl_record_fhrd) {
        this.update_master_tbl_record_fhrd = update_master_tbl_record_fhrd;
    }

    public boolean isView_detail_master_table_fhrd() {
        return view_detail_master_table_fhrd;
    }

    public void setView_detail_master_table_fhrd(boolean view_detail_master_table_fhrd) {
        this.view_detail_master_table_fhrd = view_detail_master_table_fhrd;
    }

    // </editor-fold>
    //<editor-fold defaultstate="collapsed" desc="getPIS_GlobalData()">
    public PIS_GlobalData getPIS_GlobalData() {
        PIS_GlobalData pis_GlobalData = new PIS_GlobalData();
        pis_GlobalData.setEnt_login_user(getGlobalSettings().getEnt_login_user());
        pis_GlobalData.setUser_id(pis_GlobalData.getEnt_login_user().getUserId());
        pis_GlobalData.setEnt_working_ou(getGlobalSettings().getEnt_working_ou());
        pis_GlobalData.setWorking_ou(pis_GlobalData.getEnt_working_ou().getOumUnitSrno());
        pis_GlobalData.setEnt_posting_ou(pis_GlobalData.getEnt_login_user().getPostingOumUnitSrno());
        pis_GlobalData.setOrganisation(getGlobalSettings().getOrganisation());
        pis_GlobalData.setOrganisationShortName(pis_GlobalData.getOrganisation().getOumShortName());
        pis_GlobalData.setOrganisationName(pis_GlobalData.getOrganisation().getOumName());
        pis_GlobalData.setSystemUserFHRD(getSystemUser_FHRD());
        pis_GlobalData.setSystemUserFTAS(getSystemUser_FTAS());
        return pis_GlobalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="getMonthFullNameFromDate">

    public String getMonthFullNameFromDate(String p_month) {
        String v_returning_value = null;
        if (p_month != null && !p_month.trim().isEmpty()) {

            Date d = DateTimeUtility.stringToDate("1-" + p_month + "-" + "2000", "dd-MMM-yyyy");
            SimpleDateFormat s = new SimpleDateFormat("MMMM");
            v_returning_value = s.format(d);
        }
        return v_returning_value;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Default Constructor & Other Beans">
    public PIS_GlobalSettings() {
    }
    // <editor-fold defaultstate="collapsed" desc="getGlobalUtilities">

    protected GlobalResources getGlobalResources() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalResources) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalResources");
    }// </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    //</editor-fold>
}
