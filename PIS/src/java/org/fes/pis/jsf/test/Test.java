/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fes.pis.jsf.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.imageio.stream.FileImageOutputStream;
import javax.servlet.ServletContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author jitesh
 */
@ManagedBean
@ViewScoped
public class Test implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="ImageUpload Settings">
    String image_for_user_id;

    public String getImage_for_user_id() {
        return image_for_user_id;
    }

    public void setImage_for_user_id(String image_for_user_id) {
        this.image_for_user_id = image_for_user_id;
    }
    //<editor-fold defaultstate="collapsed" desc="image_random_name">
    String image_random_name;

    public String getImage_random_name() {
        return image_random_name;
    }

    public void setImage_random_name(String image_random_name) {
        this.image_random_name = image_random_name;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="showUploaded">
    String showUploaded;

    public String getShowUploaded() {
        return showUploaded;
    }

    public void setShowUploaded(String showUploaded) {
        this.showUploaded = showUploaded;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="uploaded_image">
    UploadedFile uploaded_image;

    public UploadedFile getUploaded_image() {
        return uploaded_image;
    }

    public void setUploaded_image(UploadedFile uploaded_image) {
        this.uploaded_image = uploaded_image;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="upload_location">
    String upload_location;

    public String getUpload_location() {
        return upload_location;
    }

    public void setUpload_location(String upload_location) {
        this.upload_location = upload_location;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="handleFileUpload">

    public void handleFileUpload(FileUploadEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        uploaded_image = event.getFile();
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        String v_file_ext = uploaded_image.getFileName().split("\\.")[(uploaded_image.getFileName().split("\\.").length) - 1];
        upload_location = servletContext.getRealPath("") + File.separator + "temp-images" + File.separator + image_random_name + "." + v_file_ext;
        FileImageOutputStream imageOutput;
        try {
            imageOutput = new FileImageOutputStream(new File(upload_location));
            imageOutput.write(uploaded_image.getContents(), 0, uploaded_image.getContents().length);
            imageOutput.close();
        } catch (FileNotFoundException e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "File Not Found"));

        } catch (IOException e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected Error occurred during uploading image"));

        }
        showUploaded = "Y";
    }
    //</editor-fold>
    //</editor-fold>D

    public Test() {
    }
}
