package org.fes.pis.jsf.components;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.fes.lib.utilities.DateTimeUtility;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.Daily_Abs_Tour_Train;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.entities.*;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.*;
import org.primefaces.event.SelectEvent;

@ManagedBean
@ViewScoped
public class Dtl_attendance_summary implements Serializable {

    @EJB
    private FtasLeavemstFacadeLocal ftasLeavemstFacade;
    @EJB
    private FtasTrainingScheduleDtlFacadeLocal ftasTrainingScheduleDtlFacade;
    @EJB
    private FtasTourScheduleDtlFacadeLocal ftasTourScheduleDtlFacade;
    @EJB
    private FtasAbsenteesFacadeLocal ftasAbsenteesFacade;
    @EJB
    private FtasDailyFacadeLocal ftasDailyFacade;
    
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>  
    //<editor-fold defaultstate="collapsed" desc="selectedUser">
    FhrdEmpmst selectedUser;

    public FhrdEmpmst getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(FhrdEmpmst selectedUser) {
        this.selectedUser = selectedUser;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_from_date">
    Date cal_from_date;
    String cal_from_date_min;
    String cal_from_date_max;

    public Date getCal_from_date() {
        return cal_from_date;
    }

    public void setCal_from_date(Date cal_from_date) {
        this.cal_from_date = cal_from_date;
    }

    public String getCal_from_date_max() {
        return cal_from_date_max;
    }

    public void setCal_from_date_max(String cal_from_date_max) {
        this.cal_from_date_max = cal_from_date_max;
    }

    public String getCal_from_date_min() {
        return cal_from_date_min;
    }

    public void setCal_from_date_min(String cal_from_date_min) {
        this.cal_from_date_min = cal_from_date_min;
    }

    private void setCal_from_date() {
        if (cal_from_date == null) {
            Calendar c = new GregorianCalendar();
            c.set(Calendar.DATE, 1);
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            cal_from_date = c.getTime();
        }
        cal_from_date_changed();
    }

    public void cal_from_date_changed(SelectEvent event) {
        cal_from_date = (Date) event.getObject();
        cal_from_date_changed();
    }

    private void cal_from_date_changed() {
        cal_from_date_min = DateTimeUtility.ChangeDateFormat(cal_from_date, null);
        cal_from_date_max = DateTimeUtility.ChangeDateFormat(DateTimeUtility.converToDateTime(cal_from_date).plusMonths(3).minusDays(1).toDate(), null);
        setCal_to_date();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="cal_to_date">
    Date cal_to_date;
    String cal_to_date_min;
    String cal_to_date_max;

    public Date getCal_to_date() {
        return cal_to_date;
    }

    public void setCal_to_date(Date cal_to_date) {
        this.cal_to_date = cal_to_date;
    }

    public String getCal_to_date_max() {
        return cal_to_date_max;
    }

    public void setCal_to_date_max(String cal_to_date_max) {
        this.cal_to_date_max = cal_to_date_max;
    }

    public String getCal_to_date_min() {
        return cal_to_date_min;
    }

    public void setCal_to_date_min(String cal_to_date_min) {
        this.cal_to_date_min = cal_to_date_min;
    }

    private void setCal_to_date() {
        if (cal_to_date == null) {
            cal_to_date = DateTimeUtility.converToDateTime(cal_from_date).plusMonths(1).minusDays(1).toDate();
        } else if (cal_to_date.before(cal_from_date)) {
            cal_to_date = cal_from_date;
        }
        cal_to_date_min = DateTimeUtility.ChangeDateFormat(cal_from_date, null);
        cal_to_date_max = DateTimeUtility.ChangeDateFormat(DateTimeUtility.converToDateTime(cal_from_date).plusMonths(3).minusDays(1).toDate(), null);
        cal_to_date_changed();
    }

    public void cal_to_date_changed(SelectEvent event) {
        cal_to_date = (Date) event.getObject();
        cal_to_date_changed();
    }

    private void cal_to_date_changed() {
        setTbl_attendance_summary();
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="lst_Daily">
    List<Daily_Abs_Tour_Train> tbl_attendance_summary;

    public List<Daily_Abs_Tour_Train> getTbl_attendance_summary() {
        return tbl_attendance_summary;
    }

    public void setTbl_attendance_summary(List<Daily_Abs_Tour_Train> tbl_attendance_summary) {
        this.tbl_attendance_summary = tbl_attendance_summary;
    }

    private void setTbl_attendance_summary() {
        tbl_attendance_summary = new ArrayList<Daily_Abs_Tour_Train>();
        Date v_date = cal_from_date;

        totalDays = BigDecimal.valueOf(DateTimeUtility.findDuration(cal_from_date, cal_to_date, "D")).add(BigDecimal.ONE);
        totalHolidays = BigDecimal.ZERO;
        totalPresents = BigDecimal.ZERO;
        totalLeaves = BigDecimal.ZERO;
        totalPresents = BigDecimal.ZERO;
        totalFieldVisits = BigDecimal.ZERO;
        totalTours = BigDecimal.ZERO;
        totalTrainings = BigDecimal.ZERO;
        totalPending = BigDecimal.ZERO;
        BigDecimal v_half_value = new BigDecimal("0.5");
        Integer v_srg_key = 1;

        //FtasDaily
        List<FtasDaily> lst_Dailys = ftasDailyFacade.findAll(selectedUser.getUserId(), cal_from_date, cal_to_date);
        Iterator<FtasDaily> iterator = lst_Dailys.iterator();
        FtasDaily lastAttd = null;
        Date v_attddt = null;
        if (iterator.hasNext()) {
            lastAttd = iterator.next();
            v_attddt = lastAttd.getAttddt();
        }

        //FtasAbsentees
        List<FtasAbsentees> lst_Absenteeses = ftasAbsenteesFacade.findAll(globalData.getWorking_ou(), selectedUser.getUserId(), false, true, cal_from_date, cal_to_date, null, false, "L", true, null);
        Iterator<FtasAbsentees> iteratorAbsentees = lst_Absenteeses.iterator();
        FtasAbsentees lastAbsentees = null;
        Date v_abs_from_date = null;
        if (iteratorAbsentees.hasNext()) {
            lastAbsentees = iteratorAbsentees.next();
            v_abs_from_date = DateTimeUtility.findBiggestDate(lastAbsentees.getFromDate(), cal_from_date);
        }

        //FtasTourScheduleDtl
        List<FtasTourScheduleDtl> lst_TourScheduleDtls = ftasTourScheduleDtlFacade.findAll(selectedUser.getUserId(), cal_from_date, cal_to_date, true);
        Iterator<FtasTourScheduleDtl> iteratorTourScheduleDtl = lst_TourScheduleDtls.iterator();
        FtasTourScheduleDtl lastTourScheduleDtl = null;
        Date v_tour_from_date = null;
        if (iteratorTourScheduleDtl.hasNext()) {
            lastTourScheduleDtl = iteratorTourScheduleDtl.next();
            v_tour_from_date = DateTimeUtility.findBiggestDate(DateTimeUtility.removeTime(lastTourScheduleDtl.getFromDatetime()), cal_from_date);
        }

        //FtasTrainingScheduleDtl
        List<FtasTrainingScheduleDtl> lst_TrainingScheduleDtls = ftasTrainingScheduleDtlFacade.findAll(selectedUser.getUserId(), cal_from_date, cal_to_date, true);
        Iterator<FtasTrainingScheduleDtl> iteratorTrainingScheduleDtl = lst_TrainingScheduleDtls.iterator();
        FtasTrainingScheduleDtl lastTrainingScheduleDtl = null;
        Date v_training_from_date = null;
        if (iteratorTrainingScheduleDtl.hasNext()) {
            lastTrainingScheduleDtl = iteratorTrainingScheduleDtl.next();
            v_training_from_date = DateTimeUtility.findBiggestDate(DateTimeUtility.removeTime(lastTrainingScheduleDtl.getFromDatetime()), cal_from_date);
        }




        while (!v_date.after(cal_to_date)) {
            Daily_Abs_Tour_Train daily_Abs_Tour_Train = new Daily_Abs_Tour_Train(v_srg_key, v_date);
            try {
                //FtasDaily
                if (v_attddt != null && v_date.equals(v_attddt)) {
                    daily_Abs_Tour_Train.setFtasDaily(lastAttd);
                    if (lastAttd.getActualHalf1LmSrgKey() == null && lastAttd.getPendingHalf1LmSrgKey() == null) {
                        totalPending = totalPending.add(v_half_value);
                    } else if ((lastAttd.getPendingHalf1LmSrgKey() != null && lastAttd.getPendingHalf1LmSrgKey().getConsiderAsAttendanceFlag().equals("T"))
                            || (lastAttd.getActualHalf1LmSrgKey() != null && lastAttd.getActualHalf1LmSrgKey().getConsiderAsAttendanceFlag().equals("T"))) {
                        totalTours = totalTours.add(v_half_value);
                    } else if ((lastAttd.getPendingHalf1LmSrgKey() != null && lastAttd.getPendingHalf1LmSrgKey().getConsiderAsAttendanceFlag().equals("R"))
                            || (lastAttd.getActualHalf1LmSrgKey() != null && lastAttd.getActualHalf1LmSrgKey().getConsiderAsAttendanceFlag().equals("R"))) {
                        totalTrainings = totalTrainings.add(v_half_value);
                    } else if ((lastAttd.getPendingHalf1LmSrgKey() != null && lastAttd.getPendingHalf1LmSrgKey().getLeaveFlag().equals("Y"))
                            || (lastAttd.getActualHalf1LmSrgKey() != null && lastAttd.getActualHalf1LmSrgKey().getLeaveFlag().equals("Y"))) {
                        totalLeaves = totalLeaves.add(v_half_value);
                    } else if (lastAttd.getActualHalf1LmSrgKey() != null && lastAttd.getActualHalf1LmSrgKey().getConsiderAsAttendanceFlag().equals("F")) {
                        totalFieldVisits = totalFieldVisits.add(v_half_value);
                    } else if (lastAttd.getActualHalf1LmSrgKey() != null && lastAttd.getActualHalf1LmSrgKey().getHolidayFlag().equals("Y")) {
                        totalHolidays = totalHolidays.add(v_half_value);
                    } else if (lastAttd.getActualHalf1LmSrgKey() != null && lastAttd.getActualHalf1LmSrgKey().getConsiderAsAttendanceFlag().equals("Y")) {
                        totalPresents = totalPresents.add(v_half_value);
                    }

                    if (lastAttd.getActualHalf2LmSrgKey() == null && lastAttd.getPendingHalf2LmSrgKey() == null) {
                        totalPending = totalPending.add(v_half_value);
                    } else if ((lastAttd.getPendingHalf2LmSrgKey() != null && lastAttd.getPendingHalf2LmSrgKey().getConsiderAsAttendanceFlag().equals("T"))
                            || (lastAttd.getActualHalf2LmSrgKey() != null && lastAttd.getActualHalf2LmSrgKey().getConsiderAsAttendanceFlag().equals("T"))) {
                        totalTours = totalTours.add(v_half_value);
                    } else if ((lastAttd.getPendingHalf2LmSrgKey() != null && lastAttd.getPendingHalf2LmSrgKey().getConsiderAsAttendanceFlag().equals("R"))
                            || (lastAttd.getActualHalf2LmSrgKey() != null && lastAttd.getActualHalf2LmSrgKey().getConsiderAsAttendanceFlag().equals("R"))) {
                        totalTrainings = totalTrainings.add(v_half_value);
                    } else if ((lastAttd.getPendingHalf2LmSrgKey() != null && lastAttd.getPendingHalf2LmSrgKey().getLeaveFlag().equals("Y"))
                            || (lastAttd.getActualHalf2LmSrgKey() != null && lastAttd.getActualHalf2LmSrgKey().getLeaveFlag().equals("Y"))) {
                        totalLeaves = totalLeaves.add(v_half_value);
                    } else if (lastAttd.getActualHalf2LmSrgKey() != null && lastAttd.getActualHalf2LmSrgKey().getConsiderAsAttendanceFlag().equals("F")) {
                        totalFieldVisits = totalFieldVisits.add(v_half_value);
                    } else if (lastAttd.getActualHalf2LmSrgKey() != null && lastAttd.getActualHalf2LmSrgKey().getHolidayFlag().equals("Y")) {
                        totalHolidays = totalHolidays.add(v_half_value);
                    } else if (lastAttd.getActualHalf2LmSrgKey() != null && lastAttd.getActualHalf2LmSrgKey().getConsiderAsAttendanceFlag().equals("Y")) {
                        totalPresents = totalPresents.add(v_half_value);
                    }


                    if (iterator.hasNext()) {
                        lastAttd = iterator.next();
                        v_attddt = lastAttd.getAttddt();
                    } else {
                        lastAttd = null;
                        v_attddt = null;
                    }
                } else {
                    totalPending = totalPending.add(BigDecimal.ONE);
                }
            } catch (Exception e) {
                LogGenerator.generateLog(systemName, Level.SEVERE, systemName, lastAttd.getAttddt().toString() + "  DD   " + lastAttd.getPendingHalf1LmSrgKey() + " LLLLLLLLLLLLLLLL", systemName, e);
            }

            //FtasAbsentees
            while (lastAbsentees != null
                    && v_abs_from_date.equals(v_date)) {
                daily_Abs_Tour_Train.setFtasAbsentees(lastAbsentees);
                daily_Abs_Tour_Train.setDescription("Leave : " + lastAbsentees.getReason1());
                if (v_abs_from_date.before(lastAbsentees.getToDate())) {
                    v_abs_from_date = DateTimeUtility.nextDate(v_abs_from_date);
                } else {
                    if (iteratorAbsentees.hasNext()) {
                        lastAbsentees = iteratorAbsentees.next();
                        v_abs_from_date = lastAbsentees.getFromDate();

                    } else {
                        lastAbsentees = null;
                        v_abs_from_date = null;
                    }

                }
            }


            //FtasTourScheduleDtl

            while (lastTourScheduleDtl != null
                    && v_tour_from_date.equals(v_date)) {
                boolean findNext = true;
                if (!v_tour_from_date.after(DateTimeUtility.removeTime(lastTourScheduleDtl.getToDatetime()))) {
                    Date v_next_date = DateTimeUtility.nextDate(v_tour_from_date);
                    if (!v_next_date.after(DateTimeUtility.removeTime(lastTourScheduleDtl.getToDatetime()))) {
                        findNext = false;
                        v_tour_from_date = v_next_date;
                    }
                    daily_Abs_Tour_Train.setFtasTourScheduleDtl(lastTourScheduleDtl);
                    daily_Abs_Tour_Train.setDescription((daily_Abs_Tour_Train.getDescription() == null ? "" : daily_Abs_Tour_Train.getDescription() + "<br/>")
                            + "Tour : " + lastTourScheduleDtl.getPurpose());
                    daily_Abs_Tour_Train.setPlace((daily_Abs_Tour_Train.getPlace() == null ? "" : daily_Abs_Tour_Train.getPlace() + "<br/>")
                            + "From " + lastTourScheduleDtl.getFromplace() + " to " + lastTourScheduleDtl.getToplace());
                }
                if (findNext) {
                    if (iteratorTourScheduleDtl.hasNext()) {
                        lastTourScheduleDtl = iteratorTourScheduleDtl.next();
                        v_tour_from_date = DateTimeUtility.removeTime(lastTourScheduleDtl.getFromDatetime());
                    } else {
                        lastTourScheduleDtl = null;
                        v_tour_from_date = null;
                    }

                }
            }


            //FtasTrainingScheduleDtl

            while (lastTrainingScheduleDtl != null
                    && v_training_from_date.equals(v_date)) {
                boolean findNext = true;
                if (!v_training_from_date.after(DateTimeUtility.removeTime(lastTrainingScheduleDtl.getToDatetime()))) {
                    Date v_next_date = DateTimeUtility.nextDate(v_training_from_date);
                    if (!v_next_date.after(DateTimeUtility.removeTime(lastTrainingScheduleDtl.getToDatetime()))) {
                        findNext = false;
                        v_training_from_date = v_next_date;
                    }
                    daily_Abs_Tour_Train.setFtasTrainingScheduleDtl(lastTrainingScheduleDtl);
                    daily_Abs_Tour_Train.setDescription((lastTrainingScheduleDtl.getDescription() == null ? "" : "<br/>") + "Training : " + lastTrainingScheduleDtl.getDescription());
                }
                if (findNext) {
                    if (iteratorTrainingScheduleDtl.hasNext()) {
                        lastTrainingScheduleDtl = iteratorTrainingScheduleDtl.next();
                        v_training_from_date = DateTimeUtility.removeTime(lastTrainingScheduleDtl.getFromDatetime());
                    } else {
                        lastTrainingScheduleDtl = null;
                        v_training_from_date = null;
                    }

                }
            }


            tbl_attendance_summary.add(daily_Abs_Tour_Train);
            v_date = DateTimeUtility.nextDate(v_date);
            v_srg_key += 1;
        }


    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="leaveCdList">
    List<FtasLeavemst> leaveCdList;

    public List<FtasLeavemst> getLeaveCdList() {
        return leaveCdList;
    }

    public void setLeaveCdList(List<FtasLeavemst> leaveCdList) {
        this.leaveCdList = leaveCdList;
    }

    private void setLeaveCdList() {
        leaveCdList = ftasLeavemstFacade.findAll(globalData.getWorking_ou());
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Summary">    
    BigDecimal totalDays;
    BigDecimal totalHolidays;
    BigDecimal totalPresents;
    BigDecimal totalFieldVisits;
    BigDecimal totalLeaves;
    BigDecimal totalTours;
    BigDecimal totalTrainings;
    BigDecimal totalPending;

    public BigDecimal getTotalHolidays() {
        return totalHolidays;
    }

    public void setTotalHolidays(BigDecimal totalHolidays) {
        this.totalHolidays = totalHolidays;
    }

    public BigDecimal getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(BigDecimal totalDays) {
        this.totalDays = totalDays;
    }

    public BigDecimal getTotalLeaves() {
        return totalLeaves;
    }

    public void setTotalLeaves(BigDecimal totalLeaves) {
        this.totalLeaves = totalLeaves;
    }

    public BigDecimal getTotalPending() {
        return totalPending;
    }

    public void setTotalPending(BigDecimal totalPending) {
        this.totalPending = totalPending;
    }

    public BigDecimal getTotalPresents() {
        return totalPresents;
    }

    public void setTotalPresents(BigDecimal totalPresents) {
        this.totalPresents = totalPresents;
    }

    public BigDecimal getTotalTours() {
        return totalTours;
    }

    public void setTotalTours(BigDecimal totalTours) {
        this.totalTours = totalTours;
    }

    public BigDecimal getTotalTrainings() {
        return totalTrainings;
    }

    public void setTotalTrainings(BigDecimal totalTrainings) {
        this.totalTrainings = totalTrainings;
    }

    public BigDecimal getTotalFieldVisits() {
        return totalFieldVisits;
    }

    public void setTotalFieldVisits(BigDecimal totalFieldVisits) {
        this.totalFieldVisits = totalFieldVisits;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="showDialog">
    public void showDialog(FtasAbsentees ftasAbsentees) {
        selectedUser = ftasAbsentees.getUserId();
        cal_from_date = DateTimeUtility.startDateOfMonthFromDate(ftasAbsentees.getFromDate());
        cal_to_date = DateTimeUtility.endDateOfMonthFromDate(ftasAbsentees.getFromDate());
        cal_to_date = DateTimeUtility.findBiggestDate(ftasAbsentees.getToDate(), cal_to_date);
        setCal_from_date();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        setLeaveCdList();


    }
//</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Default Constructor & Other Beans Methods">    //<editor-fold defaultstate="collapsed" desc="Default Constructor">
    //<editor-fold defaultstate="collapsed" desc="Default Constructor">

    public Dtl_attendance_summary() {
    }
    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Global Utilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    //</editor-fold>
}
