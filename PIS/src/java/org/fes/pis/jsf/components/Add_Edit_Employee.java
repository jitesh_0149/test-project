package org.fes.pis.jsf.components;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.imageio.stream.FileImageOutputStream;
import javax.servlet.ServletContext;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.Database_Output;
import org.fes.pis.custom_entities.PIS_GlobalData;
import org.fes.pis.custom_entities.System_Properties;
import org.fes.pis.custom_sessions.customJpaController;
import org.fes.pis.db.fes_project.pack.Fcad_Pack;
import org.fes.pis.db.fes_project.pack.Mail_pack;
import org.fes.pis.db.fes_project.pack.Ou_Pack;
import org.fes.pis.entities.*;
import org.fes.pis.jsf.GlobalResources;
import org.fes.pis.jsf.GlobalSettings;
import org.fes.pis.jsf.GlobalUtilities;
import org.fes.pis.jsf.PIS_GlobalSettings;
import org.fes.pis.sessions.*;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author anu
 */
@ManagedBean(name = "Add_Edit_Employee")
@ViewScoped
public class Add_Edit_Employee implements Serializable {

    //<editor-fold defaultstate="collapsed" desc="globalData">
    PIS_GlobalData globalData;

    public PIS_GlobalData getGlobalData() {
        return globalData;
    }

    public void setGlobalData(PIS_GlobalData globalData) {
        this.globalData = globalData;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Declarations">
    System_Properties system_Properties = new System_Properties();
    String systemName = system_Properties.getSystemName();
    Ou_Pack ou_Pack = new Ou_Pack();
    Fcad_Pack fcad_Pack = new Fcad_Pack();
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="boolean variables">
    boolean editMode;
    boolean createMode;
    boolean viewMode;
    boolean forVerification;
    boolean chk_verified_by_user;

    public boolean isChk_verified_by_user() {
        return chk_verified_by_user;
    }

    public void setChk_verified_by_user(boolean chk_verified_by_user) {
        this.chk_verified_by_user = chk_verified_by_user;
    }

    public boolean isCreateMode() {
        return createMode;
    }

    public void setCreateMode(boolean createMode) {
        this.createMode = createMode;
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    public boolean isViewMode() {
        return viewMode;
    }

    public void setViewMode(boolean viewMode) {
        this.viewMode = viewMode;
    }

    public boolean isForVerification() {
        return forVerification;
    }

    public void setForVerification(boolean forVerification) {
        this.forVerification = forVerification;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="binded attributes">
    String txt_user_name;
    String txt_user_middle_name;
    String txt_user_last_name;
    String txt_father_name;
    String txt_mother_name;
//    String txt_emp_init;
    String ddl_gender;
    String ddl_nationality;
    Date cal_birthdate;
    String ddl_marital_status;
    String ddl_blood_group;
    String txt_home_town;
    Date cal_org_joining_date;
    Date cal_ou_joining_date;
    String txt_pf_no;
    String txt_ntgcfpf_no;
    String txt_ifsc_code;
    String txt_bank_account_no;
    String txt_pan_no;
    String txt_bank_name;
    String txt_bank_location;
    Date cal_passport_issue_date;
    Date cal_passport_expire_date;
    String txt_passport_auth;
    String txt_passport_no;
    String txt_probation_period;
    String txt_notice_period;

    public String getTxt_notice_period() {
        return txt_notice_period;
    }

    public void setTxt_notice_period(String txt_notice_period) {
        this.txt_notice_period = txt_notice_period;
    }

    public String getTxt_probation_period() {
        return txt_probation_period;
    }

    public void setTxt_probation_period(String txt_probation_period) {
        this.txt_probation_period = txt_probation_period;
    }

    public String getTxt_ifsc_code() {
        return txt_ifsc_code;
    }

    public void setTxt_ifsc_code(String txt_ifsc_code) {
        this.txt_ifsc_code = txt_ifsc_code;
    }

    public Date getCal_passport_expire_date() {
        return cal_passport_expire_date;
    }

    public void setCal_passport_expire_date(Date cal_passport_expire_date) {
        this.cal_passport_expire_date = cal_passport_expire_date;
    }

    public Date getCal_passport_issue_date() {
        return cal_passport_issue_date;
    }

    public String getTxt_father_name() {
        return txt_father_name;
    }

    public void setTxt_father_name(String txt_father_name) {
        this.txt_father_name = txt_father_name;
    }

    public String getTxt_mother_name() {
        return txt_mother_name;
    }

    public void setTxt_mother_name(String txt_mother_name) {
        this.txt_mother_name = txt_mother_name;
    }

    public void setCal_passport_issue_date(Date cal_passport_issue_date) {
        this.cal_passport_issue_date = cal_passport_issue_date;
    }

    public String getTxt_passport_auth() {
        return txt_passport_auth;
    }

    public void setTxt_passport_auth(String txt_passport_auth) {
        this.txt_passport_auth = txt_passport_auth;
    }

    public String getTxt_passport_no() {
        return txt_passport_no;
    }

    public void setTxt_passport_no(String txt_passport_no) {
        this.txt_passport_no = txt_passport_no;
    }

    public String getDdl_nationality() {
        return ddl_nationality;
    }

    public void setDdl_nationality(String ddl_nationality) {
        this.ddl_nationality = ddl_nationality;
    }

    //<editor-fold defaultstate="collapsed" desc="getter/setter">
    public String getTxt_user_middle_name() {
        return txt_user_middle_name;
    }

    public void setTxt_user_middle_name(String txt_user_middle_name) {
        this.txt_user_middle_name = txt_user_middle_name;
    }

    public String getTxt_user_last_name() {
        return txt_user_last_name;
    }

    public void setTxt_user_last_name(String txt_user_last_name) {
        this.txt_user_last_name = txt_user_last_name;
    }

    public Date getCal_org_joining_date() {
        return cal_org_joining_date;
    }

    public void setCal_org_joining_date(Date cal_org_joining_date) {
        this.cal_org_joining_date = cal_org_joining_date;
    }

    public Date getCal_ou_joining_date() {
        return cal_ou_joining_date;
    }

    public void setCal_ou_joining_date(Date cal_ou_joining_date) {
        this.cal_ou_joining_date = cal_ou_joining_date;
    }

    public String getTxt_bank_account_no() {
        return txt_bank_account_no;
    }

    public void setTxt_bank_account_no(String txt_bank_account_no) {
        this.txt_bank_account_no = txt_bank_account_no;
    }

    public String getTxt_bank_location() {
        return txt_bank_location;
    }

    public void setTxt_bank_location(String txt_bank_location) {
        this.txt_bank_location = txt_bank_location;
    }

    public String getTxt_bank_name() {
        return txt_bank_name;
    }

    public void setTxt_bank_name(String txt_bank_name) {
        this.txt_bank_name = txt_bank_name;
    }

    public String getTxt_home_town() {
        return txt_home_town;
    }

    public void setTxt_home_town(String txt_home_town) {
        this.txt_home_town = txt_home_town;
    }

    public String getTxt_ntgcfpf_no() {
        return txt_ntgcfpf_no;
    }

    public void setTxt_ntgcfpf_no(String txt_ntgcfpf_no) {
        this.txt_ntgcfpf_no = txt_ntgcfpf_no;
    }

    public String getTxt_pan_no() {
        return txt_pan_no;
    }

    public void setTxt_pan_no(String txt_pan_no) {
        this.txt_pan_no = txt_pan_no;
    }

    public String getTxt_pf_no() {
        return txt_pf_no;
    }

    public void setTxt_pf_no(String txt_pf_no) {
        this.txt_pf_no = txt_pf_no;
    }

    public Date getCal_birthdate() {
        return cal_birthdate;
    }

    public void setCal_birthdate(Date cal_birthdate) {
        this.cal_birthdate = cal_birthdate;
    }

    public String getDdl_blood_group() {
        return ddl_blood_group;
    }

    public void setDdl_blood_group(String ddl_blood_group) {
        this.ddl_blood_group = ddl_blood_group;
    }

    public String getDdl_gender() {
        return ddl_gender;
    }

    public void setDdl_gender(String ddl_gender) {
        this.ddl_gender = ddl_gender;
    }

    public String getDdl_marital_status() {
        return ddl_marital_status;
    }

    public void setDdl_marital_status(String ddl_marital_status) {
        this.ddl_marital_status = ddl_marital_status;
    }

//    public String getTxt_emp_init() {
//        return txt_emp_init;
//    }
//
//    public void setTxt_emp_init(String txt_emp_init) {
//        this.txt_emp_init = txt_emp_init;
//    }
    public String getTxt_user_name() {
        return txt_user_name;
    }

    public void setTxt_user_name(String txt_user_name) {
        this.txt_user_name = txt_user_name;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_posting_oum">
    String ddl_posting_oum;
    List<SelectItem> ddl_posting_oum_options;

    public String getDdl_posting_oum() {
        return ddl_posting_oum;
    }

    public void setDdl_posting_oum(String ddl_posting_oum) {
        this.ddl_posting_oum = ddl_posting_oum;
    }

    public List<SelectItem> getDdl_posting_oum_options() {
        return ddl_posting_oum_options;
    }

    public void setDdl_posting_oum_options(List<SelectItem> ddl_posting_oum_options) {
        this.ddl_posting_oum_options = ddl_posting_oum_options;
    }

    public void setDdl_posting_oum() {
        List<SelectItem> lst_Items = new ArrayList<SelectItem>();
        List<SystOrgUnitMst> lstSystOrgUnitMsts = systOrgUnitMstFacade.findPostingOuForNewEmployee(globalData.getWorking_ou());
        if (lstSystOrgUnitMsts.isEmpty()) {
            SelectItem s = new SelectItem(null, "---Not Found---");
            lst_Items.add(s);
        } else {
            SelectItem s = new SelectItem(null, "--- Select ---");
            lst_Items.add(s);
            Integer n = lstSystOrgUnitMsts.size();
            for (int i = 0; i < n; i++) {
                s = new SelectItem(lstSystOrgUnitMsts.get(i).getOumUnitSrno(), lstSystOrgUnitMsts.get(i).getOumName());
                lst_Items.add(s);
            }
        }
        ddl_posting_oum_options = lst_Items;
        ddl_posting_oum = lst_Items.get(0).toString();
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Global ent_FhrdEmpmst"> 
    FhrdEmpmst ent_FhrdEmpmst;

    public FhrdEmpmst getEnt_FhrdEmpmst() {
        return ent_FhrdEmpmst;
    }

    public void setEnt_FhrdEmpmst(FhrdEmpmst ent_FhrdEmpmst) {
        this.ent_FhrdEmpmst = ent_FhrdEmpmst;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="EJB Declarations">    
    @EJB
    private SystCountryMstFacadeLocal systCountryMstFacade;
    @EJB
    private FhrdDegreeStreamMstFacadeLocal fhrdDegreeStreamMstFacade;
    @EJB
    private FhrdReligionMstFacadeLocal fhrdReligionMstFacade;
    @EJB
    private FhrdPortfolioMstFacade fhrdPortfolioMstFacade;
    @EJB
    private FhrdDegreeLevelMstFacadeLocal fhrdDegreeLevelMstFacade;
    @EJB
    private FcadAddrDtlFacadeLocal fcadAddrDtlFacade;
    @EJB
    private FhrdCasteCategorymstFacadeLocal FhrdCasteCategoryMstFacade;
    @EJB
    private FhrdDesigmstFacadeLocal fhrdDesigmstFacade;
    @EJB
    private FhrdEmpEventDtlFacadeLocal fhrdEmpEventDtlFacade;
    @EJB
    private FhrdDependentFacadeLocal fhrdDependentFacade;
    @EJB
    private FhrdQualificationFacadeLocal fhrdQualificationFacade;
    @EJB
    private FhrdExperienceFacadeLocal fhrdExperienceFacade;
    @EJB
    private customJpaController customJpaController;
    @EJB
    private FhrdEventMstFacadeLocal fhrdEventMstFacade;
    @EJB
    private SystOrgUnitMstFacadeLocal systOrgUnitMstFacade;
    @EJB
    private FhrdUniversitymstFacadeLocal fhrdUniversitymstFacade;
    @EJB
    private FhrdDegreemstFacadeLocal fhrdDegreemstFacade;
    @EJB
    private FhrdEmpmstFacadeLocal fhrdEmpmstFacade;
    @EJB
    private SystUserTypeMstFacadeLocal systUserTypeMstFacade;
    @EJB
    private FcadAddrMstFacadeLocal fcadAddrMstFacade;
    @EJB
    private FhrdDegreedtlFacadeLocal fhrdDegreedtlFacade;
    //</editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="Common Details">
    //<editor-fold defaultstate="collapsed" desc="address attributes">
    String txt_addr1;
    String txt_addr2;
    String txt_addr3;
    String txt_city;
    String txt_state;
    String txt_country;
    String txt_pincode;
    String txt_email;
    String txt_email_alt;
    String txt_website;
    String txt_present_addr1;
    String txt_present_addr2;
    String txt_present_addr3;
    String txt_present_city;
    String txt_present_state;
    String txt_present_country;
    String txt_present_pincode;
    String txt_permanent_addr1;
    String txt_permanent_addr2;
    String txt_permanent_addr3;
    String txt_permanent_city;
    String txt_permanent_state;
    String txt_permanent_country;
    String txt_permanent_pincode;

    public String getTxt_permanent_addr1() {
        return txt_permanent_addr1;
    }

    public void setTxt_permanent_addr1(String txt_permanent_addr1) {
        this.txt_permanent_addr1 = txt_permanent_addr1;
    }

    public String getTxt_permanent_addr2() {
        return txt_permanent_addr2;
    }

    public void setTxt_permanent_addr2(String txt_permanent_addr2) {
        this.txt_permanent_addr2 = txt_permanent_addr2;
    }

    public String getTxt_permanent_addr3() {
        return txt_permanent_addr3;
    }

    public void setTxt_permanent_addr3(String txt_permanent_addr3) {
        this.txt_permanent_addr3 = txt_permanent_addr3;
    }

    public String getTxt_present_city() {
        return txt_present_city;
    }

    public void setTxt_present_city(String txt_present_city) {
        this.txt_present_city = txt_present_city;
    }

    public String getTxt_permanent_city() {
        return txt_permanent_city;
    }

    public void setTxt_permanent_city(String txt_permanent_city) {
        this.txt_permanent_city = txt_permanent_city;
    }

    public String getTxt_permanent_country() {
        return txt_permanent_country;
    }

    public void setTxt_permanent_country(String txt_permanent_country) {
        this.txt_permanent_country = txt_permanent_country;
    }

    public String getTxt_permanent_pincode() {
        return txt_permanent_pincode;
    }

    public void setTxt_permanent_pincode(String txt_permanent_pincode) {
        this.txt_permanent_pincode = txt_permanent_pincode;
    }

    public String getTxt_permanent_state() {
        return txt_permanent_state;
    }

    public void setTxt_permanent_state(String txt_permanent_state) {
        this.txt_permanent_state = txt_permanent_state;
    }

    public String getTxt_present_addr1() {
        return txt_present_addr1;
    }

    public void setTxt_present_addr1(String txt_present_addr1) {
        this.txt_present_addr1 = txt_present_addr1;
    }

    public String getTxt_present_addr2() {
        return txt_present_addr2;
    }

    public void setTxt_present_addr2(String txt_present_addr2) {
        this.txt_present_addr2 = txt_present_addr2;
    }

    public String getTxt_present_addr3() {
        return txt_present_addr3;
    }

    public void setTxt_present_addr3(String txt_present_addr3) {
        this.txt_present_addr3 = txt_present_addr3;
    }

    public String getTxt_present_country() {
        return txt_present_country;
    }

    public void setTxt_present_country(String txt_present_country) {
        this.txt_present_country = txt_present_country;
    }

    public String getTxt_present_pincode() {
        return txt_present_pincode;
    }

    public void setTxt_present_pincode(String txt_present_pincode) {
        this.txt_present_pincode = txt_present_pincode;
    }

    public String getTxt_present_state() {
        return txt_present_state;
    }

    public void setTxt_present_state(String txt_present_state) {
        this.txt_present_state = txt_present_state;
    }

    public String getTxt_addr1() {
        return txt_addr1;
    }

    public void setTxt_addr1(String txt_addr1) {
        this.txt_addr1 = txt_addr1;
    }

    public String getTxt_addr2() {
        return txt_addr2;
    }

    public void setTxt_addr2(String txt_addr2) {
        this.txt_addr2 = txt_addr2;
    }

    public String getTxt_addr3() {
        return txt_addr3;
    }

    public void setTxt_addr3(String txt_addr3) {
        this.txt_addr3 = txt_addr3;
    }

    public String getTxt_city() {
        return txt_city;
    }

    public void setTxt_city(String txt_city) {
        this.txt_city = txt_city;
    }

    public String getTxt_country() {
        return txt_country;
    }

    public void setTxt_country(String txt_country) {
        this.txt_country = txt_country;
    }

    public String getTxt_email() {
        return txt_email;
    }

    public void setTxt_email(String txt_email) {
        this.txt_email = txt_email;
    }

    public String getTxt_email_alt() {
        return txt_email_alt;
    }

    public void setTxt_email_alt(String txt_email_alt) {
        this.txt_email_alt = txt_email_alt;
    }

    public String getTxt_pincode() {
        return txt_pincode;
    }

    public void setTxt_pincode(String txt_pincode) {
        this.txt_pincode = txt_pincode;
    }

    public String getTxt_state() {
        return txt_state;
    }

    public void setTxt_state(String txt_state) {
        this.txt_state = txt_state;
    }

    public String getTxt_website() {
        return txt_website;
    }

    public void setTxt_website(String txt_website) {
        this.txt_website = txt_website;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_user_type">
    List<SelectItem> ddl_user_type_option;
    String ddl_user_type;

    public String getDdl_user_type() {
        return ddl_user_type;
    }

    public void setDdl_user_type(String ddl_user_type) {
        this.ddl_user_type = ddl_user_type;
    }

    public List<SelectItem> getDdl_user_type_option() {
        return ddl_user_type_option;
    }

    public void setDdl_user_type_option(List<SelectItem> ddl_user_type_option) {
        this.ddl_user_type_option = ddl_user_type_option;
    }

    public void setDdl_user_type() {
        ddl_user_type_option = new ArrayList<SelectItem>();
        List<SystUserTypeMst> lstSystUserTypeMsts = systUserTypeMstFacade.findAll(globalData.getWorking_ou(), true);
        if (lstSystUserTypeMsts.isEmpty()) {
            SelectItem s = new SelectItem(null, "--- Not Available ---");
            ddl_user_type_option.add(s);
        } else {
            SelectItem s = new SelectItem(null, "--- Select ---");
            ddl_user_type_option.add(s);
            Integer n = lstSystUserTypeMsts.size();
            for (int i = 0; i < n; i++) {
                s = new SelectItem(lstSystUserTypeMsts.get(i).getSystUserTypeMstPK().getUserTypeSrno(), lstSystUserTypeMsts.get(i).getUserTypeDesc());
                ddl_user_type_option.add(s);
            }
        }
        ddl_user_type = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_designation">
    String ddl_designation;
    List<SelectItem> ddl_designation_options;

    public String getDdl_designation() {
        return ddl_designation;
    }

    public void setDdl_designation(String ddl_designation) {
        this.ddl_designation = ddl_designation;
    }

    public List<SelectItem> getDdl_designation_options() {
        return ddl_designation_options;
    }

    public void setDdl_designation_options(List<SelectItem> ddl_designation_options) {
        this.ddl_designation_options = ddl_designation_options;
    }

    public void setDdl_designation() {
        ddl_designation_options = new ArrayList<SelectItem>();
        List<FhrdDesigmst> lstDesigmsts = fhrdDesigmstFacade.findAllDesignation(globalData.getWorking_ou());
        if (lstDesigmsts.isEmpty()) {
            SelectItem s = new SelectItem(null, "--- Not Available ---");
            ddl_designation_options.add(s);
        } else {
            SelectItem s = new SelectItem(null, "--- Select ---");
            ddl_designation_options.add(s);
            Integer n = lstDesigmsts.size();
            for (int i = 0; i < n; i++) {
                s = new SelectItem(lstDesigmsts.get(i).getDsgmSrgKey(), lstDesigmsts.get(i).getDesigDesc());
                ddl_designation_options.add(s);
            }
        }
        ddl_designation = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_reporting_user">
    String ddl_reporting_user;
    List<SelectItem> ddl_reporting_user_options;

    public String getDdl_reporting_user() {
        return ddl_reporting_user;
    }

    public void setDdl_reporting_user(String ddl_reporting_user) {
        this.ddl_reporting_user = ddl_reporting_user;
    }

    public List<SelectItem> getDdl_reporting_user_options() {
        return ddl_reporting_user_options;
    }

    public void setDdl_reporting_user_options(List<SelectItem> ddl_reporting_user_options) {
        this.ddl_reporting_user_options = ddl_reporting_user_options;
    }

    public void setDdl_reporting_user() {
        ddl_reporting_user_options = new ArrayList<SelectItem>();
        List<FhrdEmpmst> lstEmpmsts = fhrdEmpmstFacade.findAll(globalData.getWorking_ou(), false, false, true, null, null, null, false, false, false, null);
        if (lstEmpmsts.isEmpty()) {
            SelectItem s = new SelectItem(null, "--- Not Available ---");
            ddl_reporting_user_options.add(s);
        } else {
            SelectItem s = new SelectItem(null, "--- Select ---");
            ddl_reporting_user_options.add(s);
            Integer n = lstEmpmsts.size();
            for (int i = 0; i < n; i++) {
                s = new SelectItem(lstEmpmsts.get(i).getUserId(), lstEmpmsts.get(i).getUserName());
                ddl_reporting_user_options.add(s);
            }
        }
        ddl_reporting_user = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_category">
    String ddl_category;
    List<SelectItem> ddl_category_options;

    public String getDdl_category() {
        return ddl_category;
    }

    public void setDdl_category(String ddl_category) {
        this.ddl_category = ddl_category;
    }

    public List<SelectItem> getDdl_category_options() {
        return ddl_category_options;
    }

    public void setDdl_category_options(List<SelectItem> ddl_category_options) {
        this.ddl_category_options = ddl_category_options;
    }

    public void setDdl_category() {
        ddl_category_options = new ArrayList<SelectItem>();
        List<FhrdCasteCategoryMst> lstCategorymsts = FhrdCasteCategoryMstFacade.findAll(globalData.getWorking_ou());
        if (lstCategorymsts.isEmpty()) {
            SelectItem s = new SelectItem(null, "--- Not Availalbe ---");
            ddl_category_options.add(s);
        } else {
            SelectItem s = new SelectItem(null, "--- Select ---");
            ddl_category_options.add(s);
            Integer n = lstCategorymsts.size();
            for (int i = 0; i < n; i++) {
                s = new SelectItem(lstCategorymsts.get(i).getCcmSrgKey(), lstCategorymsts.get(i).getShortname());
                ddl_category_options.add(s);
            }
        }
        ddl_category = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_physically_challenged">
    String ddl_physically_challenged;

    public String getDdl_physically_challenged() {
        return ddl_physically_challenged;
    }

    public void setDdl_physically_challenged(String ddl_physically_challenged) {
        this.ddl_physically_challenged = ddl_physically_challenged;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_religion">
    String ddl_religion;
    List<SelectItem> ddl_religion_options;

    public String getDdl_religion() {
        return ddl_religion;
    }

    public void setDdl_religion(String ddl_religion) {
        this.ddl_religion = ddl_religion;
    }

    public List<SelectItem> getDdl_religion_options() {
        return ddl_religion_options;
    }

    public void setDdl_religion_options(List<SelectItem> ddl_religion_options) {
        this.ddl_religion_options = ddl_religion_options;
    }

    public void setDdl_religion() {
        ddl_religion_options = new ArrayList<SelectItem>();
        List<FhrdReligionMst> lsFhrdReligionMsts = fhrdReligionMstFacade.findAll(globalData.getWorking_ou());
        if (lsFhrdReligionMsts.isEmpty()) {
            SelectItem s = new SelectItem(null, "--- Not Availalbe ---");
            ddl_religion_options.add(s);
        } else {
            SelectItem s = new SelectItem(null, "--- Select ---");
            ddl_religion_options.add(s);
            Integer n = lsFhrdReligionMsts.size();
            for (int i = 0; i < n; i++) {
                s = new SelectItem(lsFhrdReligionMsts.get(i).getRgSrgKey(), lsFhrdReligionMsts.get(i).getRgDescription());
                ddl_religion_options.add(s);
            }
        }
        ddl_religion = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_responsibility">
    String ddl_responsibility;
    List<SelectItem> ddl_responsibility_options;

    public String getDdl_responsibility() {
        return ddl_responsibility;
    }

    public void setDdl_responsibility(String ddl_responsibility) {
        this.ddl_responsibility = ddl_responsibility;
    }

    public List<SelectItem> getDdl_responsibility_options() {
        return ddl_responsibility_options;
    }

    public void setDdl_responsibility_options(List<SelectItem> ddl_responsibility_options) {
        this.ddl_responsibility_options = ddl_responsibility_options;
    }

    private void setDdl_responsibility() {
        ddl_responsibility_options = new ArrayList<SelectItem>();
        List<FhrdPortfolioMst> lst_temp = fhrdPortfolioMstFacade.findAll(globalData.getWorking_ou(), true);
        ddl_responsibility_options.add(new SelectItem(null, "--- Select ---"));
        for (FhrdPortfolioMst e : lst_temp) {
            ddl_responsibility_options.add(new SelectItem(e.getPmUniqueSrno().toString(), e.getPmName()));
        }
        ddl_responsibility = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_comment">
    String txt_comment;

    public String getTxt_comment() {
        return txt_comment;
    }

    public void setTxt_comment(String txt_comment) {
        this.txt_comment = txt_comment;
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Qualification"> 
    //<editor-fold defaultstate="collapsed" desc="auto_college">
    String auto_college;
    List<String> collegeList;

    public List<String> getCollegeList() {
        return collegeList;
    }

    public void setCollegeList(List<String> collegeList) {
        this.collegeList = collegeList;
    }

    public String getAuto_college() {
        return auto_college;
    }

    public void setAuto_college(String auto_college) {
        this.auto_college = auto_college;
    }

    private void setAuto_college() {
        collegeList = fhrdQualificationFacade.findAllColleges(globalData.getWorking_ou());
    }

    public List<String> completeCollegeList(String query) {
        List<String> auto_college_options = new ArrayList<String>();
        for (String s : collegeList) {
            if (s.contains(query.toUpperCase())) {
                auto_college_options.add(s);
            }
        }
        return auto_college_options;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="getter/setter methods"> 
    String txt_grade;
    String txt_percentage;
    String txt_remarks;
    List<FhrdQualification> tbl_qualification;

    public List<FhrdQualification> getTbl_qualification() {
        return tbl_qualification;
    }

    public void setTbl_qualification(List<FhrdQualification> tbl_qualification) {
        this.tbl_qualification = tbl_qualification;
    }

    public String getTxt_grade() {
        return txt_grade;
    }

    public void setTxt_grade(String txt_grade) {
        this.txt_grade = txt_grade;
    }

    public String getTxt_percentage() {
        return txt_percentage;
    }

    public void setTxt_percentage(String txt_percentage) {
        this.txt_percentage = txt_percentage;
    }

    public String getTxt_remarks() {
        return txt_remarks;
    }

    public void setTxt_remarks(String txt_remarks) {
        this.txt_remarks = txt_remarks;
    }
    //</editor-fold>        
    //<editor-fold defaultstate="collapsed" desc="ddl_degree">
    String ddl_degree;
    List<SelectItem> ddl_degree_options;

    public String getDdl_degree() {
        return ddl_degree;
    }

    public void setDdl_degree(String ddl_degree) {
        this.ddl_degree = ddl_degree;
    }

    public void setDdl_degree() {
        ddl_degree_options = new ArrayList<SelectItem>();
        List<FhrdDegreemst> lstDegreemsts = fhrdDegreemstFacade.findAll(globalData.getWorking_ou());
        if (lstDegreemsts.isEmpty()) {
            SelectItem s = new SelectItem(null, "--- Not Available ---");
            ddl_degree_options.add(s);
        } else {
            SelectItem s = new SelectItem(null, "--- Select ---");
            ddl_degree_options.add(s);
            Integer n = lstDegreemsts.size();
            for (int i = 0; i < n; i++) {
                s = new SelectItem(lstDegreemsts.get(i).getDgrmSrgKey(), lstDegreemsts.get(i).getDegreecd() + " -- " + lstDegreemsts.get(i).getDegreeName());
                ddl_degree_options.add(s);
            }
        }
        ddl_degree = null;
    }

    public List<SelectItem> getDdl_degree_options() {
        return ddl_degree_options;
    }

    public void setDdl_degree_options(List<SelectItem> ddl_degree_options) {
        this.ddl_degree_options = ddl_degree_options;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_degree_change">

    public void ddl_degree_change(ValueChangeEvent event) {
        if (event.getNewValue() == null) {
            ddl_degree = null;
        } else {
            ddl_degree = event.getNewValue().toString();
        }
        setDdl_subject();

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_subject">
    String ddl_subject;
    List<SelectItem> ddl_subject_options;

    public String getDdl_subject() {
        return ddl_subject;
    }

    public void setDdl_subject(String ddl_subject) {
        this.ddl_subject = ddl_subject;
    }

    public List<SelectItem> getDdl_subject_options() {
        return ddl_subject_options;
    }

    public void setDdl_subject_options(List<SelectItem> ddl_subject_options) {
        this.ddl_subject_options = ddl_subject_options;
    }

    public void setDdl_subject() {
        ddl_subject_options = new ArrayList<SelectItem>();
        if (ddl_degree == null) {
            SelectItem s = new SelectItem(null, "--- Select Degree First ---");
            ddl_subject_options.add(s);
            ddl_subject = null;
        } else {
            List<FhrdDegreedtl> lst_subjects = fhrdDegreedtlFacade.findSubjectList(new BigDecimal(ddl_degree), globalData.getOrganisation().getOumUnitSrno());
            if (lst_subjects.isEmpty()) {
                SelectItem s = new SelectItem(null, "--- Not Available ---");
                ddl_subject_options.add(s);
                ddl_subject = null;
            } else {
                SelectItem s = new SelectItem(null, "--- Select ---");
                ddl_subject_options.add(s);
                Integer n = lst_subjects.size();
                for (int i = 0; i < n; i++) {
                    s = new SelectItem(lst_subjects.get(i).getFhrdDegreedtlPK().getSubjectSrno().toString(), lst_subjects.get(i).getSubjectName());
                    ddl_subject_options.add(s);
                }
                ddl_subject = ddl_subject_options.get(0).toString();
            }
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_stream">
    String ddl_stream;
    List<SelectItem> ddl_stream_options;

    public String getDdl_stream() {
        return ddl_stream;
    }

    public void setDdl_stream(String ddl_stream) {
        this.ddl_stream = ddl_stream;
    }

    public List<SelectItem> getDdl_stream_options() {
        return ddl_stream_options;
    }

    public void setDdl_stream_options(List<SelectItem> ddl_stream_options) {
        this.ddl_stream_options = ddl_stream_options;
    }

    private void setDdl_stream() {
        ddl_stream_options = new ArrayList<SelectItem>();
        ddl_stream_options.add(new SelectItem(null, "--- Select ---"));
        List<FhrdDegreeStreamMst> lst_DegreeStreamMsts = fhrdDegreeStreamMstFacade.findAll(globalData.getWorking_ou());
        for (FhrdDegreeStreamMst s : lst_DegreeStreamMsts) {
            ddl_stream_options.add(new SelectItem(s.getDsmSrgKey().toString(), s.getDsmDescription()));
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_university">
    String ddl_university;
    List<SelectItem> ddl_university_options;

    public String getDdl_university() {
        return ddl_university;
    }

    public void setDdl_university(String ddl_university) {
        this.ddl_university = ddl_university;
    }

    public void setDdl_university() {
        List<SelectItem> lst_Items = new ArrayList<SelectItem>();
        List<FhrdUniversitymst> lstUniversitymsts = fhrdUniversitymstFacade.findAll(globalData.getWorking_ou());
        if (lstUniversitymsts.isEmpty()) {
            SelectItem s = new SelectItem(null, "--- Not Available ---");
            lst_Items.add(s);
        } else {
            SelectItem s = new SelectItem(null, "--- Select ---");
            lst_Items.add(s);
            Integer n = lstUniversitymsts.size();
            for (int i = 0; i < n; i++) {
                s = new SelectItem(lstUniversitymsts.get(i).getUmSrgKey(), lstUniversitymsts.get(i).getUniversitycd() + "--" + lstUniversitymsts.get(i).getUniversityName());
                lst_Items.add(s);
            }
        }
        ddl_university_options = lst_Items;
        ddl_university = lst_Items.get(0).toString();
    }

    public List<SelectItem> getDdl_university_options() {
        return ddl_university_options;
    }

    public void setDdl_university_options(List<SelectItem> ddl_university_options) {
        this.ddl_university_options = ddl_university_options;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_pass_month">
    String ddl_pass_month;
    List<SelectItem> ddl_pass_month_options;

    public String getDdl_pass_month() {
        return ddl_pass_month;
    }

    public void setDdl_pass_month(String ddl_pass_month) {
        this.ddl_pass_month = ddl_pass_month;
    }

    public List<SelectItem> getDdl_pass_month_options() {
        return ddl_pass_month_options;
    }

    public void setDdl_pass_month_options(List<SelectItem> ddl_pass_month_options) {
        this.ddl_pass_month_options = ddl_pass_month_options;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_pass_year"> 
    String ddl_pass_year;
    List<SelectItem> ddl_pass_year_options;

    public String getDdl_pass_year() {
        return ddl_pass_year;
    }

    public void setDdl_pass_year(String ddl_pass_year) {
        this.ddl_pass_year = ddl_pass_year;
    }

    public List<SelectItem> getDdl_pass_year_options() {
        return ddl_pass_year_options;
    }

    public void setDdl_pass_year_options(List<SelectItem> ddl_pass_year_options) {
        this.ddl_pass_year_options = ddl_pass_year_options;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_add_qualification_click"> 

    public void btn_add_qualification_click(ActionEvent event) {
        if (tbl_qualification == null) {
            tbl_qualification = new ArrayList<FhrdQualification>();
        }
        FhrdQualification entFhrdQualification = new FhrdQualification();
        entFhrdQualification.setFhrdQualificationPK(new FhrdQualificationPK(0, tbl_qualification.size()));
        if (ddl_degree != null && ddl_subject != null) {
            entFhrdQualification.setFhrdDegreedtl(fhrdDegreedtlFacade.find(new FhrdDegreedtlPK(new BigDecimal(ddl_subject), new BigDecimal(ddl_degree))));
        }
        if (ddl_degree != null) {
            if (entFhrdQualification.getFhrdDegreedtl() != null) {
                entFhrdQualification.setDgrmSrgKey(entFhrdQualification.getFhrdDegreedtl().getFhrdDegreemst());
            } else {
                entFhrdQualification.setDgrmSrgKey(fhrdDegreemstFacade.find(new BigDecimal(ddl_degree)));
            }
        }
        if (ddl_stream != null) {
            entFhrdQualification.setDsmSrgKey(fhrdDegreeStreamMstFacade.find(new Integer(ddl_stream)));

        }
        if (ddl_university != null) {
            entFhrdQualification.setUmSrgKey(fhrdUniversitymstFacade.find(new BigDecimal(ddl_university)));
        }
        if (auto_college != null) {
            entFhrdQualification.setCollegeName(auto_college.toUpperCase());
        }

        if (ddl_pass_month != null) {
            entFhrdQualification.setPassMonth(ddl_pass_month.toUpperCase());
        }
        entFhrdQualification.setPassYear(new Short(ddl_pass_year));
        if (!txt_grade.trim().isEmpty()) {
            entFhrdQualification.setGrade(txt_grade.toUpperCase());
        }
        if (!txt_percentage.trim().isEmpty()) {
            entFhrdQualification.setPercentage(new BigDecimal(txt_percentage));
        }
        if (!txt_remarks.trim().isEmpty()) {
            entFhrdQualification.setRemarks(txt_remarks.toUpperCase());
        }
        tbl_qualification.add(entFhrdQualification);
        resetQualification();
    }

    //<editor-fold defaultstate="collapsed" desc="setDdl_year_value()">
    public void setDdl_year_value() {
        ddl_pass_year_options = new ArrayList<SelectItem>();
        ddl_pass_year_options.add(new SelectItem(null, "--- Select ---"));
        for (int i = 1960; i <= (new Date().getYear() + 1900); i++) {
            ddl_pass_year_options.add(new SelectItem(i, String.valueOf(i)));
        }
        ddl_pass_year = null;
    }
    //</editor-fold>    
    //<editor-fold defaultstate="collapsed" desc="setDdl_pass_month_value">

    public void setDdl_pass_month_value() {
        List<SelectItem> lst_Items = new ArrayList<SelectItem>();
        SelectItem s = new SelectItem(null, "--- Select ---");
        lst_Items.add(s);
        s = new SelectItem("JAN", "January");
        lst_Items.add(s);
        s = new SelectItem("FEB", "February");
        lst_Items.add(s);
        s = new SelectItem("MAR", "March");
        lst_Items.add(s);
        s = new SelectItem("APR", "April");
        lst_Items.add(s);
        s = new SelectItem("MAY", "May");
        lst_Items.add(s);
        s = new SelectItem("JUN", "June");
        lst_Items.add(s);
        s = new SelectItem("JUL", "July");
        lst_Items.add(s);
        s = new SelectItem("AUG", "August");
        lst_Items.add(s);
        s = new SelectItem("SEP", "September");
        lst_Items.add(s);
        s = new SelectItem("OCT", "October");
        lst_Items.add(s);
        s = new SelectItem("NOV", "November");
        lst_Items.add(s);
        s = new SelectItem("DEC", "December");
        lst_Items.add(s);
        ddl_pass_month_options = lst_Items;
        ddl_pass_month = lst_Items.get(0).toString();
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ent_QualDel">
    FhrdQualification ent_QualDel = new FhrdQualification();

    public FhrdQualification getEnt_QualDel() {
        return ent_QualDel;
    }

    public void setEnt_QualDel(FhrdQualification ent_QualDel) {
        tbl_qualification.remove(ent_QualDel);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetQualification"> 

    public void resetQualification() {
        auto_college = "";
        txt_grade = "";
        txt_percentage = "";
        txt_remarks = "";
        ddl_degree = "";
        ddl_subject = null;
        List<SelectItem> lst_Items = new ArrayList<SelectItem>();
        SelectItem s = new SelectItem(null, "---No Subjects are available---");
        lst_Items.add(s);
        ddl_subject_options = lst_Items;
        ddl_subject = lst_Items.get(0).toString();
        ddl_university = "";
        ddl_pass_month = "";
        ddl_pass_year = "";
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Experience"> 
    //<editor-fold defaultstate="collapsed" desc="Other Objects">
    String txt_gross_pay;
    String txt_exp_years;
    String txt_perks;
    String txt_exp_remarks;
    Date cal_exp_from_date;
    Date cal_exp_to_date;
    //<editor-fold defaultstate="collapsed" desc="getter/setter">

    public Date getCal_exp_from_date() {
        return cal_exp_from_date;
    }

    public void setCal_exp_from_date(Date cal_exp_from_date) {
        this.cal_exp_from_date = cal_exp_from_date;
    }

    public Date getCal_exp_to_date() {
        return cal_exp_to_date;
    }

    public void setCal_exp_to_date(Date cal_exp_to_date) {
        this.cal_exp_to_date = cal_exp_to_date;
    }

    public String getTxt_exp_remarks() {
        return txt_exp_remarks;
    }

    public void setTxt_exp_remarks(String txt_exp_remarks) {
        this.txt_exp_remarks = txt_exp_remarks;
    }

    public String getTxt_perks() {
        return txt_perks;
    }

    public void setTxt_perks(String txt_perks) {
        this.txt_perks = txt_perks;
    }

    public String getTxt_exp_years() {
        return txt_exp_years;
    }

    public void setTxt_exp_years(String txt_exp_years) {
        this.txt_exp_years = txt_exp_years;
    }

    public String getTxt_gross_pay() {
        return txt_gross_pay;
    }

    public void setTxt_gross_pay(String txt_gross_pay) {
        this.txt_gross_pay = txt_gross_pay;
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="auto_company">
    String auto_company;
    List<String> companyList;

    public List<String> getCompanyList() {
        return companyList;
    }

    public void setCompanyList(List<String> companyList) {
        this.companyList = companyList;
    }

    public String getAuto_company() {
        return auto_company;
    }

    public void setAuto_company(String auto_company) {
        this.auto_company = auto_company;
    }

    private void setAuto_company() {
        companyList = fhrdExperienceFacade.findAllCompanies(globalData.getWorking_ou());
    }

    public List<String> completeCompanyList(String query) {
        List<String> auto_company_options = new ArrayList<String>();
        for (String s : companyList) {
            if (s.contains(query.toUpperCase())) {
                auto_company_options.add(s);
            }
        }
        return auto_company_options;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="auto_designation">
    String auto_designation;
    List<String> designationList;

    public List<String> getDesignationList() {
        return designationList;
    }

    public void setDesignationList(List<String> designationList) {
        this.designationList = designationList;
    }

    public String getAuto_designation() {
        return auto_designation;
    }

    public void setAuto_designation(String auto_designation) {
        this.auto_designation = auto_designation;
    }

    private void setAuto_designation() {
        designationList = fhrdExperienceFacade.findAllDesignations(globalData.getWorking_ou());
    }

    public List<String> completeDesignationList(String query) {
        List<String> auto_designation_options = new ArrayList<String>();
        for (String s : designationList) {
            if (s.contains(query.toUpperCase())) {
                auto_designation_options.add(s);
            }
        }
        return auto_designation_options;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_experience">
    List<FhrdExperience> tbl_experience;

    public List<FhrdExperience> getTbl_experience() {
        return tbl_experience;
    }

    public void setTbl_experience(List<FhrdExperience> tbl_experience) {
        this.tbl_experience = tbl_experience;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ent_ExpDel">
    FhrdExperience ent_ExpDel = new FhrdExperience();

    public FhrdExperience getEnt_ExpDel() {
        return ent_ExpDel;
    }

    public void setEnt_ExpDel(FhrdExperience ent_ExpDel) {
        tbl_experience.remove(ent_ExpDel);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_add_experience_click">

    public void btn_add_experience_click(ActionEvent event) {
        if (tbl_experience == null) {
            tbl_experience = new ArrayList<FhrdExperience>();
        }

        FhrdExperience entFhrdExperience = new FhrdExperience();

        entFhrdExperience.setFhrdExperiencePK(new FhrdExperiencePK(0, new Integer("-" + (tbl_experience.size() + 1))));

        // if (!txt_company_name.trim().isEmpty()) {
        entFhrdExperience.setCompanyname(auto_company.toUpperCase());
        // }
        //if (!txt_designation.trim().isEmpty()) {
        entFhrdExperience.setDesignation(auto_designation.toString().toUpperCase());
        // }
        // if (cal_exp_from_date != null) {
        entFhrdExperience.setFromdate(cal_exp_from_date);
        //  }
        //  if (cal_exp_to_date != null) {
        entFhrdExperience.setTodate(cal_exp_to_date);
        // }
        if (!txt_gross_pay.trim().isEmpty()) {
            entFhrdExperience.setGrosspay(new Long(txt_gross_pay));
        }
        if (!txt_exp_years.trim().isEmpty()) {
            entFhrdExperience.setYears(new BigDecimal(txt_exp_years));
        }
        if (!txt_exp_remarks.trim().isEmpty()) {
            entFhrdExperience.setRemarks(txt_exp_remarks.toString().toUpperCase());
        }
        if (!txt_perks.trim().isEmpty()) {
            entFhrdExperience.setPerks(txt_perks.toUpperCase());
        }
        tbl_experience.add(entFhrdExperience);
        resetExperience();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="resetExperience">

    public void resetExperience() {
        auto_company = null;
        auto_designation = "";
        txt_gross_pay = "";
        txt_perks = "";
        txt_exp_remarks = "";
        txt_exp_years = "";
        cal_exp_from_date = null;
        cal_exp_to_date = null;
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Dependents"> 
    //<editor-fold defaultstate="collapsed" desc="auto_occupation">
    String auto_occupation;
    List<String> occupationList;

    public List<String> getOccupationList() {
        return occupationList;
    }

    public void setOccupationList(List<String> occupationList) {
        this.occupationList = occupationList;
    }

    public String getAuto_occupation() {
        return auto_occupation;
    }

    public void setAuto_occupation(String auto_occupation) {
        this.auto_occupation = auto_occupation;
    }

    private void setAuto_occupation() {
        occupationList = fhrdDependentFacade.findAllOccupation(globalData.getWorking_ou());
    }

    public List<String> completeOccupationList(String query) {
        List<String> auto_occupation_options = new ArrayList<String>();
        for (String s : occupationList) {
            if (s.contains(query.toUpperCase())) {
                auto_occupation_options.add(s);
            }
        }
        return auto_occupation_options;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Object">
    String txt_dependent_name;
    String ddl_dependent_relation;
    String ddl_dependent_gender;
    Date cal_dependent_birthdate;

    public Date getCal_dependent_birthdate() {
        return cal_dependent_birthdate;
    }

    public void setCal_dependent_birthdate(Date cal_dependent_birthdate) {
        this.cal_dependent_birthdate = cal_dependent_birthdate;
    }

    public String getDdl_dependent_gender() {
        return ddl_dependent_gender;
    }

    public void setDdl_dependent_gender(String ddl_dependent_gender) {
        this.ddl_dependent_gender = ddl_dependent_gender;
    }

    public String getTxt_dependent_name() {
        return txt_dependent_name;
    }

    public void setTxt_dependent_name(String txt_dependent_name) {
        this.txt_dependent_name = txt_dependent_name;
    }

    public String getDdl_dependent_relation() {
        return ddl_dependent_relation;
    }

    public void setDdl_dependent_relation(String ddl_dependent_relation) {
        this.ddl_dependent_relation = ddl_dependent_relation;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="tbl_dependents">
    List<FhrdDependent> tbl_dependents;

    public List<FhrdDependent> getTbl_dependents() {
        return tbl_dependents;
    }

    public void setTbl_dependents(List<FhrdDependent> tbl_dependents) {
        this.tbl_dependents = tbl_dependents;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ent_DepDet">
    FhrdDependent ent_DepDet = new FhrdDependent();

    public FhrdDependent getEnt_DepDet() {
        return ent_DepDet;
    }

    public void setEnt_DepDet(FhrdDependent ent_DepDet) {
        tbl_dependents.remove(ent_DepDet);
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_add_dependent_click"> 

    public void btn_add_dependent_click(ActionEvent event) {
        if (tbl_dependents == null) {
            tbl_dependents = new ArrayList<FhrdDependent>();
        }
        FhrdDependent entDependent = new FhrdDependent();
        entDependent.setFhrdDependentPK(new FhrdDependentPK(0, new Integer("-" + String.valueOf(tbl_dependents.size() + 1))));
        entDependent.setBirthdt(cal_dependent_birthdate);
        if (ddl_dependent_gender != null) {
            entDependent.setGender(ddl_dependent_gender);
        }
        if (!txt_dependent_name.trim().isEmpty()) {
            entDependent.setName(txt_dependent_name.toUpperCase());
        }
        if (!auto_occupation.trim().isEmpty()) {
            entDependent.setOccupation(auto_occupation.toString().toUpperCase());
        } else {
            entDependent.setOccupation("-");
        }
        if (!ddl_dependent_relation.trim().isEmpty()) {
            entDependent.setRelation(ddl_dependent_relation.toString().toUpperCase());
        }
        tbl_dependents.add(entDependent);
        resetDependents();
    }

    public void resetDependents() {
        txt_dependent_name = null;
        auto_occupation = null;
        ddl_dependent_relation = null;
        ddl_dependent_gender = null;
        cal_dependent_birthdate = null;
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_set_verification_remark_clicked"> 
//    public void btn_set_verification_remark_clicked(ActionEvent event) {
//        FacesContext context = FacesContext.getCurrentInstance();
//        try {
//            ent_FhrdEmpmst = fhrdEmpmstFacade.find(ent_FhrdEmpmst.getUserId());
//            ent_FhrdEmpmst.setUpdateby(globalData.getUser_id());
//            ent_FhrdEmpmst.setUpdatedt(new Date());
//            ent_FhrdEmpmst.setVerificationRemark(txt_verification_remark);
//            fhrdEmpmstFacade.edit(ent_FhrdEmpmst);
//            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Verification Remark saved Successfully."));
//            resetForm();
//        } catch (ConstraintViolationException cex) {
//            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Constraint violation error occurred."));
//            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_set_verification_remark_clicked", null, cex);
//        } catch (EJBException eex) {
//            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "EJB error occurred."));
//            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_set_verification_remark_clicked", null, eex);
//        } catch (Exception e) {
//            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred."));
//            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_set_verification_remark_clicked", null, e);
//        }
//        
//    }
    //</editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="btn_verify_detail_clicked">

    public void btn_verify_detail_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            ent_FhrdEmpmst = fhrdEmpmstFacade.find(ent_FhrdEmpmst.getUserId());
            ent_FhrdEmpmst.setUpdateby(globalData.getUser_id());
            ent_FhrdEmpmst.setUpdatedt(new Date());
            ent_FhrdEmpmst.setVerification("V");
            try {
                fhrdEmpmstFacade.edit(ent_FhrdEmpmst);
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Employee Detail Verified Successfully"));
                forVerification = false;
                RequestContext.getCurrentInstance().execute("updateParent()");
            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred during verifying Employee Detail."));
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_verify_detail_clicked", null, e);
            }

        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred."));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_verify_detail_clicked", null, e);
        }

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="txt_updation_remark">
    String txt_updation_remark;

    public String getTxt_updation_remark() {
        return txt_updation_remark;
    }

    public void setTxt_updation_remark(String txt_updation_remark) {
        this.txt_updation_remark = txt_updation_remark;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn Save Employee Click"> 

    public void btn_add_new_emp_click(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            setEmpDetail();
            setQualifications();
            setEmpExperience();
            setEmpDependent();
            FcadAddrDtl entAddrDtl = setAddressDetail();
            try {
                if (createMode) {
                    setEmpEventDetail();
                    //if (ddl_user_type.equalsIgnoreCase("1")) {

                    //}
                }
                boolean isTransactionComplete = customJpaController.createNewEmployee(ent_FhrdEmpmst, entAddrDtl, isCreateMode(), globalData.getUser_id(), globalData.getWorking_ou());
                if (isTransactionComplete) {
                    if (createMode) {
                        RequestContext.getCurrentInstance().execute("redirectToList()");
                    } else if (editMode) {
                        if (!ent_FhrdEmpmst.getUserId().equals(globalData.getUser_id())) {
                            ent_FhrdEmpmst = fhrdEmpmstFacade.find(ent_FhrdEmpmst.getUserId());
//                            Mail_pack m = new Mail_pack();
//                            String htmlMessage = "<html><style type=\"text/css\"> .font-settings *{color:#454550;font-family: Helvetica, Arial, sans-serif;font-size:12px;}</style><div class=\"scdl-cont\" style=\"color:#454550;font-family: Helvetica, Arial, sans-serif;font-size:12px\">"
//                                    + "Your Personal information is updated by system admin. Please verify or reply for the same if any correction is required. ";
//                            htmlMessage += "<br/><br/> <b>Updation Remarks :</b> \"" + txt_updation_remark.toUpperCase() + "\"<br/><br/>";
//                            htmlMessage += "<br/><br/><hr/><br/>Please do not reply on this email id.</div></html>";
//                            String textMessage = " User " + ent_FhrdEmpmst.getUserName() + " with user id " + ent_FhrdEmpmst.getUserId() + " left following comment on your change(s)."
//                                    + "\n\n\n------------------------------------\n\nNote: You are not able to view html version of this email which may hide many inforation. Please check e-mail view preference. \n\nPlease add noreply@fes.org.in in you address book/contacts to get uninterrupted schedule update. \n\nPlease do not reply on this email id.";
//                            Database_Output database_Output = m.send_mail_as_html(this.getClass().getName(),
//                                    "btn_leave_comment_clicked",
//                                    "\"Personal Information System\"<noreply@fes.org.in>",
//                                    ent_FhrdEmpmst.getUpdateby() == null ? (fhrdEmpmstFacade.find(ent_FhrdEmpmst.getCreateby()).getEmailId()) : (fhrdEmpmstFacade.find(ent_FhrdEmpmst.getUpdateby()).getEmailId()),
//                                    "Changes in your personal information",
//                                    textMessage,
//                                    htmlMessage, GlobalUtilities.smtpHost, GlobalUtilities.smtpPort);
                            Database_Output database_Output = new Database_Output(true);
                            if (database_Output.isExecuted_successfully()) {
                                RequestContext.getCurrentInstance().execute("updateParent()");
                            } else {
                                RequestContext.getCurrentInstance().execute("alert('Please email this comment to admin manually as some unexpected error occurred during sending email notifications for comments');updateParent();");
                            }
                        } else {
                            RequestContext.getCurrentInstance().execute("updateParent()");
                        }

                    } else {
                        setOperationMode(false, true, false, false);
                    }
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Employee " + ent_FhrdEmpmst.getUserFirstName() + " " + ent_FhrdEmpmst.getUserLastName() + " saved Successfully with User Id " + ent_FhrdEmpmst.getUserId()));
                } else {
                    context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred."));
                    LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_add_new_emp_click", null, null);
                }

            } catch (Exception e) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred."));
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_add_new_emp_click", null, e);
            }

        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected error occurred."));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "btn_add_new_emp_click", null, e);
        }

    }

    public void setEmpDetail() {
        if (createMode) {
            ent_FhrdEmpmst = new FhrdEmpmst();
            BigDecimal v_userid = getGlobalSettings().getUniqueSrno(null, "FHRD_EMPMST");
            ent_FhrdEmpmst.setEmpno(Integer.valueOf(v_userid.toString()));
            ent_FhrdEmpmst.setUserId(Integer.valueOf(v_userid.toString()));
            Integer v_birthyear = cal_birthdate.getYear() + 1900;
            ent_FhrdEmpmst.setPasswd(v_userid + "#" + v_birthyear);
//            if (!txt_emp_init.trim().isEmpty()) {
//                ent_FhrdEmpmst.setEmpinit(txt_emp_init.trim().toUpperCase());
//            }


            String v_middle_name = txt_user_middle_name.trim().isEmpty() ? "" : " " + txt_user_middle_name.trim().toUpperCase();
            ent_FhrdEmpmst.setUserName(txt_user_name.toUpperCase() + v_middle_name + " " + txt_user_last_name.toUpperCase());
            ent_FhrdEmpmst.setCreateby(globalData.getUser_id());
            ent_FhrdEmpmst.setCreatedt(new Date());
            SystOrgUnitMst entSystOrgUnitMst = new SystOrgUnitMst(Integer.valueOf(ddl_posting_oum));
            ent_FhrdEmpmst.setPostingOumUnitSrno(entSystOrgUnitMst);
        } else if (editMode) {
            ent_FhrdEmpmst = fhrdEmpmstFacade.find(ent_FhrdEmpmst.getUserId());
            SystOrgUnitMst entSystOrgUnitMst = new SystOrgUnitMst(ent_FhrdEmpmst.getPostingOumUnitSrno().getOumUnitSrno());
            if (getPIS_GlobalSettings().isUpdate_master_tbl_record_fhrd()) {
                entSystOrgUnitMst = new SystOrgUnitMst(Integer.valueOf(ddl_posting_oum));
            }
            ent_FhrdEmpmst.setPostingOumUnitSrno(entSystOrgUnitMst);
            if (getPIS_GlobalSettings().isUpdate_master_tbl_record_fhrd() && !globalData.getUser_id().equals(ent_FhrdEmpmst.getUserId())) {
                ent_FhrdEmpmst.setVerification("U");
            } else {
                ent_FhrdEmpmst.setVerification("A");
            }
            ent_FhrdEmpmst.setUpdateby(globalData.getUser_id());
            ent_FhrdEmpmst.setUpdatedt(new Date());
            ent_FhrdEmpmst.setUpdationRemark(txt_updation_remark.trim().toUpperCase());
        }
        if (uploaded_image != null) {
            byte[] file = new byte[uploaded_image.getContents().length];
            System.arraycopy(uploaded_image.getContents(), 0, file, 0, uploaded_image.getContents().length);
            ent_FhrdEmpmst.setEmpImg(file);
        }
        ent_FhrdEmpmst.setDsgmSrgKey(new FhrdDesigmst(new BigDecimal(ddl_designation)));
        SystUserTypeMst entSystUserTypeMst = new SystUserTypeMst(globalData.getOrganisation().getOumUnitSrno(), Integer.valueOf(ddl_user_type));
        ent_FhrdEmpmst.setFatherName(txt_father_name.trim().toUpperCase());
        ent_FhrdEmpmst.setMotherName(txt_mother_name.trim().toUpperCase());
        ent_FhrdEmpmst.setSystUserTypeMst(entSystUserTypeMst);
        ent_FhrdEmpmst.setBirthdt(cal_birthdate);
        ent_FhrdEmpmst.setGender(ddl_gender);
        ent_FhrdEmpmst.setMarital(ddl_marital_status);
        ent_FhrdEmpmst.setPhysicallyChallenged(ddl_physically_challenged);


        ent_FhrdEmpmst.setNationality(ddl_nationality);
        //ent_FhrdEmpmst.setVerification("P");
        ent_FhrdEmpmst.setUserFirstName(txt_user_name.toUpperCase());
        ent_FhrdEmpmst.setUserMiddleName(txt_user_middle_name.trim().isEmpty() ? null : txt_user_middle_name.trim().toUpperCase());
        //ent_FhrdEmpmst.setUserName(txt_user_name.toUpperCase());
        ent_FhrdEmpmst.setUserLastName(txt_user_last_name.toUpperCase());
        FhrdEmpmst entFhrdEmpmstReporting = new FhrdEmpmst(Integer.valueOf(ddl_reporting_user));
        ent_FhrdEmpmst.setReportingUserId(entFhrdEmpmstReporting);
        ent_FhrdEmpmst.setCcmSrgKey(new FhrdCasteCategoryMst(Integer.valueOf(ddl_category)));
        if (cal_org_joining_date != null) {
            ent_FhrdEmpmst.setOrgJoinDate(cal_org_joining_date);
        }

        if (ddl_blood_group != null) {
            ent_FhrdEmpmst.setBloodgroup(ddl_blood_group);
        }
        if (txt_bank_account_no.trim().isEmpty()) {
            ent_FhrdEmpmst.setBankAccountno(txt_bank_account_no);
        }
        if (ddl_religion != null) {
            ent_FhrdEmpmst.setRgSrgKey(Integer.valueOf(ddl_religion));
        }
        if (ddl_responsibility != null) {
            ent_FhrdEmpmst.setPmUniqueSrno(new FhrdPortfolioMst(Integer.valueOf(ddl_responsibility)));
        }
        if (!txt_bank_name.trim().isEmpty()) {
            ent_FhrdEmpmst.setBankName(txt_bank_name);
        }
        if (!txt_bank_location.trim().isEmpty()) {
            ent_FhrdEmpmst.setBankLocation(txt_bank_location);
        }

        if (!txt_pan_no.trim().isEmpty()) {
            ent_FhrdEmpmst.setPanno(txt_pan_no.trim().toUpperCase());
        }
        if (!txt_home_town.trim().isEmpty()) {
            ent_FhrdEmpmst.setHometown(txt_home_town.toUpperCase());
        }
        if (!txt_ntgcfpf_no.trim().isEmpty()) {
            ent_FhrdEmpmst.setNtgcfpfno(txt_ntgcfpf_no);
        }
        if (!txt_ifsc_code.trim().isEmpty()) {
            ent_FhrdEmpmst.setIfscCode(txt_ifsc_code);
        }
        //Passport Detail

        ent_FhrdEmpmst.setPassNo((txt_passport_no.trim().equals("")) ? null : txt_passport_no.trim().toUpperCase());
        ent_FhrdEmpmst.setPassAuthority((txt_passport_auth.trim().equals("")) ? null : txt_passport_auth.trim().toUpperCase());
        ent_FhrdEmpmst.setPassIssueDt(cal_passport_issue_date);
        ent_FhrdEmpmst.setPassExpiryDt(cal_passport_expire_date);
    }

    public void setEmpEventDetail() {
        List<FhrdEmpEventDtl> lstFhrdEmpEventDtls = new ArrayList<FhrdEmpEventDtl>();
        if (cal_org_joining_date != null) {
            FhrdEventMst entEventMst = fhrdEventMstFacade.findByEventName("ORG_JOIN_DATE");
            FhrdEmpEventDtl entFhrdEmpEventDtl = new FhrdEmpEventDtl();
            entFhrdEmpEventDtl.setEmpedSrgKey(new BigDecimal("-1"));
            entFhrdEmpEventDtl.setSrno(-1);
            entFhrdEmpEventDtl.setCreateby(globalData.getEnt_login_user());
            entFhrdEmpEventDtl.setCreatedt(new Date());
            entFhrdEmpEventDtl.setEventDate(cal_org_joining_date);
            entFhrdEmpEventDtl.setEmSrgKey(entEventMst);
            entFhrdEmpEventDtl.setOumUnitSrno(ent_FhrdEmpmst.getPostingOumUnitSrno().getOumUnitSrno());
            entFhrdEmpEventDtl.setUserId(ent_FhrdEmpmst);
            entFhrdEmpEventDtl.setEventEffectTime(entEventMst.getEventEffectTime());
            lstFhrdEmpEventDtls.add(entFhrdEmpEventDtl);
        }
        if (cal_ou_joining_date != null) {
            FhrdEventMst entEventMst = fhrdEventMstFacade.findByEventName("OU_JOIN_DATE");
            FhrdEmpEventDtl entFhrdEmpEventDtl = new FhrdEmpEventDtl();
            entFhrdEmpEventDtl.setEmpedSrgKey(new BigDecimal("-2"));
            entFhrdEmpEventDtl.setSrno(-1);
            entFhrdEmpEventDtl.setCreateby(globalData.getEnt_login_user());
            entFhrdEmpEventDtl.setCreatedt(new Date());
            entFhrdEmpEventDtl.setEventDate(cal_ou_joining_date);
            entFhrdEmpEventDtl.setEmSrgKey(entEventMst);
            entFhrdEmpEventDtl.setOumUnitSrno(ent_FhrdEmpmst.getPostingOumUnitSrno().getOumUnitSrno());
            entFhrdEmpEventDtl.setUserId(ent_FhrdEmpmst);
            entFhrdEmpEventDtl.setEventEffectTime(entEventMst.getEventEffectTime());
            lstFhrdEmpEventDtls.add(entFhrdEmpEventDtl);
        }


        ent_FhrdEmpmst.setFhrdEmpEventDtlCollection2(lstFhrdEmpEventDtls);
    }

    private FcadAddrDtl setAddressDetail() {
        FcadAddrMst entAddrmst = new FcadAddrMst();
        if (createMode) {
            entAddrmst.setAmUniqueSrno(selectedAddrMst.getAmUniqueSrno());
            entAddrmst.setAmFirstName(txt_user_name.toUpperCase().trim());
            entAddrmst.setAmMiddleName(txt_user_middle_name.toUpperCase().trim());
            entAddrmst.setAmLastName(txt_user_last_name.toUpperCase().trim());
            entAddrmst.setAmAddrType("P");
            entAddrmst.setAmUserManFlag("N");
            entAddrmst.setCreateby(globalData.getEnt_login_user());
            entAddrmst.setCreatedt(new Date());
            entAddrmst.setFcadTeleFaxCollection(tbl_gen_tele_fax);
        } else if (editMode) {
            if (ent_FhrdEmpmst.getAddressno() == null) {
                entAddrmst.setAmUniqueSrno(selectedAddrMst.getAmUniqueSrno());
                entAddrmst.setAmFirstName(txt_user_name.toUpperCase().trim());
                entAddrmst.setAmMiddleName(txt_user_middle_name.toUpperCase().trim());
                entAddrmst.setAmLastName(txt_user_last_name.toUpperCase().trim());
                entAddrmst.setAmAddrType("P");
                entAddrmst.setAmUserManFlag("N");
                entAddrmst.setCreateby(globalData.getEnt_login_user());
                entAddrmst.setCreatedt(new Date());
                entAddrmst.setFcadTeleFaxCollection(tbl_gen_tele_fax);
            } else {
                entAddrmst = fcadAddrMstFacade.find(ent_FhrdEmpmst.getAddressno().getAmUniqueSrno());
                entAddrmst.setAmFirstName(txt_user_name.toUpperCase().trim());
                entAddrmst.setAmMiddleName(txt_user_middle_name.toUpperCase().trim());
                entAddrmst.setAmLastName(txt_user_last_name.toUpperCase().trim());
                entAddrmst.setUpdateby(globalData.getEnt_login_user());
                entAddrmst.setUpdatedt(new Date());
                entAddrmst.setFcadTeleFaxCollection(tbl_gen_tele_fax);
            }
        }

        FcadAddrDtl ent_AddrDtl = new FcadAddrDtl();
        ent_AddrDtl.setFcadAddrDtlPK(new FcadAddrDtlPK(new BigInteger(entAddrmst.getAmUniqueSrno().toString()), 0));
        ent_AddrDtl.setFcadAddrMst(entAddrmst);
        ent_AddrDtl.setAdAddr1(txt_addr1.trim());
        ent_AddrDtl.setAdAddr2(txt_addr2.trim());
        ent_AddrDtl.setAdAddr3(txt_addr3.trim());
        ent_AddrDtl.setAdCity(txt_city.toUpperCase().trim());
        ent_AddrDtl.setAdState(txt_state.toUpperCase().trim());
        ent_AddrDtl.setAdStartDate(new Date());
        ent_AddrDtl.setCreateby(globalData.getEnt_login_user());
        ent_AddrDtl.setCreatedt(new Date());
        ent_AddrDtl.setAdPincode(txt_pincode.toUpperCase().trim());
        ent_AddrDtl.setAdCountry(txt_country.toUpperCase().trim());
        ent_AddrDtl.setAdEmail(txt_email.toLowerCase().trim());
        ent_AddrDtl.setAdEmailAlt(txt_email_alt.toLowerCase().trim());
        ent_AddrDtl.setAdWebsite(txt_website.toLowerCase().trim());
        //entAddrmst.setSystOrgUnitMst(globalData.getEnt_working_ou());
        ent_AddrDtl.setOumUnitSrno(globalData.getEnt_working_ou());

        ent_AddrDtl.setAdPresAddr1(txt_present_addr1.trim());

        ent_AddrDtl.setAdPresAddr2(txt_present_addr2.trim());

        ent_AddrDtl.setAdPresAddr3(txt_present_addr3.trim());

        ent_AddrDtl.setAdPresCity(txt_present_city.toUpperCase().trim());

        ent_AddrDtl.setAdPresState(txt_present_state.toUpperCase().trim());

        ent_AddrDtl.setAdPresPincode(txt_present_pincode.toUpperCase().trim());

        ent_AddrDtl.setAdPresCountry(txt_present_country.toUpperCase().trim());

        ent_AddrDtl.setAdPermAddr1(txt_permanent_addr1.trim());

        ent_AddrDtl.setAdPermAddr2(txt_permanent_addr2.trim());

        ent_AddrDtl.setAdPermAddr3(txt_permanent_addr3.trim());

        ent_AddrDtl.setAdPermCity(txt_permanent_city.toUpperCase().trim());

        ent_AddrDtl.setAdPermState(txt_permanent_state.toUpperCase().trim());

        ent_AddrDtl.setAdPermPincode(txt_permanent_pincode.toUpperCase().trim());

        ent_AddrDtl.setAdPermCountry(txt_permanent_country.toUpperCase().trim());

//        ent_FhrdEmpmst.setFcadAddrMstCollection();
        return ent_AddrDtl;
    }

    public void setEmpExperience() {
        int n = tbl_experience == null ? 0 : tbl_experience.size();
        for (int i = 0; i < n; i++) {
            FhrdExperience ent_Experience = tbl_experience.get(i);
            if (ent_Experience.getCreateby() == null) {
                ent_Experience.setFhrdExperiencePK(new FhrdExperiencePK(ent_FhrdEmpmst.getUserId(), (-1) * i));
                ent_Experience.setCreateby(globalData.getEnt_login_user());
                ent_Experience.setCreatedt(new Date());
                ent_Experience.setOumUnitSrno(new SystOrgUnitMst(new Integer(ddl_posting_oum)));

                ent_Experience.setEmpno(ent_FhrdEmpmst.getEmpno());
            }
        }
        ent_FhrdEmpmst.setFhrdExperienceCollection1(tbl_experience);
    }

    private void setQualifications() {
        int n = tbl_qualification == null ? 0 : tbl_qualification.size();
        for (int i = 0; i < n; i++) {
            FhrdQualification ent_Qualification = tbl_qualification.get(i);
            if (ent_Qualification.getCreateby() == null) {
                ent_Qualification.setFhrdQualificationPK(new FhrdQualificationPK(ent_FhrdEmpmst.getUserId(), (-1) * i));
                ent_Qualification.setCreateby(globalData.getEnt_login_user());
                ent_Qualification.setCreatedt(new Date());
                ent_Qualification.setOumUnitSrno(new SystOrgUnitMst(new Integer(ddl_posting_oum)));
            }
//            else {
//                //tbl_qualification.set(i, fhrdQualificationFacade.find(ent_Qualification.getQnSrgKey()));
//                tbl_qualification.get(i).setUpdateby(globalData.getEnt_login_user());
//                tbl_qualification.get(i).setUpdatedt(new Date());
//            }
        }
        ent_FhrdEmpmst.setFhrdQualificationCollection(tbl_qualification);
    }

    public void setEmpDependent() {
        int n = tbl_dependents == null ? 0 : tbl_dependents.size();

        for (int i = 0; i < n; i++) {
            FhrdDependent ent_Dependent = tbl_dependents.get(i);
            if (ent_Dependent.getCreateby() == null) {
                ent_Dependent.setFhrdDependentPK(new FhrdDependentPK(ent_FhrdEmpmst.getUserId(), (-1) * i));
                ent_Dependent.setCreateby(globalData.getEnt_login_user());
                ent_Dependent.setCreatedt(new Date());
                ent_Dependent.setOumUnitSrno(new SystOrgUnitMst(new Integer(ddl_posting_oum)));
            }
        }
        ent_FhrdEmpmst.setFhrdDependentCollection1(tbl_dependents);
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="setOperationMode">
    public void setOperationMode(boolean createMode, boolean editMode, boolean viewMode, boolean forVerification) {
        this.editMode = editMode;
        this.createMode = createMode;
        this.viewMode = viewMode;
        this.forVerification = forVerification;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="checkForEditMode()"> 

    public void checkForEditMode() {
        if (editMode || viewMode) {
            txt_user_name = ent_FhrdEmpmst.getUserFirstName() == null ? ent_FhrdEmpmst.getUserName() : ent_FhrdEmpmst.getUserFirstName();
            txt_user_last_name = ent_FhrdEmpmst.getUserLastName();
            txt_user_middle_name = ent_FhrdEmpmst.getUserMiddleName();
            txt_father_name = ent_FhrdEmpmst.getFatherName();
            txt_mother_name = ent_FhrdEmpmst.getMotherName();
            ddl_gender = ent_FhrdEmpmst.getGender().toString();
//            txt_emp_init = ent_FhrdEmpmst.getEmpinit();
            ddl_marital_status = ent_FhrdEmpmst.getMarital();
            ddl_religion = ent_FhrdEmpmst.getRgSrgKey() == null ? null : ent_FhrdEmpmst.getRgSrgKey().toString();
            cal_birthdate = ent_FhrdEmpmst.getBirthdt();
            ddl_blood_group = ent_FhrdEmpmst.getBloodgroup();
            txt_home_town = ent_FhrdEmpmst.getHometown();
            ddl_user_type = String.valueOf(ent_FhrdEmpmst.getSystUserTypeMst().getSystUserTypeMstPK().getUserTypeSrno());
            ddl_category = ent_FhrdEmpmst.getCcmSrgKey() == null ? null : ent_FhrdEmpmst.getCcmSrgKey().getCcmSrgKey().toString();
            ddl_physically_challenged = ent_FhrdEmpmst.getPhysicallyChallenged();
            ddl_designation = ent_FhrdEmpmst.getDsgmSrgKey() == null ? null : ent_FhrdEmpmst.getDsgmSrgKey().getDsgmSrgKey().toString();
            ddl_reporting_user = ent_FhrdEmpmst.getReportingUserId().getUserId().toString();
            ddl_posting_oum = ent_FhrdEmpmst.getPostingOumUnitSrno().getOumUnitSrno().toString();
            ddl_nationality = ent_FhrdEmpmst.getNationality();
            ddl_responsibility = ent_FhrdEmpmst.getPmUniqueSrno() == null ? null : ent_FhrdEmpmst.getPmUniqueSrno().getPmUniqueSrno().toString();
            txt_passport_no = ent_FhrdEmpmst.getPassNo();
            txt_passport_auth = ent_FhrdEmpmst.getPassAuthority();
            cal_passport_issue_date = ent_FhrdEmpmst.getPassIssueDt();
            cal_passport_expire_date = ent_FhrdEmpmst.getPassExpiryDt();
            txt_pan_no = ent_FhrdEmpmst.getPanno();
            txt_bank_account_no = ent_FhrdEmpmst.getBankAccountno();
            txt_bank_location = ent_FhrdEmpmst.getBankLocation();
            txt_bank_name = ent_FhrdEmpmst.getBankName();
            ///////////////////////////////////////////////////////Addresses
            if (ent_FhrdEmpmst.getAddressno() != null) {
                FcadAddrDtl ent_AddrDtl = fcadAddrDtlFacade.findActive(ent_FhrdEmpmst.getAddressno().getAmUniqueSrno());
                txt_addr1 = ent_AddrDtl.getAdAddr1();
                txt_addr2 = ent_AddrDtl.getAdAddr2();
                txt_addr3 = ent_AddrDtl.getAdAddr3();
                txt_city = ent_AddrDtl.getAdCity();
                txt_pincode = ent_AddrDtl.getAdPincode();
                txt_state = ent_AddrDtl.getAdState();
                txt_country = ent_AddrDtl.getAdCountry();
                txt_present_addr1 = ent_AddrDtl.getAdPresAddr1();
                txt_present_addr2 = ent_AddrDtl.getAdPresAddr2();
                txt_present_addr3 = ent_AddrDtl.getAdPresAddr3();
                txt_present_city = ent_AddrDtl.getAdPresCity();
                txt_present_pincode = ent_AddrDtl.getAdPresPincode();
                txt_present_state = ent_AddrDtl.getAdPresState();
                txt_present_country = ent_AddrDtl.getAdPresCountry();
                txt_permanent_addr1 = ent_AddrDtl.getAdPermAddr1();
                txt_permanent_addr2 = ent_AddrDtl.getAdPermAddr2();
                txt_permanent_addr3 = ent_AddrDtl.getAdPermAddr3();
                txt_permanent_city = ent_AddrDtl.getAdPermCity();
                txt_permanent_pincode = ent_AddrDtl.getAdPermPincode();
                txt_permanent_state = ent_AddrDtl.getAdPermState();
                txt_permanent_country = ent_AddrDtl.getAdPermCountry();
                txt_website = ent_AddrDtl.getAdWebsite();
                txt_email = ent_AddrDtl.getAdEmail();
                txt_email_alt = ent_AddrDtl.getAdEmailAlt();


            }
            ///////////////////////////////////////////////////////organization detail
            cal_ou_joining_date = setEventDate("ORG_JOIN_DATE");
            cal_org_joining_date = setEventDate("OU_JOIN_DATE");


            txt_ntgcfpf_no = ent_FhrdEmpmst.getNtgcfpfno();
            txt_ifsc_code = ent_FhrdEmpmst.getIfscCode();

            //cal_confirm_date=ent_FhrdEmpmst.getFhrdContractGroupRelationCollection()
            /////////////////////////////////////////////////////Qualification
            tbl_qualification = fhrdQualificationFacade.findQualificationUseridwise(ent_FhrdEmpmst.getUserId());
            tbl_experience = fhrdExperienceFacade.findExperienceUseridwise(ent_FhrdEmpmst.getUserId());
            tbl_dependents = fhrdDependentFacade.findUseridwise(ent_FhrdEmpmst.getUserId());
            /////////////////////////////////////////////////////Experience

        } else {

            tbl_dependents = null;
            tbl_experience = null;
            tbl_qualification = null;
        }
        if (selectedAddrMst == null) {
            selectedAddrMst = new FcadAddrMst(new BigDecimal("-1"));
        }
    }

    public Date setEventDate(String p_event_name) {
        List<FhrdEmpEventDtl> entEmpEventDtl = fhrdEmpEventDtlFacade.findEventEmpwise(ent_FhrdEmpmst.getUserId(), p_event_name);
        Date d_event_date;
        if (!entEmpEventDtl.isEmpty()) {
            d_event_date = entEmpEventDtl.get(0).getEventDate();
        } else {
            d_event_date = null;
        }
        return d_event_date;
    }
    //</editor-fold>  
    //<editor-fold defaultstate="collapsed" desc="ImageUpload Settings">
    String image_for_user_id;

    public String getImage_for_user_id() {
        return image_for_user_id;
    }

    public void setImage_for_user_id(String image_for_user_id) {
        this.image_for_user_id = image_for_user_id;
    }
    //<editor-fold defaultstate="collapsed" desc="image_random_name">
    String image_random_name;

    public String getImage_random_name() {
        return image_random_name;
    }

    public void setImage_random_name(String image_random_name) {
        this.image_random_name = image_random_name;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="showUploaded">
    String showUploaded;

    public String getShowUploaded() {
        return showUploaded;
    }

    public void setShowUploaded(String showUploaded) {
        this.showUploaded = showUploaded;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="uploaded_image">
    UploadedFile uploaded_image;

    public UploadedFile getUploaded_image() {
        return uploaded_image;
    }

    public void setUploaded_image(UploadedFile uploaded_image) {
        this.uploaded_image = uploaded_image;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="upload_location">
    String upload_location;

    public String getUpload_location() {
        return upload_location;
    }

    public void setUpload_location(String upload_location) {
        this.upload_location = upload_location;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="handleFileUpload">

    public void handleFileUpload(FileUploadEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        uploaded_image = event.getFile();
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        String v_file_ext = uploaded_image.getFileName().split("\\.")[(uploaded_image.getFileName().split("\\.").length) - 1];
        upload_location = servletContext.getRealPath("") + File.separator + "temp-images" + File.separator + image_random_name + "." + v_file_ext;
        FileImageOutputStream imageOutput;
        try {
            imageOutput = new FileImageOutputStream(new File(upload_location));
            imageOutput.write(uploaded_image.getContents(), 0, uploaded_image.getContents().length);
            imageOutput.close();
        } catch (FileNotFoundException e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "File Not Found"));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "handleFileUpload", null, e);
        } catch (IOException e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some Unexpected Error occurred during uploading image"));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "handleFileUpload", null, e);
        }
        showUploaded = "Y";
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="University, Degree & Subject Settings">
    //<editor-fold defaultstate="collapsed" desc="Flag Settings">
    boolean renderUniversity = false;
    boolean renderDegree = false;
    boolean renderSubject = false;

    public boolean isRenderDegree() {
        return renderDegree;
    }

    public void setRenderDegree(boolean renderDegree) {
        this.renderDegree = renderDegree;
    }

    public boolean isRenderSubject() {
        return renderSubject;
    }

    public void setRenderSubject(boolean renderSubject) {
        this.renderSubject = renderSubject;
    }

    public boolean isRenderUniversity() {
        return renderUniversity;
    }

    public void setRenderUniversity(boolean renderUniversity) {
        this.renderUniversity = renderUniversity;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ShowUni_Deg_Sub">
    public void showUni_Deg_Sub(boolean renderUniversity, boolean renderDegree, boolean renderSubject) {
        this.renderUniversity = renderUniversity;
        this.renderDegree = renderDegree;
        this.renderSubject = renderSubject;
        if (renderUniversity) {
            resetUniversity();
            RequestContext.getCurrentInstance().execute("showUni_Deg_Sub_Dialog('U')");
        } else if (renderDegree) {
            resetDegree();
            RequestContext.getCurrentInstance().execute("showUni_Deg_Sub_Dialog('D')");
        } else if (renderSubject) {
            resetSubject();
            RequestContext.getCurrentInstance().execute("showUni_Deg_Sub_Dialog('S')");
        }

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="University">

    private void resetUniversity() {
        txt_uni_code = null;
        txt_uni_city = null;
        txt_uni_country = null;
        txt_uni_state = null;
        txt_uni_name = null;
        txt_uni_pin_code = null;
    }
    //<editor-fold defaultstate="collapsed" desc="getter/setter">
    public String txt_uni_name;
    public String txt_uni_city;
    public String txt_uni_state;
    public String txt_uni_code;
    public String txt_uni_country;
    public String txt_uni_pin_code;

    public String getTxt_uni_code() {
        return txt_uni_code;
    }

    public void setTxt_uni_code(String txt_uni_code) {
        this.txt_uni_code = txt_uni_code;
    }

    public String getTxt_uni_city() {
        return txt_uni_city;
    }

    public void setTxt_uni_city(String txt_uni_city) {
        this.txt_uni_city = txt_uni_city;
    }

    public String getTxt_uni_country() {
        return txt_uni_country;
    }

    public void setTxt_uni_country(String txt_uni_country) {
        this.txt_uni_country = txt_uni_country;
    }

    public String getTxt_uni_pin_code() {
        return txt_uni_pin_code;
    }

    public void setTxt_uni_pin_code(String txt_uni_pin_code) {
        this.txt_uni_pin_code = txt_uni_pin_code;
    }

    public String getTxt_uni_state() {
        return txt_uni_state;
    }

    public void setTxt_uni_state(String txt_uni_state) {
        this.txt_uni_state = txt_uni_state;
    }

    public String getTxt_uni_name() {
        return txt_uni_name;
    }

    public void setTxt_uni_name(String txt_uni_name) {
        this.txt_uni_name = txt_uni_name;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="saveUniversity()">
    public void saveUniversity() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (!validateEntry()) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "Same University Code or University Name exists in same City and State"));
            return;
        }
        BigDecimal v_lm_srgkey = getGlobalSettings().getUniqueSrno(null, "FHRD_UNIVERSITYMST");
        FhrdUniversitymst ent_FhrdUniversitymst = new FhrdUniversitymst(v_lm_srgkey);
        ent_FhrdUniversitymst.setUniversitySrno(-1);
        ent_FhrdUniversitymst.setUniversitycd(txt_uni_code.toUpperCase().trim());
        ent_FhrdUniversitymst.setUniversityName(txt_uni_name.toUpperCase().trim());
        if (!txt_uni_city.isEmpty()) {
            ent_FhrdUniversitymst.setCity(txt_uni_city.toUpperCase().trim());
        }
        ent_FhrdUniversitymst.setState(txt_uni_state.toUpperCase().trim());
        if (!txt_uni_pin_code.isEmpty()) {
            ent_FhrdUniversitymst.setPincode(txt_uni_pin_code.toUpperCase());
        }
        ent_FhrdUniversitymst.setCountry(txt_uni_country.toUpperCase());
        ent_FhrdUniversitymst.setOumUnitSrno(globalData.getEnt_working_ou());
        ent_FhrdUniversitymst.setCreateby(globalData.getEnt_login_user());
        ent_FhrdUniversitymst.setCreatedt(new Date());
        try {
            fhrdUniversitymstFacade.create(ent_FhrdUniversitymst);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucess", "University Created Successfully"));
            setDdl_university();
            ddl_university = fhrdUniversitymstFacade.findUniversity(globalData.getWorking_ou(), txt_uni_code.trim().toUpperCase()).toString();
            RequestContext.getCurrentInstance().execute("pw_dlg_uni_deg_sub.hide()");

        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occerred during saving records"));
            LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "saveUniversity", null, e);
        }
    }

    private boolean validateEntry() {
        if (fhrdUniversitymstFacade.isUniversityExist(globalData.getWorking_ou(), txt_uni_code.toUpperCase().trim(), txt_uni_name.toUpperCase().trim(), txt_uni_city.toUpperCase().trim(), txt_uni_state.trim().toUpperCase())) {
            return false;
        }
        return true;
    }
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Degree">
    //<editor-fold defaultstate="collapsed" desc="Object Declarations">
    String txt_degree_cd;
    String txt_degree_name;

    public String getTxt_degree_cd() {
        return txt_degree_cd;
    }

    public void setTxt_degree_cd(String txt_degree_cd) {
        this.txt_degree_cd = txt_degree_cd;
    }

    public String getTxt_degree_name() {
        return txt_degree_name;
    }

    public void setTxt_degree_name(String txt_degree_name) {
        this.txt_degree_name = txt_degree_name;
    }

    private void resetDegree() {
        txt_degree_cd = null;
        ddl_degree_level = null;
        txt_degree_name = null;
        ddl_stream = null;

    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="ddl_degree_level">
    String ddl_degree_level;
    List<SelectItem> ddl_degree_level_options;

    public String getDdl_degree_level() {
        return ddl_degree_level;
    }

    public void setDdl_degree_level(String ddl_degree_level) {
        this.ddl_degree_level = ddl_degree_level;
    }

    public List<SelectItem> getDdl_degree_level_options() {
        return ddl_degree_level_options;
    }

    public void setDdl_degree_level_options(List<SelectItem> ddl_degree_level_options) {
        this.ddl_degree_level_options = ddl_degree_level_options;
    }

    private void setDdl_degree_level() {
        List<FhrdDegreeLevelMst> lst_LevelMsts = fhrdDegreeLevelMstFacade.findAll(globalData.getWorking_ou(), true);
        ddl_degree_level_options = new ArrayList<SelectItem>();
        ddl_degree_level_options.add(new SelectItem(null, "--- Select ---"));
        for (FhrdDegreeLevelMst l : lst_LevelMsts) {
            ddl_degree_level_options.add(new SelectItem(l.getDlmSrgKey().toString(), l.getDlmDescription()));
        }
        ddl_degree_level = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="createDegree">

    public void createDegree(ActionEvent event) {
        if (isValidDegree()) {
            FacesContext context = FacesContext.getCurrentInstance();
            BigDecimal v_dgrm_srgkey = getGlobalSettings().getUniqueSrno(null, "FHRD_DEGREEMST");
            SystOrgUnitMst ent_SystOrgUnitMst = new SystOrgUnitMst(globalData.getWorking_ou());
            FhrdDegreemst ent_FhrdDegreemst = new FhrdDegreemst(v_dgrm_srgkey);
            ent_FhrdDegreemst.setDegreeSrno(-1);
            ent_FhrdDegreemst.setDegreeName(txt_degree_name.toUpperCase().trim());
            ent_FhrdDegreemst.setDegreecd(txt_degree_cd.toUpperCase().trim());
            ent_FhrdDegreemst.setDlmSrgKey(new FhrdDegreeLevelMst(new Integer(ddl_degree_level)));
            ent_FhrdDegreemst.setCreateby(new FhrdEmpmst(globalData.getUser_id()));
            ent_FhrdDegreemst.setCreatedt(new Date());
            ent_FhrdDegreemst.setOumUnitSrno(ent_SystOrgUnitMst);
            try {
                fhrdDegreemstFacade.create(ent_FhrdDegreemst);
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record saved Successfully."));
                setDdl_degree();
                ddl_degree = v_dgrm_srgkey.toString();
            } catch (Exception ex) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occerred during saving records"));
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createDegree", null, ex);
            }
            RequestContext.getCurrentInstance().execute("pw_dlg_uni_deg_sub.hide()");
        }
    }

    private boolean isValidDegree() {
        if (!fhrdDegreemstFacade.isDegreeValid(globalData.getWorking_ou(), txt_degree_cd.trim().toUpperCase(), txt_degree_name.trim().toUpperCase())) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "Either Degree Code or Degree Name already exists in database."));
            return false;
        }
        return true;
    }
//</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Subject">
    //<editor-fold defaultstate="collapsed" desc="Object Declaration">
    String txt_subject_name;

    public String getTxt_subject_name() {
        return txt_subject_name;
    }

    public void setTxt_subject_name(String txt_subject_name) {
        this.txt_subject_name = txt_subject_name;
    }

    private void resetSubject() {

        txt_subject_name = null;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="createSubject">

    public void createSubject(ActionEvent event) {
        txt_subject_name = txt_subject_name.toUpperCase().trim();
        if (isSubjectValid()) {
            FacesContext context = FacesContext.getCurrentInstance();
            FhrdDegreedtl ent_FhrdDegreedtl = new FhrdDegreedtl(new BigDecimal("0"), new BigDecimal(ddl_degree));
            ent_FhrdDegreedtl.setCreateby(globalData.getEnt_login_user());
            ent_FhrdDegreedtl.setCreatedt(new Date());
            ent_FhrdDegreedtl.setOumUnitSrno(globalData.getEnt_working_ou());
            ent_FhrdDegreedtl.setSubjectName(txt_subject_name);

            try {
                fhrdDegreedtlFacade.create(ent_FhrdDegreedtl);
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record saved Successfully."));
                setDdl_subject();
                ddl_subject = fhrdDegreedtlFacade.findAll(globalData.getWorking_ou(), new BigDecimal(ddl_degree), txt_subject_name).get(0).getFhrdDegreedtlPK().getSubjectSrno().toString();
            } catch (Exception ex) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpected error occerred during saving records"));
                LogGenerator.generateLog(systemName, Level.SEVERE, this.getClass().getName(), "createSubject", null, ex);
            }
            RequestContext.getCurrentInstance().execute("pw_dlg_uni_deg_sub.hide()");
        }
    }

    private boolean isSubjectValid() {
        if (!fhrdDegreedtlFacade.isSubjectValid(globalData.getWorking_ou(), ddl_degree, null, txt_subject_name)) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Validation Failed", "Subject Code/Subject Name already exists under same degree."));
            return false;
        }
        return true;
    }
    //</editor-fold>
    //</editor-fold>
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="btn_leave_comment_clicked">

    public void btn_leave_comment_clicked(ActionEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
//            
//            HtmlEmail email = new HtmlEmail();
//            email.setHostName("mail.fes.org.in");
//            String message = "<html><style type=\"text/css\"> .font-settings *{color:#454550;font-family: Helvetica, Arial, sans-serif;font-size:12px;}</style><div class=\"scdl-cont\" style=\"color:#454550;font-family: Helvetica, Arial, sans-serif;font-size:12px\">"
//                    + " User <b>" + ent_FhrdEmpmst.getUserName() + "</b> with user id <b>" + ent_FhrdEmpmst.getUserId() + "</b> left following comment on your change(s).";
//            message += "<br/><br/> <b>\"" + txt_comment.toUpperCase() + "\"</b><br/><br/>";
//            message += "<br/><br/><hr/><br/><Please do not reply on this email id. Add <a href=\"#\">noreply@fes.org.in</a> in you address book/contacts to get uninterrupted schedule update.</div></html>";
//            email.setFrom("noreply@fes.org.in", "Employee Updation Remark");
//            email.setSubject("Employee Remark on updations of his/her detail");
//            email.setHtmlMsg(message);
//            email.setBounceAddress("jitesh@fes.org.in");
//            email.addTo(ent_FhrdEmpmst.getUpdateby() == null ? (fhrdEmpmstFacade.find(ent_FhrdEmpmst.getCreateby()).getEmailId()) : (fhrdEmpmstFacade.find(ent_FhrdEmpmst.getUpdateby()).getEmailId()));
//            // set the alternative message
//            email.setTextMsg(" User " + ent_FhrdEmpmst.getUserName() + " with user id " + ent_FhrdEmpmst.getUserId() + " left following comment on your change(s)."
//                    + "\n\n\n------------------------------------\n\nNote: You are not able to view html version of this email which may hide many inforation. Please check e-mail view preference. \n\nPlease add noreply@fes.org.in in you address book/contacts to get uninterrupted schedule update. \n\nPlease do not reply on this email id.");
//            email.send();
            ent_FhrdEmpmst = fhrdEmpmstFacade.find(ent_FhrdEmpmst.getUserId());
            ent_FhrdEmpmst.setVerificationRemark(txt_comment.toUpperCase());
            ent_FhrdEmpmst.setVerification("R");
            fhrdEmpmstFacade.edit(ent_FhrdEmpmst);
            Mail_pack m = new Mail_pack();
            String htmlMessage = "<html><style type=\"text/css\"> .font-settings *{color:#454550;font-family: Helvetica, Arial, sans-serif;font-size:12px;}</style><div class=\"scdl-cont\" style=\"color:#454550;font-family: Helvetica, Arial, sans-serif;font-size:12px\">";
            if (ent_FhrdEmpmst.getUserId().equals(globalData.getUser_id())) {
                htmlMessage += " User <b>" + ent_FhrdEmpmst.getUserName() + "</b> with user id <b>" + ent_FhrdEmpmst.getUserId() + "</b> left following comment on your change(s).";
            } else {
                htmlMessage += "System Administrator left following comment on your change(s).";
            }
            htmlMessage += "<br/><br/> <b>\"" + txt_comment.toUpperCase() + "\"</b><br/><br/>";

            htmlMessage += "<br/><br/><hr/><br/>Please do not reply on this email id.</div></html>";


            String textMessage;
            if (ent_FhrdEmpmst.getUserId().equals(globalData.getUser_id())) {
                textMessage = " User " + ent_FhrdEmpmst.getUserName() + " with user id " + ent_FhrdEmpmst.getUserId() + " left following comment on your change(s)."
                        + "\n\n\n------------------------------------\n\nNote: You are not able to view html version of this email which may hide many inforation. Please check e-mail view preference. \n\nPlease add noreply@fes.org.in in you address book/contacts to get uninterrupted schedule update. \n\nPlease do not reply on this email id.";
            } else {
                textMessage = " System Administrator left following comment on your change(s)."
                        + "\n\n\n------------------------------------\n\nNote: You are not able to view html version of this email which may hide many inforation. Please check e-mail view preference. \n\nPlease add noreply@fes.org.in in you address book/contacts to get uninterrupted schedule update. \n\nPlease do not reply on this email id.";
            }
            String toEmailId = ent_FhrdEmpmst.getUpdateby() == null ? (fhrdEmpmstFacade.find(ent_FhrdEmpmst.getCreateby()).getEmailId()) : (fhrdEmpmstFacade.find(ent_FhrdEmpmst.getUpdateby()).getEmailId());
            Database_Output database_Output = m.send_mail_as_html(this.getClass().getName(),
                    "btn_leave_comment_clicked",
                    "\"Personal Information System\"<noreply@fes.org.in>",
                    toEmailId,
                    "Employee Remark on updations of his/her detail",
                    textMessage,
                    htmlMessage, GlobalUtilities.smtpHost, GlobalUtilities.smtpPort);

            if (database_Output.isExecuted_successfully() && toEmailId != null) {
                RequestContext.getCurrentInstance().execute("updateParent()");
            } else {
                RequestContext.getCurrentInstance().execute("alert('Please email this comment to admin manually as some unexpected error occurred during sending email notifications for comments');updateParent();");
            }


        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Some unexpedcted Error Occurred During sending email notifications"));
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, this.getClass().getName(), "btn_leave_comment_clicked", null, e);
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="init()">

    @PostConstruct
    public void init() {
        globalData = getPIS_GlobalSettings().getPIS_GlobalData();
        showUploaded = "N";
        Integer v_userid;

        if (getPIS_GlobalSettings().getEmployeeForUpdate() != null) {
            v_userid = getPIS_GlobalSettings().getEmployeeForUpdate();
            setOperationMode(false, true, false, false);
            ent_FhrdEmpmst = fhrdEmpmstFacade.find(v_userid);
        } else if (getPIS_GlobalSettings().getEmployeeForView() != null) {
            v_userid = getPIS_GlobalSettings().getEmployeeForView();
            setOperationMode(false, false, true, getPIS_GlobalSettings().isForVerification());
            ent_FhrdEmpmst = fhrdEmpmstFacade.find(v_userid);
        } else {
            setOperationMode(true, false, false, false);
        }
        if (!createMode || (createMode && getPIS_GlobalSettings().isCreate_master_tbl_record_fhrd())) {
            getPIS_GlobalSettings().setEmployeeForUpdate(null);
            getPIS_GlobalSettings().setEmployeeForView(null);
            getPIS_GlobalSettings().setForVerification(false);
            resetForm();
        }

    }

    //<editor-fold defaultstate="collapsed" desc="resetForm">
    private void resetForm() {
        setDdl_user_type();
        setDdl_category();
        setDdl_religion();
        setDdl_designation();
        setDdl_reporting_user();
        setDdl_responsibility();
        setDdl_stream();
        setDdl_degree();
        setDdl_degree_level();
        setDdl_university();
        setDdl_subject();
        setDdl_posting_oum();
        setDdl_pass_month_value();
        setDdl_year_value();
        setDdl_mobile_code();

        if (createMode) {
            ent_FhrdEmpmst = new FhrdEmpmst();
            Random rand = new Random();
            image_random_name = String.valueOf(rand.nextInt(1000000) + 1);
            image_for_user_id = null;
        } else {
            image_for_user_id = image_random_name = ent_FhrdEmpmst.getUserId().toString();
        }
        checkForEditMode();
        setTbl_gen_tele_fax();
        setAuto_company();
        setAuto_designation();
        setAuto_college();
        setAuto_occupation();
    }
    //</editor-fold>
//    public void resetForm() {
//        //emp detail
//        ddl_posting_oum = null;
//        txt_user_name = null;
//        txt_user_name = null;
//        txt_user_last_name = null;
//        txt_emp_init = null;
//        ddl_blood_group = null;
//        ddl_gender = null;
//        cal_birthdate = null;
//        ddl_marital_status = null;
//        txt_home_town = null;
//        txt_pf_no = null;
//        txt_ntgcfpf_no = null;
//        txt_bank_account_no = null;
//        txt_pan_no = null;
//        ddl_reporting_user = null;
//        //event detail
//        cal_confirm_date = null;
//        cal_designation_date = null;
//        cal_org_joining_date = null;
//        cal_ou_joining_date = null;
//        cal_probation_due_date = null;
//        cal_training_due_date = null;
//        //contract detail
//        ddl_contract = null;
//        ddl_supplement_contract = null;
//        cal_contract_end_date = null;
//        cal_contract_start_date = null;
//        tbl_leaverule = null;
//        // address detail
//        txt_addr1 = null;
//        txt_addr2 = null;
//        txt_addr3 = null;
//        txt_city = null;
//        txt_pincode = null;
//        txt_state = null;
//        txt_country = null;
//        txt_present_addr1 = null;
//        txt_present_addr2 = null;
//        txt_present_addr3 = null;
//        txt_present_city = null;
//        txt_present_pincode = null;
//        txt_present_state = null;
//        txt_present_country = null;
//        txt_permanent_addr1 = null;
//        txt_permanent_addr2 = null;
//        txt_permanent_addr3 = null;
//        txt_permanent_city = null;
//        txt_permanent_pincode = null;
//        txt_permanent_state = null;
//        txt_permanent_country = null;
//        txt_email = null;
//        txt_website = null;
//        //experience detail
//        tbl_experience = null;
//        //qualification detail
//        tbl_qualification = null;
//        tbl_dependents = null;
//    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Default Constructor & Other Beans Methods"> 

    public Add_Edit_Employee() {
    }
    // <editor-fold defaultstate="collapsed" desc="Global Settings">
    // <editor-fold defaultstate="collapsed" desc="Global Settings">

    protected GlobalSettings getGlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="PIS Global Settings">

    protected PIS_GlobalSettings getPIS_GlobalSettings() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (PIS_GlobalSettings) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "PIS_GlobalSettings");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalUtilities">

    protected GlobalUtilities getGlobalUtilities() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalUtilities) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalUtilities");
    }// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="GlobalResources">

    protected GlobalResources getGlobalResources() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return (GlobalResources) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "GlobalResources");
    }// </editor-fold>
    // </editor-fold>
    //</editor-fold>   
    /////
    //<editor-fold defaultstate="collapsed" desc="selectedCountry">
    SystCountryMst selectedCountry;

    public SystCountryMst getSelectedCountry() {
        return selectedCountry;
    }

    public void setSelectedCountry(SystCountryMst selectedCountry) {
        this.selectedCountry = selectedCountry;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Ddl_mobile_code">
    String ddl_mobile_code;
    List<SelectItem> ddl_mobile_code_option;

    public String getDdl_mobile_code() {
        return ddl_mobile_code;
    }

    public void setDdl_mobile_code(String ddl_mobile_code) {
        this.ddl_mobile_code = ddl_mobile_code;
    }

    public List<SelectItem> getDdl_mobile_code_option() {
        return ddl_mobile_code_option;
    }

    public void setDdl_mobile_code_option(List<SelectItem> ddl_mobile_code_option) {
        this.ddl_mobile_code_option = ddl_mobile_code_option;
    }

    private void setDdl_mobile_code() {
        List<SystCountryMst> lst_CountryMst = systCountryMstFacade.findAll(globalData.getWorking_ou(), null);

        ddl_mobile_code_option = new ArrayList<SelectItem>();
        for (SystCountryMst c : lst_CountryMst) {
            ddl_mobile_code_option.add(new SelectItem(c.getCtryCode(), c.getCtryName() + " (" + c.getCtryCode() + ")"));
        }
        selectedCountry = systCountryMstFacade.findAll(globalData.getWorking_ou(), system_Properties.getCountryName()).get(0);
        ddl_mobile_code = selectedCountry.getCtryCode();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="General Telephone/Fax Settings">
    //<editor-fold defaultstate="collapsed" desc="tbl_gen_tele_fax">
    List<FcadTeleFax> tbl_gen_tele_fax;

    public List<FcadTeleFax> getTbl_gen_tele_fax() {
        return tbl_gen_tele_fax;
    }

    public void setTbl_gen_tele_fax(List<FcadTeleFax> tbl_gen_tele_fax) {
        this.tbl_gen_tele_fax = tbl_gen_tele_fax;
    }
    //<editor-fold defaultstate="collapsed" desc="selectedAddrMst">
    FcadAddrMst selectedAddrMst;

    public FcadAddrMst getSelectedAddrMst() {
        return selectedAddrMst;
    }

    public void setSelectedAddrMst(FcadAddrMst selectedAddrMst) {
        this.selectedAddrMst = selectedAddrMst;
    }
    //</editor-fold>

    private void setTbl_gen_tele_fax() {
        tbl_gen_tele_fax = new ArrayList<FcadTeleFax>();
        if (selectedAddrMst != null && selectedAddrMst.getFcadTeleFaxCollection() != null) {
            Collection<FcadTeleFax> lst_telefax = selectedAddrMst.getFcadTeleFaxCollection();
            for (FcadTeleFax t : lst_telefax) {
                if (t.getPdUniqueSrno() == null) {
                    tbl_gen_tele_fax.add(t);
                }
            }
        }
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Objects Settings">
    String ddl_gen_tel_fax;
    String ddl_gen_tel_fax_location;
    String txt_gen_tel_fax_number;

    public String getDdl_gen_tel_fax() {
        return ddl_gen_tel_fax;
    }

    public void setDdl_gen_tel_fax(String ddl_gen_tel_fax) {
        this.ddl_gen_tel_fax = ddl_gen_tel_fax;
    }

    public String getDdl_gen_tel_fax_location() {
        return ddl_gen_tel_fax_location;
    }

    public void setDdl_gen_tel_fax_location(String ddl_gen_tel_fax_location) {
        this.ddl_gen_tel_fax_location = ddl_gen_tel_fax_location;
    }

    public String getTxt_gen_tel_fax_number() {
        return txt_gen_tel_fax_number;
    }

    public void setTxt_gen_tel_fax_number(String txt_gen_tel_fax_number) {
        this.txt_gen_tel_fax_number = txt_gen_tel_fax_number;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="addToGenTeleFaxList">

    public void addToGenTeleFaxList(ActionEvent event) {
        FcadTeleFax ent_TeleFax = new FcadTeleFax();
        ent_TeleFax.setTelFaxUniqueSrno((-1) * (tbl_gen_tele_fax.size() + 1));
        ent_TeleFax.setAmUniqueSrno(selectedAddrMst);
        ent_TeleFax.setTelefax(ddl_gen_tel_fax.trim().toUpperCase());
        ent_TeleFax.setCountryCode(ddl_mobile_code.trim().toUpperCase());
        ent_TeleFax.setTelefaxno(txt_gen_tel_fax_number.trim().toUpperCase());
        ent_TeleFax.setLocation(ddl_gen_tel_fax_location.trim().toUpperCase());
        ent_TeleFax.setCreateby(new FhrdEmpmst(globalData.getUser_id()));
        ent_TeleFax.setCreatedt(new Date());
        ent_TeleFax.setOumUnitSrno(new SystOrgUnitMst(globalData.getWorking_ou()));
        tbl_gen_tele_fax.add(ent_TeleFax);
        RequestContext.getCurrentInstance().execute("pw_dlg_gen_tel_fax_entry.hide()");
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="deleteGenTeleFax">

    public void deleteGenTeleFax(FcadTeleFax p_gen_tele_fax) {
        tbl_gen_tele_fax.remove(p_gen_tele_fax);
    }
    //</editor-fold>
    //</editor-fold> 
}
