/* 
 * To change this template, choose Tools | Templates 
 * and open the template in the editor. 
 */ 
package org.fes.pis.jsf; 

import java.io.Serializable;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.System_Properties;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

@ManagedBean 
@ApplicationScoped 
public class SystemsList implements Serializable { 

    String systemName = new System_Properties().getSystemName(); 
    String sysetmUrl = new System_Properties().getSystemUrl(); 
    HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest(); 
    String project_root_url = req.getScheme() + "://" + req.getServerName() + ":" + req.getServerPort() + "/"; 
    String project_path = req.getScheme() + "://" + req.getServerName() + ":" + req.getServerPort() + req.getContextPath() + "/"; 
    //<editor-fold defaultstate="collapsed" desc="systemListHtml"> 
    String systemListHtml; 

    public String getSystemListHtml() { 
        return systemListHtml; 
    } 

    public void setSystemListHtml(String systemListHtml) { 
        this.systemListHtml = systemListHtml; 
    } 

    private void setSystemListHtml() { 
        try { 
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(); 
            DocumentBuilder db = dbf.newDocumentBuilder(); 
            Document doc = db.parse("http://database.fes.org.in/web-resources/fes-systems-list/config.xml"); 
            doc.getDocumentElement().normalize(); 
            NodeList systems = doc.getElementsByTagName("system"); 
            systemListHtml = "<ul class=\"system-list\">"; 
            for (int i = 0; i < systems.getLength(); i++) { 
                Element e = (Element) systems.item(i); 
                if (!sysetmUrl.equals(e.getElementsByTagName("system-url").item(0).getTextContent())) { 
                    systemListHtml += "<li>" 
                            + "<a href=\"" + project_root_url + e.getElementsByTagName("system-url").item(0).getTextContent() + "\">" 
                            + " <div class=\"icon\"><img src=\"" + e.getElementsByTagName("system-image").item(0).getTextContent() + "\" />" 
                            + "</div> " 
                            + "<div class=\"text\"> " 
                            + e.getElementsByTagName("system-name").item(0).getTextContent() 
                            + "</div> " 
                            + "</a> " 
                            + "</li> "; 
                } 
            } 
            systemListHtml += "</ul>"; 
        } catch (Exception ex) { 
            LogGenerator.generateLog(systemName, Level.SEVERE, GlobalResources.class.getName(), "setSystemListHtml()", null, ex); 
        } 
    } 

    //</editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="init()"> 
    @PostConstruct 
    public void init() { 
        setSystemListHtml(); 
    } 
    //</editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="Default Constructor"> 

    public SystemsList() { 
    } 
    //</editor-fold> 
} 

