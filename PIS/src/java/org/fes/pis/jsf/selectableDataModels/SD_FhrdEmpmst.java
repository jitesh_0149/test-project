package org.fes.pis.jsf.selectableDataModels;

import java.util.List;
import javax.faces.model.ListDataModel;
import org.fes.pis.entities.FhrdEmpmst;
import org.primefaces.model.SelectableDataModel;

public class SD_FhrdEmpmst extends ListDataModel<FhrdEmpmst> implements SelectableDataModel<FhrdEmpmst> {

    public SD_FhrdEmpmst() {
    }

    public SD_FhrdEmpmst(List<FhrdEmpmst> data) {
        super(data);
    }

    @Override
    public FhrdEmpmst getRowData(String rowKey) {
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  

        List<FhrdEmpmst> rows = (List<FhrdEmpmst>) getWrappedData();

        for (FhrdEmpmst row : rows) {
            if (row.getUserId().toString().equals(rowKey)) {
                return row;
            }
        }

        return null;
    }

    @Override
    public Object getRowKey(FhrdEmpmst row) {
        return row.getUserId();
    }
}
