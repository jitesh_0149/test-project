package org.fes.pis.jsf.selectableDataModels;  
  
import java.util.List;
import javax.faces.model.ListDataModel;
import org.fes.pis.custom_entities.AttendanceLockDataList;
import org.primefaces.model.SelectableDataModel;
  
public class SD_AttendanceLockDataList extends ListDataModel<AttendanceLockDataList> implements SelectableDataModel<AttendanceLockDataList> {    
  
    public SD_AttendanceLockDataList() {  
    }  
  
    public SD_AttendanceLockDataList(List<AttendanceLockDataList> data) {  
        super(data);  
    }  
      
    @Override  
    public AttendanceLockDataList getRowData(String rowKey) {  
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  
          
        List<AttendanceLockDataList> rows = (List<AttendanceLockDataList>) getWrappedData();  
          
        for(AttendanceLockDataList row : rows) {  
            if(row.getRowid().toString().equals(rowKey))  
                return row;  
        }  
          
        return null;  
    }  
  
    @Override  
    public Object getRowKey(AttendanceLockDataList row) {  
        return row.getRowid();
    }  
}  
            