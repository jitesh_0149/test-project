package org.fes.pis.jsf.selectableDataModels;  
  
import java.util.List;
import javax.faces.model.ListDataModel;
import org.fes.pis.entities.FtasAbsentees;
import org.primefaces.model.SelectableDataModel;
  
public class SD_FtasAbsentees extends ListDataModel<FtasAbsentees> implements SelectableDataModel<FtasAbsentees> {    
  
    public SD_FtasAbsentees() {  
    }  
  
    public SD_FtasAbsentees(List<FtasAbsentees> data) {  
        super(data);  
    }  
      
    @Override  
    public FtasAbsentees getRowData(String rowKey) {  
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  
          
        List<FtasAbsentees> rows = (List<FtasAbsentees>) getWrappedData();  
          
        for(FtasAbsentees row : rows) {  
            if(row.getAbsSrgKey().toString().equals(rowKey))  
                return row;  
        }  
          
        return null;  
    }  
  
    @Override  
    public Object getRowKey(FtasAbsentees row) {  
        return row.getAbsSrgKey();
    }  
}  
            