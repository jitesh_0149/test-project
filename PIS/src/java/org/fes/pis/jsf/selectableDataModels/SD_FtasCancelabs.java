package org.fes.pis.jsf.selectableDataModels;  
  
import java.util.List;
import javax.faces.model.ListDataModel;
import org.fes.pis.entities.FtasCancelabs;
import org.primefaces.model.SelectableDataModel;
  
public class SD_FtasCancelabs extends ListDataModel<FtasCancelabs> implements SelectableDataModel<FtasCancelabs> {    
  
    public SD_FtasCancelabs() {  
    }  
  
    public SD_FtasCancelabs(List<FtasCancelabs> data) {  
        super(data);  
    }  
      
    @Override  
    public FtasCancelabs getRowData(String rowKey) {  
        //In a real app, a more efficient way like a query by rowKey should be implemented to deal with huge data  
          
        List<FtasCancelabs> rows = (List<FtasCancelabs>) getWrappedData();  
          
        for(FtasCancelabs row : rows) {  
            if(row.getCabsSrgKey().toString().equals(rowKey))  
                return row;  
        }  
          
        return null;  
    }  
  
    @Override  
    public Object getRowKey(FtasCancelabs row) {  
        return row.getCabsSrgKey();
    }  
}  
            