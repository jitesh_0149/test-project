package org.fes.pis.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.fes.lib.utilities.LogGenerator;
import org.fes.pis.custom_entities.System_Properties;

/**
 *
 * @author Krishna
 */
public class Logout extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        System_Properties system_Properties = new System_Properties();
        PrintWriter out = response.getWriter();
        try {
            request.getSession().invalidate();
            request.getSession().invalidate();
            request.getSession().invalidate();
            request.getSession().invalidate();
            //response.sendRedirect("/FIS-war/Login.html");
        } catch (Exception e) {
        
            LogGenerator.generateLog(system_Properties.getSystemName(), Level.SEVERE, "Logout", null, "Exceptions logout from MIS", e);
        } finally {
            response.sendRedirect("/" + system_Properties.getSystemUrl() + "/Login.htm");
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
