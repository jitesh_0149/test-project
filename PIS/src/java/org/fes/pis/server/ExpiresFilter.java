package org.fes.pis.server;


import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;

public class ExpiresFilter implements Filter { 

    private FilterConfig filterConfig; 

    public ExpiresFilter() { 
    } 

    private void addCacheHeaders(ServletRequest request, ServletResponse response) 
            throws IOException, ServletException { 

        HttpServletResponse sr = (HttpServletResponse) response; 
        sr.setHeader("Cache-Control", "max-age=31540000;public;must-revalidate;"); 

    } 

    @Override 
    public void doFilter(ServletRequest request, ServletResponse response, 
            FilterChain chain) 
            throws IOException, ServletException { 

        addCacheHeaders(request, response); 
        chain.doFilter(request, response); 
    } 

    private FilterConfig getFilterConfig() { 
        return filterConfig; 
    } 

    private void setFilterConfig(FilterConfig filterConfig) { 
        this.filterConfig = filterConfig; 
    } 

    @Override 
    public void destroy() { 
    } 

    @Override 
    public void init(FilterConfig filterConfig) { 
        setFilterConfig(filterConfig); 
    } 

    @Override 
    public String toString() { 
        if (getFilterConfig() == null) { 
            return ("ExpiresFilter()"); 
        } 
        return ("ExpiresFilter(" + getFilterConfig() + ")"); 

    } 
} 
