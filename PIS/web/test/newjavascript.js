function ValidateEntriesForFundingAgency(){
    $('#tabview1_id\\:err_txt_short_name').hide();
    $('#tabview1_id\\:err_txt_agency_name').hide();
    $('#err_ddl_addresses').hide();
    $('#err_ddl_agency_type').hide();
    $('#tabview1_id\\:err_cal_start_date').text('');
    $('#tabview1_id\\:err_cal_end_date').text('');
    valid=true;
    if(!isvalid_text_val('tabview1_id\\:txt_short_name')){
        $('#tabview1_id\\:err_txt_short_name').text('Please Enter Short Name');
        $('#tabview1_id\\:err_txt_short_name').show();
        valid=false;
    }else{
        $('#tabview1_id\\:txt_short_name').val($('#tabview1_id\\:txt_short_name').val().toUpperCase());
    }
    if(!isvalid_text_val('tabview1_id\\:txt_agency_name')){
        $('#tabview1_id\\:err_txt_agency_name').text('Please Enter agency Name');
        $('#tabview1_id\\:err_txt_agency_name').show();
        valid=false;
    }
    else{
        $('#tabview1_id\\:txt_agency_name').val($('#tabview1_id\\:txt_agency_name').val().toUpperCase());
    }
    if($('#tabview1_id\\:ddl_addresses').val()==-50 ||$('#tabview1_id\\:ddl_addresses').val()==-51){
        $('#err_ddl_addresses').show();
        valid=false;
    }
    if(document.getElementById('tabview1_id:ddl_agency_type').selectedIndex==0){
        $('#err_ddl_agency_type').show();
        valid=false;
    }
    if(!isvalid_text_val('cal_start_date_input')){
        valid=false;
        $('#tabview1_id\\:err_cal_start_date').text('Please enter start date.');        
    }else if(isvalid_text_val('cal_end_date_input')){
        if(findBigSmallDate($('#tabview1_id\\:cal_start_date_input').val(),$('#tabview1_id\\:cal_end_date_input').val(),null,'S')!=$('#tabview1_id\\:cal_start_date_input').val()){
            valid=false;
            $('#tabview1_id\\:err_cal_start_date').text('Start date can not be greater than end date.');
        }
    }    
    if(isvalid_text_val('hdn_cal_end_date_util')){
        if(!isvalid_text_val('cal_end_date_input')){
            $('#tabview1_id\\:err_cal_end_date').text('Please enter end date');
            valid=false;
        }
    }
    if(valid==true){
        SetAndShowDialog('WaitDialog','Saving...','Please wait while record is being saved');
        return true;
    }else{
        return false;
    }

}