last_input_type=null;
current_input_type=null;
braces_count=0;
dot_count=0;
var actions=new Array();
var actions_type=new Array();
actions_count=0;

(function($){
    tile = function() {
    };
})(jQuery);
function is_valid(){
    v_txt_str_box=$('#txt_str_box').val();
    v_txt_str_box_length=v_txt_str_box.length;
    var re = /[\+\-\/\*\.]/;
    if(braces_count!=0){
        alert('Parenthasis Mismatch');
        return false;
    }else if(re.test(v_txt_str_box.substring(v_txt_str_box_length-1, v_txt_str_box_length))){
        alert('Equation is incomplete');
        return false;
    }else if(is_devide_by_zero()){
        alert('Can not devide by zero');
        return false;
    }else{
        return true;
    }

}
function is_devide_by_zero(){
    var re = /[0-9\.]/;
    var v_final_str='';
    v_txt_str_box=$('#txt_str_box').val();
    v_txt_str_box_length=v_txt_str_box.length;
    v_current_char=v_txt_str_box.substring(v_txt_str_box_length-1, v_txt_str_box_length);
    var i=0;
    while(i<v_txt_str_box_length){
        if((/[\/]/).test(v_txt_str_box.substring(i, i+1))){
            i++;
            if((/[0\.]/).test(v_txt_str_box.substring(i, i+1))){
                while((/[0-9\.]/).test(v_txt_str_box.substring(i, i+1))){
                    v_final_str=v_final_str+v_txt_str_box.substring(i, i+1);
                    i++;
                }
            }
            if(parseFloat(v_final_str)==0 ){
                return true;
            }
        }
        i++;
    }
    return false;
}
function remove_last_value(){
    if(actions_count>0){
        v_txt_str_box=$('#txt_str_box').val();
        v_txt_str_box_length=v_txt_str_box.length;
        $('#txt_str_box').val(v_txt_str_box.substring(0, v_txt_str_box_length-actions[actions_count-1].toString().length));
        if(actions[actions_count-1].toString().indexOf('(')!=-1){
            braces_count--;
        }else if(actions[actions_count-1].toString().indexOf(')')!=-1){
            braces_count++;
        }else if(actions[actions_count-1].toString().indexOf('.')!=-1){
            dot_count=0;
        }
        //Logic for dot
        v_txt_str_box=$('#txt_str_box').val();
        v_txt_str_box_length=v_txt_str_box.length;
        var re = /[0-9\.]/;
        dot_count=0;
        v_current_char=v_txt_str_box.substring(v_txt_str_box_length-1, v_txt_str_box_length);
        while(re.test(v_current_char)){
            v_txt_str_box=v_txt_str_box.substring(0, (parseFloat(v_txt_str_box_length--)));
            if(v_current_char=='.'){
                dot_count=1;
                break;
            }
            v_current_char=v_txt_str_box.substring(v_txt_str_box_length-1, v_txt_str_box_length);
        }
        actions_count--;
        last_input_type=actions_type[actions_count-1];
    }
}

function clear_input(){
    $('#txt_str_box').val('');
    last_input_type=null;
    current_input_type=null;
    braces_count=0;
    dot_count=0;
    var actions=new Array();
    var actions_type=new Array();
    actions_count=0;
}

function insert_digit(p_char){
    current_input_type='dig';
    if(check_input(p_char)){
        $('#txt_str_box').val($('#txt_str_box').val()+p_char);
        last_input_type='dig';
        actions[actions_count]=p_char;
        actions_type[actions_count]=last_input_type;
        actions_count++;

    }
}
function insert_operator(p_char){
    current_input_type='op';
    if(check_input(p_char)){
        $('#txt_str_box').val($('#txt_str_box').val()+p_char);
        last_input_type='op';
        actions[actions_count]=p_char;
        actions_type[actions_count]=last_input_type;
        actions_count++;
        dot_count=0;
    }
}
function insert_dot(p_char){
    current_input_type='dot';
    if(check_input(p_char) && dot_count==0){
        $('#txt_str_box').val($('#txt_str_box').val()+p_char);
        last_input_type='dot';
        actions[actions_count]=p_char;
        actions_type[actions_count]=last_input_type;
        actions_count++;
        dot_count++;
    }
}
function insert_left_brace(p_char){
    current_input_type='(';
    if(check_input(p_char)){
        $('#txt_str_box').val($('#txt_str_box').val()+p_char);
        last_input_type='(';
        actions[actions_count]=p_char;
        actions_type[actions_count]=last_input_type;
        actions_count++;
        braces_count++;
    }
}
function insert_right_brace(p_char){
    if(braces_count>0){
        current_input_type=')';
        if(check_input(p_char)){
            $('#txt_str_box').val($('#txt_str_box').val()+p_char);
            last_input_type=')';
            actions[actions_count]=p_char;
            actions_type[actions_count]=last_input_type;
            actions_count++;
            braces_count--;
            dot_count=0;
        }
    }
}

function insert_ddl_fun(p_char){
    if(p_char.trim().length>0){
        current_input_type='ddl_fun';    
        p_char=p_char+'(';
        if(check_input(p_char)){
            $('#txt_str_box').val($('#txt_str_box').val()+p_char);
            last_input_type='ddl_fun';
            actions[actions_count]=p_char;
            actions_type[actions_count]=last_input_type;
            actions_count++;
            braces_count++;
        }
    }
}

function insert_ddl_code(p_char){
    if(p_char.trim().length>0){
        current_input_type='ddl_code';
        if(check_input(p_char)){
            $('#txt_str_box').val($('#txt_str_box').val()+p_char);
            last_input_type='ddl_code';
            actions[actions_count]=p_char;
            actions_type[actions_count]=last_input_type;
            actions_count++;
        }
    }
    
    $('#ddl_ed_code_list_input').val('');
}

function check_input(p_char){
    if(last_input_type=='dig'){
        if(current_input_type=='dig' || current_input_type=='op' || current_input_type==")" ||current_input_type=='dot'){
            return true;
        }else{
            return false;
        }
    }
    if(last_input_type=='dot'){
        if(current_input_type=='dig'){
            return true;
        }else{
            return false;
        }
    }
    if(last_input_type=='ddl_code'){
        if(current_input_type=='op' || current_input_type==')'){
            return true;
        }else{
            return false;
        }
    }
    if(last_input_type=='ddl_fun'){
        if(current_input_type=='dig' || current_input_type=='(' || current_input_type=='ddl_code' || p_char=='-' || current_input_type=='ddl_fun'){
            return true;
        }else{
            return false;
        }
    }

    if(last_input_type=='op'){
        if(current_input_type=='dig' || current_input_type=='(' || current_input_type=='ddl_code' || current_input_type=='ddl_fun' || current_input_type=='dot'){
            return true;
        }else{
            return false;
        }
    }
    if(last_input_type==')' ){
        if(current_input_type=='op' || current_input_type==')' ){
            return true;
        }else{
            return false;
        }
    }
    if(last_input_type=='(' ){
        if(current_input_type=='dig' || current_input_type=='(' || current_input_type=='ddl_code' || p_char=='-' || current_input_type=='ddl_fun' || current_input_type=='dot'){
            return true;
        }else{
            return false;
        }
    }
    if(last_input_type==null){
        if(current_input_type=='dig' || current_input_type=='(' || current_input_type=='ddl_code' || current_input_type=='ddl_fun' || current_input_type=='dot'){
            return true;
        }else{
            return false;
        }
    }

    return false;
}
