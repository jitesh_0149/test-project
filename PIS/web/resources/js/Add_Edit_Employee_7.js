var valid;
var img_uploaded;
var isCreateMode;
var isViewMode;
var isUpdateMode;
var forVerification;

function validateGenTeleFax(){
    var valid=true;
    $('#err-tel-fax').text('');
    if($('#add_Edit_Employee\\:txt_gen_tel_fax_number').val().trim()==''){
        $('#err-tel-fax').text('Please enter all required fields');
        valid=false;
    }
    return valid;
}
//Other

$(document).ready(function() {                
    setNumericEntry(); 
    img_uploaded=true;
    isCreateMode=$('#add_Edit_Employee\\:hdn_is_create_mode').val()=='true'?true:false;
    isViewMode=$('#add_Edit_Employee\\:hdn_is_view_mode').val()=='true'?true:false;
    isUpdateMode=$('#add_Edit_Employee\\:hdn_is_update_mode').val()=='true'?true:false;
    forVerification=$('#add_Edit_Employee\\:hdn_forVerification').val()=='true'?true:false;
});
function setNumericEntry(){
    $('.number-entry-4-2').autoNumeric({
        aSep: '', 
        dGroup: '0', 
        vMin: '0',
        vMax:'99.99',
        aPad: false
    });     
    $('.number-entry-4-0').autoNumeric({
        aSep: '', 
        dGroup: '0', 
        vMin: '0',
        vMax:'9999',
        aPad: false
    });
    $('.number-entry-3-0').autoNumeric({
        aSep: '', 
        dGroup: '0', 
        vMin: '0',
        vMax:'999',
        aPad: false
    });
    $('.number-entry-2-0-max-12').autoNumeric({
        aSep: '', 
        dGroup: '0', 
        vMin: '0',
        vMax:'12',
        aPad: false
    });
    $('.number-entry-10-0').autoNumeric({
        aSep: '', 
        dGroup: '0', 
        vMin: '0',
        vMax:'9999999999',
        aPad: false
    });
}
function AddressSaveStarts(){                                   
    $('#err_txt_email').text('');
    $('#err_txt_email').hide();
    $('#err_add_address').hide();                            
    if(!isvalid_text_val('add_Edit_Employee\\:txt_addr1')||
        !isvalid_text_val('add_Edit_Employee\\:txt_city')||
        !isvalid_text_val('add_Edit_Employee\\:txt_state')||
        !isvalid_text_val('add_Edit_Employee\\:txt_country')
        ){
        return false;
    }else{ 
        $('#add_Edit_Employee\\:txt_city').val($('#add_Edit_Employee\\:txt_city').val().toUpperCase());
        $('#add_Edit_Employee\\:txt_state').val($('#add_Edit_Employee\\:txt_state').val().toUpperCase());
        $('#add_Edit_Employee\\:txt_country').val($('#add_Edit_Employee\\:txt_country').val().toUpperCase());
        if(isvalid_text_val('txt_email')){
            if(!validateEmail()){                                                    
                return false;
            }
        }     
        if(isvalid_text_val('txt_email_alt')){
            if(!validateEmailAlt()){                                                    
                return false;
            }
        }     
    }
    return true;
}


function validateEmail(){
    var re=/^(\w+[\-\.])*\w+@(\w+\.)+[A-Za-z]+$/;
    $('#err_txt_email').text('');
    $('#err_txt_email').hide();
    if(!re.test($('#add_Edit_Employee\\:txt_email').val())){
        $('#err_txt_email').text('Please enter valid email');
        $('#err_txt_email').show();
        return false;
    }
    return true;
}
function validateEmailAlt(){
    var re=/^(\w+[\-\.])*\w+@(\w+\.)+[A-Za-z]+$/;
    $('#err_txt_email_alt').text('');
    $('#err_txt_email_alt').hide();
    if(!re.test($('#add_Edit_Employee\\:txt_email_alt').val())){
        $('#err_txt_email_alt').text('Please enter valid email');
        $('#err_txt_email_alt').show();
        return false;
    }
    return true;
}



function isValid_new_employee(){
    var v_valid=true;
    if(!forVerification){
        if(!img_uploaded){
            SetAndShowDialog('GeneralDialog','Please wait','Please wait until the image is uploaded');                                            
            return false;
        }
        $('#err_user_name').text('');
        $('#err_gender').text('');
        $('#err_marital_status').text('');
        $('#err_ddl_nationality').text('');
        $('#err_birthdate').text('');
        $('#err_physically_challenged').text('');
        $('#err_user_type').text('');
        $('#err_ddl_category').text('');
        $('#err_ddl_designation').text('');
        $('#err_father_name').text('');
        $('#err_mother_name').text('');
        $('#err_contract_start_date').text('');
        $('#err_contract_end_date').text('');
        $('#err_exp_cons_at_joining').text('');
        if($('#add_Edit_Employee\\:ddl_nationality select').val().trim()==''){
            $('#err_ddl_nationality').text('Please select Nationality');
        }            
        if($('#add_Edit_Employee\\:txt_user_name').val().trim()=='' || $('#add_Edit_Employee\\:txt_user_last_name').val().trim()==''){
            $('#err_user_name').text('Please enter First Name/Last Name');
            v_valid=false;
        }
        if($('#add_Edit_Employee\\:txt_father_name').val()==''){
            $('#err_father_name').text("Please enter Father's Name");
            v_valid=false;
        }
        if($('#add_Edit_Employee\\:txt_mother_name').val()==''){
            $('#err_mother_name').text("Please enter mother's Name");
            v_valid=false;
        }
   
        if($('#add_Edit_Employee\\:ddl_gender_input').val()==''){
            $('#err_gender').text('Please select Gender');
            v_valid=false;
        }
        if($('#add_Edit_Employee\\:ddl_marital_status_input').val()==''){
            $('#err_marital_status').text('Please select Marital Status');
            v_valid=false;
        }
        if($('#add_Edit_Employee\\:cal_birthdate_input').val().trim()==''){
            $('#err_birthdate').text('Please select Birthdate');
            v_valid=false;
        }
        if($('#add_Edit_Employee\\:ddl_user_type_input').val()==''){
            $('#err_user_type').text('Please select User Type');
            v_valid=false;
        }
        if($('#add_Edit_Employee\\:ddl_category_input').val()==''){
            $('#err_ddl_category').text('Please select Category');
            v_valid=false;
        }
        if($('#add_Edit_Employee\\:ddl_physically_challenged_input').val().trim()==''){
            $('#err_physically_challenged').text('Please select physically challenged status');
            v_valid=false;
        }
        if(!AddressSaveStarts()){
            $('#err_add_address').show();
            v_valid=false;
        }
        if($('#add_Edit_Employee\\:ddl_posting_oum_input').val()==''){
            $('#err_posting_oum').text('Please select Posting Organization');
            v_valid=false;
        }
        if($('#add_Edit_Employee\\:ddl_reporting_user_input').val()==''){
            $('#err_reporting_user').text('Select Reporting User Id');
            v_valid=false;
        }
        if($('#add_Edit_Employee\\:ddl_designation_input').val()==''){
            $('#err_ddl_designation').text('Select Designation');
            v_valid=false;
        }        
        if(isCreateMode){
            if($('#add_Edit_Employee\\:cal_org_joining_date_input').val()==''){
                $('#err_org_joining_date').text('Select Organization joining date');
                v_valid=false;
            }
            if($('#add_Edit_Employee\\:cal_ou_joining_date_input').val()==''){
                $('#err_ou_joining_date').text('Select Ou joining date');
                v_valid=false;
            }
           // if($('#add_Edit_Employee\\:cal_confirm_date_input').val()==''){
           //     $('#err_confirm_date').text('Select Confirm date');
           //     v_valid=false;
           // }
            
            if($('#add_Edit_Employee\\:cal_contract_start_date_input').val()==''){
                $('#err_contract_start_date').text('Select Contract Start date');
                v_valid=false;
            }
            if($('#add_Edit_Employee\\:cal_contract_end_date_input').val()==''){
                $('#err_contract_end_date').text('Select Contract End date');
                v_valid=false;
            }
            if($('#add_Edit_Employee\\:ddl_contract_input').val()==''){
                $('#err_contract').text('Select Contract');
                v_valid=false;
            }
        }else if(isUpdateMode){
            $('#err_txt_updation_remark').text('');
            if($('#add_Edit_Employee\\:txt_updation_remark').val().trim()==''){
                $('#err_txt_updation_remark').text('Please enter remark for updation');
                v_valid=false;
            }
        }        
    }
    return v_valid;
}

function isValid_qualification(){
    var valid=true;
    $('#err_ddl_degree').text('');
    $('#err_ddl_subject').text('');
    $('#err_ddl_university').text('');
    $('#err_auto_college').text('');
    $('#err_ddl_pass_month').text('');
    $('#err_ddl_pass_year').text('');
    if($('#add_Edit_Employee\\:ddl_degree_input').val().trim()==''){
        $('#err_ddl_degree').text('Select Degree');
        valid=false;
    }    
    if($('#add_Edit_Employee\\:ddl_university_input').val().trim()=='' && $('#add_Edit_Employee\\:auto_college input').val().trim()==''){
        $('#err_ddl_university').text('Select any one of University or College/School');
        valid=false;
    }    
    if($('#add_Edit_Employee\\:ddl_pass_year_input').val().trim()==''){
        $('#err_ddl_pass_year').text('Select passing year');
        valid=false;
    }
    return valid;
}

function isValid_experience(){
    var valid=true;
    $('#err_auto_company').text('');
    $('#err_auto_designation').text('');
    $('#err_cal_exp_from_date').text('');
    $('#err_cal_exp_to_date').text('');
    if($('#add_Edit_Employee\\:auto_company input').val().trim()==''){
        $('#err_auto_company').text('Enter Company Name');
        valid=false;
    }
    if($('#add_Edit_Employee\\:auto_designation input').val().trim()==''){
        $('#err_auto_designation').text('Enter Designation');
        valid=false;
    }
    if(pw_cal_exp_from_date.getDate()==null){
        $('#err_cal_exp_from_date').text('Select From Date');
        valid=false;
    }
    if(pw_cal_exp_to_date.getDate()==null){
        $('#err_cal_exp_to_date').text('Select To Date');
        valid=false;
    }
    if(pw_cal_exp_from_date.getDate()!=null && pw_cal_exp_to_date.getDate()!=null){
        if(pw_cal_exp_from_date.getDate()>pw_cal_exp_to_date.getDate()){
            $('#err_cal_exp_to_date').text('To date can not be greater than from date');
            valid=false;
        }
    }
    

    return valid;
}

function isValid_dependent(){
    valid=true;
    $('#err_txt_dependent_name').text('');
    $('#err_ddl_dependent_relation').text('');
    $('#err_ddl_dependent_gender').text('');
    $('#err_cal_dependent_birthdate').text('');
    $('#err_auto_occupation').text('');
    if($('#add_Edit_Employee\\:txt_dependent_name').val()==''){
        $('#err_txt_dependent_name').text('Enter Name');
        valid=false;
    }
    if($('#add_Edit_Employee\\:ddl_dependent_relation select').val().trim()==''){
        $('#err_ddl_dependent_relation').text('Enter Relation');
        valid=false;
    }
    if($('#add_Edit_Employee\\:ddl_dependent_gender_input').val().trim()==''){
        $('#err_ddl_dependent_gender').text('Select Gender');
        valid=false;
    }
    if($('#add_Edit_Employee\\:cal_dependent_birthdate_input').val().trim()==''){
        $('#err_cal_dependent_birthdate').text('Select Birthdate');
        valid=false;
    }
    if($('#add_Edit_Employee\\:auto_occupation input').val().trim()==''){
        $('#err_auto_occupation').text('Enter Occupation');
        valid=false;
    }
    return valid;
}
//University Degree Subject Common Settings
var uni_deg_sub_flag;
function showUni_Deg_Sub_Dialog(p_type){
    $('#add_Edit_Employee\\:pnl_university_mst').hide();                    
    $('#add_Edit_Employee\\:pnl_degree_mst').hide();                    
    $('#add_Edit_Employee\\:pnl_subject_mst').hide();                    
    if(p_type=='U'){
        $('#add_Edit_Employee\\:dlg_uni_deg_sub .ui-dialog-title').text('New University')
        $('#add_Edit_Employee\\:pnl_university_mst').show();
    }else if(p_type=='D'){
        $('#add_Edit_Employee\\:dlg_uni_deg_sub .ui-dialog-title').text('New Degree')
        $('#add_Edit_Employee\\:pnl_degree_mst').show();
    }else if(p_type=='S'){
        $('#add_Edit_Employee\\:dlg_uni_deg_sub .ui-dialog-title').text('New Subject')
        $('#add_Edit_Employee\\:pnl_subject_mst').show();
    }
    pw_dlg_uni_deg_sub.show();
}
function sameAsAddress(p_add_type){
    $('#add_Edit_Employee\\:txt_'+p_add_type+'_addr1').val($('#add_Edit_Employee\\:txt_addr1').val());
    $('#add_Edit_Employee\\:txt_'+p_add_type+'_addr2').val($('#add_Edit_Employee\\:txt_addr2').val());
    $('#add_Edit_Employee\\:txt_'+p_add_type+'_addr3').val($('#add_Edit_Employee\\:txt_addr3').val());
    $('#add_Edit_Employee\\:txt_'+p_add_type+'_city').val($('#add_Edit_Employee\\:txt_city').val());
    $('#add_Edit_Employee\\:txt_'+p_add_type+'_state').val($('#add_Edit_Employee\\:txt_state').val());
    $('#add_Edit_Employee\\:txt_'+p_add_type+'_pincode').val($('#add_Edit_Employee\\:txt_pincode').val());
    $('#add_Edit_Employee\\:txt_'+p_add_type+'_country').val($('#add_Edit_Employee\\:txt_country').val());           
}
var parent_window = window.opener;
function updateParent(){    
    
    try{
        
        closeWindow();
    }catch(e){}
    if (parent_window && !parent_window.closed) {                                
        parent_window.document.getElementById('btn_refresh').click();
    }
}
function redirectToList(){
    window.location='PIS_Employee_Information.htm';
}
function closeWindow(){    
    window.onbeforeunload = function(e) {
            return null;
        };
        window.close();
}
function setHeaderDetailSaveUser(){
    $('#add_Edit_Employee\\:dlg_confirm_change .ui-dialog-title').text('Confirmation');
    //$('#add_Edit_Employee\\:dlg_confirm_change .ui-confirm-dialog-message').text('After saving this record no changes can be made until user verifies  or comments on the detail. Are you sure to save employee\'s details?')
    $('#add_Edit_Employee\\:dlg_confirm_change .ui-confirm-dialog-message').text('Are you sure to save the changes?')
}
function setHeaderDetailVerify(){
    $('#add_Edit_Employee\\:dlg_confirm_change .ui-dialog-title').text('Confirmation');
    $('#add_Edit_Employee\\:dlg_confirm_change .ui-confirm-dialog-message').text('Kindly confirm you have verified all the details before saving. Are you sure?')
}