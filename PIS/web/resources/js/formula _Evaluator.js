function getLastCharacter(){
    var v_length=$('#txt_str_box').val().length;    
    return $('#txt_str_box').val().substr(v_length-1,v_length);
}
function insert_digit(p_digit){            
    if( (/^[0-9\+\-\(\*\/\.]*$/).test(getLastCharacter())){
        $('#txt_str_box').val($('#txt_str_box').val()+p_digit);
    }
}
function insert_operator(p_operator){
    if(p_operator!='-' && !(/^[\+\-\(\*\/\.]*$/).test(getLastCharacter())){
        $('#txt_str_box').val($('#txt_str_box').val()+p_operator);
    }else if(p_operator=='-' && !(/[\+\-\*\/\.]/).test(getLastCharacter())){
        $('#txt_str_box').val($('#txt_str_box').val()+p_operator);
    }
}
function insert_left_brace(){
    if((/^[\+\-\(\*\/]*$/).test(getLastCharacter())){
        $('#txt_str_box').val($('#txt_str_box').val()+'(');
    }
}
function insert_right_brace(){
    if(!(/^[\+\-\(\*\/\.]*$/).test(getLastCharacter())){
        var containsLeftBraces=$('#txt_str_box').val().indexOf('(')==-1?false:true;
        var containsRightBraces=$('#txt_str_box').val().indexOf(')')==-1?false:true;
        if(containsLeftBraces && ((containsRightBraces && $('#txt_str_box').val().match(/\(/g).length>$('#txt_str_box').val().match(/\)/g).length)
            || !containsRightBraces)){
            $('#txt_str_box').val($('#txt_str_box').val()+')');
        }        
    }
}
function insert_ddl_code(){    
    if((/^[\+\-\(\*\/\(]*$/).test(getLastCharacter())){
        $('#txt_str_box').val($('#txt_str_box').val()+$('#ddl_ed_code_list select').val());
    }else{
        if(!(/^[0-9\.\)]*$/).test(getLastCharacter())){
            remove_last_value();
            insert_ddl_code();
        }
    }    
}
function insert_dot(){
    if((/^[0-9]*$/).test(getLastCharacter())){
        var v_value=$('#txt_str_box').val();        
        if(v_value.substr(v_value.replace(/[\+\-\(\*\/]/g, '@').lastIndexOf('@')+1,v_value.length).indexOf('.')==-1){
            $('#txt_str_box').val($('#txt_str_box').val()+'.');
        }
    }
}
function remove_last_value(){
    var v_value=$('#txt_str_box').val();
    if((/^[0-9\.\+\-\(\*\/\(\)]*$/).test(getLastCharacter())){                
        $('#txt_str_box').val(v_value.substr(0,v_value.length-1));
    }else{        
        $('#txt_str_box').val(v_value.substr(0,v_value.replace(/[0-9\.\+\-\(\*\/\)]/g, '@').lastIndexOf('@')+1));        
    }
}
function is_valid(){    
    if((/[\+\-\/\*\.\(]/).test(getLastCharacter())){
        SetAndShowDialog('GeneralDialog','Formula validation failed','Formula is incomplete');
        return false;
    }else if(!is_paranthesis_valid()){
        SetAndShowDialog('GeneralDialog','Formula validation failed','Paranthesis mismatch');
        return false;
    }else  if(is_devide_by_zero()){
        SetAndShowDialog('GeneralDialog','Formula validation failed','Can not devide by zero');
        return false;
    }else{
        return true;
    }

}
function is_devide_by_zero(){    
    var v_value=$('#txt_str_box').val();
    var v_splitted=v_value.split('/');
    var n=v_splitted.length,i;
    
    for(i=0;i<n-1;i++){
        var v_split_value=v_splitted[i+1];
        if((/^[0-9]*$/).test(v_split_value.substr(0,1))){            
            var v_last_index=v_split_value.replace(/[\+\-\*\)]/g, '@').indexOf('@');
            v_split_value=v_split_value.substr(0,v_last_index==-1?v_split_value.length:v_last_index)                        
        }
    }
    
    return parseFloat(v_split_value)==parseFloat('0');
}

function is_paranthesis_valid(){
    var containsLeftBraces=$('#txt_str_box').val().indexOf('(')==-1?false:true;
    var containsRightBraces=$('#txt_str_box').val().indexOf(')')==-1?false:true;
    if(containsLeftBraces && ((!containsRightBraces || $('#txt_str_box').val().match(/\(/g).length!=$('#txt_str_box').val().match(/\)/g).length))){
        return false;
    }    
    return true;
}
function clear_input(){
    $('#txt_str_box').val('');
}
