![FES icon](http://fes.org.in/images/logo_fes.gif)
# Personnel Information System (PIS)
### Components
- Personnel Information
- Time Attendance System
- Tour & Travelling Bill (Under Development)
- Reports

### Benefits
- User friendly interface
- Manages user's personal, qualification related, experience related, and other informations
- Easy maintanance of employee leaves, leave rules, contracts.
- Allow to mark daily presence as well as missing attendance of an employee.
- Facilitates to create leave/tour/training applications and to manage their status(i.e. approve/reject) with reason if any.
- Automatic leave balance management according rules.
- Facilitates locking of employee's attendance and attendance calculation. Attendance calculation generates information for payable and non-payable days which is useful in  salary disbursment.
- Various useful reports can be generated.
- The need of sending dumps from team to CO is now not necessary as it is accessible directly from CO.
